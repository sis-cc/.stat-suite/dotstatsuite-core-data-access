using System;
using System.Data.Common;
using DbUp;
using DbUp.Engine.Output;
using MySqlConnector;

namespace DotStat.DbUp;

public static class MySqlExtensions {
	public static void MySqlDatabase(
		this SupportedDatabasesForDropDatabase supported,
		string connectionString,
		int commandTimeout) {
		supported.MySqlDatabase(connectionString, new ConsoleUpgradeLog(), commandTimeout);
	}

	public static void MySqlDatabase(
		this SupportedDatabasesForDropDatabase supported,
		string connectionString,
		IUpgradeLog logger,
		int timeout = -1) {
		string masterConnectionString;
		string databaseName;
		GetMySqlMasterConnectionStringBuilder(connectionString, logger, out masterConnectionString, out databaseName);
		using (MySqlConnection connection = new (masterConnectionString)) {
			connection.Open();
			if (!MySqlDatabaseExists(connection, databaseName))
				return;
			MySqlCommand sqlCommand1 = new (@$"
#Kill all connections
SELECT concat('KILL ',id,';') 
FROM information_schema.processlist 
WHERE db = '{databaseName}';

#Drop db
DROP DATABASE IF EXISTS `{databaseName}`;", connection);

			using (sqlCommand1)
				sqlCommand1.ExecuteNonQuery();
			logger.LogInformation("Dropped database {0}", databaseName);
		}
	}

	private static void GetMySqlMasterConnectionStringBuilder(
		string connectionString,
		IUpgradeLog logger,
		out string masterConnectionString,
		out string databaseName) {
		if (string.IsNullOrEmpty(connectionString) || connectionString.Trim() == string.Empty)
			throw new ArgumentNullException(nameof (connectionString));
		if (logger == null)
			throw new ArgumentNullException(nameof (logger));
		MySqlConnectionStringBuilder connectionStringBuilder1 = new (connectionString);
		databaseName = connectionStringBuilder1.Database;
		connectionStringBuilder1.Database = null;
		if (string.IsNullOrEmpty(databaseName) || databaseName.Trim() == string.Empty)
			throw new InvalidOperationException("The connection string does not specify a database name.");
		MySqlConnectionStringBuilder connectionStringBuilder2 = new (connectionStringBuilder1.ConnectionString) {
			Password = string.Empty.PadRight(connectionStringBuilder1.Password.Length, '*')
		};
		logger.LogInformation("ConnectionString => {0}", connectionStringBuilder2.ConnectionString);
		masterConnectionString = connectionStringBuilder1.ConnectionString;
	}

	private static bool MySqlDatabaseExists(MySqlConnection connection, string databaseName) {
		MySqlCommand sqlCommand = new ($"SELECT CASE WHEN EXISTS (SELECT SCHEMA_NAME FROM information_schema.SCHEMATA WHERE SCHEMA_NAME = '{databaseName}') THEN 1 ELSE 0 END AS database_exists;", connection);
		using (sqlCommand) {
			int? nullable = (int?) sqlCommand.ExecuteScalar();
			return nullable.HasValue && nullable.Value == 1;
		}
	}
}