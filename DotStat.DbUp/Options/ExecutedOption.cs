﻿using CommandLine;

namespace DotStat.DbUp.Options
{
    [Verb("executed", HelpText = "List already executed scripts, e.g.: DotStat.DbUp.exe executed --connectionString \"Server=.; Database=MyDataDb; Trusted_connection=true\" --dataDb")]
    public class ExecutedOption : BaseOption
    {
        [Option("useMariaDb", Required = false, Default = false, HelpText = "Uses MariaDB configuration for upgrading database")]
        public bool UseMariaDb { get; set; }
    }
}