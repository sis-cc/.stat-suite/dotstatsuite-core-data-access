﻿using System;
using System.IO;
using System.Reflection;
using System.Text;
using DbUp.Builder;
using DbUp.Engine.Output;
using DotStat.DbUp.Options;
using Google.Cloud.Logging.Log4Net;
using log4net;
using log4net.Appender;
using log4net.Config;
using log4net.Core;
using log4net.Layout;
using log4net.Repository;
using log4net.Repository.Hierarchy;
using Microsoft.Data.SqlClient;

namespace DotStat.DbUp
{
    public static class DbUpGoogleLogger
    {
        private static bool _isGoogleLogging;
        private static ILoggerRepository _logRepo;

        public static void ConfigureLog4net(UpgradeOption option)
        {
            _logRepo = LogManager.GetRepository(Assembly.GetExecutingAssembly());
            _isGoogleLogging = !string.IsNullOrEmpty(option.GoogleProjectId);

            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
            var configurationPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "log4net.xml");
            XmlConfigurator.Configure(new FileInfo(configurationPath));

            if (_isGoogleLogging)
            {
                ((Hierarchy)_logRepo).Root.AddAppender(CreateGoogleAppender(option));
            }
        }

        public static void Flush()
        {
            if (_isGoogleLogging)
            {
                ((IFlushable)_logRepo).Flush(10_000);
            }
        }

        static IAppender CreateGoogleAppender(UpgradeOption option)
        {
            var appender = new GoogleStackdriverAppender()
            {
                Name = "Google appender",
                ProjectId = option.GoogleProjectId,
                LogId = option.GoogleLogId ?? "DbUp",
                Threshold = Level.Debug,
                UsePatternWithinCustomLabels = true,
                Layout = new PatternLayout("%logger - %message"),
                DisableResourceTypeDetection = true
            };

            appender.AddCustomLabel(new GoogleStackdriverAppender.Label()
            {
                Key = "exception",
                Value = "%exception"
            });

            appender.AddCustomLabel(new GoogleStackdriverAppender.Label()
            {
                Key = "db",
                Value = new SqlConnectionStringBuilder(option.ConnectionString).InitialCatalog
            });

            if (!option.MappingStoreDb)
            {
                appender.AddCustomLabel(new GoogleStackdriverAppender.Label()
                {
                    Key = "dbVersion",
                    Value = (option.CommonDb ? DatabaseVersion.CommonDbVersion : DatabaseVersion.DataDbVersion).ToString()
                });
            }

            appender.ActivateOptions();

            return appender;
        }
    }

    public static class DbUpGoogleLogExtension
    {
        public static UpgradeEngineBuilder LogWithLog4net(this UpgradeEngineBuilder builder, UpgradeOption option)
        {
            return builder.LogTo(new DbUpGoogleLog());
        }
    }

    internal class DbUpGoogleLog : IUpgradeLog
    {
        static readonly ILog Logger = LogManager.GetLogger(typeof(DbUpGoogleLog));

        public void LogTrace(string format, params object[] args) => Logger.InfoFormat(format, args);

        public void LogDebug(string format, params object[] args) => Logger.DebugFormat(format, args);

        public void LogInformation(string format, params object[] args) => Logger.Info(string.Format(format, args));

        public void LogError(string format, params object[] args) => Logger.Error(string.Format(format, args));

        public void LogError(Exception ex, string format, params object[] args) => Logger.ErrorFormat(string.Format(format, args), ex);

        public void LogWarning(string format, params object[] args) => Logger.Warn(string.Format(format, args));
    }
}