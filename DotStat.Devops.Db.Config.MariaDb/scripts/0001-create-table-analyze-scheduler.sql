SET GLOBAL event_scheduler = ON;

DELIMITER $$

CREATE PROCEDURE IF NOT EXISTS analyze_all_tables_proc()
BEGIN
    DECLARE done INT DEFAULT FALSE;
    DECLARE tbl CHAR(255);
    DECLARE db CHAR(255);
    DECLARE cur CURSOR FOR SELECT table_schema, table_name FROM information_schema.tables WHERE table_schema NOT IN ('information_schema', 'mysql', 'performance_schema', 'sys') AND table_type = 'BASE TABLE';
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

    OPEN cur;

    read_loop: LOOP
        FETCH cur INTO db, tbl;
        IF done THEN
            LEAVE read_loop;
        END IF;
        PREPARE stmt from CONCAT('ANALYZE TABLE `', db, '`.`', tbl, '`');
        EXECUTE stmt;
        DEALLOCATE PREPARE stmt;
    END LOOP;

    CLOSE cur;
END$$

CREATE EVENT IF NOT EXISTS analyze_all_tables
    ON SCHEDULE EVERY 3 DAY
        STARTS (TIMESTAMP(CURRENT_DATE) + INTERVAL 1 DAY + INTERVAL '02:00:00' HOUR_SECOND)
    DO
    BEGIN
        CALL analyze_all_tables_proc();
    END$$

DELIMITER ;
