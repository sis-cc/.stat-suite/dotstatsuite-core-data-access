﻿using CsvHelper.Configuration;

namespace DotStat.MappingStore
{
    public enum DbType {
        SqlServer,
        MariaDb
    }
    public static class DbTypeHelper
    {
        public static DbType GetDbType(string dbTypeStr, bool useDefault = true)
        {

            if (dbTypeStr.Equals(DbType.SqlServer.ToString())) {
                return DbType.SqlServer;
            }else if (dbTypeStr.Equals(DbType.MariaDb.ToString())) {
                return DbType.MariaDb;
            }else if (useDefault) {
                return DbType.SqlServer;
            }

            throw new ConfigurationException($"Configuration error: invalid DbType. value={dbTypeStr}");
        }
        
    }
}
