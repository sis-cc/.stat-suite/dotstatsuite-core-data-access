﻿using DotStat.Common.Exceptions;

namespace DotStat.MappingStore.Exception
{
    public class ExternalDataflowException : DotStatException
    {
        public ExternalDataflowException()
        {
        }

        public ExternalDataflowException(string message) : base(message)
        {
        }

        public ExternalDataflowException(string message, System.Exception innerException) : base(message, innerException)
        {
        }
    }
}
