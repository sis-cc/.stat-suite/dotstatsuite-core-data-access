﻿using DotStat.Common.Exceptions;

namespace DotStat.MappingStore.Exception
{
    public class DuplicateComponentIdException : DotStatException
    {
        public DuplicateComponentIdException()
        {
        }

        public DuplicateComponentIdException(string message) : base(message)
        {
        }

        public DuplicateComponentIdException(string message, System.Exception innerException) : base(message, innerException)
        {
        }
    }
}
