﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using DotStat.Db.Repository;
using DotStat.Domain;

namespace DotStat.Test.Moq
{
    public class TestCodeListRepository: ICodelistRepository
    {
        private Dataflow _df;
        private Dictionary<int, Codelist> _codelists;

        public TestCodeListRepository(Domain.Dataflow dataflow)
        {
            FillIdsFromDisseminationDb(dataflow);
        }

        public Task<string[]> GetDimensionCodesFromDb(int codeListId, CancellationToken cancellationToken)
        {
            Codelist cl;

            if(!_codelists.TryGetValue(codeListId, out cl))
                throw new InvalidOperationException($"Codelist with [{codeListId}] not found");

            var list = new string[cl.Codes.Count + 1];
            Array.Copy(cl.Codes.Select(c=>c.Code).ToArray(), 0, list, 1, list.Length - 1);
            return Task.FromResult(list);
        }

        private void FillIdsFromDisseminationDb(Dataflow dataflow)
        {
            _df                 = dataflow;
            _codelists          = new Dictionary<int, Codelist>();
            // --------------------------------------------

            _df.Dsd.LiveVersion = 'A';
            _df.Dsd.DbId        = 7;
            _df.DbId = 1;

            for (var i = 0; i < dataflow.Dsd.Dimensions.Count; ++i)
            {
                var dimension   = dataflow.Dsd.Dimensions[i];
                var id          = i + 101;
                dimension.DbId  = id;

                if (!dimension.Base.TimeDimension)
                {
                    dimension.Codelist.DbId = id;
                    _codelists[id] = dimension.Codelist;
                }
            }

            for (var i = 0; i < dataflow.Dsd.Attributes.Count; ++i)
            {
                var attr = dataflow.Dsd.Attributes[i];
                var id = i + 201;
                attr.DbId = id;

                if (attr.Base.HasCodedRepresentation())
                {
                    attr.Codelist.DbId = id;
                    _codelists[id] = attr.Codelist;
                }
            }
        }
        
    }
}
