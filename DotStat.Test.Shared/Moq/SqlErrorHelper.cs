﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Reflection;
using Microsoft.Data.SqlClient;


namespace DotStat.Test.Moq
{
    [ExcludeFromCodeCoverage]
    public static class SqlExceptionHelper
    {
        public static SqlException NewSqlException(int errorNumber, string msg)
        {
            SqlError error =CreateError(errorNumber);
            SqlErrorCollection errorCollection = CreateErrorCollection(error);
            SqlException exception = CreateException(errorCollection, msg);

            return exception;
        }

        private static SqlError CreateError(int errorNumber)
        {
            var ctors = typeof(SqlError).GetConstructors(BindingFlags.NonPublic | BindingFlags.Instance);
            var firstSqlErrorCtor = ctors.FirstOrDefault(ctor => ctor.GetParameters().Count() == 8); 
            SqlError error = firstSqlErrorCtor.Invoke(
                new object[]
                {
                errorNumber,
                new byte(),
                new byte(),
                string.Empty,
                string.Empty,
                string.Empty,
                new int(),
                new Exception()
                }) as SqlError;

            return error;
        }

        private static SqlErrorCollection CreateErrorCollection(SqlError error)
        {
            var sqlErrorCollectionCtor = typeof(SqlErrorCollection).GetConstructors(BindingFlags.NonPublic | BindingFlags.Instance)[0];
            SqlErrorCollection errorCollection = sqlErrorCollectionCtor.Invoke(new object[] { }) as SqlErrorCollection;
            typeof(SqlErrorCollection).GetMethod("Add", BindingFlags.NonPublic | BindingFlags.Instance).Invoke(errorCollection, new object[] { error });

            return errorCollection;
        }

        private static SqlException CreateException(SqlErrorCollection errorCollection, string msg)
        {
            var ctor = typeof(SqlException).GetConstructors(BindingFlags.NonPublic | BindingFlags.Instance)[0];
            SqlException sqlException = ctor.Invoke(new object[]{ msg, errorCollection, null, Guid.NewGuid()}) as SqlException;

            return sqlException;
        }
    }
}
