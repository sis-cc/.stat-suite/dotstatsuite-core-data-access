FROM mcr.microsoft.com/dotnet/sdk:6.0-alpine AS build-env
WORKDIR /app

# Copy csproj and restore as distinct layers
COPY DotStat.DbUp/*.csproj ./DotStat.DbUp/
COPY DotStat.DbUp.MsSql/*.csproj ./DotStat.DbUp.MsSql/
COPY DotStat.DbUp.MariaDb/*.csproj ./DotStat.DbUp.MariaDb/
COPY nuget.config global.json version.json Directory.Build.props ./
RUN dotnet restore DotStat.DbUp

# Copy everything else and build
COPY DotStat.DbUp ./DotStat.DbUp/
COPY DotStat.DbUp.MsSql ./DotStat.DbUp.MsSql/
COPY DotStat.DbUp.MariaDb ./DotStat.DbUp.MariaDb/
WORKDIR /app/DotStat.DbUp
RUN dotnet publish -c Release -o out -r linux-musl-x64

# Build runtime image
FROM mcr.microsoft.com/dotnet/runtime:6.0-alpine AS runtime-env
WORKDIR /app
COPY --from=build-env /app/DotStat.DbUp/out/ .

#install extra libs (icu-libs, gcompat) needed in alpine based docker image 
RUN apk update && apk add --no-cache icu-libs
ENV DOTNET_SYSTEM_GLOBALIZATION_INVARIANT=false

# Single parameter
ENV EXECUTION_PARAMETERS=help

ENTRYPOINT dotnet DotStat.DbUp.dll $EXECUTION_PARAMETERS