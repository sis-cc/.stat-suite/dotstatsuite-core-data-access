CREATE OR REPLACE PROCEDURE `CREATE_DF_VIEWS` (
    IN `DSD_ID` INT,
    IN `table_version` CHAR(1)
)
proc_label: BEGIN
    -- Declarations
    DECLARE `sdmxId` TEXT;
    DECLARE `U_DSD_VERSION_ID` VARCHAR(20);
    DECLARE `NewLineChar` CHAR(2);
    DECLARE `isLiveSet` BIT;
    DECLARE `isPITset` BIT;            
    DECLARE `HasTimeDim` BIT DEFAULT 0;
    DECLARE `msg` TEXT;
    DECLARE `sql` TEXT;
    DECLARE `SelectPart` TEXT;
    DECLARE `FromPart` TEXT;
    DECLARE `TempString` TEXT;
    DECLARE `TempMessage` TEXT;
    DECLARE `DF_ID` INT;

    SET `NewLineChar` = CHAR(10);

    -- Temporary table definitions
    DROP TEMPORARY TABLE IF EXISTS `DSD_Components`;
    CREATE TEMPORARY TABLE `DSD_Components` (
        `COMP_ID` INT NOT NULL,
        `ID` VARCHAR(50) NOT NULL, 
        `TYPE` VARCHAR(50) NOT NULL,    
        `CL_ID` INT, 
        `ATT_ASS_LEVEL` VARCHAR(50),
        `ATT_STATUS` VARCHAR(11)
    );

    DROP TEMPORARY TABLE IF EXISTS `DF_IDS`;
    CREATE TEMPORARY TABLE `DF_IDS` (
        `ART_ID` INT PRIMARY KEY, 
        `SDMX_ID` TEXT
    );

    -- Logic to populate U_DSD_VERSION_ID, sdmxId, isLiveSet, isPITset from `management_ARTEFACT`
    SELECT 
        CONCAT(CAST(`DSD_ID` AS CHAR), '_', `table_version`),
        CONCAT(`AGENCY`, ':', `ID`, '(', CAST(`VERSION_1` AS CHAR), '.', CAST(`VERSION_2` AS CHAR), IFNULL(CONCAT('.', CAST(`VERSION_3` AS CHAR)), ''), ')'),
        IF(`DSD_LIVE_VERSION` IS NULL, 0, 1),
        IF(`DSD_PIT_VERSION` IS NULL, 0, 1)
    INTO 
        `U_DSD_VERSION_ID`,
        `sdmxId`,
        `isLiveSet`,
        `isPITset`
    FROM `management_ARTEFACT`
    WHERE `type` = 'DSD' AND `ART_ID` = `DSD_ID`;

    -- Check if fact table exists
    IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = DATABASE() AND TABLE_NAME = CONCAT('data_FACT_', `U_DSD_VERSION_ID`)) THEN
        -- SKIP when no fact table has been created and no DSD_LIVE_VERSION, nor DSD_PIT_VERSION has been set
        IF (`isLiveSet` = 0 AND `isPITset` = 0) THEN
            LEAVE proc_label;
        END IF;

        SET `msg` = CONCAT('The following error was found while trying to recreate the DF View for the DSD [', `sdmxId`, '] (The creation of the view will be skipped): The table `data_FACT_', `U_DSD_VERSION_ID`, '` does not exist in the database, but a value has been set for the fields `DSD_LIVE_VERSION` or `DSD_PIT_VERSION in the `management_ARTEFACT` for the given DSD.');

        SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = `msg`;
    END IF;

    -- Check if DSD has TIME dimension
    SET `HasTimeDim` = (SELECT IF(COUNT(*) > 0, 1, 0) FROM `management_COMPONENT` WHERE `TYPE` = 'TimeDimension' AND `DSD_ID` = `DSD_ID`);

    -- Get components
    INSERT INTO `DSD_Components`
    SELECT `c`.`COMP_ID`, `c`.`ID`, `c`.`TYPE`, `c`.`CL_ID`, `c`.`ATT_ASS_LEVEL`, `c`.`ATT_STATUS`
    FROM `management_COMPONENT` AS `c`
    WHERE `c`.`DSD_ID` = `DSD_ID`;

    -- Build SELECT statement
    SET `SelectPart` = CONCAT('SELECT `SID`', IFNULL((SELECT GROUP_CONCAT(
        CASE 
            -- Non-coded dataset level attributes
            WHEN `C`.`CL_ID` IS NULL AND `C`.`TYPE` = 'Attribute' AND `C`.`ATT_ASS_LEVEL` = 'DataSet' THEN
                CONCAT(', ADF.`COMP_', CAST(`C`.`COMP_ID` AS CHAR), '` AS `', `C`.`ID`, '`')
            -- Coded dataset level attributes
            WHEN `C`.`CL_ID` IS NOT NULL AND `C`.`TYPE` = 'Attribute' AND `C`.`ATT_ASS_LEVEL` = 'DataSet' THEN
                CONCAT(', `CL_', `C`.`ID`, '`.`ID` AS `', `C`.`ID`, '`')
            ELSE
                CONCAT(', `', `C`.`ID`, '`')
            END
        SEPARATOR ''
    ) FROM `DSD_Components` AS `C`), ''));

    SET `SelectPart` = CONCAT(`SelectPart`, CASE `HasTimeDim`
        WHEN 1 THEN CONCAT(', `PERIOD_START`, `PERIOD_END`', `NewLineChar`)
        ELSE ''
    END, ', `VI`.`LAST_UPDATED`', `NewLineChar`);

	-- Start Build From Statement
	SET `FromPart` = CONCAT('FROM `data_VI_CurrentDataDsd_', `U_DSD_VERSION_ID`, '` AS `VI`', `NewLineChar`);
	
	IF EXISTS (SELECT 1 FROM `DSD_Components` WHERE `TYPE` = 'Attribute' AND `ATT_ASS_LEVEL` = 'DataSet') THEN
	    SET `FromPart` = CONCAT(`FromPart`, 'FULL JOIN `data_ATTR_', `U_DSD_VERSION_ID`, '_DF` ADF ON DF_ID = ', `DF_ID`, `NewLineChar`);
	    
	    SET TempString = NULL;
	    SELECT GROUP_CONCAT('LEFT JOIN `management_CL_', CAST(CL_ID AS CHAR), '` AS `CL_', `ID`, '` ON `ADF`.`DF_ID`=', `DF_ID`, ' AND ADF.COMP_', CAST(COMP_ID AS CHAR), ' = `CL_', `ID`, '`.`ITEM_ID`', `NewLineChar` ORDER BY `ID`)
	    INTO TempString
	    FROM `DSD_Components` 
	    WHERE `CL_ID` IS NOT NULL AND `TYPE` = 'Attribute' AND `ATT_ASS_LEVEL` = 'DataSet';
	
	    SET `FromPart` = IFNULL(CONCAT(`FromPart`, TempString), `FromPart`);
	END IF;
	
	
	INSERT INTO `DF_IDS` 
	SELECT `ART_ID`, CONCAT(`AGENCY`, ':', `ID`, '(', CAST(`VERSION_1` AS CHAR), '.', CAST(`VERSION_2` AS CHAR), IFNULL(CONCAT('.', CAST(`VERSION_3` AS CHAR)), ''), ')')
	FROM `management_ARTEFACT` WHERE `TYPE`='DF' AND `DF_DSD_ID`=`DSD_ID`;
	
	-- Loop through DF_IDS
	WHILE (SELECT COUNT(*) FROM `DF_IDS`) > 0 DO
	    SELECT `ART_ID`, `SDMX_ID` INTO `DF_ID`, `sdmxId` FROM `DF_IDS` LIMIT 1;
	    DELETE FROM `DF_IDS` WHERE `ART_ID` = `DF_ID`;
	    
	    SET TempMessage = CONCAT('Recreating VIEW(', `table_version`, ') for DF: ', `sdmxId`, ', ID:', CAST(`DF_ID` AS CHAR));
	    SELECT TempMessage; -- This is a replacement for PRINT
	    
	    SET `sql` = REPLACE(
	        CONCAT('CREATE OR REPLACE VIEW `data_VI_CurrentDataDataFlow_', `DF_ID`, '_', `table_version`, '` AS ', `NewLineChar`, `SelectPart`, `FromPart`),
	        '{DF_ID}', CAST(`DF_ID` AS CHAR)
	    );
	    
	    -- In MariaDB, you can use PREPARE and EXECUTE for dynamic SQL
	    PREPARE stmt FROM `sql`;
	    EXECUTE stmt;
	    DEALLOCATE PREPARE stmt;
	    
	END WHILE;


    -- Cleanup temporary tables at the end
    DROP TEMPORARY TABLE IF EXISTS `DSD_Components`;
    DROP TEMPORARY TABLE IF EXISTS `DF_IDS`;

END;