BEGIN NOT ATOMIC
IF (SELECT count(1) FROM information_schema.tables WHERE table_schema = '$dbName$' AND table_name = 'MANAGEMENT_ARTEFACT') = 0
THEN
    CREATE TABLE MANAGEMENT_ARTEFACT
    (
        ART_ID                   int         NOT NULL AUTO_INCREMENT,
        SQL_ART_ID               int         NULL,
        TYPE                     varchar(50) NOT NULL,
        ID                       varchar(50) NOT NULL,
        AGENCY                   varchar(50) NOT NULL,
        VERSION_1                int         NOT NULL DEFAULT 0,
        VERSION_2                int         NOT NULL DEFAULT 0,
        VERSION_3                int         NULL,
        DSD_LIVE_VERSION         char(1)     NULL,
        DSD_PIT_VERSION          char(1)     NULL,
        DSD_PIT_RELEASE_DATE     datetime(6)    NULL,
        DSD_PIT_RESTORATION_DATE datetime(6)    NULL,
        DSD_DATE_CREATED         datetime(6)    NULL,
        DSD_DATE_DELETED         datetime(6)    NULL,
        DSD_CUD_MODE             bit         NULL,
        DSD_TRANSFER_MODE        bit         NULL,
        DSD_VALIDATION           bit         NULL,
        DSD_MAX_TEXT_ATTR_LENGTH int         NULL,
        DF_DSD_ID                int         NULL,
        DF_WHERE_CLAUSE          TEXT        NULL,
        LAST_UPDATED             datetime(6)    NULL,
        MSD_ID                   int         NULL,
        DATA_COMPRESSION         varchar(30) NULL,
        KEEP_HISTORY             bit         NOT NULL DEFAULT 0,
        PRIMARY KEY PK_ARTEFACT_backup (ART_ID ASC),
        UNIQUE INDEX UK_ARTEFACT_SDMX_AGENCY_ID_VERSION (
                                                         TYPE ASC,
                                                         ID ASC,
                                                         AGENCY ASC,
                                                         VERSION_1 ASC,
                                                         VERSION_2 ASC,
                                                         VERSION_3 ASC
            )
    );
END IF;
END;



BEGIN NOT ATOMIC
    IF (SELECT count(1) FROM information_schema.tables WHERE table_schema = '$dbName$' AND table_name = 'MANAGEMENT_COMPONENT') = 0
    THEN
        CREATE TABLE MANAGEMENT_COMPONENT
        (
            COMP_ID       int         NOT NULL AUTO_INCREMENT,
            ID            varchar(50) NOT NULL,
            `TYPE`        varchar(50) NOT NULL,
            DSD_ID        int         NOT NULL,
            CL_ID         int         NULL,
            ATT_ASS_LEVEL varchar(15) NULL,
            ATT_STATUS    varchar(11) NULL,
            ATT_GROUP_ID  varchar(50) NULL,
            ENUM_ID       bigint      NULL,
            MIN_LENGTH       int      NULL,
            MAX_LENGTH       int      NULL,
            PATTERN         nvarchar(4000),
            MIN_VALUE       int      NULL,
            MAX_VALUE       int      NULL,
            PRIMARY KEY PK_COMPONENT (COMP_ID ASC)
        );
    END IF;
END;

BEGIN NOT ATOMIC
    IF (SELECT count(1) FROM information_schema.tables WHERE table_schema = '$dbName$' AND table_name = 'MANAGEMENT_ATTR_DIM_SET') = 0
    THEN
        CREATE TABLE MANAGEMENT_ATTR_DIM_SET
        (
            ATTR_ID int NOT NULL,
            DIM_ID  int NOT NULL,
            PRIMARY KEY PK_ATTR_DIM_SET (ATTR_ID ASC, DIM_ID ASC)
        );
    END IF;
END;

BEGIN NOT ATOMIC
    IF (SELECT count(1) FROM information_schema.tables WHERE table_schema = '$dbName$' AND table_name = 'MANAGEMENT_DSD_TRANSACTION') = 0
    THEN
        CREATE TABLE MANAGEMENT_DSD_TRANSACTION
        (
            TRANSACTION_ID           int           NOT NULL,
            ART_ID                   int           NULL,
            TABLE_VERSION            char(1)       NULL,
            EXECUTION_START          datetime(6)      NULL,
            EXECUTION_END            datetime(6)      NULL,
            SUCCESSFUL               bit           NULL,
            USER_EMAIL               nvarchar(350) NULL,
            ARTEFACT_FULL_ID         nvarchar(255) NOT NULL DEFAULT (''),
            SOURCE_DATASPACE         nvarchar(200) NULL,
            DATA_SOURCE              TEXT          NULL,
            QUEUED_DATETIME          datetime(6)      NOT NULL DEFAULT (CURRENT_DATE()),
            STATUS                   VARCHAR(250)  NULL,
            TYPE                     VARCHAR(250)  NULL,
            REQUESTED_TARGET_VERSION VARCHAR(250)  NULL,
            FINAL_TARGET_VERSION     VARCHAR(250)  NULL,
            BLOCK_ALL_TRANSACTIONS   BIT           NOT NULL DEFAULT (0),
            LAST_UPDATED             datetime(6)      NOT NULL DEFAULT (CURRENT_DATE()),
            ART_CHILD_ID             INT           NULL,
            SERVICE_ID               VARCHAR(20)  NULL,
            PRIMARY KEY PK_DSD_TRANSACTION (TRANSACTION_ID ASC),
            INDEX IX_DSD_TRANSACTION (ART_ID ASC, ART_CHILD_ID, SUCCESSFUL)
            #TODO: WHERE ([SUCCESSFUL] IS NULL AND [ART_ID] IS NOT NULL);
            # Conditional indexing not supported in mariaDb/mySql
        );
    END IF;
END;

/*BEGIN NOT ATOMIC
    IF (SELECT count(1) FROM information_schema.tables WHERE table_schema = '$dbName$' AND table_name = 'MANAGEMENT_DSD_TRANSACTION_LOG') = 0
    THEN
        CREATE TABLE MANAGEMENT_DSD_TRANSACTION_LOG
        (
            TRANSACTION_ID            int      NOT NULL,
            ART_ID                    int      NOT NULL,
            DSD_TARGET_VERSION        char(1)  NOT NULL,
            DSD_DATE_CREATED          datetime(6) NOT NULL DEFAULT (CURRENT_DATE()),
            READY_FOR_VALIDATION_DATE datetime(6) NULL,
            VALIDATION_DATE           datetime(6) NULL,
            RELEASE_DATE              datetime(6) NULL,
            SUCCESSFUL                bit      NULL,
            PRIMARY KEY PK_DSD_TRANSACTION_LOG_bk (TRANSACTION_ID ASC)
        );
    END IF;
END;*/

BEGIN NOT ATOMIC
    IF (SELECT count(1) FROM information_schema.tables WHERE table_schema = '$dbName$' AND table_name = 'MANAGEMENT_LOGS') = 0
    THEN
        CREATE TABLE MANAGEMENT_LOGS
        (
            TRANSACTION_ID int           NULL,
            `DATE`         datetime(6)      NULL,
            `LEVEL`        nvarchar(50)  NULL,
            SERVER         nvarchar(50)  NULL,
            LOGGER         nvarchar(255) NULL,
            MESSAGE        TEXT          NULL,
            `EXCEPTION`    TEXT          NULL,
            THREAD         nvarchar(255) NULL,
            APPLICATION    nvarchar(255) NULL,
            URLREFERRER    TEXT          NULL,
            HTTPMETHOD     TEXT          NULL,
            REQUESTPATH    TEXT          NULL,
            QUERYSTRING    TEXT          NULL,
            USEREMAIL      nvarchar(350) NULL
        );
    END IF;
END;

/*BEGIN NOT ATOMIC
    IF (SELECT count(1) FROM information_schema.tables WHERE table_schema = '$dbName$' AND table_name = 'MANAGEMENT_CL_TIME') = 0
    THEN
        CREATE TABLE MANAGEMENT_CL_TIME
        (
            ITEM_ID        int AUTO_INCREMENT NOT NULL,
            ITEM_PARENT_ID int                NOT NULL,
            ID             varchar(50)        NOT NULL,
            START_PERIOD   datetime(6)           NOT NULL,
            END_PERIOD     datetime(6)           NOT NULL,
            PRIMARY KEY PK_CL_TIME (ITEM_ID ASC)
        );
    END IF;
END;*/

BEGIN
    NOT ATOMIC
    IF (SELECT count(1) FROM information_schema.tables WHERE table_schema = '$dbName$' AND table_name = 'MANAGEMENT_ENUMERATIONS') = 0
    THEN
        CREATE TABLE MANAGEMENT_ENUMERATIONS
        (
            ENUM_ID             bigint      NOT NULL,
            ENUM_NAME           varchar(30) NULL,
            ENUM_VALUE          varchar(50) NULL,
            ENUM_VALUE_DATATYPE varchar(20) NULL,
            PRIMARY KEY PK_ENUMERATIONS (ENUM_ID ASC)
        );
    END IF;
END;

BEGIN NOT ATOMIC
    IF (SELECT count(1) FROM information_schema.tables WHERE table_schema = '$dbName$' AND table_name = 'MANAGEMENT_DF_OUTPUT_TIMESTAMP') = 0
    THEN
        CREATE TABLE MANAGEMENT_DF_OUTPUT_TIMESTAMP
        (
            ART_ID      int          NOT NULL,
            FILETYPE    varchar(20)  NOT NULL,
            LANG        char(2)      NOT NULL,
            FILENAME    varchar(100) NOT NULL,
            `TIMESTAMP` datetime(6)     NOT NULL,
            PRIMARY KEY PK_DF_OUTPUT_TIMESTAMP (
                                                ART_ID ASC,
                                                FileType ASC,
                                                Lang ASC
                )
        );
    END IF;
END;

BEGIN NOT ATOMIC
    IF (SELECT count(1) FROM information_schema.tables WHERE table_schema = '$dbName$' AND table_name = 'MANAGEMENT_METADATA_ATTRIBUTE') = 0
    THEN
        CREATE TABLE MANAGEMENT_METADATA_ATTRIBUTE
        (
            MTD_ATTR_ID int AUTO_INCREMENT NOT NULL,
            ID varchar(4000) NOT NULL,
            MSD_ID int NOT NULL,
            CL_ID int NULL,
            PARENT int NULL,
            MIN_OCCURS int NULL,
            MAX_OCCURS int NULL,
            IS_REPRESENTATIONAL smallint NULL,
            ENUM_ID bigint NULL,
            PRIMARY KEY PK_METADATA_ATTRIBUTE (MTD_ATTR_ID ASC),
            FOREIGN KEY FK_MD_ATTR_MD_ATTR (PARENT) REFERENCES MANAGEMENT_METADATA_ATTRIBUTE(MTD_ATTR_ID)
        );
    END IF;
END;

BEGIN NOT ATOMIC
    IF (SELECT count(1) FROM information_schema.tables WHERE table_schema = '$dbName$' AND table_name = 'DB_VERSION') = 0
    THEN
        CREATE TABLE DB_VERSION
        (
            SYS_ID    bigint AUTO_INCREMENT NOT NULL,
            `VERSION` varchar(250)          NULL,
            PRIMARY KEY PK_DB_VERSION (SYS_ID ASC)
        );
        INSERT INTO DB_VERSION (`VERSION`) VALUES ('1.0');
    END IF;
END;

BEGIN NOT ATOMIC
    CREATE SEQUENCE IF NOT EXISTS MANAGEMENT_TRANSFER_SEQUENCE
        START WITH 0
        INCREMENT BY 1
        MINVALUE = -2147483648
        MAXVALUE = 2147483647
        CACHE = 1000;
END;
