CREATE OR REPLACE PROCEDURE CREATE_DSD_VIEW(
    IN DSD_ID INT,
    IN table_version CHAR(1)
)
main_proc: BEGIN
    -- Variable Declarations
    DECLARE sdmxId VARCHAR(1000);
    DECLARE U_DSD_VERSION_ID VARCHAR(20);
    DECLARE NewLineChar CHAR(2) DEFAULT CHAR(10);
    DECLARE isLiveSet TINYINT DEFAULT 0;
    DECLARE isPITset TINYINT DEFAULT 0;
    DECLARE HasTimeDim TINYINT DEFAULT 0;
    DECLARE TimeDimId INT DEFAULT 0;
    DECLARE msg TEXT;
    DECLARE sqlText TEXT;
    DECLARE SelectPart TEXT;
    DECLARE FromPart TEXT;
    DECLARE WherePart TEXT;

    -- Temporary Table Equivalent in MariaDB
    DROP TEMPORARY TABLE IF EXISTS DSD_Components;
    CREATE TEMPORARY TABLE DSD_Components (
                                              COMP_ID INT NOT NULL,
                                              ID VARCHAR(50) NOT NULL,
                                              TYPE VARCHAR(50) NOT NULL,
                                              CL_ID INT,
                                              ATT_ASS_LEVEL VARCHAR(50),
                                              ATT_STATUS VARCHAR(11),
                                              IS_TIME_DIM_REFERENCED TINYINT NOT NULL
    );

    -- Using the SELECT INTO structure for variable assignment
    SELECT
        CONCAT(CAST(DSD_ID AS CHAR), '_', table_version),
        CONCAT(AGENCY, ':', ID, '(', CAST(VERSION_1 AS CHAR), '.', CAST(VERSION_2 AS CHAR), IF(VERSION_3 IS NULL, '', CONCAT('.', CAST(VERSION_3 AS CHAR))), ')'),
        IF(DSD_LIVE_VERSION IS NULL, 0, 1),
        IF(DSD_PIT_VERSION IS NULL, 0, 1)
    INTO
        U_DSD_VERSION_ID,
        sdmxId,
        isLiveSet,
        isPITset
    FROM `management_ARTEFACT`
    WHERE type = 'DSD' AND ART_ID = DSD_ID;

    -- Checking if fact table exists
    IF (NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = DATABASE() AND TABLE_NAME = CONCAT('data_FACT_', U_DSD_VERSION_ID))) THEN
        IF isLiveSet = 0 AND isPITset = 0 THEN
            LEAVE main_proc;
        END IF;

        SET msg = CONCAT('The following error was found while trying to recreate the DSD Views for the DSD [', sdmxId, '] (The creation of the view will be skipped): The table `',DATABASE(),'`.`data_FACT_', U_DSD_VERSION_ID, '` does not exist in the database, but a value has been set for the fields [DSD_LIVE_VERSION] or [DSD_PIT_VERSION] in the [management].[ARTEFACT] for the given DSD.');

        SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = msg;
    END IF;

    -- Checking if the DSD has a TIME dimension
    SELECT 1, COMP_ID INTO HasTimeDim, TimeDimId
    FROM `management_COMPONENT`
    WHERE TYPE ='TimeDimension' AND DSD_ID = DSD_ID;

    -- Getting components
    INSERT INTO DSD_Components
    SELECT c.COMP_ID, c.ID, c.TYPE, c.CL_ID, c.ATT_ASS_LEVEL, c.ATT_STATUS,
           IF(RT.ATTR_ID IS NULL, 0, 1)
    FROM `management_COMPONENT` c
             LEFT JOIN (
        SELECT ATTR_ID
        FROM management_attr_dim_set
        WHERE DIM_ID = TimeDimId
    ) AS RT ON C.COMP_ID = RT.ATTR_ID
    WHERE c.DSD_ID = DSD_ID AND TYPE IN ('Dimension', 'Attribute')
      AND IFNULL(c.ATT_ASS_LEVEL, '') <> 'DataSet';

    -- BUILD SELECT STATEMENT
    SET SelectPart = CONCAT('SELECT FI.SID,FA.VALUE AS OBS_VALUE',
                            CASE HasTimeDim
                                WHEN 1 THEN ',FA.PERIOD_SDMX AS TIME_PERIOD,FA.PERIOD_START,FA.PERIOD_END'
                                ELSE ''
                                END
        );

    SET SelectPart = CONCAT(SelectPart,
                            IFNULL(
                                    (SELECT GROUP_CONCAT(
                                                    (SELECT CASE
                                                                -- coded dimensions and coded attributes
                                                                WHEN c.CL_ID IS NOT NULL
                                                                    THEN CONCAT(',`CL_', c.ID, '`.ID AS `', c.ID, '`', NewLineChar)
                                                                -- Non-coded dimensions
                                                                WHEN c.CL_ID IS NULL AND c.TYPE = 'Dimension'
                                                                    THEN CONCAT(',FI.`DIM_', CAST(c.COMP_ID AS CHAR), '` AS `', c.ID, '`')
                                                                -- Non-coded attributes at observation level
                                                                WHEN c.CL_ID IS NULL AND c.TYPE = 'Attribute' AND
                                                                     c.ATT_ASS_LEVEL = 'Observation'
                                                                    THEN CONCAT(',FA.`COMP_', CAST(c.COMP_ID AS CHAR), '` AS `', c.ID, '`')
                                                                -- Non-coded attributes at referencing time dimension
                                                                WHEN c.CL_ID IS NULL AND c.TYPE = 'Attribute' AND c.IS_TIME_DIM_REFERENCED = 1
                                                                    THEN CONCAT(',FA.`COMP_', CAST(c.COMP_ID AS CHAR), '` AS `', c.ID, '`')
                                                                WHEN C.TYPE = 'Attribute' AND
                                                                     (c.ATT_ASS_LEVEL IN ('DimensionGroup', 'Group'))
                                                                    THEN CONCAT(',ATTR.`COMP_', CAST(c.COMP_ID AS CHAR), '` AS `', c.ID, '`')
                                                                END)
                                                    SEPARATOR ''
                                                )
                                     FROM DSD_Components c)
                                ,'')
                            );
    
    SET SelectPart = CONCAT(SelectPart, ',FA.LAST_UPDATED');

    --  BUILD WHERE STATEMENT
    SET WherePart = CONCAT('WHERE EXISTS (SELECT 1 FROM `data_FACT_', U_DSD_VERSION_ID, '` AS FA WHERE FI.SID = FA.SID)');

    -- BUILD FROM STATEMENT
    SET FromPart = CONCAT('FROM `data_FILT_', CAST(DSD_ID AS CHAR),'` FI', NewLineChar, 
        ' LEFT JOIN `data_FACT_', U_DSD_VERSION_ID, '` FA ON FI.SID=FA.SID', NewLineChar);
    
    IF EXISTS (SELECT 1 FROM DSD_Components WHERE ATT_ASS_LEVEL IN ('DimensionGroup','Group')) THEN
        SET FromPart = CONCAT(FromPart, 'LEFT JOIN `data_ATTR_', U_DSD_VERSION_ID, '` AS ATTR ON FI.SID=ATTR.SID', NewLineChar);
        SET WherePart = CONCAT(WherePart, ' OR EXISTS (SELECT 1 FROM `data_ATTR_', U_DSD_VERSION_ID,'` AS ATTR WHERE FI.SID=ATTR.SID)' );
    END IF;

    -- START JOIN  STATEMENT
    SET FromPart = CONCAT(FromPart,
                          IFNULL(
                                  (SELECT GROUP_CONCAT(
                                          (SELECT
                                               CASE
                                                   -- coded dimensions
                                                   WHEN c.CL_ID IS NOT NULL AND c.TYPE = 'Dimension'
                                                       THEN CONCAT('LEFT JOIN `management_CL_', CAST(c.CL_ID AS CHAR), '` AS `CL_', c.ID, '` ON FI.`DIM_', CAST(c.COMP_ID AS CHAR), '` = `CL_', c.ID, '`.ITEM_ID', NewLineChar)
                                                    -- coded attributes at observation level
                                                   WHEN c.CL_ID IS NOT NULL AND c.TYPE = 'Attribute' AND c.ATT_ASS_LEVEL = 'Observation'
                                                       THEN CONCAT('LEFT JOIN `management_CL_', CAST(c.CL_ID AS CHAR), '` AS `CL_', c.ID, '` ON FA.`COMP_', CAST(c.COMP_ID AS CHAR), '` = `CL_', c.ID, '`.ITEM_ID', NewLineChar)
                                                    -- coded attributes at group level
                                                   WHEN c.CL_ID IS NOT NULL AND c.ATT_ASS_LEVEL IN ('DimensionGroup','Group')
                                                       THEN CONCAT('LEFT JOIN `management_CL_', CAST(c.CL_ID AS CHAR), '` AS `CL_', c.ID, '` ON ATTR.`COMP_', CAST(c.COMP_ID AS CHAR), '` = `CL_', c.ID, '`.ITEM_ID', NewLineChar)
                                                   END
                                          )
                                          SEPARATOR ''
                                      )
                                   FROM DSD_Components c
                                 )
                              ,'')
        );
    
    
    SET sqlText = CONCAT('CREATE OR REPLACE VIEW `data_VI_CurrentDataDsd_' , U_DSD_VERSION_ID , '` AS'  , NewLineChar
        , SelectPart , NewLineChar
        , FromPart , NewLineChar
        , WherePart, ';');

    PREPARE stmt FROM sqlText;
    EXECUTE stmt;
    DEALLOCATE PREPARE stmt;

    SET msg = CONCAT('Recreating DSD VIEW(', table_version, ') for DF: ', sdmxId, ', ID:' , CAST(DSD_ID AS CHAR));
    SELECT msg;

    DROP TEMPORARY TABLE IF EXISTS DSD_Components;
END;
