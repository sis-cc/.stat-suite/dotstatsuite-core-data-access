UPDATE MANAGEMENT_ARTEFACT
SET DSD_MAX_TEXT_ATTR_LENGTH =
        (SELECT MAX(character_maximum_length) / 2
         FROM INFORMATION_SCHEMA.COLUMNS
         WHERE (table_name like CONCAT('FACT_', CAST(ART_ID AS VARCHAR(30))) OR table_name like CONCAT('ATTR_', CAST(ART_ID AS VARCHAR(30))))
           AND column_name like 'COMP_%'
           AND data_type = 'nvarchar')
WHERE TYPE = 'DSD'
  AND DSD_MAX_TEXT_ATTR_LENGTH IS NULL;