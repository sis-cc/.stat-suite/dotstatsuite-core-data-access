BEGIN NOT ATOMIC
/*
    DECLARE YearMin int DEFAULT 1900;
    DECLARE YearMax int DEFAULT 2080;
    DECLARE CurrentYear int;
    DECLARE CurrentYearItemId int;

    DECLARE YearStart datetime;
    DECLARE YearEnd datetime;
    DECLARE YearId varchar(50);

    DECLARE CurrentMonth int;
    DECLARE CurrentSemesterItemId int;
    DECLARE CurrentQuarterItemId int;
    DECLARE CurrentMonthItemId int;

    DECLARE DateTimeFormat varchar(20);

    DECLARE Semester int;
    DECLARE DateStr varchar(100);
    DECLARE SemesterStart datetime;
    DECLARE SemesterEnd datetime;
    DECLARE SemesterId varchar(50);

    DECLARE Q int;
    DECLARE QStr varchar(100);
    DECLARE QuarterStart datetime;
    DECLARE QuarterEnd datetime;
    DECLARE QuarterId varchar(50);

    DECLARE MonthStr varchar(100);
    DECLARE MonthStart datetime;
    DECLARE MonthEnd datetime;
    DECLARE MonthId varchar(50);

    SET CurrentYear = YearMin;
    TRUNCATE TABLE datadb.CL_TIME;
    
    WHILE CurrentYear <= YearMax DO
        BEGIN NOT ATOMIC
            SET YearStart = MAKEDATE(CurrentYear, 1);
            SET YearEnd = DATE_ADD(CAST(MAKEDATE(CurrentYear + 1, 1) as DATETIME), INTERVAL -2 MICROSECOND);
            SET YearId = CAST(CurrentYear AS VARCHAR(50));

            SET CurrentMonth = 1;
            SET CurrentSemesterItemId = -1;
            SET CurrentQuarterItemId = -1;

            SET DateTimeFormat = '%Y,%m,%d';
    
            INSERT INTO MANAGEMENT_CL_TIME (ITEM_PARENT_ID, ID, START_PERIOD, END_PERIOD)
                VALUES (LAST_INSERT_ID(), YearId, YearStart, YearEnd);
    
            SET CurrentYearItemId = LAST_INSERT_ID();
    
            WHILE CurrentMonth <= 12 DO
                BEGIN NOT ATOMIC
                    IF (CurrentMonth % 6 = 1)
                    THEN
                        SET Semester = CurrentMonth / 6 + 1;
                        SET DateStr = CONCAT(CAST(CurrentYear AS varchar(10)), ',', CAST((Semester - 1) * 6 + 1 AS varchar(10)), ',', '1');
                        SET SemesterStart = CAST(STR_TO_DATE(DateStr, DateTimeFormat) AS DATETIME);
                        SET SemesterEnd = DATE_ADD(DATE_ADD(SemesterStart, INTERVAL 6 MONTH), INTERVAL -2 MICROSECOND);
                        SET SemesterId = CONCAT(CAST(CurrentYear as VARCHAR(10)), '-B', CAST(Semester AS VARCHAR(10)));
        
                        INSERT INTO MANAGEMENT_CL_TIME (ITEM_PARENT_ID, ID, START_PERIOD, END_PERIOD)
                            VALUES (CurrentYearItemId, SemesterId, SemesterStart, SemesterEnd);
        
                        SET CurrentSemesterItemId = LAST_INSERT_ID();
        
                    END IF;
                END;
    
                BEGIN NOT ATOMIC
                    IF (CurrentMonth % 3 = 1)
                    THEN
                        SET Q = CurrentMonth / 3 + 1;
                        SET QStr = CONCAT(CAST(CurrentYear AS varchar(10)), ',', CAST((Q - 1) * 3 + 1 AS varchar(10)), ',','1');
                        SET QuarterStart = CAST(STR_TO_DATE(QStr, DateTimeFormat) AS DATETIME);
                        SET QuarterEnd = DATE_ADD(DATE_ADD(QuarterStart, INTERVAL 3 MONTH), INTERVAL -2 MICROSECOND);
                        SET QuarterId = CONCAT(CAST(CurrentYear as VARCHAR(10)), '-Q', CAST(Q AS VARCHAR(10)));
        
                        INSERT INTO MANAGEMENT_CL_TIME (ITEM_PARENT_ID, ID, START_PERIOD, END_PERIOD)
                            VALUES (CurrentSemesterItemId, QuarterId, QuarterStart, QuarterEnd);
        
                        SET CurrentQuarterItemId = LAST_INSERT_ID();
        
                    END IF;
                END;
    
                BEGIN NOT ATOMIC
                    SET MonthStr = CONCAT(CAST(CurrentYear AS varchar(10)), ',', CAST(CurrentMonth AS varchar(10)), ',', '1');
                    SET MonthStart = CAST(STR_TO_DATE(MonthStr, DateTimeFormat) AS DATETIME);
                    SET MonthEnd = DATE_ADD(DATE_ADD(MonthStart, INTERVAL 1 MONTH), INTERVAL -2 MICROSECOND);
                    SET MonthId = CONCAT(CAST(CurrentYear as VARCHAR(10)), '-', LPAD(CAST(CurrentMonth as varchar(10)), 2, '0'));
        
                    INSERT INTO MANAGEMENT_CL_TIME (ITEM_PARENT_ID, ID, START_PERIOD, END_PERIOD)
                        VALUES (CurrentQuarterItemId, MonthId, MonthStart, MonthEnd);
        
                    SET CurrentMonthItemId = LAST_INSERT_ID();
                    SET CurrentMonth = CurrentMonth + 1;
                END;
            END WHILE;
            SET CurrentYear = CurrentYear + 1;
        END;
    END WHILE;
 */
END;