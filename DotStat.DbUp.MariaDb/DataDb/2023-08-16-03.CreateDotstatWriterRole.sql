BEGIN NOT ATOMIC
    CREATE ROLE IF NOT EXISTS DotStatWriter;

    IF (SELECT count(1) FROM information_schema.applicable_roles WHERE ROLE_NAME = 'DotStatWriter') > 0
    THEN
        GRANT SELECT, INSERT, UPDATE, DELETE, INDEX, DROP ON $dbName$.* to DotStatWriter;
        GRANT ALTER ON $dbName$.* TO DotStatWriter;
        GRANT CREATE ON $dbName$.* TO DotStatWriter;
        GRANT CREATE VIEW ON $dbName$.* TO DotStatWriter;
        GRANT CREATE TEMPORARY TABLES ON $dbName$.* TO DotStatWriter;
        GRANT EXECUTE ON $dbName$.* TO DotStatWriter;
    END IF;
    FLUSH PRIVILEGES;
END;