#TODO needs to be tested with metadata
CREATE OR REPLACE PROCEDURE CREATE_MSD_VIEW(
    IN DSD_ID INT,
    IN MSD_ID INT,
    IN table_version CHAR(1)
)
main_proc: BEGIN
    -- Declare variables
    DECLARE sdmxId NVARCHAR(1000);
    DECLARE U_DSD_VERSION_ID VARCHAR(20);
    DECLARE NewLineChar CHAR(2) DEFAULT CHAR(10);
    DECLARE isLiveSet BIT;
    DECLARE isPITset BIT;
    DECLARE HasTimeDim BIT DEFAULT 0;
    DECLARE TimeDimId INT DEFAULT 0;
    DECLARE msg TEXT;
    DECLARE sqlStatement TEXT;
    DECLARE SelectPart TEXT;
    DECLARE FromPart TEXT;

    -- Step 1: Retrieve values using SELECT INTO
SELECT
    CONCAT(CAST(DSD_ID AS CHAR), '_', table_version),
    CONCAT(AGENCY, ':', ID, '(', CAST(VERSION_1 AS CHAR), '.', CAST(VERSION_2 AS CHAR),
           IFNULL(CONCAT('.', CAST(VERSION_3 AS CHAR)), '')),
    CASE WHEN DSD_LIVE_VERSION IS NULL THEN 0 ELSE 1 END,
    CASE WHEN DSD_PIT_VERSION IS NULL THEN 0 ELSE 1 END
INTO
    U_DSD_VERSION_ID,
    sdmxId,
    isLiveSet,
    isPITset
FROM management_ARTEFACT
WHERE type = 'DSD' AND ART_ID = DSD_ID;

-- Step 2: Check table existence
IF NOT EXISTS (SELECT 1 FROM information_schema.TABLES WHERE TABLE_SCHEMA = DATABASE() AND TABLE_NAME = CONCAT('data_META_', U_DSD_VERSION_ID)) THEN
        IF (isLiveSet = 0 AND isPITset = 0) THEN
            LEAVE main_proc; -- Skip further processing
END IF;

        -- Handle missing table error
        SET msg = CONCAT('The following error was found while trying to recreate the MSD Views for the DSD `', sdmxId ,'` (The creation of the view will be skiped): The table `data_META_', U_DSD_VERSION_ID , '` does not exist in the database, but a value has been set for the fields `DSD_LIVE_VERSION` or `DSD_PIT_VERSION` in the `management_ARTEFACT` for the given DSD.');
        SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = msg;
END IF;

-- Retrieve IF DSD has TIME dimension
SELECT 1 INTO HasTimeDim, TimeDimId
FROM management_COMPONENT
WHERE TYPE = 'TimeDimension' AND DSD_ID = DSD_ID;

-- get dimensions
INSERT INTO DSD_Components
SELECT c.COMP_ID,c.ID,c.TYPE,c.CL_ID
FROM management_COMPONENT c
WHERE c.DSD_ID = DSD_ID and c.TYPE = 'Dimension';

-- get referential metadata attributes
INSERT INTO DSD_Components
SELECT m.MTD_ATTR_ID,m.ID, 'MetadataAttribute',m.CL_ID
FROM management_METADATA_ATTRIBUTE m
WHERE m.MSD_ID = MSD_ID;

-- Step 3: Build SELECT Part
SET SelectPart = (
        SELECT CONCAT(
                       'SELECT ',
                       IFNULL(
                               GROUP_CONCAT(
                                       CASE
                                           WHEN C.CL_ID IS NOT NULL THEN CONCAT('`CL_', C.ID, '`.ID AS `', C.ID, '`,', NewLineChar)
                                           WHEN C.CL_ID IS NULL AND C.TYPE = 'Dimension' THEN CONCAT('ME.`DIM_', C.COMP_ID, '` AS `', C.ID, '`,', NewLineChar)
                                           WHEN C.CL_ID IS NULL AND C.TYPE = 'MetadataAttribute' THEN CONCAT('ME.`COMP_', C.COMP_ID, '` AS [', C.ID, '],', NewLineChar)
                                           END
                                       SEPARATOR ''
                                   ), ''
                           ),
                       CASE HasTimeDim
                           WHEN 1 THEN 'ME.PERIOD_SDMX AS TIME_PERIOD,ME.PERIOD_START,ME.PERIOD_END,'
                           ELSE ''
                           END,
                       ' ME.LAST_UPDATED'
                   )
        FROM DSD_Components C
    );

-- Step 4: Build FROM Part
    SET FromPart = (
        SELECT CONCAT(
                       'FROM data_META_', U_DSD_VERSION_ID, '] ME', CHAR(10),
                       IFNULL(
                               GROUP_CONCAT(
                                       CASE
                                           WHEN C.CL_ID IS NOT NULL AND C.TYPE = 'Dimension' THEN CONCAT(
                                                   'LEFT JOIN `management_CL_', C.CL_ID, '` AS `CL_', C.ID, '` ON ME.`DIM_', C.COMP_ID, '` = `CL_', C.ID, '`.ITEM_ID', NewLineChar
                                               )
                                           WHEN C.CL_ID IS NOT NULL AND C.TYPE = 'MetadataAttribute' THEN CONCAT(
                                                   'LEFT JOIN `management_CL_', C.CL_ID, '` AS `CL_', C.ID, '` ON ME.`COMP_', C.COMP_ID, '` = `CL_', C.ID, '`.ITEM_ID', NewLineChar
                                               )
                                           END
                                       SEPARATOR ''
                                   ), ''
                           )
                   )
        FROM DSD_Components C
    );

-- Recreating MetadataDsd VIEW for DSD
    SET sqlStatement = CONCAT(
            'CREATE OR REPLACE VIEW data_VI_MetadataDsd_', U_DSD_VERSION_ID, ' AS ',
            SelectPart, FromPart
        );

-- Dynamic SQL Execution
PREPARE stmt FROM sqlStatement;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;

END;


