BEGIN NOT ATOMIC
IF (SELECT count(1) FROM information_schema.tables WHERE table_schema ='$dbName$' AND table_name ='AUTHORIZATIONRULES') = 0
THEN
CREATE TABLE AUTHORIZATIONRULES(
                                   ID INT NOT NULL AUTO_INCREMENT,
                                   USERMASK varchar(100) NOT NULL,
                                   ISGROUP bit NOT NULL,
                                   DATASPACE varchar(100),
                                   ARTEFACTTYPE int,
                                   ARTEFACTAGENCYID varchar(255),
                                   ARTEFACTID varchar(255),
                                   ARTEFACTVERSION varchar(10),
                                   PERMISSION int NOT NULL,
                                   EDITEDBY varchar(100) NOT NULL,
                                   EDITDATE datetime(6) NOT null,
                                   PRIMARY KEY PK_AUTHORIZATIONRULES(ID),

                                   INDEX I_AUTHORIZATIONRULES (USERMASK,ISGROUP),
                                   UNIQUE INDEX U_AUTHORIZATIONRULES (USERMASK,ISGROUP,DATASPACE,ARTEFACTTYPE,ARTEFACTAGENCYID,ARTEFACTID,ARTEFACTVERSION)
);
INSERT AUTHORIZATIONRULES (USERMASK, ISGROUP, DATASPACE, ARTEFACTTYPE, ARTEFACTAGENCYID, ARTEFACTID, ARTEFACTVERSION, PERMISSION, EDITEDBY, EDITDATE) 
	    VALUES ('admin', 0, '*', 0, '*', '*', '*', 4095, 'system', CURRENT_DATE());
	INSERT AUTHORIZATIONRULES (USERMASK, ISGROUP, DATASPACE, ARTEFACTTYPE, ARTEFACTAGENCYID, ARTEFACTID, ARTEFACTVERSION, PERMISSION, EDITEDBY, EDITDATE) 
	    VALUES ('admins', 1, '*', 0, '*', '*', '*', 4095, 'system', CURRENT_DATE());
END IF;
END;


BEGIN NOT ATOMIC
IF (SELECT count(1) FROM information_schema.tables WHERE table_schema ='$dbName$' AND table_name ='DB_VERSION') = 0
THEN
CREATE TABLE DB_VERSION(
                           SYS_ID bigint AUTO_INCREMENT NOT NULL,
                           VERSION varchar(250) NULL,
                           PRIMARY KEY PK_DB_VERSION (SYS_ID ASC)
);

INSERT INTO DB_VERSION (VERSION) VALUES ('1.0');
END IF;
END;