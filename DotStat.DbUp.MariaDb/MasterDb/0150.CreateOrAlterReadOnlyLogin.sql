﻿BEGIN NOT ATOMIC
    IF (SELECT count(1) FROM mysql.user WHERE `User` = '$ROloginName$') = 0
    THEN
        CREATE USER $ROloginName$ IDENTIFIED BY '$ROloginPwd$';
    ELSEIF '$alterPassword$' = '1'
    THEN
        ALTER USER $ROloginName$ IDENTIFIED by '$ROloginPwd$';
    END IF;
END;
