﻿BEGIN NOT ATOMIC
    IF (SELECT count(1) FROM mysql.user WHERE `User` = '$loginName$') = 0
    THEN
        CREATE USER $loginName$ IDENTIFIED BY '$loginPwd$';
    ELSEIF '$alterPassword$' = '1'
    THEN
        ALTER USER $loginName$ IDENTIFIED by '$loginPwd$';
    END IF;
END;