BEGIN NOT ATOMIC
CREATE ROLE IF NOT EXISTS DotStatReader;

IF (SELECT count(1) FROM information_schema.applicable_roles WHERE ROLE_NAME = 'DotStatReader') > 0
THEN
    GRANT SELECT ON $dbName$.* to DotStatReader;
END IF;

FLUSH PRIVILEGES;
END;
