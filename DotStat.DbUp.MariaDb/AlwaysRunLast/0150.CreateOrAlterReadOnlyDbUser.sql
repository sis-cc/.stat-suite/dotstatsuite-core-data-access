﻿BEGIN NOT ATOMIC
    IF (SELECT count(1) FROM mysql.user WHERE `User` = '$ROloginName$') = 0
    THEN
        CREATE USER $ROloginName$ IDENTIFIED BY '$ROloginPwd$';
        REVOKE ALL PRIVILEGES, GRANT OPTION FROM '$ROloginName$'@'%';
    END IF;
    
    IF (SELECT count(1) FROM information_schema.applicable_roles WHERE ROLE_NAME = 'DotStatReader') > 0
    THEN
        GRANT DotStatReader TO '$ROloginName$'@'%';
        SET DEFAULT ROLE DotStatReader FOR '$ROloginName$'@'%';
    END IF;
    
    FLUSH PRIVILEGES;
END;