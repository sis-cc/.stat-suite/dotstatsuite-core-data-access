﻿BEGIN NOT ATOMIC
    IF (SELECT count(1) FROM mysql.user WHERE `User` = '$loginName$') = 0
    THEN
        CREATE USER $loginName$ IDENTIFIED BY '$loginPwd$';
        REVOKE ALL PRIVILEGES, GRANT OPTION FROM '$loginName$'@'%';
    END IF;
    
    IF (SELECT count(1) FROM information_schema.applicable_roles WHERE ROLE_NAME = 'DotStatWriter') > 0
    THEN
        GRANT DotStatWriter TO '$loginName$'@'%';
        SET DEFAULT ROLE DotStatWriter FOR '$loginName$'@'%';
    END IF;

    FLUSH PRIVILEGES;
END;