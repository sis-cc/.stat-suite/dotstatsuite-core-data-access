﻿using System;
using System.IO;
using DotStat.Common.Logger;
using FluentAssertions;
using log4net.Config;
using Microsoft.AspNetCore.Http;
using Moq;
using NUnit.Framework;

namespace DotStat.Test.DataAccess.MariaDb; 

[SetUpFixture]
internal class GlobalSetup
{
    [OneTimeSetUp]
    public void Init()
    {

#if DEBUG
        //log4net console setup
        XmlConfigurator.Configure(new FileInfo(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "config", "log4net.xml")));
        Log.Configure(new Mock<IHttpContextAccessor>().Object);
#endif

        AssertionOptions.AssertEquivalencyUsing(options =>
        {
            options.Using<DateTime>(ctx => ctx.Subject.Should().BeCloseTo(ctx.Expectation, TimeSpan.FromMilliseconds(100))).WhenTypeIs<DateTime>();
            options.Using<DateTimeOffset>(ctx => ctx.Subject.Should().BeCloseTo(ctx.Expectation, TimeSpan.FromMilliseconds(100))).WhenTypeIs<DateTimeOffset>();
            return options;
        });
    }
}