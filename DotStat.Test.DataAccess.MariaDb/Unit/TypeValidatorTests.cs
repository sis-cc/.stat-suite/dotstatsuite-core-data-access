﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using DotStat.Common.Configuration;
using DotStat.Common.Exceptions;
using DotStat.Db.Validation;
using DotStat.DB.Validation.TypeValidation;
using DotStat.Domain;
using DotStat.MappingStore;
using DotStat.Test.Moq;
using Microsoft.Extensions.Configuration;
using NUnit.Framework;
using Org.Sdmxsource.Sdmx.Api.Constants;
using PrimaryMeasure = DotStat.Domain.PrimaryMeasure;

namespace DotStat.Test.DataAccess.MariaDb.Unit
{
    [TestFixture, Parallelizable(ParallelScope.All)]
    public sealed class TypeValidatorTests : UnitTestBase
    {
        private readonly TestMappingStoreDataAccess _mappingStoreDataAccess;

        private readonly SdmxTypeValidatorBuilder _typeValidatorBuilder;

        public TypeValidatorTests()
        {
            _mappingStoreDataAccess = new TestMappingStoreDataAccess("sdmx/OBS_TYPE_TEST.xml");

            var configuration = GetConfiguration().Get<BaseConfiguration>();
            configuration.MaxTransferErrorAmount = 100;

            _typeValidatorBuilder = new SdmxTypeValidatorBuilder();
        }

        [TestCase("AA0as")]
        [TestCase("1323")]
        [TestCase(@"12312~!@#@#!$@!#$jko@!#$OPIO@!#I*($&*-01@#$&*-0n8i 90qsduiopwqumWOQIPUoiwqeu")]
        [TestCase("")]
        [TestCase(null)]
        [TestCase("NaN")]
        public void StringValidatorTest(string observationValue, ValidationErrorType expectedValidationError = ValidationErrorType.Undefined)
        {
            TypeValidatorTest(TextEnumType.String, observationValue, expectedValidationError);
        }

        [TestCase("AAAAasfasdf")]
        [TestCase("123asfasdf", false)]
        [TestCase("Not valid", false)]
        [TestCase("")]
        [TestCase(null)]
        [TestCase("NaN")]
        public void AlphaValidatorTest(string observationValue, bool isValid = true)
        {
            TypeValidatorTest(TextEnumType.Alpha, observationValue, isValid ? ValidationErrorType.Undefined : ValidationErrorType.InvalidValueFormatNotAlpha);
        }

        [TestCase("11AAAbsdf23")]
        [TestCase("11!@#!23", false)]
        [TestCase("Not valid", false)]
        [TestCase("")]
        [TestCase(null)]
        [TestCase("NaN")]
        public void AlphanumericValidatorTest(string observationValue, bool isValid = true)
        {
            TypeValidatorTest(TextEnumType.Alphanumeric, observationValue, isValid ? ValidationErrorType.Undefined : ValidationErrorType.InvalidValueFormatNotAlphaNumeric);
        }

        [TestCase("00011089723149023")]
        [TestCase("123.90890", false)]
        [TestCase("Not valid", false)]
        [TestCase("")]
        [TestCase(null)]
        [TestCase("NaN", false)]
        public void NumericValidatorTest(string observationValue, bool isValid = true)
        {
            TypeValidatorTest(TextEnumType.Numeric, observationValue, isValid ? ValidationErrorType.Undefined : ValidationErrorType.InvalidValueFormatNotNumeric);
        }

        [TestCase("-12345678901234567890123456789012345678")]
        [TestCase("12345678901234567890123456789012345678")]
        [TestCase("123.90890", false)]
        [TestCase(".9080003090", false)]
        [TestCase("Not valid", false)]
        [TestCase("")]
        [TestCase(null)]
        [TestCase("NaN", false)]
        [TestCase("NAN", false)]
        [TestCase("NaN123", false)]
        [TestCase("123NaN", false)]
        [TestCase("123NaN123", false)]
        public void BigIntegerValidatorTest(string observationValue, bool isValid = true)
        {
            TypeValidatorTest(TextEnumType.BigInteger, observationValue, isValid ? ValidationErrorType.Undefined : ValidationErrorType.InvalidValueFormatNotBigInteger);
        }

        [TestCase("-9223372036854775808")]
        [TestCase("123.90890", false)]
        [TestCase("9223372036854775808", false)]
        [TestCase("Not valid", false)]
        [TestCase("")]
        [TestCase(null)]
        [TestCase("NaN", false)]
        [TestCase("NAN", false)]
        [TestCase("NaN123", false)]
        [TestCase("123NaN", false)]
        [TestCase("123NaN123", false)]
        public void LongValidatorTest(string observationValue, bool isValid = true)
        {
            TypeValidatorTest(TextEnumType.Long, observationValue, isValid ? ValidationErrorType.Undefined : ValidationErrorType.InvalidValueFormatNotLong);
        }

        [TestCase("-32768")]
        [TestCase("+32767")]
        [TestCase("32768", false)]
        [TestCase("123.90890", false)]
        [TestCase("Not valid", false)]
        [TestCase("")]
        [TestCase(null)]
        [TestCase("NaN", false)]
        [TestCase("NAN", false)]
        [TestCase("NaN123", false)]
        [TestCase("123NaN", false)]
        [TestCase("123NaN123", false)]
        public void ShortValidatorTest(string observationValue, bool isValid = true)
        {
            TypeValidatorTest(TextEnumType.Short, observationValue, isValid ? ValidationErrorType.Undefined : ValidationErrorType.InvalidValueFormatNotShort);
        }

        [TestCase("0")]
        [TestCase("1")]
        [TestCase("true")]
        [TestCase("false")]
        [TestCase("False", false)]
        [TestCase("True", false)]
        [TestCase("yes", false)]
        [TestCase("-1", false)]
        [TestCase("32768", false)]
        [TestCase("123.90890", false)]
        [TestCase("Not valid", false)]
        [TestCase("")]
        [TestCase(null)]
        [TestCase("NaN", false)]
        [TestCase("NAN", false)]
        [TestCase("NaN1", false)]
        [TestCase("0NaN", false)]
        [TestCase("0NaN1", false)]
        public void BooleanValidatorTest(string observationValue, bool isValid = true)
        {
            TypeValidatorTest(TextEnumType.Boolean, observationValue, isValid ? ValidationErrorType.Undefined : ValidationErrorType.InvalidValueFormatNotBoolean);
        }

        [TestCase("-12345678901234567890123456789012345.678")]
        [TestCase("12345678901234567890123456789012345.678")]
        [TestCase("123.90890")]
        [TestCase(".8900")]
        [TestCase("1234567890123456789012345678901234567.890")]
        [TestCase("1234567890123456789012345678901234567.", false)]
        [TestCase("Not valid", false)]
        [TestCase("")]
        [TestCase(null)]
        [TestCase("NaN", false)]
        [TestCase("NAN", false)]
        [TestCase("NaN123", false)]
        [TestCase("123NaN", false)]
        [TestCase("123NaN123", false)]
        public void DecimalValidatorTest(string observationValue, bool isValid = true)
        {
            TypeValidatorTest(TextEnumType.Decimal, observationValue, isValid ? ValidationErrorType.Undefined : ValidationErrorType.InvalidValueFormatNotDecimal);
        }

        [TestCase("-3.40E+38")]
        [TestCase("+1.18E-38")]
        [TestCase("11838")]
        [TestCase("11.9999838", true, false, "en")]
        [TestCase("11,9999838", true, false, "fr")]
        [TestCase("3.41E+138", false)]
        [TestCase("NaN", false)]
        [TestCase("INF", false)]
        [TestCase("-INF", false)]
        [TestCase("Not valid", false)]
        [TestCase("")]
        [TestCase(null)]
        [TestCase("NAN", false)]
        [TestCase("NaN123", false)]
        [TestCase("123NaN", false)]
        [TestCase("123NaN123", false)]
        [TestCase("NaN", true, true)]
        [TestCase("#N/A", false, true)]
        public void FloatValidatorTest(string observationValue, bool isValid = true, bool supportIntentionallyMissingValues = false, string cultureInfo = null)
        {
            TypeValidatorTest(TextEnumType.Float, observationValue, isValid ? ValidationErrorType.Undefined : ValidationErrorType.InvalidValueFormatNotFloat, false, supportIntentionallyMissingValues, cultureInfo);
        }

        [TestCase("-1.79E+308")]
        [TestCase("+2.23E-308")]
        [TestCase("11838")]
        [TestCase("11.9999838", true, false, "en")]
        [TestCase("11,9999838", true, false, "fr")]
        [TestCase("4.23E408", false)]
        [TestCase("NaN", false)]
        [TestCase("INF", false)]
        [TestCase("-INF", false)]
        [TestCase("Not valid", false)]
        [TestCase("")]
        [TestCase(null)]
        [TestCase("NAN", false)]
        [TestCase("NaN123", false)]
        [TestCase("123NaN", false)]
        [TestCase("123NaN123", false)]
        [TestCase("NaN", true, true)]
        [TestCase("#N/A", false, true)]
        public void DoubleValidatorTest(string observationValue, bool isValid = true, bool supportIntentionallyMissingValues = false, string cultureInfo = null)
        {
            TypeValidatorTest(TextEnumType.Double, observationValue, isValid ? ValidationErrorType.Undefined : ValidationErrorType.InvalidValueFormatNotDouble, false, supportIntentionallyMissingValues, cultureInfo);
        }

        [TestCase("http://dotstat.suite.org")]
        [TestCase("/customer/test")]
        [TestCase("/customer/test?y=hello&x=bye")]
        [TestCase(@"C:\Temp\file.txt", false)]
        [TestCase("Not valid", false)]
        [TestCase("")]
        [TestCase(null)]
        [TestCase("NaN")]
        public void UriValidatorTest(string observationValue, bool isValid = true)
        {
            TypeValidatorTest(TextEnumType.Uri, observationValue, isValid ? ValidationErrorType.Undefined : ValidationErrorType.InvalidValueFormatNotUri);
        }

        [TestCase("-2147483648")]
        [TestCase("123.90890", false)]
        [TestCase("2147483648", false)]
        [TestCase("Not valid", false)]
        [TestCase("")]
        [TestCase(null)]
        [TestCase("NaN", false)]
        [TestCase("NAN", false)]
        [TestCase("NaN123", false)]
        [TestCase("123NaN", false)]
        [TestCase("123NaN123", false)]
        public void CountValidatorTest(string observationValue, bool isValid = true)
        {
            TypeValidatorTest(TextEnumType.Count, observationValue, isValid ? ValidationErrorType.Undefined : ValidationErrorType.InvalidValueFormatNotCount);
        }

        public void GregorianTimePeriodValidatorTest(string observationValue, ValidationErrorType expectedValidationError = ValidationErrorType.Undefined)
        {
            TypeValidatorTest(TextEnumType.GregorianTimePeriod, observationValue, expectedValidationError);
        }

        [TestCase("2020")]
        [TestCase("2020Z")]
        [TestCase("2020-14:00")]
        [TestCase("2020+02:30")]
        [TestCase("10896Z")]
        [TestCase("0896")]
        [TestCase("2020-A1", false, true)]
        [TestCase("896", false)]
        [TestCase("123.90890", false)]
        [TestCase("0", false)]
        [TestCase("Not valid", false)]
        [TestCase("")]
        [TestCase(null)]
        [TestCase("NaN", false)]
        public void GregorianYearValidatorTest(string observationValue, bool isValid = true, bool skipTestOfSuperSetTypes = false)
        {
            TypeValidatorTest(TextEnumType.GregorianYear, observationValue, isValid ? ValidationErrorType.Undefined : ValidationErrorType.InvalidValueFormatNotGregorianYear);

            if (skipTestOfSuperSetTypes) return;

            TypeValidatorTest(TextEnumType.GregorianTimePeriod, observationValue, isValid ? ValidationErrorType.Undefined : ValidationErrorType.InvalidValueFormatNotGregorianTimePeriod);
            TypeValidatorTest(TextEnumType.BasicTimePeriod, observationValue, isValid ? ValidationErrorType.Undefined : ValidationErrorType.InvalidValueFormatNotBasicTimePeriod);
            TypeValidatorTest(TextEnumType.StandardTimePeriod, observationValue, isValid ? ValidationErrorType.Undefined : ValidationErrorType.InvalidValueFormatNotStandardTimePeriod);
            TypeValidatorTest(TextEnumType.ObservationalTimePeriod, observationValue, isValid ? ValidationErrorType.Undefined : ValidationErrorType.InvalidValueFormatNotObservationalTimePeriod);
        }

        [TestCase("2020-09")]
        [TestCase("2020-09Z")]
        [TestCase("2020-09-14:00")]
        [TestCase("2020-09+02:30")]
        [TestCase("10896-09Z")]
        [TestCase("0896-09")]
        [TestCase("896-09", false)]
        [TestCase("1222-14", false)]
        [TestCase("123.90890", false, true)]
        [TestCase("0", false, true)]
        [TestCase("Not valid", false, true)]
        [TestCase("", true, true)]
        [TestCase(null, true, true)]
        [TestCase("NaN", false)]
        public void GregorianYearMonthValidatorTest(string observationValue, bool isValid = true, bool skipTestOfSuperSetTypes = false)
        {
            TypeValidatorTest(TextEnumType.GregorianYearMonth, observationValue, isValid ? ValidationErrorType.Undefined : ValidationErrorType.InvalidValueFormatNotGregorianYearMonth);

            if (skipTestOfSuperSetTypes) return;

            TypeValidatorTest(TextEnumType.GregorianTimePeriod, observationValue, isValid ? ValidationErrorType.Undefined : ValidationErrorType.InvalidValueFormatNotGregorianTimePeriod);
            TypeValidatorTest(TextEnumType.BasicTimePeriod, observationValue, isValid ? ValidationErrorType.Undefined : ValidationErrorType.InvalidValueFormatNotBasicTimePeriod);
            TypeValidatorTest(TextEnumType.StandardTimePeriod, observationValue, isValid ? ValidationErrorType.Undefined : ValidationErrorType.InvalidValueFormatNotStandardTimePeriod);
            TypeValidatorTest(TextEnumType.ObservationalTimePeriod, observationValue, isValid ? ValidationErrorType.Undefined : ValidationErrorType.InvalidValueFormatNotObservationalTimePeriod);
        }

        [TestCase("2020-09-24")]
        [TestCase("2020-09-24Z")]
        [TestCase("2020-09-24-14:00")]
        [TestCase("2020-09-24+02:30")]
        [TestCase("10896-09-24Z")]
        [TestCase("0896-09-24")]
        [TestCase("896-09-24", false)]
        [TestCase("1222-14-24", false)]
        [TestCase("1222-09-34", false)]
        [TestCase("123.90890", false, true)]
        [TestCase("0", false, true)]
        [TestCase("Not valid", false, true)]
        [TestCase("", true, true)]
        [TestCase(null, true, true)]
        [TestCase("NaN", false)]
        public void GregorianDayValidatorTest(string observationValue, bool isValid = true, bool skipTestOfSuperSetTypes = false)
        {
            TypeValidatorTest(TextEnumType.GregorianDay, observationValue, isValid ? ValidationErrorType.Undefined : ValidationErrorType.InvalidValueFormatNotGregorianDay);

            if (skipTestOfSuperSetTypes) return;

            TypeValidatorTest(TextEnumType.GregorianTimePeriod, observationValue, isValid ? ValidationErrorType.Undefined : ValidationErrorType.InvalidValueFormatNotGregorianTimePeriod);
            TypeValidatorTest(TextEnumType.BasicTimePeriod, observationValue, isValid ? ValidationErrorType.Undefined : ValidationErrorType.InvalidValueFormatNotBasicTimePeriod);
            TypeValidatorTest(TextEnumType.StandardTimePeriod, observationValue, isValid ? ValidationErrorType.Undefined : ValidationErrorType.InvalidValueFormatNotStandardTimePeriod);
            TypeValidatorTest(TextEnumType.ObservationalTimePeriod, observationValue, isValid ? ValidationErrorType.Undefined : ValidationErrorType.InvalidValueFormatNotObservationalTimePeriod);
        }

        [TestCase("2020-A1")]
        [TestCase("2020-A1Z")]
        [TestCase("2020-A1-14:00")]
        [TestCase("2020-A1+02:30")]
        [TestCase("0896-A1")]
        [TestCase("896-A1Z", false)]
        [TestCase("10896-A1Z", false)]
        [TestCase("2020", false, true)]
        [TestCase("2020-A", false)]
        [TestCase("2020-A0", false)]
        [TestCase("2020-A2", false)]
        [TestCase("2020-S1", false, true)]
        [TestCase("896", false)]
        [TestCase("123.90890", false, true)]
        [TestCase("0", false, true)]
        [TestCase("Not valid", false, true)]
        [TestCase("", true, true)]
        [TestCase(null, true, true)]
        [TestCase("NaN", false)]
        public void ReportingYearValidatorTest(string observationValue, bool isValid = true, bool skipTestOfSuperSetTypes = false)
        {
            TypeValidatorTest(TextEnumType.ReportingYear, observationValue, isValid ? ValidationErrorType.Undefined : ValidationErrorType.InvalidValueFormatNotReportingYear);

            if (skipTestOfSuperSetTypes) return;

            TypeValidatorTest(TextEnumType.ReportingTimePeriod, observationValue, isValid ? ValidationErrorType.Undefined : ValidationErrorType.InvalidValueFormatNotReportingTimePeriod);
            TypeValidatorTest(TextEnumType.StandardTimePeriod, observationValue, isValid ? ValidationErrorType.Undefined : ValidationErrorType.InvalidValueFormatNotStandardTimePeriod);
            TypeValidatorTest(TextEnumType.ObservationalTimePeriod, observationValue, isValid ? ValidationErrorType.Undefined : ValidationErrorType.InvalidValueFormatNotObservationalTimePeriod);
        }

        [TestCase("2020-S1")]
        [TestCase("2020-S2")]
        [TestCase("2020-S1Z")]
        [TestCase("2020-S1-14:00")]
        [TestCase("2020-S1+02:30")]
        [TestCase("0896-S1")]
        [TestCase("896-S1Z", false)]
        [TestCase("10896-S1Z", false)]
        [TestCase("2020", false, true)]
        [TestCase("2020-S", false)]
        [TestCase("2020-S0", false)]
        [TestCase("2020-S3", false)]
        [TestCase("2020-A1", false, true)]
        [TestCase("123.90890", false, true)]
        [TestCase("0", false, true)]
        [TestCase("Not valid", false, true)]
        [TestCase("", true, true)]
        [TestCase(null, true, true)]
        [TestCase("NaN", false)]
        public void ReportingSemesterValidatorTest(string observationValue, bool isValid = true, bool skipTestOfSuperSetTypes = false)
        {
            TypeValidatorTest(TextEnumType.ReportingSemester, observationValue, isValid ? ValidationErrorType.Undefined : ValidationErrorType.InvalidValueFormatNotReportingSemester);

            if (skipTestOfSuperSetTypes) return;

            TypeValidatorTest(TextEnumType.ReportingTimePeriod, observationValue, isValid ? ValidationErrorType.Undefined : ValidationErrorType.InvalidValueFormatNotReportingTimePeriod);
            TypeValidatorTest(TextEnumType.StandardTimePeriod, observationValue, isValid ? ValidationErrorType.Undefined : ValidationErrorType.InvalidValueFormatNotStandardTimePeriod);
            TypeValidatorTest(TextEnumType.ObservationalTimePeriod, observationValue, isValid ? ValidationErrorType.Undefined : ValidationErrorType.InvalidValueFormatNotObservationalTimePeriod);
        }

        [TestCase("2020-T1")]
        [TestCase("2020-T2")]
        [TestCase("2020-T3")]
        [TestCase("2020-T1Z")]
        [TestCase("2020-T1-14:00")]
        [TestCase("2020-T1+02:30")]
        [TestCase("0896-T1")]
        [TestCase("896-T1Z", false)]
        [TestCase("10896-T1Z", false)]
        [TestCase("2020", false, true)]
        [TestCase("2020-T", false)]
        [TestCase("2020-T0", false)]
        [TestCase("2020-T4", false)]
        [TestCase("2020-A1", false, true)]
        [TestCase("123.90890", false, true)]
        [TestCase("0", false, true)]
        [TestCase("Not valid", false,true)]
        [TestCase("", true, true)]
        [TestCase(null, true, true)]
        [TestCase("NaN", false)]
        public void ReportingTrimesterValidatorTest(string observationValue, bool isValid = true, bool skipTestOfSuperSetTypes = false)
        {
            TypeValidatorTest(TextEnumType.ReportingTrimester, observationValue, isValid ? ValidationErrorType.Undefined : ValidationErrorType.InvalidValueFormatNotReportingTrimester);

            if (skipTestOfSuperSetTypes) return;

            TypeValidatorTest(TextEnumType.ReportingTimePeriod, observationValue, isValid ? ValidationErrorType.Undefined : ValidationErrorType.InvalidValueFormatNotReportingTimePeriod);
            TypeValidatorTest(TextEnumType.StandardTimePeriod, observationValue, isValid ? ValidationErrorType.Undefined : ValidationErrorType.InvalidValueFormatNotStandardTimePeriod);
            TypeValidatorTest(TextEnumType.ObservationalTimePeriod, observationValue, isValid ? ValidationErrorType.Undefined : ValidationErrorType.InvalidValueFormatNotObservationalTimePeriod);
        }

        [TestCase("2020-Q1")]
        [TestCase("2020-Q2")]
        [TestCase("2020-Q3")]
        [TestCase("2020-Q4")]
        [TestCase("2020-Q1Z")]
        [TestCase("2020-Q1-14:00")]
        [TestCase("2020-Q1+02:30")]
        [TestCase("0896-Q1")]
        [TestCase("896-Q1Z", false)]
        [TestCase("10896-Q1Z", false)]
        [TestCase("2020", false, true)]
        [TestCase("2020-Q", false)]
        [TestCase("2020-Q0", false)]
        [TestCase("2020-Q5", false)]
        [TestCase("2020-A1", false, true)]
        [TestCase("123.90890", false, true)]
        [TestCase("0", false, true)]
        [TestCase("Not valid", false, true)]
        [TestCase("", true, true)]
        [TestCase(null, true, true)]
        [TestCase("NaN", false)]
        public void ReportingQuarterValidatorTest(string observationValue, bool isValid = true, bool skipTestOfSuperSetTypes = false)
        {
            TypeValidatorTest(TextEnumType.ReportingQuarter, observationValue, isValid ? ValidationErrorType.Undefined : ValidationErrorType.InvalidValueFormatNotReportingQuarter);

            if (skipTestOfSuperSetTypes) return;

            TypeValidatorTest(TextEnumType.ReportingTimePeriod, observationValue, isValid ? ValidationErrorType.Undefined : ValidationErrorType.InvalidValueFormatNotReportingTimePeriod);
            TypeValidatorTest(TextEnumType.StandardTimePeriod, observationValue, isValid ? ValidationErrorType.Undefined : ValidationErrorType.InvalidValueFormatNotStandardTimePeriod);
            TypeValidatorTest(TextEnumType.ObservationalTimePeriod, observationValue, isValid ? ValidationErrorType.Undefined : ValidationErrorType.InvalidValueFormatNotObservationalTimePeriod);
        }

        [TestCase("2020-M01")]
        [TestCase("2020-M12")]
        [TestCase("2020-M03")]
        [TestCase("2020-M01Z")]
        [TestCase("2020-M01-14:00")]
        [TestCase("2020-M01+02:30")]
        [TestCase("0896-M01")]
        [TestCase("896-M01Z", false)]
        [TestCase("10896-M01Z", false)]
        [TestCase("2020", false, true)]
        [TestCase("2020-M", false)]
        [TestCase("2020-M2", false)]
        [TestCase("2020-M00", false)]
        [TestCase("2020-M13", false)]
        [TestCase("2020-A1", false, true)]
        [TestCase("123.90890", false, true)]
        [TestCase("0", false, true)]
        [TestCase("Not valid", false, true)]
        [TestCase("", true, true)]
        [TestCase(null, true, true)]
        [TestCase("NaN", false)]
        public void ReportingMonthValidatorTest(string observationValue, bool isValid = true, bool skipTestOfSuperSetTypes = false)
        {
            TypeValidatorTest(TextEnumType.ReportingMonth, observationValue, isValid ? ValidationErrorType.Undefined : ValidationErrorType.InvalidValueFormatNotReportingMonth);

            if (skipTestOfSuperSetTypes) return;

            TypeValidatorTest(TextEnumType.ReportingTimePeriod, observationValue, isValid ? ValidationErrorType.Undefined : ValidationErrorType.InvalidValueFormatNotReportingTimePeriod);
            TypeValidatorTest(TextEnumType.StandardTimePeriod, observationValue, isValid ? ValidationErrorType.Undefined : ValidationErrorType.InvalidValueFormatNotStandardTimePeriod);
            TypeValidatorTest(TextEnumType.ObservationalTimePeriod, observationValue, isValid ? ValidationErrorType.Undefined : ValidationErrorType.InvalidValueFormatNotObservationalTimePeriod);
        }

        [TestCase("2020-W01")]
        [TestCase("2020-W53")]
        [TestCase("2020-W03")]
        [TestCase("2020-W01Z")]
        [TestCase("2020-W01-14:00")]
        [TestCase("2020-W01+02:30")]
        [TestCase("0896-W01")]
        [TestCase("896-W01Z", false)]
        [TestCase("10896-W01Z", false)]
        [TestCase("2020", false, true)]
        [TestCase("2020-W", false)]
        [TestCase("2020-W2", false)]
        [TestCase("2020-W00", false)]
        [TestCase("2020-W54", false)]
        [TestCase("2020-A1", false, true)]
        [TestCase("123.90890", false, true)]
        [TestCase("0", false, true)]
        [TestCase("Not valid", false, true)]
        [TestCase("", true, true)]
        [TestCase(null, true, true)]
        [TestCase("NaN", false)]
        public void ReportingWeekValidatorTest(string observationValue, bool isValid = true, bool skipTestOfSuperSetTypes = false)
        {
            TypeValidatorTest(TextEnumType.ReportingWeek, observationValue, isValid ? ValidationErrorType.Undefined : ValidationErrorType.InvalidValueFormatNotReportingWeek);

            if (skipTestOfSuperSetTypes) return;

            TypeValidatorTest(TextEnumType.ReportingTimePeriod, observationValue, isValid ? ValidationErrorType.Undefined : ValidationErrorType.InvalidValueFormatNotReportingTimePeriod);
            TypeValidatorTest(TextEnumType.StandardTimePeriod, observationValue, isValid ? ValidationErrorType.Undefined : ValidationErrorType.InvalidValueFormatNotStandardTimePeriod);
            TypeValidatorTest(TextEnumType.ObservationalTimePeriod, observationValue, isValid ? ValidationErrorType.Undefined : ValidationErrorType.InvalidValueFormatNotObservationalTimePeriod);
        }

        [TestCase("2020-D001")]
        [TestCase("2020-D366")]
        [TestCase("2020-D017")]
        [TestCase("2020-D017Z")]
        [TestCase("2020-D017-14:00")]
        [TestCase("2020-D101+02:30")]
        [TestCase("0896-D201")]
        [TestCase("896-D301Z", false)]
        [TestCase("10896-D301Z", false)]
        [TestCase("2020", false, true)]
        [TestCase("2020-D", false)]
        [TestCase("2020-D2", false)]
        [TestCase("2020-D39", false)]
        [TestCase("2020-D000", false)]
        [TestCase("2020-D367", false)]
        [TestCase("2020-A1", false, true)]
        [TestCase("123.90890", false, true)]
        [TestCase("0", false, true)]
        [TestCase("Not valid", false, true)]
        [TestCase("", true, true)]
        [TestCase(null, true, true)]
        [TestCase("NaN", false)]
        public void ReportingDayValidatorTest(string observationValue, bool isValid = true, bool skipTestOfSuperSetTypes = false)
        {
            TypeValidatorTest(TextEnumType.ReportingDay, observationValue, isValid ? ValidationErrorType.Undefined : ValidationErrorType.InvalidValueFormatNotReportingDay);

            if (skipTestOfSuperSetTypes) return;

            TypeValidatorTest(TextEnumType.ReportingTimePeriod, observationValue, isValid ? ValidationErrorType.Undefined : ValidationErrorType.InvalidValueFormatNotReportingTimePeriod);
            TypeValidatorTest(TextEnumType.StandardTimePeriod, observationValue, isValid ? ValidationErrorType.Undefined : ValidationErrorType.InvalidValueFormatNotStandardTimePeriod);
            TypeValidatorTest(TextEnumType.ObservationalTimePeriod, observationValue, isValid ? ValidationErrorType.Undefined : ValidationErrorType.InvalidValueFormatNotObservationalTimePeriod);
        }

        [TestCase("2020-09-24T14:25:37")]
        [TestCase("2020-09-24T14:25:37Z")]
        [TestCase("2020-09-24T14:25:37.123Z")]
        [TestCase("2020-09-24T24:00:00")]
        [TestCase("2020-09-24T14:25:37-14:00")]
        [TestCase("2020-09-24T14:25:37+02:30")]
        [TestCase("2020-09-24T14:25:37.123+02:30")]
        [TestCase("0896-09-24T14:25:37")]
        [TestCase("108966-09-24T14:25:37Z")]
        [TestCase("896-09-24T14:25:37Z", false)]
        [TestCase("T14:25:37Z", false)]
        [TestCase("2020", false, true)]
        [TestCase("2020-09", false, true)]
        [TestCase("2020-09-24", false, true)]
        [TestCase("2020-09-24Z", false, true)]
        [TestCase("2020-09-24+14:00", false, true)]
        [TestCase("2020-D309", false, true)]
        [TestCase("2020-W09", false, true)]
        [TestCase("2020-M11", false, true)]
        [TestCase("2020-A1", false, true)]
        [TestCase("123.90890", false, true)]
        [TestCase("0", false, true)]
        [TestCase("Not valid", false, true)]
        [TestCase("", true, true)]
        [TestCase(null, true, true)]
        [TestCase("NaN", false)]
        public void DateTimeValidatorTest(string observationValue, bool isValid = true, bool skipTestOfSuperSetTypes = false)
        {
            TypeValidatorTest(TextEnumType.DateTime, observationValue, isValid ? ValidationErrorType.Undefined : ValidationErrorType.InvalidValueFormatNotDateTime);

            if (skipTestOfSuperSetTypes) return;

            TypeValidatorTest(TextEnumType.BasicTimePeriod, observationValue, isValid ? ValidationErrorType.Undefined : ValidationErrorType.InvalidValueFormatNotBasicTimePeriod);
            TypeValidatorTest(TextEnumType.StandardTimePeriod, observationValue, isValid ? ValidationErrorType.Undefined : ValidationErrorType.InvalidValueFormatNotStandardTimePeriod);
            TypeValidatorTest(TextEnumType.ObservationalTimePeriod, observationValue, isValid ? ValidationErrorType.Undefined : ValidationErrorType.InvalidValueFormatNotObservationalTimePeriod);
        }

        [TestCase("2020-09-24T14:25:37/P1Y")]
        [TestCase("2020-09-24T14:25:37/-P32Y")]
        [TestCase("2020-09-24T14:25:37/P3M")]
        [TestCase("2020-09-24T14:25:37/-P287M")]
        [TestCase("2020-09-24T14:25:37/P1D")]
        [TestCase("2020-09-24T14:25:37/-P427D")]
        [TestCase("2020-09-24T14:25:37/P1Y4M")]
        [TestCase("2020-09-24T14:25:37/P1Y88D")]
        [TestCase("2020-09-24T14:25:37/P23M88D")]
        [TestCase("2020-09-24T14:25:37/PT23H")]
        [TestCase("2020-09-24T14:25:37/-PT279H")]
        [TestCase("2020-09-24T14:25:37/PT23M")]
        [TestCase("2020-09-24T14:25:37/-PT279M")]
        [TestCase("2020-09-24T14:25:37/PT23S")]
        [TestCase("2020-09-24T14:25:37/-PT279S")]
        [TestCase("2020-09-24T14:25:37/PT423.123S")]
        [TestCase("2020-09-24T14:25:37/PT23H48M")]
        [TestCase("2020-09-24T14:25:37/PT23H48M231S")]
        [TestCase("2020-09-24T14:25:37/PT23H48M231.45S")]
        [TestCase("2020-09-24T14:25:37/P21Y345M87DT23H48M231.45S")]
        [TestCase("2020-09-24T14:25:37/P87DT23H48M231.45S")]
        [TestCase("2020-09-24T14:25:37Z/P21Y345M87DT23H48M231.45S")]
        [TestCase("2020-09-24T14:25:37+03:30/P21Y345M87DT23H48M231.45S")]
        [TestCase("2020-09-24T14:25:37-14:00/P21Y345M87DT23H48M231.45S")]
        [TestCase("2020-09-24Z/P21Y345M87DT23H48M231.45S")]
        [TestCase("2020-09-24+03:30/P21Y345M87DT23H48M231.45S")]
        [TestCase("2020-09-24-14:00/P21Y345M87DT23H48M231.45S")]
        [TestCase("2020-09-24T24:00:00/-P345DT23H")]
        [TestCase("0896-09-24T14:25:37/P345DT23H")]
        [TestCase("108966-09-24T14:25:37Z/P345DT23H")]
        [TestCase("P21Y345M87DT23H48M231.45S",false)]
        [TestCase("/P21Y345M87DT23H48M231.45S", false)]
        [TestCase("2020/P21Y345M87DT23H48M231.45S", false)]
        [TestCase("2020-09-24T14:25:37", false, true)]
        [TestCase("2020-09-24T14:25:37.123ZP1Y",false)]
        [TestCase("896-09-24T14:25:37Z", false)]
        [TestCase("T14:25:37Z", false)]
        [TestCase("2020", false, true)]
        [TestCase("2020-09", false, true)]
        [TestCase("2020-09-24", false, true)]
        [TestCase("2020-09-24Z", false, true)]
        [TestCase("2020-09-24+14:00", false, true)]
        [TestCase("123.90890", false, true)]
        [TestCase("0", false, true)]
        [TestCase("Not valid", false, true)]
        [TestCase("", true, true)]
        [TestCase(null, true, true)]
        [TestCase("NaN", false)]
        public void TimeRangeValidatorTest(string observationValue, bool isValid = true, bool skipTestOfSuperSetTypes = false)
        {
            TypeValidatorTest(TextEnumType.TimesRange, observationValue, isValid ? ValidationErrorType.Undefined : ValidationErrorType.InvalidValueFormatNotTimeRange);

            if (skipTestOfSuperSetTypes) return;

            TypeValidatorTest(TextEnumType.ObservationalTimePeriod, observationValue, isValid ? ValidationErrorType.Undefined : ValidationErrorType.InvalidValueFormatNotObservationalTimePeriod);
        }

        [TestCase("--09")]
        [TestCase("--12Z")]
        [TestCase("--07-14:00")]
        [TestCase("--09+02:30")]
        [TestCase("09", false)]
        [TestCase("9", false)]
        [TestCase("--1", false)]
        [TestCase("2020-09", false)]
        [TestCase("2020-09-24", false)]
        [TestCase("2020-09-24+14:30", false)]
        [TestCase("2020-M11", false)]
        [TestCase("123.90890", false)]
        [TestCase("0", false)]
        [TestCase("Not valid", false)]
        [TestCase("", true)]
        [TestCase(null, true)]
        [TestCase("NaN", false)]
        public void MonthValidatorTest(string observationValue, bool isValid = true)
        {
            TypeValidatorTest(TextEnumType.Month, observationValue, isValid ? ValidationErrorType.Undefined : ValidationErrorType.InvalidValueFormatNotMonth);
        }

        [TestCase("--07-11")]
        [TestCase("--12-21Z")]
        [TestCase("--07-31-14:00")]
        [TestCase("--09-20+02:30")]
        [TestCase("--09-32",false)]
        [TestCase("--09", false)]
        [TestCase("--09-00", false)]
        [TestCase("---09-30", false)]
        [TestCase("-9", false)]
        [TestCase("---1-1", false)]
        [TestCase("2020-09", false)]
        [TestCase("2020-09-24", false)]
        [TestCase("2020-09-24+14:30", false)]
        [TestCase("2020-M11", false)]
        [TestCase("123.90890", false)]
        [TestCase("0", false)]
        [TestCase("Not valid", false)]
        [TestCase("", true)]
        [TestCase(null, true)]
        [TestCase("NaN", false)]
        public void MonthDayValidatorTest(string observationValue, bool isValid = true)
        {
            TypeValidatorTest(TextEnumType.MonthDay, observationValue, isValid ? ValidationErrorType.Undefined : ValidationErrorType.InvalidValueFormatNotMonthDay);
        }

        [TestCase("---11")]
        [TestCase("---21Z")]
        [TestCase("---01-14:00")]
        [TestCase("---20+02:30")]
        [TestCase("--09-30", false)]
        [TestCase("---9", false)]
        [TestCase("---32", false)]
        [TestCase("----09", false)]
        [TestCase("-9", false)]
        [TestCase("2020-09-24", false)]
        [TestCase("2020-D111", false)]
        [TestCase("123", false)]
        [TestCase("123.90890", false)]
        [TestCase("0", false)]
        [TestCase("Not valid", false)]
        [TestCase("", true)]
        [TestCase(null, true)]
        [TestCase("NaN", false)]
        public void DayValidatorTest(string observationValue, bool isValid = true)
        {
            TypeValidatorTest(TextEnumType.Day, observationValue, isValid ? ValidationErrorType.Undefined : ValidationErrorType.InvalidValueFormatNotDay);
        }

        [TestCase("14:25:37")]
        [TestCase("14:25:37Z")]
        [TestCase("14:25:37.123Z")]
        [TestCase("24:00:00")]
        [TestCase("14:25:37-14:00")]
        [TestCase("14:25:37+02:30")]
        [TestCase("14:25:37.123+02:30")]
        [TestCase("2020-09-24T14:25:37Z", false)]
        [TestCase("T14:25:37Z", false)]
        [TestCase("2020", false)]
        [TestCase("06:10:11AM", false)]
        [TestCase("2020-09-24+14:00", false)]
        [TestCase("123.90890", false)]
        [TestCase("0", false)]
        [TestCase("Not valid", false)]
        [TestCase("", true)]
        [TestCase(null, true)]
        [TestCase("NaN", false)]
        public void TimeValidatorTest(string observationValue, bool isValid = true)
        {
            TypeValidatorTest(TextEnumType.Time, observationValue, isValid ? ValidationErrorType.Undefined : ValidationErrorType.InvalidValueFormatNotTime);
        }

        [TestCase("P1Y")]
        [TestCase("-P32Y")]
        [TestCase("P3M")]
        [TestCase("-P287M")]
        [TestCase("P1D")]
        [TestCase("-P427D")]
        [TestCase("P1Y4M")]
        [TestCase("P1Y88D")]
        [TestCase("P23M88D")]
        [TestCase("PT23H")]
        [TestCase("-PT279H")]
        [TestCase("PT23M")]
        [TestCase("-PT279M")]
        [TestCase("PT23S")]
        [TestCase("-PT279S")]
        [TestCase("PT423.123S")]
        [TestCase("PT23H48M")]
        [TestCase("PT23H48M231S")]
        [TestCase("PT23H48M231.45S")]
        [TestCase("P21Y345M87DT23H48M231.45S")]
        [TestCase("P87DT23H48M231.45S")]
        [TestCase("1Y", false)]
        [TestCase("P1Y23D48M", false)]
        [TestCase("T14H", false)]
        [TestCase("2020", false)]
        [TestCase("06:10:11AM", false)]
        [TestCase("2020-09-24+14:00", false)]
        [TestCase("123.90890", false)]
        [TestCase("0", false)]
        [TestCase("Not valid", false)]
        [TestCase("", true)]
        [TestCase(null, true)]
        [TestCase("NaN", false)]
        public void DurationValidatorTest(string observationValue, bool isValid = true)
        {
            TypeValidatorTest(TextEnumType.Duration, observationValue, isValid ? ValidationErrorType.Undefined : ValidationErrorType.InvalidValueFormatNotDuration);
        }

        [Test]
        public void NotSupportedTypesTest()
        {
            var notSupportedTextTypes = new[]
                {TextEnumType.ExclusiveValueRange, TextEnumType.InclusiveValueRange, TextEnumType.Incremental};

            foreach (var textType in notSupportedTextTypes)
            {
                var ex = Assert.Throws<DotStatException>(() =>
                    TypeValidatorTest(textType, null, ValidationErrorType.Undefined));

                StringAssert.Contains($"OECD:OBS_TYPE_DSD_{textType.ToString().ToUpper()}(11.0).OBS_VALUE", ex.Message);
                StringAssert.Contains(textType.ToString(), ex.Message);
            }
        }

        [TestCase("1")]
        [TestCase("19")]
        [TestCase("01", false)]
        [TestCase("InvalidCode", false)]
        [TestCase("  ", false)]
        [TestCase("")]
        [TestCase(null)]
        [TestCase("NaN", false)]
        public void EnumeratedValidatorTest(string observationValue, bool isValidExpected = true)
        {
            var expectedValidationError = isValidExpected
                ? ValidationErrorType.Undefined
                : ValidationErrorType
                    .InvalidValueFormatUnknownCodeMember;
            var sdmxObjects = _mappingStoreDataAccess.GetSdmxObjects("sdmx/OBS_TYPE_TEST.xml");

            var dataflowsToRemove = sdmxObjects.Dataflows.Where(df => !df.Id.Equals("OBS_TYPE_DF_ENUMERATED")).ToArray();
            foreach (var df2Remove in dataflowsToRemove)
            {
                sdmxObjects.RemoveDataflow(df2Remove);
            }
            
            var dsdId = "OBS_TYPE_DSD_ENUMERATED";
            var dsdsToRemove = sdmxObjects.DataStructures.Where(d => !d.Id.Equals(dsdId)).ToArray();
            foreach (var dsd2Remove in dsdsToRemove)
            {
                sdmxObjects.RemoveDataStructure(dsd2Remove);
            }

            var dataflow = SdmxParser.ParseDataFlowStructure(sdmxObjects);
            var dsd = dataflow.Dsd;
            
            Assert.IsNotNull(dsd, $"DSD {dsdId} not found.");

            var primaryMeasure = dsd.PrimaryMeasure;

            Assert.IsNull(primaryMeasure.TextFormat);
            Assert.IsNotNull(primaryMeasure.RepresentationReference);
            Assert.IsNotNull(primaryMeasure.Codelist);
            Assert.AreEqual("OECD:CL_MEASURE(11.0)", primaryMeasure.Codelist.FullId);

            var validators = _typeValidatorBuilder.BuildFromPrimaryMeasure(primaryMeasure, false, CultureInfo.InvariantCulture).ToList();

            Assert.AreNotEqual(0, validators.Count, $"No validators built for dsd {dsdId}");

            var isValid = true;
            var validationErrorTypes = new List<ValidationErrorType>();

            foreach (var validator in validators)
            {
                isValid = isValid && validator.IsValid(observationValue);

                validationErrorTypes.Add(validator.ValidationError);
            }

            Assert.AreEqual(isValidExpected, isValid,
                $"For dsd {dsdId} validation result of [{observationValue}] is {isValid} but {isValidExpected} was expected.");

                Assert.Contains(expectedValidationError, validationErrorTypes,
                    $"For dsd {dsdId} expected validation error of [{observationValue}] is {expectedValidationError} but it's not among validation errors received: [{string.Join(",", validationErrorTypes.Select(e => e.ToString()))}]");
        }

        [TestCase("-12345678901234567890123456789012345.678", ValidationErrorType.InvalidValueFormatMinValue)]
        [TestCase("12345678901234567890123456789012345.678", ValidationErrorType.InvalidValueFormatMaxValue)]
        [TestCase("123.90890", ValidationErrorType.InvalidValueFormatMaxValue)]
        [TestCase("-100", ValidationErrorType.InvalidValueFormatMinValue)]
        [TestCase("100", ValidationErrorType.InvalidValueFormatMaxValue)]
        [TestCase("-23.9767", ValidationErrorType.InvalidValueFormatPatternMismatch)]
        [TestCase(".8900")]
        [TestCase("0")]
        [TestCase("-20.435324523")]
        [TestCase("Not valid", ValidationErrorType.InvalidValueFormatNotDecimal)]
        [TestCase("")]
        [TestCase(null)]
        [TestCase("NaN", ValidationErrorType.InvalidValueFormatNotDecimal)]
        [TestCase("NAN", ValidationErrorType.InvalidValueFormatNotDecimal)]
        [TestCase("NaN123", ValidationErrorType.InvalidValueFormatNotDecimal)]
        [TestCase("123NaN", ValidationErrorType.InvalidValueFormatNotDecimal)]
        [TestCase("123NaN123", ValidationErrorType.InvalidValueFormatNotDecimal)]
        public void DecimalValidatorMinMaxTest(string observationValue, ValidationErrorType expectedValidationError = ValidationErrorType.Undefined)
        {
            // Representation: <structure:TextFormat textType="Decimal" minValue="-99.9" maxValue="99.9" pattern="^.*[0].*$"/>
            TypeValidatorTest(TextEnumType.Decimal, observationValue, expectedValidationError, true);
        }

        [TestCase("-3.40E+38", ValidationErrorType.InvalidValueFormatMinValue)]
        [TestCase("-100", ValidationErrorType.InvalidValueFormatMinValue)]
        [TestCase("100", ValidationErrorType.InvalidValueFormatMaxValue)]
        [TestCase("+1.18E-38", ValidationErrorType.InvalidValueFormatPatternMismatch)]
        [TestCase("11.9999838",  ValidationErrorType.InvalidValueFormatPatternMismatch)]
        [TestCase("3.41E+138", ValidationErrorType.InvalidValueFormatNotFloat)]
        [TestCase("NaN", ValidationErrorType.InvalidValueFormatNotFloat)]
        [TestCase("INF",  ValidationErrorType.InvalidValueFormatNotFloat)]
        [TestCase("-INF", ValidationErrorType.InvalidValueFormatNotFloat)]
        [TestCase(".8900")]
        [TestCase("0")]
        [TestCase("-20.435324523")]
        [TestCase("-2E-02")]
        [TestCase("Not valid", ValidationErrorType.InvalidValueFormatNotFloat)]
        [TestCase("")]
        [TestCase(null)]
        [TestCase("NAN", ValidationErrorType.InvalidValueFormatNotFloat)]
        [TestCase("NaN123", ValidationErrorType.InvalidValueFormatNotFloat)]
        [TestCase("123NaN", ValidationErrorType.InvalidValueFormatNotFloat)]
        [TestCase("123NaN123", ValidationErrorType.InvalidValueFormatNotFloat)]
        public void FloatValidatorMinMaxTest(string observationValue, ValidationErrorType expectedValidationError = ValidationErrorType.Undefined)
        {
            // Representation: <structure:TextFormat textType="Float" minValue="-99.9" maxValue="99.9" pattern="^.*[0].*$"/>
            TypeValidatorTest(TextEnumType.Float, observationValue, expectedValidationError, true);
        }

        [TestCase("-2147483648", ValidationErrorType.InvalidValueFormatMinValue)]
        [TestCase("101", ValidationErrorType.InvalidValueFormatMaxValue)]
        [TestCase("2147483648", ValidationErrorType.InvalidValueFormatNotInteger)]
        [TestCase("89", ValidationErrorType.InvalidValueFormatPatternMismatch)]
        [TestCase("20")]
        [TestCase("0")]
        [TestCase("-20")]
        [TestCase("2.5", ValidationErrorType.InvalidValueFormatNotInteger)]
        [TestCase("Not valid", ValidationErrorType.InvalidValueFormatNotInteger)]
        [TestCase("")]
        [TestCase(null)]
        [TestCase("NaN", ValidationErrorType.InvalidValueFormatNotInteger)]
        [TestCase("NAN", ValidationErrorType.InvalidValueFormatNotInteger)]
        [TestCase("NaN123", ValidationErrorType.InvalidValueFormatNotInteger)]
        [TestCase("123NaN", ValidationErrorType.InvalidValueFormatNotInteger)]
        [TestCase("123NaN123", ValidationErrorType.InvalidValueFormatNotInteger)]
        public void IntegerValidatorMinMaxTest(string observationValue, ValidationErrorType expectedValidationError = ValidationErrorType.Undefined)
        {
            // Representation: <structure:TextFormat textType="Integer" minValue="-100" maxValue="100" pattern="^.*[0].*$"/>
            TypeValidatorTest(TextEnumType.Integer, observationValue, expectedValidationError, true);
        }

        [TestCase("AA0as")]
        [TestCase("1A.as", ValidationErrorType.InvalidValueFormatPatternMismatch)]
        [TestCase("1", ValidationErrorType.InvalidValueFormatMinLength)]
        [TestCase("Too long, so not valid", ValidationErrorType.InvalidValueFormatMaxLength)]
        [TestCase("")]
        [TestCase(null)]
        public void StringValidatorMinMaxTest(string observationValue, ValidationErrorType expectedValidationError = ValidationErrorType.Undefined)
        {
            TypeValidatorTest(TextEnumType.String, observationValue, expectedValidationError, true);
        }

        private void TypeValidatorTest(TextEnumType textType, string observationValue,
            ValidationErrorType expectedValidationError, bool isMinMaxValidation = false, bool supportIntentionallyMissingValues = false, string cultureInfo = null)
        {
            var sdmxObjects = _mappingStoreDataAccess.GetSdmxObjects(supportIntentionallyMissingValues ? "sdmx/OBS_TYPE_TEST_with_intentionally_missing_annotation.xml" : "sdmx/OBS_TYPE_TEST.xml");

            var dsdId = "OBS_TYPE_DSD_" + (isMinMaxValidation ? "MINMAX_" : "") + (textType == TextEnumType.TimesRange ? "TIMERANGE" : textType.ToString().ToUpper());
            var dsd = new Dsd(sdmxObjects.DataStructures.FirstOrDefault(d => d.Id.Equals(dsdId, StringComparison.InvariantCultureIgnoreCase)));

            Assert.IsNotNull(dsd, $"DSD {dsdId} not found.");

            var primaryMeasure = new PrimaryMeasure(dsd.Base.PrimaryMeasure);

            Assert.IsNotNull(primaryMeasure.TextFormat);
            Assert.AreEqual(textType, primaryMeasure.TextFormat.TextType.EnumType, $"Type of primary measure is not as expected for dsd related to type {textType}.");

            var validators = _typeValidatorBuilder.BuildFromPrimaryMeasure(primaryMeasure, supportIntentionallyMissingValues, string.IsNullOrEmpty(cultureInfo) ? CultureInfo.InvariantCulture : new CultureInfo(cultureInfo)).ToList();

            if (textType == TextEnumType.String && !isMinMaxValidation)
            {
                Assert.AreEqual(0, validators.Count, $"No validators built for type {textType}");
            }
            else
            {
                Assert.AreNotEqual(0, validators.Count, $"No validators built for type {textType}");

                var isValid = true;
                var validationErrorTypes = new List<ValidationErrorType>();
                foreach (var validator in validators)
                {
                    isValid = isValid && validator.IsValid(observationValue);

                    validationErrorTypes.Add(validator.ValidationError);
                }

                var isValidExpected = expectedValidationError == ValidationErrorType.Undefined;
                Assert.AreEqual(isValidExpected, isValid,
                    $"For type {textType} validation result of [{observationValue}] is {isValid} but {isValidExpected} was expected.");

                Assert.Contains(expectedValidationError, validationErrorTypes,
                    $"For type {textType} expected validation error of [{observationValue}] is {expectedValidationError} but it's not among validation errors received: [{string.Join(",", validationErrorTypes.Select(e => e.ToString()))}]");
            }
        }
    }
}
