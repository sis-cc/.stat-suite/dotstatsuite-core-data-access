﻿using DotStat.Common.Exceptions;
using DotStat.Db.DB;
using DotStat.Db.Dto;
using DotStat.Db.Exception;
using DotStat.DB.Repository.MariaDb;
using DotStat.Domain;
using DotStat.Test.Moq;
using Moq;
using NUnit.Framework;
using Org.Sdmxsource.Sdmx.Api.Constants;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Dataflow = DotStat.Domain.Dataflow;

namespace DotStat.Test.DataAccess.MariaDb.Unit.Repository
{
    [TestFixture, Parallelizable(ParallelScope.Self)]
    public sealed class SqlComponentRepositoryTests : SdmxUnitTestBase
    {
        private readonly MariaDbComponentRepository _sqlComponentRepository;
        private readonly Mock<MariaDbDotStatDb> _dotStatDbMock;

        public SqlComponentRepositoryTests()
        {
            _dotStatDbMock = new Mock<MariaDbDotStatDb>(Configuration.SpacesInternal.FirstOrDefault(), DbUp.DatabaseVersion.DataDbVersion) { CallBase = true };
            _dotStatDbMock.Setup(m => m.DataSchema).Returns("data");
            _dotStatDbMock.Setup(m => m.ManagementSchema).Returns("management");

            _sqlComponentRepository = new MariaDbComponentRepository(_dotStatDbMock.Object, Configuration);
        }

        [TestCase]
        public async Task GetComponentDbId()
        {
            var dataflow = GetDataflow();
            dataflow.Dsd.DbId = 1;
            var dbId = 1;

            //Measures
            var compItem = new ComponentItem
            {
                DbId = dbId++,
                Id = dataflow.Dsd.PrimaryMeasure.Code,
                Type = dataflow.Dsd.PrimaryMeasure.DbType,
                DsdId = 1,
                CodelistId = dataflow.Dsd.PrimaryMeasure.Base.HasCodedRepresentation() ? dbId++ : null
            };

            SetDotStatDbMock(new List<ComponentItem> { compItem });
            var compDbId = await _sqlComponentRepository.GetComponentDbId(dataflow.Dsd.PrimaryMeasure, dataflow.Dsd.DbId, CancellationToken);
            Assert.AreEqual(compItem.DbId, compDbId);

            //Dimensions
            foreach (var dim in dataflow.Dsd.Dimensions)
            {
                compItem = new ComponentItem
                {
                    DbId = dbId++,
                    Id = dim.Code,
                    Type = dim.DbType,
                    DsdId = 1,
                    CodelistId = dim.Base.HasCodedRepresentation()? dbId++ : null
                };

                SetDotStatDbMock(new List<ComponentItem> { compItem });
                compDbId = await _sqlComponentRepository.GetComponentDbId(dim, dataflow.Dsd.DbId, CancellationToken);
                Assert.AreEqual(compItem.DbId, compDbId);
            }

            //Attributes
            foreach (var attr in dataflow.Dsd.Attributes)
            {
                compItem = new ComponentItem
                {
                    DbId = dbId++,
                    Id = attr.Code,
                    Type = attr.DbType,
                    DsdId = 1,
                    CodelistId = attr.Base.HasCodedRepresentation() ? dbId++ : null
                };

                SetDotStatDbMock(new List<ComponentItem> { compItem });
                compDbId = await _sqlComponentRepository.GetComponentDbId(attr, dataflow.Dsd.DbId, CancellationToken);
                Assert.AreEqual(compItem.DbId, compDbId);
            }            
        }

        [TestCase]
        public async Task GetComponentDbIdNotInDb()
        {
            var dataflow = GetDataflow();
            dataflow.Dsd.DbId = 1;
            SetDotStatDbMock(new List<ComponentItem>());

            var compDbId = await _sqlComponentRepository.GetComponentDbId(dataflow.Dsd.Dimensions[0], dataflow.Dsd.DbId, CancellationToken);
            Assert.AreEqual(-1, compDbId);

            Assert.ThrowsAsync<ArtefactNotFoundException>(async () =>
                await _sqlComponentRepository.GetComponentDbId(dataflow.Dsd.Dimensions[0], dataflow.Dsd.DbId, CancellationToken, true));
        }

        [TestCase]
        public async Task GetComponentDsdDbIdNotInitialized()
        {
            var dataflow = GetDataflow();
            dataflow.Dsd.DbId = -1;
            var dbId = 1;

            //Measures
            var compItem = new ComponentItem
            {
                DbId = dbId++,
                Id = dataflow.Dsd.PrimaryMeasure.Code,
                Type = dataflow.Dsd.PrimaryMeasure.DbType,
                DsdId = 1,
                CodelistId = dataflow.Dsd.PrimaryMeasure.Base.HasCodedRepresentation() ? dbId++ : null
            };
            SetDotStatDbMock(new List<ComponentItem> { compItem });

            var compDbId = await _sqlComponentRepository.GetComponentDbId(dataflow.Dsd.PrimaryMeasure, dataflow.Dsd.DbId, CancellationToken);
            Assert.AreEqual(-1, compDbId);
        }

        [TestCase]
        public void GetComponentUnknownComponent()
        {
            var dataflow = GetDataflow("sdmx/CsvV2.xml");

            //Referential Metadata Attribute
            Assert.ThrowsAsync<DotStatException>(async ()=> 
                await _sqlComponentRepository.GetComponentDbId(
                    dataflow.Dsd.Msd.MetadataAttributes[0], 1, CancellationToken));

            //Dsd
            Assert.ThrowsAsync<DotStatException>(async () =>
                await _sqlComponentRepository.GetComponentDbId(dataflow.Dsd, 1, CancellationToken));

            //Dataflow
            Assert.ThrowsAsync<DotStatException>(async () =>
                await _sqlComponentRepository.GetComponentDbId(dataflow, 1, CancellationToken));
        }

        [TestCase]
        public async Task GetComponentsOfDsd()
        {
            var dataflow = GetDataflow();
            dataflow.Dsd.DbId = 1;
            var dbId = 1;
            var mockComponents = new List<ComponentItem>();
            //Measures
            var compItem = new ComponentItem
            {
                DbId = dbId++,
                Id = dataflow.Dsd.PrimaryMeasure.Code,
                Type = dataflow.Dsd.PrimaryMeasure.DbType,
                DsdId = 1,
                CodelistId = dataflow.Dsd.PrimaryMeasure.Base.HasCodedRepresentation() ? dbId++ : null
            };
            mockComponents.Add(compItem);

            //Dimensions
            foreach (var dim in dataflow.Dsd.Dimensions)
            {
                compItem = new ComponentItem
                {
                    DbId = dbId++,
                    Id = dim.Code,
                    Type = dim.DbType,
                    DsdId = 1,
                    CodelistId = dim.Base.HasCodedRepresentation() ? dbId++ : null
                };
                mockComponents.Add(compItem);
            }

            //Attributes
            foreach (var attr in dataflow.Dsd.Attributes)
            {
                compItem = new ComponentItem
                {
                    DbId = dbId++,
                    Id = attr.Code,
                    Type = attr.DbType,
                    DsdId = 1,
                    CodelistId = attr.Base.HasCodedRepresentation() ? dbId++ : null
                };
                mockComponents.Add(compItem);
            }

            SetDotStatDbMock(mockComponents);
            var componentsInDb = await _sqlComponentRepository.GetComponentsOfDsd( dataflow.Dsd, CancellationToken);
            
            Assert.AreEqual(mockComponents.Count, componentsInDb.Count);

            var i = 0;
            foreach(var component in mockComponents)
            {
                Assert.AreEqual(component.DbId, componentsInDb[i].DbId);
                Assert.AreEqual(component.Id, componentsInDb[i].Id);
                Assert.AreEqual(component.Type, componentsInDb[i].Type);
                Assert.AreEqual(component.Id, componentsInDb[i].Id);
                Assert.AreEqual(component.DsdId, componentsInDb[i].DsdId);
                Assert.AreEqual(component.CodelistId, componentsInDb[i].CodelistId);
                i++;
            }
        }

        [TestCase]
        public void GetComponentsOfDsdDbIdNotInitialized()
        {
            var dataflow = GetDataflow();
            dataflow.Dsd.DbId = -1;
            var dbId = 1;

            //Measures
            var compItem = new ComponentItem
            {
                DbId = dbId++,
                Id = dataflow.Dsd.PrimaryMeasure.Code,
                Type = dataflow.Dsd.PrimaryMeasure.DbType,
                DsdId = 1,
                CodelistId = dataflow.Dsd.PrimaryMeasure.Base.HasCodedRepresentation() ? dbId++ : null
            };
            SetDotStatDbMock(new List<ComponentItem> { compItem });

            Assert.ThrowsAsync<ArgumentException>(async () =>
                await _sqlComponentRepository.GetComponentsOfDsd(dataflow.Dsd, CancellationToken));
        }

        [TestCase]
        public async Task GetAllComponents()
        {
            var dbId = 1;
            var mockComponents = new List<ComponentItem>();

            var dataflows = new List<Dataflow>
            {
                GetDataflow(),
                GetDataflow("sdmx/KH_NIS,DF_AGRI_NO_TIME,2.0.xml")
            };

            foreach (var dataflow in dataflows)
            {
                dataflow.Dsd.DbId = dbId++;

                //Measures
                var compItem = new ComponentItem
                {
                    DbId = dbId++,
                    Id = dataflow.Dsd.PrimaryMeasure.Code,
                    Type = dataflow.Dsd.PrimaryMeasure.DbType,
                    DsdId = 1,
                    CodelistId = dataflow.Dsd.PrimaryMeasure.Base.HasCodedRepresentation() ? dbId++ : null
                };
                mockComponents.Add(compItem);

                //Dimensions
                foreach (var dim in dataflow.Dsd.Dimensions)
                {
                    compItem = new ComponentItem
                    {
                        DbId = dbId++,
                        Id = dim.Code,
                        Type = dim.DbType,
                        DsdId = 1,
                        CodelistId = dim.Base.HasCodedRepresentation() ? dbId++ : null
                    };
                    mockComponents.Add(compItem);
                }

                //Attributes
                foreach (var attr in dataflow.Dsd.Attributes)
                {
                    compItem = new ComponentItem
                    {
                        DbId = dbId++,
                        Id = attr.Code,
                        Type = attr.DbType,
                        DsdId = 1,
                        CodelistId = attr.Base.HasCodedRepresentation() ? dbId++ : null,
                        AttributeAssignmentlevel = attr.Base.AttachmentLevel,
                        AttributeStatus = attr.Base.Mandatory ? 
                            AttributeAssignmentStatus.Mandatory : AttributeAssignmentStatus.Conditional,
                        EnumId = 1,
                        AttributeGroup = attr.Base.AttachmentGroup,
                        MinLength = 1,
                        MaxLength = 100,
                        Pattern = "pattern",
                        MinValue = 2,
                        MaxValue = 200
                    };
                    mockComponents.Add(compItem);
                }
            }
            SetDotStatDbMock(mockComponents);

            var allComponentsInDb = await _sqlComponentRepository.GetAllComponents(CancellationToken);

            Assert.AreEqual(mockComponents.Count, allComponentsInDb.Count);

            var i = 0;
            foreach (var component in mockComponents)
            {
                Assert.AreEqual(component.DbId, allComponentsInDb[i].DbId);
                Assert.AreEqual(component.Id, allComponentsInDb[i].Id);
                Assert.AreEqual(component.Type, allComponentsInDb[i].Type);
                Assert.AreEqual(component.Id, allComponentsInDb[i].Id);
                Assert.AreEqual(component.DsdId, allComponentsInDb[i].DsdId);
                Assert.AreEqual(component.CodelistId, allComponentsInDb[i].CodelistId);
                Assert.AreEqual(component.AttributeAssignmentlevel, allComponentsInDb[i].AttributeAssignmentlevel);
                Assert.AreEqual(component.AttributeStatus, allComponentsInDb[i].AttributeStatus);
                Assert.AreEqual(component.EnumId, allComponentsInDb[i].EnumId);
                Assert.AreEqual(component.AttributeGroup, allComponentsInDb[i].AttributeGroup);
                Assert.AreEqual(component.MinLength, allComponentsInDb[i].MinLength);
                Assert.AreEqual(component.MaxLength, allComponentsInDb[i].MaxLength);
                Assert.AreEqual(component.Pattern, allComponentsInDb[i].Pattern);
                Assert.AreEqual(component.MinValue, allComponentsInDb[i].MinValue);
                Assert.AreEqual(component.MaxValue, allComponentsInDb[i].MaxValue);
                i++;
            }
        }

        [TestCase("sdmx/264D_264_SALDI2_ATTRIBUTE_TEST.xml")]
        [TestCase("sdmx/KH_NIS,DF_AGRI_NO_TIME,2.0.xml")]
        public async Task CheckTimeDimensionOfDsdInDb(string file)
        {
            var dataflow = GetDataflow(file);
            var hasTimeDim = dataflow.Dsd.Base.TimeDimension != null;

            _dotStatDbMock.Setup(m =>
                m.ColumnExists(
                    It.IsAny<string>(),
                    It.IsAny<string>(),
                    It.IsAny<CancellationToken>()))
                .Returns(() => Task.FromResult(hasTimeDim));

            Assert.AreEqual(hasTimeDim, await _sqlComponentRepository.CheckTimeDimensionOfDsdInDb(dataflow.Dsd, DbTableVersion.A, CancellationToken));
        }

        private void SetDotStatDbMock(IList<ComponentItem> components)
        {
            _dotStatDbMock.Reset();
            var componentsInDb = new List<object[]>();
            if (components != null)
            {
                for (var i = 0; i < components.Count; i++)
                {
                    componentsInDb.Add(new object[] {
                        components[i].DbId,
                        components[i].Id,
                        components[i].Type,
                        components[i].DsdId,
                        components[i].CodelistId,
                        components[i].AttributeAssignmentlevel.ToString(),
                        components[i].AttributeStatus.ToString(),
                        components[i].EnumId,
                        components[i].AttributeGroup,
                        components[i].MinLength,
                        components[i].MaxLength,
                        components[i].Pattern,
                        components[i].MinValue,
                        components[i].MaxValue });
                }
            }

            DbDataReader dbDataReader = new TestDbDataReader(componentsInDb);

            _dotStatDbMock.Setup(m =>
                m.ExecuteReaderSqlWithParamsAsync(
                    It.IsAny<string>(),
                    It.IsAny<CancellationToken>(),
                    It.IsAny<CommandBehavior>(),
                    It.IsAny<bool>(),
                    It.IsAny<DbParameter[]>()))
                .Returns(() => Task.FromResult(dbDataReader));
        }
    }
}