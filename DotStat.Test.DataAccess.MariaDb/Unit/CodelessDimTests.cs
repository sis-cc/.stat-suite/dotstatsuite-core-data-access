﻿using System.Linq;
using System.Threading.Tasks;
using DotStat.Domain;
using NUnit.Framework;
using Org.Sdmxsource.Sdmx.Api.Constants;

namespace DotStat.Test.DataAccess.MariaDb.Unit
{
    [TestFixture, Parallelizable(ParallelScope.All)]
    public sealed class CodelessDimTests : SdmxUnitTestBase
    {
        public CodelessDimTests() :base("sdmx/TEST_CODELESS_DS.xml")
        {}

        [Test]
        public void ParseDataflow()
        {
            var df = this.GetDataflow();

            Assert.IsNotNull(df);
            Assert.AreEqual(3, df.Dimensions.Count);

            var dim = df.Dimensions.First(x => x.Code == "DIM_2");

            Assert.IsNull(dim.Codelist);
            Assert.IsFalse(dim.Base.HasCodedRepresentation());
            Assert.AreEqual(TextEnumType.String, dim.Base.Representation.TextFormat.TextType.EnumType);
        }

        [Test]
        public async Task GenerateObservations()
        {
            var observations = await ObservationGenerator.Generate(
                    this.GetDataflow().Dsd,
                this.GetDataflow(),
                true,
                2020,
                2020,
                10,
                action: StagingRowActionEnum.Merge
            )
            .ToArrayAsync();

            Assert.AreEqual(10, observations.Length);
        }
    }
}
