﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlTypes;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using DotStat.Common.Configuration.Dto;
using DotStat.Common.Configuration.Interfaces;
using DotStat.DB;
using DotStat.Db.DB;
using DotStat.Db.Repository;
using DotStat.DB.Repository;
using DotStat.DB.Repository.MariaDb;
using DotStat.Db.Service;
using DotStat.Domain;
using DotStat.Domain.Cache;
using DotStat.MappingStore;
using DryIoc;
using Estat.Sri.Mapping.Api.Engine;
using Estat.Sri.Mapping.Api.Factory;
using Estat.Sri.Mapping.Api.Manager;
using Estat.Sri.Mapping.Api.Model;
using Estat.Sri.Mapping.Api.Utils;
using Estat.Sri.Mapping.MappingStore.Manager;
using Estat.Sri.MappingStoreRetrieval.Factory;
using Estat.Sri.MappingStoreRetrieval.Helper;
using Estat.Sri.Plugin.MySql.Engine;
using NUnit.Framework;
using Org.Sdmxsource.Sdmx.Api.Manager.Parse;
using Org.Sdmxsource.Sdmx.Api.Model;
using Org.Sdmxsource.Sdmx.Structureparser.Manager.Parsing;
using Estat.Sri.Plugin.MySql.Factory;

namespace DotStat.Test.DataAccess.MariaDb.Unit
{
    // todo make a real integration test

    [TestFixture, Explicit]
    public sealed class MappingStoreDataAccessTests : UnitTestBase
    {
        private readonly DataspaceInternal dataSpace;
        private MappingStoreDataAccess _mappingStoreDataAccess;
        private IDataStoreRepository _dataStoreRepository;
        private IMetadataStoreRepository _metadataStoreRepository;
        private readonly Dataflow Df;
        private readonly Dataflow DfWithMeta;

        public MappingStoreDataAccessTests()
        {
            dataSpace = Configuration.SpacesInternal.First();

            var assemblies = new[]
            {
               // typeof(IDatabaseProviderManager).Assembly,
                typeof(DatabaseManager).Assembly,
                typeof(IEntityRetrieverManager).Assembly
            };

            MappingStoreIoc.Container.Register<IRetrievalEngineContainerFactory, RetrievalEngineContainerFactory>(Reuse.Singleton);

            MappingStoreIoc.Container.RegisterMany(assemblies,
                type =>
                    !typeof(IEntity).IsAssignableFrom(type) && !typeof(Exception).IsAssignableFrom(type) 
                       && type != typeof(SingleRequestScope), 
                reuse: Reuse.Singleton, 
                made: FactoryMethod.ConstructorWithResolvableArguments
            );

            MappingStoreIoc.Container.Register<IDatabaseProviderEngine, MySqlDatabaseProviderEngine>();
            MappingStoreIoc.Container.Register<IDatabaseProviderFactory, MysqlDatabaseProviderFactory>();
            MappingStoreIoc.Container.Register<IStructureParsingManager, StructureParsingManager>(Reuse.Singleton, ifAlreadyRegistered: IfAlreadyRegistered.Keep, made: FactoryMethod.ConstructorWithResolvableArguments);
            MappingStoreIoc.Container.RegisterInstance<IConfigurationStoreManager>(new MappingStoreConnectionManager(Configuration), IfAlreadyRegistered.Replace);

            _mappingStoreDataAccess = new MappingStoreDataAccess(
                    Configuration,
                    MappingStoreIoc.Container.Resolve<IEntityPersistenceManager>(),
                    MappingStoreIoc.Container.Resolve<IEntityRetrieverManager>(),
                    MappingStoreIoc.Container.Resolve<IDatabaseProviderManager>(),
                    new MemoryCache()
                );

            var db = new MariaDbDotStatDb(dataSpace, SupportedDatabaseVersion.DataDbVersion);

            _dataStoreRepository = new MariaDbDataStoreRepository(db, Configuration);
            _metadataStoreRepository = new MariaDbMetadataStoreRepository(db, Configuration);

            Df = _mappingStoreDataAccess.GetDataflow(
                dataSpace.Id,
                "FCSA",
                "DF_CROPALL",
                "1.4.0",
                true,
                ResolveCrossReferences.ResolveExcludeAgencies
            );

            Df.DbId = 13;

            DfWithMeta = _mappingStoreDataAccess.GetDataflow(
                dataSpace.Id,
                "UNSD",
                "DF_JENS_DAILY",
                "1.0",
                true,
                ResolveCrossReferences.ResolveExcludeAgencies
            );

        }

        [Test]
        public void GetDataflow()
        {
            Assert.IsNotNull(Df);
            Assert.IsNull(Df.Dsd.Msd);

            //Get dataflow from base structure
            var dfLight = _mappingStoreDataAccess.GetDataflow(
                dataSpace.Id, 
                Df.Base, 
                Df.Dsd.Base, 
                Df.Dsd.Msd?.Base,
                ResolveCrossReferences.DoNotResolve,
                false,
                true
            );

            dfLight.DbId = Df.DbId;

            Assert.IsNotNull(dfLight);
            Assert.IsNull(dfLight.Dsd.Msd);
            Assert.AreEqual(Df.Dsd.Dimensions.Count, dfLight.Dsd.Dimensions.Count);
            Assert.AreEqual(Df.Dsd.Attributes.Count, dfLight.Dsd.Attributes.Count);
            Assert.AreEqual(Df.Dsd.Base.PrimaryMeasure.Id, dfLight.Dsd.Base.PrimaryMeasure.Id);

            var tableVersion = (char) DbTableVersion.A;

            var @paramsFull = _dataStoreRepository.GetMappingSetParams(Df, tableVersion);
            var @paramsLight = _dataStoreRepository.GetMappingSetParams(dfLight, tableVersion);

            Assert.AreEqual(@paramsFull.ActiveQuery, @paramsLight.ActiveQuery);
            Assert.AreEqual(@paramsFull.DeleteQuery, @paramsLight.DeleteQuery);
            Assert.AreEqual(@paramsFull.ReplaceQuery, @paramsLight.ReplaceQuery);
            Assert.AreEqual(@paramsFull.IncludeHistoryQuery, @paramsLight.IncludeHistoryQuery);
            Assert.AreEqual(@paramsFull.OrderByTimeAsc, @paramsLight.OrderByTimeAsc);
            Assert.AreEqual(@paramsFull.OrderByTimeDesc, @paramsLight.OrderByTimeDesc);
            Assert.AreEqual(@paramsFull.IsMetadata, @paramsLight.IsMetadata);
        }

        [Test]
        public void CheckMappingSet()
        {
            var hasMappingSet = _mappingStoreDataAccess.HasMappingSet(dataSpace.Id, Df);
            Console.WriteLine(hasMappingSet);
        }

        [Test]
        public void CreateMappingSet()
        {
            var tableVersion = DbTableVersion.A;
            DateTime? validFrom = null;
            DateTime? validTo = SqlDateTime.MaxValue.Value;

            var @params = _dataStoreRepository.GetMappingSetParams(Df, (char) tableVersion);
            
            var result = _mappingStoreDataAccess.CreateOrUpdateMappingSet(
                dataSpace.Id,
                Df,
                (char)tableVersion,
                tableVersion == DbTableVersion.A ? validFrom : validTo,
                tableVersion == DbTableVersion.A ? validTo : validFrom,
                @params
            );

            Assert.IsTrue(result);
        }

        [Test]
        public void DeleteMappingSet()
        {
            var result = _mappingStoreDataAccess.DeleteDataSetsAndMappingSets(dataSpace.Id, Df, false);
            Assert.IsTrue(result);
        }

        [Test]
        public void GetDataflowWithMsd()
        {
            Assert.IsNotNull(DfWithMeta);
            Assert.IsNotNull(DfWithMeta.Dsd.Msd);

            //Get dataflow from base structure
            var dfLight = _mappingStoreDataAccess.GetDataflow(
                dataSpace.Id,
                DfWithMeta.Base,
                DfWithMeta.Dsd.Base,
                DfWithMeta.Dsd.Msd?.Base,
                ResolveCrossReferences.DoNotResolve,
                false,
                true
            );

            dfLight.DbId = DfWithMeta.DbId;

            var tableVersion = (char)DbTableVersion.A;
            var @paramsFull = _metadataStoreRepository.GetMappingSetParams(DfWithMeta, tableVersion);
            var @paramsLight = _metadataStoreRepository.GetMappingSetParams(DfWithMeta, tableVersion);

            Assert.AreEqual(@paramsFull.ActiveQuery, @paramsLight.ActiveQuery);
            Assert.AreEqual(@paramsFull.DeleteQuery, @paramsLight.DeleteQuery);
            Assert.AreEqual(@paramsFull.ReplaceQuery, @paramsLight.ReplaceQuery);
            Assert.AreEqual(@paramsFull.IncludeHistoryQuery, @paramsLight.IncludeHistoryQuery);
            Assert.AreEqual(@paramsFull.OrderByTimeAsc, @paramsLight.OrderByTimeAsc);
            Assert.AreEqual(@paramsFull.OrderByTimeDesc, @paramsLight.OrderByTimeDesc);
            Assert.AreEqual(@paramsFull.IsMetadata, @paramsLight.IsMetadata);
        }

        [Test]
        public void CreateMappingSetMetadata()
        {
            var tableVersion = DbTableVersion.A;

            var @params = _metadataStoreRepository.GetMappingSetParams(DfWithMeta, (char) tableVersion);

            var result = _mappingStoreDataAccess.CreateOrUpdateMappingSet(
                dataSpace.Id,
                DfWithMeta,
                (char)tableVersion,
                null,
                null,
                @params
            );

            Assert.IsTrue(result);
        }

        [Test]
        public void DeleteMappingSetMeta()
        {
            var result = _mappingStoreDataAccess.DeleteDataSetsAndMappingSets(dataSpace.Id, DfWithMeta, false);
            Assert.IsTrue(result);
        }

        internal sealed class MappingStoreConnectionManager : Estat.Sri.Mapping.Api.Manager.IConfigurationStoreManager
        {
            private readonly IList<ConnectionStringSettings> _connections;

            public MappingStoreConnectionManager(IDataspaceConfiguration config)
            {
                _connections = config.SpacesInternal.Select(space => new ConnectionStringSettings(
                    $"DotStatSuiteCoreStructDb_{space.Id}",
                    space.DotStatSuiteCoreStructDbConnectionString,
                    "MySqlConnector"
                )).ToArray();
            }

            public IEnumerable<TSettings> GetSettings<TSettings>()
            {
                return typeof(TSettings) == typeof(ConnectionStringSettings)
                    ? (IEnumerable<TSettings>)_connections
                    : null;
            }

            public IActionResult SaveSettings<TSettings>(TSettings settings) => throw new NotImplementedException();
            public IActionResult DeleteSettings<TTettings>(string name) => throw new NotImplementedException();
        }

        [Test]
        public void TimeTrack()
        {
            var space = "qa:stable";

            var sw = new Stopwatch();

            sw.Start();
            var df = _mappingStoreDataAccess.GetDataflow(
                space,
                "ABS",
                "CAPEX",
                "1.1.0",
                true,
                ResolveCrossReferences.DoNotResolve
            );
            Console.WriteLine(sw.Elapsed.ToString("g"));
            
            sw.Restart();
            _mappingStoreDataAccess.DeleteDataSetsAndMappingSets(space, df, false);
            sw.Stop();
            
            Console.WriteLine(sw.Elapsed.ToString("g"));
        }

        [Test]
        public async Task TestInitAll()
        {
            var space = Configuration.SpacesInternal.First(x => x.Id == "design"); //qa:stable

            IDotStatDb dotStatDb = new MariaDbDotStatDb(
                space,
                DbUp.DatabaseVersion.DataDbVersion
            );

            var DotStatDb = dotStatDb as MariaDbDotStatDb;

            var UnitOfWork = new UnitOfWork(
                dotStatDb,
                new MariaDbArtefactRepository(DotStatDb, Configuration),
                new MariaDbAttributeRepository(DotStatDb, Configuration),
                new MariaDbDataStoreRepository(DotStatDb, Configuration),
                new MariaDbMetadataStoreRepository(DotStatDb, Configuration),
                new MariaDbMetadataComponentRepository(DotStatDb, Configuration),
                new MariaDbObservationRepository(DotStatDb, Configuration),
                new MariaDbTransactionRepository(DotStatDb, Configuration),
                new MariaDbComponentRepository(DotStatDb, Configuration),
                new MariaDbCodelistRepository(DotStatDb, Configuration)
            );

            var dbService = new DotStatDbService(UnitOfWork, Configuration);

            var transactionId = await UnitOfWork.TransactionRepository.GetNextTransactionId(CancellationToken.None);

            var transaction = await UnitOfWork.TransactionRepository.CreateTransactionItem
            (
                transactionId,
                SpecialTransactionArtefactIds.All,
                principal: null,
                sourceDataSpace: null,
                dataSource: null,
                TransactionType.InitAllMappingSets,
                requestedTargetVersion: null,
                serviceId: null,
                CancellationToken.None,
                true
            );

            var sw = new Stopwatch();
            
            sw.Start();
            await dbService.InitializeAllMappingSets(transaction, _mappingStoreDataAccess, CancellationToken.None);
            sw.Stop();

            Console.WriteLine($"{sw.Elapsed:g}");
        }

        [Test]
        public async Task TestIsExternal()
        {
            var dataflows = _mappingStoreDataAccess.GetDataflows("qa:stable");
            var df = dataflows.FirstOrDefault(x => x.FullId() == "ILO:DF_SDG_A871_SEX_AGE_RT(1.0)");
            
            Assert.IsNotNull(df);
            Assert.IsTrue(df.IsExternalReference.IsTrue);

            df = dataflows.FirstOrDefault(x => x.FullId() != "ILO:DF_SDG_A871_SEX_AGE_RT(1.0)");
            
            Assert.IsNotNull(df);
            Assert.IsFalse(df.IsExternalReference?.IsTrue ?? false);
        }

    }
}