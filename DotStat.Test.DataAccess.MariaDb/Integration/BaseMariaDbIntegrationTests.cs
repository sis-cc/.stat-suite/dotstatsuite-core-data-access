﻿using System.Linq;
using System.Threading.Tasks;
using DotStat.Common.Configuration.Dto;
using DotStat.Db.DB;
using DotStat.DB.Repository;
using DotStat.DB.Repository.MariaDb;
using DotStat.Db.Service;
using DotStat.Domain;

namespace DotStat.Test.DataAccess.MariaDb.Integration
{
    public abstract class BaseMariaDbIntegrationTests : SdmxUnitTestBase
    {
        public MariaDbDotStatDb DotStatDb;
        public IDotStatDbService  DotStatDbService;
        public IUnitOfWork  UnitOfWork;
        private readonly DataspaceInternal _dataSpace;

        protected BaseMariaDbIntegrationTests(string dfPath = null) : base(dfPath)
        {
            _dataSpace = Configuration.SpacesInternal.FirstOrDefault();

            InitObjects();
        }

        public void SetArchive(bool archive)
        {
            _dataSpace.Archive = archive;
        }

        public void SetKeepHistory(bool keepHistory)
        {
            _dataSpace.KeepHistory = keepHistory;
        }

        private void InitObjects()
        {
            IDotStatDb dotStatDb = new MariaDbDotStatDb(
                _dataSpace,
                DbUp.DatabaseVersion.DataDbVersion
                );

            DotStatDb = dotStatDb as MariaDbDotStatDb;

            UnitOfWork = new UnitOfWork(
                dotStatDb,
                new MariaDbArtefactRepository(DotStatDb, Configuration),
                new MariaDbAttributeRepository(DotStatDb, Configuration),
                new MariaDbDataStoreRepository(DotStatDb, Configuration),
                new MariaDbMetadataStoreRepository(DotStatDb, Configuration),
                new MariaDbMetadataComponentRepository(DotStatDb, Configuration),
                new MariaDbObservationRepository(DotStatDb, Configuration),
                new MariaDbTransactionRepository(DotStatDb, Configuration),
                new MariaDbComponentRepository(DotStatDb, Configuration),
                new MariaDbCodelistRepository(DotStatDb, Configuration)
            );

            DotStatDbService = new DotStatDbService(UnitOfWork, Configuration);
        }

        protected async ValueTask<bool> CleanUpStructure(IImportReferenceableStructure referencedStructure)
        {
            var dsd = referencedStructure.Dsd;

            var dsdArtefact = await UnitOfWork.ArtefactRepository.GetArtefactByDbId(dsd.DbId, CancellationToken);

            if (dsdArtefact == null)
                return false;

            var dataflowArtefacts = await UnitOfWork.ArtefactRepository.GetListOfDataflowArtefactsForDsd(dsd.DbId, CancellationToken);
            var allDsdComponents = await UnitOfWork.ComponentRepository.GetComponentsOfDsd(dsd, CancellationToken);

            if (dsd.Msd != null)
            {
                var msdComponents = await UnitOfWork.MetaMetadataComponentRepository.GetMetadataAttributesOfMsd(
                    dsd.Msd,
                    CancellationToken
                );

                await UnitOfWork.ArtefactRepository.CleanUpMsdOfDsd(
                    dsd.DbId,
                    dataflowArtefacts,
                    allDsdComponents,
                    msdComponents,
                    CancellationToken
                );
            }

            return await UnitOfWork.ArtefactRepository.CleanUpDsd(
                dsd.DbId, 
                dataflowArtefacts, 
                allDsdComponents, 
                CancellationToken
            );
        }


        protected async Task<bool> IndexExists(string indexName, string tableName, DataCompressionEnum dataCompression = DataCompressionEnum.NONE)
        {
            //TODO-MARIA
            //            var dataCompressionQuery = "";
            //            if (dataCompression is not DataCompressionEnum.NONE)
            //            {
            //                dataCompressionQuery = $"AND [p].[data_compression_desc] = '{dataCompression}'";
            //            }

            //            return (int)await DotStatDb.ExecuteScalarSqlAsync($@"
            //IF EXISTS(SELECT TOP 1 1
            //    FROM [sys].[partitions] AS [p]
            //    INNER JOIN sys.indexes AS [i] ON [i].[object_id] = [p].[object_id] AND [i].[index_id] = [p].[index_id]
            //    WHERE [i].name = '{indexName}' 
            //    AND [i].object_id = OBJECT_ID('[{DotStatDb.DataSchema}].[{tableName}]') 
            //    {dataCompressionQuery}
            //)
            //	SELECT 1;
            //ELSE 
            //	SELECT 0;", CancellationToken) > 0;

            return true;
        }
    }
}
