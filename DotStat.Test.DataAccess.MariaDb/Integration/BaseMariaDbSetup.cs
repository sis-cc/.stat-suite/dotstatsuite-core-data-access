﻿using System;
using System.Collections.Generic;
using DbUp;
using DbUp.Engine;
using MySqlConnector;

namespace DotStat.Test.DataAccess.MariaDb.Integration
{
    internal class BaseMariaDbSetup : SdmxUnitTestBase
    {
        protected void CreateDb(string connectionString, string scriptFolder)
        {
            var dbVersion = DbUp.DatabaseVersion.DataDbVersion;
            var connectionManager = new DotstatConnectionManager(connectionString, true);

            EnsureDatabase.For.MySqlDatabase(connectionString);

            var databaseName = new MySqlConnectionStringBuilder(connectionString).Database;

            UpgradeEngine dbUpEngine =
                MySqlExtensions.MySqlDatabase(connectionManager, databaseName)
                    .WithScriptsEmbeddedInAssembly(typeof(DbUp.MariaDb.Resources).Assembly,
                        s => s.Contains(scriptFolder, StringComparison.OrdinalIgnoreCase))
                    .WithScript("version.sql", $"UPDATE DB_VERSION SET VERSION = '{dbVersion}'",
                        new SqlScriptOptions() { RunGroupOrder = 1000 })
                    .WithVariables(new Dictionary<string, string>()
                    {
                        { "dbName", databaseName },
                        { "loginName", "dotStatTestUser" },
                        { "loginPwd", "j48j0Lh_rkd$jjRnGr_ejLj0Jr" },
                        { "ROloginName", "dotStatTestROUser" },
                        { "ROloginPwd", "j48j0Lh_rkd$jjRnGr_ejLj0Jr" }
                    })
                    .WithExecutionTimeout(TimeSpan.FromSeconds(180))
                    .LogToConsole()
                    .Build();

            var result = dbUpEngine.PerformUpgrade();

            if (!result.Successful)
            {
                throw result.Error;
            }
        }

        protected void DeleteDb(string connectionString) {
            MySqlConnectionStringBuilder builder = new MySqlConnectionStringBuilder(connectionString);
            using MySqlConnection connection = new MySqlConnection(connectionString);
            connection.Open();

            using MySqlCommand command = new MySqlCommand($"DROP DATABASE IF EXISTS {builder.Database};", connection);
            command.ExecuteNonQuery();
        }
    }
}
