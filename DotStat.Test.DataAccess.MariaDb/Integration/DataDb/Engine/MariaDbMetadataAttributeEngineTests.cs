﻿using System.Threading.Tasks;
using DotStat.DB.Engine.MariaDb;
using NUnit.Framework;

namespace DotStat.Test.DataAccess.MariaDb.Integration.DataDb.Engine
{
    [TestFixture, Parallelizable(ParallelScope.Fixtures)]
    public class MariaDbMetadataAttributeEngineTests : BaseMariaDbIntegrationTests
    {
        private MariaDbMetadataAttributeEngine _engine;
        private Domain.Dataflow _dataflow;

        public MariaDbMetadataAttributeEngineTests() : base("sdmx/CsvV2.xml")
        {
            _engine = new MariaDbMetadataAttributeEngine(Configuration);
            _dataflow = this.GetDataflow();

            InitMetadataForTests();
        }

        private void InitMetadataForTests()
        {
            _dataflow.Dsd.Msd.DbId = 6;

            var i = 100;

            foreach (var dim in _dataflow.Dsd.Dimensions)
                dim.DbId = i++;

            foreach (var attr in _dataflow.Dsd.Msd.MetadataAttributes)
                attr.DbId = i++;
            
        }

        [Test, Order(1)]
        public async Task No_Artifact_Record_Should_Exist()
        {
            foreach(var attr in _dataflow.Dsd.Msd.MetadataAttributes)
                Assert.AreEqual(-1, await _engine.GetDbId(attr.Code, _dataflow.Dsd.Msd.DbId, DotStatDb, CancellationToken));
        }

        [Test, Order(2)]
        public async Task Insert_Artifact()
        {
            foreach (var attr in _dataflow.Dsd.Msd.MetadataAttributes)
            {
                attr.Dsd = _dataflow.Dsd;
                Assert.IsTrue((attr.DbId = await _engine.InsertToComponentTable(attr, DotStatDb, CancellationToken)) > 0);
            }
        }

        [Test, Order(3)]
        public async Task CleanUp()
        {
            foreach (var attr in _dataflow.Dsd.Msd.MetadataAttributes)
            {
                attr.Dsd = _dataflow.Dsd;
                Assert.IsTrue(await _engine.GetDbId(attr.HierarchicalId, _dataflow.Dsd.Msd.DbId, DotStatDb, CancellationToken) > 0);

                await _engine.CleanUp(attr, DotStatDb, CancellationToken);
                Assert.AreEqual(-1, await _engine.GetDbId(attr.HierarchicalId, _dataflow.Dsd.Msd.DbId, DotStatDb, CancellationToken));
            }
        }

    }
}
