﻿using NUnit.Framework;
using System;
using System.Linq;
using System.Threading.Tasks;
using DotStat.Db;
using DotStat.DB.Engine.MariaDb;
using DotStat.Domain;

namespace DotStat.Test.DataAccess.MariaDb.Integration.DataDb.Engine
{
    [TestFixture, Parallelizable(ParallelScope.Fixtures)]
    public class MariaDbMetadataEnginesTests : BaseMariaDbIntegrationTests
    {
        private MariaDbMsdEngine _mdsEngine;
        private MariaDbMetadataDataflowEngine _metadataDataflowEngine;
        private Dataflow _dataflow;

        public MariaDbMetadataEnginesTests() : base("sdmx/CsvV2.xml")
        {
            _mdsEngine = new MariaDbMsdEngine(Configuration);
            _metadataDataflowEngine = new MariaDbMetadataDataflowEngine(Configuration);
            _dataflow = this.GetDataflow();
        }

        [Test, Order(1)]
        public async Task No_Artifact_Record_Should_Exist()
        {
            var id = await _mdsEngine.GetDbId(_dataflow.Dsd.Msd.Code, _dataflow.Dsd.Msd.AgencyId, _dataflow.Dsd.Msd.Version, DotStatDb, CancellationToken);

            Assert.AreEqual(-1, id);

            Assert.ThrowsAsync<NotImplementedException>(async () => await _metadataDataflowEngine.GetDbId(_dataflow.Dsd.Msd.Code, _dataflow.Dsd.Msd.AgencyId, _dataflow.Dsd.Msd.Version, DotStatDb, CancellationToken));
        }

        [Test, Order(2)]
        public async Task Insert_Artifact()
        {
            var tableVersion = DbTableVersion.A;
            var targetVersion = TargetVersion.Live;

            var transactionId = UnitOfWork.TransactionRepository.GetNextTransactionId(CancellationToken).Result;
            var transaction = UnitOfWork.TransactionRepository.CreateTransactionItem(transactionId, _dataflow.Dsd.FullId, null, null, "dataSource", TransactionType.Import, targetVersion, serviceId: null, CancellationToken).Result;

            // create dataflow DB objects 
            var tryNewTransactionResult =
                DotStatDbService.TryNewTransaction(transaction, _dataflow, MsAccess, CancellationToken).Result;
            Assert.IsTrue(tryNewTransactionResult, "Creation of the transaction failed.");

            // close transaction
            _dataflow.Dsd.LiveVersion = (char?)tableVersion;
            var success = DotStatDbService.CloseTransaction(transaction, _dataflow, false, false, true,
                MsAccess, false, false, null, CancellationToken).Result;
            Assert.IsTrue(success);

            Assert.IsTrue(_dataflow.Dsd.Msd.DbId > 0);

            var id = await _mdsEngine.GetDbId(_dataflow.Dsd.Msd.Code, _dataflow.Dsd.Msd.AgencyId, _dataflow.Dsd.Msd.Version, DotStatDb, CancellationToken);

            Assert.AreEqual(_dataflow.Dsd.Msd.DbId, id);
            Assert.AreEqual(_dataflow.Dsd.MsdDbId, id);

            Assert.ThrowsAsync<NotImplementedException>(async () => await _metadataDataflowEngine.InsertToArtefactTable(_dataflow, DotStatDb, CancellationToken));
        }

        [Test, Order(3)]
        public async Task Check_Msd_Meta_Tables_Created()
        {
            // Create tables ...
            //await _engine.CreateDynamicDbObjects(_dataflow.Dsd, DotStatDb, CancellationToken);

            var expectedTables = new[]
            {
                $"{DotStatDb.DataSchema}_{_dataflow.Dsd.MariaDbMetadataDataStructureTable('A')}",
                $"{DotStatDb.DataSchema}_{_dataflow.Dsd.MariaDbMetadataDataStructureTable('B')}",
                $"{DotStatDb.DataSchema}_{_dataflow.Dsd.MariaDbMetadataDataSetTable('A')}",
                $"{DotStatDb.DataSchema}_{_dataflow.Dsd.MariaDbMetadataDataSetTable('B')}",
                $"{DotStatDb.DataSchema}_{_dataflow.Dsd.MariaDbDeletedMetadataTable('A')}",
                $"{DotStatDb.DataSchema}_{_dataflow.Dsd.MariaDbDeletedMetadataTable('B')}",
            };

            foreach (var table in expectedTables)
                Assert.IsTrue(await DotStatDb.TableExists(table, CancellationToken), $"Table {table} have not been found");
            
            var expectedViews = new[]
            {
                $"{DotStatDb.DataSchema}_{_dataflow.Dsd.MariaDbMetaDataDsdViewName('A')}",
                $"{DotStatDb.DataSchema}_{_dataflow.Dsd.MariaDbMetaDataDsdViewName('B')}",
                $"{DotStatDb.DataSchema}_{_dataflow.Dsd.MariaDbDeletedMetadataDsdViewName('A')}",
                $"{DotStatDb.DataSchema}_{_dataflow.Dsd.MariaDbDeletedMetadataDsdViewName('B')}",
                $"{DotStatDb.DataSchema}_{_dataflow.MariaDbMetadataDataflowViewName('A')}",
                $"{DotStatDb.DataSchema}_{_dataflow.MariaDbMetadataDataflowViewName('B')}",
                $"{DotStatDb.DataSchema}_{_dataflow.MariaDbDeletedMetadataDataflowViewName('A')}",
                $"{DotStatDb.DataSchema}_{_dataflow.MariaDbDeletedMetadataDataflowViewName('B')}"
            };

            foreach (var view in expectedViews)
                Assert.IsTrue(await DotStatDb.ViewExists(view, CancellationToken), $"View {view} have not been found");
        }
        
        [Test, Order(4)]
        public async Task DropAllIndexes()
        {
            var dsd = _dataflow.Dsd;
            await _mdsEngine.DropAllIndexes(dsd, DotStatDb, CancellationToken);
            await _metadataDataflowEngine.DropAllIndexes(_dataflow, DotStatDb, CancellationToken);

            var expectedTables = new[]
            {
                $"{dsd.MariaDbMetadataDataStructureTable((char)DbTableVersion.A)}",
                $"{dsd.MariaDbMetadataDataStructureTable((char)DbTableVersion.B)}",
                $"{dsd.MariaDbDeletedMetadataTable((char)DbTableVersion.A)}",
                $"{dsd.MariaDbDeletedMetadataTable((char)DbTableVersion.B)}",
                $"{_dataflow.MariaDbMetadataDataflowTable((char)DbTableVersion.A)}",
                $"{_dataflow.MariaDbMetadataDataflowTable((char)DbTableVersion.B)}"
            };

            foreach (var tableName in expectedTables)
            {
                var indexCount = (long)await DotStatDb.ExecuteScalarSqlAsync($@"
                    SELECT COUNT(*)  
                      FROM information_schema.statistics 
                     WHERE UPPER(TABLE_NAME) = UPPER('{DotStatDb.DataSchema}_{tableName}') AND TABLE_SCHEMA = DATABASE();"
                    , CancellationToken);

                Assert.AreEqual(0, indexCount);
            }
        }

        [Test, Order(5)]
        public async Task AddUniqueConstraints()
        {
            var dsd = _dataflow.Dsd;
            await _mdsEngine.AddUniqueConstraints(dsd, false, DotStatDb, CancellationToken);
            await _metadataDataflowEngine.AddUniqueConstraints(_dataflow, false, DotStatDb, CancellationToken);
            
            var expectedTables = new[]
            {
                $"{dsd.MariaDbMetadataDataStructureTable((char)DbTableVersion.A)}",
                $"{dsd.MariaDbMetadataDataStructureTable((char)DbTableVersion.B)}",
                $"{_dataflow.MariaDbMetadataDataflowTable((char)DbTableVersion.A)}",
                $"{_dataflow.MariaDbMetadataDataflowTable((char)DbTableVersion.B)}"
            };
            
            foreach (var tableName in expectedTables)
            {
                var indexName = $"UI_{tableName}";
                var indexExists = await IndexExists(indexName, tableName);
                Assert.IsTrue(indexExists);
            }
        }

        [Test, Order(6)]
        [TestCase(DataCompressionEnum.COLUMNSTORE)]
        [TestCase(DataCompressionEnum.COLUMNSTORE_ARCHIVE)]
        public async Task Compress(DataCompressionEnum dataCompression)
        {
            var dsd = _dataflow.Dsd;
            await _mdsEngine.DropAllIndexes(dsd, DotStatDb, CancellationToken);
            await _mdsEngine.Compress(dsd, dataCompression, DotStatDb, CancellationToken);

            await _metadataDataflowEngine.DropAllIndexes(_dataflow, DotStatDb, CancellationToken);
            await _metadataDataflowEngine.Compress(_dataflow, dataCompression, DotStatDb, CancellationToken);

            var expectedTables = new[]
            {
                $"{dsd.MariaDbMetadataDataStructureTable((char)DbTableVersion.A)}",
                $"{dsd.MariaDbMetadataDataStructureTable((char)DbTableVersion.B)}",
                $"{dsd.MariaDbDeletedMetadataTable((char)DbTableVersion.A)}",
                $"{dsd.MariaDbDeletedMetadataTable((char)DbTableVersion.B)}",
                $"{_dataflow.MariaDbMetadataDataflowTable((char)DbTableVersion.A)}",
                $"{_dataflow.MariaDbMetadataDataflowTable((char)DbTableVersion.B)}"
            };

            foreach (var tableName in expectedTables)
            {
                var indexName = $"CCI_{tableName}";
                var indexExists = await IndexExists(indexName, tableName, dataCompression);

                Assert.IsTrue(indexExists);
            }
        }

        [Test, Order(7)]
        public async Task Cleanup()
        {
            var allDbComponents = UnitOfWork.ComponentRepository.GetAllComponents(CancellationToken).Result.ToList();
            var allMsdDbComponents = UnitOfWork.MetaMetadataComponentRepository.GetAllMetadataAttributeComponents(CancellationToken).Result.ToList();

            await DotStatDbService.CleanUpDsd(_dataflow.Dsd.DbId, false, this.MsAccess, allDbComponents, allMsdDbComponents, this.CancellationToken);
        }
    }
}
