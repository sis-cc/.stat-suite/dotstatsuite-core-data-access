﻿using System.Threading.Tasks;
using DotStat.Db;
using DotStat.DB.Engine.MariaDb;
using DotStat.Domain;
using NUnit.Framework;

namespace DotStat.Test.DataAccess.MariaDb.Integration.DataDb.Engine
{
    [TestFixture, Parallelizable(ParallelScope.Fixtures)]
    public class MariaDbAttributeEngineTests : BaseMariaDbIntegrationTests
    {
        private MariaDbAttributeEngine _engine;
        private Domain.Dataflow _dataflow;

        public MariaDbAttributeEngineTests() : base("sdmx/264D_264_SALDI2_ATTRIBUTE_TEST.xml")
        {
            _engine = new MariaDbAttributeEngine(Configuration);
            _dataflow = this.GetDataflow();

            InitDataForTests();
        }

        private void InitDataForTests()
        {
            _dataflow.Dsd.DbId = 5;

            var i = 100;

            foreach (var dim in _dataflow.Dsd.Dimensions)
                dim.DbId = i++;

            foreach (var attr in _dataflow.Dsd.Attributes)
                attr.DbId = i++;

            _dataflow.Dsd.PrimaryMeasure.DbId = i;
        }

        [Test, Order(1)]
        public async Task No_Artifact_Record_Should_Exist()
        {
            foreach(var attr in _dataflow.Dsd.Attributes)
                Assert.AreEqual(-1, await _engine.GetDbId(attr.Code, _dataflow.Dsd.DbId, DotStatDb, CancellationToken));
        }

        [Test, Order(2)]
        public async Task Insert_Artifact()
        {
            foreach (var attr in _dataflow.Dsd.Attributes)
                Assert.IsTrue((attr.DbId = await _engine.InsertToComponentTable(attr, DotStatDb, CancellationToken)) > 0);
        }

        [Test, Order(3)]
        public async Task Check_Tables_Created()
        {
            var dsd = _dataflow.Dsd;

            // Create Attribute tables ...

            await _engine.CreateDynamicDbObjects(dsd, DotStatDb, CancellationToken);

            var expectedTables = new[]
            {
                $"{MariaDbExtensions.DataSchema}_{dsd.MariaDbDsdAttrTable('A')}",
                $"{MariaDbExtensions.DataSchema}_{dsd.MariaDbDsdAttrTable('B')}",
                $"{MariaDbExtensions.DataSchema}_{dsd.MariaDbDimGroupAttrTable('A')}",
                $"{MariaDbExtensions.DataSchema}_{dsd.MariaDbDimGroupAttrTable('B')}"
            };

            foreach (var table in expectedTables)
                Assert.IsTrue(await DotStatDb.TableExists(table, CancellationToken), $"Table {table} have not been found");
        }

        //TODO-MARIA
        //        [Test, Order(4)]
        //        public async Task DropAllIndexes()
        //        {
        //            var dsd = _dataflow.Dsd;
        //            await _engine.DropAllIndexes(dsd, DotStatDb, CancellationToken);

        //            var expectedTables = new[]
        //            {
        //                $"{dsd.MariaDbDimGroupAttrTable((char)DbTableVersion.A)}",
        //                $"{dsd.MariaDbDimGroupAttrTable((char)DbTableVersion.B)}"
        //            };

        //            foreach (var tableName in expectedTables)
        //            {
        //                var indexCount = (int)await DotStatDb.ExecuteScalarSqlAsync($@"
        //SELECT count(*) FROM sys.indexes AS [i] 
        //WHERE [i].object_id = OBJECT_ID('[{DotStatDb.DataSchema}].[{tableName}]')
        //AND [type_desc] <>'HEAP';", CancellationToken);

        //                Assert.AreEqual(0, indexCount);
        //            }
        //        }

        [Test, Order(5)]
        public async Task AddUniqueConstraints()
        {
            var dsd = _dataflow.Dsd;
            await _engine.AddUniqueConstraints(dsd, false, DotStatDb, CancellationToken);

            var expectedTables = new[]
            {
                $"{dsd.MariaDbDimGroupAttrTable((char)DbTableVersion.A)}",
                $"{dsd.MariaDbDimGroupAttrTable((char)DbTableVersion.B)}"
            };

            foreach (var tableName in expectedTables)
            {
                var indexName = $"UI_{tableName}";
                var indexExists = await IndexExists(indexName, tableName);
                Assert.IsTrue(indexExists);
            }
        }

        //TODO-MARIA
        //[Test, Order(6)]
        //[TestCase(DataCompressionEnum.COLUMNSTORE)]
        //[TestCase(DataCompressionEnum.COLUMNSTORE_ARCHIVE)]
        //public async Task Compress(DataCompressionEnum dataCompression)
        //{
        //    var dsd = _dataflow.Dsd;
        //    await _engine.DropAllIndexes(dsd, DotStatDb, CancellationToken);
        //    await _engine.Compress(dsd, dataCompression, DotStatDb, CancellationToken);

        //    var expectedTables = new[]
        //    {
        //        $"{dsd.MariaDbDimGroupAttrTable((char)DbTableVersion.A)}",
        //        $"{dsd.MariaDbDimGroupAttrTable((char)DbTableVersion.B)}"
        //    };

        //    foreach (var tableName in expectedTables)
        //    {
        //        var indexName = $"CCI_{tableName}";
        //        var indexExists = await IndexExists(indexName, tableName, dataCompression);

        //        Assert.IsTrue(indexExists);
        //    }
        //}

        [Test, Order(7)]
        public async Task CleanUp()
        {
            foreach (var attr in _dataflow.Dsd.Attributes)
            {
                Assert.IsTrue(await _engine.GetDbId(attr.Code, _dataflow.Dsd.DbId, DotStatDb, CancellationToken) > 0);

                await _engine.CleanUp(attr, DotStatDb, CancellationToken);

                Assert.AreEqual(-1, await _engine.GetDbId(attr.Code, _dataflow.Dsd.DbId, DotStatDb, CancellationToken));
            }
        }

        //TODO-MARIA
        //[Test, Order(8)]
        //public async Task Check_Temporal_Tables_Created()
        //{
        //    var dsd = _dataflow.Dsd;
        //    dsd.KeepHistory = true;
        //    dsd.DbId++;

        //    // Create Attribute tables ...

        //    await _engine.CreateDynamicDbObjects(dsd, DotStatDb, CancellationToken);

        //    var expectedTables = new[]
        //    {
        //        dsd.MariaDbDsdAttrTable('A'),
        //        dsd.MariaDbDsdAttrTable('B'),
        //        dsd.MariaDbDsdAttrHistoryTable('A'),
        //        dsd.MariaDbDsdAttrHistoryTable('B'),
        //        dsd.MariaDbDimGroupAttrTable('A'),
        //        dsd.MariaDbDimGroupAttrTable('B'),
        //        dsd.MariaDbDimGroupAttrHistoryTable('A'),
        //        dsd.MariaDbDimGroupAttrHistoryTable('B'),
        //    };

        //    foreach (var table in expectedTables)
        //        Assert.IsTrue(await DotStatDb.TableExists(table, CancellationToken), $"Table {table} have not been found");
        //}
    }
}
