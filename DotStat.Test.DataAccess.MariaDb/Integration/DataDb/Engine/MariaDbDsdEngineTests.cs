﻿using System.Linq;
using System.Threading.Tasks;
using DotStat.Db;
using DotStat.DB.Engine.MariaDb;
using DotStat.Domain;
using NUnit.Framework;

namespace DotStat.Test.DataAccess.MariaDb.Integration.DataDb.Engine
{
    [TestFixture]
    public class MariaDbDsdEngineTests : BaseMariaDbIntegrationTests
    {
        private MariaDbDsdEngine _engine;
        private Domain.Dataflow _dataflow;

        public MariaDbDsdEngineTests()
        {
            _engine = new MariaDbDsdEngine(Configuration);
            _dataflow = this.GetDataflow();

            InitDataForTests();
        }

        private void InitDataForTests()
        {
            var i = 100;

            foreach (var dim in _dataflow.Dsd.Dimensions)
                dim.DbId = i++;

            foreach (var attr in _dataflow.Dsd.Attributes)
                attr.DbId = i++;

            _dataflow.Dsd.PrimaryMeasure.DbId = i;
        }

        [Test, Order(1)]
        public async Task No_Artifact_Record_Should_Exist()
        {
            var id = await _engine.GetDbId(_dataflow.Dsd.Code, _dataflow.Dsd.AgencyId, _dataflow.Dsd.Version, DotStatDb, CancellationToken);

            Assert.AreEqual(-1, id);
        }

        [Test, Order(2)]
        public async Task Insert_Artifact()
        {
            var id = await _engine.InsertToArtefactTable(_dataflow.Dsd, DotStatDb, CancellationToken);

            Assert.IsTrue(id > 0);

            _dataflow.Dsd.DbId = await _engine.GetDbId(_dataflow.Dsd.Code, _dataflow.Dsd.AgencyId, _dataflow.Dsd.Version, DotStatDb, CancellationToken);

            Assert.AreEqual(_dataflow.Dsd.DbId, id);
        }

        [Test, Order(3)]
        public async Task Check_Dsd_Fact_Tables_Created()
        {
            var dsd = _dataflow.Dsd;

            // Create Fact tables ...

            await _engine.CreateDynamicDbObjects(dsd, DotStatDb, CancellationToken);

            var expectedTables = new string[]
            {
                $"{MariaDbExtensions.DataSchema}_{dsd.MariaDbFactTable('A')}",
                $"{MariaDbExtensions.DataSchema}_{dsd.MariaDbFactTable('B')}",
                $"{MariaDbExtensions.DataSchema}_{dsd.MariaDbFilterTable()}",
            };

            foreach (var table in expectedTables)
                Assert.IsTrue(await DotStatDb.TableExists(table, CancellationToken), $"Table {table} have not been found");
        }

        //TODO-MARIA
//        [Test, Order(4)]
//        public async Task DropAllIndexes()
//        {
//            var dsd = _dataflow.Dsd;
//            await _engine.DropAllIndexes(dsd, DotStatDb, CancellationToken);

//            var expectedTables = new[]
//            {
//                $"{dsd.MariaDbFilterTable()}",
//                $"{dsd.MariaDbFactTable((char)DbTableVersion.A)}",
//                $"{dsd.MariaDbFactTable((char)DbTableVersion.B)}",
//                $"{dsd.MariaDbDeletedTable((char)DbTableVersion.A)}",
//                $"{dsd.MariaDbDeletedTable((char)DbTableVersion.B)}"
//            };

//            foreach (var tableName in expectedTables)
//            {
//                var indexCount = (int)await DotStatDb.ExecuteScalarSqlAsync($@"
//SELECT count(*) FROM sys.indexes AS [i] 
//WHERE [i].object_id = OBJECT_ID('[{DotStatDb.DataSchema}].[{tableName}]')
//AND [type_desc] <>'HEAP';", CancellationToken);

//                Assert.AreEqual(0, indexCount);
//            }
//        }

        [Test, Order(5)]
        public async Task AddUniqueConstraints()
        {
            var dsd = _dataflow.Dsd;
            await _engine.AddUniqueConstraints(dsd, clustered: true, DotStatDb, CancellationToken);

            var expectedTables = new[]
            {
                $"{dsd.MariaDbFilterTable()}",
                $"{dsd.MariaDbFactTable((char)DbTableVersion.A)}",
                $"{dsd.MariaDbFactTable((char)DbTableVersion.B)}",
            };

            var indexName = $"PK_{dsd.MariaDbFilterTable()}";
            var indexExists= await IndexExists(indexName, dsd.MariaDbFilterTable());
            Assert.IsTrue(indexExists);

            indexName = $"UI_{dsd.MariaDbFilterTable()}";
            indexExists = await IndexExists(indexName, dsd.MariaDbFilterTable());
            Assert.IsTrue(indexExists);

            foreach (var tableName in expectedTables)
            {
                indexName = $"PK_{tableName}";
                indexExists = await IndexExists(indexName, tableName);

                Assert.IsTrue(indexExists);
            }
        }

        //[Test, Order(6)]
        //[TestCase(DataCompressionEnum.COLUMNSTORE)]
        //[TestCase(DataCompressionEnum.COLUMNSTORE_ARCHIVE)]
        //public async Task Compress(DataCompressionEnum dataCompression)
        //{
        //    var dsd = _dataflow.Dsd;
        //    await _engine.DropAllIndexes(dsd, DotStatDb, CancellationToken);
        //    await _engine.Compress(dsd, dataCompression, DotStatDb, CancellationToken);

        //    var expectedTables = new []
        //    {
        //        $"{dsd.MariaDbFilterTable()}",
        //        $"{dsd.MariaDbFactTable((char)DbTableVersion.A)}",
        //        $"{dsd.MariaDbFactTable((char)DbTableVersion.B)}",
        //        $"{dsd.MariaDbDeletedTable((char)DbTableVersion.A)}",
        //        $"{dsd.MariaDbDeletedTable((char)DbTableVersion.B)}"
        //    };

        //    foreach (var tableName in expectedTables)
        //    {
        //        var indexName = $"CCI_{tableName}";
        //        var indexExists = await IndexExists(indexName, tableName, dataCompression);

        //        Assert.IsTrue(indexExists);
        //    }
        //}

        //[Test, Order(7)]
        //public async Task Check_Dsd_Fact_Temporal_Tables_Created()
        //{
        //    var dsd = _dataflow.Dsd;
        //    dsd.KeepHistory = true;
        //    dsd.DbId = 100;

        //    // Create Fact tables ...

        //    await _engine.CreateDynamicDbObjects(dsd, DotStatDb, CancellationToken);

        //    var expectedTables = new string[]
        //    {
        //        dsd.MariaDbFactTable('A'),
        //        dsd.MariaDbFactTable('B'),
        //        $"{dsd.MariaDbFactHistoryTable('A')}",
        //        $"{dsd.MariaDbFactHistoryTable('B')}",
        //        dsd.MariaDbFilterTable(),
        //    };

        //    foreach (var table in expectedTables)
        //        Assert.IsTrue(await DotStatDb.TableExists(table, CancellationToken), $"Table {table} have not been found");
        //}

        [TestCase( false, "CONCAT(CONVERT(IFNULL(DIM_1, 0), binary(11)),CONVERT(IFNULL(DIM_2, 0), binary(11)))")]
        [TestCase( true, "CONCAT(CONVERT(IFNULL(DIM_1, 0), binary(11)),CONVERT(IFNULL(DIM_2, 0), binary(11)),UNHEX(MD5(COALESCE(PERIOD_SDMX, ''))))")]
        public void BuildRowIdFormula(bool includeTimeDim, string expectedSql)
        {
            var dsd = new Domain.Dsd(_dataflow.Dsd.Base);
            dsd.SetDimensions(_dataflow.Dsd.Dimensions.Where((d,index)=> index < 2 || d.Base.TimeDimension));

            var i = 1;

            foreach (var dim in dsd.Dimensions)
                dim.DbId = i++;

            var sql = dsd.MariaDbBuildRowIdFormula(includeTimeDim);
            
            Assert.AreEqual(expectedSql, sql);
        }

    }
}
