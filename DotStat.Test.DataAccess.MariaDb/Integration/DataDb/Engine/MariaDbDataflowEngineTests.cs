﻿using System.Threading.Tasks;
using DotStat.DB.Engine.MariaDb;
using NUnit.Framework;

namespace DotStat.Test.DataAccess.MariaDb.Integration.DataDb.Engine
{
    [TestFixture, Parallelizable(ParallelScope.Fixtures)]
    public class MariaDbDataflowEngineTests : BaseMariaDbIntegrationTests
    {
        private MariaDbDataflowEngine _engine;
        private Domain.Dataflow _dataflow;

        public MariaDbDataflowEngineTests()
        {
            _engine = new MariaDbDataflowEngine(Configuration);
            _dataflow = GetDataflow();
        }

        [Test, Order(1)]
        public async Task No_Artifact_Record_Should_Exist()
        {
            var id = await _engine.GetDbId(_dataflow.Code, _dataflow.AgencyId, _dataflow.Version, DotStatDb, CancellationToken);

            Assert.AreEqual(-1, id);
        }

        [Test, Order(2)]
        public async Task Insert_Artifact()
        {
            var id = await _engine.InsertToArtefactTable(_dataflow, DotStatDb, CancellationToken);

            Assert.IsTrue(id > 0);

            _dataflow.DbId = await _engine.GetDbId(_dataflow.Code, _dataflow.AgencyId, _dataflow.Version, DotStatDb, CancellationToken);

            Assert.AreEqual(_dataflow.DbId, id);
        }

        [Test, Order(3)]
        public async Task CleanUp()
        {
            var id = _dataflow.DbId = await _engine.GetDbId(_dataflow.Code, _dataflow.AgencyId, _dataflow.Version, DotStatDb, CancellationToken);

            Assert.IsTrue(id > 0);

            await _engine.CleanUp(_dataflow, DotStatDb, CancellationToken);

            id = await _engine.GetDbId(_dataflow.Code, _dataflow.AgencyId, _dataflow.Version, DotStatDb, CancellationToken);

            Assert.AreEqual(-1, id);
        }
    }
}
