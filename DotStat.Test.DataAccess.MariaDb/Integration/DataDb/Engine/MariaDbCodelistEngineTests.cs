﻿using System.Threading.Tasks;
using DotStat.Db;
using DotStat.DB.Engine.MariaDb;
using NUnit.Framework;

namespace DotStat.Test.DataAccess.MariaDb.Integration.DataDb.Engine
{
    [TestFixture, Parallelizable(ParallelScope.Fixtures)]
    public class MariaDbCodelistEngineTests : BaseMariaDbIntegrationTests
    {
        private MariaDbCodelistEngine _engine;
        private Domain.Dataflow _dataflow;

        public MariaDbCodelistEngineTests()
        {
            _engine = new MariaDbCodelistEngine(Configuration);
            _dataflow = GetDataflow();
        }

        [Test, Order(1)]
        public async Task No_Artifact_Record_Should_Exist()
        {
            foreach(var dim in _dataflow.Dsd.Dimensions)
                if(dim.Codelist!=null)
                    Assert.AreEqual(-1, await _engine.GetDbId(dim.Codelist.Code, dim.Codelist.AgencyId, dim.Codelist.Version, DotStatDb, CancellationToken));
        }

        [Test, Order(2)]
        public async Task Insert_Artifact()
        {
            foreach (var dim in _dataflow.Dsd.Dimensions)
                if (dim.Codelist != null)
                    Assert.IsTrue((dim.Codelist.DbId = await _engine.InsertToArtefactTable(dim.Codelist, DotStatDb, CancellationToken)) > 0);
        }

        [Test, Order(3)]
        public async Task Check_Tables_Created()
        {
            foreach (var dim in _dataflow.Dsd.Dimensions)
                if (dim.Codelist != null)
                {
                    var codelistTable = $"{MariaDbExtensions.ManagementSchema}_{MariaDbExtensions.GetCodelistTableName(dim.Codelist.DbId)}";

                    Assert.IsFalse(await DotStatDb.TableExists(codelistTable, CancellationToken));

                    await _engine.CreateDynamicDbObjects(dim.Codelist, DotStatDb, CancellationToken);

                    Assert.IsTrue(await DotStatDb.TableExists(codelistTable, CancellationToken));
                }
        }

        [Test, Order(4)]
        public async Task Get_Codes_Of_CodeList()
        {
            foreach (var dim in _dataflow.Dsd.Dimensions)
                if (dim.Codelist != null)
                {
                    var list = await _engine.GetCodesOfCodeList(dim.Codelist, DotStatDb, CancellationToken);

                    Assert.IsNotNull(list);
                    Assert.AreEqual(dim.Codelist.Codes.Count, list.Count);
                }
        }

        [Test, Order(5)]
        public async Task CleanUp()
        {
            foreach (var dim in _dataflow.Dsd.Dimensions)
                if (dim.Codelist != null)
                {
                    var id = dim.Codelist.DbId = await _engine.GetDbId(dim.Codelist.Code, dim.Codelist.AgencyId, dim.Codelist.Version, DotStatDb, CancellationToken);
                    var codelistTable = DbExtensions.GetCodelistTableName(dim.Codelist.DbId);

                    Assert.IsTrue(id>0);

                    await _engine.CleanUp(dim.Codelist, DotStatDb, CancellationToken);

                    id = await _engine.GetDbId(dim.Codelist.Code, dim.Codelist.AgencyId, dim.Codelist.Version, DotStatDb, CancellationToken);

                    Assert.AreEqual(-1, id);
                    Assert.IsFalse(await DotStatDb.TableExists(codelistTable, CancellationToken));
                }
        }

    }
}
