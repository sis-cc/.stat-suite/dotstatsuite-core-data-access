﻿using System.Threading.Tasks;
using DotStat.DB.Engine.MariaDb;
using DotStat.Db.Util;
using NUnit.Framework;

namespace DotStat.Test.DataAccess.MariaDb.Integration.DataDb.Engine
{
    [TestFixture, Parallelizable(ParallelScope.Fixtures)]
    public class MariaDbPrimaryMeasureEngineTests : BaseMariaDbIntegrationTests
    {
        private MariaDbPrimaryMeasureEngine _engine;
        private Domain.Dataflow _dataflow;

        public MariaDbPrimaryMeasureEngineTests() : base("sdmx/264D_264_SALDI2_ATTRIBUTE_TEST.xml")
        {
            _dataflow = this.GetDataflow();
            _engine = new MariaDbPrimaryMeasureEngine(Configuration, new MariaDbDataTypeEnumHelper(DotStatDb));

            InitDataForTests();
        }

        private void InitDataForTests()
        {
            _dataflow.Dsd.DbId = 5;

            var i = 100;

            foreach (var dim in _dataflow.Dsd.Dimensions)
                dim.DbId = i++;

            foreach (var attr in _dataflow.Dsd.Attributes)
                attr.DbId = i++;

            _dataflow.Dsd.PrimaryMeasure.DbId = i;
        }

        [Test, Order(1)]
        public async Task No_Artifact_Record_Should_Exist()
        {
            var measure = _dataflow.Dsd.PrimaryMeasure;
            Assert.AreEqual(-1, await _engine.GetDbId(measure.Code, _dataflow.Dsd.DbId, DotStatDb, CancellationToken));
        }

        [Test, Order(2)]
        public async Task Insert_PrimaryMeasure()
        {
            var measure = _dataflow.Dsd.PrimaryMeasure;
            Assert.IsTrue((measure.DbId = await _engine.InsertToComponentTable(measure, DotStatDb, CancellationToken)) > 0);
        }

        [Test, Order(3)]
        public async Task CleanUp()
        {
            var measure = _dataflow.Dsd.PrimaryMeasure;

            Assert.IsTrue(await _engine.GetDbId(measure.Code, _dataflow.Dsd.DbId, DotStatDb, CancellationToken) > 0);

            await _engine.CleanUp(measure, DotStatDb, CancellationToken);
            
            Assert.AreEqual(-1, await _engine.GetDbId(measure.Code, _dataflow.Dsd.DbId, DotStatDb, CancellationToken));
        }
    }
}
