﻿using System.Threading.Tasks;
using DotStat.Db.Exception;
using DotStat.Domain;
using NUnit.Framework;

namespace DotStat.Test.DataAccess.MariaDb.Integration.DataDb.Repository.DsdChangeTests
{
    [TestFixture, Parallelizable(ParallelScope.Children)]
    [TestFixture(false)]
    [TestFixture(true)]
    public class GroupAttributeChangeTests : MariaDbBaseDsdChangeTests
    {
        private IImportReferenceableStructure _originalStructure;
        private IImportReferenceableStructure _originalStructure4NewCodeTest;
        private bool _useDsdAsReferencedStructure;

        public GroupAttributeChangeTests(bool useDsdAsReferencedStructure)
        {
            _useDsdAsReferencedStructure = useDsdAsReferencedStructure;
        }

        [OneTimeSetUp]
        public async Task SetUp()
        {
            _originalStructure = await InitOriginalStructure("ATTR_TYPE_DIFF_TEST.xml", useDsdReference: _useDsdAsReferencedStructure);
            _originalStructure4NewCodeTest = await InitOriginalStructure("ATTR_TYPE_DIFF_TEST_GROUP_2.xml", useDsdReference: _useDsdAsReferencedStructure);
        }

        [OneTimeTearDown]
        public async Task TearDown()
        {
            await CleanUpStructure(_originalStructure);
            await CleanUpStructure(_originalStructure4NewCodeTest);
        }

        [Test]
        public void GroupAttributeAddedTest()
        {
            var referencedStructure = GetStructureWithBaseStructures("ATTR_TYPE_DIFF_TEST_GROUP_ATTR_ADDED.xml", useDsdReference: _useDsdAsReferencedStructure);
            referencedStructure.Dsd.DbId = _originalStructure.Dsd.DbId;

            var ex = Assert.ThrowsAsync<ChangeInDsdException>(async () => await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(referencedStructure, MappingStoreDataAccess, CancellationToken));

            StringAssert.Contains("OECD:ATTR_TYPE_DSD(11.1).ATTR_GROUP_NEW", ex.Message);
        }

        [Test]
        public void GroupAttributeRemovedTest()
        {
            var referencedStructure = GetStructureWithBaseStructures("ATTR_TYPE_DIFF_TEST_GROUP_ATTR_REMOVED.xml", useDsdReference: _useDsdAsReferencedStructure);
            referencedStructure.Dsd.DbId = _originalStructure.Dsd.DbId;

            var ex = Assert.ThrowsAsync<ChangeInDsdException>(async () => await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(referencedStructure, MappingStoreDataAccess, CancellationToken));

            StringAssert.Contains("OECD:ATTR_TYPE_DSD(11.1)", ex.Message);
            StringAssert.Contains("ATTR_GROUP_STRING", ex.Message);
        }

        [Test]
        public void GroupAttributeCodedChangedToNonCodedTest()
        {
            var referencedStructure = GetStructureWithBaseStructures("ATTR_TYPE_DIFF_TEST_GROUP_ENUMERATED_TO_STRING.xml", useDsdReference: _useDsdAsReferencedStructure);
            referencedStructure.Dsd.DbId = _originalStructure.Dsd.DbId;

            var ex = Assert.ThrowsAsync<ChangeInDsdException>(async () => await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(referencedStructure, MappingStoreDataAccess, CancellationToken));

            StringAssert.Contains("OECD:ATTR_TYPE_DSD(11.1).ATTR_GROUP_ENUMERATED", ex.Message);
        }

        [Test]
        public void GroupAttributeNonCodedChangedToCodedTest()
        {
            var referencedStructure = GetStructureWithBaseStructures("ATTR_TYPE_DIFF_TEST_GROUP_STRING_TO_ENUMERATED.xml", useDsdReference: _useDsdAsReferencedStructure);
            referencedStructure.Dsd.DbId = _originalStructure.Dsd.DbId;

            var ex = Assert.ThrowsAsync<ChangeInDsdException>(async () => await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(referencedStructure, MappingStoreDataAccess, CancellationToken));

            StringAssert.Contains("OECD:ATTR_TYPE_DSD(11.1).ATTR_GROUP_STRING", ex.Message);
        }

        [Test]
        public void GroupAttributeMandatoryToConditional()
        {
            var referencedStructure = GetStructureWithBaseStructures("ATTR_TYPE_DIFF_TEST_GROUP_MANDATORY_TO_CONDITIONAL.xml", useDsdReference: _useDsdAsReferencedStructure);
            referencedStructure.Dsd.DbId = _originalStructure.Dsd.DbId;

            var ex = Assert.ThrowsAsync<ChangeInDsdException>(async () => await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(referencedStructure, MappingStoreDataAccess, CancellationToken));

            StringAssert.Contains("OECD:ATTR_TYPE_DSD(11.1).ATTR_GROUP_ENUMERATED", ex.Message);
            StringAssert.Contains("from Mandatory to Conditional", ex.Message);
        }

        [Test]
        public void GroupAttributeConditionalToMandatory()
        {
            var referencedStructure = GetStructureWithBaseStructures("ATTR_TYPE_DIFF_TEST_GROUP_CONDITIONAL_TO_MANDATORY.xml", useDsdReference: _useDsdAsReferencedStructure);
            referencedStructure.Dsd.DbId = _originalStructure.Dsd.DbId;

            var ex = Assert.ThrowsAsync<ChangeInDsdException>(async () => await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(referencedStructure, MappingStoreDataAccess, CancellationToken));

            StringAssert.Contains("OECD:ATTR_TYPE_DSD(11.1).ATTR_GROUP_STRING", ex.Message);
            StringAssert.Contains("Conditional to Mandatory", ex.Message);
        }

        [Test]
        public void GroupAttributeGroupChanegeTest()
        {
            var referencedStructure = GetStructureWithBaseStructures("ATTR_TYPE_DIFF_TEST_GROUP_NEW_GROUP.xml", useDsdReference: _useDsdAsReferencedStructure);
            referencedStructure.Dsd.DbId = _originalStructure.Dsd.DbId;

            var ex = Assert.ThrowsAsync<ChangeInDsdException>(async () => await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(referencedStructure, MappingStoreDataAccess, CancellationToken));

            StringAssert.Contains("OECD:ATTR_TYPE_DSD(11.1).ATTR_GROUP_STRING", ex.Message);
            StringAssert.Contains("from TestGroup to NewGroup", ex.Message);
        }

        [Test]
        public void GroupAttributeChangedToDatasetLevelTest()
        {
            var referencedStructure = GetStructureWithBaseStructures("ATTR_TYPE_DIFF_TEST_GROUP_TO_DATASET.xml", useDsdReference: _useDsdAsReferencedStructure);
            referencedStructure.Dsd.DbId = _originalStructure.Dsd.DbId;

            var ex = Assert.ThrowsAsync<ChangeInDsdException>(async () => await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(referencedStructure, MappingStoreDataAccess, CancellationToken));

            StringAssert.Contains("OECD:ATTR_TYPE_DSD(11.1).ATTR_GROUP_STRING", ex.Message);
            StringAssert.Contains("from Group to DataSet", ex.Message);
        }

        [Test]
        public void GroupAttributeChangedToObservationLevelTest()
        {
            var referencedStructure = GetStructureWithBaseStructures("ATTR_TYPE_DIFF_TEST_GROUP_TO_OBSERVATION.xml", useDsdReference: _useDsdAsReferencedStructure);
            referencedStructure.Dsd.DbId = _originalStructure.Dsd.DbId;

            var ex = Assert.ThrowsAsync<ChangeInDsdException>(async () => await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(referencedStructure, MappingStoreDataAccess, CancellationToken));

            StringAssert.Contains("OECD:ATTR_TYPE_DSD(11.1).ATTR_GROUP_STRING", ex.Message);
            StringAssert.Contains("from Group to Observation", ex.Message);
        }

        [Test]
        public void GroupAttributeChangedToDimensionLevelTest()
        {
            var referencedStructure = GetStructureWithBaseStructures("ATTR_TYPE_DIFF_TEST_GROUP_TO_DIMENSION.xml", useDsdReference: _useDsdAsReferencedStructure);
            referencedStructure.Dsd.DbId = _originalStructure.Dsd.DbId;

            var ex = Assert.ThrowsAsync<ChangeInDsdException>(async () => await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(referencedStructure, MappingStoreDataAccess, CancellationToken));

            StringAssert.Contains("OECD:ATTR_TYPE_DSD(11.1).ATTR_GROUP_STRING", ex.Message);
            StringAssert.Contains("from Group to DimensionGroup", ex.Message);
        }

        [Test]
        public void GroupAttributeCodelistChangeTest()
        {
            var referencedStructure = GetStructureWithBaseStructures("ATTR_TYPE_DIFF_TEST_GROUP_NEW_CODELIST.xml", useDsdReference: _useDsdAsReferencedStructure);
            referencedStructure.Dsd.DbId = _originalStructure.Dsd.DbId;

            var ex = Assert.ThrowsAsync<ChangeInDsdException>(async () => await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(referencedStructure, MappingStoreDataAccess, CancellationToken));

            StringAssert.Contains("OECD:ATTR_TYPE_DSD(11.1).ATTR_GROUP_ENUMERATED", ex.Message);
            StringAssert.Contains("OECD:CL_FREQ(11.0)", ex.Message); //New code list
        }

        [Test]
        public void GroupAttributeCodelistHasNewCodeTest()
        {
            var referencedStructure = GetStructureWithBaseStructures("ATTR_TYPE_DIFF_TEST_GROUP_2_NEW_CODE_IN_CODELIST.xml", useDsdReference: _useDsdAsReferencedStructure);
            referencedStructure.Dsd.DbId = _originalStructure4NewCodeTest.Dsd.DbId; 

            Assert.DoesNotThrowAsync(async () => await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(referencedStructure, MappingStoreDataAccess, CancellationToken));
        }

        [Test]
        public void GroupAttributeCodelistHasRemovedCodeTest()
        {
            var referencedStructure = GetStructureWithBaseStructures("ATTR_TYPE_DIFF_TEST_GROUP_CODE_REMOVED_FROM_CODELIST.xml", useDsdReference: _useDsdAsReferencedStructure);
            referencedStructure.Dsd.DbId = _originalStructure.Dsd.DbId;

            var ex = Assert.ThrowsAsync<ChangeInDsdException>(async () => await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(referencedStructure, MappingStoreDataAccess, CancellationToken));

            StringAssert.Contains("T2, T3", ex.Message); // codes removed
            StringAssert.Contains("OECD:ATTR_TYPE_DSD(11.1).ATTR_GROUP_ENUMERATED", ex.Message); //affected component
            StringAssert.Contains("OECD:CL_TEST_GROUP(11.0)", ex.Message); //affected codelist
        }
    }
}

