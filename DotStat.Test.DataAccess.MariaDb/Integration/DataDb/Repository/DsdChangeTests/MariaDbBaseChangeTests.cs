﻿using System.Linq;
using System.Threading.Tasks;
using DotStat.DB.Engine.MariaDb;
using DotStat.Domain;
using DotStat.MappingStore;
using DotStat.Test.Moq;
using Org.Sdmxsource.Sdmx.Api.Model.Objects;

namespace DotStat.Test.DataAccess.MariaDb.Integration.DataDb.Repository.DsdChangeTests
{
    public abstract class MariaDbBaseDsdChangeTests : BaseMariaDbIntegrationTests
    {
        protected TestMappingStoreDataAccess MappingStoreDataAccess;
        protected readonly ISdmxObjects BaseSdmxObjects;

        protected MariaDbBaseDsdChangeTests()
        {
            Configuration.MaxTransferErrorAmount = 100;

            BaseSdmxObjects = GetSdmxObjects("DIFF_TEST_BASE.xml");

            MappingStoreDataAccess = new TestMappingStoreDataAccess();
        }

        protected void AppendBaseStructures(ISdmxObjects sdmxObjects)
        {
            foreach (var sdmxObjectsConceptScheme in BaseSdmxObjects.ConceptSchemes)
            {
                sdmxObjects.AddConceptScheme(sdmxObjectsConceptScheme);
            }

            var existingCodelists = sdmxObjects.Codelists.ToArray();
            foreach (var sdmxObjectsCodelist in BaseSdmxObjects.Codelists)
            {
                if (!existingCodelists.Any(cl => cl.Urn.Equals(sdmxObjectsCodelist.Urn)))
                {
                    sdmxObjects.AddCodelist(sdmxObjectsCodelist);
                }
            }
        }

        protected IImportReferenceableStructure GetStructureWithBaseStructures(string fileName, string folderName = "sdmx/dsd-change-test", bool useDsdReference = false)
        {
            var sdmxObjects = GetSdmxObjects(fileName, folderName);

            AppendBaseStructures(sdmxObjects);

            if(useDsdReference)
                return SdmxParser.ParseDsdStructure(sdmxObjects);

            return SdmxParser.ParseDataFlowStructure(sdmxObjects);
        }

        protected async Task<IImportReferenceableStructure> InitOriginalStructure(string fileName, string folderName = "sdmx/dsd-change-test", bool useDsdReference = false)
        {
            var structure = GetStructureWithBaseStructures(fileName, folderName, useDsdReference);
            var engine = new MariaDbDsdEngine(Configuration);

            var ver = new SdmxVersion(structure.Dsd.Version.ToString());
            structure.Dsd.DbId = await engine.GetDbId(structure.Dsd.Base.Id, structure.Dsd.AgencyId, ver.Major, ver.Minor, ver.Patch, DotStatDb, CancellationToken);
            if (structure.Dsd.DbId == -1)
                structure.Dsd.DbId = await engine.InsertToArtefactTable(structure.Dsd, DotStatDb, CancellationToken);

            await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(structure, MappingStoreDataAccess, CancellationToken);
            return structure;
        }

        protected ISdmxObjects GetSdmxObjects(string fileName, string folderName = "sdmx/dsd-change-test")
        {
            return MsAccess.GetSdmxObjects($"{folderName}/{fileName}");
        }
    }
}

