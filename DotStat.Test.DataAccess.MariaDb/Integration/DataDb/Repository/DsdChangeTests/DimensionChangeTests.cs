﻿using System.Threading.Tasks;
using DotStat.Db.Exception;
using DotStat.Domain;
using NUnit.Framework;

namespace DotStat.Test.DataAccess.MariaDb.Integration.DataDb.Repository.DsdChangeTests
{
    [TestFixture, Parallelizable(ParallelScope.Children)]
    [TestFixture(false)]
    [TestFixture(true)]
    public class DimensionChangeTests : MariaDbBaseDsdChangeTests
    {
        private IImportReferenceableStructure _originalStructure;
        private IImportReferenceableStructure _originalStructureNoTimeDim;
        private IImportReferenceableStructure _originalStructure4NewCodeTest;
        private readonly bool _useDsdAsReferencedStructure;

        public DimensionChangeTests(bool useDsdAsReferencedStructure)
        {
            _useDsdAsReferencedStructure = useDsdAsReferencedStructure;
        }


        [OneTimeSetUp]
        public async Task SetUp()
        {
            _originalStructure = await InitOriginalStructure("DIM_TYPE_DIFF_TEST.xml", useDsdReference: _useDsdAsReferencedStructure);
            _originalStructureNoTimeDim = await InitOriginalStructure("DIM_TYPE_DIFF_TEST_NO_TIME_DIM.xml", useDsdReference: _useDsdAsReferencedStructure);
            _originalStructure4NewCodeTest = await InitOriginalStructure("DIM_TYPE_DIFF_TEST2.xml", useDsdReference: _useDsdAsReferencedStructure);
        }

        [OneTimeTearDown]
        public async Task TearDown()
        {
            await CleanUpStructure(_originalStructure);
            await CleanUpStructure(_originalStructureNoTimeDim);
            await CleanUpStructure(_originalStructure4NewCodeTest);
        }

        [Test]
        public void DimensionAddedTest()
        {
            var referencedStructure = GetStructureWithBaseStructures("DIM_TYPE_DIFF_TEST_DIM_ADDED.xml", useDsdReference: _useDsdAsReferencedStructure);
            referencedStructure.Dsd.DbId = _originalStructure.Dsd.DbId;

            var ex = Assert.ThrowsAsync<ChangeInDsdException>(async () => await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(referencedStructure, MappingStoreDataAccess, CancellationToken));
            StringAssert.Contains("OECD:DIM_TYPE_DSD(11.1).NEW_DIM", ex.Message);
        }

        [Test]
        public void DimensionRemovedTest()
        {
            var referencedStructure = GetStructureWithBaseStructures("DIM_TYPE_DIFF_TEST_DIM_REMOVED.xml", useDsdReference: _useDsdAsReferencedStructure);
            referencedStructure.Dsd.DbId = _originalStructure.Dsd.DbId;

            var ex = Assert.ThrowsAsync<ChangeInDsdException>(async () => await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(referencedStructure, MappingStoreDataAccess, CancellationToken));
            StringAssert.Contains("OECD:DIM_TYPE_DSD(11.1)", ex.Message);
            StringAssert.Contains("MEAS", ex.Message);
        }

        [Test]
        public void TimeDimensionAddedTest()
        {
            var referencedStructure = GetStructureWithBaseStructures("DIM_TYPE_DIFF_TEST_NO_TIME_DIM_ADDED_TIME_DIM.xml", useDsdReference: _useDsdAsReferencedStructure);
            referencedStructure.Dsd.DbId = _originalStructureNoTimeDim.Dsd.DbId;

            var ex = Assert.ThrowsAsync<ChangeInDsdException>(async () => await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(referencedStructure, MappingStoreDataAccess, CancellationToken));

            StringAssert.Contains("OECD:DIM_TYPE_DSD_NO_TIME_DIM(11.1)", ex.Message);
            StringAssert.Contains("time dimension was added", ex.Message);
        }

        [Test]
        public void TimeDimensionRemovedTest()
        {
            var referencedStructure = GetStructureWithBaseStructures("DIM_TYPE_DIFF_TEST_TIME_DIM_REMOVED.xml", useDsdReference: _useDsdAsReferencedStructure);
            referencedStructure.Dsd.DbId = _originalStructure.Dsd.DbId;

            var ex = Assert.ThrowsAsync<ChangeInDsdException>(async () => await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(referencedStructure, MappingStoreDataAccess, CancellationToken));

            StringAssert.Contains("OECD:DIM_TYPE_DSD(11.1)", ex.Message);
            StringAssert.Contains("time dimension was removed", ex.Message);
        }

        [Test]
        public void DimensionCodelistChangeTest()
        {
            var referencedStructure = GetStructureWithBaseStructures("DIM_TYPE_DIFF_TEST_DIM_NEW_CODELIST.xml", useDsdReference: _useDsdAsReferencedStructure);
            referencedStructure.Dsd.DbId = _originalStructure.Dsd.DbId;

            var ex = Assert.ThrowsAsync<ChangeInDsdException>(async () => await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(referencedStructure, MappingStoreDataAccess, CancellationToken));

            StringAssert.Contains("OECD:DIM_TYPE_DSD(11.1).MEAS", ex.Message);
            StringAssert.Contains("OECD:CL_TEST(11.0)", ex.Message); //New code list
        }

        [Test]
        public void DimensionCodelistHasNewCodeTest()
        {
            var referencedStructure = GetStructureWithBaseStructures("DIM_TYPE_DIFF_TEST2_DIM_NEW_CODE_IN_CODELIST.xml", useDsdReference: _useDsdAsReferencedStructure);
            referencedStructure.Dsd.DbId = _originalStructure4NewCodeTest.Dsd.DbId;

            Assert.DoesNotThrowAsync(async () => await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(referencedStructure, MappingStoreDataAccess, CancellationToken));
        }

        [Test]
        public void DimensionCodelistHasRemovedCodeTest()
        {
            var referencedStructure = GetStructureWithBaseStructures("DIM_TYPE_DIFF_TEST_DIM_CODE_REMOVED_FROM_CODELIST.xml", useDsdReference: _useDsdAsReferencedStructure);
            referencedStructure.Dsd.DbId = _originalStructure.Dsd.DbId;

            var ex = Assert.ThrowsAsync<ChangeInDsdException>(async () => await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(referencedStructure, MappingStoreDataAccess, CancellationToken));

            StringAssert.Contains("Q, W", ex.Message); // codes removed
            StringAssert.Contains("OECD:DIM_TYPE_DSD(11.1).FREQ", ex.Message); //affected component
            StringAssert.Contains("OECD:CL_FREQ(11.0)", ex.Message); //affected codelist
        }
    }
}

