﻿using System;
using System.Linq;
using System.Threading.Tasks;
using DotStat.Common.Configuration.Dto;
using DotStat.Db.Dto;
using DotStat.DB.Engine.MariaDb;
using DotStat.Domain;
using NUnit.Framework;

namespace DotStat.Test.DataAccess.MariaDb.Integration.DataDb.Repository
{
    [TestFixture, NonParallelizable]
    public class MariaDbManagementRepositoryTests : BaseMariaDbIntegrationTests
    {
        private DataspaceInternal _dataspace;
        private Dataflow _dataflow;
        
        public MariaDbManagementRepositoryTests() : base("sdmx/CsvV2.xml")
        {
            _dataspace = Configuration.SpacesInternal.First();
            _dataflow = this.GetDataflow();
        }

        [OneTimeTearDown]
        public async Task TearDown()
        {
            await CleanUpStructure(_dataflow.Dsd);
        }

        [Test]
        public void GetDb()
        {
            Assert.IsNotNull(DotStatDb);
            Assert.AreEqual(_dataspace.Id.ToUpper(), DotStatDb.Id);
            Assert.AreEqual(_dataspace.DatabaseCommandTimeoutInSec, DotStatDb.DatabaseCommandTimeout);
            Assert.AreEqual("management", DotStatDb.ManagementSchema);
            Assert.AreEqual("data", DotStatDb.DataSchema);
        }

        [Test]
        public async Task GetNextTransactionId()
        {
            var id1 = await UnitOfWork.TransactionRepository.GetNextTransactionId(CancellationToken);
            var id2 = await UnitOfWork.TransactionRepository.GetNextTransactionId(CancellationToken);

            Assert.IsTrue(id2 > id1);
        }

        [Test]
        public async Task CleanUpNonExistentDsd()
        {
            var result = await UnitOfWork.ArtefactRepository.CleanUpDsd(
                -1, 
                Array.Empty<ArtefactItem>(),
                Array.Empty<ComponentItem>(),
                CancellationToken
            );

            Assert.IsFalse(result);
        }
        [Test]
        public async Task CleanUpNonExistentMsd()
        {
            var result = await UnitOfWork.ArtefactRepository.CleanUpMsdOfDsd(
                100,
                Array.Empty<ArtefactItem>(),
                Array.Empty<ComponentItem>(),
                Array.Empty<MetadataAttributeItem>(),
                CancellationToken
            );

            Assert.IsFalse(result);
        }

        [Test]
        public async Task CleanUpExistingDsd()
        {
            _dataflow.Dsd.MsdDbId = 0;
            _dataflow.Dsd.Msd.DbId = 0;
            _dataflow.Dsd.DbId = await new MariaDbDsdEngine(Configuration).InsertToArtefactTable(_dataflow.Dsd, DotStatDb, CancellationToken);

            await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(_dataflow, MsAccess, CancellationToken, false);

            Assert.IsTrue(await CleanUpStructure(_dataflow.Dsd));
        }

        [Test]
        public async Task CleanUpExistingMsd()
        {
            _dataflow.Dsd.MsdDbId = 0;
            _dataflow.Dsd.Msd.DbId = 0;
            _dataflow.Dsd.DbId = await new MariaDbDsdEngine(Configuration).InsertToArtefactTable(_dataflow.Dsd, DotStatDb, CancellationToken);

            await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(_dataflow, MsAccess, CancellationToken, false);

            var dataflowArtefacts = await UnitOfWork.ArtefactRepository.GetListOfDataflowArtefactsForDsd(_dataflow.Dsd.DbId, CancellationToken);
            var allDsdComponents = await UnitOfWork.ComponentRepository.GetAllComponents(CancellationToken);
            var allMsdComponents = await UnitOfWork.MetaMetadataComponentRepository.GetAllMetadataAttributeComponents(CancellationToken);

            Assert.IsTrue(
                await UnitOfWork.ArtefactRepository.CleanUpMsdOfDsd(_dataflow.Dsd.DbId, dataflowArtefacts, allDsdComponents.ToList(), allMsdComponents.ToList(), CancellationToken));
        }
    }
}
