﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DotStat.Db;
using DotStat.Db.Util;
using DotStat.Domain;
using NUnit.Framework;
using Org.Sdmxsource.Sdmx.Api.Model.Data;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Data;

namespace DotStat.Test.DataAccess.MariaDb.Integration.DataDb.Import
{
    [TestFixture("sdmx/CsvV2.xml"), NonParallelizable]
    public sealed class MetadataImportDsdTestsDelete : BaseMariaDbIntegrationTests
    {
        private readonly Dsd _dsd;

        public MetadataImportDsdTestsDelete(string structure) : base(structure)
        {
            _dsd = this.GetDataflow().Dsd;
        }

        [OneTimeTearDown]
        public async Task TearDown()
        {
            await CleanUpStructure(_dsd);
        }

        [Test]
        public async Task MetadataDeleteAll()
        {
            await InitializeAllData();

            var components = new List<string>();
            components.AddRange(Enumerable.Repeat(string.Empty, 28));

            var observationRows = GetObservationRows(components);

            await ImportObservations(observationRows);

            var view = _dsd.MariaDbMetaDataDsdViewName((char)DbTableVersion.A);
            Assert.AreEqual(0, (long)await DotStatDb.ExecuteScalarSqlAsync($"SELECT COUNT(*) FROM data_{view}", CancellationToken));

            //check deleted view
            view = _dsd.MariaDbDeletedMetadataTable((char)DbTableVersion.A);
            Assert.AreEqual(1, (long)await DotStatDb.ExecuteScalarSqlAsync($"SELECT COUNT(*) FROM data_{view}", CancellationToken));

        }

        [Test]
        public async Task MetadataDeleteAllDatasetLevelComponents()
        {
            await InitializeAllData();

            var components = new List<string>();
            components.AddRange(Enumerable.Repeat(MariaDbExtensions.DimensionSwitchedOff, 6));
            components.AddRange(Enumerable.Repeat(string.Empty, 21));

            var observationRows = GetObservationRows(components);

            await ImportObservations(observationRows);

            var view = _dsd.MariaDbMetaDataDsdViewName((char)DbTableVersion.A);
            Assert.AreEqual(8, (long)await DotStatDb.ExecuteScalarSqlAsync($"SELECT COUNT(*) FROM data_{view}", CancellationToken));

            //check deleted view
            view = _dsd.MariaDbDeletedMetadataTable((char)DbTableVersion.A);
            Assert.AreEqual(1, (long)await DotStatDb.ExecuteScalarSqlAsync($"SELECT COUNT(*) FROM data_{view}", CancellationToken));
        }

        [Test]
        public async Task MetadataDeleteOneDatasetLevelComponent()
        {
            await InitializeAllData();

            var components = new List<string>();
            components.AddRange(Enumerable.Repeat(MariaDbExtensions.DimensionSwitchedOff, 6));
            components.Add("x");
            components.AddRange(Enumerable.Repeat(string.Empty, 20));

            var observationRows = GetObservationRows(components);

            await ImportObservations(observationRows);

            var view = _dsd.MariaDbMetaDataDsdViewName((char)DbTableVersion.A);
            var datasetAttribute = _dsd.MariaDbMetadataDataSetTable((char)DbTableVersion.A);
            var attributeColumn0 = _dsd.Msd.MetadataAttributes[0].MariaDbColumn();
            var attributeColumn1 = _dsd.Msd.MetadataAttributes[1].MariaDbColumn();

            Assert.AreEqual(9, (long)await DotStatDb.ExecuteScalarSqlAsync($"SELECT COUNT(*) FROM data_{view}", CancellationToken));
            Assert.AreEqual(1, (long)await DotStatDb.ExecuteScalarSqlAsync($"SELECT COUNT(*) FROM data_{datasetAttribute} WHERE {attributeColumn0} IS NULL AND {attributeColumn1} IS NOT NULL AND DF_ID = -1", CancellationToken));

            //check deleted view
            view = _dsd.MariaDbDeletedMetadataTable((char)DbTableVersion.A);
            Assert.AreEqual(1, (long)await DotStatDb.ExecuteScalarSqlAsync($"SELECT COUNT(*) FROM data_{view}", CancellationToken));
        }

        [Test]
        public async Task MetadataDeleteOneComponentOnAllLevels()
        {
            await InitializeAllData();

            var components = new List<string>();
            components.AddRange(Enumerable.Repeat(string.Empty, 6));
            components.Add("x");
            components.AddRange(Enumerable.Repeat(string.Empty, 21));

            var observationRows = GetObservationRows(components);

            await ImportObservations(observationRows);

            var view = _dsd.MariaDbMetaDataDsdViewName((char)DbTableVersion.A);
            var attributeColumn = _dsd.Msd.MetadataAttributes[0].Code;

            Assert.AreEqual(9, (long)await DotStatDb.ExecuteScalarSqlAsync($"SELECT COUNT(*) FROM data_{view}", CancellationToken));
            Assert.AreEqual(9, (long)await DotStatDb.ExecuteScalarSqlAsync($"SELECT COUNT(*) FROM data_{view} WHERE {attributeColumn} IS NULL", CancellationToken));

            //check deleted view
            view = _dsd.MariaDbDeletedMetadataTable((char)DbTableVersion.A);
            Assert.AreEqual(2, (long)await DotStatDb.ExecuteScalarSqlAsync($"SELECT COUNT(*) FROM data_{view}", CancellationToken));
        }

        [Test]
        public async Task MetadataDeleteAllAttachedToDimensionValue()
        {
            await InitializeAllData();

            var components = new List<string>();
            components.AddRange(Enumerable.Repeat(string.Empty, 3));
            components.Add(MariaDbExtensions.DimensionSwitchedOff);
            components.Add(MariaDbExtensions.DimensionSwitchedOff);
            components.Add(string.Empty);
            components.AddRange(Enumerable.Repeat(string.Empty, 21));

            var observationRows = GetObservationRows(components);

            await ImportObservations(observationRows);

            var view = _dsd.MariaDbMetaDataDsdViewName((char)DbTableVersion.A);
            Assert.AreEqual(6, (long)await DotStatDb.ExecuteScalarSqlAsync($"SELECT COUNT(*) FROM data_{view}", CancellationToken));

            //check deleted view
            view = _dsd.MariaDbDeletedMetadataTable((char)DbTableVersion.A);
            Assert.AreEqual(1, (long)await DotStatDb.ExecuteScalarSqlAsync($"SELECT COUNT(*) FROM data_{view}", CancellationToken));
        }

        [Test]
        public async Task MetadataDeleteRelatedToTimeValue()
        {
            await InitializeAllData();

            var components = new List<string>();
            components.AddRange(Enumerable.Repeat(string.Empty, 4));
            components.Add("F");
            components.Add("2023");
            components.AddRange(Enumerable.Repeat(string.Empty, 21));

            var observationRows = GetObservationRows(components);

            await ImportObservations(observationRows);

            var view = _dsd.MariaDbMetaDataDsdViewName((char)DbTableVersion.A);
            Assert.AreEqual(5, (long)await DotStatDb.ExecuteScalarSqlAsync($"SELECT COUNT(*) FROM data_{view}", CancellationToken));

            //check deleted view
            view = _dsd.MariaDbDeletedMetadataTable((char)DbTableVersion.A);
            Assert.AreEqual(1, (long)await DotStatDb.ExecuteScalarSqlAsync($"SELECT COUNT(*) FROM data_{view}", CancellationToken));
        }

        private IAsyncEnumerable<ObservationRow> GetObservationRows(IReadOnlyList<string> components)
        {
            var i = 0;
            //Dimensions
            var hasWildCardedDimensions = false;
            var seriesKey = new List<IKeyValue>();
            foreach (var dimId in _dsd.Dimensions.Where(d => !d.Base.TimeDimension).Select(d => d.Base.Id))
            {
                var keyVal = new KeyValueImpl(components[i++], dimId);
                seriesKey.Add(keyVal);
                if (string.IsNullOrEmpty(keyVal.Code) || keyVal.Code == MariaDbExtensions.DimensionSwitchedOff || keyVal.Code == MariaDbExtensions.DimensionWildCarded)
                    hasWildCardedDimensions = true;
            }

            var key = new KeyableImpl(null, _dsd.Base, seriesKey, null);
            var obsTime = components[i++];
            if (string.IsNullOrEmpty(obsTime) || obsTime == MariaDbExtensions.DimensionSwitchedOff || obsTime == MariaDbExtensions.DimensionWildCarded)
                hasWildCardedDimensions = true;

            //Attributes
            var attributeValues = new List<IKeyValue>();
            if (components.Skip(6).Any(x => !string.IsNullOrEmpty(x)))
            {
                foreach (var dimId in _dsd.Msd.MetadataAttributes.Select(d => d.HierarchicalId))
                {
                    if (!string.IsNullOrEmpty(components[i]))
                    {
                        attributeValues.Add(new KeyValueImpl(components[i], dimId));
                    }

                    i++;
                }
            }

            var observation = new ObservationImpl(key, obsTime, null, attributeValues, crossSectionValue: null);

            return new List<ObservationRow>
            {
                new(1, StagingRowActionEnum.Delete, observation, hasWildCardedDimensions)
            }.ToAsyncEnumerable();
        }

        private async Task InitializeAllData()
        {
            await UnitOfWork.MetadataStoreRepository.TruncateMetadataTables(_dsd, DbTableVersion.A, CancellationToken);

            var observations = ObservationGenerator.Generate(
                _dsd,
                null,
                false,
                2023,
                2023,
                8,
                action: StagingRowActionEnum.Merge);

            await ImportObservations(observations, true);
        }

        private async Task ImportObservations(IAsyncEnumerable<ObservationRow> observations, bool initialiseDataSetAttributes = false)
        {
            var reportedComponents = new ReportedComponents
            {
                Dimensions = _dsd.Dimensions.ToList(),
                MetadataAttributes = _dsd.Msd.MetadataAttributes.ToList(),
                IsPrimaryMeasureReported = false,
                TimeDimension = _dsd.Dimensions.First(x => x.Base.TimeDimension)
            };

            var transactionId = await UnitOfWork.TransactionRepository.GetNextTransactionId(CancellationToken);
            var transaction = await UnitOfWork.TransactionRepository.CreateTransactionItem(transactionId,
                _dsd.FullId, null, null, "testFile.csv", TransactionType.Import, TargetVersion.Live, serviceId: null,
                CancellationToken);

            var tryNewTransactionResult = await DotStatDbService.TryNewTransaction(transaction, _dsd, MsAccess, CancellationToken);

            await UnitOfWork.MetadataStoreRepository.RecreateMetadataStagingTables(_dsd, CancellationToken);

            var codeTranslator = new CodeTranslator(UnitOfWork.CodeListRepository);
            await codeTranslator.FillDict(_dsd, CancellationToken);

            var bulkImportResult = await UnitOfWork.MetadataStoreRepository.BulkInsertMetadata(observations, reportedComponents, codeTranslator, _dsd,
                false, false, CancellationToken);

            var datasetAttributes = initialiseDataSetAttributes
                ? new List<DataSetAttributeRow>
                    {  new (1, StagingRowActionEnum.Merge)
                       {
                        Attributes = new List<IKeyValue>{
                                    new KeyValueImpl("value", "STRING_TYPE"),
                                    new KeyValueImpl("value2", "STRING_MULTILANG_TYPE")
                        }
                    }}
                : bulkImportResult.DataSetLevelAttributeRows;

            await UnitOfWork.MetadataStoreRepository.MergeStagingTable(_dsd, reportedComponents, 
                bulkImportResult.BatchActions, datasetAttributes, codeTranslator, DbTableVersion.A, true, CancellationToken);

            _dsd.LiveVersion = (char?)DbTableVersion.A;
            await DotStatDbService.CloseTransaction(transaction, _dsd, false, false, true,
                MsAccess, false, false, null, CancellationToken);
        }
    }
}
