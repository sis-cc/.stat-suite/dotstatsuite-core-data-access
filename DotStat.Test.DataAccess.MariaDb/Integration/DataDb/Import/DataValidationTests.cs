﻿using DotStat.Db;
using DotStat.Db.Util;
using DotStat.Db.Validation;
using DotStat.Db.Validation.MariaDb;
using DotStat.Domain;
using NUnit.Framework;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Data.Query;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference;
using Org.Sdmxsource.Sdmx.StructureRetrieval.Manager;
using Org.Sdmxsource.Sdmx.Util.Objects.Container;
using System.Linq;
using System.Threading.Tasks;

namespace DotStat.Test.DataAccess.MariaDb.Integration.DataDb.Import
{
    [NonParallelizable]
    [TestFixture("sdmx/264D_264_SALDI2_ATTRIBUTE_TEST.xml", "........./?startPeriod=1900&endPeriod=2120-04")]
    [TestFixture("sdmx/KH_NIS,DF_AGRI_NO_TIME,2.0.xml", "........./?startPeriod=1900&endPeriod=2120-04")]
    public sealed class DataValidationTests : BaseMariaDbIntegrationTests
    {
        private readonly Dataflow _dataflow;
        private readonly string _dataQuery;
        private MariaDbKeyableDatabaseValidator _keyableDatabaseValidator;
        private readonly ReportedComponents _reportedComponents;

        public DataValidationTests(string structure, string dataQuery) : base(structure)
        {
            
            _dataflow = this.GetDataflow();
            _dataQuery = dataQuery; 
            _reportedComponents = GetReportedComponents(_dataflow);
        }

        [OneTimeSetUp]
        public void Init()
        {
            base.Configuration.MaxTransferErrorAmount = 100;
            _keyableDatabaseValidator = new MariaDbKeyableDatabaseValidator(base.Configuration);
        }

        [OneTimeTearDown]
        public async Task TearDown()
        {
            await CleanUpStructure(_dataflow.Dsd);
        }


        [Test, Order(1)]
        public async Task FullValidateStaging()
        {
            const DbTableVersion tableVersion = DbTableVersion.A;
            const TargetVersion targetVersion = TargetVersion.Live;
            const bool fullValidation = true;
            const bool isTimeAtTimeDimensionSupported = false;
            const string dataSource = "testFile.csv";

            var transactionId = await UnitOfWork.TransactionRepository.GetNextTransactionId(CancellationToken);
            var transaction= await UnitOfWork.TransactionRepository.CreateTransactionItem(transactionId, _dataflow.FullId, null, null, dataSource, TransactionType.Import, targetVersion, serviceId: null, CancellationToken);

            // create dataflow DB objects 
            var tryNewTransactionResult = await DotStatDbService.TryNewTransaction(transaction, _dataflow, MsAccess, CancellationToken);
            Assert.IsTrue(tryNewTransactionResult);

            // generate observations
            var obsCount = 1000;
            var observations = ObservationGenerator.Generate(
                _dataflow.Dsd,
                    _dataflow,
                    true,
                    1980,
                    1999,
                    obsCount,null,
                    true,
                    action: StagingRowActionEnum.Merge
                );

            Assert.AreEqual(obsCount, await observations.CountAsync(CancellationToken));
           
            // ------ Logic bellow normally in SqlConsumer of Transfer service ----------

            // Wipe and/or create staging table and target tables
            await UnitOfWork.DataStoreRepository.RecreateStagingTables(_dataflow.Dsd, _reportedComponents, isTimeAtTimeDimensionSupported, CancellationToken);

            // import to staging
            var codeTranslator = new CodeTranslator(UnitOfWork.CodeListRepository);
            await codeTranslator.FillDict(_dataflow, CancellationToken);
            var bulkImportResult = await UnitOfWork.DataStoreRepository.BulkInsertData(observations, _reportedComponents, codeTranslator, _dataflow, fullValidation, isTimeAtTimeDimensionSupported, false, CancellationToken);
            
            Assert.GreaterOrEqual(obsCount, bulkImportResult.RowsCopied);
            
            //---------------------
            await _keyableDatabaseValidator.Validate(DotStatDb, codeTranslator, _dataflow, _dataflow.Dsd.Attributes.ToList(), tableVersion,100, CancellationToken);
            bulkImportResult.Errors.AddRange(_keyableDatabaseValidator.GetErrors());
            Assert.GreaterOrEqual(bulkImportResult.Errors.Count, 1);
            Assert.GreaterOrEqual(bulkImportResult.Errors.Count(e => e.Type == ValidationErrorType.MandatoryAttributeWithNullValueInStaging), 1);
            // --------------------------------------------------------------------------

            // check view is created & if observations are returned
            var view = $"{MariaDbExtensions.DataSchema}_{_dataflow.Dsd.MariaDbDataDsdViewName((char) tableVersion)}";
           Assert.IsTrue(await DotStatDb.ViewExists(view, CancellationToken));

           
           var dbObsCount = (long) await DotStatDb.ExecuteScalarSqlAsync($"select count(*) from {view}", CancellationToken);
           Assert.AreEqual(0, dbObsCount);

           // close transaction
           _dataflow.Dsd.LiveVersion = (char?)tableVersion;
           var success = await DotStatDbService.CloseTransaction(transaction, _dataflow, false, obsCount>0, true,
                MsAccess, false, false, null, CancellationToken);
           Assert.IsTrue(success);

            // ---------------------------------
            
            var dataQuery = new DataQueryImpl(
                 new RESTDataQueryCore($"data/{_dataflow.AgencyId},{_dataflow.Code},{_dataflow.Version}/{_dataQuery}"),
                 new InMemoryRetrievalManager(new SdmxObjectsImpl(_dataflow.Dsd.Base, _dataflow.Base)));

            var dbObservations = await UnitOfWork.ObservationRepository.GetObservations(
                    dataQuery,
                    _dataflow,
                    tableVersion,
                    isDataQuery: true,
                    CancellationToken
                )
                .ToArrayAsync(CancellationToken);

            Assert.AreEqual(0, dbObservations.Length);
        }

        [Test, Order(2)]
        public async Task ImportBasicValidation()
        {
            const DbTableVersion tableVersion = DbTableVersion.A;
            const TargetVersion targetVersion = TargetVersion.Live;
            const bool fullValidation = false;
            const bool isTimeAtTimeDimensionSupported = false;
            const string dataSource = "testFile.csv";

            var transactionId = await UnitOfWork.TransactionRepository.GetNextTransactionId(CancellationToken);
            var transaction = await UnitOfWork.TransactionRepository.CreateTransactionItem(transactionId, _dataflow.FullId, null, null, dataSource, TransactionType.Import, targetVersion, serviceId: null, CancellationToken);

            // create dataflow DB objects 
            var tryNewTransactionResult = await DotStatDbService.TryNewTransaction(transaction, _dataflow, MsAccess, CancellationToken);
            Assert.IsTrue(tryNewTransactionResult);

            // generate observations
            const int obsCount = 1000;
            var observations = ObservationGenerator.Generate(
                _dataflow.Dsd,
                _dataflow,
                true,
                1980,
                2020,
                obsCount, null,
                true,
                action: StagingRowActionEnum.Merge
            );

            Assert.AreEqual(obsCount, await observations.CountAsync(CancellationToken));

            // ------ Logic bellow normally in SqlConsumer of Transfer service ----------

            // Wipe and/or create staging table and target tables
            await UnitOfWork.DataStoreRepository.RecreateStagingTables(_dataflow.Dsd, _reportedComponents, isTimeAtTimeDimensionSupported, CancellationToken);

            // import to staging
            var codeTranslator = new CodeTranslator(UnitOfWork.CodeListRepository);
            await codeTranslator.FillDict(_dataflow, CancellationToken);
            var bulkImportResult = await UnitOfWork.DataStoreRepository.BulkInsertData(observations, _reportedComponents, codeTranslator, _dataflow, fullValidation, isTimeAtTimeDimensionSupported, false, CancellationToken);
            
            Assert.AreEqual(0, bulkImportResult.Errors.Count, "No of errors during bulk insert.");
            Assert.AreEqual(obsCount, bulkImportResult.RowsCopied);

            // merge series
            var importSummary = await UnitOfWork.DataStoreRepository.MergeStagingTable(_dataflow, _reportedComponents,
                bulkImportResult.BatchActions, bulkImportResult.DataSetLevelAttributeRows, codeTranslator,
                tableVersion, fullValidation, CancellationToken);
            
            Assert.AreEqual(0, importSummary.ObservationLevelMergeResult.Errors.Count, "No of errors during merging to fact data.");
            // --------------------------------------------------------------------------

            // check view is created & if observations are returned
            var view = $"{MariaDbExtensions.DataSchema}_{_dataflow.Dsd.MariaDbDataDsdViewName((char)tableVersion)}";
            Assert.IsTrue(await DotStatDb.ViewExists(view, CancellationToken));

            var dbObsCount = (long)await DotStatDb.ExecuteScalarSqlAsync($"select count(*) from {view}", CancellationToken);
            Assert.AreEqual(obsCount, dbObsCount);

            // close transaction
            _dataflow.Dsd.LiveVersion = (char?)tableVersion;
            var success = await DotStatDbService.CloseTransaction(transaction, _dataflow, false, obsCount>0, true,
                MsAccess, false, false, null, CancellationToken);
            Assert.IsTrue(success);

            // ---------------------------------
            
            var dataQuery = new DataQueryImpl(
                 new RESTDataQueryCore($"data/{_dataflow.AgencyId},{_dataflow.Code},{_dataflow.Version}/{_dataQuery}"),
                 new InMemoryRetrievalManager(new SdmxObjectsImpl(_dataflow.Dsd.Base, _dataflow.Base)));

            var dbObservations = await UnitOfWork.ObservationRepository.GetObservations(
                    dataQuery,
                    _dataflow,
                    tableVersion,
                    isDataQuery: true,
                    CancellationToken
                )
                .ToArrayAsync(CancellationToken);

            Assert.AreEqual(dbObsCount, dbObservations.Length);
            Assert.AreEqual(_dataflow.Dimensions.Count(x => !x.Base.TimeDimension), (await observations.FirstAsync()).Observation.SeriesKey.Key.Count);
        }
        
        [Test, Order(3)]
        public async Task ImportFullValidation()
        {
            const DbTableVersion tableVersion = DbTableVersion.A;
            const TargetVersion targetVersion = TargetVersion.Live;
            const bool fullValidation = true;
            const bool isTimeAtTimeDimensionSupported = false;
            const string dataSource = "testFile.csv";

            var transactionId = await UnitOfWork.TransactionRepository.GetNextTransactionId(CancellationToken);
            var transaction = await UnitOfWork.TransactionRepository.CreateTransactionItem(transactionId, _dataflow.FullId, null, null, dataSource, TransactionType.Import, targetVersion, serviceId: null, CancellationToken);

            // create dataflow DB objects 
            var tryNewTransactionResult = await DotStatDbService.TryNewTransaction(transaction, _dataflow, MsAccess, CancellationToken);
            Assert.IsTrue(tryNewTransactionResult);

            // ------ Logic bellow normally in SqlConsumer of Transfer service ----------

            // Wipe and/or create staging table and target tables
            await UnitOfWork.DataStoreRepository.RecreateStagingTables(_dataflow.Dsd, _reportedComponents, isTimeAtTimeDimensionSupported, CancellationToken);

            var observations = AsyncEnumerable.Empty<ObservationRow>();

            // import to staging
            var codeTranslator = new CodeTranslator(UnitOfWork.CodeListRepository);
            await codeTranslator.FillDict(_dataflow, CancellationToken);
            var bulkImportResult = await UnitOfWork.DataStoreRepository.BulkInsertData(observations, _reportedComponents, codeTranslator, _dataflow, fullValidation, isTimeAtTimeDimensionSupported, false, CancellationToken);

            //---------------------
            await _keyableDatabaseValidator.Validate(DotStatDb, codeTranslator, _dataflow, _dataflow.Dsd.Attributes.ToList(), tableVersion, 100, CancellationToken);
            bulkImportResult.Errors.AddRange(_keyableDatabaseValidator.GetErrors());
            Assert.GreaterOrEqual(bulkImportResult.Errors.Count, 0); //Set back to 1 this validation is re-introduced with the ticket https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/375
            Assert.GreaterOrEqual(bulkImportResult.Errors.Count(e => e.Type == ValidationErrorType.MandatoryAttributeWithNullValueInDatabase), 0); //Set back to 1 this validation is re-introduced with the ticket https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/375

            // close transaction
            _dataflow.Dsd.LiveVersion = (char?)tableVersion;
            var success = await DotStatDbService.CloseTransaction(transaction, _dataflow, false, bulkImportResult.RowsCopied > 0, true,
                MsAccess, false, false, null, CancellationToken);
            Assert.IsTrue(success);
        }


        
    }
}
