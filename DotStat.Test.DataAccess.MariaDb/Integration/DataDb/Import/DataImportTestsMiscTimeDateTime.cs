﻿using System.Threading.Tasks;
using NUnit.Framework;

namespace DotStat.Test.DataAccess.MariaDb.Integration.DataDb.Import
{
    [TestFixture, NonParallelizable]
    public class DataImportTestsMiscTimeDateTime : BaseDataImportTestsMiscTime
    {
        public DataImportTestsMiscTimeDateTime() : base("sdmx/ABS,DF_WPI_OS_2,0.0.3.xml")
        {
        }

        [Test, Order(1)]
        public async Task ImportTest()
        {
            // Import with time supported at time dim, using full validation. 2 errors
            await Import(true, true, CancellationToken, 1); //TODO: change back to 2 when proper validation of fractional seconds are implemented in .Stat instead of SdmxSource 

            // Import with time supported at time dim, using basic validation. 2 errors
            await Import(true, false, CancellationToken, 1); //TODO: change back to 2 when proper validation of fractional seconds are implemented in .Stat instead of SdmxSource
        }

        /// <summary>
        ///  runs only after Import Test
        /// </summary>
        [Test, Order(2)]
        public async Task CopyToNewVersionTest()
        {
            await CopyToNewVersion(CancellationToken);
        }

        /// <summary>
        ///  runs only after Import Test
        /// </summary>
        [Test, Order(3)]
        public async Task CheckTypeOfPeriodColumnsTest()
        {
            await CheckSupportOfTimeAtTimeDimension(true, CancellationToken);
        }
    }
}
