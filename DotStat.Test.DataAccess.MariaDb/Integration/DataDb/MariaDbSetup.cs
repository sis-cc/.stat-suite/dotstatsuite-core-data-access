﻿using NUnit.Framework;

namespace DotStat.Test.DataAccess.MariaDb.Integration.DataDb
{
    [SetUpFixture]
    internal sealed class MariaDbSetup : BaseMariaDbSetup
    {
        private readonly string _sqlConnectionString;
        
        public MariaDbSetup()
        {
            _sqlConnectionString = Configuration.SpacesInternal[0].DotStatSuiteCoreDataDbConnectionString;
        }

        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            CreateDb(_sqlConnectionString, "Data");
        }

        [OneTimeTearDown]
        public void OneTimeTearDown()
        {
            DeleteDb(_sqlConnectionString);
        }
    }
}
