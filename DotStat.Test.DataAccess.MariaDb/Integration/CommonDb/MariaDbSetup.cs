﻿using NUnit.Framework;

namespace DotStat.Test.DataAccess.MariaDb.Integration.CommonDb
{
    [SetUpFixture]
    internal sealed class MariaDbSetup : BaseMariaDbSetup
    {
        private readonly string _sqlConnectionString;
        
        public MariaDbSetup()
        {
            _sqlConnectionString = Configuration.DotStatSuiteCoreCommonDbConnectionString;
        }

        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            CreateDb(_sqlConnectionString, "Common");
        }

        [OneTimeTearDown]
        public void OneTimeTearDown()
        {
            DeleteDb(_sqlConnectionString);
        }
    }
}
