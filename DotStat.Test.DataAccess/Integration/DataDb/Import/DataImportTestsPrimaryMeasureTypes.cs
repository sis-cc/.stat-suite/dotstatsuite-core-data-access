﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DotStat.Db;
using DotStat.Db.Util;
using DotStat.Domain;
using NUnit.Framework;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Model.Data;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Data;

namespace DotStat.Test.DataAccess.Integration.DataDb.Import
{
    [NonParallelizable]
    [TestFixture("sdmx/OBS_TYPE_TEST.xml")]
    public sealed class DataImportTestsPrimaryMeasureTypes : BaseDataImportTestsPrimaryMeasureTypes
    {
        public DataImportTestsPrimaryMeasureTypes(string structure) : base(structure)
        {
        }

        [TestCase(TextEnumType.String, null, null, null, StagingRowActionEnum.Merge, false, true)]
        //Returns 1 because all components are missing
        [TestCase(TextEnumType.String, null, "1", null, StagingRowActionEnum.Delete, false, false)]
        [TestCase(TextEnumType.String, "#N/A", "#N/A", "#N/A", StagingRowActionEnum.Merge, false, false)]
        [TestCase(TextEnumType.String, "#N/A", "1", null, StagingRowActionEnum.Delete, false, false)]
        [TestCase(TextEnumType.String, "NaN", "NaN", "NaN", StagingRowActionEnum.Merge, false, false)]
        [TestCase(TextEnumType.String, "NaN", "1", null, StagingRowActionEnum.Delete, false, false)]
        [TestCase(TextEnumType.String, "normal value", "normal value", "normal value", StagingRowActionEnum.Merge, false, false)]
        [TestCase(TextEnumType.String, "normal value", "1", null, StagingRowActionEnum.Delete, false, false)]
        [TestCase(TextEnumType.Decimal, "#N/A", null, null, StagingRowActionEnum.Merge, true, false)]
        [TestCase(TextEnumType.Decimal, "#N/A", null, null, StagingRowActionEnum.Delete, true, false)]
        [TestCase(TextEnumType.Decimal, "NaN", null, null, StagingRowActionEnum.Merge, true, false)]
        [TestCase(TextEnumType.Decimal, "NaN", null, null, StagingRowActionEnum.Delete, true, false)]
        [TestCase(TextEnumType.Decimal, "5.5", "5.5", "5.5", StagingRowActionEnum.Merge, false, false)]
        [TestCase(TextEnumType.Decimal, "5.5", "1", null, StagingRowActionEnum.Delete, false, false)]
        [TestCase(TextEnumType.Integer, "#N/A", null, null, StagingRowActionEnum.Merge, true, false)]
        [TestCase(TextEnumType.Integer, "#N/A", null, null, StagingRowActionEnum.Delete, true, false)]
        [TestCase(TextEnumType.Integer, "NaN", null, null, StagingRowActionEnum.Merge, true, false)]
        [TestCase(TextEnumType.Integer, "NaN", null, null, StagingRowActionEnum.Delete, true, false)]
        [TestCase(TextEnumType.Integer, "5", "5", "5", StagingRowActionEnum.Merge, false, false)]
        [TestCase(TextEnumType.Integer, "5", "1", null, StagingRowActionEnum.Delete, false, false)]
        [TestCase(TextEnumType.Long, "#N/A", null, null, StagingRowActionEnum.Merge, true, false)]
        [TestCase(TextEnumType.Long, "#N/A", null, null, StagingRowActionEnum.Delete, true, false)]
        [TestCase(TextEnumType.Long, "NaN", null, null, StagingRowActionEnum.Merge, true, false)]
        [TestCase(TextEnumType.Long, "NaN", null, null, StagingRowActionEnum.Delete, true, false)]
        [TestCase(TextEnumType.Long, "5", "5", "5", StagingRowActionEnum.Merge, false, false)]
        [TestCase(TextEnumType.Long, "5", "1", null, StagingRowActionEnum.Delete, false, false)]
        [TestCase(TextEnumType.Short, "#N/A", null, null, StagingRowActionEnum.Merge, true, false)]
        [TestCase(TextEnumType.Short, "#N/A", null, null, StagingRowActionEnum.Delete, true, false)]
        [TestCase(TextEnumType.Short, "NaN", null, null, StagingRowActionEnum.Merge, true, false)]
        [TestCase(TextEnumType.Short, "NaN", null, null, StagingRowActionEnum.Delete, true, false)]
        [TestCase(TextEnumType.Short, "5", "5", "5", StagingRowActionEnum.Merge, false, false)]
        [TestCase(TextEnumType.Short, "5", "1", null, StagingRowActionEnum.Delete, false, false)]
        [TestCase(TextEnumType.Boolean, "#N/A", null, null, StagingRowActionEnum.Merge, true, false)]
        [TestCase(TextEnumType.Boolean, "#N/A", null, null, StagingRowActionEnum.Delete, true, false)]
        [TestCase(TextEnumType.Boolean, "NaN", null, null, StagingRowActionEnum.Merge, true, false)]
        [TestCase(TextEnumType.Boolean, "NaN", null, null, StagingRowActionEnum.Delete, true, false)]
        [TestCase(TextEnumType.Boolean, "false", "0", "0", StagingRowActionEnum.Merge, false, false)]
        [TestCase(TextEnumType.Boolean, "false", "1", null, StagingRowActionEnum.Delete, false, false)]
        [TestCase(TextEnumType.Float, "#N/A", null, null, StagingRowActionEnum.Merge, true, false)]
        [TestCase(TextEnumType.Float, "#N/A", null, null, StagingRowActionEnum.Delete, true, false)]
        [TestCase(TextEnumType.Float, "NaN", null, null, StagingRowActionEnum.Merge, true, false)]
        [TestCase(TextEnumType.Float, "NaN", null, null, StagingRowActionEnum.Delete, true, false)]
        [TestCase(TextEnumType.Float, "5.5", "5.5", "5.5", StagingRowActionEnum.Merge, false, false)]
        [TestCase(TextEnumType.Float, "5.5", "1", null, StagingRowActionEnum.Delete, false, false)]
        [TestCase(TextEnumType.Double, "#N/A", null, null, StagingRowActionEnum.Merge, true, false)]
        [TestCase(TextEnumType.Double, "#N/A", null, null, StagingRowActionEnum.Delete, true, false)]
        [TestCase(TextEnumType.Double, "NaN", null, null, StagingRowActionEnum.Merge, true, false)]
        [TestCase(TextEnumType.Double, "NaN", null, null, StagingRowActionEnum.Delete, true, false)]
        [TestCase(TextEnumType.Double, "5.5", "5.5", "5.5", StagingRowActionEnum.Merge, false, false)]
        [TestCase(TextEnumType.Double, "5.5", "1", null, StagingRowActionEnum.Delete, false, false)]
        [TestCase(TextEnumType.GregorianYear, "#N/A", null, null, StagingRowActionEnum.Merge, true, false)]
        [TestCase(TextEnumType.GregorianYear, "#N/A", null, null, StagingRowActionEnum.Delete, true, false)]
        [TestCase(TextEnumType.GregorianYear, "NaN", null, null, StagingRowActionEnum.Merge, true, false)]
        [TestCase(TextEnumType.GregorianYear, "NaN", null, null, StagingRowActionEnum.Delete, true, false)]
        [TestCase(TextEnumType.GregorianYear, "2020", "2020", "2020", StagingRowActionEnum.Merge, false, false)]
        [TestCase(TextEnumType.GregorianYear, "2020", "1", null, StagingRowActionEnum.Delete, false, false)]

        public async Task CheckImportedMeasureTypes(TextEnumType textType, string observationValueStr, string expectedValueInStaging, string expectedValueInFact, StagingRowActionEnum action, bool shouldGetValidationError = false, bool skipped = false)
        {
            await Execute(textType, observationValueStr, expectedValueInStaging, expectedValueInFact, action, shouldGetValidationError, skipped);
        }
    }

    [NonParallelizable]
    [TestFixture("sdmx/OBS_TYPE_TEST_with_intentionally_missing_annotation.xml")]
    public sealed class DataImportTestsPrimaryMeasureTypesWithIntentionallyMissing : BaseDataImportTestsPrimaryMeasureTypes
    {
        public DataImportTestsPrimaryMeasureTypesWithIntentionallyMissing(string structure) : base(structure)
        {}

        [TestCase(TextEnumType.String, null, null, null, StagingRowActionEnum.Merge, false, true)]
        //Staging table should store a value of 1 because all components are missing
        [TestCase(TextEnumType.String, null, "1", null, StagingRowActionEnum.Delete, false, false)]
        [TestCase(TextEnumType.String, "#N/A", "#N/A", "#N/A", StagingRowActionEnum.Merge, false, false)]
        [TestCase(TextEnumType.String, "#N/A", "1", null, StagingRowActionEnum.Delete, false, false)]
        [TestCase(TextEnumType.String, "NaN", "NaN", "NaN", StagingRowActionEnum.Merge, false, false)]
        [TestCase(TextEnumType.String, "NaN", "1", null, StagingRowActionEnum.Delete, false, false)]
        [TestCase(TextEnumType.String, "normal value", "normal value", "normal value", StagingRowActionEnum.Merge, false, false)]
        [TestCase(TextEnumType.String, "normal value", "1", null, StagingRowActionEnum.Delete, false, false)]
        [TestCase(TextEnumType.Decimal, "#N/A", null, null, StagingRowActionEnum.Merge, true, false)]
        [TestCase(TextEnumType.Decimal, "#N/A", null, null, StagingRowActionEnum.Delete, true, false)]
        [TestCase(TextEnumType.Decimal, "NaN", null, null, StagingRowActionEnum.Merge, true, false)]
        [TestCase(TextEnumType.Decimal, "NaN", null, null, StagingRowActionEnum.Delete, true, false)]
        [TestCase(TextEnumType.Decimal, "5.5", "5.5", "5.5", StagingRowActionEnum.Merge, false, false)]
        [TestCase(TextEnumType.Decimal, "5.5", "1", null, StagingRowActionEnum.Delete, false, false)]
        [TestCase(TextEnumType.Integer, "#N/A", null, null, StagingRowActionEnum.Merge, true, false)]
        [TestCase(TextEnumType.Integer, "#N/A", null, null, StagingRowActionEnum.Delete, true, false)]
        [TestCase(TextEnumType.Integer, "NaN", null, null, StagingRowActionEnum.Merge, true, false)]
        [TestCase(TextEnumType.Integer, "NaN", null, null, StagingRowActionEnum.Delete, true, false)]
        [TestCase(TextEnumType.Integer, "5", "5", "5", StagingRowActionEnum.Merge, false, false)]
        [TestCase(TextEnumType.Integer, "5", "1", null, StagingRowActionEnum.Delete, false, false)]
        [TestCase(TextEnumType.Long, "#N/A", null, null, StagingRowActionEnum.Merge, true, false)]
        [TestCase(TextEnumType.Long, "#N/A", null, null, StagingRowActionEnum.Delete, true, false)]
        [TestCase(TextEnumType.Long, "NaN", null, null, StagingRowActionEnum.Merge, true, false)]
        [TestCase(TextEnumType.Long, "NaN", null, null, StagingRowActionEnum.Delete, true, false)]
        [TestCase(TextEnumType.Long, "5", "5", "5", StagingRowActionEnum.Merge, false, false)]
        [TestCase(TextEnumType.Long, "5", "1", null, StagingRowActionEnum.Delete, false, false)]
        [TestCase(TextEnumType.Short, "#N/A", null, null, StagingRowActionEnum.Merge, true, false)]
        [TestCase(TextEnumType.Short, "#N/A", null, null, StagingRowActionEnum.Delete, true, false)]
        [TestCase(TextEnumType.Short, "NaN", null, null, StagingRowActionEnum.Merge, true, false)]
        [TestCase(TextEnumType.Short, "NaN", null, null, StagingRowActionEnum.Delete, true, false)]
        [TestCase(TextEnumType.Short, "5", "5", "5", StagingRowActionEnum.Merge, false, false)]
        [TestCase(TextEnumType.Short, "5", "1", null, StagingRowActionEnum.Delete, false, false)]
        [TestCase(TextEnumType.Boolean, "#N/A", null, null, StagingRowActionEnum.Merge, true, false)]
        [TestCase(TextEnumType.Boolean, "#N/A", null, null, StagingRowActionEnum.Delete, true, false)]
        [TestCase(TextEnumType.Boolean, "NaN", null, null, StagingRowActionEnum.Merge, true, false)]
        [TestCase(TextEnumType.Boolean, "NaN", null, null, StagingRowActionEnum.Delete, true, false)]
        [TestCase(TextEnumType.Boolean, "false", "0", "0", StagingRowActionEnum.Merge, false, false)]
        [TestCase(TextEnumType.Boolean, "false", "1", null, StagingRowActionEnum.Delete, false, false)]
        [TestCase(TextEnumType.Float, "#N/A", null, null, StagingRowActionEnum.Merge, true, false)]
        [TestCase(TextEnumType.Float, "#N/A", null, null, StagingRowActionEnum.Delete, true, false)]
        [TestCase(TextEnumType.Float, "NaN", "NaN", "NaN", StagingRowActionEnum.Merge, false, false)]
        [TestCase(TextEnumType.Float, "NaN", "1", null, StagingRowActionEnum.Delete, false, false)]
        [TestCase(TextEnumType.Float, "5.5", "5.5", "5.5", StagingRowActionEnum.Merge, false, false)]
        [TestCase(TextEnumType.Float, "5.5", "1", null, StagingRowActionEnum.Delete, false, false)]
        [TestCase(TextEnumType.Double, "#N/A", null, null, StagingRowActionEnum.Merge, true, false)]
        [TestCase(TextEnumType.Double, "#N/A", null, null, StagingRowActionEnum.Delete, true, false)]
        [TestCase(TextEnumType.Double, "NaN", "NaN", "NaN", StagingRowActionEnum.Merge, false, false)]
        [TestCase(TextEnumType.Double, "NaN", "1", null, StagingRowActionEnum.Delete, false, false)]
        [TestCase(TextEnumType.Double, "5.5", "5.5", "5.5", StagingRowActionEnum.Merge, false, false)]
        [TestCase(TextEnumType.Double, "5.5", "1", null, StagingRowActionEnum.Delete, false, false)]
        [TestCase(TextEnumType.GregorianYear, "#N/A", null, null, StagingRowActionEnum.Merge, true, false)]
        [TestCase(TextEnumType.GregorianYear, "#N/A", null, null, StagingRowActionEnum.Delete, true, false)]
        [TestCase(TextEnumType.GregorianYear, "NaN", null, null, StagingRowActionEnum.Merge, true, false)]
        [TestCase(TextEnumType.GregorianYear, "NaN", null, null, StagingRowActionEnum.Delete, true, false)]
        [TestCase(TextEnumType.GregorianYear, "2020", "2020", "2020", StagingRowActionEnum.Merge, false, false)]
        [TestCase(TextEnumType.GregorianYear, "2020", "1", null, StagingRowActionEnum.Delete, false, false)]

        public async Task CheckImportedMeasureTypes(TextEnumType textType, string observationValueStr, string expectedValueInStaging, string expectedValueInFact, StagingRowActionEnum action, bool shouldThrowException = false, bool skipped = false)
        {
            await Execute(textType, observationValueStr, expectedValueInStaging, expectedValueInFact, action, shouldThrowException, skipped);
        }
    }

    public abstract class BaseDataImportTestsPrimaryMeasureTypes : BaseDbIntegrationTests
    {
        protected Dataflow dataFlow;

        protected BaseDataImportTestsPrimaryMeasureTypes(string structure) : base(structure)
        {}

        [OneTimeTearDown]
        public async Task TearDown()
        {
            await CleanUpStructure(dataFlow.Dsd);
        }

        protected async Task Execute(TextEnumType textType, string observationValueStr, string expectedValueInStaging, string expectedValueInFact, StagingRowActionEnum action, bool shouldGetValidationError, bool skipped)
        {
            dataFlow = MsAccess.GetDataflow();
            dataFlow.Dsd.SetPrimaryMeasure(GetDsd(textType).PrimaryMeasure);

            //Test case setup
            var seriesKey = new List<IKeyValue> { new KeyValueImpl("A", "FREQ") };
            var key = new KeyableImpl(dataFlow.Base, dataFlow.Dsd.Base, seriesKey, null);
            var observation = new ObservationImpl(key, "2020", observationValueStr, new List<IKeyValue>(), crossSectionValue: null);
            var observationRows = new List<ObservationRow> { new(1, action, observation, false) }.ToAsyncEnumerable();

            var tableVersion = DbTableVersion.A;
            var targetVersion = TargetVersion.Live;
            const bool fullValidation = false;
            const bool isTimeAtTimeDimensionSupported = false;
            const string dataSource = "testFile.csv";

            var reportedComponents = GetReportedComponents(dataFlow);

            var transactionId = await UnitOfWork.TransactionRepository.GetNextTransactionId(CancellationToken);
            var transaction = await UnitOfWork.TransactionRepository.CreateTransactionItem(transactionId, dataFlow.FullId, null,
                null, dataSource, TransactionType.Import, targetVersion, serviceId: null, CancellationToken);

            await DotStatDbService.TryNewTransaction(transaction, dataFlow, MsAccess, CancellationToken);
            await UnitOfWork.DataStoreRepository.RecreateStagingTables(dataFlow.Dsd, reportedComponents, isTimeAtTimeDimensionSupported, CancellationToken);

            var codeTranslator = new CodeTranslator(UnitOfWork.CodeListRepository);
            await codeTranslator.FillDict(dataFlow, CancellationToken);

            var bulkImportResult = await UnitOfWork.DataStoreRepository.BulkInsertData(observationRows, reportedComponents, codeTranslator, dataFlow, fullValidation, isTimeAtTimeDimensionSupported, false, CancellationToken);

            if (shouldGetValidationError)
            {
                Assert.IsTrue(bulkImportResult.Errors.Count > 0);

                await CleanUp(transaction);

                return;
            }

            var table = dataFlow.Dsd.SqlStagingTable();
            var sql = $@"SELECT [VALUE] FROM [{DotStatDb.DataSchema}].{table};";
            var valueInDb = await DotStatDb.ExecuteScalarSqlAsync(sql, CancellationToken);
            Assert.IsNotNull(valueInDb);
            
            if (valueInDb == DBNull.Value)
            {
                valueInDb = null;
            }
            
            Assert.AreEqual(expectedValueInStaging, valueInDb?.ToString());

            await UnitOfWork.DataStoreRepository.MergeStagingTable(dataFlow, reportedComponents,
                bulkImportResult.BatchActions, bulkImportResult.DataSetLevelAttributeRows, codeTranslator, tableVersion, true,
                CancellationToken);

            var view = dataFlow.SqlDataDataflowViewName((char)tableVersion);

            if (action is StagingRowActionEnum.Merge)
            {
                valueInDb = await DotStatDb.ExecuteScalarSqlAsync($"SELECT [OBS_VALUE] FROM [{DotStatDb.DataSchema}].{view}", CancellationToken);
                
                if (valueInDb == DBNull.Value)
                {
                    valueInDb = null;
                }

                Assert.AreEqual(expectedValueInFact, valueInDb?.ToString());
            }

            var expectedObsCount = action is StagingRowActionEnum.Delete || skipped ? 0 : 1;
            var obsCountInDb = (int)await DotStatDb.ExecuteScalarSqlAsync($"SELECT COUNT(*) FROM [{DotStatDb.DataSchema}].{view}", CancellationToken);
            Assert.AreEqual(expectedObsCount, obsCountInDb);

            await CleanUp(transaction);
        }

        private async Task CleanUp(Transaction transaction)
        {
            await DotStatDbService.CloseTransaction(transaction, dataFlow, false, false, true,
                MsAccess, false, false, null, CancellationToken);

            var allDbComponents = UnitOfWork.ComponentRepository.GetAllComponents(CancellationToken).Result.ToList();
            var allMsdDbComponents = UnitOfWork.MetaMetadataComponentRepository.GetAllMetadataAttributeComponents(CancellationToken).Result.ToList();

            await DotStatDbService.CleanUpDsd(dataFlow.Dsd.DbId, false, this.MsAccess, allDbComponents, allMsdDbComponents, this.CancellationToken);
        }

        protected Dsd GetDsd(TextEnumType textType)
        {
            var sdmxObjects = MsAccess.GetSdmxObjects("sdmx/OBS_TYPE_TEST.xml");

            var dsdId = "OBS_TYPE_DSD_" + (textType == TextEnumType.TimesRange ? "TIMERANGE" : textType.ToString().ToUpper());
            var dsDataStructureObject = sdmxObjects.DataStructures.FirstOrDefault(d => d.Id.Equals(dsdId, StringComparison.InvariantCultureIgnoreCase));

            var dsd = new Dsd(dsDataStructureObject);
            dsd.SetPrimaryMeasure(new PrimaryMeasure(dsDataStructureObject.PrimaryMeasure));

            return dsd;
        }
    }
}
