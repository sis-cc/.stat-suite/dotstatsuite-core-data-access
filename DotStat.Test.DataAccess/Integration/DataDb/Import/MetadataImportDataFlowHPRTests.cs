﻿using System.Linq;
using System.Threading.Tasks;
using DotStat.Db;
using DotStat.Db.Util;
using DotStat.Domain;
using NUnit.Framework;

namespace DotStat.Test.DataAccess.Integration.DataDb.Import
{
    [NonParallelizable]
    [TestFixture("sdmx/CsvV2.xml")]
    [TestFixture("sdmx/TEST_CODELESS_DS.xml")]
    public sealed class MetadataImportDataFlowHPRTests : BaseDbIntegrationTests
    {
        private readonly Dataflow _dataflow;

        public MetadataImportDataFlowHPRTests(string structure) : base(structure)
        {
            _dataflow = this.GetDataflow();
        }

        [OneTimeTearDown]
        public async Task TearDown()
        {
            await CleanUpStructure(_dataflow.Dsd);
        }

        [Test]
        public async Task Import()
        {
            var tableVersion = DbTableVersion.A;
            var targetVersion = TargetVersion.Live;
            const bool fullValidation = false;
            const bool isTimeAtTimeDimensionSupported = false;
            const string dataSource = "testFile.csv";
            
            var reportedComponents = new ReportedComponents
            {
                Dimensions = _dataflow.Dimensions.ToList(),
                MetadataAttributes = _dataflow.Dsd.Msd.MetadataAttributes.ToList(),
                IsPrimaryMeasureReported = false,
                TimeDimension = _dataflow.Dsd.TimeDimension
            };
            
            var transactionId = await UnitOfWork.TransactionRepository.GetNextTransactionId(CancellationToken);
            var transaction= await UnitOfWork.TransactionRepository.CreateTransactionItem(transactionId, _dataflow.FullId, null, null, dataSource, TransactionType.Import, targetVersion, serviceId: null, CancellationToken);

            // create dataflow DB objects 
            var tryNewTransactionResult = await DotStatDbService.TryNewTransaction(transaction, _dataflow, MsAccess, CancellationToken);
            Assert.IsTrue(tryNewTransactionResult);

            // generate observations
            var obsCount = 50;
            var observations = ObservationGenerator.Generate(
                _dataflow.Dsd,
                    _dataflow,
                    false,
                    2018,
                    2020,
                    obsCount,
                    action: StagingRowActionEnum.Merge
                );

            Assert.AreEqual(obsCount, await observations.CountAsync());

            // Wipe and/or create staging table and target tables
            await UnitOfWork.MetadataStoreRepository.RecreateMetadataStagingTables(_dataflow.Dsd, CancellationToken);

            // import to staging
            var codeTranslator = new CodeTranslator(UnitOfWork.CodeListRepository);
            await codeTranslator.FillDict(_dataflow, CancellationToken);

            var bulkImportResult = await UnitOfWork.MetadataStoreRepository.BulkInsertMetadata(observations, reportedComponents, codeTranslator, _dataflow.Dsd,
                fullValidation, isTimeAtTimeDimensionSupported, CancellationToken);
            Assert.AreEqual(0, bulkImportResult.Errors.Count, "No of errors during bulk insert.");
            
            // merge from staging
            var importSummary = await UnitOfWork.MetadataStoreRepository.MergeStagingTable(_dataflow, reportedComponents,
                bulkImportResult.BatchActions, bulkImportResult.DataSetLevelAttributeRows, codeTranslator, tableVersion, true, CancellationToken);
            Assert.AreEqual(0, importSummary.Errors.Count, "No of errors during merging to fact data.");
            Assert.AreEqual(obsCount, importSummary.ObservationLevelMergeResult.TotalCount);

            // close transaction
            _dataflow.Dsd.LiveVersion = (char?) tableVersion;
            Assert.IsTrue(await DotStatDbService.CloseTransaction(transaction, _dataflow, false, true, true,
                MsAccess, false, true, null, CancellationToken));

            var view = _dataflow.SqlMetadataDataflowViewName((char)tableVersion, true);
            Assert.IsTrue(await DotStatDb.ViewExists(view, CancellationToken));

            var dbObsCount = (int)await DotStatDb.ExecuteScalarSqlAsync($"select count(*) from [data].{view}", CancellationToken);
            Assert.AreEqual(obsCount, dbObsCount);

            view = _dataflow.SqlMetadataDataflowViewName((char)tableVersion);
            Assert.IsTrue(await DotStatDb.ViewExists(view, CancellationToken));

            dbObsCount = (int)await DotStatDb.ExecuteScalarSqlAsync($"select count(*) from [data].{view}", CancellationToken);
            Assert.AreEqual(obsCount, dbObsCount);
        }
    }
}
