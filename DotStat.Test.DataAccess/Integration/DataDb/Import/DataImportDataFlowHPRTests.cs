﻿using System.Linq;
using System.Threading.Tasks;
using DotStat.Db;
using DotStat.Db.Util;
using DotStat.Domain;
using DotStat.Test.Moq;
using NUnit.Framework;
using Org.Sdmxsource.Sdmx.Api.Constants;

namespace DotStat.Test.DataAccess.Integration.DataDb.Import
{
    [NonParallelizable]
    public sealed class DataImportDataFlowHPRTests : BaseDbIntegrationTests
    {
        //private readonly Dataflow _dataflow;

        public DataImportDataFlowHPRTests() : base(null)
        {
            //_dataflow = this.GetDataflow();
            Configuration.SpacesInternal[0].OptimizedForHighPerformanceReads = true;
        }

        //[OneTimeTearDown]
        //public async Task TearDown()
        //{
        //    await CleanUpStructure(_dataflow.Dsd);
        //}

        [Test]
        [TestCase("sdmx/DSD-NO-ATTR.xml", "data/DATA-DF-NO-ATTR.csv")]
        [TestCase("sdmx/DSD-DS-ATTR.xml", "data/DATA-DF-DS-ATTR.csv")]
        [TestCase("sdmx/DSD-DIMGROUP-ATTR.xml", "data/DATA-DF-DIMGROUP-ATTR.csv")]
        [TestCase("sdmx/DSD-OBS-ATTR.xml", "data/DATA-DF-OBS-ATTR.csv")]
        public async Task Import(string structureFile, string dataFile)
        {
            var _dataflow = 1;

            var dataflow = GetDataflow(structureFile);

            var mappingStoreDataAccess = new TestMappingStoreDataAccess(structureFile);

            var tableVersion = DbTableVersion.A;
            var targetVersion = TargetVersion.Live;
            const bool fullValidation = false;
            const bool isTimeAtTimeDimensionSupported = false;
            
            var reportedComponents = new ReportedComponents
            {
                Dimensions = dataflow.Dimensions.ToList(),
                DatasetAttributes = dataflow.Dsd.Attributes
                    .Where(a => a.Base.AttachmentLevel is AttributeAttachmentLevel.DataSet or AttributeAttachmentLevel.Null)
                    .ToList(),
                SeriesAttributesWithNoTimeDim = dataflow.Dsd.Attributes
                    .Where(a => a.Base.AttachmentLevel is AttributeAttachmentLevel.Group or AttributeAttachmentLevel.DimensionGroup)
                    .ToList(),
                ObservationAttributes = dataflow.Dsd.Attributes
                    .Where(a => a.Base.AttachmentLevel == AttributeAttachmentLevel.Observation)
                    .ToList(),
                IsPrimaryMeasureReported = true,
                TimeDimension = dataflow.Dsd.TimeDimension
            };

            var transactionId = await UnitOfWork.TransactionRepository.GetNextTransactionId(CancellationToken);
            var transaction = await UnitOfWork.TransactionRepository.CreateTransactionItem(transactionId,
                dataflow.FullId, null, null, dataFile, TransactionType.Import, targetVersion, serviceId: null,
                CancellationToken);

            // create dataflow DB objects 
            var tryNewTransactionResult = await DotStatDbService.TryNewTransaction(transaction, dataflow, mappingStoreDataAccess, CancellationToken);
            Assert.IsTrue(tryNewTransactionResult);

            // generate observations
            var obsCount = 50;
            var observations = ObservationGenerator.Generate(
                dataflow.Dsd,
                    dataflow,
                    true,
                    2020,
                    2024,
                    obsCount,
                    action: StagingRowActionEnum.Merge
                );

            Assert.AreEqual(obsCount, await observations.CountAsync());

            // Wipe and/or create staging table and target tables
            await UnitOfWork.DataStoreRepository.RecreateStagingTables(dataflow.Dsd, reportedComponents, isTimeAtTimeDimensionSupported,
                CancellationToken);

            // import to staging
            var codeTranslator = new CodeTranslator(UnitOfWork.CodeListRepository);
            await codeTranslator.FillDict(dataflow, CancellationToken);

            var bulkImportResult = await UnitOfWork.DataStoreRepository.BulkInsertData(observations, reportedComponents, codeTranslator, dataflow,
                fullValidation, isTimeAtTimeDimensionSupported, false, CancellationToken);
            Assert.AreEqual(0, bulkImportResult.Errors.Count, "No of errors during bulk insert.");
            
            // merge from staging
            var importSummary = await UnitOfWork.DataStoreRepository.MergeStagingTable(dataflow, reportedComponents,
                bulkImportResult.BatchActions, bulkImportResult.DataSetLevelAttributeRows, codeTranslator, tableVersion,
                true, CancellationToken);
            Assert.AreEqual(0, importSummary.Errors.Count, "No of errors during merging to fact data.");
            Assert.AreEqual(obsCount, importSummary.ObservationLevelMergeResult.TotalCount);

            // close transaction
            dataflow.Dsd.LiveVersion = (char?) tableVersion;
            Assert.IsTrue(await DotStatDbService.CloseTransaction(transaction, dataflow, false, true, true,
                mappingStoreDataAccess, true, false, null, CancellationToken));

            var view = dataflow.SqlDataDataflowViewName((char)tableVersion);
            Assert.IsTrue(await DotStatDb.ViewExists(view, CancellationToken));

            var dbObsCount = (int)await DotStatDb.ExecuteScalarSqlAsync($"select count(*) from [data].{view}", CancellationToken);
            Assert.AreEqual(obsCount, dbObsCount);

            await CleanUpStructure(dataflow.Dsd);
        }
    }
}
