﻿using System.Linq;
using System.Threading.Tasks;
using DotStat.Db;
using DotStat.Db.Util;
using DotStat.Domain;
using NUnit.Framework;

namespace DotStat.Test.DataAccess.Integration.DataDb.Import
{
    [NonParallelizable]
    [TestFixture("sdmx/CsvV2.xml", "........./?startPeriod=1900&endPeriod=2079-01")]
    public sealed class MetadataValidationDsdTests : BaseDbIntegrationTests
    {
        private readonly Dsd _dsd;
        private readonly string _dataQuery;

        public MetadataValidationDsdTests(string structure, string dataQuery) : base(structure)
        {
            _dsd = this.GetDataflow().Dsd;
            _dataQuery = dataQuery;
        }

        [OneTimeTearDown]
        public async Task TearDown()
        {
            await CleanUpStructure(_dsd);
        }

        [Test]
        public async Task ImportBasicValidation()
        {
            const DbTableVersion tableVersion = DbTableVersion.A;
            const TargetVersion targetVersion = TargetVersion.Live;
            const bool fullValidation = false;
            const bool isTimeAtTimeDimensionSupported = false;
            const string dataSource = "testFile.csv";
            
            var reportedComponents = new ReportedComponents
            {
                Dimensions = _dsd.Dimensions.ToList(),
                MetadataAttributes = _dsd.Msd.MetadataAttributes.ToList(),
                TimeDimension = _dsd.TimeDimension
            };

            var transactionId = await UnitOfWork.TransactionRepository.GetNextTransactionId(CancellationToken);
            var transaction = await UnitOfWork.TransactionRepository.CreateTransactionItem(transactionId, _dsd.FullId, null, null, dataSource, TransactionType.Import, targetVersion, serviceId: null, CancellationToken);

            // create dataflow DB objects 
            var tryNewTransactionResult = await DotStatDbService.TryNewTransaction(transaction, _dsd, MsAccess, CancellationToken);
            Assert.IsTrue(tryNewTransactionResult);

            // generate observations
            const int obsCount = 1000;
            var observations = ObservationGenerator.Generate(
                _dsd,
                null,
                false,
                1980,
                2020,
                obsCount, null,
                false,
                action: StagingRowActionEnum.Merge
            );

            Assert.AreEqual(obsCount, await observations.CountAsync());

            // ------ Logic bellow normally in SqlConsumer of Transfer service ----------

            // Wipe and/or create staging table and target tables
            await UnitOfWork.MetadataStoreRepository.RecreateMetadataStagingTables(_dsd, CancellationToken);

            // import to staging
            var codeTranslator = new CodeTranslator(UnitOfWork.CodeListRepository);
            await codeTranslator.FillDict(_dsd, CancellationToken);
            var bulkImportResult = await UnitOfWork.MetadataStoreRepository.BulkInsertMetadata(observations, reportedComponents, codeTranslator, _dsd, fullValidation, isTimeAtTimeDimensionSupported, CancellationToken);
            Assert.AreEqual(0, bulkImportResult.Errors.Count, "No of errors during bulk insert.");
            Assert.AreEqual(obsCount, bulkImportResult.RowsCopied);

            // merge from staging
            var importSummary = await UnitOfWork.MetadataStoreRepository.MergeStagingTable(_dsd, reportedComponents,
                bulkImportResult.BatchActions, bulkImportResult.DataSetLevelAttributeRows, codeTranslator, tableVersion, false, CancellationToken);
            Assert.AreEqual(0, importSummary.Errors.Count, "No of errors during merging to META.");
            // --------------------------------------------------------------------------

            // check view is created & if metadata rows are returned
            var view = _dsd.SqlMetadataDsdViewName((char)tableVersion);
            Assert.IsTrue(await DotStatDb.ViewExists(view, CancellationToken));

            var metadataCount = (int)await DotStatDb.ExecuteScalarSqlAsync($"select count(*) from [data].{view}", CancellationToken);
            Assert.AreEqual(obsCount, metadataCount);

            // close transaction
            _dsd.LiveVersion = (char?)tableVersion;
            var success = await DotStatDbService.CloseTransaction(transaction, _dsd, false, false, true,
                MsAccess, false, false, null, CancellationToken);
            Assert.IsTrue(success);

            // ---------------------------------

            //var sqlRepo = new SqlObservationRepository(new SqlServerDbManager(Configuration, Configuration));

            //var dataQuery = new DataQueryImpl(
            //     new RESTDataQueryCore($"data/{_dsd.AgencyId},{_dsd.Code},{_dsd.Version}/{_dataQuery}"),
            //     new InMemoryRetrievalManager(new SdmxObjectsImpl(_dsd.Base, _dsd.Base)));

            //var dbObservations = sqlRepo.GetObservations(
            //        dataQuery,
            //        _dsd,
            //        codeTranslator,
            //        _dataspace.Id,
            //        targetVersion
            //    )
            //    .ToArray();

            //Assert.AreEqual(dbObsCount, dbObservations.Length);
            //Assert.AreEqual(_dsd.Dimensions.Count(x => !x.Base.TimeDimension), observations[0].SeriesKey.Key.Count);
        }
        
        [Test]
        public async Task ImportFullValidation()
        {
            const DbTableVersion tableVersion = DbTableVersion.A;
            const TargetVersion targetVersion = TargetVersion.Live;
            const bool fullValidation = true;
            const bool isTimeAtTimeDimensionSupported = false;
            const string dataSource = "testFile.csv";
            
            var reportedComponents = new ReportedComponents
            {
                Dimensions = _dsd.Dimensions.ToList(),
                MetadataAttributes = _dsd.Msd.MetadataAttributes.ToList()
            };

            var transactionId = await UnitOfWork.TransactionRepository.GetNextTransactionId(CancellationToken);
            var transaction = await UnitOfWork.TransactionRepository.CreateTransactionItem(
                transactionId, _dsd.FullId, null, null, dataSource, 
                TransactionType.Import, targetVersion, serviceId: null, CancellationToken);

            // create dataflow DB objects 
            var tryNewTransactionResult = await DotStatDbService.TryNewTransaction(transaction, _dsd, MsAccess, CancellationToken);
            Assert.IsTrue(tryNewTransactionResult);

            // ------ Logic bellow normally in SqlConsumer of Transfer service ----------

            // Wipe and/or create staging table and target tables
            await UnitOfWork.MetadataStoreRepository.RecreateMetadataStagingTables(_dsd, CancellationToken);

            // import to staging
            var codeTranslator = new CodeTranslator(UnitOfWork.CodeListRepository);
            await codeTranslator.FillDict(_dsd, CancellationToken);
            var bulkImportResult = await UnitOfWork.MetadataStoreRepository.BulkInsertMetadata(AsyncEnumerable.Empty<ObservationRow>(), reportedComponents, codeTranslator, _dsd, fullValidation, isTimeAtTimeDimensionSupported, CancellationToken);
           
            //---------------------
            Assert.GreaterOrEqual(bulkImportResult.Errors.Count, 0);
            
            // close transaction
            _dsd.LiveVersion = (char?)tableVersion;
            var success = await DotStatDbService.CloseTransaction(transaction, _dsd, false, true, true,
                MsAccess, false, false, null, CancellationToken);
            Assert.IsTrue(success);
        }
    }
}
