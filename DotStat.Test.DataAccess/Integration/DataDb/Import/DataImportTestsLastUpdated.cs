﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DotStat.Db;
using DotStat.Db.Util;
using DotStat.DB.Util;
using DotStat.Domain;
using NUnit.Framework;
using Org.Sdmxsource.Sdmx.Api.Model.Data;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Data;

namespace DotStat.Test.DataAccess.Integration.DataDb.Import
{
    [NonParallelizable]
    [TestFixture("sdmx/OECD-DF_TEST_DELETE-1.0-all_structures.xml")]
    public sealed class DataImportTestsLastUpdated : BaseDbIntegrationTests
    {
        private readonly Dataflow _dataflow;

        public DataImportTestsLastUpdated(string structure) : base(structure)
        {
            _dataflow = this.GetDataflow();
        }

        [OneTimeTearDown]
        public async Task TearDown()
        {
            await CleanUpStructure(_dataflow.Dsd);
        }

        [Test]
        public async Task LastUpdatedTests()
        {
            const DbTableVersion tableVersion = DbTableVersion.A;

            var seriesKey = new List<IKeyValue>
            {
                new KeyValueImpl("A", "DIM_1"),
                new KeyValueImpl("B", "DIM_2"),
            }; 
            var key = new KeyableImpl(_dataflow.Base, _dataflow.Dsd.Base, seriesKey, null);
            
            //Init values
            //,A,B,2020,4321,A,AB,B,DF
            var attributeValues = new List<IKeyValue>
            {
                new KeyValueImpl("A", "OBS_ATTR"),
                new KeyValueImpl("AB", "TS_ATTR"),
                new KeyValueImpl("B", "GR_ATTR"),
                new KeyValueImpl("DF", "DF_ATTR"),
            };
            var observationRows = new List<ObservationRow>{
                new ObservationRow(1, StagingRowActionEnum.Merge, new ObservationImpl(key, "2020", "4321", attributeValues, crossSectionValue: null), false){}
            }.ToAsyncEnumerable();
            await ImportObservations(observationRows);

            var fact = _dataflow.Dsd.SqlFactTable((char)tableVersion);
            var attr = _dataflow.Dsd.SqlDimGroupAttrTable((char)tableVersion);
            var dfAttr = _dataflow.Dsd.SqlDsdAttrTable((char)tableVersion);

            var maxObsLvlLastUpdated = (DateTime)await DotStatDb.ExecuteScalarSqlAsync($"SELECT MAX([LAST_UPDATED]) FROM [data].{fact}", CancellationToken);
            var maxSeriesLvlLastUpdated = (DateTime)await DotStatDb.ExecuteScalarSqlAsync($"SELECT MAX([LAST_UPDATED]) FROM [data].{attr}", CancellationToken);
            var maxDataflowLvlLastUpdated = (DateTime)await DotStatDb.ExecuteScalarSqlAsync($"SELECT MAX([LAST_UPDATED]) FROM [data].{dfAttr} WHERE DF_ID = {_dataflow.DbId}", CancellationToken);

            //Update with the same values
            attributeValues = new List<IKeyValue>
            {
                new KeyValueImpl("A", "OBS_ATTR"),
                new KeyValueImpl("AB", "TS_ATTR"),
                new KeyValueImpl("B", "GR_ATTR"),
                new KeyValueImpl("DF", "DF_ATTR"),
            };
            observationRows = new List<ObservationRow>{ 
                new ObservationRow(1, StagingRowActionEnum.Merge, new ObservationImpl(key, "2020", "4321", attributeValues, crossSectionValue: null), false){}
            }.ToAsyncEnumerable();
            await ImportObservations(observationRows);

            var newMaxObsLvlLastUpdated = (DateTime)await DotStatDb.ExecuteScalarSqlAsync($"SELECT MAX([LAST_UPDATED]) FROM [data].{fact}", CancellationToken);
            var newMaxSeriesLvlLastUpdated = (DateTime)await DotStatDb.ExecuteScalarSqlAsync($"SELECT MAX([LAST_UPDATED]) FROM [data].{attr}", CancellationToken);
            var newMaxDataflowLvlLastUpdated = (DateTime)await DotStatDb.ExecuteScalarSqlAsync($"SELECT MAX([LAST_UPDATED]) FROM [data].{dfAttr} WHERE DF_ID = {_dataflow.DbId}", CancellationToken);

            Assert.AreEqual(newMaxObsLvlLastUpdated, maxObsLvlLastUpdated);
            Assert.AreEqual(newMaxSeriesLvlLastUpdated, maxSeriesLvlLastUpdated);
            Assert.AreEqual(newMaxDataflowLvlLastUpdated, maxDataflowLvlLastUpdated);

            //Replace with the same values
            attributeValues = new List<IKeyValue>
            {
                new KeyValueImpl("A", "OBS_ATTR"),
                new KeyValueImpl("AB", "TS_ATTR"),
                new KeyValueImpl("B", "GR_ATTR"),
                new KeyValueImpl("DF", "DF_ATTR"),
            };
            observationRows = new List<ObservationRow>{
                new ObservationRow(1, StagingRowActionEnum.Replace, new ObservationImpl(key, "2020", "4321", attributeValues, crossSectionValue: null), false){}
            }.ToAsyncEnumerable();
            await ImportObservations(observationRows);

            newMaxObsLvlLastUpdated = (DateTime)await DotStatDb.ExecuteScalarSqlAsync($"SELECT MAX([LAST_UPDATED]) FROM [data].{fact}", CancellationToken);
            newMaxSeriesLvlLastUpdated = (DateTime)await DotStatDb.ExecuteScalarSqlAsync($"SELECT MAX([LAST_UPDATED]) FROM [data].{attr}", CancellationToken);
            newMaxDataflowLvlLastUpdated = (DateTime)await DotStatDb.ExecuteScalarSqlAsync($"SELECT MAX([LAST_UPDATED]) FROM [data].{dfAttr} WHERE DF_ID = {_dataflow.DbId}", CancellationToken);

            Assert.AreEqual(newMaxObsLvlLastUpdated, maxObsLvlLastUpdated);
            Assert.AreEqual(newMaxSeriesLvlLastUpdated, maxSeriesLvlLastUpdated);
            Assert.AreEqual(newMaxDataflowLvlLastUpdated, maxDataflowLvlLastUpdated);

            //Update with new values
            attributeValues = new List<IKeyValue>
            {
                new KeyValueImpl("B", "OBS_ATTR"),
                new KeyValueImpl("AB", "TS_ATTR"),
                new KeyValueImpl("A", "GR_ATTR"),
                new KeyValueImpl("new value", "DF_ATTR"),
            };
            observationRows = new List<ObservationRow>{
                new ObservationRow(1, StagingRowActionEnum.Replace, new ObservationImpl(key, "2020", "4321", attributeValues, crossSectionValue: null), false){}
            }.ToAsyncEnumerable();
            await ImportObservations(observationRows);

            newMaxObsLvlLastUpdated = (DateTime)await DotStatDb.ExecuteScalarSqlAsync($"SELECT MAX([LAST_UPDATED]) FROM [data].{fact}", CancellationToken);
            newMaxSeriesLvlLastUpdated = (DateTime)await DotStatDb.ExecuteScalarSqlAsync($"SELECT MAX([LAST_UPDATED]) FROM [data].{attr}", CancellationToken);
            newMaxDataflowLvlLastUpdated = (DateTime)await DotStatDb.ExecuteScalarSqlAsync($"SELECT MAX([LAST_UPDATED]) FROM [data].{dfAttr} WHERE DF_ID = {_dataflow.DbId}", CancellationToken);

            Assert.Greater(newMaxObsLvlLastUpdated, maxObsLvlLastUpdated);
            Assert.Greater(newMaxSeriesLvlLastUpdated, maxSeriesLvlLastUpdated);
            Assert.Greater(newMaxDataflowLvlLastUpdated, maxDataflowLvlLastUpdated);

            maxObsLvlLastUpdated = newMaxObsLvlLastUpdated;
            maxSeriesLvlLastUpdated = newMaxSeriesLvlLastUpdated;
            maxDataflowLvlLastUpdated = newMaxDataflowLvlLastUpdated;

            //Partial delete
            attributeValues = new List<IKeyValue>
            {
                new KeyValueImpl("B", "OBS_ATTR"),
                new KeyValueImpl("new value", "TS_ATTR"),
            };
            observationRows = new List<ObservationRow>{
                new ObservationRow(1, StagingRowActionEnum.Delete, new ObservationImpl(key, "2020", "", attributeValues, crossSectionValue: null), false){}
            }.ToAsyncEnumerable();
            await ImportObservations(observationRows);

            newMaxObsLvlLastUpdated = (DateTime)await DotStatDb.ExecuteScalarSqlAsync($"SELECT MAX([LAST_UPDATED]) FROM [data].{fact}", CancellationToken);
            newMaxSeriesLvlLastUpdated = (DateTime)await DotStatDb.ExecuteScalarSqlAsync($"SELECT MAX([LAST_UPDATED]) FROM [data].{attr}", CancellationToken);
            newMaxDataflowLvlLastUpdated = (DateTime)await DotStatDb.ExecuteScalarSqlAsync($"SELECT MAX([LAST_UPDATED]) FROM [data].{dfAttr} WHERE DF_ID = {_dataflow.DbId}", CancellationToken);

            Assert.Greater(newMaxObsLvlLastUpdated, maxObsLvlLastUpdated);
            Assert.Greater(newMaxSeriesLvlLastUpdated, maxSeriesLvlLastUpdated);
            Assert.AreEqual(newMaxDataflowLvlLastUpdated, maxDataflowLvlLastUpdated);

            maxObsLvlLastUpdated = newMaxObsLvlLastUpdated;
            maxSeriesLvlLastUpdated = newMaxSeriesLvlLastUpdated;

            //Replace with null
            attributeValues = new List<IKeyValue>
            {
                new KeyValueImpl("", "OBS_ATTR"),
                new KeyValueImpl("", "TS_ATTR"),
                new KeyValueImpl("", "DF_ATTR"),
            };
            observationRows = new List<ObservationRow>{
                new ObservationRow(1, StagingRowActionEnum.Replace, new ObservationImpl(key, "2020", "-1", attributeValues, crossSectionValue: null), false){}
            }.ToAsyncEnumerable();
            await ImportObservations(observationRows);

            newMaxObsLvlLastUpdated = (DateTime)await DotStatDb.ExecuteScalarSqlAsync($"SELECT MAX([LAST_UPDATED]) FROM [data].{fact}", CancellationToken);
            newMaxSeriesLvlLastUpdated = (DateTime)await DotStatDb.ExecuteScalarSqlAsync($"SELECT MAX([LAST_UPDATED]) FROM [data].{attr}", CancellationToken);
            newMaxDataflowLvlLastUpdated = (DateTime)await DotStatDb.ExecuteScalarSqlAsync($"SELECT MAX([LAST_UPDATED]) FROM [data].{dfAttr} WHERE DF_ID = {_dataflow.DbId}", CancellationToken);

            Assert.Greater(newMaxObsLvlLastUpdated, maxObsLvlLastUpdated);
            Assert.AreEqual(newMaxSeriesLvlLastUpdated, maxSeriesLvlLastUpdated);
            Assert.AreEqual(newMaxDataflowLvlLastUpdated, maxDataflowLvlLastUpdated);

            var expectedValues = new List<string> { "-1", "", "", "A", "new value", };
            var i = 0;
            var view = _dataflow.SqlDataDataflowViewName((char)tableVersion);
            
            using var dr = await DotStatDb.ExecuteReaderSqlAsync($"SELECT OBS_VALUE,OBS_ATTR,TS_ATTR,GR_ATTR,DF_ATTR FROM [data].{view}", CancellationToken);
            dr.Read();
            foreach (var expectedValue in expectedValues)
            {
                var dbValue = dr[i++];
                var actualValue = (dbValue == DBNull.Value || dbValue == null) ? string.Empty : dbValue.ToString();
                Assert.AreEqual(expectedValue, actualValue);
            }
        }

        private async Task<BulkImportResult> ImportObservations(IAsyncEnumerable<ObservationRow> observations, bool initialiseData = false)
        {
            var tableVersion = DbTableVersion.A;
            var targetVersion = TargetVersion.Live;
            const bool fullValidation = false;
            const bool isTimeAtTimeDimensionSupported = false;
            const string dataSource = "testFile.csv";

            var reportedComponents = GetReportedComponents(_dataflow);

            var transactionId = await UnitOfWork.TransactionRepository.GetNextTransactionId(CancellationToken);
            var transaction = await UnitOfWork.TransactionRepository.CreateTransactionItem(transactionId, _dataflow.FullId, null, null, dataSource, TransactionType.Import, targetVersion, serviceId: null, CancellationToken);
            // create dataflow DB objects 
            var tryNewTransactionResult = await DotStatDbService.TryNewTransaction(transaction, _dataflow, MsAccess, CancellationToken);

            // ------ Logic bellow normally in SqlConsumer of Transfer service ----------

            // Wipe and/or create staging table and target tables
            await UnitOfWork.DataStoreRepository.RecreateStagingTables(_dataflow.Dsd, reportedComponents, isTimeAtTimeDimensionSupported, CancellationToken);

            // import to staging
            var codeTranslator = new CodeTranslator(UnitOfWork.CodeListRepository);
            await codeTranslator.FillDict(_dataflow, CancellationToken);

            var bulkImportResult = await UnitOfWork.DataStoreRepository.BulkInsertData(observations, reportedComponents, codeTranslator, _dataflow,
                fullValidation, isTimeAtTimeDimensionSupported, false, CancellationToken);

            // merge from staging

            var dfAttributes = initialiseData ? new List<DataSetAttributeRow>
            {  new (1, StagingRowActionEnum.Merge)
               {
                    Attributes = new List<IKeyValue>{ new KeyValueImpl("DF", "DF_ATTR") }
               } 
            } : bulkImportResult.DataSetLevelAttributeRows;

            var importSummary= await UnitOfWork.DataStoreRepository.MergeStagingTable(_dataflow, reportedComponents,
                bulkImportResult.BatchActions, dfAttributes, codeTranslator, tableVersion, true, CancellationToken);

            if (importSummary.Errors.Count > 0)
            {
                bulkImportResult.Errors.AddRange(importSummary.Errors);
            }

            // close transaction
            _dataflow.Dsd.LiveVersion = (char?)tableVersion;
            await DotStatDbService.CloseTransaction(transaction, _dataflow, false, false, true,
                MsAccess, false, false, null, CancellationToken);
            
            return bulkImportResult;
        }

    }
}
