﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DotStat.Db;
using DotStat.Db.Util;
using DotStat.Domain;
using NUnit.Framework;
using Org.Sdmxsource.Sdmx.Api.Model.Data;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Data;

namespace DotStat.Test.DataAccess.Integration.DataDb.Import
{
    [NonParallelizable]
    [TestFixture("sdmx/OECD-DF_TEST_DELETE-1.0-all_structures.xml")]
    public sealed class DataImportTestsDelete : BaseDbIntegrationTests
    {
        private readonly Dataflow _dataflow;

        public DataImportTestsDelete(string structure) : base(structure)
        {
            _dataflow = this.GetDataflow();
        }

        [OneTimeTearDown]
        public async Task TearDown()
        {
            await CleanUpStructure(_dataflow.Dsd);
        }

        [Test]
        [TestCase(new[] { "", "", "", "", "", "", "", "" }, 0, null, 0, null, 0, null, 0, TestName = "case_1__delete_whole_content_of_the_dataflow")]
        [TestCase(new[] { null, null, null, "*", "*", null, null, null }, 9, "OBS_VALUE", 0, null, 9, null, 1, TestName = "case_2__delete_all_observations_of_the_dataflow_and_the_observation-level_attributes")]
        [TestCase(new[] { null, null, null, "*", null, null, null, null }, 27, "OBS_VALUE", 0, null, 9, null, 1, TestName = "case_3__delete_all_observation_values_of_the_dataflow")]
        [TestCase(new[] { null, null, null, null, "*", "*", "*", "*"}, 27, "OBS_ATTR", 0, null, 0, null, 0, TestName = "case_4__delete_all_attributes_of_the_dataflow")]
        [TestCase(new[] { null, null, null, null, null, null, null, "*"}, 27, "DF_ATTR", 0, null, 9, null, 0, TestName = "case_5__delete_attributes_attached_at_dataflow_level")]
        [TestCase(new[] { null, "B", null, null, null, null, null, null }, 18, "OBS_VALUE", 18, null, 6, null, 1, TestName = "case_6-1__delete_everything_related_to_DIM_2_B_way1")]
        [TestCase(new[] { null, "B", null, "*", "*", "*", "*", null }, 18, "OBS_VALUE", 18, null, 6, null, 1, TestName = "case_6-2__delete_everything_related_to_DIM_2_B_way2")]
        [TestCase(new[] { null, "B", null, null, null, null, "*", null }, 27, "GR_ATTR", 18, "GR_ATTR", 6, null, 1, TestName = "case_7__delete_everything_attached_to_DIM_2_B")]
        [TestCase(new[] { null, "B", null, "*", "*", null, null, null }, 21, "OBS_VALUE", 18, null, 9, null, 1, TestName = "case_8__delete_all_observations__and_its_observation-level_attributes__related_to_DIM_2_B")]
        [TestCase(new[] { null, null, null, null, null, "*", null, null }, 27, "TS_ATTR", 0, "TS_ATTR", 0, null, 1, TestName = "case_9__delete_all_time_series_attributes")]
        [TestCase(new[] { "A", "B", null, null, null, null, null, null }, 24, "OBS_VALUE", 24, "TS_ATTR", 8, null, 1, TestName = "case_10__delete_whole_time_series_DIM_1_A_DIM_2_B")]
        [TestCase(new[] { "A", "B", null, null, null, "*", null, null }, 27, "TS_ATTR", 24, "TS_ATTR", 8, null, 1, TestName = "case_11__delete_the_attributes_attached_to_time_series_DIM_1_A_DIM_2_B")]
        [TestCase(new[] { "A", "B", null, "*", "*", null, null, null }, 25, "OBS_VALUE", 24, null, 9, null, 1, TestName = "case_12__delete_observation_values___observation-level_attributes_for_time_series_DIM_1_A_DIM_2_B")]
        [TestCase(new[] { "A", "B", "2020", null, null, null, null, null }, 26, "OBS_VALUE", 26, null, 9, null, 1, TestName = "case_13__delete_observation__and_its_observation-level_attributes__attached_to_key_DIM_1_A_DIM_2_B_TIME_PERIOD_2021")]
        [TestCase(new[] { "A", "B", "2020", "*", null, null, null, null }, 27, "OBS_VALUE", 26, null, 9, null, 1, TestName = "case_14__delete_observation_value_attached_to_key_DIM_1_A_DIM_2_B_TIME_PERIOD_2021")]
        [TestCase(new[] { "A", null, null, "*", "*", "*", "*", "*" }, 18, "OBS_VALUE", 18, null, 6, null, 0, TestName = "case_15__delete_everything_stored_with_DIM_1_A_way1")]
        [TestCase(new[] { "A", null, null, null, null, null, null, null }, 18, "OBS_VALUE", 18, null, 6, null, 1, TestName = "case_15__delete_everything_stored_with_DIM_1_A_way2")]
        [TestCase(new[] { null, "B", null, null, null, null, "*", "*" }, 27, "OBS_VALUE", 27, "GR_ATTR", 6, null, 0, TestName = "case_16__delete_everything_related_to_DIM_2_B")]
        [TestCase(new[] { "A", "B", null, null, "*", "*", "*", "*" }, 27, "OBS_VALUE", 27, null, 8, null, 0, TestName = "case_17__delete_everything_not_attached_to_DIM_1_A_DIM_2_B")]
        [TestCase(new[] { "A", "B", "2020", "*", "*", "*", "*", "*" }, 26, "OBS_VALUE", 26, null, 8, null, 0, TestName = "case_18__delete_everything_not_attached_to_key_DIM_1_A_DIM_2_B_TIME_PERIOD_2021")]
        public async Task DeleteTests(string[] components, int remainingViewRows, string obsColumn, int remainingObsCount, string attrColumnCode, int remainingAttrCount, string dfAttrColumn, int remainingDfAttrCount)
        {
            //Initialize all data
            await InitializeAllData();

            //Test case setup
            var observationRows = new List<ObservationRow>().ToAsyncEnumerable();

            var nonDeletedComponents = new List<string>();

            var i = 0;

            //Dimensions
            var hasWildCardedDimensions = false;
            var seriesKey = new List<IKeyValue>();
            foreach (var dimId in _dataflow.Dimensions.Where(d => !d.Base.TimeDimension).Select(d => d.Base.Id))
            {
                var keyVal = new KeyValueImpl(components[i++], dimId);
                seriesKey.Add(keyVal);
                if (string.IsNullOrEmpty(keyVal.Code))
                    hasWildCardedDimensions = true;
            }
            var key = new KeyableImpl(_dataflow.Base, _dataflow.Dsd.Base, seriesKey, null);
            var obsTime = components[i++];
            if (string.IsNullOrEmpty(obsTime))
                hasWildCardedDimensions = true;

            //Measure
            var obsVal = components[i++];
            if(string.IsNullOrEmpty(obsVal))
                nonDeletedComponents.Add(_dataflow.Dsd.Base.PrimaryMeasure.Id);

            //Attributes
            var attributeValues = new List<IKeyValue>();
            foreach (var dimId in _dataflow.Dsd.Attributes.Select(d => d.Base.Id))
            {
                attributeValues.Add(new KeyValueImpl(components[i++], dimId));
            }
            nonDeletedComponents.AddRange(attributeValues.Where(a => string.IsNullOrEmpty(a.Code)).Select(a=> a.Concept));

            var observation = new ObservationImpl(key, obsTime, obsVal, attributeValues, crossSectionValue: null);
            observationRows = new List<ObservationRow>
            {
                new (1, StagingRowActionEnum.Delete, observation, hasWildCardedDimensions)
            }.ToAsyncEnumerable();

            //Delete
            await ImportObservations(observationRows);
            
            const DbTableVersion tableVersion = DbTableVersion.A;

            //Check view deletions
            var view = _dataflow.SqlDataDataflowViewName((char)tableVersion);

            var allNonDeleted = nonDeletedComponents.Count == _dataflow.Dsd.Attributes.Count + 1;
            var filterNulledColumns = nonDeletedComponents.Any() && !allNonDeleted ? "WHERE " + string.Join(" AND ", nonDeletedComponents.Select(c => $"[{c}] IS NOT NULL")) : "";
            var dbObsCount = (int)await DotStatDb.ExecuteScalarSqlAsync($"SELECT COUNT (*) FROM [data].{view} {filterNulledColumns}", CancellationToken);
            Assert.AreEqual(remainingViewRows, dbObsCount);

            //Check obs level deletions
            var table = _dataflow.SqlDataDataflowViewName((char)tableVersion);
            var filterNulledColumn = string.IsNullOrEmpty(obsColumn) ? "": $"WHERE [{obsColumn}] IS NOT NULL;";
            dbObsCount = (int)await DotStatDb.ExecuteScalarSqlAsync($"SELECT COUNT (*) FROM [data].{table} {filterNulledColumn}", CancellationToken);
            Assert.AreEqual(remainingObsCount, dbObsCount);

            //Check attr level deletions
            table = _dataflow.Dsd.SqlDimGroupAttrTable((char)tableVersion);
            filterNulledColumn = string.IsNullOrEmpty(attrColumnCode)
                ? ""
                : $"WHERE [COMP_{_dataflow.Dsd.Attributes.First(x => x.Code == attrColumnCode).DbId}] IS NOT NULL;";

            dbObsCount = (int)await DotStatDb.ExecuteScalarSqlAsync($"SELECT COUNT (*) FROM [data].{table} {filterNulledColumn}", CancellationToken);
            Assert.AreEqual(remainingAttrCount, dbObsCount);

            //Check df attr level deletions
            table = _dataflow.Dsd.SqlDsdAttrTable((char)tableVersion);
            filterNulledColumn = string.IsNullOrEmpty(dfAttrColumn) ? "" : $"WHERE [{dfAttrColumn}] IS NOT NULL;";
            dbObsCount = (int)await DotStatDb.ExecuteScalarSqlAsync($"SELECT COUNT (*) FROM [data].{table} {filterNulledColumn}", CancellationToken);
            Assert.AreEqual(remainingDfAttrCount, dbObsCount);


        }

        [Test]
        [TestCase(new[] { "", "", ""}, 0, "", 0, null, 0, null, 0)]
        [TestCase(new[] { "A", null, null }, 18, "OBS_VALUE", 18, "TS_ATTR", 6, null, 1)]
        [TestCase(new[] { null, "B", null }, 18, "OBS_VALUE", 18, "GR_ATTR", 6, null, 1)]
        [TestCase(new[] { "A", "B", null }, 24, "OBS_VALUE", 24, "TS_ATTR", 8, null, 1)]
        [TestCase(new[] { "A", "B", "2020" }, 26, "OBS_VALUE", 26, "TS_ATTR", 9, null, 1)]
        public async Task DeleteTestsOmitedColumns(string[] dimensions, int remainingViewRows, string obsColumn, int remainingObsCount, string attrColumnCode, int remainingAttrCount, string dfAttrColumn, int remainingDfAttrCount)
        {
            //Initialize all data
            await InitializeAllData();

            //Test case setup
            var observationRows = new List<ObservationRow>().ToAsyncEnumerable();

            var i = 0;

            //Dimensions
            var hasWildCardedDimensions = false;
            var seriesKey = new List<IKeyValue>();
            foreach (var dimId in _dataflow.Dimensions.Where(d => !d.Base.TimeDimension).Select(d => d.Base.Id))
            {
                var keyVal = new KeyValueImpl(dimensions[i++], dimId);
                seriesKey.Add(keyVal);
                if (string.IsNullOrEmpty(keyVal.Code))
                    hasWildCardedDimensions = true;
            }
            var key = new KeyableImpl(_dataflow.Base, _dataflow.Dsd.Base, seriesKey, null);
            var obsTime = dimensions[i++];
            if (string.IsNullOrEmpty(obsTime))
                hasWildCardedDimensions = true;

            //Attributes
            var observation = new ObservationImpl(key, obsTime, "", new List<IKeyValue>(), crossSectionValue: null);
            observationRows = new List<ObservationRow>
            {
                new (1, StagingRowActionEnum.Delete, observation, hasWildCardedDimensions)
            }.ToAsyncEnumerable();

            //Delete
            await ImportObservations(observationRows);

            const DbTableVersion tableVersion = DbTableVersion.A;

            //Check view deletions
            var view = _dataflow.SqlDataDataflowViewName((char)tableVersion);
            var dbObsCount = (int)await DotStatDb.ExecuteScalarSqlAsync($"SELECT COUNT (*) FROM [data].{view}", CancellationToken);
            Assert.AreEqual(remainingViewRows, dbObsCount);

            //Check obs level deletions
            var table = _dataflow.SqlDataDataflowViewName((char)tableVersion);
            var filterNulledColumn = string.IsNullOrEmpty(obsColumn) ? "" : $"WHERE [{obsColumn}] IS NOT NULL;";
            dbObsCount = (int)await DotStatDb.ExecuteScalarSqlAsync($"SELECT COUNT (*) FROM [data].{table} {filterNulledColumn}", CancellationToken);
            Assert.AreEqual(remainingObsCount, dbObsCount);

            //Check attr level deletions
            table = _dataflow.Dsd.SqlDimGroupAttrTable((char)tableVersion);
            
            filterNulledColumn = string.IsNullOrEmpty(attrColumnCode)
                ? "" 
                : $"WHERE [COMP_{_dataflow.Dsd.Attributes.First(x => x.Code == attrColumnCode).DbId}] IS NOT NULL;";

            dbObsCount = (int)await DotStatDb.ExecuteScalarSqlAsync($"SELECT COUNT (*) FROM [data].{table} {filterNulledColumn}", CancellationToken);
            Assert.AreEqual(remainingAttrCount, dbObsCount);

            //Check df attr level deletions
            table = _dataflow.Dsd.SqlDsdAttrTable((char)tableVersion);
            filterNulledColumn = string.IsNullOrEmpty(dfAttrColumn) ? "" : $"WHERE [{dfAttrColumn}] IS NOT NULL;";
            dbObsCount = (int)await DotStatDb.ExecuteScalarSqlAsync($"SELECT COUNT (*) FROM [data].{table} {filterNulledColumn}", CancellationToken);
            Assert.AreEqual(remainingDfAttrCount, dbObsCount);
        }

        private async Task InitializeAllData()
        {
            // generate observations
            const int obsCount = 27;
            var observations = ObservationGenerator.Generate(
                _dataflow.Dsd,
                _dataflow,
                true,
                2020,
                2022,
                obsCount,
                action: StagingRowActionEnum.Merge
            );

            //Populate all data
            await ImportObservations(observations, true);
        }

        private async Task ImportObservations(IAsyncEnumerable<ObservationRow> observations, bool initialiseData = false)
        {
            var tableVersion = DbTableVersion.A;
            var targetVersion = TargetVersion.Live;
            const bool fullValidation = false;
            const bool isTimeAtTimeDimensionSupported = false;
            const string dataSource = "testFile.csv";

            var reportedComponents = GetReportedComponents(_dataflow);
            
            var transactionId = await UnitOfWork.TransactionRepository.GetNextTransactionId(CancellationToken);
            var transaction = await UnitOfWork.TransactionRepository.CreateTransactionItem(transactionId, _dataflow.FullId, null, null, dataSource, TransactionType.Import, targetVersion, serviceId: null, CancellationToken);
            // create dataflow DB objects 
            var tryNewTransactionResult = await DotStatDbService.TryNewTransaction(transaction, _dataflow, MsAccess, CancellationToken);
                
            // ------ Logic bellow normally in SqlConsumer of Transfer service ----------

            // Wipe and/or create staging table and target tables
            await UnitOfWork.DataStoreRepository.RecreateStagingTables(_dataflow.Dsd, reportedComponents, isTimeAtTimeDimensionSupported, CancellationToken);

            // import to staging
            var codeTranslator = new CodeTranslator(UnitOfWork.CodeListRepository);
            await codeTranslator.FillDict(_dataflow, CancellationToken);

            var bulkImportResult = await UnitOfWork.DataStoreRepository.BulkInsertData(observations, reportedComponents, codeTranslator, _dataflow,
                fullValidation, isTimeAtTimeDimensionSupported, false,CancellationToken);
                
            // merge from staging

            var dfAttributes =initialiseData? new List<DataSetAttributeRow>
            {   new (1, StagingRowActionEnum.Merge)
                {
                    Attributes = new List<IKeyValue>{ new KeyValueImpl("DF", "DF_ATTR") }
                }
            } : bulkImportResult.DataSetLevelAttributeRows;
            
            await UnitOfWork.DataStoreRepository.MergeStagingTable(_dataflow, reportedComponents,
                bulkImportResult.BatchActions, dfAttributes, codeTranslator, tableVersion, true, CancellationToken);
                
            // close transaction
            _dataflow.Dsd.LiveVersion = (char?)tableVersion;
             await DotStatDbService.CloseTransaction(transaction, _dataflow, false, false, true, MsAccess, false, false, null, CancellationToken);
        }

    }
}
