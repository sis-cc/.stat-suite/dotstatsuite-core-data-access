﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DotStat.Db;
using DotStat.Db.Helpers;
using DotStat.Db.Util;
using DotStat.DB.Util;
using DotStat.Db.Validation;
using DotStat.Domain;
using NUnit.Framework;
using Org.Sdmxsource.Sdmx.Api.Model.Data;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Data;

namespace DotStat.Test.DataAccess.Integration.DataDb.Import
{
    [NonParallelizable]
    [TestFixture("sdmx/OECD-DF_TEST_DELETE-1.0-all_structures.xml")]
    public sealed class DataImportTestsReplace : BaseDbIntegrationTests
    {
        private readonly Dataflow _dataflow;

        public DataImportTestsReplace(string structure) : base(structure)
        {
            _dataflow = this.GetDataflow();
        }

        [OneTimeTearDown]
        public async Task TearDown()
        {
            await CleanUpStructure(_dataflow.Dsd);
        }

        [Test]
        [TestCase(new[] { null, "", "" }, new[] { null, "", "", "", "", "" }, new[] { "*", "*", "*", "*", "*" }, 27, 27, TestName = "case 1 - Try to replace by null the dataflow level attribute")]
        [TestCase(new[] { null, "", "" }, new[] { "-1", "C", "CC", "C", "New value dataset level attribute" }, new[] { "*", "*", "*", "*", "New value dataset level attribute" }, 27, 27, ValidationErrorType.MissingCodeMember, TestName = "case 2 - Try to replace values in all levels when the key attachment is at dataflow")]
        [TestCase(new[] { null, "", "" }, new[] { null, "", "", null, "New value dataset level attribute" }, new[] { "*", "*", "*", "*", "New value dataset level attribute" }, 27, 27, TestName = "case 3 - Replace dataflow attribute")]
        
        [TestCase(new[] { null, "B", null }, new[] { null, null, null, null, "" }, new[] { "*", "*", "*", "*", "*" }, 9, 27, TestName = "case 4 - Try to replace by null the attribute attached to DIM_2 B")]
        [TestCase(new[] { null, "B", null }, new[] { null, null, "CC", "C", "New value dataset level attribute" }, new[] { "*", "*", "*", "C", "New value dataset level attribute" }, 9, 27, ValidationErrorType.MissingCodeMember, TestName = "case 5 - Try to replace values in all levels when they key attachment is at DIM_2 B")]
        [TestCase(new[] { null, "B", null }, new[] { null, null, null, "C", null }, new[] { "*", "*", "*", "C", "DF" }, 9, 27, TestName = "case 6 - Replace group attribute attached to DIM_2 B")]

        [TestCase(new[] { "A", "B", null }, new[] { null, null, null, null, "" }, new[] { "*", "*", "*", "*", "*" }, 3, 27, TestName = "case 7 - Try to replace by null the attribute attached to DIM_1 A and DIM_2 B")]
        [TestCase(new[] { "A", "B", null }, new[] { "-1", "C", "CC", "C", "New value dataset level attribute" }, new[] { "*", "*", "CC", "C", "New value dataset level attribute" }, 3, 27, ValidationErrorType.ObservationReferencedDimensionMissing, TestName = "case 8 - Try to replace values in all levels when they key attachment is at DIM_1 A and DIM_2 B")]
        [TestCase(new[] { "A", "B", null }, new[] { null, null, "CC", "C", null }, new[] { "*", "*", "CC", "C", "DF" }, 3, 27, TestName = "case 9 - Try to replace attributes attatched to DIM_1 A and DIM_2 B (One replace one skip)")]
        [TestCase(new[] { "A", "B", null }, new[] { null, null, null, "C", null }, new[] { "*", "*", "TS_ATTR - DimensionGroup - text", "C", "DF" }, 3, 27, TestName = "case 10 - Try to replace attribute attached to DIM_2, using DIM_1 A and DIM_2 B")]
        [TestCase(new[] { "A", "B", null }, new[] { null, null, "CC", null, null }, new[] { "*", "*", "CC", "A", "DF" }, 3, 27, TestName = "case 11 - Replace attribute attached to DIM_1 A and DIM_2 B")]
        
        [TestCase(new[] { "A", "B", "2020" }, new[] { null, null, null, null, "" }, new[] { null, null, "TS_ATTR - DimensionGroup - text", "A", "DF" }, 0, 26, TestName = "case 12 - Set  to null measure and replace observation level attributes attached to key DIM_1 A, DIM_2 B and TIME_PERIOD 2021")]
        [TestCase(new[] { "A", "B", "2020" }, new[] { "-1", "C", "CC", "C", "New value dataset level attribute" }, new[] { "-1", "C", "CC", "C", "New value dataset level attribute" }, 1, 27, TestName = "case 13 - Replace values in all levels when the key attachment is at DIM_1 A, DIM_2 B and TIME_PERIOD 2021 ")]
        [TestCase(new[] { "A", "B", "2020" }, new[] { "-1", "C", null, null, null }, new[] { "-1", "C", "TS_ATTR - DimensionGroup - text", "A", "DF" }, 1, 27, TestName = "case 14 - Replace measure and observation level attributes attached to key DIM_1 A, DIM_2 B and TIME_PERIOD 2021")]
        [TestCase(new[] { "A", "B", "2020" }, new[] { "-1", null, null, null, null }, new[] { "-1", null, "TS_ATTR - DimensionGroup - text", "A", "DF" }, 1, 27, TestName = "case 15 - Replace measure and set to null observation level attributes attached to key DIM_1 A, DIM_2 B and TIME_PERIOD 2021")]
        [TestCase(new[] { "A", "B", "2020" }, new[] { null, "C", null, null, null }, new[] { null, "C", "TS_ATTR - DimensionGroup - text", "A", "DF" }, 1, 27, TestName = "case 16 - Set measure to null and replace observation level attributes attached to key DIM_1 A, DIM_2 B and TIME_PERIOD 2021")]
        public async Task ReplaceTests(string[] replaceKey, string[] replaceValues, string[] expectedValues, int expectedFilteredRowsCount, int expectedTotalRowsCount, ValidationErrorType? expectedErrorType = null)
        {
            //Initialize all data
            await InitializeAllData();

            //Test case setup
            var observationRows = new List<ObservationRow>().ToAsyncEnumerable();

            var i = 0;
            //Dimensions
            var hasWildCardedDimensions = false;
            var seriesKey = new List<IKeyValue>();
            foreach (var dimId in _dataflow.Dimensions.Where(d => !d.Base.TimeDimension).Select(d => d.Base.Id))
            {
                var keyVal = new KeyValueImpl(replaceKey[i++], dimId);
                seriesKey.Add(keyVal); 
                if (string.IsNullOrEmpty(keyVal.Code))
                    hasWildCardedDimensions = true;
            }
            var key = new KeyableImpl(_dataflow.Base, _dataflow.Dsd.Base, seriesKey, null);
            var obsTime = replaceKey[i++];
            if (string.IsNullOrEmpty(obsTime))
                hasWildCardedDimensions = true;
            else
                seriesKey.Add(new KeyValueImpl(obsTime, "TIME_PERIOD"));
            i = 0;

            //Measure
            var obsVal = replaceValues[i++];
            //Attributes
            var attributeValues = new List<IKeyValue>();
            foreach (var attrId in _dataflow.Dsd.Attributes.Select(d => d.Base.Id))
            {
                attributeValues.Add(new KeyValueImpl(replaceValues[i++], attrId));
            }

            var observation = new ObservationImpl(key, obsTime, obsVal, attributeValues, crossSectionValue: null);
            observationRows = new List<ObservationRow>
            {
                new (1, StagingRowActionEnum.Replace, observation, hasWildCardedDimensions)
            }.ToAsyncEnumerable();


            var bulkImportResult = await ImportObservations(observationRows);

            if (expectedErrorType is not null)
            {
                Assert.Greater(bulkImportResult.Errors.Count, 0 );
                Assert.AreEqual(expectedErrorType, bulkImportResult.Errors[0].Type);
                return;
            }

            const DbTableVersion tableVersion = DbTableVersion.A;

            //Check view Replacements
            var view = _dataflow.SqlDataDataflowViewName((char)tableVersion);
            var dbObsCount = (int)await DotStatDb.ExecuteScalarSqlAsync($"SELECT COUNT (*) FROM [data].{view}", CancellationToken);
            Assert.AreEqual(expectedTotalRowsCount, dbObsCount);

            //Check Replacements
            var filter = string.Join(" AND ", seriesKey.Where(d => !string.IsNullOrEmpty(d.Code)).Select(d => $"{d.Concept} = '{d.Code}'"));
            if (!string.IsNullOrEmpty(filter))
                filter = "WHERE " + filter;

            var filteredRowCount = 0;
            var components = new List<string> { "OBS_VALUE" };
            components.AddRange(_dataflow.Dsd.Attributes.Select(d => d.Base.Id));
            
            var select = string.Join(",", components);
            using var dr = await DotStatDb.ExecuteReaderSqlAsync($"SELECT {select} FROM [data].{view} {filter}", CancellationToken);
            while (dr.Read())
            {
                filteredRowCount++;
                i = 0;
                foreach (var component in components)
                {
                    var expectedValue = expectedValues[i++];
                    var dbValue = component == "OBS_VALUE" ? dr.GetNullableValue<double>(component)?.ToString() : dr.GetNullableString(component);
                    if(expectedValue == "*")
                        Assert.IsNotNull(dbValue);
                    else
                        Assert.AreEqual(expectedValue, dbValue);                    
                }
            }
            Assert.AreEqual(expectedFilteredRowsCount, filteredRowCount);

        }


        private async Task InitializeAllData()
        {
            // generate observations
            const int obsCount = 27;
            var observations = ObservationGenerator.Generate(
                _dataflow.Dsd,
                _dataflow,
                true,
                2020,
                2022,
                obsCount,
                action: StagingRowActionEnum.Merge
            );

            //Populate all data
            await ImportObservations(observations, true);
        }

        private async Task<BulkImportResult> ImportObservations(IAsyncEnumerable<ObservationRow> observations, bool initialiseData = false)
        {
            var tableVersion = DbTableVersion.A;
            var targetVersion = TargetVersion.Live;
            const bool fullValidation = false;
            const bool isTimeAtTimeDimensionSupported = false;
            const string dataSource = "testFile.csv";

            var reportedComponents = GetReportedComponents(_dataflow);

            var transactionId = await UnitOfWork.TransactionRepository.GetNextTransactionId(CancellationToken);
            var transaction = await UnitOfWork.TransactionRepository.CreateTransactionItem(transactionId, _dataflow.FullId, null, null, dataSource, TransactionType.Import, targetVersion, serviceId: null, CancellationToken);
            // create dataflow DB objects 
            var tryNewTransactionResult = await DotStatDbService.TryNewTransaction(transaction, _dataflow, MsAccess, CancellationToken);

            // ------ Logic bellow normally in SqlConsumer of Transfer service ----------

            // Wipe and/or create staging table and target tables
            await UnitOfWork.DataStoreRepository.RecreateStagingTables(_dataflow.Dsd, reportedComponents, isTimeAtTimeDimensionSupported, CancellationToken);

            // import to staging
            var codeTranslator = new CodeTranslator(UnitOfWork.CodeListRepository);
            await codeTranslator.FillDict(_dataflow, CancellationToken);

            var bulkImportResult = await UnitOfWork.DataStoreRepository.BulkInsertData(observations, reportedComponents, codeTranslator, _dataflow,
                fullValidation, isTimeAtTimeDimensionSupported, false, CancellationToken);

            // merge from staging

            var dfAttributes = initialiseData ? new List<DataSetAttributeRow>
            {  new (1, StagingRowActionEnum.Merge)
               {
                    Attributes = new List<IKeyValue>{ new KeyValueImpl("DF", "DF_ATTR") }
               } 
            } : bulkImportResult.DataSetLevelAttributeRows;

            var importSummary= await UnitOfWork.DataStoreRepository.MergeStagingTable(_dataflow, reportedComponents,
                bulkImportResult.BatchActions, dfAttributes, codeTranslator, tableVersion, true, CancellationToken);

            if (importSummary.Errors.Count > 0)
            {
                bulkImportResult.Errors.AddRange(importSummary.Errors);
            }

            // close transaction
            _dataflow.Dsd.LiveVersion = (char?)tableVersion;
            await DotStatDbService.CloseTransaction(transaction, _dataflow, false, false, true,
                MsAccess, false, false, null, CancellationToken);
            
            return bulkImportResult;
        }

    }
}
