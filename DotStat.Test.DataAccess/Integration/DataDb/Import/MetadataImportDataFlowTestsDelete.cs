﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DotStat.Db;
using DotStat.Db.Util;
using DotStat.Domain;
using NUnit.Framework;
using Org.Sdmxsource.Sdmx.Api.Model.Data;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Data;

namespace DotStat.Test.DataAccess.Integration.DataDb.Import
{
    [TestFixture("sdmx/CsvV2.xml"), NonParallelizable]
    public sealed class MetadataImportDataFlowTestsDelete : BaseDbIntegrationTests
    {
        private readonly Dataflow _dataflow;

        public MetadataImportDataFlowTestsDelete(string structure) : base(structure)
        {
            _dataflow = this.GetDataflow();
        }

        [OneTimeTearDown]
        public async Task TearDown()
        {
            await CleanUpStructure(_dataflow.Dsd);
        }

        [Test]
        public async Task MetadataDeleteAll()
        {
            await InitializeAllData();

            var components = new List<string>();
            components.AddRange(Enumerable.Repeat(string.Empty, 28));

            var observationRows = GetObservationRows(components);

            await ImportObservations(observationRows);

            var view = _dataflow.SqlMetadataDataflowViewName((char)DbTableVersion.A);
            Assert.AreEqual(0, (int)await DotStatDb.ExecuteScalarSqlAsync($"SELECT COUNT(*) FROM [data].{view}", CancellationToken));

            //check deleted view
            view = _dataflow.SqlDeletedMetadataDataflowViewName((char)DbTableVersion.A);
            Assert.AreEqual(1, (int)await DotStatDb.ExecuteScalarSqlAsync($"SELECT COUNT(*) FROM [data].{view}", CancellationToken));

        }

        [Test]
        public async Task MetadataDeleteAllDatasetLevelComponents()
        {
            await InitializeAllData();

            var components = new List<string>();
            components.AddRange(Enumerable.Repeat(DbExtensions.DimensionSwitchedOff, 6));
            components.AddRange(Enumerable.Repeat(string.Empty, 21));

            var observationRows = GetObservationRows(components);

            await ImportObservations(observationRows);

            var view = _dataflow.SqlMetadataDataflowViewName((char)DbTableVersion.A);
            Assert.AreEqual(8, (int)await DotStatDb.ExecuteScalarSqlAsync($"SELECT COUNT(*) FROM [data].{view}", CancellationToken));

            //check deleted view
            view = _dataflow.SqlDeletedMetadataDataflowViewName((char)DbTableVersion.A);
            Assert.AreEqual(1, (int)await DotStatDb.ExecuteScalarSqlAsync($"SELECT COUNT(*) FROM [data].{view}", CancellationToken));
        }

        [Test]
        public async Task MetadataDeleteOneDatasetLevelComponent()
        {
            await InitializeAllData();

            var components = new List<string>();
            components.AddRange(Enumerable.Repeat(DbExtensions.DimensionSwitchedOff, 6));
            components.Add("x");
            components.AddRange(Enumerable.Repeat(string.Empty, 20));

            var observationRows = GetObservationRows(components);

            await ImportObservations(observationRows);

            var view = _dataflow.SqlMetadataDataflowViewName((char)DbTableVersion.A);
            var datasetAttribute = _dataflow.Dsd.SqlMetadataDataSetTable((char)DbTableVersion.A);
            var attributeColumn0 = _dataflow.Dsd.Msd.MetadataAttributes[0].SqlColumn();
            var attributeColumn1 = _dataflow.Dsd.Msd.MetadataAttributes[1].SqlColumn();

            Assert.AreEqual(9, (int)await DotStatDb.ExecuteScalarSqlAsync($"SELECT COUNT(*) FROM [data].{view}", CancellationToken));
            Assert.AreEqual(1, (int)await DotStatDb.ExecuteScalarSqlAsync($"SELECT COUNT(*) FROM [data].{datasetAttribute} WHERE {attributeColumn0} IS NULL AND {attributeColumn1} IS NOT NULL AND [DF_ID] = {_dataflow.DbId}", CancellationToken));

            //check deleted view
            view = _dataflow.SqlDeletedMetadataDataflowViewName((char)DbTableVersion.A);
            Assert.AreEqual(1, (int)await DotStatDb.ExecuteScalarSqlAsync($"SELECT COUNT(*) FROM [data].{view}", CancellationToken));
        }

        [Test]
        public async Task MetadataDeleteOneComponentOnAllLevels()
        {
            await InitializeAllData();

            var components = new List<string>();
            components.AddRange(Enumerable.Repeat(string.Empty, 6));
            components.Add("x");
            components.AddRange(Enumerable.Repeat(string.Empty, 21));

            var observationRows = GetObservationRows(components);

            await ImportObservations(observationRows);

            var view = _dataflow.SqlMetadataDataflowViewName((char)DbTableVersion.A);
            var attributeColumn = _dataflow.Dsd.Msd.MetadataAttributes[0].Code;

            Assert.AreEqual(9, (int)await DotStatDb.ExecuteScalarSqlAsync($"SELECT COUNT(*) FROM [data].{view}", CancellationToken));
            Assert.AreEqual(9, (int)await DotStatDb.ExecuteScalarSqlAsync($"SELECT COUNT(*) FROM [data].{view} WHERE {attributeColumn} IS NULL", CancellationToken));

            //check deleted view
            view = _dataflow.SqlDeletedMetadataDataflowViewName((char)DbTableVersion.A);
            Assert.AreEqual(2, (int)await DotStatDb.ExecuteScalarSqlAsync($"SELECT COUNT(*) FROM [data].{view}", CancellationToken));
        }

        [Test]
        public async Task MetadataDeleteAllAttachedToDimensionValue()
        {
            await InitializeAllData();

            var components = new List<string>();
            components.AddRange(Enumerable.Repeat(string.Empty, 3));
            components.Add(DbExtensions.DimensionSwitchedOff);
            components.Add(DbExtensions.DimensionSwitchedOff);
            components.Add(string.Empty);
            components.AddRange(Enumerable.Repeat(string.Empty, 21));

            var observationRows = GetObservationRows(components);

            await ImportObservations(observationRows);

            var view = _dataflow.SqlMetadataDataflowViewName((char)DbTableVersion.A);
            Assert.AreEqual(6, (int)await DotStatDb.ExecuteScalarSqlAsync($"SELECT COUNT(*) FROM [data].{view}", CancellationToken));

            //check deleted view
            view = _dataflow.SqlDeletedMetadataDataflowViewName((char)DbTableVersion.A);
            Assert.AreEqual(1, (int)await DotStatDb.ExecuteScalarSqlAsync($"SELECT COUNT(*) FROM [data].{view}", CancellationToken));
        }

        [Test]
        public async Task MetadataDeleteRelatedToTimeValue()
        {
            await InitializeAllData();

            var components = new List<string>();
            components.AddRange(Enumerable.Repeat(string.Empty, 4));
            components.Add("F");
            components.Add("2023");
            components.AddRange(Enumerable.Repeat(string.Empty, 21));

            var observationRows = GetObservationRows(components);

            await ImportObservations(observationRows);

            var view = _dataflow.SqlMetadataDataflowViewName((char)DbTableVersion.A);
            Assert.AreEqual(5, (int)await DotStatDb.ExecuteScalarSqlAsync($"SELECT COUNT(*) FROM [data].{view}", CancellationToken));

            //check deleted view
            view = _dataflow.SqlDeletedMetadataDataflowViewName((char)DbTableVersion.A);
            Assert.AreEqual(1, (int)await DotStatDb.ExecuteScalarSqlAsync($"SELECT COUNT(*) FROM [data].{view}", CancellationToken));
        }

        private IAsyncEnumerable<ObservationRow> GetObservationRows(IReadOnlyList<string> components)
        {
            var i = 0;
            //Dimensions
            var hasWildCardedDimensions = false;
            var seriesKey = new List<IKeyValue>();
            foreach (var dimId in _dataflow.Dimensions.Where(d => !d.Base.TimeDimension).Select(d => d.Base.Id))
            {
                var keyVal = new KeyValueImpl(components[i++], dimId);
                seriesKey.Add(keyVal);
                if (string.IsNullOrEmpty(keyVal.Code) || keyVal.Code == DbExtensions.DimensionSwitchedOff || keyVal.Code == DbExtensions.DimensionWildCarded)
                    hasWildCardedDimensions = true;
            }

            var key = new KeyableImpl(_dataflow.Base, _dataflow.Dsd.Base, seriesKey, null);
            var obsTime = components[i++];
            if (string.IsNullOrEmpty(obsTime) || obsTime == DbExtensions.DimensionSwitchedOff || obsTime == DbExtensions.DimensionWildCarded)
                hasWildCardedDimensions = true;

            //Attributes
            var attributeValues = new List<IKeyValue>();
            if (components.Skip(6).Any(x => !string.IsNullOrEmpty(x)))
            {
                foreach (var dimId in _dataflow.Dsd.Msd.MetadataAttributes.Select(d => d.HierarchicalId))
                {
                    if (!string.IsNullOrEmpty(components[i]))
                    {
                        attributeValues.Add(new KeyValueImpl(components[i], dimId));
                    }

                    i++;
                }
            }

            var observation = new ObservationImpl(key, obsTime, null, attributeValues, crossSectionValue: null);

            return new List<ObservationRow>
            {
                new(1, StagingRowActionEnum.Delete, observation, hasWildCardedDimensions)
            }.ToAsyncEnumerable();
        }

        private async Task InitializeAllData()
        {
            await UnitOfWork.MetadataStoreRepository.TruncateMetadataTables(_dataflow.Dsd, DbTableVersion.A, CancellationToken);

            var observations = ObservationGenerator.Generate(
                _dataflow.Dsd,
                _dataflow,
                false,
                2023,
                2023,
                8,
                action: StagingRowActionEnum.Merge);

            await ImportObservations(observations, true);
        }

        private async Task ImportObservations(IAsyncEnumerable<ObservationRow> observations, bool initialiseDataSetAttributes = false)
        {
            var reportedComponents = new ReportedComponents
            {
                Dimensions = _dataflow.Dimensions.ToList(),
                MetadataAttributes = _dataflow.Dsd.Msd.MetadataAttributes.ToList(),
                IsPrimaryMeasureReported = false,
                TimeDimension = _dataflow.Dimensions.First(x => x.Base.TimeDimension)
            };

            var transactionId = await UnitOfWork.TransactionRepository.GetNextTransactionId(CancellationToken);
            var transaction = await UnitOfWork.TransactionRepository.CreateTransactionItem(transactionId,
                _dataflow.FullId, null, null, "testFile.csv", TransactionType.Import, TargetVersion.Live, serviceId: null,
                CancellationToken);

            var tryNewTransactionResult = await DotStatDbService.TryNewTransaction(transaction, _dataflow, MsAccess, CancellationToken);

            await UnitOfWork.MetadataStoreRepository.RecreateMetadataStagingTables(_dataflow.Dsd, CancellationToken);

            var codeTranslator = new CodeTranslator(UnitOfWork.CodeListRepository);
            await codeTranslator.FillDict(_dataflow, CancellationToken);

            var bulkImportResult = await UnitOfWork.MetadataStoreRepository.BulkInsertMetadata(observations, reportedComponents, codeTranslator, _dataflow.Dsd,
                false, false, CancellationToken);

            var datasetAttributes = initialiseDataSetAttributes
                ? new List<DataSetAttributeRow>
                    {  new (1, StagingRowActionEnum.Merge)
                       {
                        Attributes = new List<IKeyValue>{
                                    new KeyValueImpl("value", "STRING_TYPE"),
                                    new KeyValueImpl("value2", "STRING_MULTILANG_TYPE")
                        }
                    }}
                : bulkImportResult.DataSetLevelAttributeRows;

            await UnitOfWork.MetadataStoreRepository.MergeStagingTable(_dataflow, reportedComponents, 
                bulkImportResult.BatchActions, datasetAttributes, codeTranslator, DbTableVersion.A, true, CancellationToken);

            _dataflow.Dsd.LiveVersion = (char?)DbTableVersion.A;
            await DotStatDbService.CloseTransaction(transaction, _dataflow, false, false, true,
                MsAccess, false, false, null, CancellationToken);
        }
    }
}
