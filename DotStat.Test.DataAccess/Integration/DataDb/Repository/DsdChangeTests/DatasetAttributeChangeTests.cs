﻿using System.Threading.Tasks;
using DotStat.Db.Exception;
using DotStat.Domain;
using NUnit.Framework;

namespace DotStat.Test.DataAccess.Integration.DataDb.Repository.DsdChangeTests
{
    [TestFixture, Parallelizable(ParallelScope.Children)]
    [TestFixture(false)]
    [TestFixture(true)]
    public class DatasetAttributeChangeTests : BaseDsdChangeTests
    {
        private IImportReferenceableStructure _originalStructure;
        private IImportReferenceableStructure _originalStructure4NewCodeTest;
        private readonly bool _useDsdAsReferencedStructure;

        public DatasetAttributeChangeTests(bool useDsdAsReferencedStructure)
        {
            _useDsdAsReferencedStructure = useDsdAsReferencedStructure;
        }

        [OneTimeSetUp]
        public async Task SetUp()
        {
            _originalStructure = await InitOriginalStructure("ATTR_TYPE_DIFF_TEST.xml", useDsdReference: _useDsdAsReferencedStructure);
            _originalStructure4NewCodeTest = await InitOriginalStructure("ATTR_TYPE_DIFF_TEST_DATASET_2.xml", useDsdReference: _useDsdAsReferencedStructure);
        }

        [OneTimeTearDown]
        public async Task TearDown()
        {
            await CleanUpStructure(_originalStructure);
            await CleanUpStructure(_originalStructure4NewCodeTest);
        }

        [Test]
        public void DatasetAttributeAddedTest()
        {
            var referencedStructure =
                GetStructureWithBaseStructures("ATTR_TYPE_DIFF_TEST_DATASET_ATTR_ADDED.xml", useDsdReference: _useDsdAsReferencedStructure);
            referencedStructure.Dsd.DbId = _originalStructure.Dsd.DbId;

            var ex = Assert.ThrowsAsync<ChangeInDsdException>(
                async () => await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(referencedStructure, MappingStoreDataAccess, CancellationToken));

            StringAssert.Contains("OECD:ATTR_TYPE_DSD(11.1).ATTR_DATASET_NEW", ex.Message);
        }

        [Test]
        public void DatasetAttributeRemovedTest()
        {
            var referencedStructure =
                GetStructureWithBaseStructures("ATTR_TYPE_DIFF_TEST_DATASET_ATTR_REMOVED.xml", useDsdReference: _useDsdAsReferencedStructure);
            referencedStructure.Dsd.DbId = _originalStructure.Dsd.DbId;

            var ex = Assert.ThrowsAsync<ChangeInDsdException>(
                async () => await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(referencedStructure, MappingStoreDataAccess, CancellationToken));

            StringAssert.Contains("OECD:ATTR_TYPE_DSD(11.1)", ex.Message);
            StringAssert.Contains("ATTR_DATASET_STRING", ex.Message);
        }

        [Test]
        public void DatasetAttributeCodedChangedToNonCodedTest()
        {
            var referencedStructure =
                GetStructureWithBaseStructures(
                    "ATTR_TYPE_DIFF_TEST_DATASET_ENUMERATED_TO_STRING.xml", useDsdReference: _useDsdAsReferencedStructure);
            referencedStructure.Dsd.DbId = _originalStructure.Dsd.DbId;

            var ex = Assert.ThrowsAsync<ChangeInDsdException>(
                async () => await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(referencedStructure, MappingStoreDataAccess, CancellationToken));

            StringAssert.Contains("OECD:ATTR_TYPE_DSD(11.1).ATTR_DATASET_ENUMERATED", ex.Message);
        }

        [Test]
        public void DatasetAttributeNonCodedChangedToCodedTest()
        {
            var referencedStructure =
                GetStructureWithBaseStructures(
                    "ATTR_TYPE_DIFF_TEST_DATASET_STRING_TO_ENUMERATED.xml", useDsdReference: _useDsdAsReferencedStructure);
            referencedStructure.Dsd.DbId = _originalStructure.Dsd.DbId;

            var ex = Assert.ThrowsAsync<ChangeInDsdException>(
                async () => await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(referencedStructure, MappingStoreDataAccess, CancellationToken));

            StringAssert.Contains("OECD:ATTR_TYPE_DSD(11.1).ATTR_DATASET_STRING", ex.Message);
        }

        [Test]
        public void DatasetAttributeMandatoryToConditional()
        {
            var referencedStructure =
                GetStructureWithBaseStructures(
                    "ATTR_TYPE_DIFF_TEST_DATASET_MANDATORY_TO_CONDITIONAL.xml", useDsdReference: _useDsdAsReferencedStructure);
            referencedStructure.Dsd.DbId = _originalStructure.Dsd.DbId;

            var ex = Assert.ThrowsAsync<ChangeInDsdException>(
                async () => await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(referencedStructure, MappingStoreDataAccess, CancellationToken));

            StringAssert.Contains("OECD:ATTR_TYPE_DSD(11.1).ATTR_DATASET_STRING", ex.Message);
            StringAssert.Contains("from Mandatory to Conditional", ex.Message);
        }

        [Test]
        public void DatasetAttributeConditionalToMandatory()
        {
            var referencedStructure =
                GetStructureWithBaseStructures(
                    "ATTR_TYPE_DIFF_TEST_DATASET_CONDITIONAL_TO_MANDATORY.xml", useDsdReference: _useDsdAsReferencedStructure);
            referencedStructure.Dsd.DbId = _originalStructure.Dsd.DbId;

            var ex = Assert.ThrowsAsync<ChangeInDsdException>(
                async () => await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(referencedStructure, MappingStoreDataAccess, CancellationToken));

            StringAssert.Contains("OECD:ATTR_TYPE_DSD(11.1).ATTR_DATASET_ENUMERATED", ex.Message);
            StringAssert.Contains("Conditional to Mandatory", ex.Message);
        }

        [Test]
        public void DatasetAttributeChangedToDimensionLevelTest()
        {
            var referencedStructure =
                GetStructureWithBaseStructures(
                    "ATTR_TYPE_DIFF_TEST_DATASET_TO_DIMENSION.xml", useDsdReference: _useDsdAsReferencedStructure);
            referencedStructure.Dsd.DbId = _originalStructure.Dsd.DbId;

            var ex = Assert.ThrowsAsync<ChangeInDsdException>(
                async () => await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(referencedStructure, MappingStoreDataAccess, CancellationToken));

            StringAssert.Contains("OECD:ATTR_TYPE_DSD(11.1).ATTR_DATASET_STRING", ex.Message);
            StringAssert.Contains("from DataSet to DimensionGroup", ex.Message);
        }

        [Test]
        public void DatasetAttributeChangedToObservationLevelTest()
        {
            var referencedStructure =
                GetStructureWithBaseStructures(
                    "ATTR_TYPE_DIFF_TEST_DATASET_TO_OBSERVATION.xml", useDsdReference: _useDsdAsReferencedStructure);
            referencedStructure.Dsd.DbId = _originalStructure.Dsd.DbId;

            var ex = Assert.ThrowsAsync<ChangeInDsdException>(
                async () => await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(referencedStructure, MappingStoreDataAccess, CancellationToken));

            StringAssert.Contains("OECD:ATTR_TYPE_DSD(11.1).ATTR_DATASET_STRING", ex.Message);
            StringAssert.Contains("from DataSet to Observation", ex.Message);
        }

        [Test]
        public void DatasetAttributeChangedToGroupLevelTest()
        {
            var referencedStructure =
                GetStructureWithBaseStructures(
                    "ATTR_TYPE_DIFF_TEST_DATASET_TO_GROUP.xml", useDsdReference: _useDsdAsReferencedStructure);
            referencedStructure.Dsd.DbId = _originalStructure.Dsd.DbId;

            var ex = Assert.ThrowsAsync<ChangeInDsdException>(
                async () => await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(referencedStructure, MappingStoreDataAccess, CancellationToken));

            StringAssert.Contains("OECD:ATTR_TYPE_DSD(11.1).ATTR_DATASET_STRING", ex.Message);
            StringAssert.Contains("from DataSet to Group", ex.Message);
            StringAssert.Contains("from DataSet to Group", ex.Message);
        }

        [Test]
        public void DatasetAttributeCodelistChangeTest()
        {
            var referencedStructure =
                GetStructureWithBaseStructures("ATTR_TYPE_DIFF_TEST_DATASET_NEW_CODELIST.xml", useDsdReference: _useDsdAsReferencedStructure);
            referencedStructure.Dsd.DbId = _originalStructure.Dsd.DbId;

            var ex = Assert.ThrowsAsync<ChangeInDsdException>(
                async () => await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(referencedStructure, MappingStoreDataAccess, CancellationToken));

            StringAssert.Contains("OECD:ATTR_TYPE_DSD(11.1).ATTR_DATASET_ENUMERATED", ex.Message);
            StringAssert.Contains("OECD:CL_FREQ(11.0)", ex.Message); //New code list
        }

        [Test]
        public void DatasetAttributeCodelistHasNewCodeTest()
        {
            var referencedStructure =
                GetStructureWithBaseStructures("ATTR_TYPE_DIFF_TEST_DATASET_2_NEW_CODE_IN_CODELIST.xml", useDsdReference: _useDsdAsReferencedStructure);
            referencedStructure.Dsd.DbId = _originalStructure4NewCodeTest.Dsd.DbId;

            Assert.DoesNotThrowAsync(
                async () => await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(referencedStructure, MappingStoreDataAccess, CancellationToken));
        }

        [Test]
        public void DatasetAttributeCodelistHasRemovedCodeTest()
        {
            var referencedStructure =
                GetStructureWithBaseStructures(
                    "ATTR_TYPE_DIFF_TEST_DATASET_CODE_REMOVED_FROM_CODELIST.xml", useDsdReference: _useDsdAsReferencedStructure);
            referencedStructure.Dsd.DbId = _originalStructure.Dsd.DbId;

            var ex = Assert.ThrowsAsync<ChangeInDsdException>(
                async () => await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(referencedStructure, MappingStoreDataAccess, CancellationToken));

            StringAssert.Contains("T2, T3", ex.Message); // codes removed
            StringAssert.Contains("OECD:ATTR_TYPE_DSD(11.1).ATTR_DATASET_ENUMERATED", ex.Message); //affected component
            StringAssert.Contains("OECD:CL_TEST_DATASET(11.0)", ex.Message); //affected codelist
        }
    }
}

