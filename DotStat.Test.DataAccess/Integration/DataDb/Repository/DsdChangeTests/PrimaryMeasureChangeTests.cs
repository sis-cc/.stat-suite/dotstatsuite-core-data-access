﻿using System.Threading.Tasks;
using DotStat.Common.Exceptions;
using DotStat.Db.Exception;
using DotStat.Domain;
using NUnit.Framework;
using Org.Sdmxsource.Sdmx.Api.Constants;

namespace DotStat.Test.DataAccess.Integration.DataDb.Repository.DsdChangeTests
{
    [TestFixture, Parallelizable(ParallelScope.Children)]
    [TestFixture(false)]
    [TestFixture(true)]
    public class PrimaryMeasureChangeTests : BaseDsdChangeTests
    {
        private IImportReferenceableStructure _originalStringStructure;
        private IImportReferenceableStructure _originalEnumeratedStructure;
        private IImportReferenceableStructure _originalEnumerated2Structure;
        private readonly bool _useDsdAsReferencedStructure;

        public PrimaryMeasureChangeTests(bool useDsdAsReferencedStructure)
        {
            _useDsdAsReferencedStructure = useDsdAsReferencedStructure;
        }

        [OneTimeSetUp]
        public async Task SetUp()
        {
            _originalStringStructure = await InitOriginalStructure("OBS_TYPE_DIFF_TEST_STRING_ALL.xml", useDsdReference: _useDsdAsReferencedStructure);
            _originalEnumeratedStructure = await InitOriginalStructure("OBS_TYPE_DIFF_TEST_ENUMERATED.xml", useDsdReference: _useDsdAsReferencedStructure);
            _originalEnumerated2Structure = await InitOriginalStructure("OBS_TYPE_DIFF_TEST_ENUMERATED2.xml", useDsdReference: _useDsdAsReferencedStructure);
        }

        [OneTimeTearDown]
        public async Task TearDown()
        {
            await CleanUpStructure(_originalStringStructure);
            await CleanUpStructure(_originalEnumeratedStructure);
            await CleanUpStructure(_originalEnumerated2Structure);
        }

        [Test]
        public void TextFormatMaxLengthChangeTest()
        {
            var referencedStructure = GetStructureWithBaseStructures("OBS_TYPE_DIFF_TEST_STRING_ALL_MAX_LENGTH.xml", useDsdReference: _useDsdAsReferencedStructure);
            referencedStructure.Dsd.DbId = _originalStringStructure.Dsd.DbId;

            var ex = Assert.ThrowsAsync<ChangeInDsdException>(async () => await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(referencedStructure, MappingStoreDataAccess, CancellationToken));

            StringAssert.Contains("maxLength", ex.Message);
            StringAssert.Contains("OECD:OBS_TYPE_DSD_MINMAX_STRING(11.0).OBS_VALUE", ex.Message);
            StringAssert.IsMatch("^.*[^0-9]5[^0-9].*$", ex.Message); //Original value
            StringAssert.IsMatch("^.*[^0-9]89[^0-9].*$", ex.Message); //New value

            referencedStructure = GetStructureWithBaseStructures("OBS_TYPE_DIFF_TEST_STRING_NO_MAX_LENGTH.xml", useDsdReference: _useDsdAsReferencedStructure); ;
            referencedStructure.Dsd.DbId = _originalStringStructure.Dsd.DbId;

            ex = Assert.ThrowsAsync<ChangeInDsdException>(
                async () => await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(referencedStructure, MappingStoreDataAccess, CancellationToken));

            StringAssert.Contains("maxLength", ex.Message);
            StringAssert.Contains("OECD:OBS_TYPE_DSD_MINMAX_STRING(11.0).OBS_VALUE", ex.Message);
            StringAssert.IsMatch("^.*[^0-9]5[^0-9].*$", ex.Message); //Original value

        }

        [Test]
        public void TextFormatMinLengthChangeTest()
        {
            var referencedStructure = GetStructureWithBaseStructures("OBS_TYPE_DIFF_TEST_STRING_ALL_MIN_LENGTH.xml", useDsdReference: _useDsdAsReferencedStructure);
            referencedStructure.Dsd.DbId = _originalStringStructure.Dsd.DbId;

            var ex = Assert.ThrowsAsync<ChangeInDsdException>(
                async () => await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(referencedStructure, MappingStoreDataAccess, CancellationToken));

            StringAssert.Contains("minLength", ex.Message);
            StringAssert.Contains("OECD:OBS_TYPE_DSD_MINMAX_STRING(11.0).OBS_VALUE", ex.Message);
            StringAssert.IsMatch("^.*[^0-9]2[^0-9].*$", ex.Message); //Original value
            StringAssert.IsMatch("^.*[^0-9]1[^0-9].*$", ex.Message); //New value

            referencedStructure = GetStructureWithBaseStructures("OBS_TYPE_DIFF_TEST_STRING_NO_MIN_LENGTH.xml", useDsdReference: _useDsdAsReferencedStructure);
            referencedStructure.Dsd.DbId = _originalStringStructure.Dsd.DbId;

            ex = Assert.ThrowsAsync<ChangeInDsdException>(
                async () => await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(referencedStructure, MappingStoreDataAccess, CancellationToken));

            StringAssert.Contains("minLength", ex.Message);
            StringAssert.Contains("OECD:OBS_TYPE_DSD_MINMAX_STRING(11.0).OBS_VALUE", ex.Message);
            StringAssert.IsMatch("^.*[^0-9]2[^0-9].*$", ex.Message); //Original value

        }

        [Test]
        public void TextFormatPatternChangeTest()
        {
            var referencedStructure = GetStructureWithBaseStructures("OBS_TYPE_DIFF_TEST_STRING_ALL_PATTERN.xml", useDsdReference: _useDsdAsReferencedStructure);
            referencedStructure.Dsd.DbId = _originalStringStructure.Dsd.DbId;

            var ex = Assert.ThrowsAsync<ChangeInDsdException>(async () => await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(referencedStructure, MappingStoreDataAccess, CancellationToken));

            StringAssert.Contains("pattern", ex.Message);
            StringAssert.Contains("OECD:OBS_TYPE_DSD_MINMAX_STRING(11.0).OBS_VALUE", ex.Message);
            StringAssert.Contains("^[A-Z][a-zA-Z0-0]*$", ex.Message); //Original value
            StringAssert.Contains("^[0-9][a-zA-Z0-0]*$", ex.Message); //New value

            referencedStructure = GetStructureWithBaseStructures("OBS_TYPE_DIFF_TEST_STRING_NO_PATTERN.xml", useDsdReference: _useDsdAsReferencedStructure);
            referencedStructure.Dsd.DbId = _originalStringStructure.Dsd.DbId;

            ex = Assert.ThrowsAsync<ChangeInDsdException>(async () => await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(referencedStructure, MappingStoreDataAccess, CancellationToken));

            StringAssert.Contains("pattern", ex.Message);
            StringAssert.Contains("OECD:OBS_TYPE_DSD_MINMAX_STRING(11.0).OBS_VALUE", ex.Message);
            StringAssert.Contains("^[A-Z][a-zA-Z0-0]*$", ex.Message); //Original value

        }

        [Test]
        public void TextFormatMaxValueChangeTest()
        {
            var referencedStructure = GetStructureWithBaseStructures("OBS_TYPE_DIFF_TEST_STRING_ALL_MAX_VALUE.xml", useDsdReference: _useDsdAsReferencedStructure);
            referencedStructure.Dsd.DbId = _originalStringStructure.Dsd.DbId;

            var ex = Assert.ThrowsAsync<ChangeInDsdException>(async () =>
                await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(referencedStructure, MappingStoreDataAccess,
                    CancellationToken));

            StringAssert.Contains("maxValue", ex.Message);
            StringAssert.Contains("OECD:OBS_TYPE_DSD_MINMAX_STRING(11.0).OBS_VALUE", ex.Message);
            StringAssert.IsMatch("^.*[^0-9]10000[^0-9].*$", ex.Message); //Original value
            StringAssert.IsMatch("^.*[^0-9]9000[^0-9].*$", ex.Message); //New value

            referencedStructure = GetStructureWithBaseStructures("OBS_TYPE_DIFF_TEST_STRING_NO_MAX_VALUE.xml", useDsdReference: _useDsdAsReferencedStructure);
            referencedStructure.Dsd.DbId = _originalStringStructure.Dsd.DbId;

            ex = Assert.ThrowsAsync<ChangeInDsdException>(async () =>
                await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(referencedStructure, MappingStoreDataAccess,
                    CancellationToken));

            StringAssert.Contains("maxValue", ex.Message);
            StringAssert.Contains("OECD:OBS_TYPE_DSD_MINMAX_STRING(11.0).OBS_VALUE", ex.Message);
            StringAssert.IsMatch("^.*[^0-9]10000[^0-9].*$", ex.Message); //Original value

        }

        [Test]
        public void TextFormatMinValueChangeTest()
        {
            var referencedStructure = GetStructureWithBaseStructures("OBS_TYPE_DIFF_TEST_STRING_ALL_MIN_VALUE.xml", useDsdReference: _useDsdAsReferencedStructure);
            referencedStructure.DbId = _originalStringStructure.DbId;
            referencedStructure.Dsd.DbId = _originalStringStructure.Dsd.DbId;

            var ex = Assert.ThrowsAsync<ChangeInDsdException>(async () => await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(referencedStructure, MappingStoreDataAccess, CancellationToken));

            StringAssert.Contains("minValue", ex.Message);
            StringAssert.Contains("OECD:OBS_TYPE_DSD_MINMAX_STRING(11.0).OBS_VALUE", ex.Message);
            StringAssert.IsMatch("^.*[^0-9]10[^0-9].*$", ex.Message); //Original value
            StringAssert.IsMatch("^.*[^0-9]100[^0-9].*$", ex.Message); //New value

            referencedStructure = GetStructureWithBaseStructures("OBS_TYPE_DIFF_TEST_STRING_NO_MIN_VALUE.xml", useDsdReference: _useDsdAsReferencedStructure);
            referencedStructure.Dsd.DbId = _originalStringStructure.Dsd.DbId;

            ex = Assert.ThrowsAsync<ChangeInDsdException>(
                async () => await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(referencedStructure, MappingStoreDataAccess, CancellationToken));

            StringAssert.Contains("minValue", ex.Message);
            StringAssert.Contains("OECD:OBS_TYPE_DSD_MINMAX_STRING(11.0).OBS_VALUE", ex.Message);
            StringAssert.IsMatch("^.*[^0-9]10[^0-9].*$", ex.Message); //Original value
        }

        [Test]
        public void TextFormatRepresentationChangeTest()
        {
            var referencedStructure = GetStructureWithBaseStructures("OBS_TYPE_DIFF_TEST_STRING_ALL_ALPHA.xml", useDsdReference: _useDsdAsReferencedStructure);
            referencedStructure.Dsd.DbId = _originalStringStructure.Dsd.DbId;

            var ex = Assert.ThrowsAsync<ChangeInDsdException>(async () => await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(referencedStructure, MappingStoreDataAccess, CancellationToken));

            StringAssert.Contains("textformat representation", ex.Message);
            StringAssert.Contains("OECD:OBS_TYPE_DSD_MINMAX_STRING(11.0).OBS_VALUE", ex.Message);
            StringAssert.Contains("String", ex.Message); //Original value
            StringAssert.Contains("Alpha", ex.Message); //New value
        }

        [Test]
        public void TextFormatNonCodedToCodedChangeTest()
        {
            var referencedStructure = GetStructureWithBaseStructures("OBS_TYPE_DIFF_TEST_STRING_ALL_ENUMERATED.xml", useDsdReference: _useDsdAsReferencedStructure);
            referencedStructure.Dsd.DbId = _originalStringStructure.Dsd.DbId;

            var ex = Assert.ThrowsAsync<ChangeInDsdException>(async () => await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(referencedStructure, MappingStoreDataAccess, CancellationToken));

            StringAssert.Contains("representation", ex.Message);
            StringAssert.Contains("OECD:OBS_TYPE_DSD_MINMAX_STRING(11.0).OBS_VALUE", ex.Message);
            StringAssert.Contains("String", ex.Message); //Original value
            StringAssert.Contains("OECD:CL_FREQ(11.0)", ex.Message); //New value
        }

        [Test]
        public async Task DefaultTextFormatWhenNotProvidedTest()
        {
            if(_useDsdAsReferencedStructure) return;//TODO: test not working with dsd reference
            var origStructure = await InitOriginalStructure("OBS_TYPE_DIFF_TEST_STRING_NO_REPRESENTATION.xml", useDsdReference: _useDsdAsReferencedStructure);

            var referencedStructure = GetStructureWithBaseStructures("OBS_TYPE_DIFF_TEST_STRING_ONLY_STRING.xml", useDsdReference: _useDsdAsReferencedStructure);
            referencedStructure.Dsd.DbId = origStructure.Dsd.DbId;

            Assert.DoesNotThrowAsync(
                async () => await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(referencedStructure, MappingStoreDataAccess, CancellationToken));
        }

        [Test]
        public void TextFormatCodelistChangeTest()
        {
            var referencedStructure = GetStructureWithBaseStructures("OBS_TYPE_DIFF_TEST_ENUMERATED_NEW_CODELIST.xml", useDsdReference: _useDsdAsReferencedStructure);
            referencedStructure.Dsd.DbId = _originalEnumeratedStructure.Dsd.DbId;

            var ex = Assert.ThrowsAsync<ChangeInDsdException>(async () => await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(referencedStructure, MappingStoreDataAccess, CancellationToken));

            StringAssert.Contains("new code list", ex.Message);
            StringAssert.Contains("OECD:OBS_TYPE_DSD_ENUMERATED(11.0).OBS_VALUE", ex.Message);
            StringAssert.Contains("OECD:CL_FREQ(11.0)", ex.Message); //New code list

        }

        [Test]
        public void TextFormatCodelistHasNewCodeTest()
        {
            var referencedStructure = GetStructureWithBaseStructures("OBS_TYPE_DIFF_TEST_ENUMERATED2_NEW_CODE_IN_CODELIST.xml", useDsdReference: _useDsdAsReferencedStructure);
            referencedStructure.Dsd.DbId = _originalEnumerated2Structure.Dsd.DbId;

            Assert.DoesNotThrowAsync(
                async () => await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(referencedStructure, MappingStoreDataAccess, CancellationToken));
        }

        [Test]
        public void TextFormatCodelistHasRemovedCodeTest()
        {
            var referencedStructure = GetStructureWithBaseStructures("OBS_TYPE_DIFF_TEST_ENUMERATED_CODE_REMOVED_FROM_CODELIST.xml", useDsdReference: _useDsdAsReferencedStructure);
            referencedStructure.Dsd.DbId = _originalEnumeratedStructure.Dsd.DbId;

            var ex = Assert.ThrowsAsync<ChangeInDsdException>(
                async () => await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(referencedStructure, MappingStoreDataAccess, CancellationToken));

            StringAssert.Contains("27, 28", ex.Message); // codes removed
            StringAssert.Contains("OECD:OBS_TYPE_DSD_ENUMERATED(11.0).OBS_VALUE", ex.Message); //affected component
            StringAssert.Contains("OECD:CL_MEASURE(11.0)", ex.Message); //affected codelist
        }

        [Test]
        public void TextFormatCodedToNonCodedChangeTest()
        {
            var referencedStructure = GetStructureWithBaseStructures("OBS_TYPE_DIFF_TEST_ENUMERATED_STRING.xml", useDsdReference: _useDsdAsReferencedStructure);
            referencedStructure.Dsd.DbId = _originalEnumeratedStructure.Dsd.DbId;

            var ex = Assert.ThrowsAsync<ChangeInDsdException>(
                async () => await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(referencedStructure, MappingStoreDataAccess, CancellationToken));

            StringAssert.Contains("representation", ex.Message);
            StringAssert.Contains("OECD:OBS_TYPE_DSD_ENUMERATED(11.0).OBS_VALUE", ex.Message);
            StringAssert.Contains("OECD:CL_MEASURE(11.0)", ex.Message); //Original value
            StringAssert.Contains("String", ex.Message); //New value
        }

        [Test]
        public void TextFormatIncrementalNotSupported()
        {
            if (_useDsdAsReferencedStructure) return;//TODO: test not working with dsd reference
            var ex = Assert.ThrowsAsync(typeof(DotStatException),
                () => InitOriginalStructure("OBS_TYPE_DIFF_TEST_INCREMENTAL.xml", useDsdReference: _useDsdAsReferencedStructure));

            StringAssert.Contains(TextEnumType.Incremental.ToString(), ex.Message);
            StringAssert.Contains("OECD:OBS_TYPE_DSD_INCREMENTAL(11.0).OBS_VALUE", ex.Message);
        }
    }
}

