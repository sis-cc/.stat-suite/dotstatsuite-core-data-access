﻿using NUnit.Framework;

namespace DotStat.Test.DataAccess.Integration.DataDb
{
    [SetUpFixture]
    internal sealed class Setup : BaseDbSetup
    {
        private readonly string _sqlConnectionString;
        
        public Setup()
        {
            _sqlConnectionString = Configuration.SpacesInternal[0].DotStatSuiteCoreDataDbConnectionString;
        }

        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            CreateDb(_sqlConnectionString, "Data");
        }

        [OneTimeTearDown]
        public void OneTimeTearDown()
        {
            DeleteDb(_sqlConnectionString);
        }
    }
}
