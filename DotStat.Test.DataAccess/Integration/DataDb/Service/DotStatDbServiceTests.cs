﻿using System.Linq;
using System.Threading.Tasks;
using DotStat.Db;
using DotStat.Domain;
using NUnit.Framework;

namespace DotStat.Test.DataAccess.Integration.DataDb.Service
{
    [NonParallelizable]
    public class DotStatDbServiceTests : BaseDbIntegrationTests
    {
        private Dataflow _dataflow;

        public DotStatDbServiceTests() : base("sdmx/CsvV2.xml")
        {
            _dataflow = this.GetDataflow();
            InitDbObjects();
        }

        public void InitDbObjects()
        {
            var tableVersion = DbTableVersion.A;
            var targetVersion = TargetVersion.Live;

            var transactionId = UnitOfWork.TransactionRepository.GetNextTransactionId(CancellationToken).Result;
            var transaction = UnitOfWork.TransactionRepository.CreateTransactionItem(transactionId, _dataflow.Dsd.FullId, null, null, "dataSource", TransactionType.Import, targetVersion, serviceId: null, CancellationToken).Result;

            // create dataflow DB objects 
            var tryNewTransactionResult =
                DotStatDbService.TryNewTransaction(transaction, _dataflow, MsAccess, CancellationToken).Result;
            
            // close transaction
            _dataflow.Dsd.LiveVersion = (char?)tableVersion;
            var success = DotStatDbService.CloseTransaction(transaction, _dataflow, false, false, true,
                MsAccess, false, false, null, CancellationToken).Result;
        }

        [Test, Order(1)]
        [TestCase(DataCompressionEnum.NONE, DataCompressionEnum.NONE, false)]
        [TestCase(DataCompressionEnum.NONE, DataCompressionEnum.COLUMNSTORE_ARCHIVE, true)]
        [TestCase(DataCompressionEnum.NONE, DataCompressionEnum.COLUMNSTORE, true)]
        [TestCase(DataCompressionEnum.COLUMNSTORE_ARCHIVE, DataCompressionEnum.NONE, false)]
        [TestCase(DataCompressionEnum.COLUMNSTORE_ARCHIVE, DataCompressionEnum.COLUMNSTORE_ARCHIVE, false)]
        [TestCase(DataCompressionEnum.COLUMNSTORE_ARCHIVE, DataCompressionEnum.COLUMNSTORE, true)]
        [TestCase(DataCompressionEnum.COLUMNSTORE, DataCompressionEnum.NONE, false)]
        [TestCase(DataCompressionEnum.COLUMNSTORE_ARCHIVE, DataCompressionEnum.COLUMNSTORE_ARCHIVE, false)]
        [TestCase(DataCompressionEnum.COLUMNSTORE, DataCompressionEnum.COLUMNSTORE, false)]
        public async Task ChangeDataCompression(DataCompressionEnum initialCompression, DataCompressionEnum dataCompression, bool expectedChanged)
        {
            _dataflow.Dsd.DataCompression = initialCompression;
            var dsd = _dataflow.Dsd;
            var changed = await DotStatDbService.ChangeDataCompression(_dataflow.Dsd, dataCompression, MsAccess, CancellationToken);
            Assert.AreEqual(expectedChanged, changed);

            if(!expectedChanged)
                return;

            var expectedTables = new[]
            {
                $"{dsd.SqlFilterTable()}",
                $"{dsd.SqlFactTable((char)DbTableVersion.A)}",
                $"{dsd.SqlFactTable((char)DbTableVersion.B)}",
                $"{dsd.SqlDeletedTable((char)DbTableVersion.A)}",
                $"{dsd.SqlDeletedTable((char)DbTableVersion.B)}",
                $"{dsd.SqlDimGroupAttrTable((char)DbTableVersion.A)}",
                $"{dsd.SqlDimGroupAttrTable((char)DbTableVersion.B)}",
                $"{dsd.SqlMetadataDataStructureTable((char)DbTableVersion.A)}",
                $"{dsd.SqlMetadataDataStructureTable((char)DbTableVersion.B)}",
                $"{dsd.SqlDeletedMetadataTable((char)DbTableVersion.A)}",
                $"{dsd.SqlDeletedMetadataTable((char)DbTableVersion.B)}",
                $"{_dataflow.SqlMetadataDataflowTable((char)DbTableVersion.A)}",
                $"{_dataflow.SqlMetadataDataflowTable((char)DbTableVersion.B)}"
            };

            foreach (var tableName in expectedTables)
            {
                var indexName = $"CCI_{tableName}";
                var indexExists = await IndexExists(indexName, tableName, dataCompression);
                Assert.IsTrue(indexExists);
            }

            if (dataCompression is DataCompressionEnum.COLUMNSTORE)
            {
                //Check unique indexes - Not created for ARCHIVE
                expectedTables = new[]
                {
                    $"{dsd.SqlFilterTable()}",
                    $"{dsd.SqlFactTable((char)DbTableVersion.A)}",
                    $"{dsd.SqlFactTable((char)DbTableVersion.B)}",
                    $"{dsd.SqlDimGroupAttrTable((char)DbTableVersion.A)}",
                    $"{dsd.SqlDimGroupAttrTable((char)DbTableVersion.B)}",
                    $"{dsd.SqlMetadataDataStructureTable((char)DbTableVersion.A)}",
                    $"{dsd.SqlMetadataDataStructureTable((char)DbTableVersion.B)}",
                    $"{_dataflow.SqlMetadataDataflowTable((char)DbTableVersion.A)}",
                    $"{_dataflow.SqlMetadataDataflowTable((char)DbTableVersion.B)}"
                };

                var indexName = $"UI_{dsd.SqlFilterTable()}";
                var indexExists = await IndexExists(indexName, dsd.SqlFilterTable());

                Assert.IsTrue(indexExists);

                foreach (var tableName in expectedTables)
                {
                    indexName = $"PK_{tableName}";
                    indexExists = await IndexExists(indexName, tableName);
                    Assert.IsTrue(indexExists);
                }
            }
        }

        [Test, Order(2)]
        public async Task Cleanup()
        {
            var allDbComponents = UnitOfWork.ComponentRepository.GetAllComponents(CancellationToken).Result.ToList();
            var allMsdDbComponents = UnitOfWork.MetaMetadataComponentRepository.GetAllMetadataAttributeComponents(CancellationToken).Result.ToList();

            await DotStatDbService.CleanUpDsd(_dataflow.Dsd.DbId, false, this.MsAccess, allDbComponents, allMsdDbComponents, this.CancellationToken);
        }
    }
}
