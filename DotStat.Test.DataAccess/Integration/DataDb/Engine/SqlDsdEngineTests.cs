﻿using System.Linq;
using System.Threading.Tasks;
using DotStat.Db;
using DotStat.Db.Engine.SqlServer;
using DotStat.Domain;
using NUnit.Framework;

namespace DotStat.Test.DataAccess.Integration.DataDb.Engine
{
    [TestFixture, Parallelizable(ParallelScope.Fixtures)]
    public class SqlDsdEngineTests : BaseDbIntegrationTests
    {
        private SqlDsdEngine _engine;
        private Domain.Dataflow _dataflow;

        public SqlDsdEngineTests()
        {
            _engine = new SqlDsdEngine(Configuration);
            _dataflow = this.GetDataflow();

            InitDataForTests();
        }

        private void InitDataForTests()
        {
            var i = 100;

            foreach (var dim in _dataflow.Dsd.Dimensions)
                dim.DbId = i++;

            foreach (var attr in _dataflow.Dsd.Attributes)
                attr.DbId = i++;

            _dataflow.Dsd.PrimaryMeasure.DbId = i;
        }

        [Test, Order(1)]
        public async Task No_Artifact_Record_Should_Exist()
        {
            var id = await _engine.GetDbId(_dataflow.Dsd.Code, _dataflow.Dsd.AgencyId, _dataflow.Dsd.Version, DotStatDb, CancellationToken);

            Assert.AreEqual(-1, id);
        }

        [Test, Order(2)]
        public async Task Insert_Artifact()
        {
            var id = await _engine.InsertToArtefactTable(_dataflow.Dsd, DotStatDb, CancellationToken);

            Assert.IsTrue(id > 0);

            _dataflow.Dsd.DbId = await _engine.GetDbId(_dataflow.Dsd.Code, _dataflow.Dsd.AgencyId, _dataflow.Dsd.Version, DotStatDb, CancellationToken);

            Assert.AreEqual(_dataflow.Dsd.DbId, id);
        }

        [Test, Order(3)]
        public async Task Check_Dsd_Fact_Tables_Created()
        {
            var dsd = _dataflow.Dsd;

            // Create Fact tables ...

            await _engine.CreateDynamicDbObjects(dsd, DotStatDb, CancellationToken);

            var expectedTables = new string[]
            {
                dsd.SqlFactTable('A'),
                dsd.SqlFactTable('B'),
                dsd.SqlFilterTable(),
            };

            foreach (var table in expectedTables)
                Assert.IsTrue(await DotStatDb.TableExists(table, CancellationToken), $"Table {table} have not been found");
        }

        [Test, Order(4)]
        public async Task DropAllIndexes()
        {
            var dsd = _dataflow.Dsd;
            await _engine.DropAllIndexes(dsd, DotStatDb, CancellationToken);

            var expectedTables = new[]
            {
                $"{dsd.SqlFilterTable()}",
                $"{dsd.SqlFactTable((char)DbTableVersion.A)}",
                $"{dsd.SqlFactTable((char)DbTableVersion.B)}",
                $"{dsd.SqlDeletedTable((char)DbTableVersion.A)}",
                $"{dsd.SqlDeletedTable((char)DbTableVersion.B)}"
            };

            foreach (var tableName in expectedTables)
            {
                var indexCount = (int)await DotStatDb.ExecuteScalarSqlAsync($@"
SELECT count(*) FROM sys.indexes AS [i] 
WHERE [i].object_id = OBJECT_ID('[{DotStatDb.DataSchema}].[{tableName}]')
AND [type_desc] <>'HEAP';", CancellationToken);

                Assert.AreEqual(0, indexCount);
            }
        }

        [Test, Order(5)]
        public async Task AddUniqueConstraints()
        {
            var dsd = _dataflow.Dsd;
            await _engine.AddUniqueConstraints(dsd, clustered: true, DotStatDb, CancellationToken);

            var expectedTables = new[]
            {
                $"{dsd.SqlFilterTable()}",
                $"{dsd.SqlFactTable((char)DbTableVersion.A)}",
                $"{dsd.SqlFactTable((char)DbTableVersion.B)}",
            };

            var indexName = $"PK_{dsd.SqlFilterTable()}";
            var indexExists= await IndexExists(indexName, dsd.SqlFilterTable());
            Assert.IsTrue(indexExists);

            indexName = $"UI_{dsd.SqlFilterTable()}";
            indexExists = await IndexExists(indexName, dsd.SqlFilterTable());
            Assert.IsTrue(indexExists);

            foreach (var tableName in expectedTables)
            {
                indexName = $"PK_{tableName}";
                indexExists = await IndexExists(indexName, tableName);

                Assert.IsTrue(indexExists);
            }
        }

        [Test, Order(6)]
        [TestCase(DataCompressionEnum.COLUMNSTORE)]
        [TestCase(DataCompressionEnum.COLUMNSTORE_ARCHIVE)]
        public async Task Compress(DataCompressionEnum dataCompression)
        {
            var dsd = _dataflow.Dsd;
            await _engine.DropAllIndexes(dsd, DotStatDb, CancellationToken);
            await _engine.Compress(dsd, dataCompression, DotStatDb, CancellationToken);

            var expectedTables = new []
            {
                $"{dsd.SqlFilterTable()}",
                $"{dsd.SqlFactTable((char)DbTableVersion.A)}",
                $"{dsd.SqlFactTable((char)DbTableVersion.B)}",
                $"{dsd.SqlDeletedTable((char)DbTableVersion.A)}",
                $"{dsd.SqlDeletedTable((char)DbTableVersion.B)}"
            };

            foreach (var tableName in expectedTables)
            {
                var indexName = $"CCI_{tableName}";
                var indexExists = await IndexExists(indexName, tableName, dataCompression);

                Assert.IsTrue(indexExists);
            }
        }

        [Test, Order(7)]
        public async Task Check_Dsd_Fact_Temporal_Tables_Created()
        {
            var dsd = _dataflow.Dsd;
            dsd.KeepHistory = true;
            dsd.DbId = 100;

            // Create Fact tables ...

            await _engine.CreateDynamicDbObjects(dsd, DotStatDb, CancellationToken);

            var expectedTables = new string[]
            {
                dsd.SqlFactTable('A'),
                dsd.SqlFactTable('B'),
                $"{dsd.SqlFactHistoryTable('A')}",
                $"{dsd.SqlFactHistoryTable('B')}",
                dsd.SqlFilterTable(),
            };

            foreach (var table in expectedTables)
                Assert.IsTrue(await DotStatDb.TableExists(table, CancellationToken), $"Table {table} have not been found");
        }
        
        [TestCase(false, "CONVERT([binary](3), ISNULL([DIM_1], 0))+CONVERT([binary](3), ISNULL([DIM_2], 0))")]
        [TestCase(true, "CONVERT([binary](3), ISNULL([DIM_1], 0))+CONVERT([binary](3), ISNULL([DIM_2], 0))+CONVERT(binary(16), HASHBYTES('md5', ISNULL([PERIOD_SDMX], '')))")]
        public void BuildRowIdFormula(bool includeTimeDim, string expectedSql)
        {
            var dsd = new Domain.Dsd(_dataflow.Dsd.Base);
            dsd.SetDimensions(_dataflow.Dsd.Dimensions.Where((d,index)=> index < 2 || d.Base.TimeDimension));

            var i = 1;

            foreach (var dim in dsd.Dimensions)
                dim.DbId = i++;

            var sql = dsd.SqlBuildRowIdFormula(includeTimeDim);
            
            Assert.AreEqual(expectedSql, sql);
        }

    }
}
