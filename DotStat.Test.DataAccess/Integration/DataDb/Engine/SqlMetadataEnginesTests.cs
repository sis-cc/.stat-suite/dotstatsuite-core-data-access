﻿using System;
using System.Linq;
using System.Threading.Tasks;
using DotStat.Db;
using DotStat.Db.Engine.SqlServer;
using DotStat.Domain;
using NUnit.Framework;

namespace DotStat.Test.DataAccess.Integration.DataDb.Engine
{
    [NonParallelizable]
    [TestFixture("sdmx/CsvV2.xml")]
    [TestFixture("sdmx/CH1.SUS-DSI_SUS_CUBE_PERSONS-1.0.0-all.xml")]
    public class SqlMetadataEnginesTests : BaseDbIntegrationTests
    {
        private SqlMsdEngine _mdsEngine;
        private SqlMetadataDataflowEngine _metadataDataflowEngine;
        private Dataflow _dataflow;

        public SqlMetadataEnginesTests(string structure) : base(structure)
        {
            _mdsEngine = new SqlMsdEngine(Configuration);
            _metadataDataflowEngine = new SqlMetadataDataflowEngine(Configuration);
            _dataflow = this.GetDataflow();
        }

        [Test, Order(1)]
        public async Task No_Artifact_Record_Should_Exist()
        {
            var id = await _mdsEngine.GetDbId(_dataflow.Dsd.Msd.Code, _dataflow.Dsd.Msd.AgencyId, _dataflow.Dsd.Msd.Version, DotStatDb, CancellationToken);

            Assert.AreEqual(-1, id);

            Assert.ThrowsAsync<NotImplementedException>(async () => await _metadataDataflowEngine.GetDbId(_dataflow.Dsd.Msd.Code, _dataflow.Dsd.Msd.AgencyId, _dataflow.Dsd.Msd.Version, DotStatDb, CancellationToken));
        }

        [Test, Order(2)]
        public async Task Insert_Artifact()
        {
            var tableVersion = DbTableVersion.A;
            var targetVersion = TargetVersion.Live;

            var transactionId = UnitOfWork.TransactionRepository.GetNextTransactionId(CancellationToken).Result;
            var transaction = UnitOfWork.TransactionRepository.CreateTransactionItem(transactionId, _dataflow.Dsd.FullId, null, null, "dataSource", TransactionType.Import, targetVersion, serviceId: null, CancellationToken).Result;

            // create dataflow DB objects 
            var tryNewTransactionResult =
                DotStatDbService.TryNewTransaction(transaction, _dataflow, MsAccess, CancellationToken).Result;
            Assert.IsTrue(tryNewTransactionResult, "Creation of the transaction failed.");

            // close transaction
            _dataflow.Dsd.LiveVersion = (char?)tableVersion;
            var success = DotStatDbService.CloseTransaction(transaction, _dataflow, false, false, true, 
                MsAccess, false, false, null, CancellationToken).Result;
            Assert.IsTrue(success);

            Assert.IsTrue(_dataflow.Dsd.Msd.DbId > 0);

            var id = await _mdsEngine.GetDbId(_dataflow.Dsd.Msd.Code, _dataflow.Dsd.Msd.AgencyId, _dataflow.Dsd.Msd.Version, DotStatDb, CancellationToken);

            Assert.AreEqual(_dataflow.Dsd.Msd.DbId, id);
            Assert.AreEqual(_dataflow.Dsd.MsdDbId, id);

            Assert.ThrowsAsync<NotImplementedException>(async () => await _metadataDataflowEngine.InsertToArtefactTable(_dataflow, DotStatDb, CancellationToken));
        }

        [Test, Order(3)]
        public async Task Check_Msd_Meta_Tables_Created()
        {
            // Create tables ...
            //await _engine.CreateDynamicDbObjects(_dataflow.Dsd, DotStatDb, CancellationToken);

            var expectedTables = new[]
            {
                $"{_dataflow.Dsd.SqlMetadataDataStructureTable('A')}",
                $"{_dataflow.Dsd.SqlMetadataDataStructureTable('B')}",
                $"{_dataflow.Dsd.SqlMetadataDataSetTable('A')}",
                $"{_dataflow.Dsd.SqlMetadataDataSetTable('B')}",
                $"{_dataflow.Dsd.SqlDeletedMetadataTable('A')}",
                $"{_dataflow.Dsd.SqlDeletedMetadataTable('B')}",
            };

            foreach (var table in expectedTables)
                Assert.IsTrue(await DotStatDb.TableExists(table, CancellationToken), $"Table {table} have not been found");
            
            var expectedViews = new[]
            {
                $"{_dataflow.Dsd.SqlMetadataDsdViewName('A')}",
                $"{_dataflow.Dsd.SqlMetadataDsdViewName('B')}",
                $"{_dataflow.Dsd.SqlDeletedMetadataDsdViewName('A')}",
                $"{_dataflow.Dsd.SqlDeletedMetadataDsdViewName('B')}",
                $"{_dataflow.SqlMetadataDataflowViewName('A')}",
                $"{_dataflow.SqlMetadataDataflowViewName('B')}",
                $"{_dataflow.SqlDeletedMetadataDataflowViewName('A')}",
                $"{_dataflow.SqlDeletedMetadataDataflowViewName('B')}"
            };

            foreach (var view in expectedViews)
                Assert.IsTrue(await DotStatDb.ViewExists(view, CancellationToken), $"View {view} have not been found");
        }
        
        [Test, Order(4)]
        public async Task DropAllIndexes()
        {
            var dsd = _dataflow.Dsd;
            await _mdsEngine.DropAllIndexes(dsd, DotStatDb, CancellationToken);
            await _metadataDataflowEngine.DropAllIndexes(_dataflow, DotStatDb, CancellationToken);

            var expectedTables = new[]
            {
                $"{dsd.SqlMetadataDataStructureTable((char)DbTableVersion.A)}",
                $"{dsd.SqlMetadataDataStructureTable((char)DbTableVersion.B)}",
                $"{dsd.SqlDeletedMetadataTable((char)DbTableVersion.A)}",
                $"{dsd.SqlDeletedMetadataTable((char)DbTableVersion.B)}",
                $"{_dataflow.SqlMetadataDataflowTable((char)DbTableVersion.A)}",
                $"{_dataflow.SqlMetadataDataflowTable((char)DbTableVersion.B)}"
            };

            foreach (var tableName in expectedTables)
            {
                var indexCount = (int)await DotStatDb.ExecuteScalarSqlAsync($@"
SELECT count(*) FROM sys.indexes AS [i] 
WHERE [i].object_id = OBJECT_ID('[{DotStatDb.DataSchema}].[{tableName}]')
AND [type_desc] <>'HEAP';", CancellationToken);

                Assert.AreEqual(0, indexCount);
            }
        }

        [Test, Order(5)]
        public async Task AddUniqueConstraints()
        {
            var dsd = _dataflow.Dsd;
            await _mdsEngine.AddUniqueConstraints(dsd, clustered: true, DotStatDb, CancellationToken);
            await _metadataDataflowEngine.AddUniqueConstraints(_dataflow, clustered: true, DotStatDb, CancellationToken);
            
            var expectedTables = new[]
            {
                $"{dsd.SqlMetadataDataStructureTable((char)DbTableVersion.A)}",
                $"{dsd.SqlMetadataDataStructureTable((char)DbTableVersion.B)}",
                $"{_dataflow.SqlMetadataDataflowTable((char)DbTableVersion.A)}",
                $"{_dataflow.SqlMetadataDataflowTable((char)DbTableVersion.B)}"
            };
            
            foreach (var tableName in expectedTables)
            {
                var indexName = $"PK_{tableName}";
                var indexExists = await IndexExists(indexName, tableName);
                Assert.IsTrue(indexExists);
            }
        }

        [Test, Order(6)]
        [TestCase(DataCompressionEnum.COLUMNSTORE)]
        [TestCase(DataCompressionEnum.COLUMNSTORE_ARCHIVE)]
        public async Task Compress(DataCompressionEnum dataCompression)
        {
            var dsd = _dataflow.Dsd;
            await _mdsEngine.DropAllIndexes(dsd, DotStatDb, CancellationToken);
            await _mdsEngine.Compress(dsd, dataCompression, DotStatDb, CancellationToken);

            await _metadataDataflowEngine.DropAllIndexes(_dataflow, DotStatDb, CancellationToken);
            await _metadataDataflowEngine.Compress(_dataflow, dataCompression, DotStatDb, CancellationToken);

            var expectedTables = new[]
            {
                $"{dsd.SqlMetadataDataStructureTable((char)DbTableVersion.A)}",
                $"{dsd.SqlMetadataDataStructureTable((char)DbTableVersion.B)}",
                $"{dsd.SqlDeletedMetadataTable((char)DbTableVersion.A)}",
                $"{dsd.SqlDeletedMetadataTable((char)DbTableVersion.B)}",
                $"{_dataflow.SqlMetadataDataflowTable((char)DbTableVersion.A)}",
                $"{_dataflow.SqlMetadataDataflowTable((char)DbTableVersion.B)}"
            };

            foreach (var tableName in expectedTables)
            {
                var indexName = $"CCI_{tableName}";
                var indexExists = await IndexExists(indexName, tableName, dataCompression);

                Assert.IsTrue(indexExists);
            }
        }

        [Test, Order(7)]
        public async Task Cleanup()
        {
            var allDbComponents = UnitOfWork.ComponentRepository.GetAllComponents(CancellationToken).Result.ToList();
            var allMsdDbComponents = UnitOfWork.MetaMetadataComponentRepository.GetAllMetadataAttributeComponents(CancellationToken).Result.ToList();

            await DotStatDbService.CleanUpDsd(_dataflow.Dsd.DbId, false, this.MsAccess, allDbComponents, allMsdDbComponents, this.CancellationToken);
        }
    }
}
