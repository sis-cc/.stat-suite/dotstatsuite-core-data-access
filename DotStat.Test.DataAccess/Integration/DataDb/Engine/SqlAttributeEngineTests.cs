﻿using System.Threading.Tasks;
using DotStat.Db;
using DotStat.Db.Engine.SqlServer;
using DotStat.Domain;
using NUnit.Framework;

namespace DotStat.Test.DataAccess.Integration.DataDb.Engine
{
    [TestFixture, Parallelizable(ParallelScope.Fixtures)]
    public class SqlAttributeEngineTests : BaseDbIntegrationTests
    {
        private SqlAttributeEngine _engine;
        private Domain.Dataflow _dataflow;

        public SqlAttributeEngineTests() : base("sdmx/264D_264_SALDI2_ATTRIBUTE_TEST.xml")
        {
            _engine = new SqlAttributeEngine(Configuration);
            _dataflow = this.GetDataflow();

            InitDataForTests();
        }

        private void InitDataForTests()
        {
            _dataflow.Dsd.DbId = 5;

            var i = 100;

            foreach (var dim in _dataflow.Dsd.Dimensions)
                dim.DbId = i++;

            foreach (var attr in _dataflow.Dsd.Attributes)
                attr.DbId = i++;

            _dataflow.Dsd.PrimaryMeasure.DbId = i;
        }

        [Test, Order(1)]
        public async Task No_Artifact_Record_Should_Exist()
        {
            foreach(var attr in _dataflow.Dsd.Attributes)
                Assert.AreEqual(-1, await _engine.GetDbId(attr.Code, _dataflow.Dsd.DbId, DotStatDb, CancellationToken));
        }

        [Test, Order(2)]
        public async Task Insert_Artifact()
        {
            foreach (var attr in _dataflow.Dsd.Attributes)
                Assert.IsTrue((attr.DbId = await _engine.InsertToComponentTable(attr, DotStatDb, CancellationToken)) > 0);
        }

        [Test, Order(3)]
        public async Task Check_Tables_Created()
        {
            var dsd = _dataflow.Dsd;

            // Create Attribute tables ...

            await _engine.CreateDynamicDbObjects(dsd, DotStatDb, CancellationToken);

            var expectedTables = new[]
            {
                dsd.SqlDsdAttrTable('A'),
                dsd.SqlDsdAttrTable('B'),
                dsd.SqlDimGroupAttrTable('A'),
                dsd.SqlDimGroupAttrTable('B')
            };

            foreach (var table in expectedTables)
                Assert.IsTrue(await DotStatDb.TableExists(table, CancellationToken), $"Table {table} have not been found");
        }

        [Test, Order(4)]
        public async Task DropAllIndexes()
        {
            var dsd = _dataflow.Dsd;
            await _engine.DropAllIndexes(dsd, DotStatDb, CancellationToken);

            var expectedTables = new[]
            {
                $"{dsd.SqlDimGroupAttrTable((char)DbTableVersion.A)}",
                $"{dsd.SqlDimGroupAttrTable((char)DbTableVersion.B)}"
            };

            foreach (var tableName in expectedTables)
            {
                var indexCount = (int)await DotStatDb.ExecuteScalarSqlAsync($@"
SELECT count(*) FROM sys.indexes AS [i] 
WHERE [i].object_id = OBJECT_ID('[{DotStatDb.DataSchema}].[{tableName}]')
AND [type_desc] <>'HEAP';", CancellationToken);

                Assert.AreEqual(0, indexCount);
            }
        }

        [Test, Order(5)]
        public async Task AddUniqueConstraints()
        {
            var dsd = _dataflow.Dsd;
            await _engine.AddUniqueConstraints(dsd, clustered: true, DotStatDb, CancellationToken);

            var expectedTables = new[]
            {
                $"{dsd.SqlDimGroupAttrTable((char)DbTableVersion.A)}",
                $"{dsd.SqlDimGroupAttrTable((char)DbTableVersion.B)}"
            };

            foreach (var tableName in expectedTables)
            {
                var indexName = $"PK_{tableName}";
                var indexExists = await IndexExists(indexName, tableName);
                Assert.IsTrue(indexExists);
            }
        }

        [Test, Order(6)]
        [TestCase(DataCompressionEnum.COLUMNSTORE)]
        [TestCase(DataCompressionEnum.COLUMNSTORE_ARCHIVE)]
        public async Task Compress(DataCompressionEnum dataCompression)
        {
            var dsd = _dataflow.Dsd;
            await _engine.DropAllIndexes(dsd, DotStatDb, CancellationToken);
            await _engine.Compress(dsd, dataCompression, DotStatDb, CancellationToken);

            var expectedTables = new[]
            {
                $"{dsd.SqlDimGroupAttrTable((char)DbTableVersion.A)}",
                $"{dsd.SqlDimGroupAttrTable((char)DbTableVersion.B)}"
            };

            foreach (var tableName in expectedTables)
            {
                var indexName = $"CCI_{tableName}";
                var indexExists = await IndexExists(indexName, tableName, dataCompression);

                Assert.IsTrue(indexExists);
            }
        }

        [Test, Order(7)]
        public async Task CleanUp()
        {
            foreach (var attr in _dataflow.Dsd.Attributes)
            {
                Assert.IsTrue(await _engine.GetDbId(attr.Code, _dataflow.Dsd.DbId, DotStatDb, CancellationToken) > 0);

                await _engine.CleanUp(attr, DotStatDb, CancellationToken);

                Assert.AreEqual(-1, await _engine.GetDbId(attr.Code, _dataflow.Dsd.DbId, DotStatDb, CancellationToken));
            }
        }
        
        [Test, Order(8)]
        public async Task Check_Temporal_Tables_Created()
        {
            var dsd = _dataflow.Dsd;
            dsd.KeepHistory = true;
            dsd.DbId++;

            // Create Attribute tables ...

            await _engine.CreateDynamicDbObjects(dsd, DotStatDb, CancellationToken);

            var expectedTables = new[]
            {
                dsd.SqlDsdAttrTable('A'),
                dsd.SqlDsdAttrTable('B'),
                dsd.SqlDsdAttrHistoryTable('A'),
                dsd.SqlDsdAttrHistoryTable('B'),
                dsd.SqlDimGroupAttrTable('A'),
                dsd.SqlDimGroupAttrTable('B'),
                dsd.SqlDimGroupAttrHistoryTable('A'),
                dsd.SqlDimGroupAttrHistoryTable('B'),
            };

            foreach (var table in expectedTables)
                Assert.IsTrue(await DotStatDb.TableExists(table, CancellationToken), $"Table {table} have not been found");
        }
    }
}
