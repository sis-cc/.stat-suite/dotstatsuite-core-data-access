﻿using System;
using System.Linq;
using System.Threading.Tasks;
using DotStat.Common.Configuration.Dto;
using DotStat.Db.DB;
using DotStat.DB.Repository;
using DotStat.Db.Repository.SqlServer;
using DotStat.Db.Service;
using DotStat.Domain;

namespace DotStat.Test.DataAccess.Integration
{
    public abstract class BaseDbIntegrationTests : SdmxUnitTestBase
    {
        public SqlDotStatDb DotStatDb;
        public IDotStatDbService DotStatDbService;
        public IUnitOfWork  UnitOfWork;
        private readonly DataspaceInternal _dataSpace;

        protected BaseDbIntegrationTests(string dfPath = null) : base(dfPath)
        {
            _dataSpace = Configuration.SpacesInternal.FirstOrDefault();

            InitObjects();
        }

        public void SetArchive(bool archive)
        {
            _dataSpace.Archive = archive;
        }

        public void SetKeepHistory(bool keepHistory)
        {
            _dataSpace.KeepHistory = keepHistory;
        }

        private void InitObjects()
        {
            IDotStatDb dotStatDb = new SqlDotStatDb(
                _dataSpace,
                DbUp.DatabaseVersion.DataDbVersion
                );

            DotStatDb = dotStatDb as SqlDotStatDb;

            UnitOfWork = new UnitOfWork(
                dotStatDb,
                new SqlArtefactRepository(DotStatDb, Configuration),
                new SqlAttributeRepository(DotStatDb, Configuration),
                new SqlDataStoreRepository(DotStatDb, Configuration),
                new SqlMetadataStoreRepository(DotStatDb, Configuration),
                new SqlMetadataComponentRepository(DotStatDb, Configuration),
                new SqlObservationRepository(DotStatDb, Configuration),
                new SqlTransactionRepository(DotStatDb, Configuration),
                new SqlComponentRepository(DotStatDb, Configuration),
                new SqlCodelistRepository(DotStatDb, Configuration)
            );

            DotStatDbService = new DotStatDbService(UnitOfWork, Configuration);
        }

        protected async ValueTask<bool> CleanUpStructure(IImportReferenceableStructure referencedStructure)
        {
            var dsd = referencedStructure.Dsd;

            var dsdArtefact = await UnitOfWork.ArtefactRepository.GetArtefactByDbId(dsd.DbId, CancellationToken);

            if (dsdArtefact == null)
                return false;

            var dataflowArtefacts = await UnitOfWork.ArtefactRepository.GetListOfDataflowArtefactsForDsd(dsd.DbId, CancellationToken);
            var allDsdComponents = await UnitOfWork.ComponentRepository.GetComponentsOfDsd(dsd, CancellationToken);

            if (dsd.Msd != null)
            {
                var msdComponents = await UnitOfWork.MetaMetadataComponentRepository.GetMetadataAttributesOfMsd(
                    dsd.Msd,
                    CancellationToken
                );

                await UnitOfWork.ArtefactRepository.CleanUpMsdOfDsd(
                    dsd.DbId,
                    dataflowArtefacts,
                    allDsdComponents,
                    msdComponents,
                    CancellationToken
                );
            }

            return await UnitOfWork.ArtefactRepository.CleanUpDsd(
                dsd.DbId, 
                dataflowArtefacts, 
                allDsdComponents, 
                CancellationToken
            );
        }

        protected async Task<bool> IndexExists(string indexName, string tableName, DataCompressionEnum dataCompression = DataCompressionEnum.NONE)
        {
            var dataCompressionQuery = "";
            if (dataCompression is not DataCompressionEnum.NONE)
            {
                dataCompressionQuery = $"AND [p].[data_compression_desc] = '{dataCompression}'";
            }
            
            return (int)await DotStatDb.ExecuteScalarSqlAsync($@"
IF EXISTS(SELECT TOP 1 1
    FROM [sys].[partitions] AS [p]
    INNER JOIN sys.indexes AS [i] ON [i].[object_id] = [p].[object_id] AND [i].[index_id] = [p].[index_id]
    WHERE [i].name = '{indexName}' 
    AND [i].object_id = OBJECT_ID('[{DotStatDb.DataSchema}].[{tableName}]') 
    {dataCompressionQuery}
)
	SELECT 1;
ELSE 
	SELECT 0;", CancellationToken) > 0;
        }
    }
}
