﻿using System.Collections.Generic;
using System.Linq;
using DotStat.Common.Configuration.Dto;
using DotStat.Common.Exceptions;
using DotStat.Common.Localization;
using DotStat.Domain;
using DotStat.MappingStore;
using Estat.Sri.Mapping.Api.Constant;
using Estat.Sri.Mapping.Api.Engine;
using Estat.Sri.Mapping.Api.Manager;
using Estat.Sri.Mapping.Api.Model;
using Estat.Sri.MappingStoreRetrieval.Config;
using Estat.Sri.Plugin.SqlServer.Engine;
using Moq;
using NUnit.Framework;

namespace DotStat.Test.DataAccess.Unit
{
    [TestFixture, Parallelizable(ParallelScope.Self)]
    public class MappingStoreSdmxObjectRetrievalTests : SdmxUnitTestBase
    {
        private DataspaceInternal _dataSpace;
        private Dataflow _dataFlow;
        private readonly Mock<IEntityPersistenceManager> _persistManager = new Mock<IEntityPersistenceManager>();
        private readonly Mock<IEntityRetrieverManager> _retrieverManager = new Mock<IEntityRetrieverManager>();
        private readonly Mock<IDatabaseProviderManager> _databaseProviderManager = new Mock<IDatabaseProviderManager>();
        private readonly IMappingStoreDataAccess _mappingStoreDataAccess;
        private readonly string _storeId;

        private const string _mappingSetPrefix = "AUTO_MAP_SET_";
        private const string _structDbPrefix = "DotStatSuiteCoreStructDb";
        private const string _dataDbPrefix = "DotStatSuiteCoreDataDb";

        public MappingStoreSdmxObjectRetrievalTests()
        {
            _dataSpace = Configuration.SpacesInternal.FirstOrDefault();
            _dataFlow = base.GetDataflow();
            _mappingStoreDataAccess = new MappingStoreDataAccess(Configuration, _persistManager.Object, _retrieverManager.Object, _databaseProviderManager.Object, null);

            _storeId = $"{_structDbPrefix}_{_dataSpace.Id}";
            
            //Entities
            _retrieverManager.Setup(x =>
                    x.GetEntities<IConnectionEntity>(It.IsAny<string>(), It.IsAny<EntityQuery>(), It.IsAny<Detail>()))
                .Returns(new List<ConnectionEntity> { new ConnectionEntity { Name = $"{_dataDbPrefix}_{_dataSpace.Id}", StoreId = _storeId } });
            
            var datasetEntityEngine = new Mock<IEntityRetrieverEngine<DatasetEntity>>();

            datasetEntityEngine
                .Setup(x=>x.GetEntities(It.IsAny<EntityQuery>(), It.IsAny<Detail>()))
                .Returns(new List<DatasetEntity>());

            _retrieverManager.Setup(x => x.GetRetrieverEngine<DatasetEntity>(It.IsAny<string>()))
                .Returns(datasetEntityEngine.Object);

            //Engines
            var mappingSetEngine = new Mock<IEntityPersistenceEngine<MappingSetEntity>>();
            mappingSetEngine.Setup(x =>
                x.Delete($"{_mappingSetPrefix}{_dataFlow.FullId}", EntityType.MappingSet)
            );
            mappingSetEngine.Setup(x =>
                x.Update(It.IsAny<PatchRequest>(), It.IsAny<EntityType>(),It.IsAny<string>())
            );
            _persistManager.Setup(x => x.GetEngine<MappingSetEntity>(_storeId))
                .Returns(mappingSetEngine.Object);

            var dataSetEngine = new Mock<IEntityPersistenceEngine<DatasetEntity>>();
            dataSetEngine.Setup(x =>
                x.Delete($"{_mappingSetPrefix}{_dataFlow.FullId}", EntityType.DataSet)
            );
            _persistManager.Setup(x => x.GetEngine<DatasetEntity>(_storeId))
                .Returns(dataSetEngine.Object);


            var datasetPropertyEngine = new Mock<IEntityPersistenceEngine<DatasetPropertyEntity>>();
            datasetPropertyEngine.Setup(x =>
                x.Delete(It.IsAny<string>(), EntityType.DataSetProperty)
            );
            mappingSetEngine.Setup(x =>
                x.Update(It.IsAny<PatchRequest>(), It.IsAny<EntityType>(), It.IsAny<string>())
            );
            _persistManager.Setup(x => x.GetEngine<DatasetPropertyEntity>(_storeId))
                .Returns(datasetPropertyEngine.Object);

            _databaseProviderManager.Setup(x => x.GetEngineByProvider(It.IsAny<string>()))
                .Returns(new SqlServerDatabaseProviderEngine());
        }

        [Test]
        public void HasMappingSet()
        {
            //With mapping sets
            var entities = new List<MappingSetEntity>
            {
                new MappingSetEntity
                {
                    DataSetId = "1", Name=$"{_mappingSetPrefix}{_dataFlow.FullId}", Description = MappingStoreDataAccess.GetDescription<MappingSetEntity>(false), ParentId = _dataFlow.Base.Urn.ToString()
                }
            };

            _retrieverManager.Setup(x =>
                    x.GetEntities<MappingSetEntity>(It.IsAny<string>(), It.IsAny<EntityQuery>(), Detail.Full))
                .Returns(entities);

            var (hasMappingSets,hasUserMappingSets) = _mappingStoreDataAccess.HasMappingSet(_dataSpace.Id, _dataFlow);
            
            Assert.AreEqual(true, hasMappingSets);
            Assert.AreEqual(false, hasUserMappingSets);
        }
        
        [Test]
        public void HasUserCreatedMappingSets()
        {
            //User created
            var entities = new List<MappingSetEntity>
            {
                new MappingSetEntity
                {
                    DataSetId = "1", Name=$"{_mappingSetPrefix}{_dataFlow.FullId}", Description = MappingStoreDataAccess.GetDescription<MappingSetEntity>(false), ParentId = _dataFlow.Base.Urn.ToString()
                },
                new MappingSetEntity
                {
                    DataSetId = "2", Name="User created", ParentId = _dataFlow.Base.Urn.ToString()
                }
            };

            _retrieverManager.Setup(x =>
                    x.GetEntities<MappingSetEntity>(It.IsAny<string>(), It.IsAny<EntityQuery>(), Detail.Full))
                .Returns(entities);

            var (_, hasUserMappingSets) = _mappingStoreDataAccess.HasMappingSet(_dataSpace.Id, _dataFlow);

            Assert.AreEqual(true, hasUserMappingSets);

            //Non-user created
            entities = new List<MappingSetEntity>
            {
                new MappingSetEntity
                {
                    DataSetId = "1", Name=$"{_mappingSetPrefix}{_dataFlow.FullId}", Description = MappingStoreDataAccess.GetDescription<MappingSetEntity>(false), ParentId = _dataFlow.Base.Urn.ToString()
                }
            };

            _retrieverManager.Setup(x =>
                    x.GetEntities<MappingSetEntity>(It.IsAny<string>(), It.IsAny<EntityQuery>(), Detail.Full))
                .Returns(entities);

            (_, hasUserMappingSets) = _mappingStoreDataAccess.HasMappingSet(_dataSpace.Id, _dataFlow);

            Assert.AreEqual(false, hasUserMappingSets);
        }

        [Test]
        public void DeleteDataSetsAndMappingSets()
        {
            //With mapping sets
            var mappingSetEntities = new List<MappingSetEntity>
            {
                new MappingSetEntity
                {
                    EntityId = $"{_mappingSetPrefix}{_dataFlow.FullId}" ,DataSetId = "1", Name=$"{_mappingSetPrefix}{_dataFlow.FullId}", Description = MappingStoreDataAccess.GetDescription<MappingSetEntity>(false), ParentId = _dataFlow.Base.Urn.ToString()
                },
                new MappingSetEntity
                {
                    DataSetId = "2", Name=$"{_mappingSetPrefix}{_dataFlow.FullId}", Description = "User created", ParentId = _dataFlow.Base.Urn.ToString()
                }
            };
            _retrieverManager.Setup(x =>
                    x.GetEntities<MappingSetEntity>(It.IsAny<string>(), It.IsAny<EntityQuery>(), It.IsAny<Detail>()))
                .Returns(mappingSetEntities);

            var deletionSucceeded = _mappingStoreDataAccess.DeleteDataSetsAndMappingSets(_dataSpace.Id, _dataFlow, false);
            Assert.AreEqual(true, deletionSucceeded);

            //No mapping sets
            _retrieverManager.Setup(x =>
                    x.GetEntities<MappingSetEntity>(It.IsAny<string>(), It.IsAny<EntityQuery>(), It.IsAny<Detail>()))
                .Returns(new List<MappingSetEntity>());

            deletionSucceeded = _mappingStoreDataAccess.DeleteDataSetsAndMappingSets(_dataSpace.Id, _dataFlow, false);
            Assert.AreEqual(false, deletionSucceeded);
        }

        [Test]
        public void CreateOrUpdateMappingSet()
        {
            //Entities
            var dataSetProperty = new DatasetPropertyEntity()
            {
                EntityId = "1",
                StoreId = $"{_structDbPrefix}_{_dataSpace.Id}"
            };

            var dataSet = new DatasetEntity
            {
                EntityId = "1",
                Name = $"{_dataFlow.FullId}_A",
                Description = MappingStoreDataAccess.GetDescription<DatasetEntity>(false),
                //ParentId = connection.EntityId,
                StoreId = $"{_structDbPrefix}_{_dataSpace.Id}",
                Query = ""
            };

            var mappingSetEntities = new List<MappingSetEntity>
            {
                new MappingSetEntity
                {
                    StoreId =_storeId, EntityId = $"{_mappingSetPrefix}{_dataFlow.FullId}" ,DataSetId = "1", Name=$"{_mappingSetPrefix}{dataSet.Name}", Description = MappingStoreDataAccess.GetDescription<MappingSetEntity>(false), ParentId = _dataFlow.Base.Urn.ToString()
                },
                new MappingSetEntity
                {
                    StoreId =_storeId, DataSetId = "2", Name=$"{_dataFlow.FullId}", Description = "User created", ParentId = _dataFlow.Base.Urn.ToString()
                }
            };

            //Retrievers not initialized
            var createdOrUpdated = _mappingStoreDataAccess.CreateOrUpdateMappingSet(_dataSpace.Id, _dataFlow, 'A', null, null, new MappingSetParams());
            Assert.AreEqual(false, createdOrUpdated);

            //Initialize retrievers  
            _retrieverManager.Setup(x =>
                    x.GetEntities<DatasetEntity>(It.IsAny<string>(), It.IsAny<EntityQuery>(), It.IsAny<Detail>()))
                .Returns(new List<DatasetEntity> { dataSet });
            _retrieverManager.Setup(x =>
                    x.GetEntities<MappingSetEntity>(It.IsAny<string>(), It.IsAny<EntityQuery>(), It.IsAny<Detail>()))
                .Returns(new List<MappingSetEntity>());
            _retrieverManager.Setup(x =>
                    x.GetEntities<DatasetPropertyEntity>(It.IsAny<string>(), It.IsAny<EntityQuery>(), It.IsAny<Detail>()))
                .Returns(new List<DatasetPropertyEntity> { dataSetProperty });

            _persistManager.Setup(x => x.Add(It.IsAny<DatasetEntity>()))
                .Returns(dataSet);
            _persistManager.Setup(x => x.Add(It.IsAny<MappingSetEntity>()))
                .Returns((MappingSetEntity e) =>e);
            _persistManager.Setup(x => x.Add(It.IsAny<DatasetPropertyEntity>()))
                .Returns((DatasetPropertyEntity e)=> e);
            _persistManager.Setup(x => x.Add(It.IsAny<DataSetColumnEntity>()))
                .Returns((DataSetColumnEntity e) => e);


            createdOrUpdated = _mappingStoreDataAccess.CreateOrUpdateMappingSet(_dataSpace.Id, _dataFlow, 'A', null, null, new MappingSetParams());
            Assert.AreEqual(true, createdOrUpdated);


            _retrieverManager.Setup(x =>
                    x.GetEntities<MappingSetEntity>(It.IsAny<string>(), It.IsAny<EntityQuery>(), It.IsAny<Detail>()))
                .Returns(mappingSetEntities);

            //Update
            createdOrUpdated = _mappingStoreDataAccess.CreateOrUpdateMappingSet(_dataSpace.Id, _dataFlow, 'A', null, null, new MappingSetParams());
            Assert.AreEqual(true, createdOrUpdated);
            
            //Wrong dataSpace
            var dataSpace = "wrong";
            var ex = Assert.Throws<DotStatException>(() =>
                _mappingStoreDataAccess.CreateOrUpdateMappingSet(dataSpace, _dataFlow, 'A', null, null, new MappingSetParams()));
            var msg = string.Format(LocalizationRepository.GetLocalisedResource(Localization.ResourceId.DataSpaceNotFound), dataSpace);

            Assert.AreEqual(msg, ex.Message);
        }

        [Test]
        public void TestTimeOutSet()
        {
            var config = ConfigManager.Config;

            Assert.IsNotNull(config);

            var dbSettings = config.GeneralDatabaseSettings;

            Assert.IsNotNull(dbSettings);

            var sqlSettings = dbSettings[MappingStoreDefaultConstants.SqlServerProvider];

            Assert.AreEqual(_dataSpace.DatabaseCommandTimeoutInSec, sqlSettings.CommandTimeout);
        }
    }
}
