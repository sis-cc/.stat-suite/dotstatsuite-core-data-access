﻿using DotStat.Db.Util;
using DotStat.Domain;
using DotStat.Test.Moq;
using NUnit.Framework;
using Org.Sdmxsource.Sdmx.Api.Exception;
using System.Threading.Tasks;

namespace DotStat.Test.DataAccess.Unit
{
    [TestFixture, Parallelizable(ParallelScope.All)]
    public sealed class ConstraintTests : SdmxUnitTestBase
    {
        private readonly Dataflow _dataflow;
        private ICodeTranslator _codeTranslator;

        /// <summary>
        /// Tests Predicate construction for Dataflow with Constraints
        /// </summary>
        public ConstraintTests() : base("sdmx/ECB_AME_(with constraints).xml")
        {
            _dataflow = GetDataflow();

            var codeListRepository = new TestCodeListRepository(_dataflow);
            _codeTranslator = new CodeTranslator(codeListRepository);
        }

        [OneTimeSetUp]
        public async Task Init()
        {
            await _codeTranslator.FillDict(_dataflow, CancellationToken);
        }

        [TestCase("data/ECB,AME,4.0/all", "([FREQ]='Q') AND ([AME_REF_AREA]='DNK' OR [AME_REF_AREA]='IRL' OR [AME_REF_AREA]='NLD' OR [AME_REF_AREA]='SVK') AND ([AME_TRANSFORMATION]='3') AND ([AME_AGG_METHOD]='4') AND ([AME_UNIT]='0' OR [AME_UNIT]='319') AND ([AME_REFERENCE]='0') AND ([AME_ITEM]='UDGGL' OR [AME_ITEM]='OVGD' OR [AME_ITEM]='UBLGE' OR [AME_ITEM]='ZUTN')")]
        [TestCase("data/ECB,AME,4.0/....../all", "([FREQ]='Q') AND ([AME_REF_AREA]='DNK' OR [AME_REF_AREA]='IRL' OR [AME_REF_AREA]='NLD' OR [AME_REF_AREA]='SVK') AND ([AME_TRANSFORMATION]='3') AND ([AME_AGG_METHOD]='4') AND ([AME_UNIT]='0' OR [AME_UNIT]='319') AND ([AME_REFERENCE]='0') AND ([AME_ITEM]='UDGGL' OR [AME_ITEM]='OVGD' OR [AME_ITEM]='UBLGE' OR [AME_ITEM]='ZUTN')")]
        [TestCase("data/ECB,AME,4.0/....../all", "([DIM_101]=8) AND ([DIM_102]=34 OR [DIM_102]=82 OR [DIM_102]=94 OR [DIM_102]=101) AND ([DIM_103]=2) AND ([DIM_104]=5) AND ([DIM_105]=1 OR [DIM_105]=6) AND ([DIM_106]=1) AND ([DIM_107]=396 OR [DIM_107]=222 OR [DIM_107]=349 OR [DIM_107]=878)", false)]
        [TestCase("data/ECB,AME,4.0/.IRL...../all", "([AME_REF_AREA]='IRL') AND ([FREQ]='Q') AND ([AME_TRANSFORMATION]='3') AND ([AME_AGG_METHOD]='4') AND ([AME_UNIT]='0' OR [AME_UNIT]='319') AND ([AME_REFERENCE]='0') AND ([AME_ITEM]='UDGGL' OR [AME_ITEM]='OVGD' OR [AME_ITEM]='UBLGE' OR [AME_ITEM]='ZUTN')")]
        [TestCase("data/ECB,AME,4.0/Q.IRL.3.4.319.0.ZUTN/all", "([FREQ]='Q') AND ([AME_REF_AREA]='IRL') AND ([AME_TRANSFORMATION]='3') AND ([AME_AGG_METHOD]='4') AND ([AME_UNIT]='319') AND ([AME_REFERENCE]='0') AND ([AME_ITEM]='ZUTN')")]
        [TestCase("data/ECB,AME,4.0/Q.IRL.3.4.319.0.ZUTN/all", "([DIM_101]=8) AND ([DIM_102]=82) AND ([DIM_103]=2) AND ([DIM_104]=5) AND ([DIM_105]=6) AND ([DIM_106]=1) AND ([DIM_107]=878)", false)]
        [TestCase("data/ECB,AME,4.0/A+Q.IRL+XXX...../all", "([FREQ]='Q') AND ([AME_REF_AREA]='IRL') AND ([AME_TRANSFORMATION]='3') AND ([AME_AGG_METHOD]='4') AND ([AME_UNIT]='0' OR [AME_UNIT]='319') AND ([AME_REFERENCE]='0') AND ([AME_ITEM]='UDGGL' OR [AME_ITEM]='OVGD' OR [AME_ITEM]='UBLGE' OR [AME_ITEM]='ZUTN')")]
        public void ValidQueries(string restQuery, string expectedSql, bool useExternalValues = true)
        {
            var query = GetQuery(restQuery);
            var sql = Predicate.BuildWhereForObservationLevel( query, _dataflow, _codeTranslator, useExternalValues: useExternalValues, useExternalColumnNames: useExternalValues);

            Assert.AreEqual(expectedSql, sql);
        }

        [Test]
        public void NonExistingCode()
        {
            var query = GetQuery("data/ECB,AME,4.0/XXX....../all");

            Assert.Throws<SdmxNoResultsException>(()=>
                Predicate.BuildWhereForObservationLevel(query, _dataflow, _codeTranslator)
            );
        }

        [Test]
        public void ConstrainedCode()
        {
            var query = GetQuery("data/ECB,AME,4.0/A.EST...../all");

            Assert.Throws<SdmxNoResultsException>(
                () => Predicate.BuildWhereForObservationLevel(query, _dataflow, _codeTranslator)
            );
        }

    }
}
