﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DotStat.Common.Configuration;
using DotStat.Db.Reader;
using DotStat.Db.Util;
using DotStat.Db.Validation;
using DotStat.Domain;
using DotStat.Test.Moq;
using NUnit.Framework;
using Org.Sdmxsource.Sdmx.Api.Model.Data;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Data;

namespace DotStat.Test.DataAccess.Unit
{
    [TestFixture, Parallelizable(ParallelScope.All)]
    public sealed class SdmxObservationReaderTests : SdmxUnitTestBase
    {
        private readonly TestMappingStoreDataAccess _mappingStoreDataAccess;
        private readonly CodeTranslator _codeTranslator;
        private readonly Dataflow _dataflow;
        private readonly ReportedComponents _reportedComponents;

        public SdmxObservationReaderTests()
        {
            _mappingStoreDataAccess = new TestMappingStoreDataAccess("sdmx/ECB_AME_(with constraints).xml");
            _dataflow = _mappingStoreDataAccess.GetDataflow();
            var codeListRepository = new TestCodeListRepository(_dataflow);
            _codeTranslator = new CodeTranslator(codeListRepository);
            _reportedComponents = GetReportedComponents(_dataflow);
        }

        [OneTimeSetUp]
        public async Task Init()
        {
            await _codeTranslator.FillDict(_dataflow, CancellationToken);
        }

        [Test]
        public async Task Read_Valid_Observations()
        {
            var cnt = 0;
            var observations = GetValidObservations().ToAsyncEnumerable();
            var observationsList = await observations.ToListAsync(CancellationToken);

            var reader = new SdmxObservationReader(observations, _reportedComponents, _dataflow, _codeTranslator, Configuration, true, false, false);

            while (reader.Read())
                cnt++;

            Assert.AreEqual(observationsList.Count, cnt);
            Assert.IsTrue(!reader.GetErrors().Any());
        }

        [Test]
        public void Ensure_Order()
        {
            // TIME,FREQ,AME_REF_AREA,AME_TRANSFORMATION,AME_AGG_METHOD,AME_UNIT,AME_REFERENCE,AME_ITEM,VALUE
            // TIME_FORMAT,OBS_STATUS,OBS_CONF,OBS_PRE_BREAK,PUBL_ECB,PUBL_MU,PUBL_PUBLIC,COMPILATION,EXT_TITLE,EXT_UNIT,TITLE_COMPL
            // STATUS
            var expected = new[]
            {
                new object[] { new DateTime(2010, 1, 1), new DateTime(2010, 12, 31, 23, 59, 59), "2010",8,34,2,5,6,1,396,7111f,"text",1,4,"text",null,null,"PUBL_ECB - text","PUBL_MU - text","PUBL_PUBLIC - text","COMPILATION - text","EXT_TITLE - text","EXT_UNIT - text","TITLE_COMPL - text", 1, 1, 1, 1},
                new object[] { new DateTime(2011, 1, 1), new DateTime(2011, 12, 31, 23, 59, 59), "2011", 8,34,2,5,6,1,396,7222f, "text", 2, null, null, null, null, null, null, null, null, null, null, null, 1, 1, 1, 1 },
                new object[] { new DateTime(2012, 1, 1), new DateTime(2012, 12, 31, 23, 59, 59), "2012", 8,34,2,5,6,1,396,7333f, "text", 3, null, null, null, null, null, null, null, null, null, null, null, 1, 1, 1, 1 },
                new object[] { new DateTime(2015, 1, 1), new DateTime(2015, 12, 31, 23, 59, 59), "2015", 8,34,2,5,6,1,396,7444f,"text",1,4,"text",null,null,"PUBL_ECB - text","PUBL_MU - text","PUBL_PUBLIC - text","COMPILATION - text","EXT_TITLE - text","EXT_UNIT - text","TITLE_COMPL - text", 1, 1, 1, 1 },
                new object[] { new DateTime(2016, 1, 1), new DateTime(2016, 12, 31, 23, 59, 59), "2016", 8,34,2,5,6,1,396,7555f, "text", 2, null, null, null, null, null, null, null, null, null, null, null, 1, 1, 1, 1 },
                new object[] { new DateTime(2017, 1, 1), new DateTime(2017, 12, 31, 23, 59, 59), "2017", 8,34,2,5,6,1,396,7666f, "text", 3, null, null, null, null, null, null, null, null, null, null, null, 1, 1, 1, 1 }
            };

            var observations = GetValidObservations().ToAsyncEnumerable();

            var reportedComponents = GetReportedComponents(_dataflow);

            var reader = new SdmxObservationReader(observations, reportedComponents, _dataflow, _codeTranslator, Configuration, true, false, false);
            var row = 0;

            while (reader.Read())
            {
                for (var i = 0; i < reader.FieldCount; i++)
                    Assert.AreEqual(expected[row][i], reader.GetValue(i), $"Error on row: {row + 1}, column: {i + 1}");

                row++;
            }
        }

        [TestCase(100, 10, 2, 5, 5)]  // error in the 12th observation
        [TestCase(100, 40, 10, 5, 5)]  // error in the 50th observation
        [TestCase(100, 98, 2, 5, 1)] // error in the last observation
        [TestCase(100, 0, 1, 5, 5)]  // error in the first observation
        [TestCase(100, 0, 0, 5, 0)]  // no error
        [TestCase(100, 800, 10, 5, 0)]  // no error, error offset bigger then total observations 
        [TestCase(100, 10, 100, 5, 0)]  // no error, error repeat bigger then total observations
        public void Ensure_Reader_Stops_On_Error(
            int observationCount,
            int errorOffset,
            int errorRepeat,
            int maxTransferErrorAmount,
            int expectedErrorCount
        )
        {
            var testConfig = new BaseConfiguration()
            {
                MaxTransferErrorAmount = maxTransferErrorAmount,
            };

            var observations = ObservationGenerator.Generate(_dataflow.Dsd, _dataflow, true, 1980, 2019, observationCount, new GenerationSettings()
            {
                InvalidObservationOffset = errorOffset,
                InvalidObservationRepeat = errorRepeat,
                MixDimensionOrder = true
            }, action: StagingRowActionEnum.Merge);

            var readCount = 0;
            var reader = new SdmxObservationReader(observations, _reportedComponents, _dataflow, _codeTranslator, testConfig, true, false, false);

            while (reader.Read())
                readCount++;

            Console.WriteLine($"Reads: {readCount}, Processed observations: {reader.CurrentIndex}, Errors: {reader.GetErrors().Count}");

            // Observed errors count
            Assert.AreEqual(expectedErrorCount, reader.GetErrors().Count);

            // Processed observations until max Error count met
            Assert.AreEqual(errorRepeat > 0 ? Math.Min(observationCount, errorOffset + errorRepeat * Math.Max(1, expectedErrorCount)) : observationCount, reader.CurrentIndex);

            // Successful reads before first error
            Assert.AreEqual(errorRepeat > 0 ? Math.Min(observationCount, errorOffset + errorRepeat - 1) : observationCount, readCount);
        }

        [Test]
        public async Task Read_Invalid_Observations()
        {
            var configuration = new BaseConfiguration()
            {
                MaxTransferErrorAmount = 2,
            };

            var cnt = 0;
            var numRowsRead = 0;

            var observations = GetInvalidObservations().ToAsyncEnumerable();
            var observationsLIst = await observations.ToListAsync(CancellationToken);
            var reader = new SdmxObservationReader(observations, _reportedComponents, _dataflow, _codeTranslator, configuration, true, false, false);

            Assert.IsFalse(reader.IsClosed);

            while (reader.Read())
            {
                cnt++;
            }

            Assert.IsTrue(reader.IsClosed);
            Assert.IsTrue(observationsLIst.Count > configuration.MaxTransferErrorAmount);
            Assert.AreEqual(numRowsRead, cnt);
            Assert.AreEqual(configuration.MaxTransferErrorAmount, reader.GetErrors().Count);
        }

        [Test]
        public void Read_Invalid_Source()
        {
            var observations = new List<ObservationRow>() { null }.ToAsyncEnumerable();

            var reader = new SdmxObservationReader(observations, _reportedComponents, _dataflow, _codeTranslator, Configuration, true, false, false);

            Assert.IsFalse(reader.Read());
            Assert.IsTrue(reader.IsClosed);
            Assert.IsFalse(reader.Read());
            Assert.IsTrue(reader.IsClosed);
            Assert.AreEqual(reader.GetErrors().Count, 1);
            Assert.AreEqual(reader.GetErrors()[0].Type, ValidationErrorType.Undefined);
        }

        [Test]
        public void GetNameOfColumnIndex()
        {
            var expectedColumns = new string[]
            {
                "PERIOD_START", "PERIOD_END", "TIME_PERIOD", // time dimension related columns
                "FREQ", "AME_REF_AREA", "AME_TRANSFORMATION", "AME_AGG_METHOD", "AME_UNIT", "AME_REFERENCE", "AME_ITEM", // non-time dimensions
                "VALUE", // primary measure
                "TIME_FORMAT","OBS_STATUS","OBS_CONF","OBS_PRE_BREAK", "DIM_CONF", "GROUP_CONF", "PUBL_ECB","PUBL_MU","PUBL_PUBLIC","COMPILATION","EXT_TITLE","EXT_UNIT","TITLE_COMPL", // attributes
                "REQUESTED_ACTION","REAL_ACTION_FACT","REAL_ACTION_ATTR","BATCH_NUMBER" // additional data import related columns
            };

            var observations = GetValidObservations().ToAsyncEnumerable();

            var reportedComponents = GetReportedComponents(_dataflow);

            var reader = new SdmxObservationReader(observations, reportedComponents, _dataflow, _codeTranslator, Configuration, true, false, false);

            for (var i = 0; i < expectedColumns.Length; i++)
            {
                var colName = reader.GetName(i);

                Assert.AreEqual(expectedColumns[i], colName, $"Name of column [{i}] is not as expected. Expected [{expectedColumns[i]}], but was [{colName}]");
            }

            Assert.IsEmpty(reader.GetName(-1), $"Name of column [-1] is not as expected. Expected empty string since the index is invalid, but was [{reader.GetName(-1)}]");
            Assert.IsEmpty(reader.GetName(expectedColumns.Length), $"Name of column [{expectedColumns.Length}] is not as expected. Expected empty string since the index is invalid, but was [{reader.GetName(expectedColumns.Length)}]");
        }

        private IList<ObservationRow> GetValidObservations()
        {
            var orderedKey = new KeyableImpl(
                _dataflow.Base,
                _dataflow.Dsd.Base,
                new List<IKeyValue>
                {
                    new KeyValueImpl("Q", "FREQ"),
                    new KeyValueImpl("DNK", "AME_REF_AREA"),
                    new KeyValueImpl("3", "AME_TRANSFORMATION"),
                    new KeyValueImpl("4", "AME_AGG_METHOD"),
                    new KeyValueImpl("319", "AME_UNIT"),
                    new KeyValueImpl("0", "AME_REFERENCE"),
                    new KeyValueImpl("UDGGL", "AME_ITEM"),
                },
                null
            );

            var unorderedKey = new KeyableImpl(
                _dataflow.Base,
                _dataflow.Dsd.Base,
                new List<IKeyValue>
                {
                    new KeyValueImpl("UDGGL", "AME_ITEM"),
                    new KeyValueImpl("3", "AME_TRANSFORMATION"),
                    new KeyValueImpl("DNK", "AME_REF_AREA"),
                    new KeyValueImpl("Q", "FREQ"),
                    new KeyValueImpl("319", "AME_UNIT"),
                    new KeyValueImpl("0", "AME_REFERENCE"),
                    new KeyValueImpl("4", "AME_AGG_METHOD"),
                },
                null
            );

            var observations = new List<ObservationRow>
            {
                // same order of attributes as in DSD ------------------------------------------------------------
                new (1, StagingRowActionEnum.Merge, new ObservationImpl(orderedKey, "2010", "7111", new []
                {
                    new KeyValueImpl("text", "TIME_FORMAT"),          // group lvl //Mandatory
                    new KeyValueImpl("A", "OBS_STATUS"),                            // obsevation lvl
                    new KeyValueImpl("F", "OBS_CONF"),                              // obsevation lvl
                    new KeyValueImpl("text", "OBS_PRE_BREAK"),      // obsevation lvl
                    new KeyValueImpl("PUBL_ECB - text", "PUBL_ECB"),                // group lvl
                    new KeyValueImpl("PUBL_MU - text", "PUBL_MU"),                  // group lvl
                    new KeyValueImpl("PUBL_PUBLIC - text", "PUBL_PUBLIC"),          // group lvl
                    new KeyValueImpl("COMPILATION - text", "COMPILATION"),          // group lvl
                    new KeyValueImpl("EXT_TITLE - text", "EXT_TITLE"),              // group lvl
                    new KeyValueImpl("EXT_UNIT - text", "EXT_UNIT"),                // group lvl
                    new KeyValueImpl("TITLE_COMPL - text", "TITLE_COMPL")           // group lvl
                }), false),
                new (1, StagingRowActionEnum.Merge, new ObservationImpl(orderedKey, "2011", "7222", new []
                {
                    new KeyValueImpl("text", "TIME_FORMAT"),//Mandatory
                    new KeyValueImpl("B", "OBS_STATUS")
                }), false),
                new (1, StagingRowActionEnum.Merge, new ObservationImpl(orderedKey, "2012", "7333", new []
                {
                    new KeyValueImpl("text", "TIME_FORMAT"),//Mandatory
                    new KeyValueImpl("D", "OBS_STATUS")
                }), false),
                // order different from DSD -----------------------------------------------------------------------
                new (1, StagingRowActionEnum.Merge, new ObservationImpl(unorderedKey, "2015", "7444", new []
                {
                    new KeyValueImpl("TITLE_COMPL - text", "TITLE_COMPL"),//Mandatory
                    new KeyValueImpl("text", "TIME_FORMAT"),
                    new KeyValueImpl("EXT_UNIT - text", "EXT_UNIT"),
                    new KeyValueImpl("F", "OBS_CONF"),
                    new KeyValueImpl("text", "OBS_PRE_BREAK"),
                    new KeyValueImpl("PUBL_ECB - text", "PUBL_ECB"),
                    new KeyValueImpl("A", "OBS_STATUS"),
                    new KeyValueImpl("COMPILATION - text", "COMPILATION"),
                    new KeyValueImpl("EXT_TITLE - text", "EXT_TITLE"),
                    new KeyValueImpl("PUBL_MU - text", "PUBL_MU"),
                    new KeyValueImpl("PUBL_PUBLIC - text", "PUBL_PUBLIC")

                }), false),
                new (1, StagingRowActionEnum.Merge, new ObservationImpl(unorderedKey, "2016", "7555", new []
                {
                    new KeyValueImpl("text", "TIME_FORMAT"),          //Mandatory
                    new KeyValueImpl("B", "OBS_STATUS")
                }), false),
                new (1, StagingRowActionEnum.Merge, new ObservationImpl(unorderedKey, "2017", "7666", new []
                {
                    new KeyValueImpl("text", "TIME_FORMAT"),//Mandatory
                    new KeyValueImpl("D", "OBS_STATUS")
                }), false),
            };

            return observations;
        }

        private IList<ObservationRow> GetInvalidObservations()
        {
            var seriesKey = new List<IKeyValue>
            {
                new KeyValueImpl("Q", "FREQ"),
                new KeyValueImpl("DNK", "AME_REF_AREA"),
                new KeyValueImpl("3", "AME_TRANSFORMATION"),
                new KeyValueImpl("4", "AME_AGG_METHOD"),
                new KeyValueImpl("319", "AME_UNIT"),
                new KeyValueImpl("0", "AME_REFERENCE"),
                new KeyValueImpl("UDGGL", "AME_ITEM"),
            };

            var key = new KeyableImpl(_dataflow.Base, _dataflow.Dsd.Base, seriesKey, null);

            var observations = new List<ObservationRow>
            {
                new (1, StagingRowActionEnum.Merge, new ObservationImpl(key, "2010", "1", null), false), 
                new (1, StagingRowActionEnum.Merge, new ObservationImpl(key, "2011", "2", new List<IKeyValue>(new [] 
                {
                    new KeyValueImpl("M", "OBS_STATUS")
                })), false),
                new (1, StagingRowActionEnum.Merge, new ObservationImpl(key, "2011", "2", new List<IKeyValue>(new [] 
                {
                    new KeyValueImpl("N", "OBS_STATUS")
                })), false),
                new (1, StagingRowActionEnum.Merge, new ObservationImpl(key, "2012", "3", new List<IKeyValue>(new [] 
                {
                    new KeyValueImpl("A", "OBS_STATUS")
                })), false),
                new (1, StagingRowActionEnum.Merge, new ObservationImpl(key, "2013", "4", new List<IKeyValue>(new [] 
                {
                    new KeyValueImpl("B", "OBS_STATUS")
                })), false),
                new (1, StagingRowActionEnum.Merge, new ObservationImpl(key, "2015", "string", null), false),  
                new (1, StagingRowActionEnum.Merge, new ObservationImpl(key, "2016", "6", new List<IKeyValue>(new [] 
                {
                    new KeyValueImpl("", "OBS_STATUS")
                })), false),
            };

            return observations;
        }


    }
}
