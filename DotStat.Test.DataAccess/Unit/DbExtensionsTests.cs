﻿using DotStat.Db;
using DotStat.Domain;
using DotStat.Test.Moq;
using NUnit.Framework;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using PrimaryMeasure = DotStat.Domain.PrimaryMeasure;

namespace DotStat.Test.DataAccess.Unit
{
    [TestFixture, Parallelizable(ParallelScope.All)]
    public sealed class DbExtensionsTests : UnitTestBase
    {
        private readonly TestMappingStoreDataAccess _mappingStoreDataAccess;
        
        public DbExtensionsTests()
        {
            _mappingStoreDataAccess = new TestMappingStoreDataAccess("sdmx/OBS_TYPE_TEST.xml");
        }

        [TestCaseSource(nameof(GetTestCaseData))]
        public void GetObjectFromStringMeasure(TextEnumType textType, string observationValueStr, object expectedValue, StagingRowActionEnum action, bool addAnnotation)
        {
            var dataStructureObject = GetDsd(_mappingStoreDataAccess, textType, addAnnotation);

            var primaryMeasure = new PrimaryMeasure(dataStructureObject.PrimaryMeasure);
            var dsd = new Dsd(dataStructureObject);
            dsd.SetPrimaryMeasure(primaryMeasure);
            var actualValue = dsd.GetPrimaryMeasureObjectFromString(observationValueStr, action, new CultureInfo("en"));
            Assert.AreEqual(expectedValue, actualValue);
        }

        public static IEnumerable<TestCaseData> GetTestCaseData
        {
            get
            {
                yield return new TestCaseData(TextEnumType.String, null, null, StagingRowActionEnum.Merge, false);
                yield return new TestCaseData(TextEnumType.String, null, null, StagingRowActionEnum.Replace, false);
                yield return new TestCaseData(TextEnumType.String, null, null, StagingRowActionEnum.Delete, false);
                yield return new TestCaseData(TextEnumType.String, "", null, StagingRowActionEnum.Merge, true);
                yield return new TestCaseData(TextEnumType.String, "", null, StagingRowActionEnum.Replace, true);
                yield return new TestCaseData(TextEnumType.String, "", null, StagingRowActionEnum.Delete, true);
                yield return new TestCaseData(TextEnumType.String, "normal value", "normal value", StagingRowActionEnum.Merge, false);
                yield return new TestCaseData(TextEnumType.String, "normal value", "normal value", StagingRowActionEnum.Replace, false);
                yield return new TestCaseData(TextEnumType.String, "normal value", "1", StagingRowActionEnum.Delete, false);
                //Supports Intentionally missing 
                yield return new TestCaseData(TextEnumType.String, "#N/A", "#N/A", StagingRowActionEnum.Merge, false);
                yield return new TestCaseData(TextEnumType.String, "#N/A", "#N/A", StagingRowActionEnum.Replace, false);
                yield return new TestCaseData(TextEnumType.String, "#N/A", "1", StagingRowActionEnum.Delete, false);
                yield return new TestCaseData(TextEnumType.String, "NaN", "NaN", StagingRowActionEnum.Merge, false);
                yield return new TestCaseData(TextEnumType.String, "NaN", "NaN", StagingRowActionEnum.Replace, false);
                yield return new TestCaseData(TextEnumType.String, "NaN", "1", StagingRowActionEnum.Delete, false);
                //Dont Supports Intentionally missing 
                yield return new TestCaseData(TextEnumType.String, "#N/A", "#N/A", StagingRowActionEnum.Merge, true);
                yield return new TestCaseData(TextEnumType.String, "#N/A", "#N/A", StagingRowActionEnum.Replace, true);
                yield return new TestCaseData(TextEnumType.String, "#N/A", "1", StagingRowActionEnum.Delete, true);
                yield return new TestCaseData(TextEnumType.String, "NaN", "NaN", StagingRowActionEnum.Merge, true);
                yield return new TestCaseData(TextEnumType.String, "NaN", "NaN", StagingRowActionEnum.Replace, true);
                yield return new TestCaseData(TextEnumType.String, "NaN", "1", StagingRowActionEnum.Delete, true);
                //Numerical cases
                //Supports Intentionally missing 
                yield return new TestCaseData(TextEnumType.Decimal, "#N/A", "#N/A", StagingRowActionEnum.Merge, false);
                yield return new TestCaseData(TextEnumType.Decimal, "#N/A", "1", StagingRowActionEnum.Delete, false);
                yield return new TestCaseData(TextEnumType.Decimal, "NaN", "NaN", StagingRowActionEnum.Merge, false);
                yield return new TestCaseData(TextEnumType.Decimal, "NaN", "1", StagingRowActionEnum.Delete, false);
                yield return new TestCaseData(TextEnumType.Decimal, "normal value", "normal value", StagingRowActionEnum.Merge, false);
                yield return new TestCaseData(TextEnumType.Decimal, "normal value", "1", StagingRowActionEnum.Delete, false);
                yield return new TestCaseData(TextEnumType.Integer, "#N/A", "#N/A", StagingRowActionEnum.Merge, false);
                yield return new TestCaseData(TextEnumType.Integer, "#N/A", 1, StagingRowActionEnum.Delete, false);
                yield return new TestCaseData(TextEnumType.Integer, "NaN", "NaN", StagingRowActionEnum.Merge, false);
                yield return new TestCaseData(TextEnumType.Integer, "NaN", 1, StagingRowActionEnum.Delete, false);
                yield return new TestCaseData(TextEnumType.Integer, "5", 5, StagingRowActionEnum.Merge, false);
                yield return new TestCaseData(TextEnumType.Integer, "5", 1, StagingRowActionEnum.Delete, false);
                yield return new TestCaseData(TextEnumType.Long, "#N/A", "#N/A", StagingRowActionEnum.Merge, false);
                yield return new TestCaseData(TextEnumType.Long, "#N/A", 1, StagingRowActionEnum.Delete, false);
                yield return new TestCaseData(TextEnumType.Long, "NaN", "NaN", StagingRowActionEnum.Merge, false);
                yield return new TestCaseData(TextEnumType.Long, "NaN", 1, StagingRowActionEnum.Delete, false);
                yield return new TestCaseData(TextEnumType.Long, "5", 5, StagingRowActionEnum.Merge, false);
                yield return new TestCaseData(TextEnumType.Long, "5", 1, StagingRowActionEnum.Delete, false);
                yield return new TestCaseData(TextEnumType.Short, "#N/A", "#N/A", StagingRowActionEnum.Merge, false);
                yield return new TestCaseData(TextEnumType.Short, "#N/A", 1, StagingRowActionEnum.Delete, false);
                yield return new TestCaseData(TextEnumType.Short, "NaN", "NaN", StagingRowActionEnum.Merge, false);
                yield return new TestCaseData(TextEnumType.Short, "NaN", 1, StagingRowActionEnum.Delete, false);
                yield return new TestCaseData(TextEnumType.Short, "5", 5, StagingRowActionEnum.Merge, false);
                yield return new TestCaseData(TextEnumType.Short, "5", 1, StagingRowActionEnum.Delete, false);
                yield return new TestCaseData(TextEnumType.Boolean, "#N/A", "#N/A", StagingRowActionEnum.Merge, false);
                yield return new TestCaseData(TextEnumType.Boolean, "#N/A", 1, StagingRowActionEnum.Delete, false);
                yield return new TestCaseData(TextEnumType.Boolean, "NaN", "NaN", StagingRowActionEnum.Merge, false);
                yield return new TestCaseData(TextEnumType.Boolean, "NaN", 1, StagingRowActionEnum.Delete, false);
                yield return new TestCaseData(TextEnumType.Boolean, "false", 0, StagingRowActionEnum.Merge, false);
                yield return new TestCaseData(TextEnumType.Boolean, "false", 1, StagingRowActionEnum.Delete, false);
                yield return new TestCaseData(TextEnumType.Float, "#N/A", "#N/A", StagingRowActionEnum.Merge, false);
                yield return new TestCaseData(TextEnumType.Float, "#N/A", 1.0f, StagingRowActionEnum.Delete, false);
                yield return new TestCaseData(TextEnumType.Float, "NaN", float.NaN, StagingRowActionEnum.Merge, false);
                yield return new TestCaseData(TextEnumType.Float, "NaN", 1.0f, StagingRowActionEnum.Delete, false);
                yield return new TestCaseData(TextEnumType.Float, "5.0", 5.0f, StagingRowActionEnum.Merge, false);
                yield return new TestCaseData(TextEnumType.Float, "5.0", 1.0f, StagingRowActionEnum.Delete, false);
                yield return new TestCaseData(TextEnumType.Double, "#N/A", "#N/A", StagingRowActionEnum.Merge, false);
                yield return new TestCaseData(TextEnumType.Double, "#N/A", 1.0d, StagingRowActionEnum.Delete, false);
                yield return new TestCaseData(TextEnumType.Double, "NaN", double.NaN, StagingRowActionEnum.Merge, false);
                yield return new TestCaseData(TextEnumType.Double, "NaN", 1.0d, StagingRowActionEnum.Delete, false);
                yield return new TestCaseData(TextEnumType.Double, "5.0", 5.0d, StagingRowActionEnum.Merge, false);
                yield return new TestCaseData(TextEnumType.Double, "5.0", 1.0d, StagingRowActionEnum.Delete, false);
                //Dont Supports Intentionally missing 
                yield return new TestCaseData(TextEnumType.Float, "#N/A", "#N/A", StagingRowActionEnum.Merge, true);
                yield return new TestCaseData(TextEnumType.Float, "#N/A", "1", StagingRowActionEnum.Delete, true);
                yield return new TestCaseData(TextEnumType.Float, "NaN", "NaN", StagingRowActionEnum.Merge, true);
                yield return new TestCaseData(TextEnumType.Float, "NaN", "1", StagingRowActionEnum.Delete, true);
                yield return new TestCaseData(TextEnumType.Float, "5.0", "5.0", StagingRowActionEnum.Merge, true);
                yield return new TestCaseData(TextEnumType.Float, "5.0", "1", StagingRowActionEnum.Delete, true);
                yield return new TestCaseData(TextEnumType.Double, "#N/A", "#N/A", StagingRowActionEnum.Merge, true);
                yield return new TestCaseData(TextEnumType.Double, "#N/A", "1", StagingRowActionEnum.Delete, true);
                yield return new TestCaseData(TextEnumType.Double, "NaN", "NaN", StagingRowActionEnum.Merge, true);
                yield return new TestCaseData(TextEnumType.Double, "NaN", "1", StagingRowActionEnum.Delete, true);
                yield return new TestCaseData(TextEnumType.Double, "5.0", "5.0", StagingRowActionEnum.Merge, true);
                yield return new TestCaseData(TextEnumType.Double, "5.0", "1", StagingRowActionEnum.Delete, true);
                yield return new TestCaseData(TextEnumType.ObservationalTimePeriod, "#N/A", "#N/A", StagingRowActionEnum.Merge, false);
                yield return new TestCaseData(TextEnumType.ObservationalTimePeriod, "#N/A", "1", StagingRowActionEnum.Delete, false);
                yield return new TestCaseData(TextEnumType.ObservationalTimePeriod, "NaN", "NaN", StagingRowActionEnum.Merge, false);
                yield return new TestCaseData(TextEnumType.ObservationalTimePeriod, "NaN", "1", StagingRowActionEnum.Delete, false);
                yield return new TestCaseData(TextEnumType.ObservationalTimePeriod, "normal value", "normal value", StagingRowActionEnum.Merge, false);
                yield return new TestCaseData(TextEnumType.ObservationalTimePeriod, "normal value", "1", StagingRowActionEnum.Delete, false);
            }
        }

        private IDataStructureObject GetDsd(TestMappingStoreDataAccess mappingStoreDataAccess, TextEnumType textType, bool addIntentionallyMissingAnnotation)
        {
            var sdmxObjects = addIntentionallyMissingAnnotation
                ? mappingStoreDataAccess.GetSdmxObjects("sdmx/OBS_TYPE_TEST_with_intentionally_missing_annotation.xml")
                : mappingStoreDataAccess.GetSdmxObjects("sdmx/OBS_TYPE_TEST.xml");

            var dsdId = "OBS_TYPE_DSD_" + (textType == TextEnumType.TimesRange ? "TIMERANGE" : textType.ToString().ToUpper());
            var dsd = sdmxObjects.DataStructures.FirstOrDefault(d => d.Id.Equals(dsdId, StringComparison.InvariantCultureIgnoreCase));

            Assert.IsNotNull(dsd, $"DSD {dsdId} not found.");

            return dsd;
        }
    }
}
