﻿using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using DotStat.Common.Configuration;
using DotStat.Common.Configuration.Interfaces;
using DotStat.Db;
using DotStat.Db.DB;
using DotStat.Db.Repository.SqlServer;
using DotStat.Db.Util;
using DotStat.Domain;
using DotStat.Test.Moq;
using DotStat.MappingStore;
using Moq;
using NUnit.Framework;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Dataflow = DotStat.Domain.Dataflow;
using System.Data;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Data.Query;
using Org.Sdmxsource.Sdmx.StructureRetrieval.Manager;
using Org.Sdmxsource.Sdmx.Util.Objects.Container;

namespace DotStat.Test.DataAccess.Unit.Repository
{
    [TestFixture, Parallelizable(ParallelScope.All)]
    public sealed class SqlObservationRepositoryTests : SdmxUnitTestBase
    {
        private readonly TestMappingStoreDataAccess _mappingStoreDataAccess;
        private readonly Dataflow _dataDataflow;
        private readonly Dataflow _metadataDataflow;
        private readonly IGeneralConfiguration _testConfiguration;
        private ICodeTranslator _codeTranslator;
        public SqlObservationRepositoryTests()
        {
            _mappingStoreDataAccess     = new TestMappingStoreDataAccess();

            _dataDataflow = _mappingStoreDataAccess.GetDataflow();
            new TestManagementRepository(_dataDataflow).FillIdsFromDisseminationDb(_dataDataflow);

            _metadataDataflow = new TestMappingStoreDataAccess("sdmx/CsvV2.xml").GetDataflow();
            new TestManagementRepository(_metadataDataflow).FillIdsFromDisseminationDb(_metadataDataflow);

            _testConfiguration = new BaseConfiguration();

            var codeListRepository = new TestCodeListRepository(_dataDataflow);
            _codeTranslator = new CodeTranslator(codeListRepository);
        }

        [OneTimeSetUp]
        public async Task Init()
        {
            await _codeTranslator.FillDict(_dataDataflow, CancellationToken);
        }

        [Test]
        [TestCase("PERIOD_SDMX, [DIM_101], [DIM_102], [DIM_103], [DIM_104], [DIM_105], [DIM_106], [DIM_107], [DIM_108], [DIM_109]", false)]
        //external Columns
        [TestCase("[TIME_PERIOD], [FREQ], [REF_AREA], [IND_TYPE], [ADJUSTMENT], [MEASURE], [ETA], [SESSO], [GRADO_ISTRUZ], [CATEG_PROF]", true)]
        public void DimensionColumnListExternalColumns(string expectedString, bool externalColumn)
        {
            var actualString = _dataDataflow.Dimensions.ToColumnList(externalColumn: externalColumn);
            Assert.AreEqual(expectedString, actualString);
        }

        [TestCase("[COMP_201],[COMP_202],[COMP_203],[COMP_204],[COMP_205],[COMP_206],[COMP_207],[COMP_208],[COMP_209],[COMP_210],[COMP_211],[COMP_212],[COMP_213]", null, false, false)]
        [TestCase("[COMP_201],[COMP_202],[COMP_203],[COMP_204],[COMP_205],[COMP_206],[COMP_207],[COMP_208]", new [] {AttributeAttachmentLevel.Observation}, false, false)]
        [TestCase("[COMP_209],[COMP_210],[COMP_211],[COMP_212],[COMP_213]", new [] {AttributeAttachmentLevel.DataSet}, false, false)]
        [TestCase("[COMP_201],[COMP_202],[COMP_203],[COMP_204],[COMP_205],[COMP_206],[COMP_207],[COMP_208],[COMP_209],[COMP_210],[COMP_211],[COMP_212],[COMP_213]", new [] { AttributeAttachmentLevel.Observation, AttributeAttachmentLevel.DataSet}, false, false)]
        [TestCase("[COMP_209] [int],[COMP_210] [int],[COMP_211] [nvarchar](MAX) NOT NULL,[COMP_212] [nvarchar](MAX),[COMP_213] [nvarchar](MAX)", new[] { AttributeAttachmentLevel.DataSet }, true, false)]
        [TestCase("", new[] { AttributeAttachmentLevel.Group }, false, false)]
        //external Columns
        [TestCase("[COMMENT],[CONF_STATUS],[OBS_PRE_BREAK],[OBS_STATUS],[DECIMALS],[NOTA_IT],[NOTA_EN],[BREAK],[COLLECTION],[TIME_FORMAT],[TITLE],[METADATA_EN],[METADATA_IT]", null, false, true)]
        [TestCase("[COMMENT],[CONF_STATUS],[OBS_PRE_BREAK],[OBS_STATUS],[DECIMALS],[NOTA_IT],[NOTA_EN],[BREAK]", new[] { AttributeAttachmentLevel.Observation }, false, true)]
        [TestCase("[COLLECTION],[TIME_FORMAT],[TITLE],[METADATA_EN],[METADATA_IT]", new[] { AttributeAttachmentLevel.DataSet }, false, true)]
        [TestCase("[COMMENT],[CONF_STATUS],[OBS_PRE_BREAK],[OBS_STATUS],[DECIMALS],[NOTA_IT],[NOTA_EN],[BREAK],[COLLECTION],[TIME_FORMAT],[TITLE],[METADATA_EN],[METADATA_IT]", new[] { AttributeAttachmentLevel.Observation, AttributeAttachmentLevel.DataSet }, false, true)]
        public void AttributeColumnList(string expectedString, IList<AttributeAttachmentLevel> levels, bool withType, bool externalColumm)
        {
            var actualString = _dataDataflow.Dsd.Attributes.ToColumnList(levels, withType, externalColumn: externalColumm);
            Assert.AreEqual(expectedString, actualString);
        }

        [TestCase("data/264D_264_SALDI/all", null)]
        [TestCase("data/264D_264_SALDI/.IT+ITC1.1_S+COF_ECO...TOTAL...", "([REF_AREA]='IT' OR [REF_AREA]='ITC1') AND ([IND_TYPE]='1_S' OR [IND_TYPE]='COF_ECO') AND ([ETA]='TOTAL')")]
        [TestCase("data/264D_264_SALDI/......../?startPeriod=2010-01-01&endPeriod=2010-12-31", "(PERIOD_END>='2010-01-01') AND (PERIOD_START<'2011-01-01')")]
        [TestCase("data/264D_264_SALDI/all/?startPeriod=2010-02&endPeriod=2015", "(PERIOD_END>='2010-02-01') AND (PERIOD_START<'2016-01-01')")]
        [TestCase("data/264D_264_SALDI/.IT.1_S...TOTAL.../?startPeriod=2009-Q1&endPeriod=2015-12", "([REF_AREA]='IT') AND ([IND_TYPE]='1_S') AND ([ETA]='TOTAL') AND (PERIOD_END>='2009-01-01') AND (PERIOD_START<'2016-01-01')")]
        [TestCase("data/264D_264_SALDI/A........2D1+1AB/?startPeriod=2010-01-01", "([FREQ]='A') AND ([CATEG_PROF]='2D1' OR [CATEG_PROF]='1AB') AND (PERIOD_END>='2010-01-01')")]
        [TestCase("data/264D_264_SALDI/A........2D1+1AB/?startPeriod=2010-01-01&updatedAfter=2009-05-15", "([FREQ]='A') AND ([CATEG_PROF]='2D1' OR [CATEG_PROF]='1AB') AND (PERIOD_END>='2010-01-01') AND [LAST_UPDATED] >= @LastUpdated")]
        [TestCase("data/264D_264_SALDI/A........2D1+1AB/?startPeriod=2010-01-01&updatedAfter=2009-05-15", "([FREQ]='A' OR [FREQ] IS NULL) AND ([CATEG_PROF]='2D1' OR [CATEG_PROF]='1AB' OR [CATEG_PROF] IS NULL) AND (PERIOD_END>='2010-01-01' OR PERIOD_END IS NULL) AND [LAST_UPDATED] >= @LastUpdated", true)]
        public void PredicateBuildTest(string restQuery, string expectedSql, bool includeNulls= false)
        {
            var dataQueryFiltered = new RESTDataQueryCore(restQuery);
            var filterLastUpdated = dataQueryFiltered.UpdatedAfter is not null;
            var query = new DataQueryImpl(dataQueryFiltered, new InMemoryRetrievalManager(new SdmxObjectsImpl(_dataDataflow.Dsd.Base, _dataDataflow.Base)));

            var sql = Predicate.BuildWhereForObservationLevel(query, _dataDataflow, _codeTranslator, filterLastUpdated, includeNulls ? NullTreatment.IncludeNulls : NullTreatment.ExcludeNulls);

            Assert.AreEqual(expectedSql, sql);
        }

        [TestCase("data/264D_264_SALDI/all")]
        [TestCase("data/264D_264_SALDI/all?updatedAfter=2009-05-15")]
        [TestCase("data/264D_264_SALDI/.IT+ITC1.1_S+COF_ECO...TOTAL...")]
        [TestCase("data/264D_264_SALDI/A..COF......")]
        [TestCase("data/264D_264_SALDI/.ITF1.......")]
        public void BuildReplaceSqlQuery(string restQuery)
        {
            var dataQueryFiltered = new RESTDataQueryCore(restQuery);
            var filterLastUpdated = dataQueryFiltered.UpdatedAfter is not null;
            var sdmxQuery = GetQuery(restQuery);

            var dotStatDb = new SqlDotStatDb(Configuration.SpacesInternal.FirstOrDefault(), DbUp.DatabaseVersion.DataDbVersion);
            var repository = new SqlObservationRepository(dotStatDb, _testConfiguration);

            var lastUpdated = filterLastUpdated ? $"WHERE [{DbExtensions.LAST_UPDATED_COLUMN}] >= @LastUpdated" : "";

            var expectedSql = string.Empty;
            
            if (filterLastUpdated)
            {
                var wherePart = Predicate.BuildWhereForObservationLevel(sdmxQuery, _dataDataflow, codeTranslator: null, filterLastUpdated: false, nullTreatment: NullTreatment.IncludeNulls);
                if (!string.IsNullOrEmpty(wherePart))
                    wherePart = $"WHERE {wherePart}";

                expectedSql = $@"WITH OBS_LEVEL AS (
	SELECT FI.[SID], [FI].[DIM_101] AS [DIM_101_OBS], [FI].[DIM_102] AS [DIM_102_OBS], [FI].[DIM_103] AS [DIM_103_OBS], [FI].[DIM_104] AS [DIM_104_OBS], [FI].[DIM_105] AS [DIM_105_OBS], [FI].[DIM_106] AS [DIM_106_OBS], [FI].[DIM_107] AS [DIM_107_OBS], [FI].[DIM_108] AS [DIM_108_OBS], [FI].[DIM_109] AS [DIM_109_OBS],[FA].PERIOD_SDMX AS [TIME_PERIOD], PERIOD_START, PERIOD_END,
         [FA].[VALUE] AS [OBS_VALUE]
        , [FA].[COMP_201] AS [COMMENT], [CL_CONF_STATUS].[ID] AS [CONF_STATUS], [FA].[COMP_203] AS [OBS_PRE_BREAK], [CL_OBS_STATUS].[ID] AS [OBS_STATUS], [CL_DECIMALS].[ID] AS [DECIMALS], [FA].[COMP_206] AS [NOTA_IT], [FA].[COMP_207] AS [NOTA_EN], [FA].[COMP_208] AS [BREAK]
	 FROM [data].FACT_7_A FA
	 LEFT JOIN [data].FILT_7 FI ON FI.[SID] = FA.[SID]
	 LEFT JOIN [management].[CL_202] AS [CL_CONF_STATUS] ON FA.[COMP_202] = [CL_CONF_STATUS].[ITEM_ID]
LEFT JOIN [management].[CL_204] AS [CL_OBS_STATUS] ON FA.[COMP_204] = [CL_OBS_STATUS].[ITEM_ID]
LEFT JOIN [management].[CL_205] AS [CL_DECIMALS] ON FA.[COMP_205] = [CL_DECIMALS].[ITEM_ID]
 {lastUpdated}	 
), DF_LEVEL AS (
SELECT [DF_ID], [CL_COLLECTION].[ID] AS [COLLECTION], [CL_TIME_FORMAT].[ID] AS [TIME_FORMAT], [ADF].[COMP_211] AS [TITLE], [ADF].[COMP_212] AS [METADATA_EN], [ADF].[COMP_213] AS [METADATA_IT]
FROM [data].ATTR_7_A_DF ADF
LEFT JOIN [management].[CL_209] AS [CL_COLLECTION] ON [ADF].[COMP_209] = [CL_COLLECTION].[ITEM_ID]
LEFT JOIN [management].[CL_210] AS [CL_TIME_FORMAT] ON [ADF].[COMP_210] = [CL_TIME_FORMAT].[ITEM_ID]
WHERE [ADF].[DF_ID] = 1 {lastUpdated.Replace("WHERE", "AND")}
)
SELECT [TIME_PERIOD], [FREQ], [REF_AREA], [IND_TYPE], [ADJUSTMENT], [MEASURE], [ETA], [SESSO], [GRADO_ISTRUZ], [CATEG_PROF], [OBS_VALUE], [COMMENT], [CONF_STATUS], [OBS_PRE_BREAK], [OBS_STATUS], [DECIMALS], [NOTA_IT], [NOTA_EN], [BREAK], BatchNumber
FROM (
SELECT [TIME_PERIOD], PERIOD_START, PERIOD_END,[CL_FREQ].[ID] AS [FREQ], [CL_REF_AREA].[ID] AS [REF_AREA], [CL_IND_TYPE].[ID] AS [IND_TYPE], [CL_ADJUSTMENT].[ID] AS [ADJUSTMENT], [CL_MEASURE].[ID] AS [MEASURE], [CL_ETA].[ID] AS [ETA], [CL_SESSO].[ID] AS [SESSO], [CL_GRADO_ISTRUZ].[ID] AS [GRADO_ISTRUZ], [CL_CATEG_PROF].[ID] AS [CATEG_PROF], [OBS_VALUE], [COMMENT], [CONF_STATUS], [OBS_PRE_BREAK], [OBS_STATUS], [DECIMALS], [NOTA_IT], [NOTA_EN], [BREAK], @BatchNumber as [BatchNumber]
FROM OBS_LEVEL OL
FULL JOIN DF_LEVEL [DF] ON [DF_ID] = 1
LEFT JOIN [management].[CL_101] AS [CL_FREQ] ON [DIM_101_OBS] = [CL_FREQ].[ITEM_ID]
LEFT JOIN [management].[CL_102] AS [CL_REF_AREA] ON [DIM_102_OBS] = [CL_REF_AREA].[ITEM_ID]
LEFT JOIN [management].[CL_103] AS [CL_IND_TYPE] ON [DIM_103_OBS] = [CL_IND_TYPE].[ITEM_ID]
LEFT JOIN [management].[CL_104] AS [CL_ADJUSTMENT] ON [DIM_104_OBS] = [CL_ADJUSTMENT].[ITEM_ID]
LEFT JOIN [management].[CL_105] AS [CL_MEASURE] ON [DIM_105_OBS] = [CL_MEASURE].[ITEM_ID]
LEFT JOIN [management].[CL_106] AS [CL_ETA] ON [DIM_106_OBS] = [CL_ETA].[ITEM_ID]
LEFT JOIN [management].[CL_107] AS [CL_SESSO] ON [DIM_107_OBS] = [CL_SESSO].[ITEM_ID]
LEFT JOIN [management].[CL_108] AS [CL_GRADO_ISTRUZ] ON [DIM_108_OBS] = [CL_GRADO_ISTRUZ].[ITEM_ID]
LEFT JOIN [management].[CL_109] AS [CL_CATEG_PROF] ON [DIM_109_OBS] = [CL_CATEG_PROF].[ITEM_ID]
) innerQuery
{wherePart}";
            }
            else
            {
                var wherePart = Predicate.BuildWhereForObservationLevel(sdmxQuery, _dataDataflow, codeTranslator: null, filterLastUpdated: false, nullTreatment: NullTreatment.ExcludeNulls);

                if (!string.IsNullOrEmpty(wherePart))
                    wherePart = $"WHERE {wherePart}";

                expectedSql = $@"SELECT [TIME_PERIOD], [FREQ], [REF_AREA], [IND_TYPE], [ADJUSTMENT], [MEASURE], [ETA], [SESSO], [GRADO_ISTRUZ], [CATEG_PROF], [OBS_VALUE], [COMMENT], [CONF_STATUS], [OBS_PRE_BREAK], [OBS_STATUS], [DECIMALS], [NOTA_IT], [NOTA_EN], [BREAK], @BatchNumber as [BatchNumber]
FROM [data].[VI_CurrentDataDataFlow_1_A]
{wherePart}";
            }
            
            var components = new List<string>
            {
                _dataDataflow.Dsd.Base.PrimaryMeasure.Id
            };

            components.AddRange(_dataDataflow.ObsAttributes.Select(a => a.Code));

            var sqlQuery = repository.BuildReplaceSqlQuery(
                sdmxQuery,
                _dataDataflow,
                components,
                DbTableVersion.A,
                true,
                filterLastUpdated: filterLastUpdated
            );

            Assert.AreEqual(expectedSql.Replace("\n", "").Replace("\r", "").Replace("\t", ""), sqlQuery.Replace("\n", "").Replace("\r", "").Replace("\t", ""));
        }

        [TestCase("data/UNSD,DF_JENS_DAILY,1.0/all", false)]
        [TestCase("data/UNSD,DF_JENS_DAILY,1.0/all?updatedAfter=2009-05-15", false)]
        [TestCase("data/UNSD,DF_JENS_DAILY,1.0/all", true)]
        [TestCase("data/UNSD,DF_JENS_DAILY,1.0/all?updatedAfter=2009-05-15", true)]
        public void BuildReplaceSqlQueryMetadata(string restQuery, bool dsdReference)
        {
            var dataQueryFiltered = new RESTDataQueryCore(restQuery);
            var filterLastUpdated = dataQueryFiltered.UpdatedAfter is not null;
            var sdmxQuery = new DataQueryImpl(dataQueryFiltered, new TestMappingStoreDataAccess("sdmx/CsvV2.xml").GetRetrievalManager(""));

            var dotStatDb = new SqlDotStatDb(Configuration.SpacesInternal.FirstOrDefault(), DbUp.DatabaseVersion.DataDbVersion);
            var repository = new SqlObservationRepository(dotStatDb, _testConfiguration);

            var components = new List<string>
            {
                _metadataDataflow.Dsd.Base.PrimaryMeasure.Id
            };

            components.AddRange(_metadataDataflow.ObsAttributes.Select(a => a.Code));

            var lastUpdated = filterLastUpdated ? $"\nWHERE [{DbExtensions.LAST_UPDATED_COLUMN}] >= @LastUpdated" : "";
            var expectedSql = dsdReference? "SELECT [TIME_PERIOD], [FREQ], [REPORTING_TYPE], [SERIES], [REF_AREA], [SEX], [OBS_VALUE], [OBS_STATUS], [UNIT_MULT], [NATURE], [COMMENT_OBS], [TIME_COVERAGE], [UPPER_BOUND], [LOWER_BOUND], [BASE_PER], [TIME_DETAIL], [SOURCE_DETAIL], @BatchNumber as [BatchNumber]\nFROM [data].[VI_MetadataDsd_7_A]" + lastUpdated
            : $@"SELECT [TIME_PERIOD], [FREQ], [REPORTING_TYPE], [SERIES], [REF_AREA], [SEX], [OBS_VALUE], [OBS_STATUS], [UNIT_MULT], [NATURE], [COMMENT_OBS], [TIME_COVERAGE], [UPPER_BOUND], [LOWER_BOUND], [BASE_PER], [TIME_DETAIL], [SOURCE_DETAIL], @BatchNumber as [BatchNumber]
FROM (
    SELECT '~' AS [FREQ], '~' AS [REPORTING_TYPE], '~' AS [SERIES], '~' AS [REF_AREA], '~' AS [SEX], '~' AS [TIME_PERIOD], '9999-12-31' AS [PERIOD_START], '0001-01-01' AS [PERIOD_END],[ME].[COMP_0] AS [STRING_TYPE], [ME].[COMP_0] AS [STRING_MULTILANG_TYPE], [ME].[COMP_0] AS [ALPHANUMERIC_TYPE], [ME].[COMP_0] AS [ALPHANUMERIC_MULTILANG_TYPE], [ME].[COMP_0] AS [BOOLEAN_TYPE], [ME].[COMP_0] AS [XHTML_TYPE], [ME].[COMP_0] AS [XHTML_MULTILANG_TYPE], [ME].[COMP_0] AS [INTEGER_TYPE], [ME].[COMP_0] AS [INTEGER_MULTILANG_TYPE], [ME].[COMP_0] AS [DECIMAL_TYPE], [ME].[COMP_0] AS [DECIMAL_MULTILANG_TYPE], [ME].[COMP_0] AS [DATETIME_TYPE], [ME].[COMP_0] AS [TIME_TYPE], [ME].[COMP_0] AS [GREGORIANDAY_TYPE], [ME].[COMP_0] AS [GREGORIAN_YEAR_TYPE], [ME].[COMP_0] AS [GREGORIAN_YEARMONTH_TYPE], [ME].[COMP_0] AS [CONTACT.CONTACT_NAME], [ME].[COMP_0] AS [CONTACT.CONTACT_EMAIL], [ME].[COMP_0] AS [CONTACT.CONTACT_PHONE], [ME].[COMP_0] AS [CONTACT.CONTACT_ORGANISATION], [ME].[COMP_0] AS [CODED_METADATA_FREQ], [LAST_UPDATED]
    FROM [data].META_DS_7_A ME
    WHERE DF_ID = 1
    UNION ALL 
    SELECT CASE WHEN [CL_FREQ].[ID] IS NULL THEN '~' ELSE [CL_FREQ].[ID] END AS [FREQ], CASE WHEN [CL_REPORTING_TYPE].[ID] IS NULL THEN '~' ELSE [CL_REPORTING_TYPE].[ID] END AS [REPORTING_TYPE], CASE WHEN [CL_SERIES].[ID] IS NULL THEN '~' ELSE [CL_SERIES].[ID] END AS [SERIES], CASE WHEN [CL_REF_AREA].[ID] IS NULL THEN '~' ELSE [CL_REF_AREA].[ID] END AS [REF_AREA], CASE WHEN [CL_SEX].[ID] IS NULL THEN '~' ELSE [CL_SEX].[ID] END AS [SEX],CASE WHEN PERIOD_SDMX IS NULL THEN '~' ELSE PERIOD_SDMX END AS [TIME_PERIOD],PERIOD_START,PERIOD_END,[COMP_0] AS [STRING_TYPE], [COMP_0] AS [STRING_MULTILANG_TYPE], [COMP_0] AS [ALPHANUMERIC_TYPE], [COMP_0] AS [ALPHANUMERIC_MULTILANG_TYPE], [COMP_0] AS [BOOLEAN_TYPE], [COMP_0] AS [XHTML_TYPE], [COMP_0] AS [XHTML_MULTILANG_TYPE], [COMP_0] AS [INTEGER_TYPE], [COMP_0] AS [INTEGER_MULTILANG_TYPE], [COMP_0] AS [DECIMAL_TYPE], [COMP_0] AS [DECIMAL_MULTILANG_TYPE], [COMP_0] AS [DATETIME_TYPE], [COMP_0] AS [TIME_TYPE], [COMP_0] AS [GREGORIANDAY_TYPE], [COMP_0] AS [GREGORIAN_YEAR_TYPE], [COMP_0] AS [GREGORIAN_YEARMONTH_TYPE], [COMP_0] AS [CONTACT.CONTACT_NAME], [COMP_0] AS [CONTACT.CONTACT_EMAIL], [COMP_0] AS [CONTACT.CONTACT_PHONE], [COMP_0] AS [CONTACT.CONTACT_ORGANISATION], [COMP_0] AS [CODED_METADATA_FREQ], [LAST_UPDATED]
    FROM [data].META_DF_1_A ME
    LEFT JOIN [management].[CL_101] AS [CL_FREQ] ON [ME].[DIM_101] = [CL_FREQ].[ITEM_ID]
LEFT JOIN [management].[CL_102] AS [CL_REPORTING_TYPE] ON [ME].[DIM_102] = [CL_REPORTING_TYPE].[ITEM_ID]
LEFT JOIN [management].[CL_103] AS [CL_SERIES] ON [ME].[DIM_103] = [CL_SERIES].[ITEM_ID]
LEFT JOIN [management].[CL_104] AS [CL_REF_AREA] ON [ME].[DIM_104] = [CL_REF_AREA].[ITEM_ID]
LEFT JOIN [management].[CL_105] AS [CL_SEX] ON [ME].[DIM_105] = [CL_SEX].[ITEM_ID]

) DF_META{lastUpdated}";

            var sqlQuery = repository.BuildReplaceSqlQuery(
                sdmxQuery,
                dsdReference? _metadataDataflow.Dsd : _metadataDataflow,
                components,
                DbTableVersion.A,
                false,
                filterLastUpdated: filterLastUpdated
            );

            Assert.AreEqual(expectedSql.Replace("\n", "").Replace("\r", "").Replace("\t", ""), sqlQuery.Replace("\n", "").Replace("\r", "").Replace("\t", ""));
        }

        [TestCase("data/OECD,264D_264_SALDI,1.0/all", true)]
        [TestCase("data/OECD,264D_264_SALDI,1.0/all?updatedAfter=2009-05-15", true)]
        [TestCase("data/UNSD,DF_JENS_DAILY,1.0/all", false)]
        [TestCase("data/UNSD,DF_JENS_DAILY,1.0/all?updatedAfter=2009-05-15", false)]
        [TestCase("data/UNSD,DF_JENS_DAILY,1.0/all?updatedAfter=209", true, true)]
        public void BuildLastDeletedAllSqlQuery(string restQuery, bool isDataQuery, bool queryFormatError = false)
        {
            var dotStatDb = new SqlDotStatDb(Configuration.SpacesInternal.FirstOrDefault(), DbUp.DatabaseVersion.DataDbVersion);
            var repository = new SqlObservationRepository(dotStatDb, _testConfiguration);

            if (queryFormatError)
            {
                Assert.Throws<Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException>(() => new RESTDataQueryCore(restQuery));
                return;
            }

            var dataQueryFiltered = new RESTDataQueryCore(restQuery);
            var filterLastUpdated = dataQueryFiltered.UpdatedAfter is not null;

            var dataflow = isDataQuery ? _dataDataflow : _metadataDataflow;

            var dimensions = dataflow.Dimensions.OrderTimeFirst().ToArray();
            var components = new List<string>();
            if(isDataQuery)
            {
                components.Add(dataflow.Dsd.Base.PrimaryMeasure.Id);
                components.AddRange(dataflow.ObsAttributes.Select(a => a.Code));
            }
            else if(dataflow.Dsd.Msd?.MetadataAttributes is not null)
            {
                components.AddRange(dataflow.Dsd.Msd?.MetadataAttributes.Select(a => a.HierarchicalId));
            }

            var lastUpdated = filterLastUpdated ? $"AND [{DbExtensions.LAST_UPDATED_COLUMN}] >= @LastUpdated" : "";

            var expectedSqlQuery = isDataQuery? $@"SELECT MAX(LAST_UPDATED)
FROM [data].[VI_DeletedDataDataFlow_1_A]
WHERE (([TIME_PERIOD] IS NULL AND [FREQ] IS NULL AND [REF_AREA] IS NULL AND [IND_TYPE] IS NULL AND [ADJUSTMENT] IS NULL AND [MEASURE] IS NULL AND [ETA] IS NULL AND [SESSO] IS NULL AND [GRADO_ISTRUZ] IS NULL AND [CATEG_PROF] IS NULL)  OR ([TIME_PERIOD] = '-1' AND [FREQ] = '-1' AND [REF_AREA] = '-1' AND [IND_TYPE] = '-1' AND [ADJUSTMENT] = '-1' AND [MEASURE] = '-1' AND [ETA] = '-1' AND [SESSO] = '-1' AND [GRADO_ISTRUZ] = '-1' AND [CATEG_PROF] = '-1')) AND (
	([OBS_VALUE] IS NULL AND [COMMENT] IS NULL AND [CONF_STATUS] IS NULL AND [OBS_PRE_BREAK] IS NULL AND [OBS_STATUS] IS NULL AND [DECIMALS] IS NULL AND [NOTA_IT] IS NULL AND [NOTA_EN] IS NULL AND [BREAK] IS NULL) -- All omitted
	OR ([OBS_VALUE] IS NOT NULL AND [COMMENT] IS NOT NULL AND [CONF_STATUS] IS NOT NULL AND [OBS_PRE_BREAK] IS NOT NULL AND [OBS_STATUS] IS NOT NULL AND [DECIMALS] IS NOT NULL AND [NOTA_IT] IS NOT NULL AND [NOTA_EN] IS NOT NULL AND [BREAK] IS NOT NULL ) -- All present
) {lastUpdated};" :
$@"SELECT MAX(LAST_UPDATED)
FROM [data].[VI_DeletedMetadataDataFlow_1_A]
WHERE (([TIME_PERIOD] IS NULL AND [FREQ] IS NULL AND [REPORTING_TYPE] IS NULL AND [SERIES] IS NULL AND [REF_AREA] IS NULL AND [SEX] IS NULL) ) AND (
	([STRING_TYPE] IS NULL AND [STRING_MULTILANG_TYPE] IS NULL AND [ALPHANUMERIC_TYPE] IS NULL AND [ALPHANUMERIC_MULTILANG_TYPE] IS NULL AND [BOOLEAN_TYPE] IS NULL AND [XHTML_TYPE] IS NULL AND [XHTML_MULTILANG_TYPE] IS NULL AND [INTEGER_TYPE] IS NULL AND [INTEGER_MULTILANG_TYPE] IS NULL AND [DECIMAL_TYPE] IS NULL AND [DECIMAL_MULTILANG_TYPE] IS NULL AND [DATETIME_TYPE] IS NULL AND [TIME_TYPE] IS NULL AND [GREGORIANDAY_TYPE] IS NULL AND [GREGORIAN_YEAR_TYPE] IS NULL AND [GREGORIAN_YEARMONTH_TYPE] IS NULL AND [CONTACT.CONTACT_NAME] IS NULL AND [CONTACT.CONTACT_EMAIL] IS NULL AND [CONTACT.CONTACT_PHONE] IS NULL AND [CONTACT.CONTACT_ORGANISATION] IS NULL AND [CODED_METADATA_FREQ] IS NULL) -- All omitted
	OR ([STRING_TYPE] IS NOT NULL AND [STRING_MULTILANG_TYPE] IS NOT NULL AND [ALPHANUMERIC_TYPE] IS NOT NULL AND [ALPHANUMERIC_MULTILANG_TYPE] IS NOT NULL AND [BOOLEAN_TYPE] IS NOT NULL AND [XHTML_TYPE] IS NOT NULL AND [XHTML_MULTILANG_TYPE] IS NOT NULL AND [INTEGER_TYPE] IS NOT NULL AND [INTEGER_MULTILANG_TYPE] IS NOT NULL AND [DECIMAL_TYPE] IS NOT NULL AND [DECIMAL_MULTILANG_TYPE] IS NOT NULL AND [DATETIME_TYPE] IS NOT NULL AND [TIME_TYPE] IS NOT NULL AND [GREGORIANDAY_TYPE] IS NOT NULL AND [GREGORIAN_YEAR_TYPE] IS NOT NULL AND [GREGORIAN_YEARMONTH_TYPE] IS NOT NULL AND [CONTACT.CONTACT_NAME] IS NOT NULL AND [CONTACT.CONTACT_EMAIL] IS NOT NULL AND [CONTACT.CONTACT_PHONE] IS NOT NULL AND [CONTACT.CONTACT_ORGANISATION] IS NOT NULL AND [CODED_METADATA_FREQ] IS NOT NULL ) -- All present
) {lastUpdated};";

            var sqlQuery = repository.BuildLastDeletedAllSqlQuery(
                dataflow,
                components,
                DbTableVersion.A,
                isDataQuery: isDataQuery,
                filterLastUpdated: filterLastUpdated
            );

            Assert.AreEqual(expectedSqlQuery, sqlQuery);
        }

        [TestCase("data/264D_264_SALDI/all")]
        [TestCase("data/264D_264_SALDI/all?updatedAfter=2009-05-15")]
        [TestCase("data/264D_264_SALDI/.IT+ITC1.1_S+COF_ECO...TOTAL...")]
        [TestCase("data/264D_264_SALDI/A..COF......")]
        [TestCase("data/264D_264_SALDI/.ITF1.......")]
        [TestCase("data/UNSD,DF_JENS_DAILY,1.0/all", false)]
        [TestCase("data/UNSD,DF_JENS_DAILY,1.0/all?updatedAfter=2009-05-15", false)]
        public void BuildDeletedSqlQuery( string restQuery, bool isDataQuery= true)
        {
            var dotStatDb = new SqlDotStatDb(Configuration.SpacesInternal.FirstOrDefault(), DbUp.DatabaseVersion.DataDbVersion);
            var repository = new SqlObservationRepository(dotStatDb, _testConfiguration);

            var dataQueryFiltered = new RESTDataQueryCore(restQuery);
            var dataQuery = isDataQuery? GetQuery(restQuery) :
                new DataQueryImpl(dataQueryFiltered, new TestMappingStoreDataAccess("sdmx/CsvV2.xml").GetRetrievalManager(""));

            var filterLastUpdated = dataQueryFiltered.UpdatedAfter is not null;

            var dataflow = isDataQuery ? _dataDataflow : _metadataDataflow;

            var dimensions = dataflow.Dimensions.OrderTimeFirst().ToArray();
            var components = new List<string>();
            if (isDataQuery)
            {
                components.Add(dataflow.Dsd.Base.PrimaryMeasure.Id);
                components.AddRange(dataflow.ObsAttributes.Select(a => a.Code));
            }
            else if (dataflow.Dsd.Msd?.MetadataAttributes is not null)
            {
                components.AddRange(dataflow.Dsd.Msd?.MetadataAttributes.Select(a => a.HierarchicalId));
            }

            var filteredQuery = Predicate.BuildWhereForObservationLevel(dataQuery, dataflow, codeTranslator: null, filterLastUpdated, nullTreatment: NullTreatment.IncludeNulls);
            if (!string.IsNullOrEmpty(filteredQuery))
                filteredQuery = $"AND {filteredQuery}";

            var expectedSqlQuery = isDataQuery? $@";WITH WILDCARDED AS(
SELECT [TIME_PERIOD], [FREQ], [REF_AREA], [IND_TYPE], [ADJUSTMENT], [MEASURE], [ETA], [SESSO], [GRADO_ISTRUZ], [CATEG_PROF], [OBS_VALUE], [COMMENT], [CONF_STATUS], [OBS_PRE_BREAK], [OBS_STATUS], [DECIMALS], [NOTA_IT], [NOTA_EN], [BREAK]
, CAST((ROW_NUMBER() OVER(ORDER BY[LAST_UPDATED])) AS INT) AS [BatchNumber]
FROM [data].[VI_DeletedDataDataFlow_1_A]
WHERE([TIME_PERIOD] IS NULL OR [FREQ] IS NULL OR [REF_AREA] IS NULL OR [IND_TYPE] IS NULL OR [ADJUSTMENT] IS NULL OR [MEASURE] IS NULL OR [ETA] IS NULL OR [SESSO] IS NULL OR [GRADO_ISTRUZ] IS NULL OR [CATEG_PROF] IS NULL OR [TIME_PERIOD] = '-1' OR [FREQ] = '-1' OR [REF_AREA] = '-1' OR [IND_TYPE] = '-1' OR [ADJUSTMENT] = '-1' OR [MEASURE] = '-1' OR [ETA] = '-1' OR [SESSO] = '-1' OR [GRADO_ISTRUZ] = '-1' OR [CATEG_PROF] = '-1')
{filteredQuery}
) 
SELECT [TIME_PERIOD], [FREQ], [REF_AREA], [IND_TYPE], [ADJUSTMENT], [MEASURE], [ETA], [SESSO], [GRADO_ISTRUZ], [CATEG_PROF], [OBS_VALUE], [COMMENT], [CONF_STATUS], [OBS_PRE_BREAK], [OBS_STATUS], [DECIMALS], [NOTA_IT], [NOTA_EN], [BREAK]
,CAST(ISNULL((SELECT MAX([BatchNumber]) FROM WILDCARDED), 0) + ROW_NUMBER() OVER (PARTITION BY [TIME_PERIOD], [FREQ], [REF_AREA], [IND_TYPE], [ADJUSTMENT], [MEASURE], [ETA], [SESSO], [GRADO_ISTRUZ], [CATEG_PROF] ORDER BY [LAST_UPDATED]) AS INT) AS [BatchNumber]
FROM [data].[VI_DeletedDataDataFlow_1_A]
WHERE ([TIME_PERIOD] IS NOT NULL AND [FREQ] IS NOT NULL AND [REF_AREA] IS NOT NULL AND [IND_TYPE] IS NOT NULL AND [ADJUSTMENT] IS NOT NULL AND [MEASURE] IS NOT NULL AND [ETA] IS NOT NULL AND [SESSO] IS NOT NULL AND [GRADO_ISTRUZ] IS NOT NULL AND [CATEG_PROF] IS NOT NULL)
{filteredQuery}
UNION ALL
SELECT [TIME_PERIOD], [FREQ], [REF_AREA], [IND_TYPE], [ADJUSTMENT], [MEASURE], [ETA], [SESSO], [GRADO_ISTRUZ], [CATEG_PROF], [OBS_VALUE], [COMMENT], [CONF_STATUS], [OBS_PRE_BREAK], [OBS_STATUS], [DECIMALS], [NOTA_IT], [NOTA_EN], [BREAK],[BatchNumber]
FROM WILDCARDED
ORDER BY [BatchNumber]" :

$@";WITH WILDCARDED AS(
SELECT [TIME_PERIOD], [FREQ], [REPORTING_TYPE], [SERIES], [REF_AREA], [SEX], [STRING_TYPE], [STRING_MULTILANG_TYPE], [ALPHANUMERIC_TYPE], [ALPHANUMERIC_MULTILANG_TYPE], [BOOLEAN_TYPE], [XHTML_TYPE], [XHTML_MULTILANG_TYPE], [INTEGER_TYPE], [INTEGER_MULTILANG_TYPE], [DECIMAL_TYPE], [DECIMAL_MULTILANG_TYPE], [DATETIME_TYPE], [TIME_TYPE], [GREGORIANDAY_TYPE], [GREGORIAN_YEAR_TYPE], [GREGORIAN_YEARMONTH_TYPE], [CONTACT.CONTACT_NAME], [CONTACT.CONTACT_EMAIL], [CONTACT.CONTACT_PHONE], [CONTACT.CONTACT_ORGANISATION], [CODED_METADATA_FREQ]
, CAST((ROW_NUMBER() OVER(ORDER BY[LAST_UPDATED])) AS INT) AS [BatchNumber]
FROM [data].[VI_DeletedMetadataDataFlow_1_A]
WHERE([TIME_PERIOD] IS NULL OR [FREQ] IS NULL OR [REPORTING_TYPE] IS NULL OR [SERIES] IS NULL OR [REF_AREA] IS NULL OR [SEX] IS NULL)
{filteredQuery}
) 
SELECT [TIME_PERIOD], [FREQ], [REPORTING_TYPE], [SERIES], [REF_AREA], [SEX], [STRING_TYPE], [STRING_MULTILANG_TYPE], [ALPHANUMERIC_TYPE], [ALPHANUMERIC_MULTILANG_TYPE], [BOOLEAN_TYPE], [XHTML_TYPE], [XHTML_MULTILANG_TYPE], [INTEGER_TYPE], [INTEGER_MULTILANG_TYPE], [DECIMAL_TYPE], [DECIMAL_MULTILANG_TYPE], [DATETIME_TYPE], [TIME_TYPE], [GREGORIANDAY_TYPE], [GREGORIAN_YEAR_TYPE], [GREGORIAN_YEARMONTH_TYPE], [CONTACT.CONTACT_NAME], [CONTACT.CONTACT_EMAIL], [CONTACT.CONTACT_PHONE], [CONTACT.CONTACT_ORGANISATION], [CODED_METADATA_FREQ]
,CAST(ISNULL((SELECT MAX([BatchNumber]) FROM WILDCARDED), 0) + ROW_NUMBER() OVER (PARTITION BY [TIME_PERIOD], [FREQ], [REPORTING_TYPE], [SERIES], [REF_AREA], [SEX] ORDER BY [LAST_UPDATED]) AS INT) AS [BatchNumber]
FROM [data].[VI_DeletedMetadataDataFlow_1_A]
WHERE ([TIME_PERIOD] IS NOT NULL AND [FREQ] IS NOT NULL AND [REPORTING_TYPE] IS NOT NULL AND [SERIES] IS NOT NULL AND [REF_AREA] IS NOT NULL AND [SEX] IS NOT NULL)
{filteredQuery}
UNION ALL
SELECT [TIME_PERIOD], [FREQ], [REPORTING_TYPE], [SERIES], [REF_AREA], [SEX], [STRING_TYPE], [STRING_MULTILANG_TYPE], [ALPHANUMERIC_TYPE], [ALPHANUMERIC_MULTILANG_TYPE], [BOOLEAN_TYPE], [XHTML_TYPE], [XHTML_MULTILANG_TYPE], [INTEGER_TYPE], [INTEGER_MULTILANG_TYPE], [DECIMAL_TYPE], [DECIMAL_MULTILANG_TYPE], [DATETIME_TYPE], [TIME_TYPE], [GREGORIANDAY_TYPE], [GREGORIAN_YEAR_TYPE], [GREGORIAN_YEARMONTH_TYPE], [CONTACT.CONTACT_NAME], [CONTACT.CONTACT_EMAIL], [CONTACT.CONTACT_PHONE], [CONTACT.CONTACT_ORGANISATION], [CODED_METADATA_FREQ],[BatchNumber]
FROM WILDCARDED
ORDER BY [BatchNumber]";

            var sqlQuery = repository.BuildDeletedSqlQuery(
                dataQuery,
                dataflow,
                components,
                DbTableVersion.A,
                isDataQuery: isDataQuery,
                filterLastUpdated: filterLastUpdated
            );

            Assert.AreEqual(expectedSqlQuery, sqlQuery);
        }

        [Test]
        public async Task GetObservations()
        {
            var sdmxQuery       = GetQuery();
            var action = $"{(int)StagingRowActionEnum.Replace}";
            var batchNumber = "1";
            DbDataReader dbDataReader = new TestDbDataReader(new List<string[]>()
                    {
                        new [] { "1900-Q1", "Q", "066001", "1","1","1","2","1","1","1","1996.15416211418",null,null,null,null,null,null,null,null,null,null,null,null,null, batchNumber},
                        new [] { "2012", "Q", "066001", "1","1","1","2","1","1","1","1996.15416211418",null,null,null,null,null,null,null,null,null,null,null,null,null, batchNumber},
                        new [] { "2012", "Q", "066001", "1", "1", "1", "2", "1", "1", "1", "1996.15416211418", null, null, null, null, null, null, null, null, null, null, null, null, null, batchNumber }
                    });

            var dotStatDbMock = new Mock<SqlDotStatDb>(Configuration.SpacesInternal.FirstOrDefault(), DbUp.DatabaseVersion.DataDbVersion) { CallBase = true };
            dotStatDbMock.Setup(m =>
                m.ExecuteReaderSqlWithParamsAsync(
                    It.IsAny<string>(),
                    It.IsAny<CancellationToken>(),
                    It.IsAny<CommandBehavior>(),
                    It.IsAny<bool>(),
                    It.IsAny<DbParameter[]>()))
                .Returns(() => Task.FromResult(dbDataReader));


            dotStatDbMock.Setup(m => m.DataSchema).Returns("data");
            dotStatDbMock.Setup(m => m.ManagementSchema).Returns("management");

            var repository = new SqlObservationRepository(dotStatDbMock.Object, _testConfiguration);

            var observations = repository.GetObservations(
                sdmxQuery,
                _dataDataflow,
                DbTableVersion.A,
                isDataQuery: true,
                CancellationToken
            );

            Assert.IsNotNull(observations);

            var listOfObservations = await observations.ToListAsync();
            var obs = listOfObservations.FirstOrDefault().Observation;

            Assert.AreEqual("1900-Q1", obs.ObsTime);
            Assert.AreEqual(9, obs.SeriesKey.Key.Count);

            Assert.AreEqual("FREQ", obs.SeriesKey.Key[0].Concept);
            Assert.AreEqual("Q", obs.SeriesKey.Key[0].Code);

            Assert.AreEqual("REF_AREA", obs.SeriesKey.Key[1].Concept);
            Assert.AreEqual("066001", obs.SeriesKey.Key[1].Code);

            Assert.AreEqual(3, listOfObservations.Count);
        }

        [Test]
        public async Task GetMetadataObservations()
        {
            var dataQueryFiltered = new RESTDataQueryCore("data/UNSD,DF_JENS_DAILY,1.0/all");
            var sdmxQuery = new DataQueryImpl(dataQueryFiltered, new TestMappingStoreDataAccess("sdmx/CsvV2.xml").GetRetrievalManager(""));

            var action = $"{(int)StagingRowActionEnum.Replace}";
            var batchNumber = "1";

            DbDataReader dbDataReader = new TestDbDataReader(new List<string[]>()
                    {
                        new [] { "1900-Q1", "Q", "066001", "1","1","1","2","1","1","1","1996.15416211418",null,null,null,null,null,null,null,null,null,null,null,null,null, null, null, null, batchNumber},
                        new [] { "2012", "Q", "066001", "1","1","1","2","1","1","1","1996.15416211418",null,null,null,null,null,null,null,null,null,null,null,null,null, null, null, null, batchNumber},
                        new [] { "2012", "Q", "066001", "1", "1", "1", "2", "1", "1", "1", "1996.15416211418", null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, batchNumber }
                    });

            var dotStatDbMock = new Mock<SqlDotStatDb>(Configuration.SpacesInternal.FirstOrDefault(), DbUp.DatabaseVersion.DataDbVersion) { CallBase = true };
            dotStatDbMock.Setup(m =>
                m.ExecuteReaderSqlWithParamsAsync(
                    It.IsAny<string>(),
                    It.IsAny<CancellationToken>(),
                    It.IsAny<CommandBehavior>(),
                    It.IsAny<bool>(),
                    It.IsAny<DbParameter[]>()))
                .Returns(() => Task.FromResult(dbDataReader));


            dotStatDbMock.Setup(m => m.DataSchema).Returns("data");
            dotStatDbMock.Setup(m => m.ManagementSchema).Returns("management");

            var repository = new SqlObservationRepository(dotStatDbMock.Object, _testConfiguration);

            var observations = repository.GetObservations(
                sdmxQuery,
                _metadataDataflow,
                DbTableVersion.A,
                isDataQuery: false,
                CancellationToken
            );

            Assert.IsNotNull(observations);

            var listOfObservations = await observations.ToListAsync();
            var obs = listOfObservations.FirstOrDefault().Observation;

            Assert.AreEqual("1900-Q1", obs.ObsTime);
            Assert.AreEqual(5, obs.SeriesKey.Key.Count);

            Assert.AreEqual("FREQ", obs.SeriesKey.Key[0].Concept);
            Assert.AreEqual("Q", obs.SeriesKey.Key[0].Code);

            Assert.AreEqual("REPORTING_TYPE", obs.SeriesKey.Key[1].Concept);
            Assert.AreEqual("066001", obs.SeriesKey.Key[1].Code);

            Assert.AreEqual(3, listOfObservations.Count);
        }

        [Test]
        public async Task GetMetadataObservationsWithSwitchedOffDimensions()
        {
            var dataQueryFiltered = new RESTDataQueryCore("data/UNSD,DF_JENS_DAILY,1.0/all");
            var sdmxQuery = new DataQueryImpl(dataQueryFiltered, new TestMappingStoreDataAccess("sdmx/CsvV2.xml").GetRetrievalManager(""));
            var batchNumber = "1";

            DbDataReader dbDataReader = new TestDbDataReader(new List<string[]>()
                    {
                        new [] { null, null, "066001", "1","1","1","2","1","1","1","1996.15416211418",null,null,null,null,null,null,null,null,null,null,null,null,null, null, null, null, batchNumber},
                        new [] { "2012", "Q", "066001", null,"1","1","2","1","1","1","1996.15416211418",null,null,null,null,null,null,null,null,null,null,null,null,null, null, null, null, batchNumber},
                        new [] { null, null, null, null, null, null,"2","1","1","1","1996.15416211418",null,null,null,null,null,null,null,null,null,null,null,null,null, null, null, null, batchNumber}
                    });

            var dotStatDbMock = new Mock<SqlDotStatDb>(Configuration.SpacesInternal.FirstOrDefault(), DbUp.DatabaseVersion.DataDbVersion) { CallBase = true };
            dotStatDbMock.Setup(m =>
                m.ExecuteReaderSqlWithParamsAsync(
                    It.IsAny<string>(),
                    It.IsAny<CancellationToken>(),
                    It.IsAny<CommandBehavior>(),
                    It.IsAny<bool>(),
                    It.IsAny<DbParameter[]>()))
                .Returns(() => Task.FromResult(dbDataReader));


            dotStatDbMock.Setup(m => m.DataSchema).Returns("data");
            dotStatDbMock.Setup(m => m.ManagementSchema).Returns("management");

            var repository = new SqlObservationRepository(dotStatDbMock.Object, _testConfiguration);

            var observations = repository.GetObservations(
                sdmxQuery,
                _metadataDataflow,
                DbTableVersion.A,
                isDataQuery: false,
                CancellationToken);

            Assert.IsNotNull(observations);

            var listOfObservations = await observations.ToListAsync();

            Assert.AreEqual(3, listOfObservations.Count);

            var observation = listOfObservations[0].Observation;

            Assert.AreEqual(DbExtensions.DimensionSwitchedOff, observation.ObsTime);
            Assert.AreEqual(DbExtensions.DimensionSwitchedOff, observation.SeriesKey.Key[0].Code);
            Assert.AreEqual("066001", observation.SeriesKey.Key[1].Code);
            Assert.AreEqual("1", observation.SeriesKey.Key[2].Code);

            observation = listOfObservations[1].Observation;

            Assert.AreEqual("2012", observation.ObsTime);
            Assert.AreEqual("Q", observation.SeriesKey.Key[0].Code);
            Assert.AreEqual("066001", observation.SeriesKey.Key[1].Code);
            Assert.AreEqual(DbExtensions.DimensionSwitchedOff, observation.SeriesKey.Key[2].Code);

            observation = listOfObservations[2].Observation;

            Assert.AreEqual(DbExtensions.DimensionSwitchedOff, observation.ObsTime);
            Assert.AreEqual(DbExtensions.DimensionSwitchedOff, observation.SeriesKey.Key[0].Code);
            Assert.AreEqual(DbExtensions.DimensionSwitchedOff, observation.SeriesKey.Key[1].Code);
            Assert.AreEqual(DbExtensions.DimensionSwitchedOff, observation.SeriesKey.Key[2].Code);
        }

        [Test]
        public async Task GetObservationCount()
        {
            long obsCount = 100;
            var dotStatDbMock = new Mock<SqlDotStatDb>(Configuration.SpacesInternal.FirstOrDefault(), DbUp.DatabaseVersion.DataDbVersion) { CallBase = true };
            dotStatDbMock.Setup(m =>
                m.ExecuteScalarSqlAsync(
                    It.IsAny<string>(),
                    It.IsAny<CancellationToken>()))
                .Returns(() => Task.FromResult((object)obsCount));

            dotStatDbMock.Setup(m => m.DataSchema).Returns("data");
            dotStatDbMock.Setup(m => m.ManagementSchema).Returns("management");

            var repository = new SqlObservationRepository(dotStatDbMock.Object, _testConfiguration);

            var sdmxQuery = GetQuery();
            var dataFlowWhereClause = Predicate.BuildWhereForObservationLevel(sdmxQuery, _dataDataflow, _codeTranslator, useExternalValues: true, useExternalColumnNames: false, filterLastUpdated: false);

            var dbObsCount = await repository.GetObservationCount(
                _dataDataflow,
                DbTableVersion.A,
                dataFlowWhereClause,
                CancellationToken
            );

            Assert.AreEqual(obsCount, dbObsCount);
        }

    }
}