﻿using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;

namespace DotStat.DB
{
    public static class AsyncEnumerableExtensions
    {
        public static async IAsyncEnumerable<List<T>> Buffer<T>(this IAsyncEnumerable<T> source, int size, [EnumeratorCancellation] CancellationToken cancellationToken = default)
        {
            List<T> buffer = new List<T>(size);

            await foreach (T item in source.WithCancellation(cancellationToken).ConfigureAwait(false))
            {
                buffer.Add(item);

                if (buffer.Count >= size)
                {
                    yield return buffer;
                    buffer.Clear();
                }
            }

            if (buffer.Count > 0)
            {
                yield return buffer;
            }
        }
    }

}