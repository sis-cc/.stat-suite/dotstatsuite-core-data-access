﻿using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("DotStat.Test.DataAccess")]
[assembly: InternalsVisibleTo("DotStat.Test.DataAccess.MariaDb")]