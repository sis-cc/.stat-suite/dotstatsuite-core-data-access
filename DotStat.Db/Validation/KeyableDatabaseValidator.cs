﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DotStat.Db.DB;
using DotStat.Domain;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Model.Data;
using Attribute = DotStat.Domain.Attribute;
using DotStat.Db.Util;
using System.Threading;
using DotStat.Common.Configuration.Interfaces;

namespace DotStat.Db.Validation
{
    public abstract class KeyableDatabaseValidator : IKeyableDatabaseValidator
    {
        protected int MaxErrorCount;
        protected List<IValidationError> Errors;
        private bool _isValid;

        protected KeyableDatabaseValidator(IGeneralConfiguration generalConfiguration)
        {
            if (generalConfiguration is null)
            {
                throw new ArgumentNullException(nameof(generalConfiguration));
            }

            MaxErrorCount = generalConfiguration.MaxTransferErrorAmount;
            Errors = new List<IValidationError>();
        }

        public async Task<bool> Validate(IDotStatDb dotStatDataDb, ICodeTranslator translator, Dataflow dataflow, List<Attribute> reportedAttributes, DbTableVersion tableVersion, int? maxErrorCount, CancellationToken cancellationToken)
        {
            if(dotStatDataDb==null) throw new ArgumentNullException(nameof(dotStatDataDb));
            if(translator == null) throw new ArgumentNullException(nameof(translator));
            if(dataflow ==null) throw new ArgumentNullException(nameof(dataflow));

            var seriesAttributes = dataflow.Dsd.Attributes
                .Where(a => reportedAttributes.Any(r => r.Code.Equals(a.Code, StringComparison.InvariantCultureIgnoreCase)))
                .Where(a =>
                    a.Base.AttachmentLevel == AttributeAttachmentLevel.Group ||
                    a.Base.AttachmentLevel == AttributeAttachmentLevel.DimensionGroup)
                .ToList();

            var nonDatasetAttributes = dataflow.Dsd.Attributes
                .Where(a => reportedAttributes.Any(r => r.Code.Equals(a.Code, StringComparison.InvariantCultureIgnoreCase)))
                .Where(a =>
                    a.Base.AttachmentLevel != AttributeAttachmentLevel.DataSet &&
                    a.Base.AttachmentLevel != AttributeAttachmentLevel.Null)
                .ToList();

            var mandatoryNonDatasetAttributes = nonDatasetAttributes.Where(a => a.Base.Mandatory).ToList();

            MaxErrorCount = maxErrorCount ?? MaxErrorCount;
            Errors.Clear();

            _isValid = true;

            if (nonDatasetAttributes.Count == 0)
                return _isValid;

            //group attributes referencing the same dimensions
            var attributesWithSameDimensionReferences =
                nonDatasetAttributes.GetAttributeGroupsOfSameDimensionRelations(dataflow.Dsd);

            if (!await CheckForDimensionGroupAttributesWithMultipleValuesInStaging(dotStatDataDb, translator, dataflow, attributesWithSameDimensionReferences, seriesAttributes, cancellationToken))
                return false;

            if (mandatoryNonDatasetAttributes.Count > 0 && !IsMaxErrorLimitReached())
            {
                if (!await CheckForMissingMandatoryNonDatasetAttributesValuesInDatabase(dotStatDataDb, translator, dataflow, tableVersion, attributesWithSameDimensionReferences, mandatoryNonDatasetAttributes, cancellationToken))
                    return false;
            }

            var seriesDims = dataflow.Dsd.Dimensions
                .Where(x => !x.Base.TimeDimension)
                .Select(d => d.Code);

            var groupAttributes = seriesAttributes
                .Where(a => a.Base.DimensionReferences != null &&
                            new HashSet<string>(a.Base.DimensionReferences)
                                .IsSubsetOf(dataflow.Dsd.Dimensions.Select(d => d.Code)) &&
                            !new HashSet<string>(a.Base.DimensionReferences)
                                .SetEquals(seriesDims)).ToList();
                
            if (groupAttributes.Count > 0 && !IsMaxErrorLimitReached())
            {
                if (!await CheckForGroupAttributesWithMultipleValuesInDatabase(dotStatDataDb, translator, dataflow, tableVersion, attributesWithSameDimensionReferences, groupAttributes, cancellationToken))
                    _isValid = false;
            }

            return _isValid;
        }

        protected abstract Task<bool> CheckForMissingMandatoryNonDatasetAttributesValuesInDatabase(IDotStatDb dotStatDataDb, ICodeTranslator translator, Dataflow dataflow, DbTableVersion tableVersion, Dictionary<string, (List<Dimension>, List<Attribute>)> attributesWithSameDimensionReferences, List<Attribute> mandatoryNonDatasetAttributes, CancellationToken cancellationToken);
        protected abstract Task<bool> CheckForDimensionGroupAttributesWithMultipleValuesInStaging(IDotStatDb dotStatDataDb, ICodeTranslator translator, Dataflow dataflow, Dictionary<string, (List<Dimension>, List<Attribute>)> attributesWithSameDimensionReferences, List<Attribute> seriesAttributes, CancellationToken cancellationToken);
        protected abstract Task<bool> CheckForGroupAttributesWithMultipleValuesInDatabase(IDotStatDb dotStatDataDb, ICodeTranslator translator, Dataflow dataflow, DbTableVersion tableVersion, Dictionary<string, (List<Dimension>, List<Attribute>)> attributesWithSameDimensionReferences, List<Attribute> groupAttributes, CancellationToken cancellationToken);

        protected void AddError(ValidationErrorType errorType, string coordinate, IKeyable keyable = null, IKeyValue keyValue = null,
            params string[] argument)
        {
            _isValid = false;

            Errors.Add(new KeyableError()
            {
                Type = errorType,
                Row = -1,
                Coordinate = string.IsNullOrWhiteSpace(coordinate) ? keyable?.GetFullKey(true) : coordinate,
                Value = keyValue == null ? null : $"{keyValue.Concept}:{keyValue.Code}",
                Argument = argument
            });
        }

        public List<IValidationError> GetErrors()
        {
            return Errors;
        }

        public string GetErrorsMessage()
        {
            var sb = new StringBuilder();

            foreach (var error in Errors)
                sb.AppendLine(error.ToString());

            return sb.ToString();
        }
        protected bool IsMaxErrorLimitReached()
        {
            return MaxErrorCount != 0 && Errors.Count >= MaxErrorCount;
        }
    }
}