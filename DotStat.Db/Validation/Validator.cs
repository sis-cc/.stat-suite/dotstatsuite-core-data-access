﻿using DotStat.Common.Configuration.Interfaces;
using Org.Sdmxsource.Sdmx.Api.Model.Data;
using System;
using System.Collections.Generic;
using System.Text;
using DotStat.Domain;

namespace DotStat.Db.Validation
{
    public abstract class Validator
    {
        protected HashSet<string> Series { get; }
        protected Dictionary<int, HashSet<string>> Duplicates { get; }
        protected List<IValidationError> Errors { get; }
        protected bool FullValidation { get; }
        protected int MaxTransferErrorAmount { get; }
        protected bool IsMaxErrorLimitReached => MaxTransferErrorAmount > 0 && Errors.Count >= MaxTransferErrorAmount;

        protected Validator(IGeneralConfiguration configuration, bool fullValidation)
        {
            Errors = new List<IValidationError>(Math.Max(configuration.MaxTransferErrorAmount, 10));
            FullValidation = fullValidation;
            Series = new HashSet<string>();
            Duplicates = new Dictionary<int, HashSet<string>>();
            MaxTransferErrorAmount =
                configuration.MaxTransferErrorAmount < 0 ? 0 : configuration.MaxTransferErrorAmount;
        }

        public abstract bool ValidateObservation(IObservation observation, int batchNumber, int row = 0, StagingRowActionEnum action = StagingRowActionEnum.Merge);

        public bool AddError(ValidationErrorType errorType, int row, IObservation observation = null, params string[] argument)
        {
            return this.AddError(new ObservationError()
                {
                    Type = errorType,
                    Coordinate = observation?.GetFullKey(true, observation.SeriesKey.DataStructure.TimeDimension?.Id),
                    Value = observation?.ObservationValue,
                    Argument = argument
                },
                row
            );
        }

        public bool AddError(ValidationErrorType errorType, int row, IObservation observation, IKeyValue keyValue, params string[] argument)
        {
            return this.AddError(new ObservationError()
                {
                    Type = errorType,
                    Coordinate = observation.GetFullKey(true, observation.SeriesKey.DataStructure.TimeDimension?.Id),
                    Value = keyValue.Code,
                    Argument = argument
                },
                row
            );
        }

        public bool AddError(IValidationError error, int row)
        {
            if (!IsMaxErrorLimitReached)
            {
                error.Row = row;
                Errors.Add(error);
            }

            return false;
        }

        public List<IValidationError> GetErrors()
        {
            return Errors;
        }

        public string GetErrorsMessage()
        {
            var sb = new StringBuilder();

            foreach (var error in Errors)
            {
                sb.AppendLine(error.ToString());
            }

            return sb.ToString();
        }
    }
}