﻿using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.ComTypes;
using DotStat.Db.Validation;

namespace DotStat.DB.Validation.TypeValidation
{
    public class ReportingTimePeriodValidator : ComplexSdmxTypeValidator
    {
        public ReportingTimePeriodValidator(bool allowNullValue = true) : base(
            new ISdmxTypeValidator[]
            {
                new ReportingYearValidator(allowNullValue), 
                new ReportingSemesterValidator(allowNullValue), 
                new ReportingTrimesterValidator(allowNullValue),
                new ReportingQuarterValidator(allowNullValue),
                new ReportingMonthValidator(allowNullValue),
                new ReportingWeekValidator(allowNullValue), 
                new ReportingDayValidator()
            }, ComplexValidationMode.Any, allowNullValue)
        {
        }

        public override bool IsValid(string observationValue)
        {
            if (base.IsValid(observationValue))
            {
                return true;
            }

            ValidationError = ValidationErrorType.InvalidValueFormatNotReportingTimePeriod;

            return false;
        }
    }
}
