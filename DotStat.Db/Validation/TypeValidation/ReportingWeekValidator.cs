﻿using DotStat.Db.Validation;

namespace DotStat.DB.Validation.TypeValidation
{
    public class ReportingWeekValidator : PatternValidator
    {
        private const string ValidationExpression =
            @"^\d{4}\-W(0[1-9]|[1-4][0-9]|5[0-3])(Z|(\+|\-)((0[0-9]|1[0-3]):[0-5][0-9]|14:00))?$";

        protected override ValidationErrorType TypeSpecificValidationError => ValidationErrorType.InvalidValueFormatNotReportingWeek;

        public ReportingWeekValidator(bool allowNullValue = true) : base (ValidationExpression, allowNullValue)
        {
        }
    }
}
