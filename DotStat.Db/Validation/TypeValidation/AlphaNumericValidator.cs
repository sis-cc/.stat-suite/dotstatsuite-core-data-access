﻿using DotStat.Db.Validation;

namespace DotStat.DB.Validation.TypeValidation
{
    public class AlphaNumericValidator : PatternValidator
    {
        private const string ValidationExpression = @"^[a-zA-Z0-9]+$";

        protected override ValidationErrorType TypeSpecificValidationError => ValidationErrorType.InvalidValueFormatNotAlphaNumeric;

        public AlphaNumericValidator(bool allowNullValue = true) : base (ValidationExpression, allowNullValue)
        {
        }
    }
}
