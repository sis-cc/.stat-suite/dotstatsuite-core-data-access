﻿namespace DotStat.DB.Validation.TypeValidation
{
    public class BooleanValidator : SimpleSdmxTypeValidator
    {
        public BooleanValidator(bool allowNullValue = true) : base(allowNullValue)
        {
        }

        public override bool IsValid(string observationValue)
        {
            ValidationError = Db.Validation.ValidationErrorType.Undefined;

            if ((AllowNullValue && string.IsNullOrEmpty(observationValue)) || 
                observationValue.Equals("0") ||
                observationValue.Equals("1") || 
                observationValue.Equals("true") || 
                observationValue.Equals("false"))
            {
                return true;
            }

            ValidationError = Db.Validation.ValidationErrorType.InvalidValueFormatNotBoolean;

            return false;
        }
    }
}
