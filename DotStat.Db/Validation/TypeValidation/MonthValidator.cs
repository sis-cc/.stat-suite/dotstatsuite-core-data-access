﻿using DotStat.Db.Validation;

namespace DotStat.DB.Validation.TypeValidation
{
    public class MonthValidator : PatternValidator
    {
        private const string ValidationExpression = @"^--(0[1-9]|1[0-2])(Z|(\+|-)((0[0-9]|1[0-3]):[0-5][0-9]|14:00))?$";

        protected override ValidationErrorType TypeSpecificValidationError => ValidationErrorType.InvalidValueFormatNotMonth;

        public MonthValidator(bool allowNullValue = true) : base (ValidationExpression, allowNullValue)
        {
        }
    }
}
