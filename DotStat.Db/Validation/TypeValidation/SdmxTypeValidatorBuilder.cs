﻿using DotStat.Common.Exceptions;
using DotStat.Common.Localization;
using DotStat.Domain;
using Org.Sdmxsource.Sdmx.Api.Constants;
using System.Collections.Generic;
using System.Globalization;

namespace DotStat.DB.Validation.TypeValidation
{
    public class SdmxTypeValidatorBuilder
    {
        public IEnumerable<ISdmxTypeValidator> BuildFromPrimaryMeasure(PrimaryMeasure primaryMeasure, bool supportsIntentionallyMissingValues, CultureInfo cultureInfo)
        {
            if (primaryMeasure.RepresentationReference != null)
            {
                yield return new EnumeratedValidator(primaryMeasure);
            }

            if (primaryMeasure.TextFormat != null)
            {
                var textFormat = primaryMeasure.TextFormat;

                var minValue = textFormat.MinValue;
                var maxValue = textFormat.MaxValue;

                var typeSpecificValidator = GetTypeSpecificValidator(textFormat.TextType.EnumType, minValue, maxValue, supportsIntentionallyMissingValues, cultureInfo);

                if (typeSpecificValidator == null && (TextEnumType) textFormat.TextType?.EnumType != TextEnumType.String )
                {
                    throw new DotStatException(string.Format(
                        LocalizationRepository.GetLocalisedResource(Localization.ResourceId.InvalidValueFormatNotSupported),
                        textFormat.TextType.EnumType, 
                        primaryMeasure.FullId));
                }

                if (typeSpecificValidator != null)
                {
                    yield return typeSpecificValidator;
                }

                var minLength = textFormat.MinLength ?? 0;
                var maxLength = textFormat.MaxLength ?? 0;

                if (minLength > 0 || maxLength > 0)
                {
                    yield return new MinMaxLengthValidator(minLength, maxLength);
                }

                if (!string.IsNullOrWhiteSpace(textFormat.Pattern))
                {
                    yield return new PatternValidator(textFormat.Pattern);
                }
            }
        }

        private static ISdmxTypeValidator GetTypeSpecificValidator(TextEnumType enumType, decimal? minValue, decimal? maxValue, bool supportsIntentionallyMissingValues, CultureInfo cultureInfo)
        {
            switch (enumType)
            {
                case TextEnumType.Alpha:
                    return new AlphaValidator();
                case TextEnumType.Alphanumeric:
                    return new AlphaNumericValidator();
                case TextEnumType.Numeric:
                    return new NumericValidator();
                case TextEnumType.BigInteger:
                    return new BigIntegerValidator(minValue, maxValue);
                case TextEnumType.Integer:
                    return new IntegerValidator(minValue, maxValue);
                case TextEnumType.Long:
                    return new LongValidator(minValue, maxValue);
                case TextEnumType.Short:
                    return new ShortValidator(minValue, maxValue);
                case TextEnumType.Decimal:
                    return new DecimalValidator(minValue, maxValue);
                case TextEnumType.Float:
                    return new FloatValidator(minValue, maxValue, supportsIntentionallyMissingValues, cultureInfo);
                case TextEnumType.Double:
                    return new DoubleValidator(minValue, maxValue, supportsIntentionallyMissingValues, cultureInfo);
                case TextEnumType.Boolean:
                    return new BooleanValidator();
                case TextEnumType.Uri:
                    return new UriValidator();
                case TextEnumType.Count:
                    return new CountValidator(minValue, maxValue);
                case TextEnumType.ObservationalTimePeriod:
                    return new ObservationalTimePeriodValidator();
                case TextEnumType.StandardTimePeriod:
                    return new StandardTimePeriodValidator();
                case TextEnumType.BasicTimePeriod:
                    return new BasicTimePeriodValidator();
                case TextEnumType.GregorianTimePeriod:
                    return new GregorianTimePeriodValidator();
                case TextEnumType.GregorianYear:
                    return new GregorianYearValidator();
                case TextEnumType.GregorianYearMonth:
                    return new GregorianYearMonthValidator();
                case TextEnumType.GregorianDay:
                    return new GregorianDayValidator();
                case TextEnumType.ReportingTimePeriod:
                    return new ReportingTimePeriodValidator();
                case TextEnumType.ReportingYear:
                    return new ReportingYearValidator();
                case TextEnumType.ReportingSemester:
                    return new ReportingSemesterValidator();
                case TextEnumType.ReportingTrimester:
                    return new ReportingTrimesterValidator();
                case TextEnumType.ReportingQuarter:
                    return new ReportingQuarterValidator();
                case TextEnumType.ReportingMonth:
                    return new ReportingMonthValidator();
                case TextEnumType.ReportingWeek:
                    return new ReportingWeekValidator();
                case TextEnumType.ReportingDay:
                    return new ReportingDayValidator();
                case TextEnumType.DateTime:
                    return new DateTimeValidator();
                case TextEnumType.TimesRange:
                    return new TimeRangeValidator();
                case TextEnumType.Month:
                    return new MonthValidator();
                case TextEnumType.MonthDay:
                    return new MonthDayValidator();
                case TextEnumType.Day:
                    return new DayValidator();
                case TextEnumType.Time:
                    return new TimeValidator();
                case TextEnumType.Duration:
                    return new DurationValidator();
            }

            return null;
        }
    }
}
