﻿using System;
using System.Collections.Generic;
using System.Linq;
using Attribute = DotStat.Domain.Attribute;
using Dataflow = DotStat.Domain.Dataflow;
using System.Data;
using System.Threading;
using System.Threading.Tasks;
using DotStat.Common.Configuration.Interfaces;
using DotStat.Db.DB;
using DotStat.Domain;
using System.Data.Common;
using Microsoft.Data.SqlClient;

namespace DotStat.Db.Validation.SqlServer
{
    public class SqlDatasetAttributeDatabaseValidator : DatasetAttributeDatabaseValidator, ISqlDatasetAttributeDatabaseValidator
    {
        public SqlDatasetAttributeDatabaseValidator(IGeneralConfiguration generalConfiguration) : base(generalConfiguration)
        {
        }

        protected override async Task CheckAttributeValuesInDatabase(IDotStatDb dotStatDataDb, List<Attribute> attributeList, DbTableVersion tableVersion, bool fullValidation, CancellationToken cancellationToken)
        {
            if (attributeList.Count == 0)
                return;
            var attrTableName = Dataflow.Dsd.SqlDsdAttrTable((char)tableVersion);
            //TODO: validate proper handling 
            if (!await dotStatDataDb.TableExists(attrTableName, cancellationToken))
            {
                return;
            }

            var sql = $@"
                        SELECT {string.Join(",", attributeList.Select(m => m.SqlColumn()))}
                            FROM [{dotStatDataDb.DataSchema}].[{attrTableName}] 
                            WHERE [DF_ID] = @DfId";

            await using (var reader = await dotStatDataDb.ExecuteReaderSqlWithParamsAsync(
                sql, cancellationToken, CommandBehavior.SingleRow, parameters: new SqlParameter("DfId", SqlDbType.Int) { Value = Dataflow.DbId }))
            {
                if (await reader.ReadAsync(cancellationToken))
                {
                    // Reduce list of mandatory attributes with the ones having value in the database for the current dataflow
                    var i = 0;
                    foreach (var attr in new List<Attribute>(attributeList))
                    {
                        var val = reader[i++];
                        if (val is not DBNull or null)
                            attributeList.Remove(attr);
                    }
                }
            }
        }

        protected override async Task<bool> CheckIfAllObservationsWillBeDeleted(IDotStatDb dotStatDataDb, Dataflow dataflow, DbTableVersion tableVersion, CancellationToken cancellationToken)
        {
            var stagingTableName = dataflow.Dsd.SqlStagingTable(); ;
            var filtTableName = dataflow.Dsd.SqlFilterTable();
            var factTableName = dataflow.Dsd.SqlFactTable((char)tableVersion);

            var noTimeDimensions = dataflow.Dimensions.Where(d => !d.Base.TimeDimension).ToList();
            var joinStatements = noTimeDimensions
                .Select(a => string.Format(" Filt.{0} = ISNULL(Import.{0}, Filt.{0}) ", a.SqlColumn()))
                .ToList();

            if (dataflow.Dsd.TimeDimension != null)
            {
                joinStatements.Add(string.Format(" Fact.{0} = ISNULL(Import.{0}, Fact.{0}) ", DbExtensions.SqlPeriodStart()));
                joinStatements.Add(string.Format(" Fact.{0} = ISNULL(Import.{0}, Fact.{0}) ", DbExtensions.SqlPeriodEnd()));
            }

            var sqlCommand = $@"
                    SELECT top 1 1
                    FROM [{dotStatDataDb.DataSchema}].[{filtTableName}] Filt
                    INNER JOIN [{dotStatDataDb.DataSchema}].[{factTableName}] Fact ON Filt.SID = Filt.SID
                    LEFT JOIN [{dotStatDataDb.DataSchema}].[{stagingTableName}] Import ON
                    Import.real_action_fact = @Delete
                    AND {string.Join("AND", joinStatements)}
                    WHERE Import.batch_number is null
                ";

            //Add Sql parameters
            var sqlParameters = new DbParameter[]
            {
                new SqlParameter("Delete", SqlDbType.Int) { Value = (int)StagingRowActionEnum.Delete },
            };

            await using var dr = await dotStatDataDb.ExecuteReaderSqlWithParamsAsync(sqlCommand, cancellationToken, commandBehavior: CommandBehavior.SingleRow, parameters: sqlParameters);
            
            return !await dr.ReadAsync(cancellationToken);
        }
    }
}