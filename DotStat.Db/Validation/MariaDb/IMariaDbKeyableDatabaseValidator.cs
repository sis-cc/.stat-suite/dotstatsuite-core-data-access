﻿using DotStat.Db.Validation;

namespace DotStat.Db.Validation.MariaDb
{
    public interface IMariaDbKeyableDatabaseValidator : IKeyableDatabaseValidator
    {

    }
}
