using DotStat.Common.Configuration.Interfaces;
using DotStat.Db.DB;
using DotStat.Domain;
using MySqlConnector;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Attribute = DotStat.Domain.Attribute;


namespace DotStat.Db.Validation.MariaDb
{
    public class MariaDbDatasetAttributeDatabaseValidator : DatasetAttributeDatabaseValidator, IMariaDbDatasetAttributeDatabaseValidator
    {
        public MariaDbDatasetAttributeDatabaseValidator(IGeneralConfiguration generalConfiguration) : base(generalConfiguration)
        {
        }

        protected override async Task CheckAttributeValuesInDatabase(IDotStatDb dotStatDataDb, List<Attribute> attributeList, DbTableVersion tableVersion, bool fullValidation, CancellationToken cancellationToken)
        {
            if (attributeList.Count == 0)
                return;
            var attrTableName = Dataflow.Dsd.MariaDbDsdAttrTable((char) tableVersion);
            if (!await dotStatDataDb.TableExists($"{dotStatDataDb.DataSchema}_{attrTableName}", cancellationToken))
            {
                return;
            }

            var sql = $@"
                        SELECT {string.Join(",", attributeList.Select(m => m.MariaDbColumn()))}
                            FROM {dotStatDataDb.DataSchema}_{attrTableName}
                            WHERE DF_ID = @DfId";

            await using (var reader = await dotStatDataDb.ExecuteReaderSqlWithParamsAsync(
                sql, cancellationToken, CommandBehavior.SingleRow, parameters: new MySqlParameter("DfId", MySqlDbType.Int32) { Value = Dataflow.DbId }))
            {
                if (await reader.ReadAsync(cancellationToken))
                {
                    // Reduce list of mandatory attributes with the ones having value in the database for the current dataflow
                    var i = 0;
                    foreach (var attr in new List<Attribute>(attributeList))
                    {
                        var val = reader[i++];
                        if (val is not DBNull or null)
                            attributeList.Remove(attr);
                    }

                }
            }
        }

        protected override async Task<bool> CheckIfAllObservationsWillBeDeleted(IDotStatDb dotStatDataDb, Dataflow dataflow, DbTableVersion tableVersion, CancellationToken cancellationToken)
        {
            var stagingTableName = dataflow.Dsd.MariaDbStagingTable(); ;
            var filtTableName = dataflow.Dsd.MariaDbFilterTable();
            var factTableName = dataflow.Dsd.MariaDbFactTable((char)tableVersion);

            var noTimeDimensions = dataflow.Dimensions.Where(d => !d.Base.TimeDimension).ToList();
            var joinStatements = noTimeDimensions
                .Select(a => string.Format(" Filt.{0} = IFNULL(Import.{0}, Filt.{0}) ", a.SqlColumn()))
                .ToList();

            if (dataflow.Dsd.TimeDimension != null)
            {
                joinStatements.Add(string.Format(" Fact.{0} = IFNULL(Import.{0}, Fact.{0}) ", DbExtensions.SqlPeriodStart()));
                joinStatements.Add(string.Format(" Fact.{0} = IFNULL(Import.{0}, Fact.{0}) ", DbExtensions.SqlPeriodEnd()));
            }

            var sqlCommand = $@"
                    SELECT 1
                    FROM {dotStatDataDb.DataSchema}_{filtTableName} Filt
                    INNER JOIN {dotStatDataDb.DataSchema}_{factTableName} Fact ON Filt.SID = Filt.SID
                    LEFT JOIN {dotStatDataDb.DataSchema}_{stagingTableName} Import ON
                    Import.REAL_ACTION_FACT = @Delete
                    AND {string.Join("AND", joinStatements)}
                    WHERE Import.BATCH_NUMBER is null
                    LIMIT 1
                ";

            //Add parameters
            var sqlParameters = new DbParameter[]
            {
                new MySqlParameter("Delete", SqlDbType.Int) { Value = (int)StagingRowActionEnum.Delete },
            };

            await using var dr = await dotStatDataDb.ExecuteReaderSqlWithParamsAsync(sqlCommand, cancellationToken, commandBehavior: CommandBehavior.SingleRow, parameters: sqlParameters);

            return !await dr.ReadAsync(cancellationToken);
        }
    }
}