﻿using System;
using System.Collections.Generic;
using System.Data.Common;

using System.Threading;
using System.Threading.Tasks;
using Dapper;
using DotStat.Common.Model;
using Microsoft.Data.SqlClient;

namespace DotStat.DB.Dapper
{
    public abstract class DapperBaseRepository
    {
        protected abstract string ConnectionString { get; }

        protected virtual DbConnection GetConnection(bool readOnly, bool open = true)
        {
            var sqlConnectionStringBuilder = new SqlConnectionStringBuilder
            {
                ConnectionString = ConnectionString,
                ApplicationIntent = readOnly ? ApplicationIntent.ReadOnly : ApplicationIntent.ReadWrite
            };
            var connection = new LoggedConnection(new SqlConnection(sqlConnectionStringBuilder.ConnectionString));
            if (open) connection.Open();
            return connection;
        }

        protected virtual async Task<DbConnection> GetConnectionAsync(bool readOnly, CancellationToken cancellationToken, bool open = true)
        {
            var sqlConnectionStringBuilder = new SqlConnectionStringBuilder
            {
                ConnectionString = ConnectionString,
                ApplicationIntent = readOnly ? ApplicationIntent.ReadOnly : ApplicationIntent.ReadWrite
            };
            var connection = new LoggedConnection(new SqlConnection(sqlConnectionStringBuilder.ConnectionString));
            if (open) await connection.OpenAsync(cancellationToken);
            return connection;
        }
    }

    public abstract class DapperBaseRepository<T, TK> : DapperBaseRepository 
        where TK:struct 
        where T : IDotStatEntity<TK>
    {
        /// <summary>
        /// Execute arbitrary sql query
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="param"></param>
        protected virtual void Execute(string sql, object @param = null)
        {
            using (var conn = GetConnection(false))
                conn.Execute(sql, @param);
        }

        /// <summary>
        /// Execute arbitrary sql query asynchronously
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="cancellationToken"></param>
        /// <param name="param"></param>
        protected virtual async Task ExecuteAsync(string sql, CancellationToken cancellationToken, object @param = null)
        {
            using (var conn = await GetConnectionAsync(false, cancellationToken))
                await conn.ExecuteAsync(sql, @param);
        }

        /// <summary>
        /// Executes sql reader and returns result as strong typed collection
        /// </summary>
        /// <param name="sql">full Sql query</param>
        /// <param name="param"></param>
        /// <returns></returns>
        protected virtual async Task<IEnumerable<T>> Query(string sql, object @param = null)
        {
            using (var conn = GetConnection(true))
                return await conn.QueryAsync<T>(sql, @param);
        }

        /// <summary>
        /// Executes sql reader and returns result as strong typed collection asynchronously
        /// </summary>
        /// <param name="sql">full Sql query</param>
        /// <param name="param"></param>
        /// <returns></returns>
        protected virtual async Task<IEnumerable<T>> QueryAsync(string sql, CancellationToken cancellationToken, object @param = null)
        {
            using (var conn = await GetConnectionAsync(true, cancellationToken))
                return await conn.QueryAsync<T>(sql, @param);
        }
        /// <summary>
        /// Executes sql reader and returns result as strong typed collection
        /// Where query will be autogenerated from criteria object
        /// </summary>
        /// <param name="criteria">parameters used in query</param>
        /// <returns></returns>
        protected virtual IEnumerable<T> GetList(object criteria = null)
        {
            using (var conn = GetConnection(true))
                return conn.GetList<T>(criteria);
        }

        /// <summary>
        /// Executes sql reader and returns result as strong typed collection
        /// </summary>
        /// <param name="sqlAppend">sql after table name</param>
        /// <param name="parameters">parameters used in query</param>
        /// <returns></returns>
        protected virtual IEnumerable<T> GetList(string sqlAppend, object parameters = null)
        {
            using (var conn = GetConnection(true))
                return conn.GetList<T>(sqlAppend, parameters);
        }

        public virtual T GetById(TK id)
        {
            using (var conn = GetConnection(true))
                return conn.Get<T>(id);
        }

        /// <summary>
        /// Returns Id of new inserted record
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public virtual TK Insert(T entity)
        {
            using (var conn = GetConnection(false))
                return entity.Id = conn.Insert<TK, T>(entity);
        }

        /// <summary>
        /// Returns number of rows affected
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public virtual int Update(T entity)
        {
            using (var conn = GetConnection(false))
                return conn.Update(entity);
        }

        /// <summary>
        /// Returns the number of records affected
        /// </summary>
        /// <param name="id"></param>
        public virtual int Delete(TK id)
        {
            using (var conn = GetConnection(false))
                return conn.Delete<T>(id);
        }
    }
}