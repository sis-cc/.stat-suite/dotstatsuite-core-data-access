﻿using System.Diagnostics.CodeAnalysis;
using DotStat.Common.Exceptions;

namespace DotStat.Db.Exception
{
    [ExcludeFromCodeCoverage]
    public class BulkCopyException : DotStatException
    {
        public BulkCopyException()
        {
        }

        public BulkCopyException(string message) : base(message)
        {
        }

        public BulkCopyException(string message, System.Exception innerException) : base(message, innerException)
        {
        }
    }
}
