﻿using System.Diagnostics.CodeAnalysis;
using DotStat.Common.Exceptions;

namespace DotStat.Db.Exception
{
    [ExcludeFromCodeCoverage]
    public class TableVersionException : DotStatException
    {
        public TableVersionException()
        {
        }

        public TableVersionException(string message) : base(message)
        {
        }

        public TableVersionException(string message, System.Exception innerException) : base(message, innerException)
        {
        }
    }
}
