using DotStat.Common.Exceptions;
using DotStat.Common.Localization;
using DotStat.Domain;
using Org.Sdmxsource.Sdmx.Api.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MySqlConnector;
using Org.Sdmxsource.Sdmx.Api.Constants.InterfaceConstant;
using Attribute = DotStat.Domain.Attribute;
using Dsd = DotStat.Domain.Dsd;
using MetadataAttribute = DotStat.Domain.MetadataAttribute;
using PrimaryMeasure = DotStat.Domain.PrimaryMeasure;

namespace DotStat.Db
{
    public static class MariaDbExtensions
    {
        public const string ValueColumn = "`VALUE`";
        public const string TimeDimColumn = "PERIOD_SDMX";
        public const string ArtefactTable = "ARTEFACT";
        public const string LAST_UPDATED_COLUMN = "LAST_UPDATED";
        public const string DimensionSwitchedOff = "~";
        public const string DimensionWildCarded = "*";
        public const string ManagementSchema = "management";
        public const string DataSchema = "data";
        public const int DimensionWildCardedDbValue = -1;
        public const int DimensionSwitchedOffDbValue = 0;
        public const int ColumnPresentDbValue = 1;
        public const string EmbargoTime = "EMBARGO_TIME";

        #region Naming conventions

        public static string MariaDbColumn(this Dimension dimension, bool externalColumn = false, string prefix = null, string suffix = null)
        {
            return
                externalColumn ? 
                    $"`{prefix}{dimension.Code}{suffix}`" :
                    dimension.Base.TimeDimension ?
                        $"`{prefix}{TimeDimColumn}{suffix}`" :
                        $"`{prefix}DIM_{dimension.DbId}{suffix}`";
        }

        public static string MariaDbArtefactTableName(string alias = null) {
            return DbExtensions.SqlArtefactTableName(alias);
        }

        public static string MariaDbPeriodStart(bool withType = false, bool withHourSupport = false, bool allowNull = false)
        {
            return MariaDbPeriod("PERIOD_START", withType, withHourSupport, allowNull);
        }

        public static string MariaDbPeriodEnd(bool withType = false, bool withHourSupport = false, bool allowNull = false)
        {
            return MariaDbPeriod("PERIOD_END", withType, withHourSupport, allowNull);
        }

        private static string MariaDbPeriod(string column, bool withType, bool withHourSupport, bool allowNull = false)
        {
            return column + (withType ? $" {(withHourSupport ? "datetime" : "date")} {(allowNull?"NULL":"NOT NULL")}" : "");
        }

        public static string MariaDbColumn(this Attribute attribute, bool externalColumn = false)
        {
            return externalColumn ? $"{attribute.Code}" : $"COMP_{attribute.DbId}";
        }

        public static string MariaDbColumn(this MetadataAttribute metaAttribute, bool externalColumn = false)
        {
            return externalColumn ? $"`{metaAttribute.HierarchicalId}`" : $"`COMP_{metaAttribute.DbId}`";
        }

        public static string MariaDbCollate(this PrimaryMeasure primaryMeasure, string value)
        {
            return !primaryMeasure.Base.HasCodedRepresentation() &&
                   (GetMariaDbType(primaryMeasure, primaryMeasure.Dsd.SupportsIntentionallyMissingValues).Contains("varchar")
                    || GetMariaDbType(primaryMeasure, primaryMeasure.Dsd.SupportsIntentionallyMissingValues).Contains("TEXT"))
                ? $" convert({value} using utf8mb4) COLLATE utf8mb4_bin "
                : value;
        }

        public static string MariaDbCollate(this Attribute attribute, string value)
        {
            return !attribute.Base.HasCodedRepresentation() &&
                   (GetMariaDbType(attribute, true, false).Contains("varchar") || GetMariaDbType(attribute, true, false).Contains("TEXT"))
                ? $" convert({value} using utf8mb4) COLLATE utf8mb4_bin "
                : value;
        }

        public static string MariaDbCollate(this MetadataAttribute metaAttribute, string value)
        {
            return !metaAttribute.Base.HasCodedRepresentation() &&
                   GetMariaDbType(metaAttribute, true).Contains("TEXT")
                ? $" convert({value} using utf8mb4) COLLATE utf8mb4_bin "
                : value;
        }

        public static string MariaDbStagingTable(this Dsd dsd, string alias = null)
        {
            return MariaDbStagingTable(dsd.DbId, alias);
        }

        public static string MariaDbStagingTable(int dsdDbId, string alias = null) {
            return DbExtensions.SqlStagingTable(dsdDbId, alias);
        }
        
        public static string MariaDbFactTable(this Dsd dsd, char tableVersion, string alias = null) {
            return dsd.SqlFactTable(tableVersion, alias);
        }
        public static string MariaDbFactTableWithSchema(this Dsd dsd, char tableVersion, string alias = null) {
            return $"{DataSchema}_{dsd.SqlFactTable(tableVersion, alias)}";
        }
        public static string MariaDbFactTable(int dbId, char tableVersion, string alias = null) {
            return DbExtensions.SqlFactTable(dbId, tableVersion, alias);
        }

        public static string MariaDbDeletedTable(this Dsd dsd, char tableVersion, string alias = null) {
            return dsd.SqlDeletedTable(tableVersion, alias);
        }

        public static string MariaDbFilterTable(this Dsd dsd, string alias = null) {
            return dsd.SqlFilterTable(alias);
        }

        public static string MariaDbDimGroupAttrTable(this Dsd dsd, char tableVersion, string alias = null)
        {
            return dsd.SqlDimGroupAttrTable(tableVersion, alias);
        }
        
        public static string MariaDbDimGroupAttrTable(int dbId, char tableVersion, string alias = null) {
            return DbExtensions.SqlDimGroupAttrTable(dbId, tableVersion, alias);
        }

        public static string MariaDbDsdAttrTable(this Dsd dsd, char tableVersion, string alias = null)
        {
            return dsd.SqlDsdAttrTable(tableVersion, alias);
        }

        public static string MariaDbDsdAttrTable(int dbId, char tableVersion, string alias = null) {
            return DbExtensions.SqlDsdAttrTable(dbId, tableVersion, alias);
        }

        public static string MariaDbMetadataStagingTable(this Dsd dsd, string alias = null)
        {
            return MariaDbMetadataStagingTable(dsd.DbId, alias);
        }

        public static string MariaDbMetadataStagingTable(int dbId, string alias = null)
        {
            return DbExtensions.SqlMetadataStagingTable(dbId, alias);
        }

        public static string MariaDbMetadataDataStructureTable(this Dsd dsd, char tableVersion, string alias = null)
        {
            return MariaDbMetadataDataStructureTable(dsd.DbId, tableVersion, alias);
        }

        public static string MariaDbMetadataDataStructureTable(int dbId, char tableVersion, string alias = null)
        {
            return $"META_DSD_{dbId}_{tableVersion} {alias}".Trim();
        }

        public static string MariaDbMetadataDataflowTable(this Dataflow dataFlow, char tableVersion, string alias = null)
        {
            return MariaDbMetadataDataflowTable(dataFlow.DbId, tableVersion, alias);
        }

        public static string MariaDbMetadataDataflowTable(int dbId, char tableVersion, string alias = null)
        {
            return $"META_DF_{dbId}_{tableVersion} {alias}".Trim();
        }

        public static string MariaDbMetadataDataSetTable(this Dsd dsd, char tableVersion, string alias = null)
        {
            return MariaDbMetadataDataSetTable(dsd.DbId, tableVersion, alias);
        }

        public static string MariaDbMetadataDataSetTable(int dbId, char tableVersion, string alias = null)
        {
            return $"META_DS_{dbId}_{tableVersion} {alias}".Trim();
        }

        public static string MariaDbDeletedMetadataTable(this Dsd dsd, char tableVersion, string alias = null)
        {
            return MariaDbDeletedMetadataTable(dsd.DbId, tableVersion, alias);
        }

        public static string MariaDbDeletedMetadataTable(int dbId, char tableVersion, string alias = null)
        {
            return $"DELETED_META_{dbId}_{tableVersion} {alias}".Trim();
        }

        public static string MariaDbMetadataAttributeTable(string alias = null)
        {
            return DbExtensions.SqlMetadataAttributeTable(alias);
        }

        public static string MariaDbDataDsdViewName(this Dsd dsd, char viewVersion)
        {
            return MariaDbDataDsdViewName(dsd.DbId, viewVersion);
        }

        public static string MariaDbDataDsdViewName(int dsdDbId, char viewVersion)
        {
            return DbExtensions.SqlDataDsdViewName(dsdDbId, viewVersion);
        }
        
        public static string MariaDbDataDsdViewNameWithSchema(this Dsd dsd, char viewVersion)
        {
            return $"{DataSchema}_{MariaDbDataDsdViewName(dsd, viewVersion)}";
            
        }

        public static string MariaDbDataflowViewName(this Dataflow dataflow, char viewVersion)
        {
            return MariaDbDataflowViewName(dataflow.DbId, viewVersion);
        }

        public static string MariaDbDataflowViewName(int dataFlowDsdId, char viewVersion)
        {
            return DbExtensions.SqlDataDataflowViewName(dataFlowDsdId, viewVersion);
        }

        public static string MariaDbDataReplaceDataflowViewName(this Dataflow dataflow, char viewVersion)
        {
            return MariaDbDataReplaceDataflowViewName(dataflow.DbId, viewVersion);
        }

        public static string MariaDbDataReplaceDataflowViewName(int dataFlowDsdId, char viewVersion)
        {
            return DbExtensions.SqlDataReplaceDataflowViewName(dataFlowDsdId, viewVersion);
        }

        public static string MariaDbMetaDataDsdViewName(this Dsd dsd, char viewVersion)
        {
            return MariaDbMetaDataDsdViewName(dsd.DbId, viewVersion);
        }

        public static string MariaDbMetaDataDsdViewName(int dsdDbId, char viewVersion)
        {
            return DbExtensions.SqlDataReplaceDataflowViewName(dsdDbId, viewVersion);
        }

        public static string MariaDbMetadataDataflowViewName(this Dataflow dataFlow, char viewVersion)
        {
            return MariaDbMetadataDataflowViewName(dataFlow.DbId, viewVersion);
        }

        public static string MariaDbMetadataDataflowViewName(int dataFlowDsdId, char viewVersion)
        {
            return DbExtensions.SqlMetadataDataflowViewName(dataFlowDsdId, viewVersion);
        }

        public static string MariaDbDeletedMetadataDataflowViewName(this Dataflow dataFlow, char viewVersion)
        {
            return MariaDbDeletedMetadataDataflowViewName(dataFlow.DbId, viewVersion);
        }

        public static string MariaDbDeletedMetadataDataflowViewName(int dataFlowDsdId, char viewVersion)
        {
            return DbExtensions.SqlDeletedMetadataDataflowViewName(dataFlowDsdId, viewVersion);
        }

        public static string MariaDbDeletedMetadataDsdViewName(this Dsd dsd, char viewVersion)
        {
            return MariaDbDeletedMetadataDsdViewName(dsd.DbId, viewVersion);
        }

        public static string MariaDbDeletedMetadataDsdViewName(int dsdId, char viewVersion)
        {
            return $"VI_DeletedMetadataDsd_{dsdId}_{viewVersion}";
        }

        public static string GetCodelistTableName(int codelistId) {
            return DbExtensions.GetCodelistTableName(codelistId);
        }

        public static string MariaDbDeletedDataViewName(this Dataflow dataflow, char viewVersion)
        {
            return MariaDbDeletedDataViewName(dataflow.DbId, viewVersion);
        }

        public static string MariaDbDeletedDataViewName(int dataFlowDbId, char viewVersion)
        {
            return DbExtensions.SqlDeletedDataViewName(dataFlowDbId, viewVersion);
        }
        
        public static string MariaDbDataIncludeHistoryDataflowViewName(this Dataflow dataflow, char viewVersion)
        {
            return MariaDbDataIncludeHistoryDataflowViewName(dataflow.DbId, viewVersion);
        }

        public static string MariaDbDataIncludeHistoryDataflowViewName(int dataFlowDsdId, char viewVersion)
        {
            return DbExtensions.SqlDataIncludeHistoryDataflowViewName(dataFlowDsdId, viewVersion);
        }
        
        public static string MariaDbFactHistoryTable(this Dsd dsd, char tableVersion, string alias = null) {
            return dsd.SqlFactHistoryTable(tableVersion, alias);
        }
        
        public static string MariaDbDimGroupAttrHistoryTable(this Dsd dsd, char tableVersion, string alias = null)
        {
            return dsd.SqlDimGroupAttrHistoryTable(tableVersion, alias);
        }

        public static string MariaDbDsdAttrHistoryTable(this Dsd dsd, char tableVersion, string alias = null)
        {
            return dsd.SqlDsdAttrHistoryTable(tableVersion, alias);
        }

        public static string MariaDbAttrTable(this Attribute attribute, char tableVersion)
        {
            if (attribute.Base.AttachmentLevel == AttributeAttachmentLevel.Observation ||
                (attribute.Base.AttachmentLevel is AttributeAttachmentLevel.DimensionGroup or AttributeAttachmentLevel.Group
                 && attribute.Base.GetDimensionReferences().Contains(DimensionObject.TimeDimensionFixedId)))
            {
                return MariaDbFactTable(attribute.Dsd.DbId, tableVersion);
            }

            if (attribute.Base.AttachmentLevel is AttributeAttachmentLevel.DimensionGroup or AttributeAttachmentLevel.Group)
            {
                return MariaDbDimGroupAttrTable(attribute.DbId, tableVersion);
            }

            if (attribute.Base.AttachmentLevel == AttributeAttachmentLevel.DataSet)
            {
                return MariaDbDsdAttrTable(attribute.DbId, tableVersion);
            }

            throw new ArgumentNullException();
        }

        #endregion

        public static string MariaDbBuildRowIdFormula(this Dsd dsd, bool includeTimeDim = true)
        {
            var str = new StringBuilder();
            str.Append("CONCAT(");

            foreach (var dim in dsd.Dimensions)
            {
                if (!dim.Base.TimeDimension)
                {
                    str.Append(dim.Base.HasCodedRepresentation()
                            ? $"CONVERT(IFNULL(DIM_{dim.DbId}, 0), binary(11))"
                            : $"UNHEX(MD5(COALESCE(DIM_{dim.DbId}, '')))"
                        )
                        .Append(',');
                }
            }

            if (dsd.TimeDimension != null && includeTimeDim)
            {
                str.Append("UNHEX(MD5(COALESCE(PERIOD_SDMX, ''))),");
            }

            return str.ToString(0, str.Length - 1) + ")";
        }

        public static string ToMariaDbColumnList(this IEnumerable<Dimension> dimensions, bool withType = false, bool allowNull=false, bool externalColumn = false, bool forIndex = false) {
            return string.Join(", ", dimensions.OrderTimeFirst()
                .Select(dim => dim.MariaDbColumn(externalColumn) + (forIndex ? GetMariaDbIndexSize(dim) : dim.GetMariaDbType(withType, allowNull))));
        }

        public static string ToMariaDbColumnList(this IEnumerable<Attribute> attributes,
            IList<AttributeAttachmentLevel> levelFilter = null,
            bool withType = false, 
            bool applyNotNull = true, 
            bool externalColumn = false)
        {
            return string.Join(",", attributes
                .Where(attr => levelFilter == null || levelFilter.Contains(attr.Base.AttachmentLevel))
                .Select(attr => attr.MariaDbColumn(externalColumn) + attr.GetMariaDbType(withType, applyNotNull))
            );
        }
        
        public static string ToMariaDbColumnListWithTablePrefix(this IEnumerable<Attribute> attributes, 
            IList<AttributeAttachmentLevel> levelFilter = null,
            bool withType = false, bool applyNotNull = true, bool externalColumn = false, string table = null)
        {
            return string.Join(",", attributes
                .Where(attr => levelFilter == null || levelFilter.Contains(attr.Base.AttachmentLevel))
                .Select(attr => (string.IsNullOrEmpty(table) ? attr.MariaDbColumn(externalColumn) : $"{table}.{attr.MariaDbColumn(externalColumn)}") + attr.GetMariaDbType(withType, applyNotNull))
            );
        }

        public static string ToMariaDbColumnListWithIsNull(this IEnumerable<Attribute> attributes, IList<AttributeAttachmentLevel> levelFilter = null)
        {
            return string.Join(" AND ", attributes
                .Where(attr => levelFilter == null || levelFilter.Contains(attr.Base.AttachmentLevel))
                .Select(attr => $"{attr.MariaDbColumn()} IS NULL")
            );
        }

        public static string ToMariaDbColumnList(this IEnumerable<MetadataAttribute> metadataAttributes,
            bool withType = false, bool externalColumn = false)
        {
            return string.Join(",", metadataAttributes.Select(attr => attr.MariaDbColumn(externalColumn) + attr.GetMariaDbType(withType)));
        }
        
        public static string ToMariaDbColumnListWithTablePrefix(this IEnumerable<MetadataAttribute> metadataAttributes,
            bool withType = false, bool externalColumn = false, string table = null)
        {
            return string.Join(",", metadataAttributes
                .Select(attr => (string.IsNullOrEmpty(table) ? attr.MariaDbColumn(externalColumn) : $"{table}.{attr.MariaDbColumn(externalColumn)}") + attr.GetMariaDbType(withType))
            );
        }

        public static string ToMariaDbColumnListWithIsNull(this IEnumerable<MetadataAttribute> metadataAttributes)
        {
            return string.Join(" AND ", metadataAttributes.Select(attr => $"{attr.MariaDbColumn()} IS NULL"));
        }

        public static string MariaDbGetTableTypeCmd(string tableName) {
            return $@"SELECT TABLE_TYPE FROM information_schema.tables WHERE TABLE_SCHEMA = DATABASE() AND TABLE_NAME = '{tableName}'";
        }

        public static MySqlDbType GetMariaDbType(this Attribute attr)
        {
            return attr.Base.HasCodedRepresentation() 
                ? MySqlDbType.Int64 
                : attr.Code.Equals(DbExtensions.EmbargoTime, StringComparison.InvariantCultureIgnoreCase)
                    ? MySqlDbType.DateTime
                    : MySqlDbType.VarChar;
        }

        public static string GetMariaDbType(this Attribute attribute, bool withType, bool applyNotNull)
        {
            if (!withType)
            {
                return null;
            }

            string sqlType;

            if (attribute.Base.HasCodedRepresentation())
            {
                sqlType = "int";
            }
            else
            {
                if (attribute.Code.Equals("EMBARGO_TIME", StringComparison.InvariantCultureIgnoreCase))
                {
                    sqlType = "datetime";
                }
                else
                {
                    //A value of less or equal to zero `0` or any value higher than **4000** means that the textual attribute values are practically unlimited
                    var maxAttributeLength = attribute.Base.Representation?.TextFormat?.MaxLength is > 0 and <= 4000
                        ? attribute.Base.Representation.TextFormat.MaxLength
                        : null;

                    sqlType = maxAttributeLength is null ? "TEXT" : $"varchar({maxAttributeLength.ToString()})";
                }
            }

            return " " + sqlType + (applyNotNull && attribute.Base.Mandatory ? " NOT NULL" : "");
        }

        private static string GetMariaDbType(this Dimension dim, bool withType, bool allowNull)
        {
            if (!withType)
            {
                return null;
            }

            var sqlType = "int";

            if (dim.Base.TimeDimension)
            {
                sqlType = "varchar(30)";
            }
            else if (!dim.Base.HasCodedRepresentation()) {
                sqlType = "TEXT";
            }

            return " " + sqlType + (allowNull ? "" : " NOT NULL");
        }
        
        private static string GetMariaDbIndexSize(this Dimension dim)
        {
            if (!dim.Base.TimeDimension && !dim.Base.HasCodedRepresentation()) {
                return "(624)";
            }

            return null;
        }

        public static MySqlDbType GetMariaDbType(this MetadataAttribute attr)
        {
            return MySqlDbType.VarChar;
        }

        private static string GetMariaDbType(this MetadataAttribute attr, bool withType)
        {
            if (!withType)
            {
                return null;
            }

            return " TEXT";
        }

        public static string GetMariaDbType(this PrimaryMeasure primaryMeasure, bool supportsIntentionallyMissingValues)
        {
            if (primaryMeasure.Codelist != null)
            {
                // Primary measure is coded, the code value is stored
                return "varchar(150)"; // Identical to the data type of ID column in code list tables
            }

            // Measure is not coded, text format of representation defines the sql data type
            if (primaryMeasure.TextFormat == null)
            {
                // No representation is defined in DSD so the default string textFormat is applied.
                return "text";
            }

            switch (primaryMeasure.TextFormat.TextType.EnumType)
            {
                case TextEnumType.String:
                case TextEnumType.Alpha:
                case TextEnumType.Alphanumeric:
                case TextEnumType.Uri:
                {
                    return primaryMeasure.TextFormat.MaxLength is > 0 and <= 4000
                        ? $"varchar({primaryMeasure.TextFormat.MaxLength.ToString()})"
                        : "TEXT";
                }
                case TextEnumType.Numeric:
                case TextEnumType.BigInteger:
                case TextEnumType.Decimal:
                {
                    return primaryMeasure.TextFormat.MaxLength is > 0 and <= 8000
                        ? $"varchar({primaryMeasure.TextFormat.MaxLength.ToString()})"
                        : "TEXT";
                }
                case TextEnumType.Integer:
                case TextEnumType.Count:
                    return "int";
                case TextEnumType.Long:
                    return "bigint";
                case TextEnumType.Short:
                case TextEnumType.Boolean:
                    return "smallint";
                case TextEnumType.Float:
                    return supportsIntentionallyMissingValues ? "varchar(6)" : "float(24)";
                case TextEnumType.Double:
                    return supportsIntentionallyMissingValues ? "varchar(10)" : "float(53)";
                case TextEnumType.ObservationalTimePeriod:
                case TextEnumType.StandardTimePeriod:
                case TextEnumType.BasicTimePeriod:
                case TextEnumType.GregorianTimePeriod:
                case TextEnumType.GregorianYear:
                case TextEnumType.GregorianYearMonth:
                case TextEnumType.GregorianDay:
                case TextEnumType.ReportingTimePeriod:
                case TextEnumType.ReportingYear:
                case TextEnumType.ReportingSemester:
                case TextEnumType.ReportingTrimester:
                case TextEnumType.ReportingQuarter:
                case TextEnumType.ReportingMonth:
                case TextEnumType.ReportingWeek:
                case TextEnumType.ReportingDay:
                case TextEnumType.DateTime:
                case TextEnumType.TimesRange:
                case TextEnumType.Month:
                case TextEnumType.MonthDay:
                case TextEnumType.Day:
                case TextEnumType.Time:
                case TextEnumType.Duration:
                    {
                        return "varchar(100)";
                    }
                default:
                    throw new DotStatException(string.Format(
                        LocalizationRepository.GetLocalisedResource(Localization.ResourceId.InvalidValueFormatNotSupported),
                        primaryMeasure.TextFormat.TextType.EnumType,
                        primaryMeasure.FullId));
            }
        }
    }

}
