﻿using System;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Transactions;
using DotStat.Common.Configuration.Dto;
using DotStat.Common.Logger;
using DotStat.DB;
using DotStat.Domain;

namespace DotStat.Db.DB
{
    public delegate IDotStatDb DotStatDbResolver(string dataSpace);

    public abstract class DotStatDbBase<TConnection> : IDotStatDb
        where TConnection : DbConnection
    {
        public string Id { get; set; }

        public virtual string ManagementSchema { get; protected set; }

        public virtual string DataSchema { get; protected set; }

        public virtual bool UseColumnstoreIndexes => true;

        public int DatabaseCommandTimeout { get; protected set; }

        public int DataImportTimeOutInMinutes { get; protected set; }
        
        public DataspaceInternal DataSpace { get; protected set; }
        
        private readonly Version _supportedDbVersion;
        protected DotStatDbBase(
            string managementSchema, 
            string dataSchema, 
            DataspaceInternal dataSpaceConfiguration,
            Version supportedDbVersion
        )
        {
            if (string.IsNullOrWhiteSpace(managementSchema))
            {
                throw new ArgumentNullException(nameof(managementSchema));
            }

            if (string.IsNullOrWhiteSpace(dataSchema))
            {
                throw new ArgumentNullException(nameof(dataSchema));
            }

            if (dataSpaceConfiguration == null)
            {
                throw new ArgumentNullException(nameof(dataSpaceConfiguration));
            }

            if (string.IsNullOrEmpty(dataSpaceConfiguration.DotStatSuiteCoreDataDbConnectionString))
            {
                throw new ArgumentNullException(nameof(dataSpaceConfiguration.DotStatSuiteCoreDataDbConnectionString));
            }

            if (string.IsNullOrWhiteSpace(dataSpaceConfiguration.Id))
            {
                throw new ArgumentNullException(nameof(dataSpaceConfiguration.Id));
            }

            ManagementSchema = managementSchema;
            DataSchema = dataSchema;
            DataSpace = dataSpaceConfiguration;
            Id = dataSpaceConfiguration.Id.ToUpper();
            DatabaseCommandTimeout = dataSpaceConfiguration.DatabaseCommandTimeoutInSec;
            DataImportTimeOutInMinutes = dataSpaceConfiguration.DataImportTimeOutInMinutes;
            _supportedDbVersion = supportedDbVersion ?? SupportedDatabaseVersion.DataDbVersion;
        }

        public abstract TConnection GetConnection(bool readOnly = false);

        public virtual DbConnection GetDbConnection(bool readOnly = false)
        {
            return GetConnection(readOnly);
        }

        public virtual MappingStore.DbType GetDbType() {
            return MappingStore.DbType.SqlServer;
        }

        public abstract Task<string> GetDatabaseVersion(CancellationToken cancellationToken);

        public virtual async Task<int> ExecuteNonQuerySqlAsync(string command, CancellationToken cancellationToken)
        {
            return await ExecuteNonQuerySqlWithParamsAsync(command, cancellationToken);
        }
        public virtual async Task<int> ExecuteNonQuerySqlWithParamsAsync(string command, CancellationToken cancellationToken, params DbParameter[] parameters)
        {
            if (string.IsNullOrWhiteSpace(command))
            {
                throw new ArgumentNullException(nameof(command));
            }

            await using var con = GetConnection();
            await using var cmd = con.CreateCommand();
            
            cmd.CommandText = command;
            cmd.CommandTimeout = DatabaseCommandTimeout;

            if (parameters != null)
            {
                cmd.Parameters.AddRange(parameters);
            }

            var result = await cmd.ExecuteNonQueryAsync(cancellationToken);
            cmd.Parameters.Clear();

            return result;
        }
        
        public virtual async Task<object> ExecuteScalarSqlAsync(string command, CancellationToken cancellationToken)
        {
            return await ExecuteScalarSqlWithParamsAsync(command, cancellationToken);
        }
        
        public virtual async Task<object> ExecuteScalarSqlWithParamsAsync(string command, CancellationToken cancellationToken, params DbParameter[] parameters)
        {
            if (string.IsNullOrWhiteSpace(command))
            {
                throw new ArgumentNullException(nameof(command));
            }

            LogSqlQuery(command, parameters);

            await using var con = GetConnection();
            await using var cmd = con.CreateCommand();

            cmd.CommandText = command;
            cmd.CommandTimeout = DatabaseCommandTimeout;

            if (parameters != null)
            {
                cmd.Parameters.AddRange(parameters);
            }
            
            var result = await cmd.ExecuteScalarAsync(cancellationToken);
            cmd.Parameters.Clear();

            return result;
        }

        public virtual async Task<DbDataReader> ExecuteReaderSqlAsync(string command, CancellationToken cancellationToken, CommandBehavior commandBehavior = CommandBehavior.CloseConnection, bool tryUseReadOnlyConnection = false)
        {
            return await ExecuteReaderSqlWithParamsAsync(command, cancellationToken, commandBehavior, tryUseReadOnlyConnection);
        }
        
        public virtual async Task<DbDataReader> ExecuteReaderSqlWithParamsAsync(string command, CancellationToken cancellationToken, CommandBehavior commandBehavior = CommandBehavior.CloseConnection, bool tryUseReadOnlyConnection = false, params DbParameter[] parameters)
        {
            if (string.IsNullOrWhiteSpace(command))
            {
                throw new ArgumentNullException(nameof(command));
            }

            LogSqlQuery(command, parameters);

            await using var cmd = GetConnection(tryUseReadOnlyConnection).CreateCommand();

            cmd.CommandText = command;
            cmd.CommandTimeout = DatabaseCommandTimeout;

            if (parameters != null)
            {
                cmd.Parameters.AddRange(parameters);
            }

            var result = await cmd.ExecuteReaderAsync(commandBehavior | CommandBehavior.CloseConnection, cancellationToken);
            cmd.Parameters.Clear();

            return result;
        }

        public abstract Task<bool> ViewExists(string viewName, CancellationToken cancellationToken);

        public abstract Task<bool> TableExists(string tableName, CancellationToken cancellationToken);

        public abstract Task<bool> ColumnExists(string tableName, string columnName, CancellationToken cancellationToken);

        public abstract Task<bool> PartitionSchemeExists(string partitioningScheme,
            CancellationToken cancellationToken);

        public abstract Task DropView(string schema, string viewName, CancellationToken cancellationToken);

        public abstract Task DropTable(string schema, string tableName, CancellationToken cancellationToken);

        public abstract Task TruncateTable(string schema, string tableName, CancellationToken cancellationToken);
        
        public abstract Task DeleteAllFromTable(string schema, string tableName, CancellationToken cancellationToken);

        public void LogSqlQuery(string sql, params DbParameter[] parameters)
        {
            var sb = new StringBuilder($"SQL: {sql}");

            if (parameters != null && parameters.Any())
            {
                sb.Append(" (");

                foreach (var p in parameters)
                    sb.Append(p.ParameterName + "=" + p.Value).Append(",");

                sb.Length--;
                sb.Append(")");
            }
            
            Log.Debug(sb.ToString());
        }

        public abstract ValueTask<Version> GetCurrentDbVersion(CancellationToken cancellationToken);

        public abstract Task DropAllIndexesOfTable(string schema, string tableName, CancellationToken cancellationToken);
        
        public abstract Task CreateColumnstoreIndex(string schema, string tableName, string indexName, DataCompressionEnum dataCompression, CancellationToken cancellationToken);

        public abstract Task AddTemporalTableSupport(string schema, string tableName, string historyTableName, CancellationToken cancellationToken);

        public abstract Task RemoveTemporalTableSupport(string schema, string tableName, CancellationToken cancellationToken);

        public abstract Task SetSystemVersioning(string schema, string tableName, string historyTableName, DbExtensions.SystemVersioning systemVersioning, CancellationToken cancellationToken);

        public abstract Task CreateUniqueIndex(string tableName, string indexName, string columns,
            CancellationToken cancellationToken);

        public abstract Task CreatePrimaryKey(string tableName, string primaryKeyName, string columns, bool clustered, CancellationToken cancellationToken);

        public Version GetSupportedDbVersion()
        {
            return _supportedDbVersion;
        }

        public virtual TransactionScope GetTransactionScope(bool useDefaultIsolationLevel = false)
        {
            // TODO: transaction options including the usage of default isolation level at TryNewTransaction should be reviewed at this ticket:
            // https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/335
            var transactionOptions = new TransactionOptions { Timeout = new TimeSpan(0, DataImportTimeOutInMinutes, 0) };

            //IsolationLevel.ReadUncommitted - Volatile data can be read and modified during the transaction. 
            //It allows to see other concurrent requests that are trying to achieve a lock, which have not been committed. 
            if (!useDefaultIsolationLevel)
                transactionOptions.IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted;

            return new TransactionScope(TransactionScopeOption.Required, transactionOptions, TransactionScopeAsyncFlowOption.Enabled);
        }
}
}
