﻿using DotStat.Common.Configuration.Dto;
using DotStat.Common.Localization;
using DotStat.Db.Helpers;
using DotStat.Domain;
using Microsoft.Data.SqlClient;
using MySqlConnector;
using System;
using System.Data;
using System.Data.Common;
using System.Threading;
using System.Threading.Tasks;
using System.Transactions;

namespace DotStat.Db.DB
{
    public class MariaDbDotStatDb : DotStatDbBase<MySqlConnection>, IMariaDbDotStatDb
    {
        private Version _currentDbVersion;
        private new const string ManagementSchema ="management";
        private new const string DataSchema = "data";
        private const string SystemVersionedTableType = "SYSTEM VERSIONED";
        private const string BaseTableType = "BASE TABLE";
        private readonly string _connectionString;
        private readonly string _readOnlyConnectionString;
        public override bool UseColumnstoreIndexes { get; }

        public MariaDbDotStatDb(DataspaceInternal dataspace, Version supportedDbVersion) : 
            base(ManagementSchema, DataSchema, dataspace, supportedDbVersion )
        {
            _connectionString = dataspace.DotStatSuiteCoreDataDbConnectionString;
            //Do not use two different connection strings if readonly replica is not available
            _readOnlyConnectionString = string.IsNullOrEmpty(DataSpace.DotStatSuiteCoreDataDbConnectionStringReadOnlyReplica) ?
                dataspace.DotStatSuiteCoreDataDbConnectionString : DataSpace.DotStatSuiteCoreDataDbConnectionStringReadOnlyReplica;
            UseColumnstoreIndexes = dataspace.MySql?.UseColumnstoreIndexes ?? false;
        }

        public override MySqlConnection GetConnection(bool readOnly = false)
        {
            return GetMySqlConnection(readOnly);
        }
        
        public override MappingStore.DbType GetDbType() {
            return MappingStore.DbType.MariaDb;
        }
        
        public MySqlConnector.MySqlConnection GetConnectionAllowLoadLocal(bool readOnly = false)
        {
            return GetMySqlConnectorConnection(readOnly, true);
        }

        public override async Task<string> GetDatabaseVersion(CancellationToken cancellationToken)
        {
            return (await ExecuteScalarSqlAsync("SELECT `VERSION` FROM DB_VERSION", cancellationToken)).ToString();
        }

        public override async Task<bool> ViewExists(string viewName, CancellationToken cancellationToken)
        {
            return await ExecuteScalarSqlWithParamsAsync(
                @"SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = DATABASE() AND TABLE_TYPE = 'VIEW' AND UPPER(TABLE_NAME) = @Name",
                cancellationToken,
                new MySqlParameter("@Name", MySqlDbType.VarChar) { Value = viewName.ToUpper() }
            ) != null;
        }
        
        public override async Task<bool> TableExists(string tableName, CancellationToken cancellationToken)
        {
            return await ExecuteScalarSqlWithParamsAsync(
                @$"SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = DATABASE() AND UPPER(TABLE_NAME) = @Name and TABLE_TYPE IN ('{BaseTableType}','{SystemVersionedTableType}')",
                cancellationToken,
                new MySqlParameter("@Name", MySqlDbType.VarChar) { Value = tableName.ToUpper() }
                ) != null;
        }

        public override async Task<bool> ColumnExists(string tableName, string columnName, CancellationToken cancellationToken)
        {
            return (long) await ExecuteScalarSqlWithParamsAsync(
                       "select count(1) noOfColumns from INFORMATION_SCHEMA.COLUMNS where TABLE_SCHEMA = DATABASE() AND table_name = @TableName AND COLUMN_NAME = @ColumnName",
                       cancellationToken,
                       new MySqlParameter("@TableName", MySqlDbType.VarChar) { Value=tableName },
                       new MySqlParameter("@ColumnName", MySqlDbType.VarChar) { Value=columnName }
                   ) == 1;
        }

        public override Task<bool> PartitionSchemeExists(string partitioningScheme, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }
        public override async Task DropView(string schema, string viewName, CancellationToken cancellationToken)
        {
            var sqlCommand = $@"DROP VIEW IF EXISTS {schema}_{viewName}";
            
            await ExecuteNonQuerySqlWithParamsAsync(
                sqlCommand,
                cancellationToken
            );
        }

        public override async Task DropTable(string schema, string tableName, CancellationToken cancellationToken)
        {
            var sqlCommand = $@"DROP TABLE IF EXISTS {schema}_{tableName}";

            await ExecuteNonQuerySqlWithParamsAsync(
                sqlCommand,
                cancellationToken
            );
        }

        public override async Task TruncateTable(string schema, string tableName, CancellationToken cancellationToken)
        {
            var sqlCommand = $@"
BEGIN NOT ATOMIC
    IF (SELECT count(1) FROM information_schema.tables WHERE TABLE_SCHEMA = DATABASE() AND UPPER(table_name) = @Name) > 0
    THEN
        TRUNCATE TABLE {schema}_{tableName};
    END IF;
    END;";

            await ExecuteNonQuerySqlWithParamsAsync(
                sqlCommand,
                cancellationToken,
                new MySqlParameter("@Name", MySqlDbType.VarChar) { Value = $"{schema}_{tableName}".ToUpper() }
            );
        }

        public override async Task DeleteAllFromTable(string schema, string tableName, CancellationToken cancellationToken)
        {
            var sqlCommand = $@"BEGIN NOT ATOMIC
                IF (SELECT count(1) FROM information_schema.tables WHERE TABLE_SCHEMA = DATABASE() AND UPPER(table_name) = @Name) > 0
                THEN
                   DELETE FROM {schema}_{tableName};
                END IF;
                END;";

            await ExecuteNonQuerySqlWithParamsAsync(
                sqlCommand,
                cancellationToken,
                new MySqlParameter("Name", SqlDbType.VarChar) { Value = tableName.ToUpper() }
            );
        }

        public override async ValueTask<Version> GetCurrentDbVersion(CancellationToken cancellationToken)
        {
            if (_currentDbVersion == null)
            {

                var version = await GetDatabaseVersion(cancellationToken);

                if (!Version.TryParse(version, out _currentDbVersion))
                {
                    throw new NotSupportedException(string.Format(
                        LocalizationRepository.GetLocalisedResource(Localization.ResourceId.DatabaseVersionIsInvalid),
                        Id, version));
                }
            }

            return _currentDbVersion;
        }

        public override async Task DropAllIndexesOfTable(string schema, string tableName, CancellationToken cancellationToken)
        {
            var sql =
                    $@"

BEGIN NOT ATOMIC
# Drop primary key
	
    DECLARE SqlQuery VARCHAR(2000);

    IF EXISTS( SELECT 1 FROM information_schema.statistics WHERE UPPER(TABLE_NAME) = UPPER('{schema}_{tableName}') AND TABLE_SCHEMA = DATABASE() AND INDEX_NAME='PRIMARY')
    THEN
	    SET SqlQuery = 'ALTER TABLE {schema}_{tableName} DROP PRIMARY KEY';
	    EXECUTE IMMEDIATE SqlQuery;
    END IF;

# Drop indexes (unique and non-unique)

	SET SqlQuery = ( SELECT CONCAT('ALTER TABLE ', TABLE_NAME, GROUP_CONCAT(CONCAT(' DROP INDEX ', INDEX_NAME) separator ',') ,';')
						FROM (
							SELECT distinct TABLE_NAME, INDEX_NAME  FROM information_schema.statistics 
							WHERE UPPER(TABLE_NAME) = UPPER('{schema}_{tableName}') AND TABLE_SCHEMA = DATABASE()
							) x
					);
	-- select SqlQuery;		
    IF (SqlQuery IS NOT NULL)
    THEN		
	   EXECUTE IMMEDIATE SqlQuery;
    END IF;	  
END;
";
            await ExecuteNonQuerySqlAsync(sql, cancellationToken);
        }

        public override async Task CreateColumnstoreIndex(string schema, string tableName, string indexName, DataCompressionEnum dataCompression, CancellationToken cancellationToken)
        {
            await ExecuteNonQuerySqlAsync(
                $@"ALTER TABLE {schema}_{tableName} ENGINE=ColumnStore", cancellationToken);
        }

        public override async Task AddTemporalTableSupport(string schema, string tableName, string historyTableName, CancellationToken cancellationToken)
        {
            var sqlCommand = $@"ALTER TABLE {schema}_{tableName} 
ADD COLUMN ValidFrom timestamp(6) GENERATED ALWAYS AS ROW START INVISIBLE, 
ADD COLUMN ValidTo timestamp(6) GENERATED ALWAYS AS ROW END INVISIBLE,
ADD PERIOD FOR SYSTEM_TIME(ValidFrom, ValidTo),
ADD SYSTEM VERSIONING;";

            await ExecuteNonQuerySqlAsync(sqlCommand, cancellationToken);
        }

        public override async Task RemoveTemporalTableSupport(string schema, string tableName, CancellationToken cancellationToken)
        {
            if (await TableVersioningExists(tableName, cancellationToken))
            {
                var sqlCommand = $@"
                BEGIN NOT ATOMIC
                    ALTER TABLE {schema}_{tableName} DROP SYSTEM VERSIONING;
                END;";

                await ExecuteNonQuerySqlWithParamsAsync(
                    sqlCommand,
                    cancellationToken,
                    new MySqlParameter("Name", MySqlDbType.VarChar) { Value = tableName.ToUpper() }
                );
            }
        }

        public override Task SetSystemVersioning(string schema, string tableName, string historyTableName, DbExtensions.SystemVersioning systemVersioning,
            CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        public override Task CreateUniqueIndex(string tableName, string indexName, string columns, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        public override Task CreatePrimaryKey(string tableName, string primaryKeyName, string columns, bool clustered,
            CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        private MySqlConnection GetMySqlConnection(bool readOnly = false, bool allowLocalRead = false)
        {
            var connString = new MySqlConnectionStringBuilder(readOnly ? _readOnlyConnectionString : _connectionString)
            {
                AllowLoadLocalInfile = allowLocalRead,
                AllowUserVariables = true
            };

            var conn = new MySqlConnection(connString.ConnectionString);            
            conn.Open();

            if (System.Transactions.Transaction.Current == null)
            {
                using var cmd = conn.CreateCommand();
                cmd.CommandText = "SET TRANSACTION ISOLATION LEVEL READ COMMITTED;";
                cmd.ExecuteNonQuery();
            }

            return conn;
        }
        
        private MySqlConnector.MySqlConnection GetMySqlConnectorConnection(bool readOnly = false, bool allowLocalRead = false)
        {
            var connString = new MySqlConnector.MySqlConnectionStringBuilder(readOnly ? _readOnlyConnectionString : _connectionString)
            {
                ApplicationName = DbHelper.DbConnectionAppName,
                AllowLoadLocalInfile = allowLocalRead,
                AllowUserVariables = true
            };

            var conn = new MySqlConnector.MySqlConnection(connString.ConnectionString);            
            conn.Open();

            if (System.Transactions.Transaction.Current == null)
            {
                using var cmd = conn.CreateCommand();
                cmd.CommandText = "SET TRANSACTION ISOLATION LEVEL READ COMMITTED;";
                cmd.ExecuteNonQuery();
            }

            return conn;
        }

        public virtual async Task<int> ExecuteScalarSqlLastInsertedIdAsync(string command, CancellationToken cancellationToken) 
        {
            return await ExecuteScalarSqlLastInsertedIdWithParamsAsync(command, cancellationToken);
        }

        public virtual async Task<int> ExecuteScalarSqlLastInsertedIdWithParamsAsync(string command, CancellationToken cancellationToken, params DbParameter[] parameters) 
        {
            if (string.IsNullOrWhiteSpace(command))
            {
                throw new ArgumentNullException(nameof(command));
            }

            LogSqlQuery(command, parameters);

            await using var con = GetConnection();
            await using var cmd = con.CreateCommand();

            cmd.CommandText = command;
            cmd.CommandTimeout = DatabaseCommandTimeout;

            if (parameters != null)
            {
                cmd.Parameters.AddRange(parameters);
            }
            
            await cmd.ExecuteScalarAsync(cancellationToken);
            cmd.Parameters.Clear();

            return (int)cmd.LastInsertedId;
        }
        
        public async Task<bool> TableVersioningExists(string tableName, CancellationToken cancellationToken)
        {
            return (string) await ExecuteScalarSqlWithParamsAsync(
                $@"SELECT TABLE_TYPE FROM information_schema.tables WHERE TABLE_SCHEMA = DATABASE() AND TABLE_NAME = @Name", 
                cancellationToken,
                new MySqlParameter("@Name", MySqlDbType.VarChar) { Value = tableName }
            ) == SystemVersionedTableType;
        }
        
        public override TransactionScope GetTransactionScope(bool useDefaultIsolationLevel = false)
        {
            var transactionOptions = new TransactionOptions { Timeout = new TimeSpan(0, DataImportTimeOutInMinutes, 0) };

            //IsolationLevel.ReadUncommitted - Volatile data can be read and modified during the transaction. 
            //It allows to see other concurrent requests that are trying to achieve a lock, which have not been committed. 
            if (!useDefaultIsolationLevel)
                transactionOptions.IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted;

            return new TransactionScope(TransactionScopeOption.Suppress, transactionOptions, TransactionScopeAsyncFlowOption.Enabled);
        }
    }
}
