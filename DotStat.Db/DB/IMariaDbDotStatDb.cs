﻿using System;
using System.Data;
using System.Data.Common;
using System.Threading;
using System.Threading.Tasks;
using System.Transactions;
using DotStat.Common.Configuration.Dto;
using DotStat.Domain;

namespace DotStat.Db.DB
{
    public interface IMariaDbDotStatDb
    {
        Task<int> ExecuteScalarSqlLastInsertedIdAsync(string command, CancellationToken cancellationToken);
        Task<int> ExecuteScalarSqlLastInsertedIdWithParamsAsync(string command, CancellationToken cancellationToken, params DbParameter[] parameters);
    }
}