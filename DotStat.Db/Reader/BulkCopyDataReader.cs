﻿using System;
using System.Data;
using System.Diagnostics.CodeAnalysis;

namespace DotStat.DB.Reader
{
    [ExcludeFromCodeCoverage]
    public abstract class  BulkCopyDataReader : IDataReader
    {
        #region Used by SqlBulkCopy

        public abstract int FieldCount { get; }

        public abstract bool Read();

        public abstract object GetValue(int i);
        public abstract int GetValues(object[] values);

        public abstract void Close();

        #endregion

        #region Not implemented & Not used by SqlBulkCopy

        public virtual string GetName(int i) => throw new NotImplementedException();

        public void Dispose() => Close();
        
        public string GetDataTypeName(int i) => throw new NotImplementedException();

        public Type GetFieldType(int i) => throw new NotImplementedException();

        public int GetOrdinal(string name) => throw new NotImplementedException();

        public bool GetBoolean(int i) => throw new NotImplementedException();

        public byte GetByte(int i) => throw new NotImplementedException();

        public long GetBytes(int i, long fieldOffset, byte[] buffer, int bufferoffset, int length) => throw new NotImplementedException();

        public char GetChar(int i) => throw new NotImplementedException();

        public long GetChars(int i, long fieldoffset, char[] buffer, int bufferoffset, int length) => throw new NotImplementedException();

        public Guid GetGuid(int i) => throw new NotImplementedException();

        public short GetInt16(int i) => throw new NotImplementedException();

        public int GetInt32(int i) => throw new NotImplementedException();

        public long GetInt64(int i) => throw new NotImplementedException();

        public float GetFloat(int i) => throw new NotImplementedException();

        public double GetDouble(int i) => throw new NotImplementedException();

        public string GetString(int i) => throw new NotImplementedException();

        public decimal GetDecimal(int i) => throw new NotImplementedException();

        public DateTime GetDateTime(int i) => throw new NotImplementedException();

        public bool IsDBNull(int i) => throw new NotImplementedException();

        object IDataRecord.this[int i] => throw new NotImplementedException();

        object IDataRecord.this[string name] => throw new NotImplementedException();

        public DataTable GetSchemaTable() => throw new NotImplementedException();

        public int Depth => throw new NotImplementedException();

        public virtual bool IsClosed => throw new NotImplementedException();

        public bool NextResult() => throw new NotImplementedException();

        public int RecordsAffected => throw new NotImplementedException();

        public IDataReader GetData(int i) => throw new NotImplementedException();

        #endregion
    }
}
