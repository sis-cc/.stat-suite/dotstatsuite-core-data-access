﻿using System;
using System.Collections.Generic;

namespace DotStat.DB.Reader
{
    internal class BulkCopyEnumeratorDataReader<T> : BulkCopyDataReader where T : class
    {
        private IEnumerator<T> _enumerator;
        private object[] _items;
        private Func<T, object[]> GetRow;

        public BulkCopyEnumeratorDataReader(IEnumerable<T> collection, int fieldCount, Func<T, object[]> getRow)
        {
            _enumerator = collection.GetEnumerator();
            FieldCount = fieldCount;
            GetRow = getRow;
        }

        public int Index { get; private set; }

        public override int FieldCount { get;}

        public override bool Read()
        {
            if (!_enumerator.MoveNext())
            {
                return false;
            }

            _items = GetRow(_enumerator.Current);
            Index++;

            return true;
        }

        public override object GetValue(int i)
        {
            return _items[i];
        }
        
        public override int GetValues(object[] values) {
            var items = GetRow(_enumerator.Current);
            for (int i = 0; i < items.Length; i++) {
                values[i] = items[i];
            }

            return items.Length;
        }

        public override void Close()
        {
            _enumerator.Dispose();
        }
    }
}
