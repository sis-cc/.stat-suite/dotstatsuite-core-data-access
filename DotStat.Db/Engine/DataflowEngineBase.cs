﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using DotStat.Common.Configuration.Interfaces;
using DotStat.Db.DB;
using DotStat.Db.Util;
using DotStat.Domain;
using Org.Sdmxsource.Sdmx.Api.Model.Data;

namespace DotStat.Db.Engine
{
    public abstract class DataflowEngineBase<TDotStatDb> : ArtefactEngineBase<Dataflow, TDotStatDb>
        where TDotStatDb : IDotStatDb
    {
        protected DataflowEngineBase(IGeneralConfiguration generalConfiguration) : base(generalConfiguration)
        {
        }

        public override async Task<bool> CreateDynamicDbObjects(Dataflow dataflow, TDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            //Set A
            await BuildDynamicDataDataFlowView(dataflow, (char) DbTableVersion.A, dotStatDb, cancellationToken);
            await BuildDynamicDataReplaceDataFlowView(dataflow, (char)DbTableVersion.A, dotStatDb, cancellationToken);
            await BuildDeletedView(dataflow, (char)DbTableVersion.A, dotStatDb, cancellationToken);

            //Set B
            await BuildDynamicDataDataFlowView(dataflow, (char) DbTableVersion.B, dotStatDb, cancellationToken);
            await BuildDynamicDataReplaceDataFlowView(dataflow, (char)DbTableVersion.B, dotStatDb, cancellationToken);
            await BuildDeletedView(dataflow, (char)DbTableVersion.B, dotStatDb, cancellationToken);
            
            if (!dataflow.Dsd.KeepHistory) return true;
            
            await CreateHistoryViews(dataflow, dotStatDb, cancellationToken);

            return true;
        }

        public override async Task CleanUp(Dataflow dataflow, TDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            if (dataflow == null)
            {
                throw new ArgumentNullException(nameof(dataflow));
            }

            if (dotStatDb == null)
            {
                throw new ArgumentNullException(nameof(dotStatDb));
            }

            if (dataflow.DbId > 0)
            {
                await DeleteFromArtefactTableByDbId(dataflow.DbId, dotStatDb, cancellationToken);
            }
        }

        public async Task CreateHistoryViews(Dataflow dataflow, TDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            //Set A
            await BuildDynamicDataIncludeHistoryDataFlowView(dataflow, (char)DbTableVersion.A, dotStatDb, cancellationToken);

            //Set B
            await BuildDynamicDataIncludeHistoryDataFlowView(dataflow, (char)DbTableVersion.B, dotStatDb, cancellationToken);
        }

        public async Task CreateSourceViews(Dataflow dataflow, TDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            foreach (var tableVersion in new[] { (char)DbTableVersion.A, (char)DbTableVersion.B })
            {
                await BuildDynamicDataDataFlowView(dataflow, tableVersion, dotStatDb, cancellationToken,
                    sourceVersion: true);
                await BuildDynamicDataReplaceDataFlowView(dataflow, tableVersion, dotStatDb, cancellationToken,
                    sourceVersion: true);
                await BuildDeletedView(dataflow, tableVersion, dotStatDb, cancellationToken, sourceVersion: true);
            }
        }

        public async Task CreateAndFillHprDataTables(Dataflow dataflow, char tableVersion, char hprVersion, TDotStatDb dotStatDb, ICodeTranslator codeTranslator, CancellationToken cancellationToken)
        {
            await BuildOptimizedDataTable(dataflow, tableVersion, hprVersion, dotStatDb, codeTranslator, cancellationToken);

            await BuildOptimizedAttributeTable(dataflow, tableVersion, hprVersion, dotStatDb, codeTranslator, cancellationToken);

            await BuildOptimizedDeletedDataTable(dataflow, tableVersion, hprVersion, dotStatDb, codeTranslator, cancellationToken);
        }

        public async Task CreateHprDataViews(Dataflow dataflow, char tableVersion, char hprVersion, TDotStatDb dotStatDb, IReadOnlyDictionary<string, string> datasetAttributes, CancellationToken cancellationToken)
        {
            await BuildOptimizedDataView(dataflow, tableVersion, hprVersion, datasetAttributes, dotStatDb, cancellationToken);

            await BuildOptimizedReplaceView(dataflow, tableVersion, hprVersion, datasetAttributes, dotStatDb, cancellationToken);

            await BuildOptimizedDeletedDataView(dataflow, tableVersion, hprVersion, dotStatDb, cancellationToken);
        }

        protected abstract Task BuildDynamicDataDataFlowView(Dataflow dataFlow, char targetVersion, TDotStatDb dotStatDb, CancellationToken cancellationToken, bool sourceVersion = false);

        protected abstract Task BuildDynamicDataReplaceDataFlowView(Dataflow dataFlow, char targetVersion, TDotStatDb dotStatDb, CancellationToken cancellationToken, bool sourceVersion = false);
        
        protected abstract Task BuildDeletedView(Dataflow dataFlow, char targetVersion, TDotStatDb dotStatDb, CancellationToken cancellationToken, bool sourceVersion = false);
        
        protected abstract Task BuildDynamicDataIncludeHistoryDataFlowView(Dataflow dataFlow, char targetVersion, TDotStatDb dotStatDb, CancellationToken cancellationToken);

        protected abstract Task BuildOptimizedDataTable(Dataflow dataflow, char targetVersion, char hprVersion, TDotStatDb dotStatDb, ICodeTranslator codeTranslator, CancellationToken cancellationToken);

        protected abstract Task BuildOptimizedAttributeTable(Dataflow dataflow, char targetVersion, char hprVersion, TDotStatDb dotStatDb, ICodeTranslator codeTranslator, CancellationToken cancellationToken);

        protected abstract Task BuildOptimizedDeletedDataTable(Dataflow dataflow, char targetVersion, char hprVersion, TDotStatDb dotStatDb, ICodeTranslator codeTranslator, CancellationToken cancellationToken);

        protected abstract Task BuildOptimizedDataView(Dataflow dataflow, char tableVersion, char hprVersion,
            IReadOnlyDictionary<string, string> datasetAttributes, TDotStatDb dotStatDb,
            CancellationToken cancellationToken);

        protected abstract Task BuildOptimizedReplaceView(Dataflow dataflow, char tableVersion, char hprVersion, IReadOnlyDictionary<string, string> datasetAttributeValues, TDotStatDb dotStatDb,
            CancellationToken cancellationToken);

        protected abstract Task BuildOptimizedDeletedDataView(Dataflow dataflow, char tableVersion, char hprVersion, TDotStatDb dotStatDb, CancellationToken cancellationToken);
    }
}