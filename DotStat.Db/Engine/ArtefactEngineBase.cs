﻿using System.Threading;
using System.Threading.Tasks;
using DotStat.Common.Configuration.Interfaces;
using DotStat.Db.DB;
using DotStat.Domain;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;

namespace DotStat.Db.Engine
{
    public abstract class ArtefactEngineBase<TArtefact, TDotStatDb> 
        where TArtefact : IDotStatMaintainable
        where TDotStatDb : IDotStatDb
    {
        protected readonly IGeneralConfiguration GeneralConfiguration;

        protected ArtefactEngineBase(IGeneralConfiguration generalConfiguration)
        {
            GeneralConfiguration = generalConfiguration;
        }

        public async Task<int> GetDbId(IDotStatMaintainable artefact, TDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            return await GetDbId(artefact.Code, artefact.AgencyId, artefact.Version, dotStatDb, cancellationToken);
        }

        public async Task<int> GetDbId(IMaintainableObject artefact, TDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            return await GetDbId(artefact.Id, artefact.AgencyId, artefact.Version, dotStatDb, cancellationToken);
        }

        public async Task<int> GetDbId(string sdmxId, string sdmxAgency, SdmxVersion sdmxVersion, TDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            return await GetDbId(sdmxId, sdmxAgency, sdmxVersion.Major, sdmxVersion.Minor, sdmxVersion.Patch, dotStatDb, cancellationToken);
        }

        public async Task<int> GetDbId(string sdmxId, string sdmxAgency, string sdmxVersion, TDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            var ver = new SdmxVersion(sdmxVersion);

            return await GetDbId(sdmxId, sdmxAgency, ver.Major, ver.Minor, ver.Patch, dotStatDb, cancellationToken);
        }
        
        public abstract Task<int> GetDbId(string sdmxId, string sdmxAgency, int verMajor, int verMinor, int? verPatch, TDotStatDb dotStatDb, CancellationToken cancellationToken);

        public abstract Task<int> InsertToArtefactTable(TArtefact artefact, TDotStatDb dotStatDb, CancellationToken cancellationToken);

        public abstract Task<bool> CreateDynamicDbObjects(TArtefact artefact, TDotStatDb dotStatDb, CancellationToken cancellationToken);

        public abstract Task CleanUp(TArtefact artefact, TDotStatDb dotStatDb, CancellationToken cancellationToken);

        protected abstract Task DeleteFromArtefactTableByDbId(int dbId, TDotStatDb dotStatDb, CancellationToken cancellationToken);

    }
}