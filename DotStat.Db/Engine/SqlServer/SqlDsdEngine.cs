﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using DotStat.Common.Configuration.Interfaces;
using DotStat.Common.Enums;
using DotStat.Db.DB;
using DotStat.Domain;
using Microsoft.Data.SqlClient;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Constants.InterfaceConstant;

namespace DotStat.Db.Engine.SqlServer
{
    public class SqlDsdEngine : DsdEngineBase<SqlDotStatDb>
    {
        public SqlDsdEngine(IGeneralConfiguration generalConfiguration) : base(generalConfiguration)
        {
        }

        public override async Task<int> GetDbId(string sdmxId, string sdmxAgency, int verMajor, int verMinor, int? verPatch, SqlDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            var aretefactDbId = await dotStatDb.ExecuteScalarSqlWithParamsAsync(
                $@"SELECT [ART_ID] 
                    FROM [{dotStatDb.ManagementSchema}].[ARTEFACT] 
                   WHERE [ID] = @Id AND [AGENCY] = @Agency AND [TYPE] = @Type
                     AND [VERSION_1] = @Version1 AND [VERSION_2] = @Version2 
                     AND ([VERSION_3] = @Version3 OR ([VERSION_3] IS NULL AND @Version3 IS NULL))",
                cancellationToken,
                new SqlParameter("Id", SqlDbType.VarChar) { Value = sdmxId },
                new SqlParameter("Agency", SqlDbType.VarChar) { Value = sdmxAgency },                
                new SqlParameter("Type", SqlDbType.VarChar) { Value = DbTypes.GetDbType(SDMXArtefactType.Dsd) },
                new SqlParameter("Version1", SqlDbType.Int) { Value = verMajor },
                new SqlParameter("Version2", SqlDbType.Int) { Value = verMinor },
                new SqlParameter("Version3", SqlDbType.Int) { Value = ((object)verPatch) ?? DBNull.Value }

                );

            return (int)(aretefactDbId ?? -1);
        }

        public override async Task<int> InsertToArtefactTable(Dsd dsd, SqlDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            var artefactId = await dotStatDb.ExecuteScalarSqlWithParamsAsync(
                $@"INSERT 
                    INTO [{dotStatDb.ManagementSchema}].[ARTEFACT] 
                       ([SQL_ART_ID],[TYPE],[ID],[AGENCY],[VERSION_1],[VERSION_2],[VERSION_3],[LAST_UPDATED],[DSD_LIVE_VERSION],[DSD_PIT_VERSION],[DSD_PIT_RELEASE_DATE],[DSD_PIT_RESTORATION_DATE],[MSD_ID],[DATA_COMPRESSION],[KEEP_HISTORY]) 
                  OUTPUT INSERTED.ART_ID 
                  VALUES (NULL, @Type, @Id, @Agency, @Version1, @Version2, @Version3, @DT_Now, @LiveVersion, @PITVersion, @PITReleaseDate, @PITRestorationDate, @MsdId, @DataCompression, @KeepHistory)",
                cancellationToken,
                new SqlParameter("Type", SqlDbType.VarChar) { Value = DbTypes.GetDbType(SDMXArtefactType.Dsd) },
                new SqlParameter("Id", SqlDbType.VarChar) { Value = dsd.Code },
                new SqlParameter("Agency", SqlDbType.VarChar) { Value = dsd.AgencyId },
                new SqlParameter("Version1", SqlDbType.Int) { Value = dsd.Version.Major },
                new SqlParameter("Version2", SqlDbType.Int) { Value = dsd.Version.Minor },
                new SqlParameter("Version3", SqlDbType.Int) { Value = (object)dsd.Version.Patch ?? DBNull.Value },
                new SqlParameter("LiveVersion", SqlDbType.Char) { Value = (object)dsd.LiveVersion ?? DBNull.Value },
                new SqlParameter("PITVersion", SqlDbType.Char) { Value = (object)dsd.PITVersion ?? DBNull.Value },
                new SqlParameter("PITReleaseDate", SqlDbType.DateTime) { Value = (object)dsd.PITReleaseDate ?? DBNull.Value },
                new SqlParameter("PITRestorationDate", SqlDbType.DateTime) { Value = (object)dsd.PITRestorationDate ?? DBNull.Value },
                new SqlParameter("MsdId", SqlDbType.Int) { Value = (object)dsd.MsdDbId?? DBNull.Value },
                new SqlParameter("DT_Now", DateTime.Now),
                new SqlParameter("DataCompression", dsd.DataCompression.ToString()),
                new SqlParameter("KeepHistory", dsd.KeepHistory)
                );

            return (int)artefactId;
        }

        public override async Task DropAllIndexes(Dsd dsd, SqlDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            var tables = new List<string> {
                $"{dsd.SqlFilterTable()}",
                $"{dsd.SqlFactTable((char)DbTableVersion.A)}",
                $"{dsd.SqlFactTable((char)DbTableVersion.B)}",
                $"{dsd.SqlDeletedTable((char)DbTableVersion.A)}",
                $"{dsd.SqlDeletedTable((char)DbTableVersion.B)}"
            };

            // Drop all existing indexes
            foreach (var table in tables)
            {
                await dotStatDb.DropAllIndexesOfTable(dotStatDb.DataSchema, table, cancellationToken);
            }
        }

        public override async Task Compress(Dsd dsd, DataCompressionEnum dataCompression, SqlDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            if (dataCompression is DataCompressionEnum.NONE)
                return;// no compression needed

            var tables = new List<string> {
                $"{dsd.SqlFilterTable()}",
                $"{dsd.SqlFactTable((char)DbTableVersion.A)}",
                $"{dsd.SqlFactTable((char)DbTableVersion.B)}",
                $"{dsd.SqlDeletedTable((char)DbTableVersion.A)}",
                $"{dsd.SqlDeletedTable((char)DbTableVersion.B)}"
            };

            // Create CCI indexes
            foreach (var table in tables)
            {
                await dotStatDb.CreateColumnstoreIndex(dotStatDb.DataSchema, table, $"CCI_{table}", dataCompression, cancellationToken);
            }
        }

        public override async Task AddUniqueConstraints(Dsd dsd, bool clustered, SqlDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            var dimensions = dsd.Dimensions.Where(dim => !dim.Base.TimeDimension).ToList();

            //FILTER
            var uniqueColumns = dsd.Dimensions.Count > 32 ? "[ROW_ID]" : dimensions.ToColumnList();
            await dotStatDb.CreateUniqueIndex($"[{dotStatDb.DataSchema}].{dsd.SqlFilterTable()}", $"[UI_{dsd.SqlFilterTable()}]",
                uniqueColumns, cancellationToken);

            await dotStatDb.CreatePrimaryKey($"[{dotStatDb.DataSchema}].{dsd.SqlFilterTable()}",
                    $"[PK_{dsd.SqlFilterTable()}]", "[SID]", clustered, cancellationToken);
            
            //FACT tables
            var timeDimColumns = dsd.TimeDimension is null ? "" : $",{DbExtensions.SqlPeriodStart()},{DbExtensions.SqlPeriodEnd()}";
            
            await dotStatDb.CreatePrimaryKey($"[{dotStatDb.DataSchema}].{dsd.SqlFactTable((char)DbTableVersion.A)}",
                    $"[PK_{dsd.SqlFactTable((char)DbTableVersion.A)}]", $"[SID] {timeDimColumns}", clustered, cancellationToken);

            await dotStatDb.CreatePrimaryKey($"[{dotStatDb.DataSchema}].{dsd.SqlFactTable((char)DbTableVersion.B)}",
                $"[PK_{dsd.SqlFactTable((char)DbTableVersion.B)}]", $"[SID] {timeDimColumns}", clustered, cancellationToken);
        }

        #region protected
        protected override async Task DeleteFromArtefactTableByDbId(int dbId, SqlDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            await dotStatDb.ExecuteNonQuerySqlWithParamsAsync(
                $@"DELETE
                    FROM [{dotStatDb.ManagementSchema}].[{DbExtensions.SqlArtefactTableName()}] 
                   WHERE [ART_ID] = @DbId AND [TYPE] = 'DSD'",
                cancellationToken,
                new SqlParameter("DbId", SqlDbType.VarChar) { Value = dbId }
                );
        }
        
        protected override async Task BuildDynamicFactTable(Dsd dsd, char targetVersion, SqlDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            var attributeColumns = dsd.Attributes.ToColumnList(new[] { AttributeAttachmentLevel.Observation }, withType: true, applyNotNull: false);

            if (!string.IsNullOrWhiteSpace(attributeColumns))
            {
                attributeColumns += ',';
            }

            var groupAttributeColumnsWithTime = dsd.Attributes
                .Where(x => (x.Base.AttachmentLevel == AttributeAttachmentLevel.DimensionGroup || x.Base.AttachmentLevel == AttributeAttachmentLevel.Group) 
                            && x.Base.GetDimensionReferences().Contains(DimensionObject.TimeDimensionFixedId))
                .ToColumnList(withType: true, applyNotNull: false);

            if (!string.IsNullOrWhiteSpace(groupAttributeColumnsWithTime))
            {
                groupAttributeColumnsWithTime += ',';
            }

            var timeDim = dsd.TimeDimension;

            var supportsDateTime = dsd.Base.HasSupportDateTimeAnnotation();
            
            var timeDimensionColumns = timeDim != null
                ? string.Join(",",
                    new[] { timeDim }.ToColumnList(true),
                    DbExtensions.SqlPeriodStart(true, supportsDateTime),
                    DbExtensions.SqlPeriodEnd(true, supportsDateTime)
                ) + ","
                : null;
            var timeConstraint =
                timeDim != null ? $", [{DbExtensions.SqlPeriodStart()}] ASC, [{DbExtensions.SqlPeriodEnd()}] DESC" : null;

            var pkClusteredPart = dsd.DataCompression == DataCompressionEnum.NONE ? "CLUSTERED" : "NONCLUSTERED";
            var temporalTableColumns = !dsd.KeepHistory ? "" : ", [ValidFrom] [datetime2](7) GENERATED ALWAYS AS ROW START NOT NULL, [ValidTo] [datetime2](7) GENERATED ALWAYS AS ROW END NOT NULL, PERIOD FOR SYSTEM_TIME (ValidFrom, ValidTo)";
            var keepHistory = !dsd.KeepHistory ? "" : $"WITH (SYSTEM_VERSIONING = ON (HISTORY_TABLE = [{dotStatDb.DataSchema}].[{dsd.SqlFactTable(targetVersion)}_History]));";
            var constraint = dsd.DataCompression == DataCompressionEnum.COLUMNSTORE_ARCHIVE && !dsd.KeepHistory
                ? ""
                : $", CONSTRAINT [PK_DSD_{dsd.DbId}_{targetVersion}] PRIMARY KEY {pkClusteredPart} ([SID]{timeConstraint})";

            var sqlCommand = $@"CREATE TABLE [{dotStatDb.DataSchema}].[{dsd.SqlFactTable(targetVersion)}](
[SID] [int] NOT NULL,
{timeDimensionColumns}
{groupAttributeColumnsWithTime}
[VALUE] {dsd.PrimaryMeasure.GetSqlType(dsd.SupportsIntentionallyMissingValues)},
{attributeColumns}
[{DbExtensions.LAST_UPDATED_COLUMN}] [datetime] NOT NULL
{temporalTableColumns}
{constraint}
)
{keepHistory}";

            await dotStatDb.ExecuteNonQuerySqlAsync(sqlCommand, cancellationToken);

            //Apply data compression
            if (dsd.DataCompression != DataCompressionEnum.NONE)
            {
                await dotStatDb.ExecuteNonQuerySqlAsync(
                    $@"CREATE CLUSTERED COLUMNSTORE INDEX CCI_{dsd.SqlFactTable(targetVersion)}
ON [{dotStatDb.DataSchema}].[{dsd.SqlFactTable(targetVersion)}]
WITH(DATA_COMPRESSION = {dsd.DataCompression})", cancellationToken);

                if (dsd.DataCompression == DataCompressionEnum.COLUMNSTORE || dsd.DataCompression == DataCompressionEnum.COLUMNSTORE_ARCHIVE && dsd.KeepHistory)
                {
                    await AddUniqueConstraints(dsd, clustered: false, dotStatDb, cancellationToken);
                }
            }
            // Performance improvement generate actual content constraint
            else if (timeDim != null)
            {
                await dotStatDb.ExecuteNonQuerySqlAsync(
                    $@"CREATE NONCLUSTERED COLUMNSTORE INDEX [NCCI_TIME_DIM_{dsd.DbId}_{targetVersion}] 
ON [{dotStatDb.DataSchema}].[FACT_{dsd.DbId}_{targetVersion}](
    {DbExtensions.SqlPeriodStart()},
    {DbExtensions.SqlPeriodEnd()}
)", cancellationToken);
            }
        }

        protected override async Task BuildDeletedTable(Dsd dsd, char targetVersion, SqlDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            var dimensions = dsd.Dimensions.Where(dim => !dim.Base.TimeDimension).ToArray();
            var dimensionColumnsWithType = dimensions.ToColumnList(true, allowNull: true);
            var attributeColumns = string.Join(",", dsd.Attributes.Select(x=>x.SqlColumn() + " char(1)"));

            if (!string.IsNullOrWhiteSpace(attributeColumns))
            {
                attributeColumns += ',';
            }

            var timeDim = dsd.TimeDimension;
            var supportsDateTime = dsd.Base.HasSupportDateTimeAnnotation();

            var timeDimensionColumns = timeDim != null
                ? string.Join(",",
                    new[] { timeDim }.ToColumnList(true, allowNull: true),
                    DbExtensions.SqlPeriodStart(true, supportsDateTime, true),
                    DbExtensions.SqlPeriodEnd(true, supportsDateTime, true)
                ) + ","
                : null;

            var sqlCommand = $@"CREATE TABLE [{dotStatDb.DataSchema}].[{dsd.SqlDeletedTable(targetVersion)}](
[DF_ID] int not null,
{timeDimensionColumns}
{dimensionColumnsWithType},
{attributeColumns}
[VALUE] char(1),
[{DbExtensions.LAST_UPDATED_COLUMN}] [datetime] NOT NULL
)";

            await dotStatDb.ExecuteNonQuerySqlAsync(sqlCommand, cancellationToken);

            //Apply data compression
            if (dsd.DataCompression != DataCompressionEnum.NONE)
            {
                sqlCommand = $@"CREATE CLUSTERED COLUMNSTORE INDEX CCI_{dsd.SqlDeletedTable(targetVersion)}
ON [{dotStatDb.DataSchema}].[{dsd.SqlDeletedTable(targetVersion)}]
WITH(DATA_COMPRESSION = {dsd.DataCompression})";
                await dotStatDb.ExecuteNonQuerySqlAsync(sqlCommand, cancellationToken);
            }
            else
            {
                sqlCommand = $@"CREATE NONCLUSTERED INDEX I_{dsd.SqlDeletedTable(targetVersion)} 
ON [{dotStatDb.DataSchema}].[{dsd.SqlDeletedTable(targetVersion)}]([DF_ID],[LAST_UPDATED])";
                await dotStatDb.ExecuteNonQuerySqlAsync(sqlCommand, cancellationToken);
            }
        }

        protected override async Task BuildDynamicFilterTable(Dsd dsd, SqlDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            var dimensions = dsd.Dimensions.Where(dim => !dim.Base.TimeDimension).ToArray();
            var dimensionColumnsWithType = dimensions.ToColumnList(true);
            var dimensionColumns =  dimensions.ToColumnList();
            var table = dsd.SqlFilterTable();

            string rowId = null;
            var uIndex = dimensionColumns;
            
            if (dsd.Dimensions.Count > 32)
            {
                rowId = $"[ROW_ID] AS ({dsd.SqlBuildRowIdFormula(false)}) PERSISTED NOT NULL,";
                uIndex = "[ROW_ID]";
            }

            var constraint = dsd.DataCompression != DataCompressionEnum.NONE ? "" : $", CONSTRAINT [U_{table}] UNIQUE ({uIndex})";
            var pk = dsd.DataCompression == DataCompressionEnum.NONE ? "PRIMARY KEY" : "";

            var sqlCommand = $@"CREATE TABLE [{dotStatDb.DataSchema}].[{table}](
                        [SID] [int] identity(1,1) NOT NULL {pk},
                        {rowId}
	                    {dimensionColumnsWithType}
                        {constraint}
                )";

            await dotStatDb.ExecuteNonQuerySqlAsync(sqlCommand, cancellationToken);

            //Apply data compression
            if (dsd.DataCompression != DataCompressionEnum.NONE)
            {
                await dotStatDb.ExecuteNonQuerySqlAsync(
                    $@"CREATE CLUSTERED COLUMNSTORE INDEX CCI_{table}
ON [{dotStatDb.DataSchema}].[{table}]
WITH(DATA_COMPRESSION = {dsd.DataCompression})", cancellationToken);
            }
            else
            {
                // Performance improvement generate actual content constraint
                await dotStatDb.ExecuteNonQuerySqlAsync(
                    $@"CREATE NONCLUSTERED COLUMNSTORE INDEX [NCCI_{table}] ON [{dotStatDb.DataSchema}].[{table}]({dimensionColumns})",
                        cancellationToken
                    );
            }
        }

        protected override async Task BuildDynamicDataDsdView(Dsd dsd, char targetVersion, SqlDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            //The codelist table doesnt exist, therefore the db is corrupted or the method is called by the unit tests
            foreach (var dimension in dsd.Dimensions.Where(x => x.Base.HasCodedRepresentation()))
            {
                if (!await dotStatDb.TableExists($"CL_{dimension.Codelist.DbId}", cancellationToken))
                {
                    return;
                }
            }

            var dimensions = dsd.Dimensions.Where(dim => !dim.Base.TimeDimension).ToList();

            var observationAttributes = dsd.Attributes
                .Where(attr => attr.Base.AttachmentLevel == AttributeAttachmentLevel.Observation ||
                               (attr.Base.AttachmentLevel == AttributeAttachmentLevel.DimensionGroup || attr.Base.AttachmentLevel == AttributeAttachmentLevel.Group) &&
                               attr.Base.GetDimensionReferences().Contains(DimensionObject.TimeDimensionFixedId))
                .ToArray();

            // SELECT ----------------------------------------------------------------

            var selectStatement = new StringBuilder();
            var whereStatement = new StringBuilder();

            //obs value
            selectStatement.Append($"[FA].[VALUE] AS [{dsd.Base.PrimaryMeasure.Id}]");
            whereStatement.Append($"WHERE EXISTS (SELECT TOP 1 1 FROM [{dotStatDb.DataSchema}].{dsd.SqlFactTable(targetVersion, "FA")} WHERE FI.[SID] = FA.[SID])");

            var timeDim = dsd.TimeDimension;

            //time dimension
            if (timeDim!=null)
            {
                selectStatement
                    .Append(",")
                    .Append(string.Join(",",
                        $"[FA].{timeDim.SqlColumn()} AS [{dsd.Base.TimeDimension.Id}]",
                        DbExtensions.SqlPeriodStart(),
                        DbExtensions.SqlPeriodEnd()
                     ));
            }

            //dimensions
            selectStatement
                .Append(",")
                .Append(string.Join(", ", dimensions.Select(d =>
                d.Base.HasCodedRepresentation() ? $"[CL_{d.Code}].[ID] AS [{d.Code}]" : $"[FI].{d.SqlColumn()} AS [{d.Code}]"
            )));

            //obs lvl attributes
            if (observationAttributes.Any())
            {
                selectStatement
                    .Append(",")
                    .Append(string.Join(", ", observationAttributes.Select(a =>
                    a.Base.HasCodedRepresentation()
                        ? $"[CL_{a.Code}].[ID] AS [{a.Code}]"
                        : $"[FA].{a.SqlColumn()} AS [{a.Code}]"
                )));
            }

            // LAST_UPDATED
            selectStatement.Append($",[FA].{DbExtensions.LAST_UPDATED_COLUMN}");

            // JOIN ------------------------------------------------------------------

            var joinStatement = new StringBuilder();

            //dimensions
            joinStatement.AppendLine(string.Join("\n", dimensions.Where(d => d.Base.HasCodedRepresentation()).Select(d =>
                    $"LEFT JOIN [{dotStatDb.ManagementSchema}].[CL_{d.Codelist.DbId}] AS [CL_{d.Code}] ON FI.{d.SqlColumn()} = [CL_{d.Code}].[ITEM_ID]"
                )
            ));

            //obs lvl attributes
            joinStatement.AppendLine(string.Join("\n", observationAttributes.Where(a => a.Base.HasCodedRepresentation()).Select(a =>
                     $"LEFT JOIN [{dotStatDb.ManagementSchema}].[CL_{a.Codelist.DbId}] AS [CL_{a.Code}] ON FA.{a.SqlColumn()} = [CL_{a.Code}].[ITEM_ID]"
                )
            ));


            // ------------------------------------------------------
            var dimGroupAttributes = dsd.Attributes.Where(attr =>
                    (attr.Base.AttachmentLevel == AttributeAttachmentLevel.DimensionGroup || attr.Base.AttachmentLevel == AttributeAttachmentLevel.Group) &&
                    !attr.Base.GetDimensionReferences().Contains(DimensionObject.TimeDimensionFixedId))
                .ToArray();

            if (dimGroupAttributes.Any())
            {
                selectStatement.Append(",");
                selectStatement.Append(string.Join(", ", dimGroupAttributes.Select(a =>
                    a.Base.HasCodedRepresentation()
                        ? $"[CL_{a.Code}].[ID] AS [{a.Code}]"
                        : $"[ATTR].{a.SqlColumn()} AS [{a.Code}]"
                )));

                joinStatement.AppendLine($"LEFT JOIN [{dotStatDb.DataSchema}].{dsd.SqlDimGroupAttrTable(targetVersion, "ATTR")} ON FI.[SID] = ATTR.[SID] ");

                joinStatement.AppendLine(string.Join("\n", dimGroupAttributes.Where(a => a.Base.HasCodedRepresentation()).Select(a =>
                        $"LEFT JOIN [{dotStatDb.ManagementSchema}].[CL_{a.Codelist.DbId}] AS [CL_{a.Code}] ON ATTR.{a.SqlColumn()} = [CL_{a.Code}].[ITEM_ID]"
                    )
                ));

                whereStatement.Append($" OR EXISTS (SELECT TOP 1 1 FROM [{dotStatDb.DataSchema}].{dsd.SqlDimGroupAttrTable(targetVersion, "ATTR")} WHERE FI.[SID] = ATTR.[SID])");
            }

            // ------------------------------------------------------

            var sqlCommand =
                $@"CREATE VIEW [{dotStatDb.DataSchema}].{dsd.SqlDataDsdViewName(targetVersion)} 
                   AS
                        SELECT FI.[SID], {selectStatement}
                        FROM [{dotStatDb.DataSchema}].{dsd.SqlFilterTable("FI")} 
                        LEFT JOIN [{dotStatDb.DataSchema}].{dsd.SqlFactTable(targetVersion, "FA")} ON FI.[SID] = FA.[SID] 
                        {joinStatement}
                        {whereStatement}
                ";

            await dotStatDb.ExecuteNonQuerySqlAsync(sqlCommand, cancellationToken);
        }
        #endregion
        
    }
}
