﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using DotStat.Common.Configuration.Interfaces;
using DotStat.Common.Enums;
using DotStat.Db.DB;
using DotStat.DB.Reader;
using DotStat.Domain;
using Microsoft.Data.SqlClient;


namespace DotStat.Db.Engine.SqlServer
{
    public class SqlCodelistEngine : CodelistEngineBase<SqlDotStatDb>
    {
        public SqlCodelistEngine(IGeneralConfiguration generalConfiguration) :
            base(generalConfiguration)
        {
        }

        public override async Task<int> GetDbId(string sdmxId, string sdmxAgency, int verMajor, int verMinor, int? verPatch, SqlDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            var aretefactDbId = await dotStatDb.ExecuteScalarSqlWithParamsAsync(
                $@"SELECT [ART_ID] 
                    FROM [{dotStatDb.ManagementSchema}].[ARTEFACT] 
                   WHERE [ID] = @Id AND [AGENCY] = @Agency AND [TYPE] = @Type
                     AND [VERSION_1] = @Version1 AND [VERSION_2] = @Version2 
                     AND ([VERSION_3] = @Version3 OR ([VERSION_3] IS NULL AND @Version3 IS NULL))",
                cancellationToken,
                new SqlParameter("Id", SqlDbType.VarChar) { Value = sdmxId },
                new SqlParameter("Agency", SqlDbType.VarChar) { Value = sdmxAgency },                
                new SqlParameter("Type", SqlDbType.VarChar) { Value = DbTypes.GetDbType(SDMXArtefactType.CodeList) },
                new SqlParameter("Version1", SqlDbType.Int) { Value = verMajor },
                new SqlParameter("Version2", SqlDbType.Int) { Value = verMinor },
                new SqlParameter("Version3", SqlDbType.Int) { Value = ((object)verPatch) ?? DBNull.Value }
                );

            return (int)(aretefactDbId ?? -1);
        }

        public override async Task<int> InsertToArtefactTable(Codelist codelist, SqlDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            var artefactId = await dotStatDb.ExecuteScalarSqlWithParamsAsync(
                $@"INSERT 
                    INTO [{dotStatDb.ManagementSchema}].[ARTEFACT] 
                         ([SQL_ART_ID],[TYPE],[ID],[AGENCY],[VERSION_1],[VERSION_2],[VERSION_3],[LAST_UPDATED]) 
                  OUTPUT INSERTED.ART_ID 
                  VALUES (NULL, @Type, @Id, @Agency, @Version1, @Version2, @Version3, @DT_Now)",
                cancellationToken,
                new SqlParameter("Type", SqlDbType.VarChar) { Value = DbTypes.GetDbType(SDMXArtefactType.CodeList) },
                new SqlParameter("Id", SqlDbType.VarChar) { Value = codelist.Code },
                new SqlParameter("Agency", SqlDbType.VarChar) { Value = codelist.AgencyId },
                new SqlParameter("Version1", SqlDbType.Int) { Value = codelist.Version.Major },
                new SqlParameter("Version2", SqlDbType.Int) { Value = codelist.Version.Minor },
                new SqlParameter("Version3", SqlDbType.Int) { Value = (object)codelist.Version.Patch ?? DBNull.Value },
                new SqlParameter("DT_Now", DateTime.Now)
                );

            return (int)artefactId;
        }

        public async Task<IList<string>> GetCodesOfCodeList(Codelist codelist, SqlDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            var result = new List<string>();
            var command = $"SELECT [ID] FROM [{dotStatDb.ManagementSchema}].[{GetCodelistTableName(codelist)}]";
            using (var dr = await dotStatDb.ExecuteReaderSqlAsync(command, cancellationToken))
            {
                while (dr.Read())
                {
                    result.Add(dr.GetString(0));
                }
            }

            return result;
        }

        protected override async Task DeleteFromArtefactTableByDbId(int dbId, SqlDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            await dotStatDb.ExecuteNonQuerySqlWithParamsAsync(
                $@"DELETE
                    FROM [{dotStatDb.ManagementSchema}].[ARTEFACT] 
                   WHERE [ART_ID] = @DbId AND [TYPE] = 'CL'",
                cancellationToken,
                new SqlParameter("DbId", SqlDbType.VarChar) { Value = dbId }
                );
        }
        protected override string GetCodelistTableName(Codelist codelist)
        {
            return $"CL_{codelist.DbId}";
        }

        protected override async Task BuildDynamicCodelistTable(Codelist codelist, SqlDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            await dotStatDb.ExecuteNonQuerySqlAsync(
                $@"CREATE TABLE [{dotStatDb.ManagementSchema}].[{GetCodelistTableName(codelist)}]( 
                        [ITEM_ID] [int] IDENTITY(1,1) NOT NULL, 
                        [ID] [varchar](150) NOT NULL,
                    PRIMARY KEY CLUSTERED ( [ITEM_ID] ASC ) )",
                cancellationToken
                );
        }

        protected override async Task FillDynamicCodelistTable(Codelist codelist, SqlDotStatDb dotStatDb, CancellationToken cancellationToken, IReadOnlyList<string> codesToInsert = null)
        {
            codesToInsert ??= codelist.Codes.Select(c => c.Code).ToArray();

            using (var connection = dotStatDb.GetConnection()) 
            {
                using (var bulkCopy = new SqlBulkCopy(connection,
                    SqlBulkCopyOptions.TableLock | SqlBulkCopyOptions.UseInternalTransaction, null))
                {
                    bulkCopy.DestinationTableName =
                        $"[{dotStatDb.ManagementSchema}].[{GetCodelistTableName(codelist)}]";
                    bulkCopy.BatchSize = 30000;
                    bulkCopy.ColumnMappings.Add(0, 1);

                    await bulkCopy.WriteToServerAsync(
                        new BulkCopyEnumeratorDataReader<string>(codesToInsert, 1, s => new object[] { s }),
                        cancellationToken);
                }
                connection.Close();
                connection.Dispose();
            }
        }
    }
}
