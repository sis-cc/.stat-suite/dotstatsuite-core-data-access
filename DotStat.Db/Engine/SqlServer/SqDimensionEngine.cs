﻿using System;
using System.Data;
using System.Threading;
using System.Threading.Tasks;
using DotStat.Common.Configuration.Interfaces;
using DotStat.Common.Enums;
using DotStat.Db.DB;
using DotStat.Db.Util;
using DotStat.Domain;
using Microsoft.Data.SqlClient;

namespace DotStat.Db.Engine.SqlServer
{
    public class SqlDimensionEngine : DimensionEngineBase<SqlDotStatDb>
    {
        private readonly DataTypeEnumHelper _dataTypeEnumHelper;

        public SqlDimensionEngine(IGeneralConfiguration generalConfiguration, DataTypeEnumHelper dataTypeEnumHelper) : base(generalConfiguration)
        {
            _dataTypeEnumHelper = dataTypeEnumHelper;
        }

        public override async Task<int> GetDbId(string sdmxId, int dsdId, SqlDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            var componentDbId = await dotStatDb.ExecuteScalarSqlWithParamsAsync(
                $@"SELECT [COMP_ID] 
                FROM [{dotStatDb.ManagementSchema}].[COMPONENT] 
                WHERE [ID] = @Id AND [DSD_ID] = @DsdId AND [TYPE] IN (@DimensionType, @TimeDimensionType)",
                cancellationToken,
                new SqlParameter("Id", SqlDbType.VarChar) {Value = sdmxId},
                new SqlParameter("DsdId", SqlDbType.Int) {Value = dsdId},
                new SqlParameter("DimensionType", SqlDbType.VarChar) {Value = DbTypes.GetDbType(SDMXArtefactType.Dimension)},
                new SqlParameter("TimeDimensionType", SqlDbType.VarChar) {Value = DbTypes.GetDbType(SDMXArtefactType.TimeDimension)}
            );

            return (int)(componentDbId ?? -1);
        }

        public override async Task<int> InsertToComponentTable(Dimension dimension, SqlDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            return dimension.Base.TimeDimension
                ? (int) await dotStatDb.ExecuteScalarSqlWithParamsAsync(
                    $@"INSERT INTO [{dotStatDb.ManagementSchema}].[COMPONENT] 
                ([ID],[TYPE],[DSD_ID],[CL_ID],[ATT_ASS_LEVEL],[ATT_STATUS],[ENUM_ID])
                OUTPUT Inserted.COMP_ID
                VALUES (@Id, @Type, @DsdId, NULL, NULL, NULL, NULL)",
                    cancellationToken,
                    new SqlParameter("Id", SqlDbType.VarChar) {Value = dimension.Code},
                    new SqlParameter("Type", SqlDbType.VarChar) {Value = dimension.DbType},
                    new SqlParameter("DsdId", SqlDbType.Int) {Value = dimension.Dsd.DbId}
                )
                : (int) await dotStatDb.ExecuteScalarSqlWithParamsAsync(
                    $@"INSERT INTO [{dotStatDb.ManagementSchema}].[COMPONENT] 
                ([ID],[TYPE],[DSD_ID],[CL_ID],[ATT_ASS_LEVEL],[ATT_STATUS],[ENUM_ID])
                OUTPUT Inserted.COMP_ID
                VALUES (@Id, @Type, @DsdId, @ClId, NULL, NULL, @EnumId)",
                    cancellationToken,
                    new SqlParameter("Id", SqlDbType.VarChar) {Value = dimension.Code},
                    new SqlParameter("Type", SqlDbType.VarChar) {Value = dimension.DbType},
                    new SqlParameter("DsdId", SqlDbType.Int) {Value = dimension.Dsd.DbId},
                    new SqlParameter("ClId", SqlDbType.Int) {Value = dimension.Codelist != null ? dimension.Codelist.DbId : DBNull.Value },
                    new SqlParameter("EnumId", SqlDbType.BigInt) { Value = dimension.Base.HasCodedRepresentation() ? DBNull.Value : (object)_dataTypeEnumHelper.GetIdFromTextFormat(dimension.Base.Representation?.TextFormat) }
                );
        }

        protected override async Task DeleteFromComponentTableByDbId(int dbId, SqlDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            await dotStatDb.ExecuteNonQuerySqlWithParamsAsync(
                $@"DELETE
                FROM [{dotStatDb.ManagementSchema}].[COMPONENT] 
                WHERE [COMP_ID] = @DbId",
                cancellationToken,
                new SqlParameter("DbId", SqlDbType.VarChar) {Value = dbId}
            );
        }
    }
}
