﻿using DotStat.Common.Configuration.Interfaces;
using DotStat.Db.DB;
using DotStat.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using DotStat.Db.Util;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Data.Query;

namespace DotStat.Db.Engine.SqlServer
{
    public class SqlMetadataDataflowEngine : MetadataDataflowEngineBase<SqlDotStatDb>
    {

        public SqlMetadataDataflowEngine(IGeneralConfiguration generalConfiguration) : base(generalConfiguration)
        {
        }

        public override Task<int> GetDbId(string sdmxId, string sdmxAgency, int verMajor, int verMinor, int? verPatch, SqlDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        public override Task<int> InsertToArtefactTable(Dataflow artefact, SqlDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        public override async Task DropAllIndexes(Dataflow dataFlow, SqlDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            if (dataFlow.Dsd.Msd is null) return;

            var tables = new List<string> {
                $"{dataFlow.SqlMetadataDataflowTable((char)DbTableVersion.A)}",
                $"{dataFlow.SqlMetadataDataflowTable((char)DbTableVersion.B)}"
            };

            // Drop all existing indexes
            foreach (var table in tables)
            {
                await dotStatDb.DropAllIndexesOfTable(dotStatDb.DataSchema, table, cancellationToken);
            }
        }

        public override async Task Compress(Dataflow dataFlow, DataCompressionEnum dataCompression, SqlDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            if (dataCompression is DataCompressionEnum.NONE)
                return;// no compression needed

            if (dataFlow.Dsd.Msd is null) return;

            var tables = new List<string> {
                $"{dataFlow.SqlMetadataDataflowTable((char)DbTableVersion.A)}",
                $"{dataFlow.SqlMetadataDataflowTable((char)DbTableVersion.B)}"
            };

            // Create CCI indexes
            foreach (var table in tables)
            {
                await dotStatDb.CreateColumnstoreIndex(dotStatDb.DataSchema, table, $"CCI_{table}", dataCompression, cancellationToken);
            }
        }

        public override async Task AddUniqueConstraints(Dataflow dataFlow, bool clustered, SqlDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            if (dataFlow.Dsd.Msd is null) return;

            var dimensions = dataFlow.Dsd.Dimensions.Where(dim => !dim.Base.TimeDimension).ToList();
            var timeDimColumns = dataFlow.Dsd.TimeDimension is null ? "" : $",{DbExtensions.SqlPeriodStart()},{DbExtensions.SqlPeriodEnd()}";
            var uniqueColumns = dimensions.ToColumnList() + timeDimColumns;
            
            //A
            await dotStatDb.CreatePrimaryKey(
                $"[{dotStatDb.DataSchema}].{dataFlow.SqlMetadataDataflowTable((char)DbTableVersion.A)}",
                $"[PK_{dataFlow.SqlMetadataDataflowTable((char)DbTableVersion.A)}]", uniqueColumns, clustered, cancellationToken);

            //B
            await dotStatDb.CreatePrimaryKey(
                $"[{dotStatDb.DataSchema}].{dataFlow.SqlMetadataDataflowTable((char)DbTableVersion.B)}",
                $"[PK_{dataFlow.SqlMetadataDataflowTable((char)DbTableVersion.B)}]", uniqueColumns, clustered, cancellationToken);
        }

        protected override async Task BuildDynamicMetadataDataFlowTable(Dataflow dataflow, char targetVersion, SqlDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            //The codelist table doesnt exist, therefore the db is corrupted or the method is called by the unit tests
            foreach (var dimension in dataflow.Dsd.Dimensions.Where(x => x.Base.HasCodedRepresentation()))
            {
                if (!await dotStatDb.TableExists($"CL_{dimension.Codelist.DbId}", cancellationToken))
                {
                    return;
                }
            }

            //The table already exists
            if (await dotStatDb.TableExists($"{dataflow.SqlMetadataDataflowTable(targetVersion)}", cancellationToken))
            {
                return;
            }

            var dimensions = dataflow.Dsd.Dimensions.Where(dim => !dim.Base.TimeDimension).ToArray();
            //Change to allowNull=true when non coded dimensions are introduced. 
            //A value of 0 should be given when the dimension is not being referenced
            var dimensionColumnsWithType = dimensions.ToColumnList(true);
            var dimensionColumns = dimensions.ToColumnList();
            var table = dataflow.SqlMetadataDataflowTable(targetVersion);

            var timeDim = dataflow.Dsd.TimeDimension;
            var supportsDateTime = dataflow.Dsd.Base.HasSupportDateTimeAnnotation();

            var timeDimensionColumns = timeDim != null
                ? string.Join(",",
                    // NULL is stored when time is not referenced by the metadata.
                    new[] { timeDim }.ToColumnList(true, true),
                    //The maximum SQL data/datetime is stored when time is not referenced by the metadata.
                    DbExtensions.SqlPeriodStart(true, supportsDateTime, false),
                    //The minimum SQL data/datetime is stored when time is not referenced by the metadata.
                    DbExtensions.SqlPeriodEnd(true, supportsDateTime, false)
                ) + ","
                : null;

            var timeConstraint =
                timeDim != null ? $", [{DbExtensions.SqlPeriodStart()}], [{DbExtensions.SqlPeriodEnd()}] DESC" : null;

            string rowId = null;
            var uIndex = dimensionColumns + timeConstraint;

            //Time dimension requires two columns (period_start and period_end)
            if (dataflow.Dsd.Dimensions.Count > (timeDim != null ? 30 : 32))
            {
                rowId = $"[ROW_ID] AS ({dataflow.Dsd.SqlBuildRowIdFormula()}) PERSISTED NOT NULL,";
                uIndex = "[ROW_ID]";
            }

            var metaAttributeColumns = dataflow.Dsd.Msd.MetadataAttributes
                .ToColumnList(true);

            if (!string.IsNullOrWhiteSpace(metaAttributeColumns))
            {
                metaAttributeColumns += ',';
            }

            var clusteredPart = dataflow.Dsd.DataCompression == DataCompressionEnum.NONE ? "CLUSTERED" : "NONCLUSTERED";
            var constraint = dataflow.Dsd.DataCompression == DataCompressionEnum.COLUMNSTORE_ARCHIVE && !dataflow.Dsd.KeepHistory
                ? ""
                : $", CONSTRAINT [PK_{table}] PRIMARY KEY {clusteredPart} ({uIndex})";

var sqlCommand = $@"CREATE TABLE [{dotStatDb.DataSchema}].[{table}](
                        {rowId}
	                    {dimensionColumnsWithType},
                        {timeDimensionColumns}
                        {metaAttributeColumns}
                        [{DbExtensions.LAST_UPDATED_COLUMN}] [datetime] NOT NULL
                        {constraint}
                )";

            await dotStatDb.ExecuteNonQuerySqlAsync(sqlCommand, cancellationToken);

            //Apply data compression
            if (dataflow.Dsd.DataCompression != DataCompressionEnum.NONE)
            {
                await dotStatDb.ExecuteNonQuerySqlAsync(
                    $@"CREATE CLUSTERED COLUMNSTORE INDEX CCI_{table}
ON [{dotStatDb.DataSchema}].[{table}]
WITH(DATA_COMPRESSION = {dataflow.Dsd.DataCompression})", cancellationToken);

                if (dataflow.Dsd.DataCompression == DataCompressionEnum.COLUMNSTORE)
                {
                    await AddUniqueConstraints(dataflow, clustered: false, dotStatDb, cancellationToken);
                }
            }

        }

        public override async Task<bool> BuildDynamicMetadataDataFlowView(Dataflow dataflow, char targetVersion, SqlDotStatDb dotStatDb, bool sourceVersion, ICodeTranslator codeTranslator,
            CancellationToken cancellationToken)
        {
            // The dependent table does not exists, therefore the db is corrupted or the method is called by the unit tests
            if (!await dotStatDb.TableExists($"{dataflow.SqlMetadataDataflowTable(targetVersion)}", cancellationToken))
            {
                return false;
            }

            // The dependent table does not exists, therefore the db is corrupted or the method is called by the unit tests
            if (!await dotStatDb.TableExists($"{dataflow.Dsd.SqlMetadataDataSetTable(targetVersion)}", cancellationToken))
            {
                return false;
            }

            // The view already exists, therefore no need to recreate it.
            if (await dotStatDb.ViewExists($"{dataflow.SqlMetadataDataflowViewName(targetVersion, sourceVersion)}", cancellationToken))
            {
                return false;
            }

            var dimensions = dataflow.Dsd.Dimensions.Where(dim => !dim.Base.TimeDimension).ToList();

            // SELECT

            var selectStatement = new StringBuilder();
            var dfSelectStatement = new StringBuilder();
            var dsSelectStatement = new StringBuilder();
            var joinDsdDf = new StringBuilder();

            // dimensions
            selectStatement.Append(
                string.Join(", ", dimensions.Select(d => d.SqlColumn(externalColumn: true))));

            dfSelectStatement.Append(string.Join(", ",
                !sourceVersion
                    ? dimensions.Select(d =>
                        d.Base.HasCodedRepresentation()
                            ? $"CASE WHEN [CL_{d.Code}].[ID] IS NULL THEN '{DbExtensions.DimensionSwitchedOff}' ELSE [CL_{d.Code}].[ID] END AS [{d.Code}]"
                            : $"CASE WHEN [ME].{d.SqlColumn()} IS NULL THEN '{DbExtensions.DimensionSwitchedOff}' ELSE [ME].{d.SqlColumn()} END AS [{d.Code}]")
                    : dimensions.Select(d =>
                        d.Base.HasCodedRepresentation()
                            ? $"CASE WHEN {d.SqlColumn()} IS NULL THEN 0 ELSE {d.SqlColumn()} END AS [{d.Code}]"
                            : $"CASE WHEN {d.SqlColumn()} IS NULL THEN '{DbExtensions.DimensionSwitchedOff}' ELSE {d.SqlColumn()} END AS [{d.Code}]")
            ));

            dsSelectStatement.Append(string.Join(", ",
                !sourceVersion
                    ? dimensions.Select(d => $"'{DbExtensions.DimensionSwitchedOff}' AS [{d.Code}]")
                    : dimensions.Select(d => d.Base.HasCodedRepresentation()
                        ? $"0 AS [{d.Code}]"
                        : $"'{DbExtensions.DimensionSwitchedOff}' AS [{d.Code}]")));

            joinDsdDf.Append(string.Join(" AND ", dimensions.Select(d => $"DF.[{d.Code}] = DSD.[{d.Code}]")));

            // time dimension
            var timeDim = dataflow.Dsd.TimeDimension;
            var (sqlMin, sqlMax) = dataflow.Dsd.Base.HasSupportDateTimeAnnotation()
                ? (DateTime.MinValue.ToString("yyyy-MM-dd HH:mm:ss"), DateTime.MaxValue.ToString("yyyy-MM-dd HH:mm:ss"))
                : (DateTime.MinValue.ToString("yyyy-MM-dd"), DateTime.MaxValue.ToString("yyyy-MM-dd"));

            if (timeDim != null)
            {
                selectStatement
                    .Append(", ")
                    .Append(string.Join(", ",
                        @$"[{dataflow.Dsd.Base.TimeDimension.Id}],
                        [{DbExtensions.SqlPeriodStart()}],
                        [{DbExtensions.SqlPeriodEnd()}]"
                    ));

                dfSelectStatement
                    .Append(", ")
                    .Append(string.Join(", ",
                        $"CASE WHEN {timeDim.SqlColumn()} IS NULL THEN '{DbExtensions.DimensionSwitchedOff}' ELSE {timeDim.SqlColumn()} END AS [{dataflow.Dsd.Base.TimeDimension.Id}]",
                        DbExtensions.SqlPeriodStart(),
                        DbExtensions.SqlPeriodEnd()
                    ));

                dsSelectStatement
                    .Append(", ")
                    .Append(string.Join(", ",
                        $"'{DbExtensions.DimensionSwitchedOff}' AS [{dataflow.Dsd.Base.TimeDimension.Id}]",
                        $"'{sqlMax}' AS [{DbExtensions.SqlPeriodStart()}]",
                        $"'{sqlMin}' AS [{DbExtensions.SqlPeriodEnd()}]"
                    ));

                joinDsdDf.Append(
                    $" AND DF.[{dataflow.Dsd.Base.TimeDimension.Id}] = DSD.[{dataflow.Dsd.Base.TimeDimension.Id}]");
            }

            // metadata attributes
            if (dataflow.Dsd.Msd.MetadataAttributes.Any())
            {
                selectStatement
                    .Append(",")
                    .Append(string.Join(", ", dataflow.Dsd.Msd.MetadataAttributes.Select(a => $"[{a.HierarchicalId}]")));

                dfSelectStatement
                    .Append(",")
                    .Append(string.Join(", ", dataflow.Dsd.Msd.MetadataAttributes.Select(a => $"{a.SqlColumn()} AS [{a.HierarchicalId}]")));

                dsSelectStatement
                    .Append(',')
                    .Append(string.Join(", ", dataflow.Dsd.Msd.MetadataAttributes.Select(a => $"[ME].{a.SqlColumn()} AS [{a.HierarchicalId}]")));
            }

            // LAST_UPDATED
            var updatedAfterColumns = $", [{DbExtensions.LAST_UPDATED_COLUMN}]";

            // JOIN dimensions
            var joinStatement = new StringBuilder();
            joinStatement.AppendLine(string.Join("\n",
                dimensions
                    .Where(d => d.Base.HasCodedRepresentation())
                    .Select(d => $"LEFT JOIN [{dotStatDb.ManagementSchema}].[CL_{d.Codelist.DbId}] AS [CL_{d.Code}] ON [ME].{d.SqlColumn()} = [CL_{d.Code}].[ITEM_ID]")));

            // Generate WHERE clause out of available content constraint of dataflow (if any)
            var constraintClause = !sourceVersion
                ? string.Empty
                : Predicate.BuildWhereForObservationLevel(
                    null,
                    dataflow,
                    codeTranslator,
                    nullTreatment: NullTreatment.IncludeNullsAndSwitchOff,
                    includeTimeConstraints: true,
                    useExternalValues: false,
                    useExternalColumnNames: true,
                    includeSpecialValues: true,
                    tableAlias: null,
                    filterLastUpdated: false,
                    serverType: dotStatDb.GetDbType());

            var sqlCommand =
                $@"CREATE VIEW [{dotStatDb.DataSchema}].{dataflow.SqlMetadataDataflowViewName(targetVersion, sourceVersion)} 
AS 
WITH [DF_LEVEL] AS ( 
    SELECT {dsSelectStatement}{updatedAfterColumns}
    FROM [{dotStatDb.DataSchema}].{dataflow.Dsd.SqlMetadataDataSetTable(targetVersion)} ME
    WHERE DF_ID = {dataflow.DbId}
    UNION ALL 
    SELECT {dfSelectStatement}{updatedAfterColumns}
    FROM [{dotStatDb.DataSchema}].{dataflow.SqlMetadataDataflowTable(targetVersion)} ME
    {(!sourceVersion ? joinStatement : string.Empty)}
)
{(!string.IsNullOrEmpty(constraintClause) ? $"SELECT * FROM (" : string.Empty)}
 SELECT {selectStatement}{updatedAfterColumns} FROM DF_LEVEL DF
 UNION ALL
 SELECT {selectStatement}{updatedAfterColumns} FROM [{dotStatDb.DataSchema}].[{dataflow.Dsd.SqlMetadataDsdViewName(targetVersion, sourceVersion)}] DSD
 WHERE NOT EXISTS ( 
		SELECT TOP 1 1 FROM DF_LEVEL DF 
        WHERE {joinDsdDf}
)
{(!string.IsNullOrEmpty(constraintClause) ? $") A WHERE ({constraintClause})" : string.Empty)}";

            await dotStatDb.ExecuteNonQuerySqlAsync(sqlCommand, cancellationToken);
            return true;
        }

        protected override Task DeleteFromArtefactTableByDbId(int dbId, SqlDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        public override async Task<bool> BuildDeletedView(Dataflow dataFlow, char targetVersion, SqlDotStatDb dotStatDb, bool sourceVersion, CancellationToken cancellationToken)
        {
            //The dependent table does not exists, therefore the db is corrupted or the method is called by the unit tests
            if (!await dotStatDb.TableExists($"{dataFlow.Dsd.SqlDeletedMetadataTable(targetVersion)}", cancellationToken))
            {
                return false;
            }

            //The view already exists, therefore no need to recreate it.
            if (await dotStatDb.ViewExists($"{dataFlow.SqlDeletedMetadataDataflowViewName(targetVersion, sourceVersion)}", cancellationToken))
                return false;

            //Dimensions
            var dimensions = dataFlow.Dsd.Dimensions
                .Where(dim => !dim.Base.TimeDimension)
                .ToList();

            //Time dimension columns
            var timeDim = dataFlow.Dsd.TimeDimension;
            var (sqlMax, sqlMaxMinOne) = dataFlow.Dsd.Base.HasSupportDateTimeAnnotation()
                ? (DateTime.MaxValue.ToString("yyyy-MM-dd HH:mm:ss"), DateTime.MaxValue.AddYears(-1).ToString("yyyy-MM-dd HH:mm:ss"))
                : (DateTime.MaxValue.ToString("yyyy-MM-dd"), DateTime.MaxValue.AddYears(-1).ToString("yyyy-MM-dd"));

            var innerTimeDimensionColumns = string.Empty;
            var innerTimeDimensionColumns4GroupBy = string.Empty;
            var outerTimeDimensionColumns = string.Empty;

            if (timeDim != null)
            {
                innerTimeDimensionColumns4GroupBy = ", " + string.Join(", ", 
                    $"[{DbExtensions.TimeDimColumn}]", 
                    DbExtensions.SqlPeriodStart(), 
                    DbExtensions.SqlPeriodEnd());

                innerTimeDimensionColumns = ", " + string.Join(", ",
                    !sourceVersion
                        ? $"[{DbExtensions.TimeDimColumn}]"
                        : $"[{DbExtensions.TimeDimColumn}] AS [{dataFlow.Dsd.Base.TimeDimension.Id}]",
                    DbExtensions.SqlPeriodStart(),
                    DbExtensions.SqlPeriodEnd());

                outerTimeDimensionColumns = ", " + string.Join(", ",
                    @$"CASE 
                            WHEN [DEL].[{DbExtensions.SqlPeriodStart()}] = '{sqlMax}' THEN '{DbExtensions.DimensionSwitchedOff}'
                            WHEN [DEL].[{DbExtensions.SqlPeriodStart()}] = '{sqlMaxMinOne}' THEN '{DbExtensions.DimensionWildCarded}'
                            ELSE [DEL].[{DbExtensions.TimeDimColumn}]
                      END AS [{dataFlow.Dsd.Base.TimeDimension.Id}]",
                    $"[{DbExtensions.SqlPeriodStart()}]",
                    $"[{DbExtensions.SqlPeriodEnd()}]");
            }


            //Dimensions
            var innerDimensionsColumns = string.Empty;
            var innerDimensionColumns4GroupBy = string.Empty;
            var outerDimensionsColumns = string.Empty;

            if (dimensions.Any())
            {
                innerDimensionColumns4GroupBy = string.Join(", ",
                    dimensions.Select(
                        d => $"{d.SqlColumn()}"));

                innerDimensionsColumns = ", " + string.Join(", ", 
                    dimensions.Select(
                        d => $"{d.SqlColumn() + (sourceVersion ? $" AS [{d.Code}]" : string.Empty)}"));

                outerDimensionsColumns = ", " + string.Join(", ",
                    dimensions.Select(d =>
                        (d.Base.HasCodedRepresentation()
                            ? @$"CASE
                                    WHEN [DEL].{d.SqlColumn()} = {DbExtensions.DimensionSwitchedOffDbValue} THEN '{DbExtensions.DimensionSwitchedOff}'
                                    WHEN [DEL].{d.SqlColumn()} = {DbExtensions.DimensionWildCardedDbValue} THEN '{DbExtensions.DimensionWildCarded}'
                                    ELSE [CL_{d.Code}].[ID]
                                END"
                            : $"{d.SqlColumn()}") + $" AS [{d.Code}]"));
            }

            //Attributes
            var innerAttributeColumns4SomeComp = dataFlow.Dsd.Msd.MetadataAttributes.Any()
                ? "," + string.Join(",", dataFlow.Dsd.Msd.MetadataAttributes.Select(a => $"MAX( {a.SqlColumn()} ) AS [{a.HierarchicalId}]"))
                : string.Empty;

            var innerAttributeColumns4NoComp = dataFlow.Dsd.Msd.MetadataAttributes.Any()
                ? "," + string.Join(",", dataFlow.Dsd.Msd.MetadataAttributes.Select(a => $"NULL AS [{a.HierarchicalId}]"))
                : string.Empty;

            var outerAttributeColumns = dataFlow.Dsd.Msd.MetadataAttributes.Any()
                ? "," + string.Join(",", dataFlow.Dsd.Msd.MetadataAttributes.Select(a => $" [{a.HierarchicalId}]"))
                : string.Empty;

            var joinStatements = string.Join(Environment.NewLine, dimensions.Where(d => d.Base.HasCodedRepresentation()).Select(d =>
                    $"LEFT JOIN [{dotStatDb.ManagementSchema}].[CL_{d.Codelist.DbId}] AS [CL_{d.Code}] ON [DEL].{d.SqlColumn()} = [CL_{d.Code}].[ITEM_ID]"));

            var innerSelectPartCommon =
                $@"SELECT MAX( {DbExtensions.LAST_UPDATED_COLUMN} ) AS {DbExtensions.LAST_UPDATED_COLUMN}
                {innerTimeDimensionColumns}
                {innerDimensionsColumns}";

            var innerFromPart = $@"FROM [{dotStatDb.DataSchema}].{dataFlow.Dsd.SqlDeletedMetadataTable(targetVersion)} ";

            var innerWherePart4NoComp = $@"WHERE [DF_ID] = {dataFlow.DbId} " +
                                        (dataFlow.Dsd.Msd.MetadataAttributes.Any()
                                            ? Environment.NewLine + "AND " + string.Join(" AND ",
                                                dataFlow.Dsd.Msd.MetadataAttributes.Select(a => $"{a.SqlColumn()} IS NULL"))
                                            : string.Empty) + Environment.NewLine;

            var innerWherePart4SomeComp = $@"WHERE [DF_ID] = {dataFlow.DbId} " +
                                          "AND (" +
                                          (dataFlow.Dsd.Msd.MetadataAttributes.Any()
                                              ? Environment.NewLine + string.Join(" OR ",
                                                  dataFlow.Dsd.Msd.MetadataAttributes.Select(a => $"{a.SqlColumn()} IS NOT NULL"))
                                              : "1 = 1") + " )";

            var innerGroupByPart = $@"GROUP BY {innerDimensionColumns4GroupBy}{innerTimeDimensionColumns4GroupBy} ";

            var innerUnionSql =
                $@"{innerSelectPartCommon}{innerAttributeColumns4SomeComp}
                   {innerFromPart} 
                   {innerWherePart4SomeComp}
                   {innerGroupByPart}
                UNION
                   {innerSelectPartCommon}{innerAttributeColumns4NoComp}
                   {innerFromPart}
                   {innerWherePart4NoComp}
                   {innerGroupByPart}";

            var sqlCommand = !sourceVersion
                ? $@"CREATE VIEW [{dotStatDb.DataSchema}].{dataFlow.SqlDeletedMetadataDataflowViewName(targetVersion, false)} 
                    AS
                    SELECT {DbExtensions.LAST_UPDATED_COLUMN}
                        {outerTimeDimensionColumns}
                        {outerDimensionsColumns}
                        {outerAttributeColumns}
                    FROM ( {innerUnionSql} ) AS [DEL]
                        {joinStatements}"
                : $@"CREATE VIEW [{dotStatDb.DataSchema}].{dataFlow.SqlDeletedMetadataDataflowViewName(targetVersion, true)} 
                    AS
                    {innerUnionSql}";

            await dotStatDb.ExecuteNonQuerySqlAsync(sqlCommand, cancellationToken);

            return true;
        }

        public override async Task<bool> CreateAndFillHprDataTables(Dataflow dataflow, char targetVersion, SqlDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            var tableName = dataflow.SqlOptimizedMetadataDataflowTable(targetVersion, dataflow.Dsd.NextMetadataHPRVersion);
            var deletedTableName = dataflow.SqlOptimizedDeletedMetadataDataflowTable(targetVersion, dataflow.Dsd.NextMetadataHPRVersion);

            await dotStatDb.ExecuteNonQuerySqlAsync($@"SELECT * INTO [{dotStatDb.DataSchema}].[{tableName}] 
                FROM [{dotStatDb.DataSchema}].{dataflow.SqlMetadataDataflowViewName(targetVersion, true)}
                {dotStatDb.DataSpace.MAXDOPForDsdOptimization.GetMaxDopPart()}", cancellationToken);

            await dotStatDb.ExecuteNonQuerySqlAsync($@"SELECT * INTO [{dotStatDb.DataSchema}].[{deletedTableName}] 
                FROM [{dotStatDb.DataSchema}].{dataflow.SqlDeletedMetadataDataflowViewName(targetVersion, true)}
                {dotStatDb.DataSpace.MAXDOPForDsdOptimization.GetMaxDopPart()}", cancellationToken);

            var partitionSchemaExists =
                await dotStatDb.PartitionSchemeExists(dotStatDb.DataSpace.DefaultPartitioningScheme, cancellationToken);

            var partitionIndex = partitionSchemaExists && await dotStatDb.ColumnExists(tableName, dotStatDb.DataSpace.DefaultPartitioningColumn,
                cancellationToken);

            var indexColumns = dataflow.Dsd.GetIndexColumns(dotStatDb.DataSpace.DefaultPartitioningColumn, false, true);
            await dotStatDb.ExecuteNonQuerySqlAsync(dotStatDb.GetClusteredIndex(tableName, indexColumns, partitionIndex), cancellationToken);

            partitionIndex = partitionSchemaExists && await dotStatDb.ColumnExists(deletedTableName, dotStatDb.DataSpace.DefaultPartitioningColumn,
                cancellationToken);

            indexColumns = dataflow.Dsd.GetIndexColumns(dotStatDb.DataSpace.DefaultPartitioningColumn, true, true);
            await dotStatDb.ExecuteNonQuerySqlAsync(dotStatDb.GetClusteredIndex(deletedTableName, indexColumns, partitionIndex), cancellationToken);

            await dotStatDb.ExecuteNonQuerySqlAsync(dotStatDb.GetClusteredColumnStoreIndex(tableName), cancellationToken);
            await dotStatDb.ExecuteNonQuerySqlAsync(dotStatDb.GetClusteredColumnStoreIndex(deletedTableName), cancellationToken);
            
            return true;
        }

        public override async Task<bool> CreateHPRMetadataViews(Dataflow dataflow, char tableVersion, SqlDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            var dimensions = dataflow.Dsd.Dimensions.Where(dim => !dim.Base.TimeDimension).ToList();
            var timeDim = dataflow.Dsd.TimeDimension;
            var (sqlMax, sqlMaxMinOne) = dataflow.Dsd.Base.HasSupportDateTimeAnnotation()
                ? (DateTime.MaxValue.ToString("yyyy-MM-dd HH:mm:ss"), DateTime.MaxValue.AddYears(-1).ToString("yyyy-MM-dd HH:mm:ss"))
                : (DateTime.MaxValue.ToString("yyyy-MM-dd"), DateTime.MaxValue.AddYears(-1).ToString("yyyy-MM-dd"));

            var selectStatement = new StringBuilder();
            var selectStatementForDeleted = new StringBuilder();

            if (dimensions.Any())
            {
                selectStatement.AppendLine(string.Join(", ",
                    dimensions.Select(d =>
                        d.Base.HasCodedRepresentation()
                            ? $@"CASE 
                                    WHEN [ME].{d.Code} = 0 THEN '{DbExtensions.DimensionSwitchedOff}' 
                                    ELSE {$"[CL_{d.Code}].[ID]"} 
                                END AS [{d.Code}]"
                            : $"[ME].{d.Code}")));

                selectStatementForDeleted.AppendLine(string.Join(", ",
                    dimensions.Select(d =>
                        d.Base.HasCodedRepresentation()
                            ? $@"CASE 
                                    WHEN [ME].{d.Code} = 0 THEN '{DbExtensions.DimensionSwitchedOff}' 
                                    WHEN [ME].{d.Code} = -1 THEN '{DbExtensions.DimensionWildCarded}' 
                                    ELSE {$"[CL_{d.Code}].[ID]"} 
                                END AS [{d.Code}]"
                            : $"[ME].{d.Code}")));
            }

            if (timeDim != null)
            {
                selectStatement
                    .Append(", ")
                    .AppendLine(string.Join(", ",
                        $@"CASE 
                            WHEN [ME].[{dataflow.Dsd.Base.TimeDimension.Id}] = '0' THEN '{DbExtensions.DimensionSwitchedOff}' 
                            ELSE [{dataflow.Dsd.Base.TimeDimension.Id}] 
                        END AS [{dataflow.Dsd.Base.TimeDimension.Id}]",
                        DbExtensions.SqlPeriodStart(),
                        DbExtensions.SqlPeriodEnd()));

                selectStatementForDeleted
                    .Append(", ")
                    .AppendLine(string.Join(", ",
                        $@"CASE 
                            WHEN [ME].[{DbExtensions.SqlPeriodStart()}] = '{sqlMax}' THEN '{DbExtensions.DimensionSwitchedOff}' 
                            WHEN [ME].[{DbExtensions.SqlPeriodStart()}] = '{sqlMaxMinOne}' THEN '{DbExtensions.DimensionWildCarded}' 
                            ELSE [{dataflow.Dsd.Base.TimeDimension.Id}] 
                        END AS [{dataflow.Dsd.Base.TimeDimension.Id}]",
                        DbExtensions.SqlPeriodStart(),
                        DbExtensions.SqlPeriodEnd()));
            }

            if (dataflow.Dsd.Msd.MetadataAttributes.Any())
            {
                var metadataAttributes = string.Join(", ", dataflow.Dsd.Msd.MetadataAttributes.Select(a => $"[ME].[{a.HierarchicalId}]"));
                
                selectStatement
                    .Append(", ")
                    .AppendLine(metadataAttributes);

                selectStatementForDeleted
                    .Append(", ")
                    .AppendLine(metadataAttributes);
            }

            var updatedAfterColumns = $", [{DbExtensions.LAST_UPDATED_COLUMN}]";

            var joinStatement = new StringBuilder();

            joinStatement.AppendLine(string.Join("\n",
                dimensions
                    .Where(d => d.Base.HasCodedRepresentation())
                    .Select(d => $"LEFT JOIN [{dotStatDb.ManagementSchema}].[CL_{d.Codelist.DbId}] AS [CL_{d.Code}] ON [ME].{d.Code} = [CL_{d.Code}].[ITEM_ID]")));

            await dotStatDb.ExecuteNonQuerySqlAsync(
                $@"ALTER VIEW [{dotStatDb.DataSchema}].{dataflow.SqlMetadataDataflowViewName(tableVersion, false)} 
                            AS 
                            SELECT {selectStatement}{updatedAfterColumns} FROM [{dotStatDb.DataSchema}].[{dataflow.SqlOptimizedMetadataDataflowTable(tableVersion, dataflow.Dsd.NextMetadataHPRVersion)}] ME
                            {joinStatement}",
                cancellationToken);

            await dotStatDb.ExecuteNonQuerySqlAsync(
                $@"ALTER VIEW [{dotStatDb.DataSchema}].{dataflow.SqlDeletedMetadataDataflowViewName(tableVersion, false)} 
                            AS 
                            SELECT {selectStatementForDeleted}{updatedAfterColumns} FROM [{dotStatDb.DataSchema}].[{dataflow.SqlOptimizedDeletedMetadataDataflowTable(tableVersion, dataflow.Dsd.NextMetadataHPRVersion)}] ME
                            {joinStatement}",
                cancellationToken);

            return true;
        }
    }
}