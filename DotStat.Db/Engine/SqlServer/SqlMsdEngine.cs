﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using DotStat.Common.Configuration.Interfaces;
using DotStat.Common.Enums;
using DotStat.Db.DB;
using DotStat.Domain;
using Microsoft.Data.SqlClient;

namespace DotStat.Db.Engine.SqlServer
{
    public class SqlMsdEngine : MsdEngineBase<SqlDotStatDb>
    {
        public SqlMsdEngine(IGeneralConfiguration generalConfiguration) : base(generalConfiguration)
        {
        }
        
        public override async Task<int> GetDbId(string sdmxId, string sdmxAgency, int verMajor, int verMinor, int? verPatch, SqlDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            var aretefactDbId = await dotStatDb.ExecuteScalarSqlWithParamsAsync(
                $@"SELECT [ART_ID] 
                    FROM [{dotStatDb.ManagementSchema}].[ARTEFACT] 
                   WHERE [ID] = @Id AND [AGENCY] = @Agency AND [TYPE] = @Type
                     AND [VERSION_1] = @Version1 AND [VERSION_2] = @Version2 
                     AND ([VERSION_3] = @Version3 OR ([VERSION_3] IS NULL AND @Version3 IS NULL))",
                cancellationToken,
                new SqlParameter("Id", SqlDbType.VarChar) { Value = sdmxId },
                new SqlParameter("Agency", SqlDbType.VarChar) { Value = sdmxAgency },                
                new SqlParameter("Type", SqlDbType.VarChar) { Value = DbTypes.GetDbType(SDMXArtefactType.Msd) },
                new SqlParameter("Version1", SqlDbType.Int) { Value = verMajor },
                new SqlParameter("Version2", SqlDbType.Int) { Value = verMinor },
                new SqlParameter("Version3", SqlDbType.Int) { Value = ((object)verPatch) ?? DBNull.Value }

                );

            return (int)(aretefactDbId ?? -1);
        }

        public override async Task<int> InsertToArtefactTable(Msd msd, SqlDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            var artefactId = await dotStatDb.ExecuteScalarSqlWithParamsAsync(
                $@"INSERT 
                    INTO [{dotStatDb.ManagementSchema}].[ARTEFACT] 
                       ([SQL_ART_ID],[TYPE],[ID],[AGENCY],[VERSION_1],[VERSION_2],[VERSION_3],[LAST_UPDATED]) 
                  OUTPUT INSERTED.ART_ID 
                  VALUES (NULL, @Type, @Id, @Agency, @Version1, @Version2, @Version3, @DT_Now)",
                cancellationToken,
                new SqlParameter("Type", SqlDbType.VarChar) { Value = DbTypes.GetDbType(SDMXArtefactType.Msd) },
                new SqlParameter("Id", SqlDbType.VarChar) { Value = msd.Code },
                new SqlParameter("Agency", SqlDbType.VarChar) { Value = msd.AgencyId },
                new SqlParameter("Version1", SqlDbType.Int) { Value = msd.Version.Major },
                new SqlParameter("Version2", SqlDbType.Int) { Value = msd.Version.Minor },
                new SqlParameter("Version3", SqlDbType.Int) { Value = (object)msd.Version.Patch ?? DBNull.Value },
                new SqlParameter("DT_Now", DateTime.Now)
                );

            return (int)artefactId;
        }

        public override Task<bool> CreateDynamicDbObjects(Msd artefact, SqlDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        public override async Task DropAllIndexes(Dsd dsd, SqlDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            if (dsd.Msd is null) return;

            var tables = new List<string> {
                $"{dsd.SqlMetadataDataStructureTable((char)DbTableVersion.A)}",
                $"{dsd.SqlMetadataDataStructureTable((char)DbTableVersion.B)}",
                $"{dsd.SqlDeletedMetadataTable((char)DbTableVersion.A)}",
                $"{dsd.SqlDeletedMetadataTable((char)DbTableVersion.B)}"
            };

            // Drop all existing indexes
            foreach (var table in tables)
            {
                await dotStatDb.DropAllIndexesOfTable(dotStatDb.DataSchema, table, cancellationToken);
            }
        }

        public override async Task Compress(Dsd dsd, DataCompressionEnum dataCompression, SqlDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            if (dataCompression is DataCompressionEnum.NONE)
                return;// no compression needed

            if (dsd.Msd is null) return;

            var tables = new List<string> {
                $"{dsd.SqlMetadataDataStructureTable((char)DbTableVersion.A)}",
                $"{dsd.SqlMetadataDataStructureTable((char)DbTableVersion.B)}",
                $"{dsd.SqlDeletedMetadataTable((char)DbTableVersion.A)}",
                $"{dsd.SqlDeletedMetadataTable((char)DbTableVersion.B)}",
            };

            // Create CCI indexes
            foreach (var table in tables)
            {
                await dotStatDb.CreateColumnstoreIndex(dotStatDb.DataSchema, table, $"CCI_{table}", dataCompression, cancellationToken);
            }
        }

        public override async Task AddUniqueConstraints(Dsd dsd, bool clustered, SqlDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            if (dsd.Msd is null) return;

            var dimensions = dsd.Dimensions.Where(dim => !dim.Base.TimeDimension).ToList();
            var timeDimColumns = dsd.TimeDimension is null ? "" : $",{DbExtensions.SqlPeriodStart()},{DbExtensions.SqlPeriodEnd()}";
            var uniqueColumns =
            //Time dimension requires two columns (period_start and period_end)
            dsd.Dimensions.Count > (dsd.TimeDimension != null ? 30 : 32)?
                "[ROW_ID]" : dimensions.ToColumnList() + timeDimColumns;
            
            //A
            await dotStatDb.CreatePrimaryKey(
                $"[{dotStatDb.DataSchema}].{dsd.SqlMetadataDataStructureTable((char)DbTableVersion.A)}",
                $"[PK_{dsd.SqlMetadataDataStructureTable((char)DbTableVersion.A)}]", uniqueColumns, clustered, cancellationToken);
            //B
            await dotStatDb.CreatePrimaryKey(
                $"[{dotStatDb.DataSchema}].{dsd.SqlMetadataDataStructureTable((char)DbTableVersion.B)}",
                $"[PK_{dsd.SqlMetadataDataStructureTable((char)DbTableVersion.B)}]", uniqueColumns, clustered, cancellationToken);
        }

        #region protected
        protected override async Task DeleteFromArtefactTableByDbId(int dbId, SqlDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            await dotStatDb.ExecuteNonQuerySqlWithParamsAsync(
                $@"DELETE
                    FROM [{dotStatDb.ManagementSchema}].[ARTEFACT] 
                   WHERE [ART_ID] = @DbId AND [TYPE] = @Type",
                cancellationToken,
                new SqlParameter("DbId", SqlDbType.VarChar) { Value = dbId },
                        new SqlParameter("Type", SqlDbType.VarChar) { Value = DbTypes.GetDbType(SDMXArtefactType.Msd) }
                );
        }
        protected override async Task BuildDynamicMetaDataTable(Dsd dsd, char targetVersion, SqlDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            var dimensions = dsd.Dimensions.Where(dim => !dim.Base.TimeDimension).ToArray();
            //Change to allowNull=true when non coded dimensions are introduced. 
            //A value of 0 should be given when the dimension is not being referenced
            var dimensionColumnsWithType = dimensions.ToColumnList(true);
            var dimensionColumns = dimensions.ToColumnList();
            var table = dsd.SqlMetadataDataStructureTable(targetVersion);
            
            var timeDim = dsd.TimeDimension;
            var supportsDateTime = dsd.Base.HasSupportDateTimeAnnotation();

            var timeDimensionColumns = timeDim != null
                ? string.Join(",",
                    // NULL is stored when time is not referenced by the metadata.
                    new[] { timeDim }.ToColumnList(true, true),
                    //The maximum SQL data/datetime is stored when time is not referenced by the metadata.
                    DbExtensions.SqlPeriodStart(true, supportsDateTime, false),
                    //The minimum SQL data/datetime is stored when time is not referenced by the metadata.
                    DbExtensions.SqlPeriodEnd(true, supportsDateTime, false)
                ) + ","
                : null;

            var timeConstraint =
                timeDim != null ? $", [{DbExtensions.SqlPeriodStart()}], [{DbExtensions.SqlPeriodEnd()}] DESC" : null;
            
            string rowId = null;
            var uIndex = dimensionColumns + timeConstraint;

            //Time dimension requires two columns (period_start and period_end)
            if (dsd.Dimensions.Count > (timeDim != null ? 30: 32))
            {
                rowId = $"[ROW_ID] AS ({dsd.SqlBuildRowIdFormula()}) PERSISTED NOT NULL,";
                uIndex = "[ROW_ID]";
            }
            
            var metaAttributeColumns = dsd.Msd.MetadataAttributes
                .ToColumnList(true);

            if (!string.IsNullOrWhiteSpace(metaAttributeColumns))
            {
                metaAttributeColumns += ',';
            }

            var clusteredPart = dsd.DataCompression == DataCompressionEnum.NONE ? "CLUSTERED" : "NONCLUSTERED";
            var constraint = dsd.DataCompression == DataCompressionEnum.COLUMNSTORE_ARCHIVE && !dsd.KeepHistory
                ? ""
                : $", CONSTRAINT [PK_{table}] PRIMARY KEY {clusteredPart} ({uIndex})";

            var sqlCommand = $@"CREATE TABLE [{dotStatDb.DataSchema}].[{table}](
                        {rowId}
	                    {dimensionColumnsWithType},
                        {timeDimensionColumns}
                        {metaAttributeColumns}
                        [{DbExtensions.LAST_UPDATED_COLUMN}] [datetime] NOT NULL
                        {constraint}
                )";

            await dotStatDb.ExecuteNonQuerySqlAsync(sqlCommand, cancellationToken);

            //Apply data compression
            if (dsd.DataCompression != DataCompressionEnum.NONE)
            {
                await dotStatDb.ExecuteNonQuerySqlAsync(
                    $@"CREATE CLUSTERED COLUMNSTORE INDEX CCI_{table}
ON [{dotStatDb.DataSchema}].[{table}]
WITH(DATA_COMPRESSION = {dsd.DataCompression})", cancellationToken);

                if (dsd.DataCompression == DataCompressionEnum.COLUMNSTORE)
                {
                    await AddUniqueConstraints(dsd, clustered: false, dotStatDb, cancellationToken);
                }
            }

        }

        protected override async Task BuildDeletedTable(Dsd dsd, char targetVersion, SqlDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            var dimensions = dsd.Dimensions.Where(dim => !dim.Base.TimeDimension).ToArray();
            var dimensionColumnsWithType = dimensions.ToColumnList(true, allowNull: true);
            var metaAttributeColumns = string.Join(",", dsd.Msd.MetadataAttributes.Select(x => x.SqlColumn() + " char(1)"));

            if (!string.IsNullOrWhiteSpace(metaAttributeColumns))
            {
                metaAttributeColumns += ',';
            }

            var timeDim = dsd.TimeDimension;
            var supportsDateTime = dsd.Base.HasSupportDateTimeAnnotation();

            var timeDimensionColumns = timeDim != null
                ? string.Join(",",
                    new[] { timeDim }.ToColumnList(true, allowNull: true),
                    DbExtensions.SqlPeriodStart(true, supportsDateTime, true),
                    DbExtensions.SqlPeriodEnd(true, supportsDateTime, true)
                ) + ","
                : null;

            var sqlCommand = $@"CREATE TABLE [{dotStatDb.DataSchema}].[{dsd.SqlDeletedMetadataTable(targetVersion)}](
[DF_ID] int not null,
{timeDimensionColumns}
{dimensionColumnsWithType},
{metaAttributeColumns}
[{DbExtensions.LAST_UPDATED_COLUMN}] [datetime] NOT NULL
)";

            await dotStatDb.ExecuteNonQuerySqlAsync(sqlCommand, cancellationToken);

            //Apply data compression
            if (dsd.DataCompression != DataCompressionEnum.NONE)
            {
                await dotStatDb.ExecuteNonQuerySqlAsync(
                    $@"CREATE CLUSTERED COLUMNSTORE INDEX CCI_{dsd.SqlDeletedMetadataTable(targetVersion)}
ON [{dotStatDb.DataSchema}].[{dsd.SqlDeletedMetadataTable(targetVersion)}]
WITH(DATA_COMPRESSION = {dsd.DataCompression})", cancellationToken);
            }
            else
            {
                sqlCommand = $@"CREATE NONCLUSTERED INDEX I_{dsd.SqlDeletedMetadataTable(targetVersion)} 
ON [{dotStatDb.DataSchema}].[{dsd.SqlDeletedMetadataTable(targetVersion)}]([DF_ID],[LAST_UPDATED])";

                await dotStatDb.ExecuteNonQuerySqlAsync(sqlCommand, cancellationToken);
            }
        }

        protected override async Task BuildDynamicDatasetMetaDataTable(Dsd dsd, char targetVersion, SqlDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            var metaAttributeColumns = dsd.Msd.MetadataAttributes
                .ToColumnList(true);

            if (!string.IsNullOrWhiteSpace(metaAttributeColumns))
            {
                metaAttributeColumns += ',';
            }

            var table = dsd.SqlMetadataDataSetTable(targetVersion);
            
            var clusteredPart = dsd.DataCompression == DataCompressionEnum.NONE ? "CLUSTERED" : "NONCLUSTERED";
            var constraint = dsd.DataCompression == DataCompressionEnum.COLUMNSTORE_ARCHIVE && !dsd.KeepHistory
                ? ""
                : $", CONSTRAINT [PK_{table}] PRIMARY KEY {clusteredPart} (DF_ID)";

            var sqlCommand = $@"CREATE TABLE [{dotStatDb.DataSchema}].[{table}](
                        DF_ID INT NOT NULL,
                        {metaAttributeColumns}
                        [{DbExtensions.LAST_UPDATED_COLUMN}] [datetime] NOT NULL
                        {constraint}
                )";

            await dotStatDb.ExecuteNonQuerySqlAsync(sqlCommand, cancellationToken);

            //Apply data compression
            if (dsd.DataCompression != DataCompressionEnum.NONE)
            {
                await dotStatDb.ExecuteNonQuerySqlAsync(
                    $@"CREATE CLUSTERED COLUMNSTORE INDEX CCI_{table}
ON [{dotStatDb.DataSchema}].[{table}]
WITH(DATA_COMPRESSION = {dsd.DataCompression})", cancellationToken);
            }
        }

        public override async ValueTask<bool> BuildDynamicMetaDataDsdView(Dsd dsd, char targetVersion, SqlDotStatDb dotStatDb, bool sourceVersion, CancellationToken cancellationToken)
        {
            //The codelist table doesnt exist, therefore the db is corrupted or the method is called by the unit tests
            foreach (var dimension in dsd.Dimensions.Where(x => x.Base.HasCodedRepresentation()))
            {
                if (!await dotStatDb.TableExists($"CL_{dimension.Codelist.DbId}", cancellationToken))
                {
                    return false;
                }
            }

            //The dependent table does not exists, therefore the db is corrupted or the method is called by the unit tests
            if (!await dotStatDb.TableExists($"{dsd.SqlMetadataDataStructureTable(targetVersion)}", cancellationToken))
            {
                return false;
            }

            //The dependent table does not exists, therefore the db is corrupted or the method is called by the unit tests
            if (!await dotStatDb.TableExists($"{dsd.SqlMetadataDataSetTable(targetVersion)}", cancellationToken))
            {
                return false;
            }

            //The view already exists, therefore no need to recreate it.
            if (await dotStatDb.ViewExists($"{dsd.SqlMetadataDsdViewName(targetVersion, sourceVersion)}", cancellationToken))
            {
                return false;
            }

            var dimensions = dsd.Dimensions.Where(dim => !dim.Base.TimeDimension).ToList();
            
            // SELECT ----------------------------------------------------------------

            var selectStatement = new StringBuilder();
            var dsSelectStatement = new StringBuilder();
            //dimensions
            selectStatement.Append(string.Join(", ",
                !sourceVersion
                    ? dimensions.Select(d =>
                        d.Base.HasCodedRepresentation()
                            ? $"CASE WHEN [CL_{d.Code}].[ID] IS NULL THEN '{DbExtensions.DimensionSwitchedOff}' ELSE [CL_{d.Code}].[ID] END AS [{d.Code}]"
                            : $"CASE WHEN [ME].{d.SqlColumn()} IS NULL THEN '{DbExtensions.DimensionSwitchedOff}' ELSE [ME].{d.SqlColumn()} END AS [{d.Code}]")
                    : dimensions.Select(d =>
                        d.Base.HasCodedRepresentation()
                            ? $"CASE WHEN {d.SqlColumn()} IS NULL THEN 0 ELSE {d.SqlColumn()} END AS [{d.Code}]"
                            : $"CASE WHEN [ME].{d.SqlColumn()} IS NULL THEN '{DbExtensions.DimensionSwitchedOff}' ELSE [ME].{d.SqlColumn()} END AS [{d.Code}]")));

            dsSelectStatement
                .Append(!sourceVersion
                    ? string.Join(", ", dimensions.Select(d => $"'{DbExtensions.DimensionSwitchedOff}' AS [{d.Code}]"))
                    : string.Join(", ", dimensions.Select(d => d.Base.HasCodedRepresentation()
                        ? $"0 AS [{d.Code}]"
                        : $"'{DbExtensions.DimensionSwitchedOff}' AS [{d.Code}]")));

            //time dimension
            var timeDim = dsd.TimeDimension;
            var (sqlMin, sqlMax) = dsd.Base.HasSupportDateTimeAnnotation()
                ? (DateTime.MinValue.ToString("yyyy-MM-dd HH:mm:ss"), DateTime.MaxValue.ToString("yyyy-MM-dd HH:mm:ss"))
                : (DateTime.MinValue.ToString("yyyy-MM-dd"), DateTime.MaxValue.ToString("yyyy-MM-dd"));

            if (timeDim != null)
            {
                selectStatement
                    .Append(",")
                    .Append(string.Join(",",
                        $"CASE WHEN [ME].{timeDim.SqlColumn()} IS NULL THEN '{DbExtensions.DimensionSwitchedOff}' ELSE [ME].{timeDim.SqlColumn()} END AS [{dsd.Base.TimeDimension.Id}]",
                        DbExtensions.SqlPeriodStart(),
                        DbExtensions.SqlPeriodEnd()));

                dsSelectStatement
                    .Append(", ")
                    .Append(string.Join(", ",
                        $"'{DbExtensions.DimensionSwitchedOff}' AS [{dsd.Base.TimeDimension.Id}]",
                        $"'{sqlMax}' AS [{DbExtensions.SqlPeriodStart()}]",
                        $"'{sqlMin}' AS [{DbExtensions.SqlPeriodEnd()}]"));
            }

            //metadata attributes
            if (dsd.Msd.MetadataAttributes.Any())
            {
                selectStatement
                    .Append(",")
                    .Append(string.Join(", ", dsd.Msd.MetadataAttributes.Select(a => $"[ME].{a.SqlColumn()} AS [{a.HierarchicalId}]")));

                dsSelectStatement
                    .Append(",")
                    .Append(string.Join(", ", dsd.Msd.MetadataAttributes.Select(a => $"{a.SqlColumn()} AS [{a.HierarchicalId}]")));
            }

            // LAST_UPDATED
            selectStatement.Append($",[ME].{DbExtensions.LAST_UPDATED_COLUMN}");
            dsSelectStatement.Append($",{DbExtensions.LAST_UPDATED_COLUMN}");

            // JOIN ------------------------------------------------------------------
            var joinStatement = new StringBuilder();

            //dimensions
            joinStatement.AppendLine(string.Join("\n", 
                dimensions
                    .Where(d => d.Base.HasCodedRepresentation())
                    .Select(d => $"LEFT JOIN [{dotStatDb.ManagementSchema}].[CL_{d.Codelist.DbId}] AS [CL_{d.Code}] ON [ME].{d.SqlColumn()} = [CL_{d.Code}].[ITEM_ID]")));

            var dsdLevel = $@"
                        SELECT {selectStatement}
                        FROM [{dotStatDb.DataSchema}].[{dsd.SqlMetadataDataStructureTable(targetVersion)}] AS [ME]
                        {(!sourceVersion ? joinStatement : string.Empty)}";

            var dsLevel = $@"
                        SELECT {dsSelectStatement}
                        FROM [{dotStatDb.DataSchema}].[{dsd.SqlMetadataDataSetTable(targetVersion)}]
                        WHERE [DF_ID] = -1";

            var sqlCommand =
                $@"CREATE VIEW [{dotStatDb.DataSchema}].{dsd.SqlMetadataDsdViewName(targetVersion, sourceVersion)} 
                   AS
                        {dsdLevel}
                        UNION ALL
                        {dsLevel}
                ";

            return await dotStatDb.ExecuteNonQuerySqlAsync(sqlCommand, cancellationToken) > 0;
        }

        public override async Task BuildDeletedView(Dsd dsd, char targetVersion, SqlDotStatDb dotStatDb, bool sourceVersion, CancellationToken cancellationToken)
        {
            //The dependent table does not exists, therefore the db is corrupted or the method is called by the unit tests
            if (!await dotStatDb.TableExists($"{dsd.SqlDeletedMetadataTable(targetVersion)}", cancellationToken))
            {
                return;
            }

            //The view already exists, therefore no need to recreate it.
            if (await dotStatDb.ViewExists($"{dsd.SqlDeletedMetadataDsdViewName(targetVersion, sourceVersion)}", cancellationToken))
            {
                return;
            }

            //Dimensions
            var dimensions = dsd.Dimensions
                .Where(dim => !dim.Base.TimeDimension)
                .ToList();

            //Time dimension columns
            var innerTimeDimensionColumns = dsd.TimeDimension != null
                    ? ", " + string.Join(", ",
                            $"[{DbExtensions.TimeDimColumn}]",
                            DbExtensions.SqlPeriodStart(),
                            DbExtensions.SqlPeriodEnd()
                        )
                    : string.Empty;

            var innerTimeDimensionColumns4GroupBy = innerTimeDimensionColumns;
            var (sqlMax, sqlMaxMinOne) = dsd.Base.HasSupportDateTimeAnnotation()
                ? (DateTime.MaxValue.ToString("yyyy-MM-dd HH:mm:ss"), DateTime.MaxValue.AddYears(-1).ToString("yyyy-MM-dd HH:mm:ss"))
                : (DateTime.MaxValue.ToString("yyyy-MM-dd"), DateTime.MaxValue.AddYears(-1).ToString("yyyy-MM-dd"));
            var outerTimeDimensionColumns = dsd.TimeDimension != null
                ? ", " + string.Join(", ",
                    @$"CASE 
                            WHEN [DEL].[{DbExtensions.SqlPeriodStart()}] = '{sqlMax}' THEN '{DbExtensions.DimensionSwitchedOff}'
                            WHEN [DEL].[{DbExtensions.SqlPeriodStart()}] = '{sqlMaxMinOne}' THEN '{DbExtensions.DimensionWildCarded}'
                            ELSE [DEL].[{DbExtensions.TimeDimColumn}]
                      END AS [{dsd.Base.TimeDimension.Id}]",
                    $"[{DbExtensions.SqlPeriodStart()}]",
                    $"[{DbExtensions.SqlPeriodEnd()}]")
                : string.Empty;

            //Dimensions
            var innerDimensionsColumns = dimensions.Any()
                        ? ", " + string.Join(", ", dimensions.Select(d => $"{d.SqlColumn()}"))
                        : string.Empty;

            var outerDimensionsColumns = dimensions.Any()
                ? ", " +
                  string.Join(
                      ", ",
                      dimensions.Select(d =>
                          (d.Base.HasCodedRepresentation()
                              ? @$"CASE
                            WHEN [DEL].{d.SqlColumn()} = {DbExtensions.DimensionSwitchedOffDbValue} THEN '{DbExtensions.DimensionSwitchedOff}'
                            WHEN [DEL].{d.SqlColumn()} = {DbExtensions.DimensionWildCardedDbValue} THEN '{DbExtensions.DimensionWildCarded}'
                            ELSE [CL_{d.Code}].[ID]
                        END"
                              : $"{d.SqlColumn()}") + $" AS [{d.Code}]"
                      )
                  )
                : string.Empty;

            //Attributes
            var innerAttributeColumns4SomeComp = dsd.Msd.MetadataAttributes.Any()
                ? "," + string.Join(",", dsd.Msd.MetadataAttributes.Select(a => $"MAX( {a.SqlColumn()} ) AS [{a.HierarchicalId}]"))
                : string.Empty;

            var innerAttributeColumns4NoComp = dsd.Msd.MetadataAttributes.Any()
                ? "," + string.Join(",", dsd.Msd.MetadataAttributes.Select(a => $"NULL AS [{a.HierarchicalId}]"))
                : string.Empty;

            var outerAttributeColumns = dsd.Msd.MetadataAttributes.Any()
                ? "," + string.Join(",", dsd.Msd.MetadataAttributes.Select(a => $" [{a.HierarchicalId}]"))
                : string.Empty;

            var joinStatements = string.Join(Environment.NewLine, dimensions.Where(d => d.Base.HasCodedRepresentation()).Select(d =>
                    $"LEFT JOIN [{dotStatDb.ManagementSchema}].[CL_{d.Codelist.DbId}] AS [CL_{d.Code}] ON [DEL].{d.SqlColumn()} = [CL_{d.Code}].[ITEM_ID]"
                )
            );

            var innerSelectPartCommon =
$@"SELECT MAX( {DbExtensions.LAST_UPDATED_COLUMN} ) AS {DbExtensions.LAST_UPDATED_COLUMN}
    {innerTimeDimensionColumns}
    {innerDimensionsColumns}";

            var innerFromPart = $@"FROM [{dotStatDb.DataSchema}].{dsd.SqlDeletedMetadataTable(targetVersion)} ";

            var innerWherePart4NoComp = $@"WHERE [DF_ID] = -1 " +
                                        (dsd.Msd.MetadataAttributes.Any()
                                            ? Environment.NewLine + "AND " + string.Join(" AND ",
                                                dsd.Msd.MetadataAttributes.Select(a => $"{a.SqlColumn()} IS NULL"))
                                            : string.Empty) + Environment.NewLine;

            var innerWherePart4SomeComp = $@"WHERE [DF_ID] = -1 " +
                                          "AND (" +
                                          (dsd.Msd.MetadataAttributes.Any()
                                              ? Environment.NewLine + string.Join(" OR ",
                                                  dsd.Msd.MetadataAttributes.Select(a => $"{a.SqlColumn()} IS NOT NULL"))
                                              : "1 = 1") + " )";

            var innerGroupByPart = $@"GROUP BY {innerDimensionsColumns.TrimStart(',')}{innerTimeDimensionColumns4GroupBy} ";

            var innerUnionSql =
                $@"{innerSelectPartCommon}{innerAttributeColumns4SomeComp}
   {innerFromPart} 
   {innerWherePart4SomeComp}
   {innerGroupByPart}
UNION
   {innerSelectPartCommon}{innerAttributeColumns4NoComp}
   {innerFromPart}
   {innerWherePart4NoComp}
   {innerGroupByPart}";

            var sqlCommand =
                !sourceVersion
                    ? $@"CREATE VIEW [{dotStatDb.DataSchema}].{dsd.SqlDeletedMetadataDsdViewName(targetVersion, false)} 
                   AS
                       SELECT {DbExtensions.LAST_UPDATED_COLUMN}
                            {outerTimeDimensionColumns}
                            {outerDimensionsColumns}
                            {outerAttributeColumns}
                       FROM ( {innerUnionSql} ) AS [DEL]
                            {joinStatements}"
                    : $@"CREATE VIEW [{dotStatDb.DataSchema}].{dsd.SqlDeletedMetadataDsdViewName(targetVersion, true)} 
                   AS
                       {innerUnionSql}";

            await dotStatDb.ExecuteNonQuerySqlAsync(sqlCommand, cancellationToken);
        }
        #endregion

    }
}
