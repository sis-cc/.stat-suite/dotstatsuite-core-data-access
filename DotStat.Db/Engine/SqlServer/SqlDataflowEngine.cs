﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using DotStat.Common.Configuration.Interfaces;
using DotStat.Common.Enums;
using DotStat.Db.DB;
using DotStat.Db.Util;
using DotStat.Domain;
using Microsoft.Data.SqlClient;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Constants.InterfaceConstant;
using Dataflow = DotStat.Domain.Dataflow;

namespace DotStat.Db.Engine.SqlServer
{
    public class SqlDataflowEngine : DataflowEngineBase<SqlDotStatDb>
    {

        public SqlDataflowEngine(IGeneralConfiguration generalConfiguration) : base(generalConfiguration)
        {
        }

        public override async Task<int> GetDbId(string sdmxId, string sdmxAgency, int verMajor, int verMinor, int? verPatch, SqlDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            var aretefactDbId = await dotStatDb.ExecuteScalarSqlWithParamsAsync(
                $@"SELECT [ART_ID] 
                    FROM [{dotStatDb.ManagementSchema}].[ARTEFACT] 
                   WHERE [ID] = @Id AND [AGENCY] = @Agency AND [TYPE] = @Type
                     AND [VERSION_1] = @Version1 AND [VERSION_2] = @Version2 
                     AND ([VERSION_3] = @Version3 OR ([VERSION_3] IS NULL AND @Version3 IS NULL))",
                cancellationToken,
                new SqlParameter("Id", SqlDbType.VarChar) { Value = sdmxId },
                new SqlParameter("Agency", SqlDbType.VarChar) { Value = sdmxAgency },
                new SqlParameter("Type", SqlDbType.VarChar) { Value = DbTypes.GetDbType(SDMXArtefactType.Dataflow) },
                new SqlParameter("Version1", SqlDbType.Int) { Value = verMajor },
                new SqlParameter("Version2", SqlDbType.Int) { Value = verMinor },
                new SqlParameter("Version3", SqlDbType.Int) { Value = ((object)verPatch) ?? DBNull.Value }
                );

            return (int)(aretefactDbId ?? -1);
        }

        public override async Task<int> InsertToArtefactTable(Dataflow dataflow, SqlDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            var artefactId = await dotStatDb.ExecuteScalarSqlWithParamsAsync(
                $@"INSERT 
                    INTO [{dotStatDb.ManagementSchema}].[ARTEFACT] 
                         ([SQL_ART_ID],[TYPE],[ID],[AGENCY],[VERSION_1],[VERSION_2],[VERSION_3],[LAST_UPDATED],[DF_DSD_ID],[DF_WHERE_CLAUSE]) 
                  OUTPUT INSERTED.ART_ID 
                  VALUES (NULL, @Type, @Id, @Agency, @Version1, @Version2, @Version3, @DT_Now, @DsdId, NULL)",
                cancellationToken,
                new SqlParameter("Type", SqlDbType.VarChar) { Value = DbTypes.GetDbType(SDMXArtefactType.Dataflow) },
                new SqlParameter("Id", SqlDbType.VarChar) { Value = dataflow.Code },
                new SqlParameter("Agency", SqlDbType.VarChar) { Value = dataflow.AgencyId },
                new SqlParameter("Version1", SqlDbType.Int) { Value = dataflow.Version.Major },
                new SqlParameter("Version2", SqlDbType.Int) { Value = dataflow.Version.Minor },
                new SqlParameter("Version3", SqlDbType.Int) { Value = (object)dataflow.Version.Patch ?? DBNull.Value },
                new SqlParameter("DsdId", SqlDbType.Int) { Value = dataflow.Dsd.DbId },
                new SqlParameter("DT_Now", DateTime.Now)
                );

            return (int)artefactId;
        }

        protected override async Task DeleteFromArtefactTableByDbId(int dbId, SqlDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            await dotStatDb.ExecuteNonQuerySqlWithParamsAsync(
                $@"DELETE
                    FROM [{dotStatDb.ManagementSchema}].[ARTEFACT] 
                   WHERE [ART_ID] = @DbId AND [TYPE] = 'DF'",
                cancellationToken,
                new SqlParameter("DbId", SqlDbType.VarChar) { Value = dbId }
                );
        }

        protected override async Task BuildOptimizedDataTable(Dataflow dataflow, char targetVersion, char hprVersion, SqlDotStatDb dotStatDb,
            ICodeTranslator codeTranslator, CancellationToken cancellationToken)
        {
            var dsd = dataflow.Dsd;

            // Create list of time dimension related columns
            var timeDim = dsd.TimeDimension;

            var innerTimeDimensionColumns = string.Empty;
            var outerTimeDimensionColumns = string.Empty;
            if (timeDim != null)
            {
                innerTimeDimensionColumns = string.Join(",",
                    $"{timeDim.SqlColumn()}",
                    DbExtensions.SqlPeriodStart(),
                    DbExtensions.SqlPeriodEnd()
                ) + ",";

                outerTimeDimensionColumns = string.Join(",",
                    $"{timeDim.SqlColumn()} AS {timeDim.SqlColumn(externalColumn: true)}",
                    DbExtensions.SqlPeriodStart(),
                    DbExtensions.SqlPeriodEnd()
                ) + ",";
            }

            // Create list of dimensions
            var innerDimensionColumns = string.Join(',',
                dsd.Dimensions
                    .Where(d => d != timeDim)
                    .Select(d => $"[FI].{d.SqlColumn()}"));
            var outerDimensionColumns = string.Join(',',
                dsd.Dimensions
                    .Where(d => d != timeDim)
                    .Select(d => $"[X].{d.SqlColumn()} AS {d.SqlColumn(externalColumn: true)}"));

            // Create list of observation level attributes
            var innerObsAttributeColumns = string.Join(',',
                dsd.Attributes
                    .Where(a => a.Base.AttachmentLevel == AttributeAttachmentLevel.Observation)
                    .Select(a => $"[FA].{a.SqlColumn()}"));
            if (!string.IsNullOrWhiteSpace(innerObsAttributeColumns))
                innerObsAttributeColumns += ",";
            var outerObsAttributeColumns = string.Join(',',
                dsd.Attributes
                    .Where(a => a.Base.AttachmentLevel == AttributeAttachmentLevel.Observation)
                    .Select(a => $"[X].{a.SqlColumn()} AS {a.SqlColumn(externalColumn: true)}"));
            if (!string.IsNullOrWhiteSpace(outerObsAttributeColumns))
                outerObsAttributeColumns += ",";

            // Create list of group attributes with time dimension relation (are stored at observation level in FACT table)
            var innerGroupAttributeColumnsWithTime = string.Join(',',
                dsd.Attributes
                    .Where(a =>
                        (a.Base.AttachmentLevel == AttributeAttachmentLevel.DimensionGroup ||
                         a.Base.AttachmentLevel == AttributeAttachmentLevel.Group)
                        && a.Base.GetDimensionReferences().Contains(DimensionObject.TimeDimensionFixedId))
                    .Select(a => $"[FA].{a.SqlColumn()}"));
            if (!string.IsNullOrWhiteSpace(innerGroupAttributeColumnsWithTime))
                innerGroupAttributeColumnsWithTime += ',';
            var outerGroupAttributeColumnsWithTime = string.Join(',',
                dsd.Attributes
                    .Where(a =>
                        (a.Base.AttachmentLevel == AttributeAttachmentLevel.DimensionGroup ||
                         a.Base.AttachmentLevel == AttributeAttachmentLevel.Group)
                        && a.Base.GetDimensionReferences().Contains(DimensionObject.TimeDimensionFixedId))
                    .Select(a => $"[X].{a.SqlColumn()} AS {a.SqlColumn(externalColumn: true)}"));
            if (!string.IsNullOrWhiteSpace(outerGroupAttributeColumnsWithTime))
                outerGroupAttributeColumnsWithTime += ',';

            // Create list of group attributes without time dimension relation
            var innerGroupAttributeColumns = string.Join(',',
                dsd.Attributes
                    .Where(a => 
                        (a.Base.AttachmentLevel == AttributeAttachmentLevel.DimensionGroup || 
                         a.Base.AttachmentLevel == AttributeAttachmentLevel.Group)
                        && !a.Base.GetDimensionReferences().Contains(DimensionObject.TimeDimensionFixedId))
                    .Select(a => $"[ATTR].{a.SqlColumn()}"));
            var outerGroupAttributeColumns = string.Join(',',
                dsd.Attributes
                    .Where(a =>
                        (a.Base.AttachmentLevel == AttributeAttachmentLevel.DimensionGroup ||
                         a.Base.AttachmentLevel == AttributeAttachmentLevel.Group)
                        && !a.Base.GetDimensionReferences().Contains(DimensionObject.TimeDimensionFixedId))
                    .Select(a => $"[X].{a.SqlColumn()} AS {a.SqlColumn(externalColumn: true)}"));
            var groupAttributeJoin = string.Empty;
            var groupAttributeWhere = string.Empty;
            if (!string.IsNullOrWhiteSpace(outerGroupAttributeColumns))
            {
                innerGroupAttributeColumns += ',';
                outerGroupAttributeColumns += ',';
                groupAttributeJoin =
                    $" LEFT JOIN [{dotStatDb.DataSchema}].{dsd.SqlDimGroupAttrTable(targetVersion, "ATTR")} ON FI.[SID] = ATTR.[SID]";
                groupAttributeWhere =
                    $"OR EXISTS (SELECT TOP 1 1 FROM [{dotStatDb.DataSchema}].{dsd.SqlDimGroupAttrTable(targetVersion, "ATTR_2")} WHERE FI.[SID] = ATTR_2.[SID])";
            }

            // Create list of dataset attributes
            var innerDatasetAttributeColumns = string.Join(',',
                dsd.Attributes
                    .Where(a =>
                        (a.Base.AttachmentLevel == AttributeAttachmentLevel.DataSet ||
                         a.Base.AttachmentLevel == AttributeAttachmentLevel.Null)
                        && !a.Base.GetDimensionReferences().Contains(DimensionObject.TimeDimensionFixedId))
                    .Select(a => $"[DF].{a.SqlColumn()}"));
            var datasetAttributeJoin = string.Empty;
            if (!string.IsNullOrWhiteSpace(innerDatasetAttributeColumns))
            {
                innerDatasetAttributeColumns += ',';
                datasetAttributeJoin = 
                    $" LEFT JOIN [{dotStatDb.DataSchema}].{dsd.SqlDsdAttrTable(targetVersion, "DF")} ON DF.[DF_ID] = {dataflow.DbId}";
            }

            // Generate WHERE clause out of available content constraint of dataflow (if any)
            var whereClause = Predicate.BuildWhereForObservationLevel(
                dataQuery: null, 
                dataflow,
                codeTranslator: codeTranslator,
                includeTimeConstraints: true,
                useExternalValues: false,
                useExternalColumnNames: false,
                tableAlias: "X",
                filterLastUpdated: false,
                serverType: dotStatDb.GetDbType(),
                includeAttributeConstraints: true);

            if (!string.IsNullOrWhiteSpace(whereClause))
                whereClause = " WHERE " + whereClause;

            var tableName = $"{dataflow.SqlOptimizedDataTable(targetVersion, hprVersion)}";

            // Build the SQL command string 
            var sqlCommand = $@"
SELECT [X].[SID], {outerTimeDimensionColumns}
	  {outerDimensionColumns}, 
      [X].[VALUE] AS [OBS_VALUE], {outerObsAttributeColumns} {outerGroupAttributeColumnsWithTime} {outerGroupAttributeColumns} 
	  [X].{DbExtensions.LAST_UPDATED_COLUMN} AS {DbExtensions.LAST_UPDATED_COLUMN}
INTO [{dotStatDb.DataSchema}].{tableName}
FROM (
SELECT FI.[SID], {innerTimeDimensionColumns}
	  {innerDimensionColumns}, 
      [FA].[VALUE], {innerObsAttributeColumns} {innerGroupAttributeColumnsWithTime} {innerGroupAttributeColumns} {innerDatasetAttributeColumns} 
	  [FA].{DbExtensions.LAST_UPDATED_COLUMN}
FROM [{dotStatDb.DataSchema}].{dsd.SqlFilterTable("FI")}
LEFT JOIN [{dotStatDb.DataSchema}].{dsd.SqlFactTable(targetVersion, "FA")} ON FI.[SID] = FA.[SID] 
{groupAttributeJoin}
{datasetAttributeJoin}
WHERE (   EXISTS (SELECT TOP 1 1 FROM [{dotStatDb.DataSchema}].{dsd.SqlFactTable(targetVersion, "FA_2")} WHERE FI.[SID] = FA_2.[SID]) 
       {groupAttributeWhere} )
) X
{whereClause}
{dotStatDb.DataSpace.MAXDOPForDsdOptimization.GetMaxDopPart()}
";

            await dotStatDb.ExecuteNonQuerySqlAsync(sqlCommand, cancellationToken);
            
            var partitionSchemaExists =
                await dotStatDb.PartitionSchemeExists(dotStatDb.DataSpace.DefaultPartitioningScheme, cancellationToken);

            var partitionIndex = partitionSchemaExists && await dotStatDb.ColumnExists(tableName, dotStatDb.DataSpace.DefaultPartitioningColumn,
                cancellationToken);
            
            var indexColumns = dataflow.Dsd.GetIndexColumns(dotStatDb.DataSpace.DefaultPartitioningColumn, false, true);

            await ApplyIndexesOnOptimizedTable(tableName, indexColumns, partitionIndex, dotStatDb, cancellationToken);
        }

        protected override async Task BuildOptimizedAttributeTable(Dataflow dataflow, char targetVersion, char hprVersion, SqlDotStatDb dotStatDb,
            ICodeTranslator codeTranslator, CancellationToken cancellationToken)
        {
            var dsd = dataflow.Dsd;

            var groupAttributes = dsd.Attributes
                .Where(a => (a.Base.AttachmentLevel == AttributeAttachmentLevel.DimensionGroup ||
                                      a.Base.AttachmentLevel == AttributeAttachmentLevel.Group)
                                  && !a.Base.GetDimensionReferences().Contains(DimensionObject.TimeDimensionFixedId)).ToList();

            if (!groupAttributes.Any())
                return;

            // Create list of dimensions
            var outerDimensionColumns = string.Join(',',
                dsd.Dimensions
                    .Where(d => !d.Base.TimeDimension)
                    .Select(d => $"[X].{d.SqlColumn()} AS {d.SqlColumn(externalColumn: true)}"));
            var innerDimensionColumns = string.Join(',',
                dsd.Dimensions
                    .Where(d => !d.Base.TimeDimension)
                    .Select(d => $"[FI].{d.SqlColumn()}"));

            // Create list of group attributes without time dimension relation
            var outerGroupAttributeColumns = string.Join(',', groupAttributes.Select(a => $"[X].{a.SqlColumn()} AS {a.SqlColumn(externalColumn:true)}") );
            var innerGroupAttributeColumns = string.Join(',', groupAttributes.Select(a => $"[ATTR].{a.SqlColumn()}"));

            // Create list of dataset attributes
            var innerDatasetAttributeColumns = string.Join(',',
                dsd.Attributes
                    .Where(a =>
                        (a.Base.AttachmentLevel == AttributeAttachmentLevel.DataSet ||
                         a.Base.AttachmentLevel == AttributeAttachmentLevel.Null)
                        && !a.Base.GetDimensionReferences().Contains(DimensionObject.TimeDimensionFixedId))
                    .Select(a => $"[DF].{a.SqlColumn()}"));
            var datasetAttributeJoin = string.Empty;
            if (!string.IsNullOrWhiteSpace(innerDatasetAttributeColumns))
            {
                innerDatasetAttributeColumns += ',';
                datasetAttributeJoin =
                    $" LEFT JOIN [{dotStatDb.DataSchema}].{dsd.SqlDsdAttrTable(targetVersion, "DF")} ON DF.[DF_ID] = {dataflow.DbId}";
            }

            // Generate WHERE clause out of available content constraint of dataflow (if any)
            var whereClause = Predicate.BuildWhereForObservationLevel(
                dataQuery: null,
                dataflow,
                codeTranslator: codeTranslator,
                includeTimeConstraints: false,
                useExternalValues: false,
                useExternalColumnNames: false,
                tableAlias: "X",
                filterLastUpdated: false,
                serverType: dotStatDb.GetDbType(),
                includeAttributeConstraints: true,
                attributeAttachmentLevels: new[]
                {
                    AttributeAttachmentLevel.DataSet, AttributeAttachmentLevel.DimensionGroup,
                    AttributeAttachmentLevel.Group, AttributeAttachmentLevel.Null
                });

            if (!string.IsNullOrWhiteSpace(whereClause))
                whereClause = "WHERE " + whereClause;

            var tableName = $"{dataflow.SqlOptimizedAttrTable(targetVersion, hprVersion)}";

            // Build the SQL command string 
            var sqlCommand = $@"
SELECT [X].[SID], 
	  {outerDimensionColumns},
      {outerGroupAttributeColumns},
	  [X].{DbExtensions.LAST_UPDATED_COLUMN} AS {DbExtensions.LAST_UPDATED_COLUMN}
INTO [{dotStatDb.DataSchema}].{tableName}
FROM (
SELECT [FI].[SID], 
	  {innerDimensionColumns},
      {innerGroupAttributeColumns}, {innerDatasetAttributeColumns}
	  [ATTR].{DbExtensions.LAST_UPDATED_COLUMN}
FROM [{dotStatDb.DataSchema}].{dsd.SqlDimGroupAttrTable(targetVersion, "ATTR")} 
LEFT JOIN [{dotStatDb.DataSchema}].{dsd.SqlFilterTable("FI")} ON [FI].[SID] = [ATTR].[SID]
{datasetAttributeJoin}
) X
{whereClause}
{dotStatDb.DataSpace.MAXDOPForDsdOptimization.GetMaxDopPart()}
";

            await dotStatDb.ExecuteNonQuerySqlAsync(sqlCommand, cancellationToken);
            
            var partitionSchemaExists =
                await dotStatDb.PartitionSchemeExists(dotStatDb.DataSpace.DefaultPartitioningScheme, cancellationToken);

            var partitionIndex = partitionSchemaExists && await dotStatDb.ColumnExists(tableName, dotStatDb.DataSpace.DefaultPartitioningColumn,
                cancellationToken);

            var indexColumns = dataflow.Dsd.GetIndexColumns(dotStatDb.DataSpace.DefaultPartitioningColumn, true, false);

            await ApplyIndexesOnOptimizedTable(tableName, indexColumns, partitionIndex, dotStatDb, cancellationToken);
        }

        protected override async Task BuildOptimizedDeletedDataTable(Dataflow dataflow, char targetVersion, char hprVersion, SqlDotStatDb dotStatDb,
            ICodeTranslator codeTranslator, CancellationToken cancellationToken)
        {
            var dsd = dataflow.Dsd;

            // Create list of time dimension related columns
            var timeDim = dsd.TimeDimension;

            var timeDimensionColumnsExternal = timeDim != null
                ? ", " + string.Join(",",
                    $"{timeDim.SqlColumn()} AS {timeDim.SqlColumn(externalColumn: true)}",
                    DbExtensions.SqlPeriodStart(),
                    DbExtensions.SqlPeriodEnd()
                )
                : null;

            var timeDimensionColumnsInternal = timeDim != null
                ? ", " + string.Join(",",
                    $"{timeDim.SqlColumn()}",
                    DbExtensions.SqlPeriodStart(),
                    DbExtensions.SqlPeriodEnd()
                )
                : null;

            // Create list of dimensions
            var nonTimeDimensions = dsd.Dimensions.Where(d => d != timeDim).ToList();

            var dimensionColumnsWithExtAlias = string.Join(',', nonTimeDimensions.Select(d => $"{d.SqlColumn()} AS {d.SqlColumn(externalColumn: true)}"));
            var dimensionInternalColumns = nonTimeDimensions.ToColumnList();

            // Create list of attribute columns
            var attributeColumns = dsd.Attributes.ToColumnList(externalColumn:true);

            var attributeMaxColumns = string.Join(',', dsd.Attributes.Select(a => $"MAX({a.SqlColumn()}) AS {a.SqlColumn(externalColumn:true)}"));
            var attributeNullColumns = string.Join(',', dsd.Attributes.Select(a => $"NULL AS {a.SqlColumn(externalColumn: true)}"));
            var attributeNotNullWhereClause = string.Join(" OR ", dsd.Attributes.Select(a => $"{a.SqlColumn()} IS NOT NULL"));
            var attributeNullWhereClause = string.Join(" AND ", dsd.Attributes.Select(a => $"{a.SqlColumn()} IS NULL"));
            if (dsd.Attributes.Any())
            {
                attributeColumns += ",";
                attributeMaxColumns += ",";
                attributeNullColumns += ",";
                attributeNotNullWhereClause += " OR ";
                attributeNullWhereClause += " AND ";
            }

            // Generate WHERE clause out of available content constraint of dataflow (if any)
            // Since any non-dimensions components have value '1' in case they are affected by the delete operation,
            //   constraints defined on attributes cannot be applied on tables storing records of data deletions.
            var whereClause = Predicate.BuildWhereForObservationLevel(
                dataQuery: null, 
                dataflow,
                codeTranslator: codeTranslator,
                includeTimeConstraints: true,
                useExternalValues: false,
                useExternalColumnNames: false,
                tableAlias: "D",
                filterLastUpdated: false,
                serverType: dotStatDb.GetDbType(),
                includeAttributeConstraints:false);

            if (!string.IsNullOrWhiteSpace(whereClause))
                whereClause = " AND " + whereClause;

            var tableName =
                $"{dataflow.SqlOptimizedDeletedDataTable(targetVersion, hprVersion)}";

            // Build the SQL command string as UNION of partial and full delete operations (in terms of components affected)
            var sqlCommand = $@"
SELECT [OBS_VALUE] {timeDimensionColumnsExternal}, {dimensionColumnsWithExtAlias}, {attributeColumns} [{DbExtensions.LAST_UPDATED_COLUMN}]
INTO [{dotStatDb.DataSchema}].{tableName}
  FROM (
    SELECT MAX([VALUE]) AS [OBS_VALUE] 
           {timeDimensionColumnsInternal}, {dimensionInternalColumns}, {attributeMaxColumns}
           MAX([D].[{DbExtensions.LAST_UPDATED_COLUMN}]) AS [{DbExtensions.LAST_UPDATED_COLUMN}]
    FROM [{dotStatDb.DataSchema}].{dsd.SqlDeletedTable(targetVersion,"D")}
    WHERE [D].DF_ID = @DfId
      AND ({attributeNotNullWhereClause} [VALUE] IS NOT NULL)
      {whereClause}
   GROUP BY  {dimensionInternalColumns} {timeDimensionColumnsInternal}

UNION ALL

    SELECT NULL AS [OBS_VALUE] 
           {timeDimensionColumnsInternal}, {dimensionInternalColumns}, {attributeNullColumns}
           MAX([D].[{DbExtensions.LAST_UPDATED_COLUMN}]) AS [{DbExtensions.LAST_UPDATED_COLUMN}]
    FROM [{dotStatDb.DataSchema}].{dsd.SqlDeletedTable(targetVersion, "D")}
    WHERE [D].DF_ID = @DfId
      AND ({attributeNullWhereClause} [VALUE] IS NULL)
      {whereClause}
   GROUP BY  {dimensionInternalColumns} {timeDimensionColumnsInternal}
) AS DELETED_DATA_DATAFLOW
{dotStatDb.DataSpace.MAXDOPForDsdOptimization.GetMaxDopPart()}
";

            await dotStatDb.ExecuteNonQuerySqlWithParamsAsync(sqlCommand, cancellationToken,
                new SqlParameter("DfId", SqlDbType.Int) { Value = dataflow.DbId });
            
            var partitionSchemaExists =
                await dotStatDb.PartitionSchemeExists(dotStatDb.DataSpace.DefaultPartitioningScheme, cancellationToken);

            var partitionIndex = partitionSchemaExists && await dotStatDb.ColumnExists(tableName, dotStatDb.DataSpace.DefaultPartitioningColumn,
                cancellationToken);

            var indexColumns = dataflow.Dsd.GetIndexColumns(dotStatDb.DataSpace.DefaultPartitioningColumn, true, true);
            
            await ApplyIndexesOnOptimizedTable(tableName, indexColumns, partitionIndex, dotStatDb, cancellationToken);
        }

        private async Task ApplyIndexesOnOptimizedTable(string tableName, List<string> indexColumns, bool partitionIndex, SqlDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            if (!indexColumns.Any()) return;

            await dotStatDb.ExecuteNonQuerySqlAsync(dotStatDb.GetClusteredIndex(tableName, indexColumns, partitionIndex), cancellationToken);

            await dotStatDb.ExecuteNonQuerySqlAsync(dotStatDb.GetClusteredColumnStoreIndex(tableName), cancellationToken);
        }

        protected override async Task BuildOptimizedDataView(Dataflow dataflow, char tableVersion, char hprVersion, IReadOnlyDictionary<string, string> datasetAttributeValues,
            SqlDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            var sqlCountCommand =
                $@"SELECT 1 WHERE EXISTS(SELECT 1 FROM [{dotStatDb.DataSchema}].[{dataflow.SqlOptimizedDataTable(tableVersion, hprVersion)}] DATA)";

            var isThereAnyData = (await dotStatDb.ExecuteScalarSqlAsync(sqlCountCommand, cancellationToken)) is not null;

            var dsd = dataflow.Dsd;

            // Create list of time dimension related columns
            var timeDim = dsd.TimeDimension;

            var timeDimensionColumns = timeDim != null
                ? string.Join(",",
                    timeDim.SqlColumn(externalColumn: true),
                    DbExtensions.SqlPeriodStart(),
                    DbExtensions.SqlPeriodEnd()
                ) + ","
                : null;

            // Create list of dimensions
            var nonTimeDimensions = dsd.Dimensions.Where(d => d != timeDim).ToList();

            var dimensionColumns = string.Join(',',
                nonTimeDimensions.Select(d =>
                    d.Codelist is null
                        ? $"{d.SqlColumn(externalColumn: true)}"
                        : $"[CL_{d.Code}].[ID] AS {d.SqlColumn(externalColumn: true)}"));

            var dimensionJoins = string.Join('\n',
                nonTimeDimensions
                    .Where(d => d.Codelist is not null)
                    .Select(d => $"INNER JOIN [{dotStatDb.ManagementSchema}].[CL_{d.Codelist.DbId}] AS [CL_{d.Code}] ON [DATA].{d.SqlColumn(externalColumn: true)} = [CL_{d.Code}].[ITEM_ID]"));

            // Create list of non-dataset attributes
            var nonDatasetAttributes = dsd.Attributes
                .Where(a => a.Base.AttachmentLevel != AttributeAttachmentLevel.DataSet && a.Base.AttachmentLevel != AttributeAttachmentLevel.Null).ToList();
            var nonDatasetAttributeColumns = string.Join(',',
                nonDatasetAttributes.Select(a =>
                    a.Codelist is null
                        ? $"{a.SqlColumn(externalColumn: true)}"
                        : $"[CL_{a.Code}].[ID] AS {a.SqlColumn(externalColumn: true)}"));
            if (!string.IsNullOrEmpty(nonDatasetAttributeColumns))
                nonDatasetAttributeColumns += ", ";

            var nonDatasetAttributeJoins = string.Join('\n',
                nonDatasetAttributes
                    .Where(a => a.Codelist is not null)
                    .Select(a => $"LEFT JOIN [{dotStatDb.ManagementSchema}].[CL_{a.Codelist.DbId}] AS [CL_{a.Code}] ON [DATA].{a.SqlColumn(externalColumn: true)} = [CL_{a.Code}].[ITEM_ID]"));

            // Create list of dataset attributes and full join clause with hard coded values, eg: "1 AS COMP_16, 'abcd' AS COMP_23, 8 AS COMP_24,"
            var datasetAttributes = dsd.Attributes
                .Where(a => a.Base.AttachmentLevel is AttributeAttachmentLevel.DataSet or AttributeAttachmentLevel.Null).ToList();

            var datasetAttributeColumns = string.Empty;

            if (datasetAttributes.Any())
            {
                foreach (var attr in datasetAttributes)
                {
                    var attrColumnName = attr.SqlColumn(externalColumn: true);
                    var attrValue = datasetAttributeValues.GetValueOrDefault(attrColumnName) ?? "NULL" ;

                    if ( attrValue.Equals("NULL"))
                    {
                        datasetAttributeColumns += $"NULL AS {attrColumnName}, ";
                    }
                    else if (attr.Codelist is null)
                    {
                        datasetAttributeColumns += $"{attrValue} AS {attrColumnName}, ";
                    }
                    else
                    {
                        datasetAttributeColumns += $"(SELECT [ID] FROM [management].[CL_{attr.Codelist.DbId}] WHERE [ITEM_ID] = {attrValue}) AS {attrColumnName}, ";
                    }
                }
            }

            // Build the SQL command string 
            string sqlCommand;

            if (datasetAttributes.Any() && !isThereAnyData)
            {
                // If there are no observations stored but there may be dataset attributes then the view displays only the dataset attribute values. This is to let
                // the dataset attribute values be retrieved in the absence of observation data. In this case any non-dataset-attribute columns are represented with
                // a constant NULL value. This separation is made to substitute the earlier used FULL JOIN statement causing some performance degradations.

                // Create list of time dimension related columns
                var timeDimensionNullColumns = timeDim != null
                    ? string.Join(",",
                        $"NULL AS {timeDim.SqlColumn(externalColumn: true)}",
                        $"NULL AS {DbExtensions.SqlPeriodStart()}",
                        $"NULL AS {DbExtensions.SqlPeriodEnd()}"
                    ) + ","
                    : null;

                // Create list of dimensions
                var dimensionNullColumns = string.Join(',',
                    nonTimeDimensions.Select(d => $"NULL AS {d.SqlColumn(externalColumn: true)}"));

                // Create list of non-dataset attributes
                var nonDatasetAttributeNullColumns = string.Join(',',
                    nonDatasetAttributes.Select(a => $"NULL AS {a.SqlColumn(externalColumn: true)}"));

                if (!string.IsNullOrEmpty(nonDatasetAttributeNullColumns))
                    nonDatasetAttributeNullColumns += ", ";

                sqlCommand = $@"
CREATE OR ALTER VIEW [{dotStatDb.DataSchema}].[{dataflow.SqlDataDataflowViewName(tableVersion)}] 
AS
SELECT NULL AS [SID], 
    {timeDimensionNullColumns}
    {dimensionNullColumns},
    NULL AS [OBS_VALUE],
    {nonDatasetAttributeNullColumns}
    {datasetAttributeColumns}
    NULL AS {DbExtensions.LAST_UPDATED_COLUMN}
";
            }
            else
            {
                // If there are observations stored then the view is created normally, using all components.

                sqlCommand = $@"
CREATE OR ALTER VIEW [{dotStatDb.DataSchema}].[{dataflow.SqlDataDataflowViewName(tableVersion)}] 
AS
SELECT [SID], 
    {timeDimensionColumns}
    {dimensionColumns},
    [OBS_VALUE],
    {nonDatasetAttributeColumns}
    {datasetAttributeColumns}
    {DbExtensions.LAST_UPDATED_COLUMN}
FROM [{dotStatDb.DataSchema}].{dataflow.SqlOptimizedDataTable(tableVersion, hprVersion)} AS DATA
{dimensionJoins}
{nonDatasetAttributeJoins}
";
            }

            await dotStatDb.ExecuteNonQuerySqlAsync(sqlCommand, cancellationToken);
        }

        protected override async Task BuildOptimizedReplaceView(Dataflow dataflow, char tableVersion, char hprVersion, IReadOnlyDictionary<string, string> datasetAttributeValues, SqlDotStatDb dotStatDb,
            CancellationToken cancellationToken)
        {var dsd = dataflow.Dsd;

            // Create list of time dimension related columns
            var timeDim = dsd.TimeDimension;

            var timeDimensionColumns = timeDim != null
                ? string.Join(",",
                    timeDim.SqlColumn(externalColumn: true),
                    DbExtensions.SqlPeriodStart(),
                    DbExtensions.SqlPeriodEnd()
                ) +","
                : null;

            var timeDimensionColumnsAsNull = timeDim != null
                ? string.Join(",",
                    $"NULL AS {timeDim.SqlColumn(externalColumn: true)}",
                    $"NULL AS {DbExtensions.SqlPeriodStart()}",
                    $"NULL AS {DbExtensions.SqlPeriodEnd()}"
                ) + ","
                : null;

            // Create list of dimensions
            var nonTimeDimensions = dsd.Dimensions.Where(d => d != timeDim).ToList();

            var dimensionColumns = string.Join(',',
                nonTimeDimensions.Select(d =>
                    d.Codelist is null ? $"{d.SqlColumn(externalColumn: true)}" : $"[CL_{d.Code}].[ID] AS {d.SqlColumn(externalColumn: true)}"));

            var dimensionColumnsAsNull = string.Join(',', nonTimeDimensions.Select(d => $"NULL AS {d.SqlColumn(externalColumn: true)}"));

            var codedDimensions = nonTimeDimensions.Where(d => d.Codelist is not null).ToArray();

            var dimensionJoinsData = string.Join('\n',
                codedDimensions
                    .Select(d => $"LEFT JOIN [{dotStatDb.ManagementSchema}].[CL_{d.Codelist.DbId}] AS [CL_{d.Code}] ON [DATA].{d.SqlColumn(externalColumn: true)} = [CL_{d.Code}].[ITEM_ID]"));

            var dimensionJoinsAttr = string.Join('\n',
                codedDimensions
                    .Select(d => $"LEFT JOIN [{dotStatDb.ManagementSchema}].[CL_{d.Codelist.DbId}] AS [CL_{d.Code}] ON [ATTR].{d.SqlColumn(externalColumn: true)} = [CL_{d.Code}].[ITEM_ID]"));

            // Create list of attributes in FACT table
            var attributesInFactArray = dsd.Attributes.Where(a =>
                (a.Base.AttachmentLevel == AttributeAttachmentLevel.Observation) ||
                (
                    a.Base.AttachmentLevel is AttributeAttachmentLevel.DimensionGroup or AttributeAttachmentLevel.Group
                    && a.Base.GetDimensionReferences().Contains(DimensionObject.TimeDimensionFixedId)
                )
            ).ToArray();

            var attributeColumnsInFactAsNull = string.Join(',', attributesInFactArray.Select(a => $"NULL AS {a.SqlColumn(externalColumn: true)}"));

            var attributeColumnsInFact = string.Join(',',
                attributesInFactArray.Select(a =>
                    a.Codelist is null ? $"{a.SqlColumn(externalColumn: true)}" : $"[CL_{a.Code}].[ID] AS {a.SqlColumn(externalColumn: true)}"));

            var attributeJoinsInFact = string.Join('\n',
                attributesInFactArray
                    .Where(a => a.Codelist is not null)
                    .Select(a =>
                        $"LEFT JOIN [{dotStatDb.ManagementSchema}].[CL_{a.Codelist.DbId}] AS [CL_{a.Code}] ON [DATA].{a.SqlColumn(externalColumn: true)} = [CL_{a.Code}].[ITEM_ID]"));

            if (attributesInFactArray.Any())
            {
                attributeColumnsInFact += ", ";
                attributeColumnsInFactAsNull += ", ";
            }

            // Create list of attributes in ATTR table
            var attributesInAttrArray = dsd.Attributes.Where(a =>
                    a.Base.AttachmentLevel is AttributeAttachmentLevel.DimensionGroup or AttributeAttachmentLevel.Group
                    && !a.Base.GetDimensionReferences().Contains(DimensionObject.TimeDimensionFixedId)
            ).ToArray();

            var attributeColumnsInAttrAsNull = string.Join(',', attributesInAttrArray.Select(a => $"NULL AS {a.SqlColumn(externalColumn: true)}"));

            var attributeColumnsInAttr = string.Join(',',
                attributesInAttrArray.Select(a =>
                    a.Codelist is null ? $"{a.SqlColumn(externalColumn: true)}" : $"[CL_{a.Code}].[ID] AS {a.SqlColumn(externalColumn: true)}"));

            var attributeJoinsInAttr = string.Join('\n',
                attributesInAttrArray
                    .Where(a => a.Codelist is not null)
                    .Select(a => $"LEFT JOIN [{dotStatDb.ManagementSchema}].[CL_{a.Codelist.DbId}] AS [CL_{a.Code}] ON [ATTR].{a.SqlColumn(externalColumn: true)} = [CL_{a.Code}].[ITEM_ID]"));

            if (attributesInAttrArray.Any())
            {
                attributeColumnsInAttr += ", ";
                attributeColumnsInAttrAsNull += ", ";
            }

            // Create list of attributes with dataset level attachment
            var attributesAtDatasetLevelArray = dsd.Attributes.Where(a =>
                a.Base.AttachmentLevel is AttributeAttachmentLevel.DataSet or AttributeAttachmentLevel.Null
            ).ToArray();

            var attributesAtDatasetLevel = string.Join(',',
                attributesAtDatasetLevelArray.Select(a =>
                    a.Codelist is null ? $"{a.SqlColumn(externalColumn: true)}" : $"[CL_{a.Code}].[ID] AS {a.SqlColumn(externalColumn: true)}"));

            var attributesAtDatasetLevelAsNull = string.Join(',', attributesAtDatasetLevelArray.Select(a => $"NULL AS {a.SqlColumn(externalColumn: true)}"));

            var attributeJoinsAtDatasetLevel = string.Join('\n',
                attributesAtDatasetLevelArray
                    .Where(a => a.Codelist is not null)
                    .Select(a => $"LEFT JOIN [{dotStatDb.ManagementSchema}].[CL_{a.Codelist.DbId}] AS [CL_{a.Code}] ON [ADF].{a.SqlColumn(externalColumn: true)} = [CL_{a.Code}].[ITEM_ID]"));

            if (attributesAtDatasetLevelArray.Any())
            {
                attributesAtDatasetLevelAsNull += ", ";
                attributesAtDatasetLevel += ", ";
            }

            var datasetLevelSubQuery = attributesAtDatasetLevelArray.Any()
                ? $@"
-- Dataset level
SELECT NULL AS [SID], 
    {timeDimensionColumnsAsNull}
    {dimensionColumnsAsNull},
    NULL AS [OBS_VALUE],
    {attributeColumnsInFactAsNull}
    {attributeColumnsInAttrAsNull}
    {attributesAtDatasetLevel}
    {DbExtensions.LAST_UPDATED_COLUMN}
FROM  (SELECT {string.Join(',', attributesAtDatasetLevelArray.Select(a => $"{(datasetAttributeValues.GetValueOrDefault(a.SqlColumn(externalColumn:true)) ?? "NULL")} AS {a.SqlColumn(externalColumn:true)}"))}, {(datasetAttributeValues.GetValueOrDefault(DbExtensions.LAST_UPDATED_COLUMN) ?? "NULL")} AS [{DbExtensions.LAST_UPDATED_COLUMN}]) AS [ADF]
{attributeJoinsAtDatasetLevel}

UNION ALL

"
                : string.Empty;

            var dimGroupLevelSubQuery = attributesInAttrArray.Any()
                ? $@"
-- Dimension group level
SELECT [SID], 
    {timeDimensionColumnsAsNull}
    {dimensionColumns},
    NULL AS [OBS_VALUE],
    {attributeColumnsInFactAsNull}
    {attributeColumnsInAttr}
    {attributesAtDatasetLevelAsNull}
    {DbExtensions.LAST_UPDATED_COLUMN}
FROM [{dotStatDb.DataSchema}].{dataflow.SqlOptimizedAttrTable(tableVersion, hprVersion)} AS ATTR
{dimensionJoinsAttr}
{attributeJoinsInAttr}

UNION ALL

"
                : string.Empty;

            // Build the SQL command string 
            var sqlCommand = $@"
CREATE OR ALTER VIEW [{dotStatDb.DataSchema}].[{dataflow.SqlDataReplaceDataflowViewName(tableVersion)}] 
AS

{datasetLevelSubQuery}
{dimGroupLevelSubQuery}
-- Observation level
SELECT [SID], 
    {timeDimensionColumns}
    {dimensionColumns},
    [OBS_VALUE],
    {attributeColumnsInFact}
    {attributeColumnsInAttrAsNull}
    {attributesAtDatasetLevelAsNull}
    {DbExtensions.LAST_UPDATED_COLUMN}
FROM [{dotStatDb.DataSchema}].{dataflow.SqlOptimizedDataTable(tableVersion, hprVersion)} AS DATA
{dimensionJoinsData}
{attributeJoinsInFact}
";

            await dotStatDb.ExecuteNonQuerySqlAsync(sqlCommand, cancellationToken);
        }

        protected override async Task BuildOptimizedDeletedDataView(Dataflow dataflow, char tableVersion, char hprVersion, SqlDotStatDb dotStatDb,
            CancellationToken cancellationToken)
        {
            var dsd = dataflow.Dsd;

            // Create list of time dimension related columns
            var timeDim = dsd.TimeDimension;

            var timeDimensionColumns = timeDim != null
                ? string.Join(",",
                    timeDim.SqlColumn(externalColumn: true),
                    DbExtensions.SqlPeriodStart(),
                    DbExtensions.SqlPeriodEnd()
                ) + ","
                : null;

            // Create list of dimensions
            var nonTimeDimensions = dsd.Dimensions.Where(d => d != timeDim).ToList();

            var dimensionColumns = string.Join(',',
                nonTimeDimensions.Select(d =>
                    d.Codelist is null ? $"{d.SqlColumn(externalColumn: true)}" : $"[CL_{d.Code}].[ID] AS [{d.Code}]"));

            var dimensionJoins = string.Join('\n',
                nonTimeDimensions
                    .Where(d => d.Codelist is not null)
                    .Select(d => $"LEFT JOIN [{dotStatDb.ManagementSchema}].[CL_{d.Codelist.DbId}] AS [CL_{d.Code}] ON [DEL].{d.SqlColumn(externalColumn: true)} = [CL_{d.Code}].[ITEM_ID]"));

            // Create list of attributes
            var attributeColumns = string.Join(',', dsd.Attributes.Select(a => $"{a.SqlColumn(externalColumn: true)}"));

            if (!string.IsNullOrEmpty(attributeColumns))
                attributeColumns = ", " + attributeColumns;

            // Build the SQL command string 
            var sqlCommand = $@"
CREATE OR ALTER VIEW [{dotStatDb.DataSchema}].[{dataflow.SqlDeletedDataViewName(tableVersion)}] 
AS
SELECT 
    {timeDimensionColumns}
    {dimensionColumns},
    [OBS_VALUE]
    {attributeColumns}
    , {DbExtensions.LAST_UPDATED_COLUMN}
FROM [{dotStatDb.DataSchema}].{dataflow.SqlOptimizedDeletedDataTable(tableVersion, hprVersion)} AS DEL
{dimensionJoins}
";

            await dotStatDb.ExecuteNonQuerySqlAsync(sqlCommand, cancellationToken);
        }

        protected override async Task BuildDynamicDataDataFlowView(Dataflow dataFlow, char targetVersion, SqlDotStatDb dotStatDb, CancellationToken cancellationToken, bool sourceVersion = false)
        {
            //The dependent view does not exists, therefore the db is corrupted or the method is called by the unit tests
            if (!await dotStatDb.ViewExists($"{dataFlow.Dsd.SqlDataDsdViewName(targetVersion)}", cancellationToken))
            {
                return;
            }

            //Dimensions
            var dimensionsColumns = ", " + string.Join(", ", dataFlow.Dsd.Dimensions.Select(d => $"[{d.Code}]"));

            //Measure
            var measureColumn = $", [{dataFlow.Dsd.Base.PrimaryMeasure.Id}]";

            //Attributes
            var attributeColumns = string.Empty;
            var attributes = dataFlow.Dsd.Attributes
                .Where(attr => attr.Base.AttachmentLevel != AttributeAttachmentLevel.DataSet).ToList();

            if (attributes.Any())
            {
                attributeColumns = ", " + string.Join(", ", attributes.Select(a => $"[{a.Code}]"));
            }

            //Dataset attributes
            var dataSetAttributesColumns = string.Empty;
            var joinDataSetAttributes = string.Empty;
            var joinCodedDataSetAttributes = string.Empty;
            var dataSetAttributes = dataFlow.Dsd.Attributes
                .Where(attr => attr.Base.AttachmentLevel == AttributeAttachmentLevel.DataSet).ToList();

            if (dataSetAttributes.Any())
            {
                dataSetAttributesColumns = ", " + string.Join(", ", dataSetAttributes.Select(a =>
                    a.Base.HasCodedRepresentation()
                        ? $"[CL_{a.Code}].[ID] AS [{a.Code}]"
                        : $"[ADF].{a.SqlColumn()} AS [{a.Code}]"));

                joinDataSetAttributes = $@"FULL JOIN  ( SELECT * FROM [{dotStatDb.DataSchema}].[{dataFlow.Dsd.SqlDsdAttrTable(targetVersion)}] WHERE [DF_ID] = {dataFlow.DbId} ) [ADF] ON [ADF].[DF_ID] = {dataFlow.DbId}";

                joinCodedDataSetAttributes = string.Join("\n",
                    dataSetAttributes.Where(a => a.Base.HasCodedRepresentation())
                        .Select(a =>
                            $"LEFT JOIN [{dotStatDb.ManagementSchema}].[CL_{a.Codelist.DbId}] AS [CL_{a.Code}] ON [ADF].[DF_ID] = {dataFlow.DbId} AND [ADF].{a.SqlColumn()} = [CL_{a.Code}].[ITEM_ID]"));
            }

            //Time dimension columns
            var timeDimensionColumns = string.Empty;

            if (dataFlow.Dsd.TimeDimension != null)
            {
                timeDimensionColumns = ", " + string.Join(", ", DbExtensions.SqlPeriodStart(), DbExtensions.SqlPeriodEnd());
            }

            // LAST_UPDATED
            var updatedAfterColumns = $",[V].{DbExtensions.LAST_UPDATED_COLUMN}";

            var sqlCommand =
                $@"CREATE VIEW [{dotStatDb.DataSchema}].{dataFlow.SqlDataDataflowViewName(targetVersion, sourceVersion)} 
                   AS
                       SELECT [SID]{dimensionsColumns}
                            {measureColumn}
                            {attributeColumns}
                            {dataSetAttributesColumns}
                            {timeDimensionColumns}   
                            {updatedAfterColumns}
                       FROM [{dotStatDb.DataSchema}].{dataFlow.Dsd.SqlDataDsdViewName(targetVersion)} AS V 
                       {joinDataSetAttributes}
                       {joinCodedDataSetAttributes}";

            await dotStatDb.ExecuteNonQuerySqlAsync(sqlCommand, cancellationToken);
        }

        protected override async Task BuildDynamicDataReplaceDataFlowView(Dataflow dataFlow, char targetVersion, SqlDotStatDb dotStatDb, CancellationToken cancellationToken, bool sourceVersion = false)
        {
            //Dimensions
            var dimensions = dataFlow.Dsd.Dimensions.Where(dim => !dim.Base.TimeDimension).ToList();
            var dimensionsJoin = string.Join("\n", dimensions.Where(d => d.Base.HasCodedRepresentation()).Select(d =>
                    $"LEFT JOIN [{dotStatDb.ManagementSchema}].[CL_{d.Codelist.DbId}] AS [CL_{d.Code}] ON FI.{d.SqlColumn()} = [CL_{d.Code}].[ITEM_ID]"
                ));

            var dimensionsSelect = string.Join(", ", dimensions.Select(d =>
                d.Base.HasCodedRepresentation() ? $"[CL_{d.Code}].[ID] AS [{d.Code}]" : $"[FI].{d.SqlColumn()} AS [{d.Code}]")) + ',';

            var dimensionsSelectNull = string.Join(", ", dimensions.Select(d => $"NULL AS [{d.Code}]")) + ',';
            //time dimension
            var timeDim = dataFlow.Dsd.TimeDimension;
            var timeDimSelect = string.Empty;
            var timeDimSelectNull = string.Empty;
            if (timeDim != null)
            {
                timeDimSelect = string.Join(", ",
                        $"[FA].{timeDim.SqlColumn()} AS [{dataFlow.Dsd.Base.TimeDimension.Id}]",
                        DbExtensions.SqlPeriodStart(),
                        DbExtensions.SqlPeriodEnd()) + ',';
                timeDimSelectNull = string.Join(", ",
                        $"NULL AS [{dataFlow.Dsd.Base.TimeDimension.Id}]",
                        $"NULL AS {DbExtensions.SqlPeriodStart()}",
                        $"NULL AS {DbExtensions.SqlPeriodEnd()}") + ',';
            }

            //Measure
            var measureColumnSelect = $" [FA].[VALUE] AS [{dataFlow.Dsd.Base.PrimaryMeasure.Id}],";
            var measureColumnSelectNull = $" NULL AS [{dataFlow.Dsd.Base.PrimaryMeasure.Id}],";

            //Attributes
            //Obs level
            var observationAttributes = dataFlow.Dsd.Attributes
                .Where(attr => attr.Base.AttachmentLevel == AttributeAttachmentLevel.Observation ||
                               (attr.Base.AttachmentLevel == AttributeAttachmentLevel.DimensionGroup || attr.Base.AttachmentLevel == AttributeAttachmentLevel.Group) &&
                               attr.Base.GetDimensionReferences().Contains(DimensionObject.TimeDimensionFixedId)).ToList();

            var obsLvlAttrSelect = string.Empty;
            var obsLvlAttrSelectNull = string.Empty;
            var obsLvlAttrJoin = string.Empty;
            if (observationAttributes.Any())
            {
                obsLvlAttrSelect = string.Join(", ", observationAttributes.Select(a => a.Base.HasCodedRepresentation()
                        ? $"[CL_{a.Code}].[ID] AS [{a.Code}]"
                        : $"[FA].{a.SqlColumn()} AS [{a.Code}]")) + ',';

                obsLvlAttrSelectNull = string.Join(", ", observationAttributes.Select(a => $"NULL AS [{a.Code}]")) + ',';

                obsLvlAttrJoin = string.Join("\n", observationAttributes.Where(a => a.Base.HasCodedRepresentation()).Select(a =>
                         $"LEFT JOIN [{dotStatDb.ManagementSchema}].[CL_{a.Codelist.DbId}] AS [CL_{a.Code}] ON FA.{a.SqlColumn()} = [CL_{a.Code}].[ITEM_ID]"));
            }

            //dimGroup level
            var dimGroupAttributes = dataFlow.Dsd.Attributes.Where(attr =>
                    (attr.Base.AttachmentLevel == AttributeAttachmentLevel.DimensionGroup || attr.Base.AttachmentLevel == AttributeAttachmentLevel.Group) &&
                    !attr.Base.GetDimensionReferences().Contains(DimensionObject.TimeDimensionFixedId)).ToList();

            var dimGroupLvlAttrSelect = string.Empty;
            var dimGroupLvlAttrSelectNull = string.Empty;
            var dimGroupLvlAttrJoin = string.Empty;
            var hasdimGroupLevelAttributes = false;
            if (dimGroupAttributes.Any())
            {
                hasdimGroupLevelAttributes = true;
                dimGroupLvlAttrSelect = string.Join(", ", dimGroupAttributes.Select(a => a.Base.HasCodedRepresentation()
                        ? $"[CL_{a.Code}].[ID] AS [{a.Code}]"
                        : $"[ATTR].{a.SqlColumn()} AS [{a.Code}]")) + ',';

                dimGroupLvlAttrSelectNull = string.Join(", ", dimGroupAttributes.Select(a => $"NULL AS [{a.Code}]")) + ',';

                dimGroupLvlAttrJoin = string.Join("\n", dimGroupAttributes.Where(a => a.Base.HasCodedRepresentation()).Select(a =>
                            $"LEFT JOIN [{dotStatDb.ManagementSchema}].[CL_{a.Codelist.DbId}] AS [CL_{a.Code}] ON ATTR.{a.SqlColumn()} = [CL_{a.Code}].[ITEM_ID]"));
            }

            //dataSet level
            var dataSetAttributes = dataFlow.Dsd.Attributes
                .Where(attr => attr.Base.AttachmentLevel == AttributeAttachmentLevel.DataSet).ToList();

            var dataSetAttrLvlSelect = string.Empty;
            var dataSetAttrLvlSelectNull = string.Empty;
            var dataSetAttrLvlJoin = string.Empty;
            var hasDataSetLevelAttributes = false;
            if (dataSetAttributes.Any())
            {
                hasDataSetLevelAttributes = true;
                dataSetAttrLvlSelect = string.Join(", ", dataSetAttributes.Select(a => a.Base.HasCodedRepresentation()
                    ? $"[CL_{a.Code}].[ID] AS [{a.Code}]"
                    : $"[ADF].{a.SqlColumn()} AS [{a.Code}]")) + ',';

                dataSetAttrLvlSelectNull = string.Join(", ", dataSetAttributes.Select(a => $"NULL AS [{a.Code}]")) + ',';

                dataSetAttrLvlJoin = string.Join("\n", dataSetAttributes.Where(a => a.Base.HasCodedRepresentation()).Select(a =>
                         $"LEFT JOIN [{dotStatDb.ManagementSchema}].[CL_{a.Codelist.DbId}] AS [CL_{a.Code}] ON [ADF].[DF_ID] = {dataFlow.DbId} AND [ADF].{a.SqlColumn()} = [CL_{a.Code}].[ITEM_ID]"));
            }

            //Tables
            var filterTable = $"[{dotStatDb.DataSchema}].{dataFlow.Dsd.SqlFilterTable("FI")}";

            var ctePart =
                $@"WITH OBS_LEVEL AS (
	SELECT FI.[SID], {dimensionsSelect}{timeDimSelect}
        {measureColumnSelect}
        {obsLvlAttrSelect}
	    {dimGroupLvlAttrSelectNull}
	    {dataSetAttrLvlSelectNull}
	    [{DbExtensions.LAST_UPDATED_COLUMN}]
	 FROM [{dotStatDb.DataSchema}].{dataFlow.Dsd.SqlFactTable(targetVersion, "FA")}
	 LEFT JOIN {filterTable} ON FI.[SID] = FA.[SID]
	 {dimensionsJoin}
	 {obsLvlAttrJoin}
 )";

            var subQueryPart = "SELECT * FROM OBS_LEVEL";

            if (hasdimGroupLevelAttributes)
            {
                ctePart += $@", SERIES_LEVEL AS (
	SELECT FI.[SID], {dimensionsSelect}{timeDimSelectNull}
        {measureColumnSelectNull}
        {obsLvlAttrSelectNull}
	    {dimGroupLvlAttrSelect}
	    {dataSetAttrLvlSelectNull}
	    [{DbExtensions.LAST_UPDATED_COLUMN}]
	 FROM [{dotStatDb.DataSchema}].{dataFlow.Dsd.SqlDimGroupAttrTable(targetVersion, "ATTR")}
	 LEFT JOIN {filterTable} ON FI.[SID] = ATTR.[SID]
	 {dimensionsJoin}
	 {dimGroupLvlAttrJoin}
 )";

                subQueryPart = "SELECT * FROM SERIES_LEVEL"  + Environment.NewLine + "UNION ALL" + Environment.NewLine + subQueryPart;
            }

            if (hasDataSetLevelAttributes)
            {
                ctePart += $@", DF_LEVEL AS (
	SELECT NULL AS [SID], {dimensionsSelectNull}{timeDimSelectNull}
        {measureColumnSelectNull}
        {obsLvlAttrSelectNull}
	    {dimGroupLvlAttrSelectNull}
	    {dataSetAttrLvlSelect}
	    [{DbExtensions.LAST_UPDATED_COLUMN}]
	FROM [{dotStatDb.DataSchema}].{dataFlow.Dsd.SqlDsdAttrTable(targetVersion, "ADF")}
	{dataSetAttrLvlJoin}
	WHERE [ADF].[DF_ID] = {dataFlow.DbId}
 )";
                subQueryPart = "SELECT * FROM DF_LEVEL" + Environment.NewLine + "UNION ALL" + Environment.NewLine + subQueryPart;
            }

            var viewDefinition = $@"CREATE VIEW [{dotStatDb.DataSchema}].{dataFlow.SqlDataReplaceDataflowViewName(targetVersion, sourceVersion)} AS
{ctePart}
{subQueryPart}
";

            await dotStatDb.ExecuteNonQuerySqlAsync(viewDefinition, cancellationToken);
        }

        protected override async Task BuildDynamicDataIncludeHistoryDataFlowView(Dataflow dataFlow, char targetVersion, SqlDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            var allComponentsSelect = "[SID]";

            //Dimensions
            var dimensions = dataFlow.Dsd.Dimensions.Where(dim => !dim.Base.TimeDimension).ToList();

            allComponentsSelect += ", " + dimensions.ToColumnList(externalColumn: true);
            var dimensionsJoin = string.Join("\n", dimensions.Where(d => d.Base.HasCodedRepresentation()).Select(d =>
                    $"LEFT JOIN [{dotStatDb.ManagementSchema}].[CL_{d.Codelist.DbId}] AS [CL_{d.Code}] ON FI.{d.SqlColumn()} = [CL_{d.Code}].[ITEM_ID]"
                ));

            var dimensionsSelect = string.Join(", ", dimensions.Select(d =>
                d.Base.HasCodedRepresentation() ? $"[CL_{d.Code}].[ID] AS [{d.Code}]" : $"[FI].{d.SqlColumn()} AS [{d.Code}]")) + ',';

            var dimensionsSelectNull = string.Join(", ", dimensions.Select(d => $"NULL AS [{d.Code}]")) + ',';
            //time dimension
            var timeDim = dataFlow.Dsd.TimeDimension;
            var timeDimSelect = string.Empty;
            var timeDimSelectNull = string.Empty;
            if (timeDim != null)
            {
                allComponentsSelect += ", " + string.Join(", ",
                    $"[{dataFlow.Dsd.Base.TimeDimension.Id}]",
                    DbExtensions.SqlPeriodStart(),
                    DbExtensions.SqlPeriodEnd());

                timeDimSelect = string.Join(", ",
                         $"[FA].{timeDim.SqlColumn()} AS [{dataFlow.Dsd.Base.TimeDimension.Id}]",
                         DbExtensions.SqlPeriodStart(),
                         DbExtensions.SqlPeriodEnd()) + ',';
                timeDimSelectNull = string.Join(", ",
                        $"NULL AS [{dataFlow.Dsd.Base.TimeDimension.Id}]",
                        $"NULL AS {DbExtensions.SqlPeriodStart()}",
                        $"NULL AS {DbExtensions.SqlPeriodEnd()}") + ',';
            }

            //Measure
            var measureColumnSelect = $" [FA].[VALUE] AS [{dataFlow.Dsd.Base.PrimaryMeasure.Id}],";
            var measureColumnSelectNull = $" NULL AS [{dataFlow.Dsd.Base.PrimaryMeasure.Id}],";

            //Attributes
            //Obs level
            var observationAttributes = dataFlow.Dsd.Attributes
                .Where(attr => attr.Base.AttachmentLevel == AttributeAttachmentLevel.Observation ||
                               (attr.Base.AttachmentLevel == AttributeAttachmentLevel.DimensionGroup || attr.Base.AttachmentLevel == AttributeAttachmentLevel.Group) &&
                               attr.Base.GetDimensionReferences().Contains(DimensionObject.TimeDimensionFixedId)).ToList();

            var obsLvlAttrSelect = string.Empty;
            var obsLvlAttrSelectNull = string.Empty;
            var obsLvlAttrJoin = string.Empty;
            if (observationAttributes.Any())
            {
                obsLvlAttrSelect = string.Join(", ", observationAttributes.Select(a => a.Base.HasCodedRepresentation()
                        ? $"[CL_{a.Code}].[ID] AS [{a.Code}]"
                        : $"[FA].{a.SqlColumn()} AS [{a.Code}]")) + ',';

                obsLvlAttrSelectNull = string.Join(", ", observationAttributes.Select(a => $"NULL AS [{a.Code}]")) + ',';

                obsLvlAttrJoin = string.Join("\n", observationAttributes.Where(a => a.Base.HasCodedRepresentation()).Select(a =>
                         $"LEFT JOIN [{dotStatDb.ManagementSchema}].[CL_{a.Codelist.DbId}] AS [CL_{a.Code}] ON FA.{a.SqlColumn()} = [CL_{a.Code}].[ITEM_ID]"));
            }

            //dimGroup level
            var dimGroupAttributes = dataFlow.Dsd.Attributes.Where(attr =>
                    (attr.Base.AttachmentLevel == AttributeAttachmentLevel.DimensionGroup || attr.Base.AttachmentLevel == AttributeAttachmentLevel.Group) &&
                    !attr.Base.GetDimensionReferences().Contains(DimensionObject.TimeDimensionFixedId)).ToList();

            var dimGroupLvlAttrSelect = string.Empty;
            var dimGroupLvlAttrSelectNull = string.Empty;
            var dimGroupLvlAttrJoin = string.Empty;
            var hasdimGroupLevelAttributes = false;
            if (dimGroupAttributes.Any())
            {
                hasdimGroupLevelAttributes = true;
                dimGroupLvlAttrSelect = string.Join(", ", dimGroupAttributes.Select(a => a.Base.HasCodedRepresentation()
                        ? $"[CL_{a.Code}].[ID] AS [{a.Code}]"
                        : $"[ATTR].{a.SqlColumn()} AS [{a.Code}]")) + ',';

                dimGroupLvlAttrSelectNull = string.Join(", ", dimGroupAttributes.Select(a => $"NULL AS [{a.Code}]")) + ',';

                dimGroupLvlAttrJoin = string.Join("\n", dimGroupAttributes.Where(a => a.Base.HasCodedRepresentation()).Select(a =>
                            $"LEFT JOIN [{dotStatDb.ManagementSchema}].[CL_{a.Codelist.DbId}] AS [CL_{a.Code}] ON ATTR.{a.SqlColumn()} = [CL_{a.Code}].[ITEM_ID]"));
            }

            //dataSet level
            var dataSetAttributes = dataFlow.Dsd.Attributes
                .Where(attr => attr.Base.AttachmentLevel == AttributeAttachmentLevel.DataSet).ToList();

            var dataSetAttrLvlSelect = string.Empty;
            var dataSetAttrLvlSelectNull = string.Empty;
            var dataSetAttrLvlJoin = string.Empty;
            var hasDataSetLevelAttributes = false;
            if (dataSetAttributes.Any())
            {
                hasDataSetLevelAttributes = true;
                dataSetAttrLvlSelect = string.Join(", ", dataSetAttributes.Select(a => a.Base.HasCodedRepresentation()
                    ? $"[CL_{a.Code}].[ID] AS [{a.Code}]"
                    : $"[ADF].{a.SqlColumn()} AS [{a.Code}]")) + ',';

                dataSetAttrLvlSelectNull = string.Join(", ", dataSetAttributes.Select(a => $"NULL AS [{a.Code}]")) + ',';

                dataSetAttrLvlJoin = string.Join("\n", dataSetAttributes.Where(a => a.Base.HasCodedRepresentation()).Select(a =>
                         $"LEFT JOIN [{dotStatDb.ManagementSchema}].[CL_{a.Codelist.DbId}] AS [CL_{a.Code}] ON [ADF].[DF_ID] = {dataFlow.DbId} AND [ADF].{a.SqlColumn()} = [CL_{a.Code}].[ITEM_ID]"));
            }

            var allAttributesSelect = dataFlow.Dsd.Attributes.ToColumnList(externalColumn: true);
            if (!string.IsNullOrEmpty(allAttributesSelect))
                allComponentsSelect += "," + allAttributesSelect;
            allComponentsSelect += $",[{dataFlow.Dsd.Base.PrimaryMeasure.Id}]";
            allComponentsSelect += $",[{DbExtensions.LAST_UPDATED_COLUMN}], [ValidTo]";

            //Tables
            var filterTable = $"[{dotStatDb.DataSchema}].{dataFlow.Dsd.SqlFilterTable("FI")}";

            var ctePart =
                $@"WITH OBS_LEVEL AS (
	SELECT FI.[SID], {dimensionsSelect}{timeDimSelect}
        {measureColumnSelect}
        {obsLvlAttrSelect}
	    {dimGroupLvlAttrSelectNull}
	    {dataSetAttrLvlSelectNull}
		[FA].[ValidFrom] AS [{DbExtensions.LAST_UPDATED_COLUMN}],
        [FA].[ValidTo] 
	 FROM [{dotStatDb.DataSchema}].{dataFlow.Dsd.SqlFactTable(targetVersion)} FOR SYSTEM_TIME ALL FA
	 LEFT JOIN {filterTable} ON FI.[SID] = FA.[SID]
	 {dimensionsJoin}
	 {obsLvlAttrJoin}
 )";

            var subQueryPart = $"SELECT {allComponentsSelect} FROM OBS_LEVEL";

            if (hasdimGroupLevelAttributes)
            {
                ctePart += $@", SERIES_LEVEL AS (
	SELECT FI.[SID], {dimensionsSelect}{timeDimSelectNull}
        {measureColumnSelectNull}
        {obsLvlAttrSelectNull}
	    {dimGroupLvlAttrSelect}
	    {dataSetAttrLvlSelectNull}
		ATTR.[ValidFrom] AS [{DbExtensions.LAST_UPDATED_COLUMN}],
        ATTR.[ValidTo]
	 FROM [{dotStatDb.DataSchema}].{dataFlow.Dsd.SqlDimGroupAttrTable(targetVersion)} FOR SYSTEM_TIME ALL ATTR
	 LEFT JOIN {filterTable} ON FI.[SID] = ATTR.[SID]
	 {dimensionsJoin}
	 {dimGroupLvlAttrJoin}
 )";

                subQueryPart = $"SELECT {allComponentsSelect} FROM SERIES_LEVEL" + Environment.NewLine + "UNION ALL" + Environment.NewLine + subQueryPart;
            }

            if (hasDataSetLevelAttributes)
            {
                ctePart += $@", DF_LEVEL AS (
	SELECT NULL AS [SID], {dimensionsSelectNull}{timeDimSelectNull}
        {measureColumnSelectNull}
        {obsLvlAttrSelectNull}
	    {dimGroupLvlAttrSelectNull}
	    {dataSetAttrLvlSelect}
		ADF.[ValidFrom] AS [{DbExtensions.LAST_UPDATED_COLUMN}],
        ADF.[ValidTo]
	FROM [{dotStatDb.DataSchema}].{dataFlow.Dsd.SqlDsdAttrTable(targetVersion)} FOR SYSTEM_TIME ALL ADF
	{dataSetAttrLvlJoin}
	WHERE [ADF].[DF_ID] = {dataFlow.DbId}
 )";
                subQueryPart = $"SELECT {allComponentsSelect} FROM DF_LEVEL" + Environment.NewLine + "UNION ALL" + Environment.NewLine + subQueryPart;
            }

            var viewDefinition = $@"CREATE VIEW [{dotStatDb.DataSchema}].{dataFlow.SqlDataIncludeHistoryDataflowViewName(targetVersion)} AS
{ctePart}
{subQueryPart}
";

            await dotStatDb.ExecuteNonQuerySqlAsync(viewDefinition, cancellationToken);
        }

        protected override async Task BuildDeletedView(Dataflow dataFlow, char targetVersion, SqlDotStatDb dotStatDb, CancellationToken cancellationToken, bool sourceVersion = false)
        {
            //The dependent table does not exists, therefore the db is corrupted or the method is called by the unit tests
            if (!await dotStatDb.TableExists($"{dataFlow.Dsd.SqlDeletedTable(targetVersion)}", cancellationToken))
            {
                return;
            }

            var dimensions = dataFlow.Dsd.Dimensions
                .Where(dim => !dim.Base.TimeDimension)
                .ToList();

            //Time dimension columns
            var innerTimeDimensionColumns = dataFlow.Dsd.TimeDimension != null
                    ? ", " + string.Join(", ",
                            $"[{DbExtensions.TimeDimColumn}] AS [{dataFlow.Dsd.Base.TimeDimension.Id}]",
                            DbExtensions.SqlPeriodStart(),
                            DbExtensions.SqlPeriodEnd()
                        )
                    : string.Empty;

            var innerTimeDimensionColumns4GroupBy = dataFlow.Dsd.TimeDimension != null
                ? ", " + string.Join(", ",
                    $"[{DbExtensions.TimeDimColumn}]",
                    DbExtensions.SqlPeriodStart(),
                    DbExtensions.SqlPeriodEnd()
                )
                : string.Empty;

            var outerTimeDimensionColumns = dataFlow.Dsd.TimeDimension != null
                ? ", " + string.Join(", ",
                    $"[{dataFlow.Dsd.Base.TimeDimension.Id}]",
                    DbExtensions.SqlPeriodStart(),
                    DbExtensions.SqlPeriodEnd()
                )
                : string.Empty;

            //Dimensions
            var innerDimensionsColumns = dimensions.Any()
                        ? ", " + string.Join(", ", dimensions.Select(d => $"{d.SqlColumn()}"))
                        : string.Empty;

            var outerDimensionsColumns = dimensions.Any()
                ? ", " + string.Join(", ", dimensions.Select(d => d.Base.HasCodedRepresentation() ? $"[CL_{d.Code}].[ID] AS [{d.Code}]" : $"{d.SqlColumn()} AS [{d.Code}]"))
                : string.Empty;


            //Attributes
            var innerAttributeColumns4SomeComp = dataFlow.Dsd.Attributes.Any()
                ? "," + string.Join(",", dataFlow.Dsd.Attributes.Select(a => $"MAX( {a.SqlColumn()} ) AS [{a.Code}]"))
                : string.Empty;

            var innerAttributeColumns4NoComp = dataFlow.Dsd.Attributes.Any()
                ? "," + string.Join(",", dataFlow.Dsd.Attributes.Select(a => $"NULL AS [{a.Code}]"))
                : string.Empty;


            var outerAttributeColumns = dataFlow.Dsd.Attributes.Any()
                ? "," + string.Join(",", dataFlow.Dsd.Attributes.Select(a => $" [{a.Code}]"))
                : string.Empty;

            var joinStatements = string.Join(Environment.NewLine, dimensions.Where(d => d.Base.HasCodedRepresentation()).Select(d =>
                    $"LEFT JOIN [{dotStatDb.ManagementSchema}].[CL_{d.Codelist.DbId}] AS [CL_{d.Code}] ON [DEL].{d.SqlColumn()} = [CL_{d.Code}].[ITEM_ID]"
                )
            );


            var innerSelectPartCommon =
$@"SELECT MAX( {DbExtensions.ValueColumn} ) AS [OBS_VALUE]
    {innerTimeDimensionColumns}
    {innerDimensionsColumns}
    , MAX( {DbExtensions.LAST_UPDATED_COLUMN} ) AS {DbExtensions.LAST_UPDATED_COLUMN}
";
            var innerFromPart = $@"FROM [{dotStatDb.DataSchema}].{dataFlow.Dsd.SqlDeletedTable(targetVersion)} ";

            var innerWherePart4NoComp = $@"WHERE [DF_ID] = {dataFlow.DbId} " +
                                        (dataFlow.Dsd.Attributes.Any()
                                            ? Environment.NewLine + "AND " + string.Join(" AND ",
                                                dataFlow.Dsd.Attributes.Select(a => $"{a.SqlColumn()} IS NULL"))
                                            : string.Empty) + Environment.NewLine +
                                        " AND [VALUE] IS NULL";

            var innerWherePart4SomeComp = $@"WHERE [DF_ID] = {dataFlow.DbId} " +
                                          "AND (" +
                                          (dataFlow.Dsd.Attributes.Any()
                                              ? Environment.NewLine + string.Join(" OR ",
                                                  dataFlow.Dsd.Attributes.Select(a => $"{a.SqlColumn()} IS NOT NULL"))
                                              : "1 = 1 ") +
                                          " OR [VALUE] IS NOT NULL )";

            var innerGroupByPart = $@"GROUP BY {innerDimensionsColumns.TrimStart(',')}{innerTimeDimensionColumns4GroupBy} ";

            var innerUnionSql =
                $@"{innerSelectPartCommon}{innerAttributeColumns4SomeComp}
   {innerFromPart} 
   {innerWherePart4SomeComp}
   {innerGroupByPart}
UNION
   {innerSelectPartCommon}{innerAttributeColumns4NoComp}
   {innerFromPart}
   {innerWherePart4NoComp}
   {innerGroupByPart}";


            var sqlCommand =
                $@"CREATE VIEW [{dotStatDb.DataSchema}].{dataFlow.SqlDeletedDataViewName(targetVersion, sourceVersion)} 
                   AS
                       SELECT [OBS_VALUE]
                            {outerTimeDimensionColumns}
                            {outerDimensionsColumns}
                            {outerAttributeColumns}                            
                            ,{DbExtensions.LAST_UPDATED_COLUMN}
                       FROM ( {innerUnionSql} ) AS [DEL]
                            {joinStatements}";

            await dotStatDb.ExecuteNonQuerySqlAsync(sqlCommand, cancellationToken);
        }

    }
}