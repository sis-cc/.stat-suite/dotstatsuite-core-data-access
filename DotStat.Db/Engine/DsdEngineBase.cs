﻿using DotStat.Common.Configuration.Interfaces;
using DotStat.Db.DB;
using DotStat.Domain;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace DotStat.Db.Engine
{

    public abstract class DsdEngineBase<TDotStatDb> : ArtefactEngineBase<Dsd, TDotStatDb>
        where TDotStatDb : IDotStatDb
    {
        protected DsdEngineBase(IGeneralConfiguration generalConfiguration) : base(generalConfiguration)
        {
        }
        
        public override async Task<bool> CreateDynamicDbObjects(Dsd dsd, TDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            if (dsd == null)
            {
                throw new ArgumentNullException(nameof(dsd));
            }

            if (dotStatDb == null)
            {
                throw new ArgumentNullException(nameof(dotStatDb));
            }

            await BuildDynamicFactTable(dsd, (char)DbTableVersion.A, dotStatDb, cancellationToken);
            await BuildDynamicFactTable(dsd, (char)DbTableVersion.B, dotStatDb, cancellationToken);
            await BuildDynamicFilterTable(dsd, dotStatDb, cancellationToken);

            await BuildDeletedTable(dsd, (char)DbTableVersion.A, dotStatDb, cancellationToken);
            await BuildDeletedTable(dsd, (char)DbTableVersion.B, dotStatDb, cancellationToken);

            await BuildDynamicDataDsdView(dsd, (char) DbTableVersion.A, dotStatDb, cancellationToken);
            await BuildDynamicDataDsdView(dsd, (char) DbTableVersion.B, dotStatDb, cancellationToken);
            
            return true;
        }

        public override Task CleanUp(Dsd dsd, TDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }

        public abstract Task DropAllIndexes(Dsd dsd, TDotStatDb dotStatDb, CancellationToken cancellationToken);
        public abstract Task Compress(Dsd dsd, DataCompressionEnum dataCompression, TDotStatDb dotStatDb, CancellationToken cancellationToken);
        public abstract Task AddUniqueConstraints(Dsd dsd, bool clustered, TDotStatDb dotStatDb, CancellationToken cancellationToken);

        protected abstract Task BuildDynamicFactTable(Dsd dsd, char targetVersion, TDotStatDb dotStatDb, CancellationToken cancellationToken);

        protected abstract Task BuildDeletedTable(Dsd dsd, char targetVersion, TDotStatDb dotStatDb, CancellationToken cancellationToken);

        protected abstract Task BuildDynamicFilterTable(Dsd dsd, TDotStatDb dotStatDb, CancellationToken cancellationToken);

        protected abstract Task BuildDynamicDataDsdView(Dsd dsd, char targetVersion, TDotStatDb dotStatDb, CancellationToken cancellationToken);

    }
}