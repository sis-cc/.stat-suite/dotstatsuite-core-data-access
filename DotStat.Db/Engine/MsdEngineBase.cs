﻿using System;
using System.Threading;
using System.Threading.Tasks;
using DotStat.Common.Configuration.Interfaces;
using DotStat.Common.Exceptions;
using DotStat.Common.Localization;
using DotStat.Db.DB;
using DotStat.Domain;

namespace DotStat.Db.Engine
{

    public abstract class MsdEngineBase<TDotStatDb> : ArtefactEngineBase<Msd, TDotStatDb>
        where TDotStatDb : IDotStatDb
    {
        protected MsdEngineBase(IGeneralConfiguration generalConfiguration) : base(generalConfiguration)
        {
        }

        public async ValueTask<bool> CreateDynamicDbObjects(Dsd dsd, TDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            if (dsd == null)
            {
                throw new ArgumentNullException(nameof(dsd));
            }

            if (dsd.Msd == null)
            {
                throw new ArgumentNullException(nameof(dsd.Msd));
            }

            if (dotStatDb == null)
            {
                throw new ArgumentNullException(nameof(dotStatDb));
            }

            await BuildDynamicMetaDataTable(dsd, (char)DbTableVersion.A, dotStatDb, cancellationToken);
            await BuildDynamicMetaDataTable(dsd, (char)DbTableVersion.B, dotStatDb, cancellationToken);

            await BuildDynamicDatasetMetaDataTable(dsd, (char)DbTableVersion.A, dotStatDb, cancellationToken);
            await BuildDynamicDatasetMetaDataTable(dsd, (char)DbTableVersion.B, dotStatDb, cancellationToken);
            
            await BuildDeletedTable(dsd, (char)DbTableVersion.A, dotStatDb, cancellationToken);
            await BuildDeletedTable(dsd, (char)DbTableVersion.B, dotStatDb, cancellationToken);

            await BuildDeletedView(dsd, (char)DbTableVersion.A, dotStatDb, false, cancellationToken);
            await BuildDeletedView(dsd, (char)DbTableVersion.B, dotStatDb, false, cancellationToken);

            await BuildDynamicMetaDataDsdView(dsd, (char) DbTableVersion.A, dotStatDb, false, cancellationToken);
            return await BuildDynamicMetaDataDsdView(dsd, (char) DbTableVersion.B, dotStatDb, false, cancellationToken);
        }

        public override async Task CleanUp(Msd msd, TDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            //Does not delete nor changes structure of related metadata table!

            if (msd == null)
            {
                throw new ArgumentNullException(nameof(msd));
            }

            if (dotStatDb == null)
            {
                throw new ArgumentNullException(nameof(dotStatDb));
            }

            if (msd.DbId < 1)
            {
                throw new DotStatException(string.Format(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.MsdInvalidDbId),
                    msd.FullId, msd.DbId)
                );
            }

            await DeleteFromArtefactTableByDbId(msd.DbId, dotStatDb, cancellationToken);
        }

        public abstract Task DropAllIndexes(Dsd dsd, TDotStatDb dotStatDb, CancellationToken cancellationToken);
        public abstract Task Compress(Dsd dsd, DataCompressionEnum dataCompression, TDotStatDb dotStatDb, CancellationToken cancellationToken);
        public abstract Task AddUniqueConstraints(Dsd dsd, bool clustered, TDotStatDb dotStatDb, CancellationToken cancellationToken);

        protected abstract Task BuildDynamicMetaDataTable(Dsd dsd, char targetVersion, TDotStatDb dotStatDb, CancellationToken cancellationToken);

        protected abstract Task BuildDynamicDatasetMetaDataTable(Dsd dsd, char targetVersion, TDotStatDb dotStatDb, CancellationToken cancellationToken);
        
        protected abstract Task BuildDeletedTable(Dsd dsd, char targetVersion, TDotStatDb dotStatDb, CancellationToken cancellationToken);

        public abstract ValueTask<bool> BuildDynamicMetaDataDsdView(Dsd dsd, char targetVersion, TDotStatDb dotStatDb, bool sourceVersion, CancellationToken cancellationToken);

        public abstract Task BuildDeletedView(Dsd dsd, char targetVersion, TDotStatDb dotStatDb, bool sourceVersion, CancellationToken cancellationToken);
    }
}