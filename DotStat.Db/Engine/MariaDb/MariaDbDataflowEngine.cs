﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using DotStat.Common.Configuration.Interfaces;
using DotStat.Common.Enums;
using DotStat.Db;
using DotStat.Db.DB;
using DotStat.Db.Engine;
using DotStat.Db.Util;
using DotStat.Domain;
using MySqlConnector;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Constants.InterfaceConstant;
using Org.Sdmxsource.Sdmx.Api.Model.Data;

namespace DotStat.DB.Engine.MariaDb
{
    public class MariaDbDataflowEngine : DataflowEngineBase<MariaDbDotStatDb> {

        public MariaDbDataflowEngine(IGeneralConfiguration generalConfiguration) : base(generalConfiguration) { }

        public override async Task<int> GetDbId(string sdmxId, string sdmxAgency, int verMajor, int verMinor, int? verPatch, MariaDbDotStatDb dotStatDb, CancellationToken cancellationToken) {
            var aretefactDbId = await dotStatDb.ExecuteScalarSqlWithParamsAsync(
                $@"SELECT ART_ID 
                    FROM {dotStatDb.ManagementSchema}_ARTEFACT 
                   WHERE ID = @Id AND AGENCY = @Agency AND `TYPE` = @Type
                     AND VERSION_1 = @Version1 AND VERSION_2 = @Version2 
                     AND (VERSION_3 = @Version3 OR (VERSION_3 IS NULL AND @Version3 IS NULL))",
                cancellationToken,
                new MySqlParameter("Id", MySqlDbType.VarChar) { Value = sdmxId },
                new MySqlParameter("Agency", MySqlDbType.VarChar) { Value = sdmxAgency },
                new MySqlParameter("Type", MySqlDbType.VarChar) { Value = DbTypes.GetDbType(SDMXArtefactType.Dataflow) },
                new MySqlParameter("Version1", MySqlDbType.Int32) { Value = verMajor },
                new MySqlParameter("Version2", MySqlDbType.Int32) { Value = verMinor },
                new MySqlParameter("Version3", MySqlDbType.Int32) { Value = ((object) verPatch) ?? DBNull.Value }
            );

            return (int) (aretefactDbId ?? -1);
        }

        public override async Task<int> InsertToArtefactTable(Dataflow dataflow, MariaDbDotStatDb dotStatDb, CancellationToken cancellationToken) {
            return await dotStatDb.ExecuteScalarSqlLastInsertedIdWithParamsAsync(
                $@"INSERT 
                    INTO {dotStatDb.ManagementSchema}_ARTEFACT 
                         (SQL_ART_ID,`TYPE`,ID,AGENCY,VERSION_1,VERSION_2,VERSION_3,LAST_UPDATED,DF_DSD_ID,DF_WHERE_CLAUSE) 
                    VALUES (NULL, @Type, @Id, @Agency, @Version1, @Version2, @Version3, @DT_Now, @DsdId, NULL)",
                cancellationToken,
                new MySqlParameter("Type", MySqlDbType.VarChar) { Value = DbTypes.GetDbType(SDMXArtefactType.Dataflow) },
                new MySqlParameter("Id", MySqlDbType.VarChar) { Value = dataflow.Code },
                new MySqlParameter("Agency", MySqlDbType.VarChar) { Value = dataflow.AgencyId },
                new MySqlParameter("Version1", MySqlDbType.Int32) { Value = dataflow.Version.Major },
                new MySqlParameter("Version2", MySqlDbType.Int32) { Value = dataflow.Version.Minor },
                new MySqlParameter("Version3", MySqlDbType.Int32) { Value = (object) dataflow.Version.Patch ?? DBNull.Value },
                new MySqlParameter("DsdId", MySqlDbType.Int32) { Value = dataflow.Dsd.DbId },
                new MySqlParameter("DT_Now", DateTime.Now)
            );
        }

        protected override async Task DeleteFromArtefactTableByDbId(int dbId, MariaDbDotStatDb dotStatDb, CancellationToken cancellationToken) {
            await dotStatDb.ExecuteNonQuerySqlWithParamsAsync(
                $@"DELETE
                    FROM {dotStatDb.ManagementSchema}_ARTEFACT
                   WHERE ART_ID = @DbId AND `TYPE` = 'DF'",
                cancellationToken,
                new MySqlParameter("DbId", MySqlDbType.VarChar) { Value = dbId }
            );
        }

        protected override Task BuildOptimizedDataTable(Dataflow dataflow, char targetVersion, char hprVersion, MariaDbDotStatDb dotStatDb, ICodeTranslator codeTranslator, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        protected override Task BuildOptimizedAttributeTable(Dataflow dataflow, char targetVersion, char hprVersion, MariaDbDotStatDb dotStatDb,
            ICodeTranslator codeTranslator, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        protected override Task BuildOptimizedDeletedDataTable(Dataflow dataflow, char targetVersion, char hprVersion, MariaDbDotStatDb dotStatDb,
            ICodeTranslator codeTranslator, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        protected override Task BuildOptimizedDataView(Dataflow dataflow, char tableVersion, char hprVersion,
            IReadOnlyDictionary<string, string> datasetAttributes,
            MariaDbDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        protected override Task BuildOptimizedReplaceView(Dataflow dataflow, char tableVersion, char hprVersion,
            IReadOnlyDictionary<string, string> datasetAttributes, MariaDbDotStatDb dotStatDb,
            CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        protected override Task BuildOptimizedDeletedDataView(Dataflow dataflow, char tableVersion, char hprVersion, MariaDbDotStatDb dotStatDb,
            CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        protected override async Task BuildDynamicDataDataFlowView(Dataflow dataFlow, char targetVersion, MariaDbDotStatDb dotStatDb, CancellationToken cancellationToken, bool sourceVersion = false) {
            //The dependent view does not exists, therefore the db is corrupted or the method is called by the unit tests
            if (!await dotStatDb.ViewExists($"{dataFlow.Dsd.MariaDbDataDsdViewNameWithSchema(targetVersion)}", cancellationToken)) {
                return;
            }

            //Dimensions
            var dimensionsColumns = string.Join(", ", dataFlow.Dsd.Dimensions.Select(d => $"`{d.Code}`"));
            var nullDimensionsColumns = string.Join(", ", dataFlow.Dsd.Dimensions.Select(d => $"NULL AS `{d.Code}`"));

            //Measure
            var measureColumn = $"`{dataFlow.Dsd.Base.PrimaryMeasure.Id}`";

            //Attributes
            var attributeColumns = string.Empty;
            var nullAttributeColumns = string.Empty;
            var attributes = dataFlow.Dsd.Attributes
                .Where(attr => attr.Base.AttachmentLevel != AttributeAttachmentLevel.DataSet).ToList();

            if (attributes.Any()) {
                attributeColumns = ", " + string.Join(", ", attributes.Select(a => $"`{a.Code}`"));
                nullAttributeColumns = ", " + string.Join(", ", attributes.Select(a => $"NULL AS `{a.Code}`"));
            }

            //Dataset attributes
            var dataSetAttributesColumns = string.Empty;
            var joinDataSetAttributes = string.Empty;
            var joinDataSetAttributesRev = string.Empty;
            var joinCodedDataSetAttributes = string.Empty;
            var dataSetAttributes = dataFlow.Dsd.Attributes
                .Where(attr => attr.Base.AttachmentLevel == AttributeAttachmentLevel.DataSet).ToList();

            if (dataSetAttributes.Any()) {
                dataSetAttributesColumns = ", " + string.Join(", ", dataSetAttributes.Select(a =>
                    a.Base.HasCodedRepresentation()
                        ? $"CL_{a.Code}.ID AS `{a.Code}`"
                        : $"ADF.{a.MariaDbColumn()} AS `{a.Code}`"));

                joinDataSetAttributes = $@"LEFT JOIN {dotStatDb.DataSchema}_{dataFlow.Dsd.MariaDbDsdAttrTable(targetVersion)} ADF ON ADF.DF_ID = {dataFlow.DbId}";

                joinCodedDataSetAttributes = string.Join("\n",
                    dataSetAttributes.Where(a => a.Base.HasCodedRepresentation())
                        .Select(a =>
                            $"LEFT JOIN {dotStatDb.ManagementSchema}_CL_{a.Codelist.DbId} AS CL_{a.Code} ON ADF.DF_ID = {dataFlow.DbId} AND ADF.{a.MariaDbColumn()} = CL_{a.Code}.ITEM_ID"));
            }

            //Time dimension columns
            var timeDimensionColumns = string.Empty;
            var nullTimeDimensionColumns = string.Empty;

            if (dataFlow.Dsd.TimeDimension != null) {
                timeDimensionColumns = ", " + string.Join(", ", MariaDbExtensions.MariaDbPeriodStart(), MariaDbExtensions.MariaDbPeriodEnd());
                nullTimeDimensionColumns = $", NULL AS {MariaDbExtensions.MariaDbPeriodStart()}, NULL AS {MariaDbExtensions.MariaDbPeriodEnd()}";
            }

            // LAST_UPDATED
            var updatedAfterColumn = $"{MariaDbExtensions.LAST_UPDATED_COLUMN}";

            var sqlCommand =
                $@"CREATE ALGORITHM = UNDEFINED SQL SECURITY INVOKER VIEW {dotStatDb.DataSchema}_{dataFlow.MariaDbDataflowViewName(targetVersion)} AS
(
       SELECT SID
            ,{dimensionsColumns}
            ,{measureColumn}
            {attributeColumns}
            {dataSetAttributesColumns}
            {timeDimensionColumns}   
            ,V.{updatedAfterColumn}
       FROM {dotStatDb.DataSchema}_{dataFlow.Dsd.MariaDbDataDsdViewName(targetVersion)} AS V 
       {joinDataSetAttributes}
       {joinCodedDataSetAttributes}
)";
            //There is no direct FULL JOIN statement in mariadb
            if (dataSetAttributes.Any()) {
                sqlCommand += $@"

UNION ALL

(
       SELECT NULL AS SID
            ,{nullDimensionsColumns}
            ,NULL AS {measureColumn}
            {nullAttributeColumns}
            {dataSetAttributesColumns}
            {nullTimeDimensionColumns}   
            ,NULL AS {updatedAfterColumn}
       FROM {dotStatDb.DataSchema}_{dataFlow.Dsd.MariaDbDsdAttrTable(targetVersion)} AS ADF
       {joinCodedDataSetAttributes}
       WHERE ADF.DF_ID = {dataFlow.DbId}
         AND NOT EXISTS (SELECT * FROM {dotStatDb.DataSchema}_{dataFlow.Dsd.MariaDbDataDsdViewName(targetVersion)})
)
";
            }

            await dotStatDb.ExecuteNonQuerySqlAsync(sqlCommand, cancellationToken);
        }

        protected override async Task BuildDynamicDataReplaceDataFlowView(Dataflow dataFlow, char targetVersion, MariaDbDotStatDb dotStatDb, CancellationToken cancellationToken, bool sourceVersion = false) {
            //Dimensions
            var dimensions = dataFlow.Dsd.Dimensions.Where(dim => !dim.Base.TimeDimension).ToList();
            var dimensionsJoin = string.Join("\n", dimensions.Where(d => d.Base.HasCodedRepresentation()).Select(d =>
                $"LEFT JOIN {dotStatDb.ManagementSchema}_CL_{d.Codelist.DbId} AS CL_{d.Code} ON FI.{d.MariaDbColumn()} = CL_{d.Code}.ITEM_ID"
            ));

            var dimensionsSelect = string.Join(", ", dimensions.Select(d =>
                d.Base.HasCodedRepresentation() ? $"CL_{d.Code}.ID AS `{d.Code}`" : $"FI.{d.MariaDbColumn()} AS `{d.Code}`")) + ',';

            var dimensionsSelectNull = string.Join(", ", dimensions.Select(d => $"NULL AS `{d.Code}`")) + ',';
            //time dimension
            var timeDim = dataFlow.Dsd.TimeDimension;
            var timeDimSelect = string.Empty;
            var timeDimSelectNull = string.Empty;
            if (timeDim != null) {
                timeDimSelect = string.Join(", ",
                    $"FA.{timeDim.MariaDbColumn()} AS `{dataFlow.Dsd.Base.TimeDimension.Id}`",
                    MariaDbExtensions.MariaDbPeriodStart(),
                    MariaDbExtensions.MariaDbPeriodEnd()) + ',';
                timeDimSelectNull = string.Join(", ",
                    $"NULL AS `{dataFlow.Dsd.Base.TimeDimension.Id}`",
                    $"NULL AS `{MariaDbExtensions.MariaDbPeriodStart()}`",
                    $"NULL AS `{MariaDbExtensions.MariaDbPeriodEnd()}`") + ',';
            }

            //Measure
            var measureColumnSelect = $" FA.VALUE AS `{dataFlow.Dsd.Base.PrimaryMeasure.Id}`,";
            var measureColumnSelectNull = $" NULL AS `{dataFlow.Dsd.Base.PrimaryMeasure.Id}`,";

            //Attributes
            //Obs level
            var observationAttributes = dataFlow.Dsd.Attributes
                .Where(attr => attr.Base.AttachmentLevel == AttributeAttachmentLevel.Observation ||
                               (attr.Base.AttachmentLevel == AttributeAttachmentLevel.DimensionGroup || attr.Base.AttachmentLevel == AttributeAttachmentLevel.Group) &&
                               attr.Base.GetDimensionReferences().Contains(DimensionObject.TimeDimensionFixedId)).ToList();

            var obsLvlAttrSelect = string.Empty;
            var obsLvlAttrSelectNull = string.Empty;
            var obsLvlAttrJoin = string.Empty;
            if (observationAttributes.Any()) {
                obsLvlAttrSelect = string.Join(", ", observationAttributes.Select(a => a.Base.HasCodedRepresentation()
                    ? $"CL_{a.Code}.ID AS `{a.Code}`"
                    : $"FA.{a.MariaDbColumn()} AS `{a.Code}`")) + ',';

                obsLvlAttrSelectNull = string.Join(", ", observationAttributes.Select(a => $"NULL AS `{a.Code}`")) + ',';

                obsLvlAttrJoin = string.Join("\n", observationAttributes.Where(a => a.Base.HasCodedRepresentation()).Select(a =>
                    $"LEFT JOIN {dotStatDb.ManagementSchema}_CL_{a.Codelist.DbId} AS CL_{a.Code} ON FA.{a.MariaDbColumn()} = CL_{a.Code}.ITEM_ID"));
            }

            //dimGroup level
            var dimGroupAttributes = dataFlow.Dsd.Attributes.Where(attr =>
                (attr.Base.AttachmentLevel == AttributeAttachmentLevel.DimensionGroup || attr.Base.AttachmentLevel == AttributeAttachmentLevel.Group) &&
                !attr.Base.GetDimensionReferences().Contains(DimensionObject.TimeDimensionFixedId)).ToList();

            var dimGroupLvlAttrSelect = string.Empty;
            var dimGroupLvlAttrSelectNull = string.Empty;
            var dimGroupLvlAttrJoin = string.Empty;
            var hasdimGroupLevelAttributes = false;
            if (dimGroupAttributes.Any()) {
                hasdimGroupLevelAttributes = true;
                dimGroupLvlAttrSelect = string.Join(", ", dimGroupAttributes.Select(a => a.Base.HasCodedRepresentation()
                    ? $"CL_{a.Code}.ID AS `{a.Code}`"
                    : $"ATTR.{a.MariaDbColumn()} AS `{a.Code}`")) + ',';

                dimGroupLvlAttrSelectNull = string.Join(", ", dimGroupAttributes.Select(a => $"NULL AS `{a.Code}`")) + ',';

                dimGroupLvlAttrJoin = string.Join("\n", dimGroupAttributes.Where(a => a.Base.HasCodedRepresentation()).Select(a =>
                    $"LEFT JOIN {dotStatDb.ManagementSchema}_CL_{a.Codelist.DbId} AS CL_{a.Code} ON ATTR.{a.MariaDbColumn()} = CL_{a.Code}.ITEM_ID"));
            }

            //dataSet level
            var dataSetAttributes = dataFlow.Dsd.Attributes
                .Where(attr => attr.Base.AttachmentLevel == AttributeAttachmentLevel.DataSet).ToList();

            var dataSetAttrLvlSelect = string.Empty;
            var dataSetAttrLvlSelectNull = string.Empty;
            var dataSetAttrLvlJoin = string.Empty;
            var hasDataSetLevelAttributes = false;
            if (dataSetAttributes.Any()) {
                hasDataSetLevelAttributes = true;
                dataSetAttrLvlSelect = string.Join(", ", dataSetAttributes.Select(a => a.Base.HasCodedRepresentation()
                    ? $"CL_{a.Code}.ID AS `{a.Code}`"
                    : $"ADF.{a.MariaDbColumn()} AS `{a.Code}`")) + ',';

                dataSetAttrLvlSelectNull = string.Join(", ", dataSetAttributes.Select(a => $"NULL AS `{a.Code}`")) + ',';

                dataSetAttrLvlJoin = string.Join("\n", dataSetAttributes.Where(a => a.Base.HasCodedRepresentation()).Select(a =>
                    $"LEFT JOIN {dotStatDb.ManagementSchema}_CL_{a.Codelist.DbId} AS CL_{a.Code} ON ADF.DF_ID = {dataFlow.DbId} AND ADF.{a.MariaDbColumn()} = CL_{a.Code}.ITEM_ID"));
            }

            //Tables
            var filterTable = $"{dotStatDb.DataSchema}_{dataFlow.Dsd.MariaDbFilterTable("FI")}";

            var ctePart =
                $@"WITH OBS_LEVEL AS (
	SELECT FI.SID, {dimensionsSelect}{timeDimSelect}
        {measureColumnSelect}
        {obsLvlAttrSelect}
	    {dimGroupLvlAttrSelectNull}
	    {dataSetAttrLvlSelectNull}
	    {MariaDbExtensions.LAST_UPDATED_COLUMN}
	 FROM {dotStatDb.DataSchema}_{dataFlow.Dsd.MariaDbFactTable(targetVersion, "FA")}
	 LEFT JOIN {filterTable} ON FI.SID = FA.SID
	 {dimensionsJoin}
	 {obsLvlAttrJoin}
 )";

            var subQueryPart =  $@"
    SELECT * FROM OBS_LEVEL";

            if (hasdimGroupLevelAttributes) {
                ctePart += $@", SERIES_LEVEL AS (
	SELECT FI.SID, {dimensionsSelect}{timeDimSelectNull}
        {measureColumnSelectNull}
        {obsLvlAttrSelectNull}
	    {dimGroupLvlAttrSelect}
	    {dataSetAttrLvlSelectNull}
	    {MariaDbExtensions.LAST_UPDATED_COLUMN}
	 FROM {dotStatDb.DataSchema}_{dataFlow.Dsd.MariaDbDimGroupAttrTable(targetVersion, "ATTR")}
	 LEFT JOIN {filterTable} ON FI.SID = ATTR.SID
	 {dimensionsJoin}
	 {dimGroupLvlAttrJoin}
 )";

                subQueryPart = $@"   SELECT * FROM SERIES_LEVEL
UNION ALL
" + subQueryPart;
            }

            if (hasDataSetLevelAttributes) {
                ctePart += $@", DF_LEVEL AS (
	SELECT NULL AS SID, {dimensionsSelectNull}{timeDimSelectNull}
        {measureColumnSelectNull}
        {obsLvlAttrSelectNull}
	    {dimGroupLvlAttrSelectNull}
	    {dataSetAttrLvlSelect}
	    {MariaDbExtensions.LAST_UPDATED_COLUMN}
	FROM {dotStatDb.DataSchema}_{dataFlow.Dsd.MariaDbDsdAttrTable(targetVersion, "ADF")}
	{dataSetAttrLvlJoin}
	WHERE ADF.DF_ID = {dataFlow.DbId}
 )";

                subQueryPart = $@"    SELECT * FROM DF_LEVEL
UNION ALL
" + subQueryPart;
            }

            var viewDefinition = $@"CREATE ALGORITHM = UNDEFINED SQL SECURITY INVOKER VIEW 
{dotStatDb.DataSchema}_{dataFlow.MariaDbDataReplaceDataflowViewName(targetVersion)} AS
{ctePart}
{subQueryPart}
";

            await dotStatDb.ExecuteNonQuerySqlAsync(viewDefinition, cancellationToken);
        }

        protected override async Task BuildDynamicDataIncludeHistoryDataFlowView(Dataflow dataFlow, char targetVersion, MariaDbDotStatDb dotStatDb, CancellationToken cancellationToken) 
        {
            var allComponentsSelect = "SID";

            //Dimensions
            var dimensions = dataFlow.Dsd.Dimensions.Where(dim => !dim.Base.TimeDimension).ToList();

            allComponentsSelect += ", " + dimensions.ToMariaDbColumnList(externalColumn : true);
            var dimensionsJoin = string.Join("\n", dimensions.Where(d => d.Base.HasCodedRepresentation()).Select(d =>
                $"LEFT JOIN {dotStatDb.ManagementSchema}_CL_{d.Codelist.DbId} AS CL_{d.Code} ON FI.{d.MariaDbColumn()} = CL_{d.Code}.ITEM_ID"
            ));

            var dimensionsSelect = string.Join(", ", dimensions.Select(d =>
                d.Base.HasCodedRepresentation() ? $"CL_{d.Code}.ID AS `{d.Code}`" : $"FI.{d.MariaDbColumn()} AS `{d.Code}`")) + ',';

            var dimensionsSelectNull = string.Join(", ", dimensions.Select(d => $"NULL AS `{d.Code}`")) + ',';
            //time dimension
            var timeDim = dataFlow.Dsd.TimeDimension;
            var timeDimSelect = string.Empty;
            var timeDimSelectNull = string.Empty;
            if (timeDim != null)
            {
                allComponentsSelect += ", " + string.Join(", ",
                    $"`{dataFlow.Dsd.Base.TimeDimension.Id}`",
                    MariaDbExtensions.MariaDbPeriodStart(),
                    MariaDbExtensions.MariaDbPeriodEnd());

                timeDimSelect = string.Join(", ",
                    $"FA.{timeDim.MariaDbColumn()} AS `{dataFlow.Dsd.Base.TimeDimension.Id}`",
                    MariaDbExtensions.MariaDbPeriodStart(),
                    MariaDbExtensions.MariaDbPeriodEnd()) + ',';
                timeDimSelectNull = string.Join(", ",
                    $"NULL AS `{dataFlow.Dsd.Base.TimeDimension.Id}`",
                    $"NULL AS {MariaDbExtensions.MariaDbPeriodStart()}",
                    $"NULL AS {MariaDbExtensions.MariaDbPeriodEnd()}") + ',';
            }

            //Measure
            var measureColumnSelect = $" FA.VALUE AS `{dataFlow.Dsd.Base.PrimaryMeasure.Id}`,";
            var measureColumnSelectNull = $" NULL AS `{dataFlow.Dsd.Base.PrimaryMeasure.Id}`,";

            //Attributes
            //Obs level
            var observationAttributes = dataFlow.Dsd.Attributes
                .Where(attr => attr.Base.AttachmentLevel == AttributeAttachmentLevel.Observation ||
                               (attr.Base.AttachmentLevel == AttributeAttachmentLevel.DimensionGroup || attr.Base.AttachmentLevel == AttributeAttachmentLevel.Group) &&
                               attr.Base.GetDimensionReferences().Contains(DimensionObject.TimeDimensionFixedId)).ToList();

            var obsLvlAttrSelect = string.Empty;
            var obsLvlAttrSelectNull = string.Empty;
            var obsLvlAttrJoin = string.Empty;
            if (observationAttributes.Any())
            {
                obsLvlAttrSelect = string.Join(", ", observationAttributes.Select(a => a.Base.HasCodedRepresentation()
                    ? $"CL_{a.Code}.ID AS `{a.Code}`"
                    : $"FA.{a.MariaDbColumn()} AS `{a.Code}`")) + ',';

                obsLvlAttrSelectNull = string.Join(", ", observationAttributes.Select(a => $"NULL AS `{a.Code}`")) + ',';

                obsLvlAttrJoin = string.Join("\n", observationAttributes.Where(a => a.Base.HasCodedRepresentation()).Select(a =>
                    $"LEFT JOIN {dotStatDb.ManagementSchema}_CL_{a.Codelist.DbId} AS CL_{a.Code} ON FA.{a.MariaDbColumn()} = CL_{a.Code}.ITEM_ID"));
            }

            //dimGroup level
            var dimGroupAttributes = dataFlow.Dsd.Attributes.Where(attr =>
                (attr.Base.AttachmentLevel == AttributeAttachmentLevel.DimensionGroup || attr.Base.AttachmentLevel == AttributeAttachmentLevel.Group) &&
                !attr.Base.GetDimensionReferences().Contains(DimensionObject.TimeDimensionFixedId)).ToList();

            var dimGroupLvlAttrSelect = string.Empty;
            var dimGroupLvlAttrSelectNull = string.Empty;
            var dimGroupLvlAttrJoin = string.Empty;
            var hasdimGroupLevelAttributes = false;
            if (dimGroupAttributes.Any())
            {
                hasdimGroupLevelAttributes = true;
                dimGroupLvlAttrSelect = string.Join(", ", dimGroupAttributes.Select(a => a.Base.HasCodedRepresentation()
                    ? $"CL_{a.Code}.ID AS `{a.Code}`"
                    : $"ATTR.{a.MariaDbColumn()} AS `{a.Code}`")) + ',';

                dimGroupLvlAttrSelectNull = string.Join(", ", dimGroupAttributes.Select(a => $"NULL AS `{a.Code}`")) + ',';

                dimGroupLvlAttrJoin = string.Join("\n", dimGroupAttributes.Where(a => a.Base.HasCodedRepresentation()).Select(a =>
                    $"LEFT JOIN {dotStatDb.ManagementSchema}_CL_{a.Codelist.DbId} AS CL_{a.Code} ON ATTR.{a.MariaDbColumn()} = CL_{a.Code}.ITEM_ID"));
            }

            //dataSet level
            var dataSetAttributes = dataFlow.Dsd.Attributes
                .Where(attr => attr.Base.AttachmentLevel == AttributeAttachmentLevel.DataSet).ToList();

            var dataSetAttrLvlSelect = string.Empty;
            var dataSetAttrLvlSelectNull = string.Empty;
            var dataSetAttrLvlJoin = string.Empty;
            var hasDataSetLevelAttributes = false;
            if (dataSetAttributes.Any())
            {
                hasDataSetLevelAttributes = true;
                dataSetAttrLvlSelect = string.Join(", ", dataSetAttributes.Select(a => a.Base.HasCodedRepresentation()
                    ? $"CL_{a.Code}.ID AS `{a.Code}`"
                    : $"ADF.{a.MariaDbColumn()} AS `{a.Code}`")) + ',';

                dataSetAttrLvlSelectNull = string.Join(", ", dataSetAttributes.Select(a => $"NULL AS `{a.Code}`")) + ',';

                dataSetAttrLvlJoin = string.Join("\n", dataSetAttributes.Where(a => a.Base.HasCodedRepresentation()).Select(a =>
                    $"LEFT JOIN {dotStatDb.ManagementSchema}_CL_{a.Codelist.DbId} AS CL_{a.Code} ON ADF.DF_ID = {dataFlow.DbId} AND ADF.{a.MariaDbColumn()} = CL_{a.Code}.ITEM_ID"));
            }

            var allAttributesSelect = dataFlow.Dsd.Attributes.ToMariaDbColumnList(externalColumn: true);
            if (string.IsNullOrEmpty(allAttributesSelect))
                allComponentsSelect += "," + allAttributesSelect;
            allComponentsSelect += $",{dataFlow.Dsd.Base.PrimaryMeasure.Id}";
            allComponentsSelect += $",{MariaDbExtensions.LAST_UPDATED_COLUMN}, ValidTo";

            //Tables
            var filterTable = $"{dotStatDb.DataSchema}_{dataFlow.Dsd.MariaDbFilterTable("FI")}";

            var ctePart =
                $@"(
	SELECT FI.SID, {dimensionsSelect}{timeDimSelect}
        {measureColumnSelect}
        {obsLvlAttrSelect}
	    {dimGroupLvlAttrSelectNull}
	    {dataSetAttrLvlSelectNull}
		FA.ValidFrom AS {MariaDbExtensions.LAST_UPDATED_COLUMN},
        FA.ValidTo
	 FROM {dotStatDb.DataSchema}_{dataFlow.Dsd.MariaDbFactTable(targetVersion, "FA")}
	 LEFT JOIN {filterTable} ON FI.SID = FA.SID
	 {dimensionsJoin}
	 {obsLvlAttrJoin}
 )";

            var subQueryPart = $"SELECT {allComponentsSelect} FROM OBS_LEVEL";

            if (hasdimGroupLevelAttributes)
            {
                ctePart += $@" UNION ALL (
	SELECT FI.SID, {dimensionsSelect}{timeDimSelectNull}
        {measureColumnSelectNull}
        {obsLvlAttrSelectNull}
	    {dimGroupLvlAttrSelect}
	    {dataSetAttrLvlSelectNull}
		ATTR.ValidFrom AS {MariaDbExtensions.LAST_UPDATED_COLUMN},
        ATTR.ValidTo
	 FROM {dotStatDb.DataSchema}_{dataFlow.Dsd.MariaDbDimGroupAttrTable(targetVersion, "ATTR")}
	 LEFT JOIN {filterTable} ON FI.SID = ATTR.SID
	 {dimensionsJoin}
	 {dimGroupLvlAttrJoin}
 )";

                subQueryPart = $"SELECT {allComponentsSelect} FROM SERIES_LEVEL" + Environment.NewLine + "UNION ALL" + Environment.NewLine + subQueryPart;
            }

            if (hasDataSetLevelAttributes)
            {
                ctePart += $@" UNION ALL (
	SELECT NULL AS SID, {dimensionsSelectNull}{timeDimSelectNull}
        {measureColumnSelectNull}
        {obsLvlAttrSelectNull}
	    {dimGroupLvlAttrSelectNull}
	    {dataSetAttrLvlSelect}
		ADF.ValidFrom AS {MariaDbExtensions.LAST_UPDATED_COLUMN},
        ADF.ValidTo
	FROM {dotStatDb.DataSchema}_{dataFlow.Dsd.MariaDbDsdAttrTable(targetVersion, "ADF")}
	{dataSetAttrLvlJoin}
	WHERE ADF.DF_ID = {dataFlow.DbId}
 )";
                subQueryPart = $"SELECT {allComponentsSelect} FROM DF_LEVEL" + Environment.NewLine + "UNION ALL" + Environment.NewLine + subQueryPart;
            }

            var viewDefinition = $@"
CREATE ALGORITHM = UNDEFINED SQL SECURITY INVOKER VIEW {dotStatDb.DataSchema}_{dataFlow.MariaDbDataIncludeHistoryDataflowViewName(targetVersion)} AS
SELECT {allComponentsSelect} FROM (
{ctePart}
{subQueryPart}
) as T
";
            
            await dotStatDb.ExecuteNonQuerySqlAsync(viewDefinition, cancellationToken);
        }

        protected override async Task BuildDeletedView(Dataflow dataFlow, char targetVersion, MariaDbDotStatDb dotStatDb, CancellationToken cancellationToken, bool sourceVersion = false)
        {
            //The dependent table does not exists, therefore the db is corrupted or the method is called by the unit tests
            if (!await dotStatDb.TableExists($"{dotStatDb.DataSchema}_{dataFlow.Dsd.MariaDbDeletedTable(targetVersion)}", cancellationToken))
            {
                return;
            }

            var dimensions = dataFlow.Dsd.Dimensions
                .Where(dim => !dim.Base.TimeDimension)
                .ToList();

            //Time dimension columns
            var innerTimeDimensionColumns = dataFlow.Dsd.TimeDimension != null
                    ? ", " + string.Join(", ",
                            $"{MariaDbExtensions.TimeDimColumn} AS `{dataFlow.Dsd.Base.TimeDimension.Id}`",
                            MariaDbExtensions.MariaDbPeriodStart(),
                            MariaDbExtensions.MariaDbPeriodEnd()
                        )
                    : string.Empty;

            var innerTimeDimensionColumns4GroupBy = dataFlow.Dsd.TimeDimension != null
                ? ", " + string.Join(", ",
                    $"{MariaDbExtensions.TimeDimColumn}",
                    MariaDbExtensions.MariaDbPeriodStart(),
                    MariaDbExtensions.MariaDbPeriodEnd()
                )
                : string.Empty;

            var outerTimeDimensionColumns = dataFlow.Dsd.TimeDimension != null
                ? ", " + string.Join(", ",
                    $"{dataFlow.Dsd.Base.TimeDimension.Id}",
                    MariaDbExtensions.MariaDbPeriodStart(),
                    MariaDbExtensions.MariaDbPeriodEnd()
                )
                : string.Empty;

            //Dimensions
            var innerDimensionsColumns = dimensions.Any()
                        ? ", " + string.Join(", ", dimensions.Select(d => $"{d.MariaDbColumn()}"))
                        : string.Empty;

            var outerDimensionsColumns = dimensions.Any()
                ? ", " + string.Join(", ", dimensions.Select(d => d.Base.HasCodedRepresentation() ? $"CL_{d.Code}.ID AS `{d.Code}`" : $"{d.MariaDbColumn()} AS `{d.Code}`"))
                : string.Empty;


            //Attributes
            var innerAttributeColumns4SomeComp = dataFlow.Dsd.Attributes.Any()
                ? "," + string.Join(",", dataFlow.Dsd.Attributes.Select(a => $"MAX( {a.MariaDbColumn()} ) AS `{a.Code}`"))
                : string.Empty;

            var innerAttributeColumns4NoComp = dataFlow.Dsd.Attributes.Any()
                ? "," + string.Join(",", dataFlow.Dsd.Attributes.Select(a => $"NULL AS `{a.Code}`"))
                : string.Empty;


            var outerAttributeColumns = dataFlow.Dsd.Attributes.Any()
                ? "," + string.Join(",", dataFlow.Dsd.Attributes.Select(a => $" {a.Code}"))
                : string.Empty;

            var joinStatements = string.Join(Environment.NewLine, dimensions.Where(d => d.Base.HasCodedRepresentation()).Select(d =>
                    $"LEFT JOIN {dotStatDb.ManagementSchema}_CL_{d.Codelist.DbId} AS `CL_{d.Code}` ON DEL.{d.MariaDbColumn()} = CL_{d.Code}.ITEM_ID"
                )
            );


            var innerSelectPartCommon =
$@"SELECT MAX( {MariaDbExtensions.ValueColumn} ) AS `OBS_VALUE`
    {innerTimeDimensionColumns}
    {innerDimensionsColumns}
    , MAX( {MariaDbExtensions.LAST_UPDATED_COLUMN} ) AS `{MariaDbExtensions.LAST_UPDATED_COLUMN}`
";
            var innerFromPart = $@"FROM {dotStatDb.DataSchema}_{dataFlow.Dsd.MariaDbDeletedTable(targetVersion)} ";

            var innerWherePart4NoComp = $@"WHERE DF_ID = {dataFlow.DbId} " +
                                        (dataFlow.Dsd.Attributes.Any()
                                            ? Environment.NewLine + "AND " + string.Join(" AND ",
                                                dataFlow.Dsd.Attributes.Select(a => $"{a.MariaDbColumn()} IS NULL"))
                                            : string.Empty) + Environment.NewLine +
                                        " AND VALUE IS NULL";

            var innerWherePart4SomeComp = $@"WHERE DF_ID = {dataFlow.DbId} " +
                                          "AND (" +
                                          (dataFlow.Dsd.Attributes.Any()
                                              ? Environment.NewLine + string.Join(" OR ",
                                                  dataFlow.Dsd.Attributes.Select(a => $"{a.MariaDbColumn()} IS NOT NULL"))
                                              : "1 = 1 ") +
                                          " OR VALUE IS NOT NULL )";

            var innerGroupByPart = $@"GROUP BY {innerDimensionsColumns.TrimStart(',')}{innerTimeDimensionColumns4GroupBy} ";

            var innerUnionSql =
                $@"{innerSelectPartCommon}{innerAttributeColumns4SomeComp}
   {innerFromPart} 
   {innerWherePart4SomeComp}
   {innerGroupByPart}
UNION
   {innerSelectPartCommon}{innerAttributeColumns4NoComp}
   {innerFromPart}
   {innerWherePart4NoComp}
   {innerGroupByPart}";


            var sqlCommand =
                $@"CREATE ALGORITHM = UNDEFINED SQL SECURITY INVOKER VIEW {dotStatDb.DataSchema}_{dataFlow.MariaDbDeletedDataViewName(targetVersion)} 
                   AS
                       SELECT OBS_VALUE
                            {outerTimeDimensionColumns}
                            {outerDimensionsColumns}
                            {outerAttributeColumns}                            
                            ,{MariaDbExtensions.LAST_UPDATED_COLUMN}
                       FROM ( {innerUnionSql} ) AS `DEL`
                            {joinStatements}";

            await dotStatDb.ExecuteNonQuerySqlAsync(sqlCommand, cancellationToken);
        }
    }
}