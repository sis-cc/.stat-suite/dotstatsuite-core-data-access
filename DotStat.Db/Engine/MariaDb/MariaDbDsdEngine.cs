﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using DotStat.Common.Configuration.Interfaces;
using DotStat.Common.Enums;
using DotStat.Db;
using DotStat.Db.DB;
using DotStat.Db.Engine;
using DotStat.Domain;
using MySqlConnector;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Constants.InterfaceConstant;


namespace DotStat.DB.Engine.MariaDb
{
    public class MariaDbDsdEngine : DsdEngineBase<MariaDbDotStatDb>
    {
        public MariaDbDsdEngine(IGeneralConfiguration generalConfiguration) : base(generalConfiguration)
        {
        }

        public override async Task<int> GetDbId(string sdmxId, string sdmxAgency, int verMajor, int verMinor, int? verPatch, MariaDbDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            var aretefactDbId = await dotStatDb.ExecuteScalarSqlWithParamsAsync(
                $@"SELECT ART_ID 
                    FROM {dotStatDb.ManagementSchema}_ARTEFACT 
                   WHERE ID = @Id AND AGENCY = @Agency AND TYPE = @Type
                     AND VERSION_1 = @Version1 AND VERSION_2 = @Version2 
                     AND (VERSION_3 = @Version3 OR (VERSION_3 IS NULL AND @Version3 IS NULL))",
                cancellationToken,
                new MySqlParameter("Id", MySqlDbType.VarChar) { Value = sdmxId },
                new MySqlParameter("Agency", MySqlDbType.VarChar) { Value = sdmxAgency },                
                new MySqlParameter("Type", MySqlDbType.VarChar) { Value = DbTypes.GetDbType(SDMXArtefactType.Dsd) },
                new MySqlParameter("Version1", MySqlDbType.Int32) { Value = verMajor },
                new MySqlParameter("Version2", MySqlDbType.Int32) { Value = verMinor },
                new MySqlParameter("Version3", MySqlDbType.Int32) { Value = ((object)verPatch) ?? DBNull.Value }

                );

            return (int)(aretefactDbId ?? -1);
        }

        public override async Task<int> InsertToArtefactTable(Dsd dsd, MariaDbDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            return await dotStatDb.ExecuteScalarSqlLastInsertedIdWithParamsAsync(
                $@"INSERT 
                    INTO {dotStatDb.ManagementSchema}_ARTEFACT 
                       (SQL_ART_ID,TYPE,ID,AGENCY,VERSION_1,VERSION_2,VERSION_3,LAST_UPDATED,DSD_LIVE_VERSION,DSD_PIT_VERSION,DSD_PIT_RELEASE_DATE,DSD_PIT_RESTORATION_DATE,MSD_ID,DATA_COMPRESSION, KEEP_HISTORY) 
                  VALUES (NULL, @Type, @Id, @Agency, @Version1, @Version2, @Version3, @DT_Now, @LiveVersion, @PITVersion, @PITReleaseDate, @PITRestorationDate, @MsdId, @DataCompression, @KeepHistory)",
                cancellationToken,
                new MySqlParameter("Type", MySqlDbType.VarChar) { Value = DbTypes.GetDbType(SDMXArtefactType.Dsd) },
                new MySqlParameter("Id", MySqlDbType.VarChar) { Value = dsd.Code },
                new MySqlParameter("Agency", MySqlDbType.VarChar) { Value = dsd.AgencyId },
                new MySqlParameter("Version1", MySqlDbType.Int32) { Value = dsd.Version.Major },
                new MySqlParameter("Version2", MySqlDbType.Int32) { Value = dsd.Version.Minor },
                new MySqlParameter("Version3", MySqlDbType.Int32) { Value = (object)dsd.Version.Patch ?? DBNull.Value },
                new MySqlParameter("LiveVersion", MySqlDbType.String) { Value = (object)dsd.LiveVersion ?? DBNull.Value },
                new MySqlParameter("PITVersion", MySqlDbType.String) { Value = (object)dsd.PITVersion ?? DBNull.Value },
                new MySqlParameter("PITReleaseDate", MySqlDbType.DateTime) { Value = (object)dsd.PITReleaseDate ?? DBNull.Value },
                new MySqlParameter("PITRestorationDate", MySqlDbType.DateTime) { Value = (object)dsd.PITRestorationDate ?? DBNull.Value },
                new MySqlParameter("MsdId", MySqlDbType.Int32) { Value = (object)dsd.MsdDbId?? DBNull.Value },
                new MySqlParameter("DT_Now", DateTime.Now),
                new MySqlParameter("DataCompression", dsd.DataCompression.ToString()),
                new MySqlParameter("KeepHistory", dsd.KeepHistory)
                );
        }

        public override async Task DropAllIndexes(Dsd dsd, MariaDbDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            var tables = new List<string> {
                $"{dsd.MariaDbFilterTable()}",
                $"{dsd.MariaDbFactTable((char)DbTableVersion.A)}",
                $"{dsd.MariaDbFactTable((char)DbTableVersion.B)}",
                $"{dsd.MariaDbDeletedTable((char)DbTableVersion.A)}",
                $"{dsd.MariaDbDeletedTable((char)DbTableVersion.B)}"
            };

            // Drop all existing indexes
            foreach (var table in tables)
            {
                await dotStatDb.DropAllIndexesOfTable(dotStatDb.DataSchema, table, cancellationToken);
            }
        }

        public override async Task Compress(Dsd dsd, DataCompressionEnum dataCompression, MariaDbDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            if (dataCompression is DataCompressionEnum.NONE)
                return;// no compression needed

            var tables = new List<string> {
                $"{dsd.MariaDbFilterTable()}",
                $"{dsd.MariaDbFactTable((char)DbTableVersion.A)}",
                $"{dsd.MariaDbFactTable((char)DbTableVersion.B)}",
                $"{dsd.MariaDbDeletedTable((char)DbTableVersion.A)}",
                $"{dsd.MariaDbDeletedTable((char)DbTableVersion.B)}"
            };

            // Create CCI indexes
            foreach (var table in tables)
            {
                await dotStatDb.CreateColumnstoreIndex(dotStatDb.DataSchema, table, $"CCI_{table}", dataCompression, cancellationToken);
            }
        }

        public override async Task AddUniqueConstraints(Dsd dsd, bool clustered, MariaDbDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            var dimensions = dsd.Dimensions.Where(dim => !dim.Base.TimeDimension).ToList();

            //FILTER
            var sqlCommand =
                $@"CREATE UNIQUE INDEX UI_{dsd.MariaDbFilterTable()}
                    ON {dotStatDb.DataSchema}_{dsd.MariaDbFilterTable()} ({dimensions.ToMariaDbColumnList()});";

            await dotStatDb.ExecuteNonQuerySqlAsync(sqlCommand, cancellationToken);

            sqlCommand =
                $@"CREATE UNIQUE INDEX UI_SID_{dsd.MariaDbFilterTable()}
                    ON {dotStatDb.DataSchema}_{dsd.MariaDbFilterTable()} (SID);";

            await dotStatDb.ExecuteNonQuerySqlAsync(sqlCommand, cancellationToken);

            //FACT tables
            var timeDimColumns = dsd.TimeDimension is null ? "" : $",{MariaDbExtensions.MariaDbPeriodStart()},{MariaDbExtensions.MariaDbPeriodEnd()}";
            //A
            sqlCommand =
                $@"CREATE UNIQUE INDEX UI_{dsd.MariaDbFactTable((char)DbTableVersion.A)}
                    ON {dotStatDb.DataSchema}_{dsd.MariaDbFactTable((char)DbTableVersion.A)} (SID {timeDimColumns});";
            await dotStatDb.ExecuteNonQuerySqlAsync(sqlCommand, cancellationToken);
            //B
            sqlCommand =
                $@"CREATE UNIQUE INDEX UI_{dsd.MariaDbFactTable((char)DbTableVersion.B)}
                    ON {dotStatDb.DataSchema}_{dsd.MariaDbFactTable((char)DbTableVersion.B)} (SID {timeDimColumns});";
            await dotStatDb.ExecuteNonQuerySqlAsync(sqlCommand, cancellationToken);
        }

        #region protected
        protected override async Task DeleteFromArtefactTableByDbId(int dbId, MariaDbDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            await dotStatDb.ExecuteNonQuerySqlWithParamsAsync(
                $@"DELETE
                    FROM {dotStatDb.ManagementSchema}_{MariaDbExtensions.MariaDbArtefactTableName()} 
                   WHERE ART_ID = @DbId AND TYPE = 'DSD'",
                cancellationToken,
                new MySqlParameter("DbId", MySqlDbType.VarChar) { Value = dbId }
                );
        }
        
        protected override async Task BuildDynamicFactTable(Dsd dsd, char targetVersion, MariaDbDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            var attributeColumns = dsd.Attributes.ToMariaDbColumnList(new[] { AttributeAttachmentLevel.Observation }, withType: true, applyNotNull: false);

            if (!string.IsNullOrWhiteSpace(attributeColumns))
            {
                attributeColumns += ',';
            }

            var groupAttributeColumnsWithTime = dsd.Attributes
                .Where(x => (x.Base.AttachmentLevel == AttributeAttachmentLevel.DimensionGroup || x.Base.AttachmentLevel == AttributeAttachmentLevel.Group) 
                            && x.Base.GetDimensionReferences().Contains(DimensionObject.TimeDimensionFixedId))
                .ToMariaDbColumnList(withType: true, applyNotNull: false);

            if (!string.IsNullOrWhiteSpace(groupAttributeColumnsWithTime))
            {
                groupAttributeColumnsWithTime += ',';
            }

            var timeDim = dsd.TimeDimension;

            var supportsDateTime = dsd.Base.HasSupportDateTimeAnnotation();
            
            var timeDimensionColumns = timeDim != null
                ? string.Join(",",
                    new[] { timeDim }.ToMariaDbColumnList(true),
                    MariaDbExtensions.MariaDbPeriodStart(true, supportsDateTime),
                    MariaDbExtensions.MariaDbPeriodEnd(true, supportsDateTime)
                ) + ","
                : null;
            var timeConstraint =
                timeDim != null ? $", {MariaDbExtensions.MariaDbPeriodStart()} ASC, {MariaDbExtensions.MariaDbPeriodEnd()} DESC" : null;

            //SYSTEM versioning not enabled available with columnstore tables
            bool useColumnStore = (dsd.DataCompression != DataCompressionEnum.NONE && dotStatDb.UseColumnstoreIndexes);
            var temporalTableColumns = !dsd.KeepHistory || useColumnStore ? "" : ", ValidFrom timestamp(6) GENERATED ALWAYS AS ROW START NOT NULL, ValidTo timestamp(6) GENERATED ALWAYS AS ROW END NOT NULL, PERIOD FOR SYSTEM_TIME (ValidFrom, ValidTo)";
            var keepHistory = !dsd.KeepHistory || useColumnStore ? "" : $" WITH SYSTEM VERSIONING;";

            var constraint = useColumnStore ? "" : $", PRIMARY KEY PK_DSD_{dsd.DbId}_{ targetVersion} (SID{timeConstraint})";
            
            var sqlCommand = $@"CREATE TABLE {dotStatDb.DataSchema}_{dsd.MariaDbFactTable(targetVersion)}(
SID int NOT NULL,
{timeDimensionColumns}
{groupAttributeColumnsWithTime}
VALUE {dsd.PrimaryMeasure.GetMariaDbType(dsd.SupportsIntentionallyMissingValues)},
{attributeColumns}
{MariaDbExtensions.LAST_UPDATED_COLUMN} datetime(6) NOT NULL
{temporalTableColumns}
{constraint}
){(useColumnStore ? "ENGINE=ColumnStore" : keepHistory)}";

            await dotStatDb.ExecuteNonQuerySqlAsync(sqlCommand, cancellationToken);

            //TODO-MARIA NONCLUSTERED COLUMNSTORE
            //Apply data compression
            if (useColumnStore || (timeDim != null && !dsd.KeepHistory && dotStatDb.UseColumnstoreIndexes) )
            {
                await dotStatDb.ExecuteNonQuerySqlAsync(
                    $@"ALTER TABLE {dotStatDb.DataSchema}_{dsd.MariaDbFactTable(targetVersion)} ENGINE=ColumnStore", cancellationToken);
            }
        }

        protected override async Task BuildDeletedTable(Dsd dsd, char targetVersion, MariaDbDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            var dimensions = dsd.Dimensions.Where(dim => !dim.Base.TimeDimension).ToArray();
            var dimensionColumnsWithType = dimensions.ToMariaDbColumnList(true, allowNull: true);
            var attributeColumns = string.Join(",", dsd.Attributes.Select(x=>x.MariaDbColumn() + " char(1)"));

            if (!string.IsNullOrWhiteSpace(attributeColumns))
            {
                attributeColumns += ',';
            }

            var timeDim = dsd.TimeDimension;
            var supportsDateTime = dsd.Base.HasSupportDateTimeAnnotation();

            var timeDimensionColumns = timeDim != null
                ? string.Join(",",
                    new[] { timeDim }.ToMariaDbColumnList(true, allowNull: true),
                    MariaDbExtensions.MariaDbPeriodStart(true, supportsDateTime, true),
                    MariaDbExtensions.MariaDbPeriodEnd(true, supportsDateTime, true)
                ) + ","
                : null;

            var sqlCommand = $@"CREATE TABLE {dotStatDb.DataSchema}_{dsd.MariaDbDeletedTable(targetVersion)}(
DF_ID int not null,
{timeDimensionColumns}
{dimensionColumnsWithType},
{attributeColumns}
VALUE char(1),
{MariaDbExtensions.LAST_UPDATED_COLUMN} datetime(6) NOT NULL
)";

            await dotStatDb.ExecuteNonQuerySqlAsync(sqlCommand, cancellationToken);

            bool useColumnStore = (dsd.DataCompression != DataCompressionEnum.NONE && dotStatDb.UseColumnstoreIndexes);

            //Apply data compression
            if (useColumnStore) {
                await dotStatDb.ExecuteNonQuerySqlAsync(
                    $@"ALTER TABLE {dotStatDb.DataSchema}_{dsd.MariaDbDeletedTable(targetVersion)} ENGINE=ColumnStore", cancellationToken);
            }
            else
            {
                sqlCommand = $@"CREATE INDEX I_{dsd.MariaDbDeletedTable(targetVersion)} 
ON {dotStatDb.DataSchema}_{dsd.MariaDbDeletedTable(targetVersion)}(DF_ID,LAST_UPDATED)";
                await dotStatDb.ExecuteNonQuerySqlAsync(sqlCommand, cancellationToken);
            }
        }

        protected override async Task BuildDynamicFilterTable(Dsd dsd, MariaDbDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            var dimensions = dsd.Dimensions.Where(dim => !dim.Base.TimeDimension).ToArray();
            var dimensionColumnsWithType = dimensions.ToMariaDbColumnList(true);
            var dimensionColumns =  dimensions.ToMariaDbColumnList();
            var table = dsd.MariaDbFilterTable();

            string rowId = null;
            var uIndex = dimensionColumns;
            
            if (dsd.Dimensions.Count > 32)
            {
                rowId = $"ROW_ID blob AS ({dsd.MariaDbBuildRowIdFormula( false)}) PERSISTENT,";
                uIndex = "ROW_ID";
            }

            bool useColumnStore = (dsd.DataCompression != DataCompressionEnum.NONE && dotStatDb.UseColumnstoreIndexes);
            var constraint = useColumnStore ? "" : $", UNIQUE KEY U_{table} ({uIndex})";
            var pk = !useColumnStore ? ", PRIMARY KEY (SID)" : "";

            var sqlCommand = $@"CREATE TABLE {dotStatDb.DataSchema}_{table}(
                        SID int AUTO_INCREMENT NOT NULL,
                        {rowId}
	                    {dimensionColumnsWithType}
                        {pk}
                        {constraint}
                )";

            await dotStatDb.ExecuteNonQuerySqlAsync(sqlCommand, cancellationToken);

            //Apply data compression
            if (useColumnStore || (dimensions.Length <= 32 && dotStatDb.UseColumnstoreIndexes))
            {
                await dotStatDb.ExecuteNonQuerySqlAsync(
                    $@"ALTER TABLE {dotStatDb.DataSchema}_{table} ENGINE=ColumnStore", cancellationToken);
            }
        }

        protected override async Task BuildDynamicDataDsdView(Dsd dsd, char targetVersion, MariaDbDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            //The codelist table doesnt exist, therefore the db is corrupted or the method is called by the unit tests
            foreach (var dimension in dsd.Dimensions.Where(x => x.Base.HasCodedRepresentation()))
            {
                if (!await dotStatDb.TableExists($"{dotStatDb.ManagementSchema}_CL_{dimension.Codelist.DbId}", cancellationToken))
                {
                    return;
                }
            }

            var dimensions = dsd.Dimensions.Where(dim => !dim.Base.TimeDimension).ToList();

            var observationAttributes = dsd.Attributes
                .Where(attr => attr.Base.AttachmentLevel == AttributeAttachmentLevel.Observation ||
                               (attr.Base.AttachmentLevel == AttributeAttachmentLevel.DimensionGroup || attr.Base.AttachmentLevel == AttributeAttachmentLevel.Group) &&
                               attr.Base.GetDimensionReferences().Contains(DimensionObject.TimeDimensionFixedId))
                .ToArray();

            // SELECT ----------------------------------------------------------------

            var selectStatement = new StringBuilder();
            var whereStatement = new StringBuilder();

            //obs value
            selectStatement.Append($"FA.VALUE AS `{dsd.Base.PrimaryMeasure.Id}`");
            whereStatement.Append($"WHERE EXISTS (SELECT 1 FROM {dotStatDb.DataSchema}_{dsd.MariaDbFactTable(targetVersion, "FA")} WHERE FI.SID = FA.SID)");

            var timeDim = dsd.TimeDimension;

            //time dimension
            if (timeDim!=null)
            {
                selectStatement
                    .Append(",")
                    .Append(string.Join(",",
                        $"FA.{timeDim.MariaDbColumn()} AS `{dsd.Base.TimeDimension.Id}`",
                        MariaDbExtensions.MariaDbPeriodStart(),
                        MariaDbExtensions.MariaDbPeriodEnd()
                     ));
            }

            //dimensions
            selectStatement
                .Append(",")
                .Append(string.Join(", ", dimensions.Select(d =>
                d.Base.HasCodedRepresentation() ? $"CL_{d.Code}.ID AS `{d.Code}`" : $"FI.{d.MariaDbColumn()} AS `{d.Code}`"
            )));

            //obs lvl attributes
            if (observationAttributes.Any())
            {
                selectStatement
                    .Append(",")
                    .Append(string.Join(", ", observationAttributes.Select(a =>
                    a.Base.HasCodedRepresentation()
                        ? $"CL_{a.Code}.ID AS `{a.Code}`"
                        : $"FA.{a.MariaDbColumn()} AS `{a.Code}`"
                )));
            }

            // LAST_UPDATED
            selectStatement.Append($",FA.{MariaDbExtensions.LAST_UPDATED_COLUMN}");

            // JOIN ------------------------------------------------------------------

            var joinStatement = new StringBuilder();

            //dimensions
            joinStatement.AppendLine(string.Join("\n", dimensions.Where(d => d.Base.HasCodedRepresentation()).Select(d =>
                    $"LEFT JOIN {dotStatDb.ManagementSchema}_CL_{d.Codelist.DbId} AS CL_{d.Code} ON FI.{d.MariaDbColumn()} = CL_{d.Code}.ITEM_ID"
                )
            ));

            //obs lvl attributes
            joinStatement.AppendLine(string.Join("\n", observationAttributes.Where(a => a.Base.HasCodedRepresentation()).Select(a =>
                     $"LEFT JOIN {dotStatDb.ManagementSchema}_CL_{a.Codelist.DbId} AS CL_{a.Code} ON FA.{a.MariaDbColumn()} = CL_{a.Code}.ITEM_ID"
                )
            ));


            // ------------------------------------------------------
            var dimGroupAttributes = dsd.Attributes.Where(attr =>
                    (attr.Base.AttachmentLevel == AttributeAttachmentLevel.DimensionGroup || attr.Base.AttachmentLevel == AttributeAttachmentLevel.Group) &&
                    !attr.Base.GetDimensionReferences().Contains(DimensionObject.TimeDimensionFixedId))
                .ToArray();

            if (dimGroupAttributes.Any())
            {
                selectStatement.Append(",");
                selectStatement.Append(string.Join(", ", dimGroupAttributes.Select(a =>
                    a.Base.HasCodedRepresentation()
                        ? $"CL_{a.Code}.ID AS `{a.Code}`"
                        : $"ATTR.{a.MariaDbColumn()} AS `{a.Code}`"
                )));

                joinStatement.AppendLine($"LEFT JOIN {dotStatDb.DataSchema}_{dsd.MariaDbDimGroupAttrTable(targetVersion, "ATTR")} ON FI.SID = ATTR.SID ");

                joinStatement.AppendLine(string.Join("\n", dimGroupAttributes.Where(a => a.Base.HasCodedRepresentation()).Select(a =>
                        $"LEFT JOIN {dotStatDb.ManagementSchema}_CL_{a.Codelist.DbId} AS CL_{a.Code} ON ATTR.{a.MariaDbColumn()} = CL_{a.Code}.ITEM_ID"
                    )
                ));

                whereStatement.Append($" OR EXISTS (SELECT 1 FROM {dotStatDb.DataSchema}_{dsd.MariaDbDimGroupAttrTable(targetVersion, "ATTR")} WHERE FI.SID = ATTR.SID)");
            }

            // ------------------------------------------------------

            var sqlCommand =
                $@"CREATE ALGORITHM = UNDEFINED SQL SECURITY INVOKER VIEW {dotStatDb.DataSchema}_{dsd.MariaDbDataDsdViewName(targetVersion)} 
                   AS
                        SELECT FI.SID, {selectStatement}
                        FROM {dotStatDb.DataSchema}_{dsd.MariaDbFilterTable("FI")} 
                        LEFT JOIN {dotStatDb.DataSchema}_{dsd.MariaDbFactTable(targetVersion, "FA")} ON FI.SID = FA.SID 
                        {joinStatement}
                        {whereStatement}
                ";

            await dotStatDb.ExecuteNonQuerySqlAsync(sqlCommand, cancellationToken);
        }
        #endregion
    }
}
