﻿using System;
using System.Threading;
using System.Threading.Tasks;
using DotStat.Common.Configuration.Interfaces;
using DotStat.Common.Enums;
using DotStat.Db.DB;
using DotStat.Db.Engine;
using DotStat.Db.Util;
using DotStat.Domain;
using MySqlConnector;
using PrimaryMeasure = DotStat.Domain.PrimaryMeasure;

namespace DotStat.DB.Engine.MariaDb
{
    public class MariaDbPrimaryMeasureEngine : PrimaryMeasureEngineBase<MariaDbDotStatDb>
    {
        private readonly MariaDbDataTypeEnumHelper _dataTypeEnumHelper;

        public MariaDbPrimaryMeasureEngine(IGeneralConfiguration generalConfiguration, MariaDbDataTypeEnumHelper dataTypeEnumHelper) : base(generalConfiguration)
        {
            _dataTypeEnumHelper = dataTypeEnumHelper;
        }

        public override async Task<int> GetDbId(string sdmxId, int dsdId, MariaDbDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            var componentDbId = await dotStatDb.ExecuteScalarSqlWithParamsAsync($@"SELECT COMP_ID 
                FROM {dotStatDb.ManagementSchema}_COMPONENT 
               WHERE ID = @Id AND DSD_ID = @DsdId AND TYPE = @Type", 
                cancellationToken,
                new MySqlParameter("Id", MySqlDbType.VarChar) { Value = sdmxId }, 
                new MySqlParameter("DsdId", MySqlDbType.Int32) { Value = dsdId }, 
                new MySqlParameter("Type", MySqlDbType.VarChar) { Value = DbTypes.GetDbType(SDMXArtefactType.PrimaryMeasure) });

            return (int) (componentDbId ?? -1);
        }

        public override async Task<int> InsertToComponentTable(PrimaryMeasure primaryMeasure, MariaDbDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            return await dotStatDb.ExecuteScalarSqlLastInsertedIdWithParamsAsync($@"INSERT 
                    INTO {dotStatDb.ManagementSchema}_COMPONENT 
                         (ID, TYPE ,DSD_ID, CL_ID, ATT_ASS_LEVEL, ATT_STATUS, ENUM_ID, ATT_GROUP_ID, MIN_LENGTH, MAX_LENGTH, PATTERN, MIN_VALUE, MAX_VALUE)
                  VALUES (@Id, @Type, @DsdId, @ClId, NULL, NULL, @EnumId, NULL, @MinLength, @MaxLength, @Pattern, @MinValue, @MaxValue)",
                cancellationToken,
                new MySqlParameter("Id", MySqlDbType.VarChar) { Value = primaryMeasure.Code }, 
                new MySqlParameter("Type", MySqlDbType.VarChar) { Value = DbTypes.GetDbType(SDMXArtefactType.PrimaryMeasure) }, 
                new MySqlParameter("DsdId", MySqlDbType.Int32) { Value = primaryMeasure.Dsd.DbId }, 
                new MySqlParameter("ClId", MySqlDbType.Int32) { Value = primaryMeasure.Base.HasCodedRepresentation() ? (object)primaryMeasure.Codelist.DbId : DBNull.Value }, 
                new MySqlParameter("EnumId", MySqlDbType.Int64) { Value = primaryMeasure.Base.HasCodedRepresentation() ? DBNull.Value : (object)_dataTypeEnumHelper.GetIdFromTextFormat(primaryMeasure.TextFormat) }, 
                new MySqlParameter("MinLength", MySqlDbType.Int32) { Value = (object)primaryMeasure.TextFormat?.MinLength ?? DBNull.Value }, 
                new MySqlParameter("MaxLength", MySqlDbType.Int32) { Value = (object)primaryMeasure.TextFormat?.MaxLength ?? DBNull.Value }, 
                new MySqlParameter("Pattern", MySqlDbType.VarChar) { Value = (object)primaryMeasure.TextFormat?.Pattern ?? DBNull.Value }, 
                new MySqlParameter("MinValue", MySqlDbType.Int32) { Value = (object)primaryMeasure.TextFormat?.MinValue ?? DBNull.Value }, 
                new MySqlParameter("MaxValue", MySqlDbType.Int32) { Value = (object)primaryMeasure.TextFormat?.MaxValue ?? DBNull.Value });
        }

        protected override async Task DeleteFromComponentTableByDbId(int dbId, MariaDbDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            await dotStatDb.ExecuteNonQuerySqlWithParamsAsync($@"DELETE
                    FROM {dotStatDb.ManagementSchema}_COMPONENT 
                   WHERE COMP_ID = @DbId AND TYPE = @Type",
                cancellationToken, 
                new MySqlParameter("DbId", MySqlDbType.VarChar) { Value = dbId }, 
                new MySqlParameter("Type", MySqlDbType.VarChar) { Value = DbTypes.GetDbType(SDMXArtefactType.PrimaryMeasure) });
        }
    }
}
