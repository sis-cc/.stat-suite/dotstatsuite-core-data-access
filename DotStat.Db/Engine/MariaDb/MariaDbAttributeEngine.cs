﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using DotStat.Common.Configuration.Interfaces;
using DotStat.Common.Enums;
using DotStat.Db;
using DotStat.Db.DB;
using DotStat.Db.Engine;
using DotStat.Domain;
using MySqlConnector;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Constants.InterfaceConstant;
using Attribute = DotStat.Domain.Attribute;

namespace DotStat.DB.Engine.MariaDb
{
    public class MariaDbAttributeEngine : AttributeEngineBase<MariaDbDotStatDb>
    {
        public MariaDbAttributeEngine(IGeneralConfiguration generalConfiguration) : base(generalConfiguration)
        {
        }

        public override async Task<int> GetDbId(string sdmxId, int dsdId, MariaDbDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            var componentDbId = await dotStatDb.ExecuteScalarSqlWithParamsAsync(
            $@"SELECT COMP_ID 
                FROM {dotStatDb.ManagementSchema}_COMPONENT 
               WHERE ID = @Id AND DSD_ID = @DsdId AND `TYPE` = @Type",
                cancellationToken,
                new MySqlParameter("Id", MySqlDbType.VarChar) { Value = sdmxId },
                new MySqlParameter("DsdId", MySqlDbType.Int32) { Value = dsdId },                
                new MySqlParameter("Type", MySqlDbType.VarChar) { Value = DbTypes.GetDbType(SDMXArtefactType.DataAttribute) }
            );

            return (int)(componentDbId ?? -1);
        }

        public override async Task<int> InsertToComponentTable(Attribute attribute, MariaDbDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            return await dotStatDb.ExecuteScalarSqlLastInsertedIdWithParamsAsync(
                $@"INSERT 
                    INTO {dotStatDb.ManagementSchema}_COMPONENT 
                         (ID,TYPE,DSD_ID,CL_ID,ATT_ASS_LEVEL,ATT_STATUS,ENUM_ID,ATT_GROUP_ID)
                  VALUES (@Id, 'Attribute', @DsdId, @ClId, @AttachmentLevel, @Status, @EnumId, @GroupId)",
                cancellationToken,
                new MySqlParameter("Id", MySqlDbType.VarChar) {Value = attribute.Code},
                new MySqlParameter("DsdId", MySqlDbType.Int32) {Value = attribute.Dsd.DbId},
                new MySqlParameter("ClId", MySqlDbType.Int32)
                {
                    Value = attribute.Base.HasCodedRepresentation() ? (object) attribute.Codelist.DbId : DBNull.Value
                },
                new MySqlParameter("AttachmentLevel", MySqlDbType.VarChar) {Value = attribute.Base.AttachmentLevel.ToString()},
                new MySqlParameter("Status", MySqlDbType.VarChar)
                {
                    Value = (attribute.Base.Mandatory ? AttributeAssignmentStatus.Mandatory : AttributeAssignmentStatus.Conditional)
                },
                new MySqlParameter("EnumId", MySqlDbType.Int64) 
                {
                    Value = attribute.Base.HasCodedRepresentation() 
                        ? DBNull.Value 
                        : attribute.Code.Equals(MariaDbExtensions.EmbargoTime, StringComparison.InvariantCultureIgnoreCase)
                            ? (object) (10) 
                            : (object) (1)
                },
                new MySqlParameter("GroupId", MySqlDbType.VarChar)
                {
                    Value = (string.IsNullOrWhiteSpace(attribute.Base.AttachmentGroup)
                        ? DBNull.Value
                        : (object) attribute.Base.AttachmentGroup)
                }
            );
        }

        public override async Task DropAllIndexes(Dsd dsd, MariaDbDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            //Check if importReferenceStructure has dim group attribute tables
            if (dsd.Attributes
                .Where(a => a.Base.AttachmentLevel == AttributeAttachmentLevel.Group || a.Base.AttachmentLevel == AttributeAttachmentLevel.DimensionGroup)
                .Any(a => !a.Base.GetDimensionReferences().Contains(DimensionObject.TimeDimensionFixedId)))
            {
                var tables =
                    new List<string> {
                        $"{dsd.MariaDbDimGroupAttrTable((char)DbTableVersion.A)}",
                        $"{dsd.MariaDbDimGroupAttrTable((char)DbTableVersion.B)}"
                    };

                // Drop all existing indexes
                foreach (var table in tables)
                {
                    await dotStatDb.DropAllIndexesOfTable(dotStatDb.DataSchema, table, cancellationToken);
                }
            }
        }

        public override async Task Compress(Dsd dsd, DataCompressionEnum dataCompression, MariaDbDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            if (dataCompression is DataCompressionEnum.NONE)
                return;// no compression needed

            //Check if importReferenceStructure has dim group attribute tables
            if (dsd.Attributes
                .Where(a => a.Base.AttachmentLevel == AttributeAttachmentLevel.Group ||
                            a.Base.AttachmentLevel == AttributeAttachmentLevel.DimensionGroup)
                .Any(a => !a.Base.GetDimensionReferences().Contains(DimensionObject.TimeDimensionFixedId)))
            {
                var tables =
                    new List<string>
                    {
                        $"{dsd.MariaDbDimGroupAttrTable((char)DbTableVersion.A)}",
                        $"{dsd.MariaDbDimGroupAttrTable((char)DbTableVersion.B)}"
                    };

                // Create CCI indexes
                foreach (var table in tables)
                {
                    await dotStatDb.CreateColumnstoreIndex(dotStatDb.DataSchema, table, $"CCI_{table}", dataCompression,
                        cancellationToken);
                }
            }
        }

        public override async Task AddUniqueConstraints(Dsd dsd, bool clustered, MariaDbDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            var attributeColumns = dsd.Attributes
                .Where(x => !x.Base.GetDimensionReferences().Contains(DimensionObject.TimeDimensionFixedId))
                .ToColumnList(
                    new[]
                    {
                        AttributeAttachmentLevel.DimensionGroup,
                        AttributeAttachmentLevel.Group
                    },
                    withType: true,
                    applyNotNull: false
                );

            if (string.IsNullOrWhiteSpace(attributeColumns))
                return;

            //DimGroupAttrTable tables
            //A
            var sqlCommand =
                $@"CREATE UNIQUE INDEX UI_{dsd.MariaDbDimGroupAttrTable((char)DbTableVersion.A)}
                    ON {dotStatDb.DataSchema}_{dsd.MariaDbDimGroupAttrTable((char)DbTableVersion.A)} (SID);
                ";
            await dotStatDb.ExecuteNonQuerySqlAsync(sqlCommand, cancellationToken);
            //B
            sqlCommand =
                $@"CREATE UNIQUE INDEX UI_{dsd.MariaDbDimGroupAttrTable((char)DbTableVersion.B)}
                    ON {dotStatDb.DataSchema}_{dsd.MariaDbDimGroupAttrTable((char)DbTableVersion.B)} (SID);
                ";
            await dotStatDb.ExecuteNonQuerySqlAsync(sqlCommand, cancellationToken);
        }
        

        #region protected 
        protected override async Task DeleteFromComponentTableByDbId(int dbId, MariaDbDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            await dotStatDb.ExecuteNonQuerySqlWithParamsAsync(
                $@"DELETE
                    FROM {dotStatDb.ManagementSchema}_COMPONENT 
                   WHERE COMP_ID = @DbId AND `TYPE` = 'Attribute'",
                    cancellationToken,
                    new MySqlParameter("DbId", MySqlDbType.VarChar) { Value = dbId }
                    );
        }

        protected override async Task BuildDynamicDatasetAttributeTable(Dsd dsd, char tableVersion, MariaDbDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            var attributeColumns = dsd.Attributes.ToMariaDbColumnList(new[] {AttributeAttachmentLevel.DataSet}, withType: true, applyNotNull: false);

            var temporalTableColumns = !dsd.KeepHistory ? "" : ", ValidFrom timestamp(6) GENERATED ALWAYS AS ROW START NOT NULL, ValidTo timestamp(6) GENERATED ALWAYS AS ROW END NOT NULL, PERIOD FOR SYSTEM_TIME (ValidFrom, ValidTo)";
            var keepHistory = !dsd.KeepHistory ? "" : $" WITH SYSTEM VERSIONING;";


            if (!string.IsNullOrWhiteSpace(attributeColumns))
            {
                await dotStatDb.ExecuteNonQuerySqlAsync(
                    $@"CREATE TABLE {dotStatDb.DataSchema}_{dsd.MariaDbDsdAttrTable(tableVersion)}( 
                        DF_ID int NOT NULL PRIMARY KEY, 
                        {attributeColumns},
                        {MariaDbExtensions.LAST_UPDATED_COLUMN} datetime(6) NOT NULL
                        {temporalTableColumns}
                        ) {keepHistory}",
                    cancellationToken
                );
            }
        }

        protected override async Task BuildDynamicDimensionGroupAttributeTable(Dsd dsd, char targetVersion, MariaDbDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            var attributeColumns = dsd.Attributes
                .Where(x => !x.Base.GetDimensionReferences().Contains(DimensionObject.TimeDimensionFixedId))
                .ToMariaDbColumnList( 
                    new[]
                    {
                        AttributeAttachmentLevel.DimensionGroup, 
                        AttributeAttachmentLevel.Group
                    }, 
                    withType: true, 
                    applyNotNull: false
                );

            if (string.IsNullOrWhiteSpace(attributeColumns))
                return;

            var indexName = dsd.MariaDbDimGroupAttrTable(targetVersion);

            var temporalTableColumns = !dsd.KeepHistory ? "" : ", ValidFrom timestamp(6) GENERATED ALWAYS AS ROW START NOT NULL, ValidTo timestamp(6) GENERATED ALWAYS AS ROW END NOT NULL, PERIOD FOR SYSTEM_TIME (ValidFrom, ValidTo)";
            var keepHistory = !dsd.KeepHistory ? "" : $"WITH SYSTEM VERSIONING;";
            bool useColumnStore = (dsd.DataCompression != DataCompressionEnum.NONE && dotStatDb.UseColumnstoreIndexes);

            var constraint = !useColumnStore ? $", CONSTRAINT PK_{indexName} PRIMARY KEY (SID)" : "";
            await dotStatDb.ExecuteNonQuerySqlAsync($@"CREATE TABLE {dotStatDb.DataSchema}_{dsd.MariaDbDimGroupAttrTable(targetVersion)} (
                SID int not null,
                {attributeColumns},
                {DbExtensions.LAST_UPDATED_COLUMN} datetime(6) NOT NULL
                {temporalTableColumns}
                {constraint}                
            ) {(useColumnStore ? "ENGINE=ColumnStore" : null)}
            {keepHistory}", cancellationToken);

            //TODO-MARIA
//            //Apply data compression
//            if (dsd.DataCompression != DataCompressionEnum.NONE)
//            {
//                await dotStatDb.ExecuteNonQuerySqlAsync(
//                    $@"CREATE CLUSTERED COLUMNSTORE INDEX CCI_{indexName}
//ON [{dotStatDb.DataSchema}].[{dsd.MariaDbDimGroupAttrTable(targetVersion)}]
//WITH(DATA_COMPRESSION = {dsd.DataCompression})", cancellationToken);

//                if (dsd.DataCompression == DataCompressionEnum.COLUMNSTORE)
//                {
//                    await AddUniqueConstraints(dsd, dotStatDb, cancellationToken);
//                }
//            }
        }

        protected override async Task AlterTextAttributeColumnsInDatasetAttributeTable(Attribute attribute, char targetVersion, MariaDbDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            await dotStatDb.ExecuteNonQuerySqlAsync(
                $@"ALTER TABLE {dotStatDb.DataSchema}_ATTR_{attribute.Dsd.DbId}_{targetVersion.ToString()}_DF ALTER COLUMN {attribute.MariaDbColumn()} {attribute.GetMariaDbType(true, false)}",
                cancellationToken);
        }

        protected override async Task AlterTextAttributeColumnsInDimensionGroupAttributeTable(Attribute attribute, char targetVersion, MariaDbDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            await dotStatDb.ExecuteNonQuerySqlAsync(
                $@"ALTER TABLE {dotStatDb.DataSchema}_{attribute.Dsd.MariaDbDimGroupAttrTable(targetVersion)} ALTER COLUMN {attribute.MariaDbColumn()} {attribute.GetMariaDbType(true, false)}",
                cancellationToken);
        }

        protected override async Task AlterTextAttributeColumnsInFactTable(Attribute attribute, char targetVersion, MariaDbDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            await dotStatDb.ExecuteNonQuerySqlAsync(
                $"ALTER TABLE {dotStatDb.DataSchema}_FACT_{attribute.Dsd.DbId}_{targetVersion} MODIFY COLUMN {attribute.MariaDbColumn()} {attribute.GetMariaDbType(true, false)}",
                cancellationToken);
        }

        #endregion
    }
}
