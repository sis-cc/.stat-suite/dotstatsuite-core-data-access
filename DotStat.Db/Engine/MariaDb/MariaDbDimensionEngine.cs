﻿using DotStat.Common.Configuration.Interfaces;
using DotStat.Common.Enums;
using DotStat.Db.DB;
using DotStat.Db.Engine;
using DotStat.Db.Util;
using DotStat.Domain;
using MySqlConnector;
using System;
using System.Threading;
using System.Threading.Tasks;


namespace DotStat.DB.Engine.MariaDb
{
    public class MariaDbDimensionEngine : DimensionEngineBase<MariaDbDotStatDb>
    {
        private readonly DataTypeEnumHelper _dataTypeEnumHelper;

        public MariaDbDimensionEngine(IGeneralConfiguration generalConfiguration, DataTypeEnumHelper dataTypeEnumHelper) : base(generalConfiguration)
        {
            _dataTypeEnumHelper = dataTypeEnumHelper;
        }

        public override async Task<int> GetDbId(string sdmxId, int dsdId, MariaDbDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            var componentDbId = await dotStatDb.ExecuteScalarSqlWithParamsAsync(
                $@"SELECT COMP_ID
                FROM {dotStatDb.ManagementSchema}_COMPONENT
                WHERE ID = @Id AND DSD_ID = @DsdId AND `TYPE` IN (@DimensionType, @TimeDimensionType)",
                cancellationToken,
                new MySqlParameter("Id", MySqlDbType.VarChar) {Value = sdmxId},
                new MySqlParameter("DsdId", MySqlDbType.Int32) {Value = dsdId},
                new MySqlParameter("DimensionType", MySqlDbType.VarChar) {Value = DbTypes.GetDbType(SDMXArtefactType.Dimension)},
                new MySqlParameter("TimeDimensionType", MySqlDbType.VarChar) {Value = DbTypes.GetDbType(SDMXArtefactType.TimeDimension)}
            );

            return (int)(componentDbId ?? -1);
        }

        public override async Task<int> InsertToComponentTable(Dimension dimension, MariaDbDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            return dimension.Base.TimeDimension
                ? await dotStatDb.ExecuteScalarSqlLastInsertedIdWithParamsAsync(
                    $@"INSERT INTO {dotStatDb.ManagementSchema}_COMPONENT 
                (ID,`TYPE`,DSD_ID,CL_ID,ATT_ASS_LEVEL,ATT_STATUS,ENUM_ID)
                VALUES (@Id, @Type, @DsdId, NULL, NULL, NULL, NULL)",
                    cancellationToken,
                    new MySqlParameter("Id", MySqlDbType.VarChar) {Value = dimension.Code},
                    new MySqlParameter("Type", MySqlDbType.VarChar) {Value = dimension.DbType},
                    new MySqlParameter("DsdId", MySqlDbType.Int32) {Value = dimension.Dsd.DbId}
                )
                : await dotStatDb.ExecuteScalarSqlLastInsertedIdWithParamsAsync(
                    $@"INSERT INTO {dotStatDb.ManagementSchema}_COMPONENT 
                (ID,TYPE,DSD_ID,CL_ID,ATT_ASS_LEVEL,ATT_STATUS,ENUM_ID)
                VALUES (@Id, @Type, @DsdId, @ClId, NULL, NULL, @EnumId)",
                    cancellationToken,
                    new MySqlParameter("Id", MySqlDbType.VarChar) {Value = dimension.Code},
                    new MySqlParameter("Type", MySqlDbType.VarChar) {Value = dimension.DbType},
                    new MySqlParameter("DsdId", MySqlDbType.Int32) {Value = dimension.Dsd.DbId},
                    new MySqlParameter("ClId", MySqlDbType.Int32) {Value = dimension.Codelist != null ? dimension.Codelist.DbId : DBNull.Value },
                    new MySqlParameter("EnumId", MySqlDbType.Int64) { Value = dimension.Base.HasCodedRepresentation() ? DBNull.Value : (object)_dataTypeEnumHelper.GetIdFromTextFormat(dimension.Base.Representation?.TextFormat) }
                );
            
        }

        protected override async Task DeleteFromComponentTableByDbId(int dbId, MariaDbDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            await dotStatDb.ExecuteNonQuerySqlWithParamsAsync(
                $@"DELETE
                FROM {dotStatDb.ManagementSchema}_COMPONENT
                WHERE COMP_ID = @DbId",
                cancellationToken,
                new MySqlParameter("DbId", MySqlDbType.VarChar) {Value = dbId}
            );
        }
    }
}
