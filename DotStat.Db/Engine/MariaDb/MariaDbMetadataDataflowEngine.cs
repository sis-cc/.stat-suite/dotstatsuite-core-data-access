﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using DotStat.Common.Configuration.Interfaces;
using DotStat.Db;
using DotStat.Db.DB;
using DotStat.Db.Engine;
using DotStat.Db.Util;
using DotStat.Domain;

namespace DotStat.DB.Engine.MariaDb
{
    public class MariaDbMetadataDataflowEngine : MetadataDataflowEngineBase<MariaDbDotStatDb>
    {

        public MariaDbMetadataDataflowEngine(IGeneralConfiguration generalConfiguration) : base(generalConfiguration)
        {
        }

        public override Task<int> GetDbId(string sdmxId, string sdmxAgency, int verMajor, int verMinor, int? verPatch, MariaDbDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        public override Task<int> InsertToArtefactTable(Dataflow artefact, MariaDbDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        public override async Task DropAllIndexes(Dataflow dataFlow, MariaDbDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            if (dataFlow.Dsd.Msd is null) return;

            var tables = new List<string> {
                $"{dataFlow.MariaDbMetadataDataflowTable((char)DbTableVersion.A)}",
                $"{dataFlow.MariaDbMetadataDataflowTable((char)DbTableVersion.B)}"
            };

            // Drop all existing indexes
            foreach (var table in tables)
            {
                await dotStatDb.DropAllIndexesOfTable(dotStatDb.DataSchema, table, cancellationToken);
            }
        }

        public override async Task Compress(Dataflow dataFlow, DataCompressionEnum dataCompression, MariaDbDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            if (dataCompression is DataCompressionEnum.NONE || !dotStatDb.UseColumnstoreIndexes)  //TODO-MARIA - columnstore index creation may not be possible this way in MariaDb
                return;// no compression needed

            if (dataFlow.Dsd.Msd is null) return;

            var tables = new List<string> {
                $"{dataFlow.MariaDbMetadataDataflowTable((char)DbTableVersion.A)}",
                $"{dataFlow.MariaDbMetadataDataflowTable((char)DbTableVersion.B)}"
            };

            // Create CCI indexes
            foreach (var table in tables)
            {
                await dotStatDb.CreateColumnstoreIndex(dotStatDb.DataSchema, table, $"CCI_{table}", dataCompression, cancellationToken);
            }
        }

        public override async Task AddUniqueConstraints(Dataflow dataFlow, bool clustered, MariaDbDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            if (dataFlow.Dsd.Msd is null) return;

            var dimensions = dataFlow.Dsd.Dimensions.Where(dim => !dim.Base.TimeDimension).ToList();
            var timeDimColumns = dataFlow.Dsd.TimeDimension is null ? "" : $",{MariaDbExtensions.MariaDbPeriodStart()},{MariaDbExtensions.MariaDbPeriodEnd()}";
            var uniqueColumns = dimensions.ToMariaDbColumnList() + timeDimColumns;
            //A
            var sqlCommand =
                $@"CREATE UNIQUE INDEX UI_{dataFlow.MariaDbMetadataDataflowTable((char)DbTableVersion.A)}
                    ON {dotStatDb.DataSchema}_{dataFlow.MariaDbMetadataDataflowTable((char)DbTableVersion.A)} ({uniqueColumns});
                ";
            await dotStatDb.ExecuteNonQuerySqlAsync(sqlCommand, cancellationToken);
            //B
            sqlCommand =
                $@"CREATE UNIQUE INDEX UI_{dataFlow.MariaDbMetadataDataflowTable((char)DbTableVersion.B)}
                    ON {dotStatDb.DataSchema}_{dataFlow.MariaDbMetadataDataflowTable((char)DbTableVersion.B)} ({uniqueColumns});
                ";
            await dotStatDb.ExecuteNonQuerySqlAsync(sqlCommand, cancellationToken);
        }

        protected override async Task BuildDynamicMetadataDataFlowTable(Dataflow dataflow, char targetVersion, MariaDbDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            //The codelist table doesnt exist, therefore the db is corrupted or the method is called by the unit tests
            foreach (var dimension in dataflow.Dsd.Dimensions.Where(x => x.Base.HasCodedRepresentation()))
            {
                if (!await dotStatDb.TableExists($"{dotStatDb.ManagementSchema}_CL_{dimension.Codelist.DbId}", cancellationToken))
                {
                    return;
                }
            }

            //The table already exists
            if (await dotStatDb.TableExists($"{dotStatDb.DataSchema}_{dataflow.MariaDbMetadataDataflowTable(targetVersion)}", cancellationToken))
            {
                return;
            }

            var dimensions = dataflow.Dsd.Dimensions.Where(dim => !dim.Base.TimeDimension).ToArray();
            //Change to allowNull=true when non coded dimensions are introduced. 
            //A value of 0 should be given when the dimension is not being referenced
            var dimensionColumnsWithType = dimensions.ToMariaDbColumnList(true);
            var dimensionColumnsIndex = dimensions.ToMariaDbColumnList(forIndex: true);
            var dimensionColumns = dimensions.ToMariaDbColumnList();
            var table = dataflow.MariaDbMetadataDataflowTable(targetVersion);

            var timeDim = dataflow.Dsd.TimeDimension;
            var supportsDateTime = dataflow.Dsd.Base.HasSupportDateTimeAnnotation();

            var timeDimensionColumns = timeDim != null
                ? string.Join(",",
                    // NULL is stored when time is not referenced by the metadata.
                    new[] { timeDim }.ToMariaDbColumnList(true, true),
                    //The maximum SQL data/datetime is stored when time is not referenced by the metadata.
                    MariaDbExtensions.MariaDbPeriodStart(true, supportsDateTime, false),
                    //The minimum SQL data/datetime is stored when time is not referenced by the metadata.
                    MariaDbExtensions.MariaDbPeriodEnd(true, supportsDateTime, false)
                ) + ","
                : null;

            var timeConstraint =
                timeDim != null ? $", {MariaDbExtensions.MariaDbPeriodStart()}, {MariaDbExtensions.MariaDbPeriodEnd()} DESC" : null;

            string rowId = null;
            var uIndex = dimensionColumnsIndex + timeConstraint;

            //Time dimension requires two columns (period_start and period_end)
            if (dataflow.Dsd.Dimensions.Count > (timeDim != null ? 30 : 32))
            {
                rowId = $"ROW_ID blob AS ({dataflow.Dsd.MariaDbBuildRowIdFormula()}) PERSISTENT,";
                uIndex = "ROW_ID";
            }

            var metaAttributeColumns = dataflow.Dsd.Msd.MetadataAttributes
                .ToMariaDbColumnList(true);

            if (!string.IsNullOrWhiteSpace(metaAttributeColumns))
            {
                metaAttributeColumns += ',';
            }

            var constraint = dataflow.Dsd.DataCompression != DataCompressionEnum.NONE ? "" : $", CONSTRAINT PK_{table} PRIMARY KEY CLUSTERED ({uIndex})";

            var sqlCommand = $@"CREATE TABLE {dotStatDb.DataSchema}_{table}(
                        {rowId}
	                    {dimensionColumnsWithType},
                        {timeDimensionColumns}
                        {metaAttributeColumns}
                        {MariaDbExtensions.LAST_UPDATED_COLUMN} datetime NOT NULL
                        {constraint}
                )";

            await dotStatDb.ExecuteNonQuerySqlAsync(sqlCommand, cancellationToken);

            //Apply data compression
            if (dotStatDb.UseColumnstoreIndexes && dataflow.Dsd.DataCompression != DataCompressionEnum.NONE)
            {
                await dotStatDb.ExecuteNonQuerySqlAsync(
                    $@"CREATE CLUSTERED COLUMNSTORE INDEX CCI_{table}
ON {dotStatDb.DataSchema}_{table}
WITH(DATA_COMPRESSION = {dataflow.Dsd.DataCompression})", cancellationToken);

                if (dataflow.Dsd.DataCompression == DataCompressionEnum.COLUMNSTORE)
                {
                    await AddUniqueConstraints(dataflow, false, dotStatDb, cancellationToken);
                }
            }

        }

        public override async Task<bool> BuildDynamicMetadataDataFlowView(Dataflow dataFlow, char targetVersion, MariaDbDotStatDb dotStatDb, bool sourceVersion, ICodeTranslator codeTranslator, CancellationToken cancellationToken)
        {
            //The dependent table does not exists, therefore the db is corrupted or the method is called by the unit tests
            if (!await dotStatDb.TableExists($"{dotStatDb.DataSchema}_{dataFlow.MariaDbMetadataDataflowTable(targetVersion)}", cancellationToken))
            {
                return false;
            }

            //The dependent table does not exists, therefore the db is corrupted or the method is called by the unit tests
            if (!await dotStatDb.TableExists($"{dotStatDb.DataSchema}_{dataFlow.Dsd.MariaDbMetadataDataSetTable(targetVersion)}", cancellationToken))
            {
                return false;
            }

            //The view already exists, therefore no need to recreate it.
            if (await dotStatDb.ViewExists($"{dotStatDb.DataSchema}_{dataFlow.MariaDbMetadataDataflowViewName(targetVersion)}", cancellationToken))
                return false;

            var dimensions = dataFlow.Dsd.Dimensions.Where(dim => !dim.Base.TimeDimension).ToList();

            // SELECT ----------------------------------------------------------------

            var selectStatement = new StringBuilder();
            var dfSelectStatement = new StringBuilder();
            var dsSelectStatement = new StringBuilder();
            var joinDsdDf = new StringBuilder();

            //dimensions
            selectStatement.Append(
                string.Join(", ", dimensions.Select(d => d.MariaDbColumn(externalColumn: true))));

            dfSelectStatement.Append(string.Join(", ", dimensions.Select(d =>
                    d.Base.HasCodedRepresentation() ?
                        $"CASE WHEN `CL_{d.Code}`.`ID` IS NULL THEN '{MariaDbExtensions.DimensionSwitchedOff}' ELSE `CL_{d.Code}`.`ID` END AS `{d.Code}`"
                        : $"CASE WHEN `ME`.{d.MariaDbColumn()} IS NULL THEN '{MariaDbExtensions.DimensionSwitchedOff}' ELSE `ME`.{d.MariaDbColumn()} END AS `{d.Code}`")));

            dsSelectStatement.Append(string.Join(", ", dimensions.Select(d => $"'{MariaDbExtensions.DimensionSwitchedOff}' AS `{d.Code}`")));
            joinDsdDf.Append(string.Join(" AND ", dimensions.Select(d => $"`DF`.`{d.Code}` = `DSD`.`{d.Code}`")));

            //time dimension
            var timeDim = dataFlow.Dsd.TimeDimension;
            var (sqlMin, sqlMax) = dataFlow.Dsd.Base.HasSupportDateTimeAnnotation()
                ? (DateTime.MinValue.ToString("yyyy-MM-dd HH:mm:ss"), DateTime.MaxValue.ToString("yyyy-MM-dd HH:mm:ss"))
                : (DateTime.MinValue.ToString("yyyy-MM-dd"), DateTime.MaxValue.ToString("yyyy-MM-dd"));

            if (timeDim != null)
            {
                selectStatement
                    .Append(", ")
                    .Append(string.Join(", ",
                        @$"{dataFlow.Dsd.Base.TimeDimension.Id},
                        {MariaDbExtensions.MariaDbPeriodStart()},
                        {MariaDbExtensions.MariaDbPeriodEnd()}"
                    ));

                dfSelectStatement
                    .Append(",")
                .Append(string.Join(",",
                        $"CASE WHEN {timeDim.MariaDbColumn()} IS NULL THEN '{MariaDbExtensions.DimensionSwitchedOff}' ELSE {timeDim.MariaDbColumn()} END AS `{dataFlow.Dsd.Base.TimeDimension.Id}`",
                        MariaDbExtensions.MariaDbPeriodStart(),
                        MariaDbExtensions.MariaDbPeriodEnd()
                    ));

                dsSelectStatement
                    .Append(", ")
                    .Append(string.Join(", ",
                        $"'{MariaDbExtensions.DimensionSwitchedOff}' AS `{dataFlow.Dsd.Base.TimeDimension.Id}`",
                        $"'{sqlMax}' AS `{MariaDbExtensions.MariaDbPeriodStart()}`",
                        $"'{sqlMin}' AS {MariaDbExtensions.MariaDbPeriodEnd()}"
                    ));

                joinDsdDf.Append(
                    $" AND DF.`{dataFlow.Dsd.Base.TimeDimension.Id}` = DSD.`{dataFlow.Dsd.Base.TimeDimension.Id}`");
            }

            //metadata attributes
            if (dataFlow.Dsd.Msd.MetadataAttributes.Any())
            {
                selectStatement
                    .Append(",")
                    .Append(string.Join(", ", dataFlow.Dsd.Msd.MetadataAttributes.Select(a => $"`{a.HierarchicalId}`"
                )));

                dfSelectStatement
                    .Append(",")
                    .Append(string.Join(", ", dataFlow.Dsd.Msd.MetadataAttributes.Select(a => $"{a.MariaDbColumn()} AS `{a.HierarchicalId}`"
                    )));

                dsSelectStatement
                    .Append(',')
                    .Append(string.Join(", ", dataFlow.Dsd.Msd.MetadataAttributes.Select(a => $"ME.{a.MariaDbColumn()} AS `{a.HierarchicalId}`"
                )));
            }

            // LAST_UPDATED
            var updatedAfterColumns = $", `{MariaDbExtensions.LAST_UPDATED_COLUMN}`";

            // JOIN ------------------------------------------------------------------
            var joinStatement = new StringBuilder();

            //dimensions
            joinStatement.AppendLine(string.Join("\n", dimensions.Where(d => d.Base.HasCodedRepresentation()).Select(d =>
                    $"LEFT JOIN {dotStatDb.ManagementSchema}_CL_{d.Codelist.DbId} AS `CL_{d.Code}` ON ME.{d.MariaDbColumn()} = CL_{d.Code}.ITEM_ID"
                )
            ));

            var sqlCommand =
                $@"CREATE ALGORITHM = UNDEFINED SQL SECURITY INVOKER VIEW {dotStatDb.DataSchema}_{dataFlow.MariaDbMetadataDataflowViewName(targetVersion)} 
                   AS 
WITH `DF_LEVEL` AS ( 
    SELECT {dsSelectStatement}{updatedAfterColumns}
    FROM {dotStatDb.DataSchema}_{dataFlow.Dsd.SqlMetadataDataSetTable(targetVersion)} ME
    WHERE DF_ID = {dataFlow.DbId}
    UNION ALL 
    SELECT {dfSelectStatement}{updatedAfterColumns}
    FROM {dotStatDb.DataSchema}_{dataFlow.SqlMetadataDataflowTable(targetVersion)} ME
    {joinStatement}
)
(
                    SELECT {selectStatement}{updatedAfterColumns} FROM DF_LEVEL DF
 
                    UNION ALL

                    SELECT {selectStatement}{updatedAfterColumns} FROM {dotStatDb.DataSchema}_{dataFlow.Dsd.MariaDbMetaDataDsdViewName(targetVersion)} DSD
                    WHERE NOT EXISTS ( 
		                               SELECT * FROM DF_LEVEL DF 
                                        WHERE {joinDsdDf}
                    )
) 
";

            await dotStatDb.ExecuteNonQuerySqlAsync(sqlCommand, cancellationToken);
            return true;
        }

        protected override Task DeleteFromArtefactTableByDbId(int dbId, MariaDbDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        public override async Task<bool> BuildDeletedView(Dataflow dataFlow, char targetVersion, MariaDbDotStatDb dotStatDb, bool sourceVersion, CancellationToken cancellationToken)
        {
            //The dependent table does not exists, therefore the db is corrupted or the method is called by the unit tests
            if (!await dotStatDb.TableExists($"{dotStatDb.DataSchema}_{dataFlow.Dsd.MariaDbDeletedMetadataTable(targetVersion)}", cancellationToken))
            {
                return false;
            }

            //The view already exists, therefore no need to recreate it.
            if (await dotStatDb.ViewExists($"{dotStatDb.DataSchema}_{dataFlow.MariaDbDeletedMetadataDataflowViewName(targetVersion)}", cancellationToken))
                return false;

            //Dimensions
            var dimensions = dataFlow.Dsd.Dimensions
                .Where(dim => !dim.Base.TimeDimension)
                .ToList();

            //Time dimension columns
            var innerTimeDimensionColumns = dataFlow.Dsd.TimeDimension != null
                    ? ", " + string.Join(", ",
                            $"{MariaDbExtensions.TimeDimColumn}",
                            MariaDbExtensions.MariaDbPeriodStart(),
                            MariaDbExtensions.MariaDbPeriodEnd()
                        )
                    : string.Empty;

            var innerTimeDimensionColumns4GroupBy = innerTimeDimensionColumns;

            var (sqlMax, sqlMaxMinOne) = dataFlow.Dsd.Base.HasSupportDateTimeAnnotation()
                ? (DateTime.MaxValue.ToString("yyyy-MM-dd HH:mm:ss"), DateTime.MaxValue.AddYears(-1).ToString("yyyy-MM-dd HH:mm:ss"))
                : (DateTime.MaxValue.ToString("yyyy-MM-dd"), DateTime.MaxValue.AddYears(-1).ToString("yyyy-MM-dd"));

            var outerTimeDimensionColumns = dataFlow.Dsd.TimeDimension != null
                ? ", " + string.Join(", ",
                    @$"CASE 
                            WHEN DEL.{MariaDbExtensions.MariaDbPeriodStart()} = '{sqlMax}' THEN '{MariaDbExtensions.DimensionSwitchedOff}'
                            WHEN DEL.{MariaDbExtensions.MariaDbPeriodStart()} = '{sqlMaxMinOne}' THEN '{MariaDbExtensions.DimensionWildCarded}'
                            ELSE DEL.{MariaDbExtensions.TimeDimColumn}
                      END AS `{dataFlow.Dsd.Base.TimeDimension.Id}`",
                    $"{MariaDbExtensions.MariaDbPeriodStart()}",
                    $"{MariaDbExtensions.MariaDbPeriodEnd()}")
                : string.Empty;

            //Dimensions
            var innerDimensionsColumns = dimensions.Any()
                        ? ", " + string.Join(", ", dimensions.Select(d => $"{d.MariaDbColumn()}"))
                        : string.Empty;

            var outerDimensionsColumns = dimensions.Any()
                ? ", " +
                  string.Join(
                      ", ",
                      dimensions.Select(d =>
                          (d.Base.HasCodedRepresentation()
                              ? @$"CASE
                            WHEN DEL.{d.MariaDbColumn()} = {MariaDbExtensions.DimensionSwitchedOffDbValue} THEN '{MariaDbExtensions.DimensionSwitchedOff}'
                            WHEN DEL.{d.MariaDbColumn()} = {MariaDbExtensions.DimensionWildCardedDbValue} THEN '{MariaDbExtensions.DimensionWildCarded}'
                            ELSE CL_{d.Code}.ID
                        END"
                              : $"{d.MariaDbColumn()}") + $" AS `{d.Code}`"
                      )
                  )
                : string.Empty;

            //Attributes
            var innerAttributeColumns4SomeComp = dataFlow.Dsd.Msd.MetadataAttributes.Any()
                ? "," + string.Join(",", dataFlow.Dsd.Msd.MetadataAttributes.Select(a => $"MAX( {a.MariaDbColumn()} ) AS `{a.HierarchicalId}`"))
                : string.Empty;

            var innerAttributeColumns4NoComp = dataFlow.Dsd.Msd.MetadataAttributes.Any()
                ? "," + string.Join(",", dataFlow.Dsd.Msd.MetadataAttributes.Select(a => $"NULL AS `{a.HierarchicalId}`"))
                : string.Empty;

            var outerAttributeColumns = dataFlow.Dsd.Msd.MetadataAttributes.Any()
                ? "," + string.Join(",", dataFlow.Dsd.Msd.MetadataAttributes.Select(a => $" `{a.HierarchicalId}`"))
                : string.Empty;

            var joinStatements = string.Join(Environment.NewLine, dimensions.Where(d => d.Base.HasCodedRepresentation()).Select(d =>
                    $"LEFT JOIN {dotStatDb.ManagementSchema}_CL_{d.Codelist.DbId} AS `CL_{d.Code}` ON DEL.{d.MariaDbColumn()} = CL_{d.Code}.ITEM_ID"
                )
            );

            var innerSelectPartCommon =
$@"SELECT MAX( {MariaDbExtensions.LAST_UPDATED_COLUMN} ) AS {MariaDbExtensions.LAST_UPDATED_COLUMN}
    {innerTimeDimensionColumns}
    {innerDimensionsColumns}";

            var innerFromPart = $@"FROM {dotStatDb.DataSchema}_{dataFlow.Dsd.MariaDbDeletedMetadataTable(targetVersion)} ";

            var innerWherePart4NoComp = $@"WHERE DF_ID = {dataFlow.DbId} " +
                                        (dataFlow.Dsd.Msd.MetadataAttributes.Any()
                                            ? Environment.NewLine + "AND " + string.Join(" AND ",
                                                dataFlow.Dsd.Msd.MetadataAttributes.Select(a => $"{a.MariaDbColumn()} IS NULL"))
                                            : string.Empty) + Environment.NewLine;

            var innerWherePart4SomeComp = $@"WHERE DF_ID = {dataFlow.DbId} " +
                                          "AND (" +
                                          (dataFlow.Dsd.Msd.MetadataAttributes.Any()
                                              ? Environment.NewLine + string.Join(" OR ",
                                                  dataFlow.Dsd.Msd.MetadataAttributes.Select(a => $"{a.MariaDbColumn()} IS NOT NULL"))
                                              : "1 = 1") + " )";

            var innerGroupByPart = $@"GROUP BY {innerDimensionsColumns.TrimStart(',')}{innerTimeDimensionColumns4GroupBy} ";

            var innerUnionSql =
                $@"{innerSelectPartCommon}{innerAttributeColumns4SomeComp}
   {innerFromPart} 
   {innerWherePart4SomeComp}
   {innerGroupByPart}
UNION
   {innerSelectPartCommon}{innerAttributeColumns4NoComp}
   {innerFromPart}
   {innerWherePart4NoComp}
   {innerGroupByPart}";

            var sqlCommand =
                $@"CREATE ALGORITHM = UNDEFINED SQL SECURITY INVOKER VIEW {dotStatDb.DataSchema}_{dataFlow.MariaDbDeletedMetadataDataflowViewName(targetVersion)} 
                   AS
                       SELECT {MariaDbExtensions.LAST_UPDATED_COLUMN}
                            {outerTimeDimensionColumns}
                            {outerDimensionsColumns}
                            {outerAttributeColumns}
                       FROM ( {innerUnionSql} ) AS `DEL`
                            {joinStatements}";

            await dotStatDb.ExecuteNonQuerySqlAsync(sqlCommand, cancellationToken);

            return true;
        }

        public override Task<bool> CreateAndFillHprDataTables(Dataflow dataflow, char targetVersion, MariaDbDotStatDb dotStatDb,
            CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        public override Task<bool> CreateHPRMetadataViews(Dataflow dataflow, char tableVersion, MariaDbDotStatDb dotStatDb,
            CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }
    }
}