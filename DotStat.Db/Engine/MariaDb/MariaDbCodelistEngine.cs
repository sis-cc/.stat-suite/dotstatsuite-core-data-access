﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using DotStat.Common.Configuration.Interfaces;
using DotStat.Common.Enums;
using DotStat.Db.DB;
using DotStat.Db.Engine;
using DotStat.DB.Reader;
using DotStat.Domain;
using MySqlConnector;
using MySqlDbType = MySqlConnector.MySqlDbType;
using MySqlParameter = MySqlConnector.MySqlParameter;


namespace DotStat.DB.Engine.MariaDb
{
    public class MariaDbCodelistEngine : CodelistEngineBase<MariaDbDotStatDb>
    {
        public MariaDbCodelistEngine(IGeneralConfiguration generalConfiguration) :
            base(generalConfiguration)
        {
        }

        public override async Task<int> GetDbId(string sdmxId, string sdmxAgency, int verMajor, int verMinor, int? verPatch, MariaDbDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            var aretefactDbId = await dotStatDb.ExecuteScalarSqlWithParamsAsync(
                $@"SELECT ART_ID 
                    FROM {dotStatDb.ManagementSchema}_ARTEFACT 
                    WHERE ID = @Id AND AGENCY = @Agency AND `TYPE` = @Type
                    AND VERSION_1 = @Version1 AND VERSION_2 = @Version2 
                    AND (VERSION_3 = @Version3 OR (VERSION_3 IS NULL AND @Version3 IS NULL))",
                cancellationToken,
                new MySqlParameter("Id", MySqlDbType.VarChar) { Value = sdmxId },
                new MySqlParameter("Agency", MySqlDbType.VarChar) { Value = sdmxAgency },                
                new MySqlParameter("Type", MySqlDbType.VarChar) { Value = DbTypes.GetDbType(SDMXArtefactType.CodeList) },
                new MySqlParameter("Version1", MySqlDbType.Int32) { Value = verMajor },
                new MySqlParameter("Version2", MySqlDbType.Int32) { Value = verMinor },
                new MySqlParameter("Version3", MySqlDbType.Int32) { Value = ((object)verPatch) ?? DBNull.Value }
                );

            return (int)(aretefactDbId ?? -1);
        }

        public override async Task<int> InsertToArtefactTable(Codelist codelist, MariaDbDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            var artefactId = await dotStatDb.ExecuteScalarSqlLastInsertedIdWithParamsAsync(
                $@"INSERT 
                    INTO {dotStatDb.ManagementSchema}_ARTEFACT 
                         (SQL_ART_ID,TYPE,ID,AGENCY,VERSION_1,VERSION_2,VERSION_3,LAST_UPDATED) 
                  VALUES (NULL, @Type, @Id, @Agency, @Version1, @Version2, @Version3, @DT_Now)",
                cancellationToken,
                new MySqlParameter("Type", MySqlDbType.VarChar) { Value = DbTypes.GetDbType(SDMXArtefactType.CodeList) },
                new MySqlParameter("Id", MySqlDbType.VarChar) { Value = codelist.Code },
                new MySqlParameter("Agency", MySqlDbType.VarChar) { Value = codelist.AgencyId },
                new MySqlParameter("Version1", MySqlDbType.Int32) { Value = codelist.Version.Major },
                new MySqlParameter("Version2", MySqlDbType.Int32) { Value = codelist.Version.Minor },
                new MySqlParameter("Version3", MySqlDbType.Int32) { Value = (object)codelist.Version.Patch ?? DBNull.Value },
                new MySqlParameter("DT_Now", DateTime.Now)
                );

            return (int)artefactId;
        }

        public async Task<IList<string>> GetCodesOfCodeList(Codelist codelist, MariaDbDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            var result = new List<string>();
            var command = $"SELECT ID FROM {dotStatDb.ManagementSchema}_{GetCodelistTableName(codelist)}";
            using (var dr = await dotStatDb.ExecuteReaderSqlAsync(command, cancellationToken))
            {
                while (dr.Read())
                {
                    result.Add(dr.GetString(0));
                }
            }

            return result;
        }

        protected override async Task DeleteFromArtefactTableByDbId(int dbId, MariaDbDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            await dotStatDb.ExecuteNonQuerySqlWithParamsAsync(
                $@"DELETE
                    FROM {dotStatDb.ManagementSchema}_ARTEFACT
                   WHERE ART_ID = @DbId AND `TYPE` = 'CL'",
                cancellationToken,
                new MySqlParameter("DbId", MySqlDbType.VarChar) { Value = dbId }
                );
        }
        protected override string GetCodelistTableName(Codelist codelist)
        {
            return $"CL_{codelist.DbId}";
        }

        protected override async Task BuildDynamicCodelistTable(Codelist codelist, MariaDbDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            await dotStatDb.ExecuteNonQuerySqlAsync(
                $@"CREATE TABLE {dotStatDb.ManagementSchema}_{GetCodelistTableName(codelist)}( 
                        ITEM_ID int AUTO_INCREMENT NOT NULL, 
                        ID varchar(150) NOT NULL,
                    PRIMARY KEY PK_{GetCodelistTableName(codelist)} (ITEM_ID ASC))",
                cancellationToken
                );
        }

        protected override async Task FillDynamicCodelistTable(Codelist codelist, MariaDbDotStatDb dotStatDb, CancellationToken cancellationToken, IReadOnlyList<string> codesToInsert = null)
        {
            codesToInsert ??= codelist.Codes.Select(c => c.Code).ToArray();

            await using (var connection = dotStatDb.GetConnectionAllowLoadLocal()) 
            {
                var bulkCopy = new MySqlBulkCopy(connection, null);
                {
                    bulkCopy.DestinationTableName =
                        $"{dotStatDb.ManagementSchema}_{GetCodelistTableName(codelist)}";
                    bulkCopy.ColumnMappings.Add(new MySqlBulkCopyColumnMapping(0, "ID"));
                    await bulkCopy.WriteToServerAsync(
                        new BulkCopyEnumeratorDataReader<string>(codesToInsert, 1, s => new object[] { s }),
                        cancellationToken);
                }
            }
        }
    }
}
