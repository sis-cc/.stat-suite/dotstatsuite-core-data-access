﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using DotStat.Common.Configuration.Interfaces;
using DotStat.Common.Enums;
using DotStat.Db;
using DotStat.Db.DB;
using DotStat.Db.Engine;
using DotStat.Domain;
using MySqlConnector;


namespace DotStat.DB.Engine.MariaDb
{
    public class MariaDbMsdEngine : MsdEngineBase<MariaDbDotStatDb>
    {
        public MariaDbMsdEngine(IGeneralConfiguration generalConfiguration) : base(generalConfiguration)
        {
        }
        
        public override async Task<int> GetDbId(string sdmxId, string sdmxAgency, int verMajor, int verMinor, int? verPatch, MariaDbDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            var aretefactDbId = await dotStatDb.ExecuteScalarSqlWithParamsAsync(
                $@"SELECT ART_ID 
                    FROM {dotStatDb.ManagementSchema}_ARTEFACT 
                   WHERE ID = @Id AND AGENCY = @Agency AND TYPE = @Type
                     AND VERSION_1 = @Version1 AND VERSION_2 = @Version2 
                     AND (VERSION_3 = @Version3 OR (VERSION_3 IS NULL AND @Version3 IS NULL))",
                cancellationToken,
                new MySqlParameter("Id", MySqlDbType.VarChar) { Value = sdmxId },
                new MySqlParameter("Agency", MySqlDbType.VarChar) { Value = sdmxAgency },                
                new MySqlParameter("Type", MySqlDbType.VarChar) { Value = DbTypes.GetDbType(SDMXArtefactType.Msd) },
                new MySqlParameter("Version1", MySqlDbType.Int32) { Value = verMajor },
                new MySqlParameter("Version2", MySqlDbType.Int32) { Value = verMinor },
                new MySqlParameter("Version3", MySqlDbType.Int32) { Value = ((object)verPatch) ?? DBNull.Value }

                );

            return (int)(aretefactDbId ?? -1);
        }

        public override async Task<int> InsertToArtefactTable(Msd msd, MariaDbDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            return await dotStatDb.ExecuteScalarSqlLastInsertedIdWithParamsAsync(
                $@"INSERT 
                    INTO {dotStatDb.ManagementSchema}_ARTEFACT
                       (SQL_ART_ID,TYPE,ID,AGENCY,VERSION_1,VERSION_2,VERSION_3,LAST_UPDATED) 
                  VALUES (NULL, @Type, @Id, @Agency, @Version1, @Version2, @Version3, @DT_Now)",
                cancellationToken,
                new MySqlParameter("Type", MySqlDbType.VarChar) { Value = DbTypes.GetDbType(SDMXArtefactType.Msd) },
                new MySqlParameter("Id", MySqlDbType.VarChar) { Value = msd.Code },
                new MySqlParameter("Agency", MySqlDbType.VarChar) { Value = msd.AgencyId },
                new MySqlParameter("Version1", MySqlDbType.Int32) { Value = msd.Version.Major },
                new MySqlParameter("Version2", MySqlDbType.Int32) { Value = msd.Version.Minor },
                new MySqlParameter("Version3", MySqlDbType.Int32) { Value = (object)msd.Version.Patch ?? DBNull.Value },
                new MySqlParameter("DT_Now", DateTime.Now)
                );
        }

        public override Task<bool> CreateDynamicDbObjects(Msd artefact, MariaDbDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        public override async Task DropAllIndexes(Dsd dsd, MariaDbDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            if (dsd.Msd is null) return;

            var tables = new List<string> {
                $"{dsd.MariaDbMetadataDataStructureTable((char)DbTableVersion.A)}",
                $"{dsd.MariaDbMetadataDataStructureTable((char)DbTableVersion.B)}",
                $"{dsd.MariaDbDeletedMetadataTable((char)DbTableVersion.A)}",
                $"{dsd.MariaDbDeletedMetadataTable((char)DbTableVersion.B)}"
            };

            // Drop all existing indexes
            foreach (var table in tables)
            {
                await dotStatDb.DropAllIndexesOfTable(dotStatDb.DataSchema, table, cancellationToken);
            }
        }

        public override async Task Compress(Dsd dsd, DataCompressionEnum dataCompression, MariaDbDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            if (dataCompression is DataCompressionEnum.NONE || !dotStatDb.UseColumnstoreIndexes)  //TODO-MARIA - columnstore index creation may not be possible this way in MariaDb
                return;// no compression needed

            if (dsd.Msd is null) return;

            var tables = new List<string> {
                $"{dsd.MariaDbMetadataDataStructureTable((char)DbTableVersion.A)}",
                $"{dsd.MariaDbMetadataDataStructureTable((char)DbTableVersion.B)}",
                $"{dsd.MariaDbDeletedMetadataTable((char)DbTableVersion.A)}",
                $"{dsd.MariaDbDeletedMetadataTable((char)DbTableVersion.B)}",
            };

            // Create CCI indexes
            foreach (var table in tables)
            {
                await dotStatDb.CreateColumnstoreIndex(dotStatDb.DataSchema, table, $"CCI_{table}", dataCompression, cancellationToken);
            }
        }

        public override async Task AddUniqueConstraints(Dsd dsd, bool clustered, MariaDbDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            if (dsd.Msd is null) return;

            var dimensions = dsd.Dimensions.Where(dim => !dim.Base.TimeDimension).ToList();
            var timeDimColumns = dsd.TimeDimension is null ? "" : $",{MariaDbExtensions.MariaDbPeriodStart()},{MariaDbExtensions.MariaDbPeriodEnd()}";
            var uniqueColumns =
                //Time dimension requires two columns (period_start and period_end)
                dsd.Dimensions.Count > (dsd.TimeDimension != null ? 30 : 32)
                    ? "ROW_ID"
                    : dimensions.ToMariaDbColumnList() + timeDimColumns;

            //A
            var sqlCommand =
                $@"CREATE UNIQUE INDEX UI_{dsd.MariaDbMetadataDataStructureTable((char)DbTableVersion.A)}
                    ON {dotStatDb.DataSchema}_{dsd.MariaDbMetadataDataStructureTable((char)DbTableVersion.A)} ({uniqueColumns});
                ";
            await dotStatDb.ExecuteNonQuerySqlAsync(sqlCommand, cancellationToken);
            //B
            sqlCommand =
                $@"CREATE UNIQUE INDEX UI_{dsd.MariaDbMetadataDataStructureTable((char)DbTableVersion.B)}
                    ON {dotStatDb.DataSchema}_{dsd.MariaDbMetadataDataStructureTable((char)DbTableVersion.B)} ({uniqueColumns});
                ";
            await dotStatDb.ExecuteNonQuerySqlAsync(sqlCommand, cancellationToken);
        }

        #region protected
        protected override async Task DeleteFromArtefactTableByDbId(int dbId, MariaDbDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            await dotStatDb.ExecuteNonQuerySqlWithParamsAsync(
                $@"DELETE
                    FROM {dotStatDb.ManagementSchema}_ARTEFACT 
                   WHERE ART_ID = @DbId AND `TYPE` = @Type",
                cancellationToken,
                new MySqlParameter("DbId", MySqlDbType.VarChar) { Value = dbId },
                new MySqlParameter("Type", MySqlDbType.VarChar) { Value = DbTypes.GetDbType(SDMXArtefactType.Msd) }
                );
        }

        protected override async Task BuildDynamicMetaDataTable(Dsd dsd, char targetVersion, MariaDbDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            var dimensions = dsd.Dimensions.Where(dim => !dim.Base.TimeDimension).ToArray();
            //Change to allowNull=true when non coded dimensions are introduced. 
            //A value of 0 should be given when the dimension is not being referenced
            var dimensionColumnsWithType = dimensions.ToMariaDbColumnList(true);
            var dimensionColumnsIndex = dimensions.ToMariaDbColumnList(forIndex: true);
            var table = dsd.MariaDbMetadataDataStructureTable(targetVersion);
            
            var timeDim = dsd.TimeDimension;
            var supportsDateTime = dsd.Base.HasSupportDateTimeAnnotation();

            var timeDimensionColumns = timeDim != null
                ? string.Join(",",
                    // NULL is stored when time is not referenced by the metadata.
                    new[] { timeDim }.ToMariaDbColumnList(true, true),
                    //The maximum SQL data/datetime is stored when time is not referenced by the metadata.
                    MariaDbExtensions.MariaDbPeriodStart(true, supportsDateTime, false),
                    //The minimum SQL data/datetime is stored when time is not referenced by the metadata.
                    MariaDbExtensions.MariaDbPeriodEnd(true, supportsDateTime, false)
                ) + ","
                : null;

            var timeConstraint =
                timeDim != null ? $", {MariaDbExtensions.MariaDbPeriodStart()}, {MariaDbExtensions.MariaDbPeriodEnd()} DESC" : null;
            
            string rowId = null;
            var uIndex = dimensionColumnsIndex + timeConstraint;

            //Time dimension requires two columns (period_start and period_end)
            if (dsd.Dimensions.Count > (timeDim != null ? 30: 32))
            {
                rowId = $"ROW_ID blob AS ({dsd.MariaDbBuildRowIdFormula()}) PERSISTENT,";
                uIndex = "ROW_ID";
            }
            
            var metaAttributeColumns = dsd.Msd.MetadataAttributes
                .ToMariaDbColumnList(true);

            if (!string.IsNullOrWhiteSpace(metaAttributeColumns))
            {
                metaAttributeColumns += ',';
            }

            bool useColumnStore = (dsd.DataCompression != DataCompressionEnum.NONE && dotStatDb.UseColumnstoreIndexes);
            var constraint = useColumnStore ? "" : $", PRIMARY KEY PK_{table} ({uIndex})";

            var sqlCommand = $@"CREATE TABLE {dotStatDb.DataSchema}_{table}(
                        {rowId}
                        {dimensionColumnsWithType},
                        {timeDimensionColumns}
                        {metaAttributeColumns}
                        {MariaDbExtensions.LAST_UPDATED_COLUMN} datetime(6) NOT NULL
                        {constraint}
                ) {(useColumnStore ? "ENGINE=ColumnStore" : null)}";

            await dotStatDb.ExecuteNonQuerySqlAsync(sqlCommand, cancellationToken);

            //Apply data compression
            if (dsd.DataCompression != DataCompressionEnum.NONE && dotStatDb.UseColumnstoreIndexes) //TODO-MARIA - not sure if such an index is possible in MariaDb
            {
                await dotStatDb.ExecuteNonQuerySqlAsync(
                    $@"CREATE CLUSTERED COLUMNSTORE INDEX CCI_{table}
ON {dotStatDb.DataSchema}_{table}
WITH(DATA_COMPRESSION = {dsd.DataCompression})", cancellationToken);

                if (dsd.DataCompression == DataCompressionEnum.COLUMNSTORE)
                {
                    await AddUniqueConstraints(dsd, false, dotStatDb, cancellationToken);
                }
            }

        }

        protected override async Task BuildDeletedTable(Dsd dsd, char targetVersion, MariaDbDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            var dimensions = dsd.Dimensions.Where(dim => !dim.Base.TimeDimension).ToArray();
            var dimensionColumnsWithType = dimensions.ToMariaDbColumnList(true, allowNull: true);
            var metaAttributeColumns = string.Join(",", dsd.Msd.MetadataAttributes.Select(x => x.MariaDbColumn() + " char(1)"));

            if (!string.IsNullOrWhiteSpace(metaAttributeColumns))
            {
                metaAttributeColumns += ',';
            }

            var timeDim = dsd.TimeDimension;
            var supportsDateTime = dsd.Base.HasSupportDateTimeAnnotation();

            var timeDimensionColumns = timeDim != null
                ? string.Join(",",
                    new[] { timeDim }.ToMariaDbColumnList(true, allowNull: true),
                    MariaDbExtensions.MariaDbPeriodStart(true, supportsDateTime, true),
                    MariaDbExtensions.MariaDbPeriodEnd(true, supportsDateTime, true)
                ) + ","
                : null;

            var sqlCommand = $@"CREATE TABLE {dotStatDb.DataSchema}_{dsd.MariaDbDeletedMetadataTable(targetVersion)}(
DF_ID int not null,
{timeDimensionColumns}
{dimensionColumnsWithType},
{metaAttributeColumns}
{MariaDbExtensions.LAST_UPDATED_COLUMN} datetime(6) NOT NULL
)";

            await dotStatDb.ExecuteNonQuerySqlAsync(sqlCommand, cancellationToken);

            bool useColumnStore = (dsd.DataCompression != DataCompressionEnum.NONE && dotStatDb.UseColumnstoreIndexes);

            //Apply data compression
            if (useColumnStore)
            {
                await dotStatDb.ExecuteNonQuerySqlAsync(
                    $@"ALTER TABLE {dotStatDb.DataSchema}_{dsd.MariaDbDeletedMetadataTable(targetVersion)} ENGINE=ColumnStore", cancellationToken);
            }
            else
            {
                sqlCommand = $@"CREATE INDEX I_{dsd.MariaDbDeletedMetadataTable(targetVersion)} 
ON {dotStatDb.DataSchema}_{dsd.MariaDbDeletedMetadataTable(targetVersion)}(DF_ID,LAST_UPDATED)";

                await dotStatDb.ExecuteNonQuerySqlAsync(sqlCommand, cancellationToken);
            }
        }

        protected override async Task BuildDynamicDatasetMetaDataTable(Dsd dsd, char targetVersion, MariaDbDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            var metaAttributeColumns = dsd.Msd.MetadataAttributes
                .ToMariaDbColumnList(true);

            if (!string.IsNullOrWhiteSpace(metaAttributeColumns))
            {
                metaAttributeColumns += ',';
            }

            var table = dsd.MariaDbMetadataDataSetTable(targetVersion);

            bool useColumnStore = (dsd.DataCompression != DataCompressionEnum.NONE && dotStatDb.UseColumnstoreIndexes);

            var constraint = useColumnStore ? "" : $", PRIMARY KEY PK_{table} (DF_ID)";
            var sqlCommand = $@"CREATE TABLE {dotStatDb.DataSchema}_{table}(
                        DF_ID INT NOT NULL,
                        {metaAttributeColumns}
                        {MariaDbExtensions.LAST_UPDATED_COLUMN} datetime(6) NOT NULL
                        {constraint}                        
                )";

            await dotStatDb.ExecuteNonQuerySqlAsync(sqlCommand, cancellationToken);

            //Apply data compression
            if (useColumnStore)
            {
                await dotStatDb.ExecuteNonQuerySqlAsync(
                    $@"ALTER TABLE {dotStatDb.DataSchema}_{table} ENGINE=ColumnStore", cancellationToken);
            }
        }

        public override async ValueTask<bool> BuildDynamicMetaDataDsdView(Dsd dsd, char targetVersion, MariaDbDotStatDb dotStatDb, bool sourceVersion, CancellationToken cancellationToken)
        {
            //The codelist table doesnt exist, therefore the db is corrupted or the method is called by the unit tests
            foreach (var dimension in dsd.Dimensions.Where(x => x.Base.HasCodedRepresentation()))
            {
                if (!await dotStatDb.TableExists($"{dotStatDb.ManagementSchema}_CL_{dimension.Codelist.DbId}", cancellationToken))
                {
                    return false;
                }
            }

            //The dependent table does not exists, therefore the db is corrupted or the method is called by the unit tests
            if (!await dotStatDb.TableExists($"{dotStatDb.DataSchema}_{dsd.MariaDbMetadataDataStructureTable(targetVersion)}", cancellationToken))
            {
                return false;
            }

            //The dependent table does not exists, therefore the db is corrupted or the method is called by the unit tests
            if (!await dotStatDb.TableExists($"{dotStatDb.DataSchema}_{dsd.MariaDbMetadataDataSetTable(targetVersion)}", cancellationToken))
            {
                return false;
            }

            //The view already exists, therefore no need to recreate it.
            if (await dotStatDb.ViewExists($"{dotStatDb.DataSchema}_{dsd.MariaDbMetaDataDsdViewName(targetVersion)}", cancellationToken))
                return false;

            var dimensions = dsd.Dimensions.Where(dim => !dim.Base.TimeDimension).ToList();
            
            // SELECT ----------------------------------------------------------------

            var selectStatement = new StringBuilder();
            var dsSelectStatement = new StringBuilder();
            //dimensions
            selectStatement
                .Append(string.Join(", ", dimensions.Select(d =>
                d.Base.HasCodedRepresentation() ? 
                    $"CASE WHEN CL_{d.Code}.ID IS NULL THEN '{MariaDbExtensions.DimensionSwitchedOff}' ELSE CL_{d.Code}.ID END AS `{d.Code}`" 
                    : $"CASE WHEN ME.{d.MariaDbColumn()} IS NULL THEN '{MariaDbExtensions.DimensionSwitchedOff}' ELSE ME.{d.MariaDbColumn()} END AS `{d.Code}`")));

            dsSelectStatement
                .Append(string.Join(", ", dimensions.Select(d => $"'{MariaDbExtensions.DimensionSwitchedOff}' AS `{d.Code}`")));

            //time dimension
            var timeDim = dsd.TimeDimension;
            var (sqlMin, sqlMax) = dsd.Base.HasSupportDateTimeAnnotation()
                ? (DateTime.MinValue.ToString("yyyy-MM-dd HH:mm:ss"), DateTime.MaxValue.ToString("yyyy-MM-dd HH:mm:ss"))
                : (DateTime.MinValue.ToString("yyyy-MM-dd"), DateTime.MaxValue.ToString("yyyy-MM-dd"));

            if (timeDim != null)
            {
                selectStatement
                    .Append(",")
                    .Append(string.Join(",",
                        $"CASE WHEN ME.{timeDim.MariaDbColumn()} IS NULL THEN '{MariaDbExtensions.DimensionSwitchedOff}' ELSE ME.{timeDim.MariaDbColumn()} END AS `{dsd.Base.TimeDimension.Id}`",
                        MariaDbExtensions.MariaDbPeriodStart(),
                        MariaDbExtensions.MariaDbPeriodEnd()
                    ));

                dsSelectStatement
                    .Append(", ")
                    .Append(string.Join(", ",
                        $"'{MariaDbExtensions.DimensionSwitchedOff}' AS `{dsd.Base.TimeDimension.Id}`",
                        $"'{sqlMax}' AS `{MariaDbExtensions.MariaDbPeriodStart()}`",
                        $"'{sqlMin}' AS `{MariaDbExtensions.MariaDbPeriodEnd()}`"
                    ));
            }

            //metadata attributes
            if (dsd.Msd.MetadataAttributes.Any())
            {
                selectStatement
                    .Append(",")
                    .Append(string.Join(", ", dsd.Msd.MetadataAttributes.Select(a => $"ME.{a.MariaDbColumn()} AS `{a.HierarchicalId}`"
                )));

                dsSelectStatement
                    .Append(",")
                    .Append(string.Join(", ", dsd.Msd.MetadataAttributes.Select(a => $"{a.MariaDbColumn()} AS `{a.HierarchicalId}`"
                    )));
            }

            // LAST_UPDATED
            selectStatement.Append($",ME.{MariaDbExtensions.LAST_UPDATED_COLUMN}");
            dsSelectStatement.Append($",{MariaDbExtensions.LAST_UPDATED_COLUMN}");

            // JOIN ------------------------------------------------------------------
            var joinStatement = new StringBuilder();

            //dimensions
            joinStatement.AppendLine(string.Join("\n", dimensions.Where(d => d.Base.HasCodedRepresentation()).Select(d =>
                    $"LEFT JOIN {dotStatDb.ManagementSchema}_CL_{d.Codelist.DbId} AS `CL_{d.Code}` ON ME.{d.MariaDbColumn()} = CL_{d.Code}.ITEM_ID"
                )
            ));

            var dsdLevel = $@"
                        SELECT {selectStatement}
                        FROM {dotStatDb.DataSchema}_{dsd.MariaDbMetadataDataStructureTable(targetVersion)} AS `ME`
                        {joinStatement}";

            var dsLevel = $@"
                        SELECT {dsSelectStatement}
                        FROM {dotStatDb.DataSchema}_{dsd.MariaDbMetadataDataSetTable(targetVersion)}
                        WHERE DF_ID = -1";

            var sqlCommand =
                $@"CREATE ALGORITHM = UNDEFINED SQL SECURITY INVOKER VIEW {dotStatDb.DataSchema}_{dsd.MariaDbMetaDataDsdViewName(targetVersion)} 
                   AS
                        {dsdLevel}
                        UNION ALL
                        {dsLevel}
                ";

            return await dotStatDb.ExecuteNonQuerySqlAsync(sqlCommand, cancellationToken) > 0;
        }

        public override async Task BuildDeletedView(Dsd dsd, char targetVersion, MariaDbDotStatDb dotStatDb, bool sourceVersion, CancellationToken cancellationToken)
        {
            //The dependent table does not exists, therefore the db is corrupted or the method is called by the unit tests
            if (!await dotStatDb.TableExists($"{dotStatDb.DataSchema}_{dsd.MariaDbDeletedMetadataTable(targetVersion)}", cancellationToken))
            {
                return;
            }

            //The view already exists, therefore no need to recreate it.
            if (await dotStatDb.ViewExists($"{dotStatDb.DataSchema}_{dsd.MariaDbDeletedMetadataDsdViewName(targetVersion)}", cancellationToken))
                return;

            //Dimensions
            var dimensions = dsd.Dimensions
                .Where(dim => !dim.Base.TimeDimension)
                .ToList();

            //Time dimension columns
            var innerTimeDimensionColumns = dsd.TimeDimension != null
                    ? ", " + string.Join(", ",
                            $"{MariaDbExtensions.TimeDimColumn}",
                            MariaDbExtensions.MariaDbPeriodStart(),
                            MariaDbExtensions.MariaDbPeriodEnd()
                        )
                    : string.Empty;

            var innerTimeDimensionColumns4GroupBy = innerTimeDimensionColumns;
            var (sqlMax, sqlMaxMinOne) = dsd.Base.HasSupportDateTimeAnnotation()
                ? (DateTime.MaxValue.ToString("yyyy-MM-dd HH:mm:ss"), DateTime.MaxValue.AddYears(-1).ToString("yyyy-MM-dd HH:mm:ss"))
                : (DateTime.MaxValue.ToString("yyyy-MM-dd"), DateTime.MaxValue.AddYears(-1).ToString("yyyy-MM-dd"));
            var outerTimeDimensionColumns = dsd.TimeDimension != null
                ? ", " + string.Join(", ",
                    @$"CASE 
                            WHEN DEL.{MariaDbExtensions.MariaDbPeriodStart()} = '{sqlMax}' THEN '{MariaDbExtensions.DimensionSwitchedOff}'
                            WHEN DEL.{MariaDbExtensions.MariaDbPeriodStart()} = '{sqlMaxMinOne}' THEN '{MariaDbExtensions.DimensionWildCarded}'
                            ELSE DEL.{MariaDbExtensions.TimeDimColumn}
                      END AS `{dsd.Base.TimeDimension.Id}`",
                    $"{MariaDbExtensions.MariaDbPeriodStart()}",
                    $"{MariaDbExtensions.MariaDbPeriodEnd()}")
                : string.Empty;

            //Dimensions
            var innerDimensionsColumns = dimensions.Any()
                        ? ", " + string.Join(", ", dimensions.Select(d => $"{d.MariaDbColumn()}"))
                        : string.Empty;

            var outerDimensionsColumns = dimensions.Any()
                ? ", " +
                  string.Join(
                      ", ",
                      dimensions.Select(d =>
                          (d.Base.HasCodedRepresentation()
                              ? @$"CASE
                            WHEN DEL.{d.MariaDbColumn()} = {MariaDbExtensions.DimensionSwitchedOffDbValue} THEN '{MariaDbExtensions.DimensionSwitchedOff}'
                            WHEN DEL.{d.MariaDbColumn()} = {MariaDbExtensions.DimensionWildCardedDbValue} THEN '{MariaDbExtensions.DimensionWildCarded}'
                            ELSE CL_{d.Code}.ID
                        END"
                              : $"{d.MariaDbColumn()}") + $" AS `{d.Code}`"
                      )
                  )
                : string.Empty;

            //Attributes
            var innerAttributeColumns4SomeComp = dsd.Msd.MetadataAttributes.Any()
                ? "," + string.Join(",", dsd.Msd.MetadataAttributes.Select(a => $"MAX( {a.MariaDbColumn()} ) AS `{a.HierarchicalId}`"))
                : string.Empty;

            var innerAttributeColumns4NoComp = dsd.Msd.MetadataAttributes.Any()
                ? "," + string.Join(",", dsd.Msd.MetadataAttributes.Select(a => $"NULL AS `{a.HierarchicalId}`"))
                : string.Empty;

            var outerAttributeColumns = dsd.Msd.MetadataAttributes.Any()
                ? "," + string.Join(",", dsd.Msd.MetadataAttributes.Select(a => $" `{a.HierarchicalId}`"))
                : string.Empty;

            var joinStatements = string.Join(Environment.NewLine, dimensions.Where(d => d.Base.HasCodedRepresentation()).Select(d =>
                    $"LEFT JOIN {dotStatDb.ManagementSchema}_CL_{d.Codelist.DbId} AS `CL_{d.Code}` ON DEL.{d.MariaDbColumn()} = CL_{d.Code}.ITEM_ID"
                )
            );

            var innerSelectPartCommon =
$@"SELECT MAX( {MariaDbExtensions.LAST_UPDATED_COLUMN} ) AS `{MariaDbExtensions.LAST_UPDATED_COLUMN}`
    {innerTimeDimensionColumns}
    {innerDimensionsColumns}";

            var innerFromPart = $@"FROM {dotStatDb.DataSchema}_{dsd.MariaDbDeletedMetadataTable(targetVersion)} ";

            var innerWherePart4NoComp = $@"WHERE DF_ID = -1 " +
                                        (dsd.Msd.MetadataAttributes.Any()
                                            ? Environment.NewLine + "AND " + string.Join(" AND ",
                                                dsd.Msd.MetadataAttributes.Select(a => $"{a.MariaDbColumn()} IS NULL"))
                                            : string.Empty) + Environment.NewLine;

            var innerWherePart4SomeComp = $@"WHERE DF_ID = -1 " +
                                          "AND (" +
                                          (dsd.Msd.MetadataAttributes.Any()
                                              ? Environment.NewLine + string.Join(" OR ",
                                                  dsd.Msd.MetadataAttributes.Select(a => $"{a.MariaDbColumn()} IS NOT NULL"))
                                              : "1 = 1") + " )";

            var innerGroupByPart = $@"GROUP BY {innerDimensionsColumns.TrimStart(',')}{innerTimeDimensionColumns4GroupBy} ";

            var innerUnionSql =
                $@"{innerSelectPartCommon}{innerAttributeColumns4SomeComp}
   {innerFromPart} 
   {innerWherePart4SomeComp}
   {innerGroupByPart}
UNION
   {innerSelectPartCommon}{innerAttributeColumns4NoComp}
   {innerFromPart}
   {innerWherePart4NoComp}
   {innerGroupByPart}";

            var sqlCommand =
                $@"CREATE VIEW {dotStatDb.DataSchema}_{dsd.MariaDbDeletedMetadataDsdViewName(targetVersion)} 
                   AS
                       SELECT {MariaDbExtensions.LAST_UPDATED_COLUMN}
                            {outerTimeDimensionColumns}
                            {outerDimensionsColumns}
                            {outerAttributeColumns}
                       FROM ( {innerUnionSql} ) AS `DEL`
                            {joinStatements}";

            await dotStatDb.ExecuteNonQuerySqlAsync(sqlCommand, cancellationToken);

            return;
        }
        #endregion

    }
}
