﻿using System;
using System.Threading;
using System.Threading.Tasks;
using DotStat.Common.Configuration.Interfaces;
using DotStat.Db;
using DotStat.Db.DB;
using DotStat.Db.Engine;
using DotStat.Domain;
using MySqlConnector;


namespace DotStat.DB.Engine.MariaDb
{
    public class MariaDbMetadataAttributeEngine : MetadataAttributeEngineBase<MariaDbDotStatDb>
    {
        public MariaDbMetadataAttributeEngine(IGeneralConfiguration generalConfiguration) : base(generalConfiguration)
        {
        }

        public override async Task<int> GetDbId(string sdmxId, int msdId, MariaDbDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            var componentDbId = await dotStatDb.ExecuteScalarSqlWithParamsAsync($@"SELECT MTD_ATTR_ID 
                FROM {dotStatDb.ManagementSchema}_{MariaDbExtensions.MariaDbMetadataAttributeTable()} 
               WHERE ID = @Id AND MSD_ID = @MsdId",
                cancellationToken,
                new MySqlParameter("Id", MySqlDbType.VarChar) { Value = sdmxId }, 
                new MySqlParameter("MsdId", MySqlDbType.Int32) { Value = msdId });

            return (int)(componentDbId ?? -1);
        }

        public override async Task<int> InsertToComponentTable(MetadataAttribute metadataAttribute, MariaDbDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            return await dotStatDb.ExecuteScalarSqlLastInsertedIdWithParamsAsync($@"INSERT 
                    INTO {dotStatDb.ManagementSchema}_{MariaDbExtensions.MariaDbMetadataAttributeTable()} 
                         (ID,MSD_ID,CL_ID,MIN_OCCURS,MAX_OCCURS,ENUM_ID)
                  VALUES (@Id, @MsdId, @ClId, @MinOccurs, @MaxOccurs, @EnumId)",
                cancellationToken,
                new MySqlParameter("Id", MySqlDbType.VarChar) { Value = metadataAttribute.HierarchicalId }, 
                new MySqlParameter("MsdId", MySqlDbType.Int32) { Value = metadataAttribute.Dsd.Msd.DbId }, 
                new MySqlParameter("ClId", MySqlDbType.Int32) { Value = DBNull.Value }, 
                new MySqlParameter("MinOccurs", MySqlDbType.Int32) { Value = (object)metadataAttribute.Base.MinOccurs ?? DBNull.Value }, 
                new MySqlParameter("MaxOccurs", MySqlDbType.Int32) { Value = (object)metadataAttribute.Base.MaxOccurs ?? DBNull.Value }, 
                new MySqlParameter("EnumId", MySqlDbType.Int64) { Value = (object)(1) });
        }

        protected override async Task DeleteFromComponentTableByDbId(int dbId, MariaDbDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            await dotStatDb.ExecuteNonQuerySqlWithParamsAsync($@"DELETE
                    FROM {dotStatDb.ManagementSchema}_{MariaDbExtensions.MariaDbMetadataAttributeTable()}
                   WHERE MTD_ATTR_ID = @DbId",
                cancellationToken, 
                new MySqlParameter("DbId", MySqlDbType.VarChar) { Value = dbId });
        }
    }
}
