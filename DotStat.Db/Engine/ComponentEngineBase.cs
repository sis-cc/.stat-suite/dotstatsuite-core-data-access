using DotStat.Common.Configuration.Interfaces;
using DotStat.Db.DB;
using DotStat.Domain;
using System.Threading;
using System.Threading.Tasks;

namespace DotStat.Db.Engine
{
    public abstract class ComponentEngineBase<TComponent, TDotStatDb> 
        where TComponent : IDotStatIdentifiable
        where TDotStatDb : IDotStatDb
    {
        protected readonly IGeneralConfiguration GeneralConfiguration;

        protected ComponentEngineBase(IGeneralConfiguration generalConfiguration)
        {
            GeneralConfiguration = generalConfiguration;
        }

        public abstract Task<int> GetDbId(string sdmxId, int parentIdId, TDotStatDb dotStatDb, CancellationToken cancellationToken);

        public abstract Task<int> InsertToComponentTable(TComponent component, TDotStatDb dotStatDb, CancellationToken cancellationToken);

        public abstract Task CleanUp(TComponent component, TDotStatDb dotStatDb, CancellationToken cancellationToken);

        protected abstract Task DeleteFromComponentTableByDbId(int dbId, TDotStatDb dotStatDb, CancellationToken cancellationToken);
    }
}
