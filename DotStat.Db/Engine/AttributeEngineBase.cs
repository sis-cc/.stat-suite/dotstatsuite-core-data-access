﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using DotStat.Common.Configuration.Interfaces;
using DotStat.Common.Exceptions;
using DotStat.Common.Localization;
using DotStat.Db.DB;
using DotStat.Domain;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Constants.InterfaceConstant;
using Attribute = DotStat.Domain.Attribute;

namespace DotStat.Db.Engine
{
    public abstract class AttributeEngineBase<TDotStatDb> : ComponentEngineBase<Attribute, TDotStatDb>
        where TDotStatDb : IDotStatDb
    {
        protected AttributeEngineBase(IGeneralConfiguration generalConfiguration) : base(generalConfiguration)
        {
        }

        public async Task CreateDynamicDbObjects(Dsd dsd, TDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            if (dsd == null)
            {
                throw new ArgumentNullException(nameof(dsd));
            }

            if (dotStatDb == null)
            {
                throw new ArgumentNullException(nameof(dotStatDb));
            }

            if (dsd.Attributes != null && dsd.Attributes.Any())
            {
                //Set A
                await BuildDynamicDatasetAttributeTable(dsd, (char)DbTableVersion.A, dotStatDb, cancellationToken);
                await BuildDynamicDimensionGroupAttributeTable(dsd, (char)DbTableVersion.A, dotStatDb, cancellationToken);
                
                //Set B
                await BuildDynamicDatasetAttributeTable(dsd, (char)DbTableVersion.B, dotStatDb, cancellationToken);
                await BuildDynamicDimensionGroupAttributeTable(dsd, (char)DbTableVersion.B, dotStatDb, cancellationToken);
            }
        }

        public override async Task CleanUp(Attribute component, TDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            //Does not delete nor changes structure of related attribute table!

            if (component == null)
            {
                throw new ArgumentNullException(nameof(component));
            }

            if (dotStatDb == null)
            {
                throw new ArgumentNullException(nameof(dotStatDb));
            }

            if (component.DbId < 1)
            {
                throw new DotStatException(string.Format(
                        LocalizationRepository.GetLocalisedResource(Localization.ResourceId.AttributeInvalidDbId),
                        component.Code, component.DbId)
                );
            }

            await DeleteFromComponentTableByDbId(component.DbId, dotStatDb, cancellationToken);
        }

        public async Task AlterTextAttributeColumns(Attribute attribute, TDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            switch (attribute.Base.AttachmentLevel)
            {
                case AttributeAttachmentLevel.Observation:
                case AttributeAttachmentLevel.DimensionGroup or AttributeAttachmentLevel.Group when attribute.Base.GetDimensionReferences().Contains(DimensionObject.TimeDimensionFixedId):
                    await AlterTextAttributeColumnsInFactTable(attribute, (char)DbTableVersion.A, dotStatDb, cancellationToken);
                    await AlterTextAttributeColumnsInFactTable(attribute, (char)DbTableVersion.B, dotStatDb, cancellationToken);
                    break;
                case AttributeAttachmentLevel.DimensionGroup or AttributeAttachmentLevel.Group when !attribute.Base.DimensionReferences.Contains(DimensionObject.TimeDimensionFixedId):
                    await AlterTextAttributeColumnsInDimensionGroupAttributeTable(attribute, (char)DbTableVersion.A, dotStatDb, cancellationToken);
                    await AlterTextAttributeColumnsInDimensionGroupAttributeTable(attribute, (char)DbTableVersion.B, dotStatDb, cancellationToken);
                    break;
                case AttributeAttachmentLevel.DataSet:
                    await AlterTextAttributeColumnsInDatasetAttributeTable(attribute, (char)DbTableVersion.A, dotStatDb, cancellationToken);
                    await AlterTextAttributeColumnsInDatasetAttributeTable(attribute, (char)DbTableVersion.B, dotStatDb, cancellationToken);
                    break;
            }
        }

        public abstract Task DropAllIndexes(Dsd dsd, TDotStatDb dotStatDb, CancellationToken cancellationToken);
        public abstract Task Compress(Dsd dsd, DataCompressionEnum dataCompression, TDotStatDb dotStatDb, CancellationToken cancellationToken);
        public abstract Task AddUniqueConstraints(Dsd dsd, bool clustered, TDotStatDb dotStatDb, CancellationToken cancellationToken);

        protected abstract Task BuildDynamicDatasetAttributeTable(Dsd dsd, char targetVersion, TDotStatDb dotStatDb, CancellationToken cancellationToken);

        protected abstract Task BuildDynamicDimensionGroupAttributeTable(Dsd dsd, char targetVersion, TDotStatDb dotStatDb, CancellationToken cancellationToken);

        protected abstract Task AlterTextAttributeColumnsInDatasetAttributeTable(Attribute attribute, char targetVersion, TDotStatDb dotStatDb, CancellationToken cancellationToken);

        protected abstract Task AlterTextAttributeColumnsInDimensionGroupAttributeTable(Attribute attribute, char targetVersion, TDotStatDb dotStatDb, CancellationToken cancellationToken);

        protected abstract Task AlterTextAttributeColumnsInFactTable(Attribute attribute, char targetVersion, TDotStatDb dotStatDb, CancellationToken cancellationToken);
    }
}
