﻿using System;
using System.Threading;
using System.Threading.Tasks;
using DotStat.Common.Configuration.Interfaces;
using DotStat.Db.DB;
using DotStat.Db.Util;
using DotStat.Domain;

namespace DotStat.Db.Engine
{
    public abstract class MetadataDataflowEngineBase<TDotStatDb> : ArtefactEngineBase<Dataflow, TDotStatDb>
        where TDotStatDb : IDotStatDb
    {
        protected MetadataDataflowEngineBase(IGeneralConfiguration generalConfiguration) : base(generalConfiguration)
        {
        }

        public override async Task<bool> CreateDynamicDbObjects(Dataflow dataflow, TDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            if (dataflow == null)
            {
                throw new ArgumentNullException(nameof(dataflow));
            }

            //Set A
            await BuildDynamicMetadataDataFlowTable(dataflow, (char)DbTableVersion.A, dotStatDb, cancellationToken);
            await BuildDynamicMetadataDataFlowView(dataflow, (char) DbTableVersion.A, dotStatDb, false, null, cancellationToken);
            await BuildDeletedView(dataflow, (char)DbTableVersion.A, dotStatDb, false, cancellationToken);

            //Set B
            await BuildDynamicMetadataDataFlowTable(dataflow, (char)DbTableVersion.B, dotStatDb, cancellationToken);
            await BuildDynamicMetadataDataFlowView(dataflow, (char) DbTableVersion.B, dotStatDb, false, null, cancellationToken);
            return await BuildDeletedView(dataflow, (char)DbTableVersion.B, dotStatDb, false, cancellationToken);

        }

        public override Task CleanUp(Dataflow dataflow, TDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }
        public abstract Task DropAllIndexes(Dataflow dataFlow, TDotStatDb dotStatDb, CancellationToken cancellationToken);
        public abstract Task Compress(Dataflow dataFlow, DataCompressionEnum dataCompression, TDotStatDb dotStatDb, CancellationToken cancellationToken);
        public abstract Task AddUniqueConstraints(Dataflow dataFlow, bool clustered, TDotStatDb dotStatDb, CancellationToken cancellationToken);

        protected abstract Task BuildDynamicMetadataDataFlowTable(Dataflow dataflow, char targetVersion, TDotStatDb dotStatDb, CancellationToken cancellationToken);

        public abstract Task<bool> BuildDynamicMetadataDataFlowView(Dataflow dataFlow, char targetVersion, TDotStatDb sqlDotStatDb, bool sourceVersion, ICodeTranslator codeTranslator, CancellationToken cancellationToken);

        public abstract Task<bool> BuildDeletedView(Dataflow dataFlow, char targetVersion, TDotStatDb dotStatDb, bool sourceVersion, CancellationToken cancellationToken);

        public abstract Task<bool> CreateAndFillHprDataTables(Dataflow dataflow, char targetVersion, TDotStatDb dotStatDb, CancellationToken cancellationToken);

        public abstract Task<bool> CreateHPRMetadataViews(Dataflow dataflow, char tableVersion, TDotStatDb dotStatDb, CancellationToken cancellationToken);
    }
}