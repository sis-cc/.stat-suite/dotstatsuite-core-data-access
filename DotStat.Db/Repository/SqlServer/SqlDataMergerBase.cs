﻿using DotStat.Common.Configuration.Interfaces;
using DotStat.Db;
using DotStat.Db.DB;
using DotStat.Db.Util;
using DotStat.DB.Util;
using DotStat.Domain;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Data.SqlClient;

namespace DotStat.DB.Repository.SqlServer
{
    public abstract class SqlDataMergerBase : DataMergerBase<SqlDotStatDb>
    {
        protected SqlDataMergerBase(SqlDotStatDb dotStatDb, IGeneralConfiguration generalConfiguration) :
            base(dotStatDb, generalConfiguration)
        {
        }

        protected async Task<MergeResult> DeleteAll(string tableName, string filterAllowedContentConstraint, bool includeSummary, CancellationToken cancellationToken)
        {
            var mergeResult = new MergeResult();
            if (!includeSummary && string.IsNullOrEmpty(filterAllowedContentConstraint))
            {
                if (DotStatDb.DataSpace.KeepHistory)
                {
                    //Temporal tables do not allow Truncate commands
                    await DotStatDb.DeleteAllFromTable(DotStatDb.DataSchema, tableName, cancellationToken);
                }
                else
                {
                    await DotStatDb.TruncateTable(DotStatDb.DataSchema, tableName, cancellationToken);
                }
            }
            else
            {
                var sqlCommand = $@"DELETE FROM [{DotStatDb.DataSchema}].[{tableName}] 
{filterAllowedContentConstraint};";

                if (includeSummary)
                {
                    sqlCommand += " SELECT @@ROWCOUNT AS [ROW_COUNT];";

                    mergeResult.DeleteCount += (int)await DotStatDb.ExecuteScalarSqlAsync(sqlCommand, cancellationToken);
                    mergeResult.TotalCount =
                        mergeResult.UpdateCount + mergeResult.DeleteCount + mergeResult.InsertCount;
                }
                else
                {
                    await DotStatDb.ExecuteNonQuerySqlAsync(sqlCommand, cancellationToken);
                }
            }

            return mergeResult;
        }

        protected async Task InsertDeletedAllRow(int dataFlowId, string tableName, DateTime dateTime,  CancellationToken cancellationToken)
        {
            //Insert delete all row to deleted table
            var sqlCommand = $@"INSERT INTO [{DotStatDb.DataSchema}].[{tableName}] ([DF_ID],[{DbExtensions.LAST_UPDATED_COLUMN}])
SELECT @DataflowId, @CurrentDateTime;";

            var sqlParameters = new List<DbParameter>
            {
                new SqlParameter("DataflowId", SqlDbType.Int) { Value = dataFlowId },
                new SqlParameter("CurrentDateTime", SqlDbType.DateTime) { Value = dateTime }
            };

            await DotStatDb.ExecuteNonQuerySqlWithParamsAsync(sqlCommand, cancellationToken, sqlParameters.ToArray());
        }
        
    }
}
