﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using DotStat.Db.Dto;
using DotStat.Db.Util;
using DotStat.Domain;

namespace DotStat.Db.Repository
{
    public interface IComponentRepository
    {
        ValueTask<int> GetComponentDbId(IDotStatIdentifiable component, int dsdDbId, CancellationToken cancellationToken, bool errorIfNotFound = false);
        
        ValueTask<IList<ComponentItem>> GetComponentsOfDsd(Dsd dsd, CancellationToken cancellationToken);
        Task<IList<ComponentItem>> GetAllComponents(CancellationToken cancellationToken);
        
        Task<bool> CheckTimeDimensionOfDsdInDb(Dsd dsd, DbTableVersion tableVersion, CancellationToken cancellationToke);
    }
}