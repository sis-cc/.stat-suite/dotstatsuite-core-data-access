﻿using DotStat.Common.Configuration.Interfaces;
using DotStat.Common.Exceptions;
using DotStat.Common.Localization;
using DotStat.Db;
using DotStat.Db.DB;
using DotStat.Db.Dto;
using DotStat.Db.Exception;
using DotStat.Db.Repository;
using DotStat.Db.Util;
using DotStat.DB.Engine.MariaDb;
using DotStat.Domain;
using MySqlConnector;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Model.Data;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using Attribute = DotStat.Domain.Attribute;

namespace DotStat.DB.Repository.MariaDb
{
    public class MariaDbAttributeRepository : DatabaseRepositoryBase<MariaDbDotStatDb>, IAttributeRepository
    {
        public MariaDbAttributeRepository(MariaDbDotStatDb DotStatDb, IGeneralConfiguration generalConfiguration) :
            base(DotStatDb, generalConfiguration)
        {
        }

        public async Task CreateAttribute(Attribute attr, Dsd dsd, IList<ComponentItem> componentItems,
            CancellationToken cancellationToken)
        {
            var attributeBuilder = new MariaDbAttributeEngine(GeneralConfiguration);

            try
            {
                attr.DbId = await attributeBuilder.InsertToComponentTable(attr, DotStatDb, cancellationToken);
                await InsertToAttributeDimensionSetTable(attr, dsd, componentItems, cancellationToken);
            }
            catch
            {
                await attributeBuilder.CleanUp(attr, DotStatDb, cancellationToken);

                throw;
            }
        }

        public async IAsyncEnumerable<IKeyValue> GetDatasetAttributes(
            Dataflow dataflow,
            ICodeTranslator codeTranslator,
            DbTableVersion tableVersion,
            [EnumeratorCancellation] CancellationToken cancellationToken
        )
        {
            var datasetAttributes =
                dataflow.Dsd.Attributes
                    .Where(a => a.Base.AttachmentLevel == AttributeAttachmentLevel.DataSet ||
                                a.Base.AttachmentLevel == AttributeAttachmentLevel.Null).ToArray();

            if (!datasetAttributes.Any())
            {
                yield break;
            }

            var sqlQuery = BuildDatasetAttributeSqlQuery(dataflow, datasetAttributes, (char)tableVersion);
            var mySqlParameters = new DbParameter[]
                { new MySqlParameter("DfId", MySqlDbType.Int32) { Value = dataflow.DbId } };

            await using (var dr = await DotStatDb.ExecuteReaderSqlWithParamsAsync(sqlQuery, cancellationToken,
                             parameters: mySqlParameters, tryUseReadOnlyConnection: true))
            {
                while (await dr.ReadAsync(cancellationToken))
                {
                    foreach (var attribute in datasetAttributes)
                    {
                        var keyValue = await GetAttributeKeyValue(
                            attribute,
                            codeTranslator,
                            dr,
                            cancellationToken
                        );

                        if (keyValue != null)
                        {
                            yield return keyValue;
                        }
                    }
                }
            }
        }

        #region Private methods

        private async Task InsertToAttributeDimensionSetTable(Domain.Attribute attribute, Dsd dsd,
            IList<ComponentItem> dsdComponentsInDb, CancellationToken cancellationToken)
        {
            IList<string> dimensionReferences = null;

            if (attribute.Base.AttachmentLevel == AttributeAttachmentLevel.DimensionGroup)
            {
                dimensionReferences = attribute.Base.DimensionReferences;
            }
            else if (attribute.Base.AttachmentLevel == AttributeAttachmentLevel.Group)
            {
                dimensionReferences = dsd.Base.Groups.FirstOrDefault(g =>
                        g.Id.Equals(attribute.Base.AttachmentGroup, StringComparison.InvariantCultureIgnoreCase))
                    ?.DimensionRefs;
            }

            if (dimensionReferences == null)
            {
                return;
            }

            foreach (var dimensionId in dimensionReferences)
            {
                var dimension = dsd.Dimensions.FirstOrDefault(d =>
                    d.Code.Equals(dimensionId, StringComparison.InvariantCultureIgnoreCase));

                if (dimension == null)
                {
                    throw new DotStatException(string.Format(
                        LocalizationRepository.GetLocalisedResource(Localization.ResourceId.NoDimensionFound),
                        dimensionId,
                        dsd.FullId,
                        attribute.Code)
                    );
                }

                var dimensionComponent = dsdComponentsInDb.FirstOrDefault(d =>
                    d.IsDimension && d.Id.Equals(dimensionId, StringComparison.InvariantCultureIgnoreCase));

                if (dimensionComponent == null || dimensionComponent.DbId <= 0)
                {
                    throw new DimensionNotFoundByTranslatorException(string.Format(
                        LocalizationRepository.GetLocalisedResource(Localization.ResourceId
                            .NoDimensionFoundInManagementDb),
                        dimensionId,
                        dsd.FullId,
                        attribute.Code)
                    );
                }

                await DotStatDb.ExecuteNonQuerySqlWithParamsAsync(
                    $@"INSERT
                            INTO {DotStatDb.ManagementSchema}_ATTR_DIM_SET
                                 (ATTR_ID, DIM_ID)
                          VALUES (@AttributeId, @DimensionId)",
                    cancellationToken,
                    new MySqlParameter("AttributeId", MySqlDbType.Int32) { Value = attribute.DbId },
                    new MySqlParameter("DimensionId", MySqlDbType.Int32) { Value = dimensionComponent.DbId });
            }
        }


        private string BuildDatasetAttributeSqlQuery(
            Dataflow dataflow,
            IList<Attribute> datasetAttributes,
            char tableVersion)
        {
            if (!datasetAttributes.Any())
            {
                return null;
            }

            var sbSql =
                $@"SELECT {datasetAttributes.ToMariaDbColumnList()}
                     FROM {DotStatDb.DataSchema}_{dataflow.Dsd.MariaDbDsdAttrTable(tableVersion)}
                    WHERE DF_ID = @DfId
                    ";

            return sbSql;
        }

        private async ValueTask<IKeyValue> GetAttributeKeyValue(Attribute attribute, ICodeTranslator codeTranslator,
            IDataReader dr, CancellationToken cancellationToken)
        {
            var sqlColumnName = attribute.MariaDbColumn().Trim("[]".ToCharArray());
            var codelistProjection = attribute.Base.HasCodedRepresentation()
                ? await codeTranslator[attribute, cancellationToken]
                : null;

            var keyValue = GetKeyValue(sqlColumnName, codelistProjection, attribute.Base, dr);

            return keyValue;
        }

        private IKeyValue GetKeyValue(string sqlColumn, ICodelistProjection codelistProjection, IComponent component,
            IDataReader dr)
        {
            var value = dr.ColumnValue<string>(sqlColumn);

            if (string.IsNullOrWhiteSpace(value))
            {
                return null;
            }

            if (!component.HasCodedRepresentation() || codelistProjection == null)
            {
                return new KeyValueImpl(value, component.Id);
            }

            var componentCode = codelistProjection[Convert.ToInt32(value)];

            return new KeyValueImpl(componentCode, component.Id);
        }

        public Task<IReadOnlyDictionary<string, string>> GetDatasetAttributesWithInternalIds(Dataflow dataflow, DbTableVersion tableVersion, CancellationToken cancellationToken, bool applyStringDelimiterOnTextualValues = true, bool useExternalColumns = true, bool addLastUpdatedDate = true)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}