﻿using Dapper;
using DotStat.Common.Auth;
using DotStat.Common.Configuration.Interfaces;
using DotStat.Common.Model;
using DotStat.DB.Dapper;
using MySqlConnector;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;


namespace DotStat.DB.Repository.MariaDb
{
    public class MariaDbAuthorizationRepository : DapperBaseRepository<UserAuthorization, int>, IAuthorizationRepository
    {
        private IGeneralConfiguration _config;
        protected override string ConnectionString => _config.DotStatSuiteCoreCommonDbConnectionString;

        public MariaDbAuthorizationRepository(IGeneralConfiguration config)
        {
            _config = config;
        }
        
        protected override DbConnection GetConnection(bool readOnly, bool open = true)
        {
            SimpleCRUD.SetDialect(SimpleCRUD.Dialect.MySQL);
            var sqlConnectionStringBuilder = new MySqlConnectionStringBuilder()
            {
                ConnectionString = ConnectionString,
            };
            var connection = new LoggedConnection(new MySqlConnection(sqlConnectionStringBuilder.ConnectionString));
            if (open) connection.Open();
            return connection;
        }
        
        protected override async Task<DbConnection> GetConnectionAsync(bool readOnly, CancellationToken cancellationToken, bool open = true)
        {
            SimpleCRUD.SetDialect(SimpleCRUD.Dialect.MySQL);
            var sqlConnectionStringBuilder = new MySqlConnectionStringBuilder
            {
                ConnectionString = ConnectionString
            };
            var connection = new LoggedConnection(new MySqlConnection(sqlConnectionStringBuilder.ConnectionString));
            if (open) await connection.OpenAsync(cancellationToken);
            return connection;
        }
        

        public IEnumerable<UserAuthorization> GetAll()
        {
            return base.GetList();
        }

        public IEnumerable<UserAuthorization> GetByUser(DotStatPrincipal principal)
        {
            var query = new StringBuilder("where UserMask='*' or (IsGroup=0 and UserMask=@UserId)");

            if (principal.Groups.Any())
            {
                query.Append(" or (IsGroup=1 and UserMask in @Groups)");
            }

            return base.GetList(query.ToString(), 
                new
                {
                    principal.UserId,
                    principal.Groups
                }
            );
        }
    }
}