﻿using DotStat.Common.Configuration.Interfaces;
using DotStat.Db;
using DotStat.Db.DB;
using DotStat.Db.Repository;
using DotStat.Db.Util;
using DotStat.Domain;
using MySqlConnector;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Constants.InterfaceConstant;
using Org.Sdmxsource.Sdmx.Api.Model.Data;
using Org.Sdmxsource.Sdmx.Api.Model.Data.Query;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Globalization;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using DbType = DotStat.MappingStore.DbType;

namespace DotStat.DB.Repository.MariaDb
{
    public class MariaDbObservationRepository: DatabaseRepositoryBase<MariaDbDotStatDb>, IObservationRepository
    {
        private const string _BATCH_NUMBER_COLUMN = "BatchNumber";
        public MariaDbObservationRepository(MariaDbDotStatDb dotStatDb, IGeneralConfiguration generalConfiguration): 
            base(dotStatDb, generalConfiguration)
        {
        }

        public async IAsyncEnumerable<ObservationRow> GetObservations(
            IDataQuery dataQuery,
            IImportReferenceableStructure referencedStructure,
            DbTableVersion tableVersion,
            bool isDataQuery, //data or metadata
            [EnumeratorCancellation] CancellationToken cancellationToken
        )
        {
            var dsd = referencedStructure.Dsd;
            var dataFlow = referencedStructure as Dataflow;

            if (dataFlow is null && isDataQuery)
                yield break;

            //Dsd components
            var components = new List<string>();
            var measure = string.Empty;
            var dimensions = dsd.Dimensions.OrderTimeFirst().ToArray();
            var attributes = new List<string>();
            if (isDataQuery)
            {
                measure = dsd.Base.PrimaryMeasure.Id;
                attributes = dsd.Attributes.Select(a => a.Code).ToList();
                components.Add(measure);
            }
            else
            {
                attributes = dsd.Msd?.MetadataAttributes.Select(a => a.HierarchicalId).ToList();
            }

            if(attributes.Any())
                components.AddRange(attributes);

            var filterLastUpdated = dataQuery?.LastUpdatedDate is not null;

            var batchNumber = 0;
            var hasDeleteAll = false;
            
            //Include deleted records
            if (dataQuery?.IncludeHistory == true)
            {
                //Sql parameters
                var mySqlParameters = new List<DbParameter>();

                if (filterLastUpdated)
                {
                    //NOTE IDataQuery LastUpdatedDate is internally stored as UTC 
                    mySqlParameters.Add(new MySqlParameter("LastUpdated", MySqlDbType.DateTime) { Value = dataQuery.LastUpdatedDate.Date });
                };

                //check if there is at least 1 deleteAll row
                var sqlQueryLastDeletedAll = BuildLastDeletedAllSqlQuery(referencedStructure, components, tableVersion, isDataQuery, filterLastUpdated);
                var lastDeleteAllDateTime = await DotStatDb.ExecuteScalarSqlWithParamsAsync(sqlQueryLastDeletedAll, cancellationToken, parameters: mySqlParameters.ToArray());
                hasDeleteAll = lastDeleteAllDateTime != DBNull.Value && lastDeleteAllDateTime != null;
                if (hasDeleteAll)
                {
                    //No need to return all the delete rows, only one deleteAll + all current values
                    yield return new ObservationRow(batchNumber: batchNumber++, action: StagingRowActionEnum.DeleteAll, observation: null, true);
                }
                else
                {
                    var sqlQueryDelete = BuildDeletedSqlQuery(dataQuery, referencedStructure, components, tableVersion, isDataQuery, filterLastUpdated);

                    //Sql parameters
                    var sqlDeleteParameters = new List<DbParameter> {
                        new MySqlParameter("Delete", MySqlDbType.Int32) { Value = (int)StagingRowActionEnum.Delete }
                    };

                    if (filterLastUpdated)
                    {
                        //NOTE IDataQuery LastUpdatedDate is internally stored as UTC 
                        sqlDeleteParameters.Add(new MySqlParameter("LastUpdated", MySqlDbType.DateTime) { Value = dataQuery.LastUpdatedDate.Date });
                    };

                    //Delete values
                    await using var dbDataReader = await DotStatDb.ExecuteReaderSqlWithParamsAsync(sqlQueryDelete, cancellationToken, tryUseReadOnlyConnection: true, parameters: sqlDeleteParameters.ToArray());
                    while (await dbDataReader.ReadAsync(cancellationToken))
                    {
                        yield return GetObservation(
                            dsd.Base,
                            dataFlow?.Base,
                            dimensions,
                            measure,
                            attributes,
                            dbDataReader,
                            StagingRowActionEnum.Delete,
                            isDataQuery,
                            ref batchNumber
                        );
                    }

                }
            }            

            //When there is one deleteAll row, return all current rows
            filterLastUpdated = filterLastUpdated && !hasDeleteAll;
            var sqlQuery = BuildReplaceSqlQuery(dataQuery, referencedStructure, components, tableVersion, isDataQuery, filterLastUpdated);

            //Sql parameters
            var sqlReplaceParameters = new List<DbParameter> {
                        new MySqlParameter("BatchNumber", MySqlDbType.Int32) { Value = ++batchNumber }
                    };

            if (filterLastUpdated)
            {
                //NOTE IDataQuery LastUpdatedDate is internally stored as UTC 
                sqlReplaceParameters.Add(new MySqlParameter("LastUpdated", MySqlDbType.DateTime) { Value = dataQuery.LastUpdatedDate.Date });
            };

            //Replace values
            await using var dr = await DotStatDb.ExecuteReaderSqlWithParamsAsync(sqlQuery, cancellationToken, tryUseReadOnlyConnection: true, parameters: sqlReplaceParameters.ToArray());
            while (await dr.ReadAsync(cancellationToken)) {
                yield return GetObservation(
                    dsd.Base,
                    dataFlow?.Base,
                    dimensions,
                    measure,
                    attributes,
                    dr,
                    StagingRowActionEnum.Replace,
                    isDataQuery,
                    ref batchNumber
                );
            }
        }

        public async Task<long> GetObservationCount(
            Dataflow dataFlow, 
            DbTableVersion tableVersion,
            string dataFlowWhereClause,
            CancellationToken cancellationToken)
        {
            var sqlCommand = $"SELECT count(*) FROM {DotStatDb.DataSchema}_{dataFlow.Dsd.MariaDbDataDsdViewName((char)tableVersion)}\n";
            if (!string.IsNullOrEmpty(dataFlowWhereClause))
                sqlCommand += $"WHERE {dataFlowWhereClause}";

            return (long)await DotStatDb.ExecuteScalarSqlAsync(sqlCommand, cancellationToken);
        }
       
        public string BuildLastDeletedAllSqlQuery(
            IImportReferenceableStructure referencedStructure,
            IList<string> components,
            DbTableVersion tableVersion,
            bool isDataQuery,
            bool filterLastUpdated
            )
        {
            var dsd = referencedStructure.Dsd;
            var dataFlow = referencedStructure as Dataflow;

            if (dataFlow is null && isDataQuery)
                return string.Empty;

            //Dimensions
            var dimensions = dataFlow.Dsd.Dimensions.OrderTimeFirst().ToArray();
            var dimensionsColumnsWhereNull = string.Join(" AND ", dimensions.Select(d => $"{d.MariaDbColumn(true)} IS NULL"));
            var dimensionsColumnsIsWildCarded = !isDataQuery ? string.Empty :
                " OR (" + string.Join(" AND ", dimensions.Select(d => $"{d.MariaDbColumn(true)} = '{MariaDbExtensions.DimensionWildCardedDbValue}'")) + ")";

            //Components
            string componentColumnsWhereAllNull;
            string componentColumnsWhereAllPresent;
            if (components.Any())
            {
                componentColumnsWhereAllNull = string.Join(" AND ", components.Select(c => $"{c} IS NULL"));
                componentColumnsWhereAllPresent = string.Join(" AND ", components.Select(c => $"{c} IS NOT NULL"));
            }
            else
                return string.Empty;

            var lastUpdated = filterLastUpdated ? $"AND {MariaDbExtensions.LAST_UPDATED_COLUMN} >= @LastUpdated" : "";
            var viewName = $"{DotStatDb.DataSchema}_{dataFlow.MariaDbDeletedDataViewName((char)tableVersion)}";
            if (!isDataQuery)
            {
                viewName = dataFlow is null ?
                    $"{DotStatDb.DataSchema}_{dsd.MariaDbDeletedMetadataDsdViewName((char)tableVersion)}":
                    $"{DotStatDb.DataSchema}_{dataFlow.MariaDbDeletedMetadataDataflowViewName((char)tableVersion)}";
            }

            return GetLastDeletedAllSqlQuery(viewName, dimensionsColumnsWhereNull, dimensionsColumnsIsWildCarded, componentColumnsWhereAllNull, componentColumnsWhereAllPresent, lastUpdated);
        }

        private static string GetLastDeletedAllSqlQuery(
            string viewName,
            string dimensionsColumnsWhereNull,
            string dimensionsColumnsIsWildCarded,
            string componentColumnsWhereAllNull,
            string componentColumnsWhereAllPresent,
            string lastUpdated
        )
        {
            var sqlQuery = $@"SELECT MAX({MariaDbExtensions.LAST_UPDATED_COLUMN})
FROM {viewName}
WHERE (({dimensionsColumnsWhereNull}) {dimensionsColumnsIsWildCarded}) AND (
	({componentColumnsWhereAllNull}) -- All omitted
	OR ({componentColumnsWhereAllPresent} ) -- All present
) {lastUpdated};";

            return sqlQuery;
        }

        public string BuildDeletedSqlQuery(
            IDataQuery dataQuery,
            IImportReferenceableStructure referencedStructure,
            IList<string> components,
            DbTableVersion tableVersion,
            bool isDataQuery,
            bool filterLastUpdated
        )
        {
            var dsd = referencedStructure.Dsd;
            var dataFlow = referencedStructure as Dataflow;

            if (dataFlow is null && isDataQuery)
                return string.Empty;

            //Dimensions
            var dimensions = dsd.Dimensions.OrderTimeFirst().ToArray();
            var dimensionsColumns = dimensions.ToMariaDbColumnList(externalColumn: true);
            var dimensionsColumnsIsNotNull =
                string.Join(" AND ", dimensions.Select(d => $"{d.MariaDbColumn(true)} IS NOT NULL"));
            var dimensionsColumnsIsNull =
                string.Join(" OR ", dimensions.Select(d => $"{d.MariaDbColumn(true)} IS NULL"));
            var dimensionsColumnsIsWildCarded = isDataQuery
                ? " OR " + string.Join(" OR ",
                    dimensions.Select(
                        d => $"{d.MariaDbColumn(true)} = '{MariaDbExtensions.DimensionWildCardedDbValue}'"))
                : string.Empty;

            //Components
            var componentColumns = string.Empty;
            if (components.Any())
            {
                componentColumns = ", " + string.Join(", ", components.Select(c => $"`{c}`"));
            }
            else
                return string.Empty;

            var viewName = $"{DotStatDb.DataSchema}_{dataFlow.MariaDbDeletedDataViewName((char)tableVersion)}";
            if (!isDataQuery)
            {
                viewName = dataFlow is null
                    ? $"{DotStatDb.DataSchema}_{dsd.MariaDbDeletedMetadataDsdViewName((char)tableVersion)}"
                    : $"{DotStatDb.DataSchema}_{dataFlow.MariaDbDeletedMetadataDataflowViewName((char)tableVersion)}";
            }

            var filteredQuery = Predicate.BuildWhereForObservationLevel(dataQuery, dataFlow, codeTranslator: null,
                filterLastUpdated,
                nullTreatment: isDataQuery ? NullTreatment.IncludeNulls : NullTreatment.IncludeNullsAndSwitchOff,
                serverType: DbType.MariaDb);
            if (!string.IsNullOrEmpty(filteredQuery))
                filteredQuery = $"AND {filteredQuery}";

            return GetDeletedSqlQuery(viewName, dimensionsColumns, componentColumns, dimensionsColumnsIsNull,
                dimensionsColumnsIsNotNull, dimensionsColumnsIsWildCarded, filteredQuery);
        }

        private static string GetDeletedSqlQuery(
            string viewName,
            string dimensionsColumns,
            string componentColumns,
            string dimensionsColumnsIsNull,
            string dimensionsColumnsIsNotNull,
            string dimensionsColumnsIsWildCarded,
            string filteredQuery
        )
        {
            //At least one dimension is null or wildcarded MariaDbExtensions.DimensionWildCardedDbValue (metadata only) therefore each row should have a different BatchNumber
            //Start BatchNumber in 1 and each wildcarded row has a different BatchNumber
            var sqlWildCarded = $@"WITH WILDCARDED AS(
SELECT {dimensionsColumns}{componentColumns}
, CAST((ROW_NUMBER() OVER(ORDER BY `{MariaDbExtensions.LAST_UPDATED_COLUMN}`)) AS INT) AS `{ _BATCH_NUMBER_COLUMN}`
FROM {viewName}
WHERE({dimensionsColumnsIsNull}{dimensionsColumnsIsWildCarded})
{filteredQuery}
) ";
            //All dimension must be present and each duplicated key should have a different BatchNumber
            //Start BatchNumber in 1 or the Max value found in the WILDCARDED
            var sqlNonWildCarded = $@"SELECT {dimensionsColumns}{componentColumns}
,CAST(IFNULL((SELECT MAX(`{_BATCH_NUMBER_COLUMN}`) FROM WILDCARDED), 0) + ROW_NUMBER() OVER (PARTITION BY {dimensionsColumns} ORDER BY {MariaDbExtensions.LAST_UPDATED_COLUMN}) AS INT) AS `{_BATCH_NUMBER_COLUMN}`
FROM {viewName}
WHERE ({dimensionsColumnsIsNotNull})
{filteredQuery}";


            var sqlQuery = $@"{sqlWildCarded}
{sqlNonWildCarded}
UNION ALL
SELECT {dimensionsColumns}{componentColumns},{_BATCH_NUMBER_COLUMN}
FROM WILDCARDED
ORDER BY `{_BATCH_NUMBER_COLUMN}`;";

            return sqlQuery;
        }

        public string BuildReplaceSqlQuery(
            IDataQuery dataQuery,
            IImportReferenceableStructure referencedStructure,
            IList<string> components,
            DbTableVersion tableVersion,
            bool isDataQuery,
            bool filterLastUpdated)
        {
            return isDataQuery
                ? BuildDataReplaceSqlQuery(dataQuery, referencedStructure as Dataflow, components, tableVersion, filterLastUpdated):
                BuildMetaReplaceSqlQuery(dataQuery, referencedStructure, components, tableVersion);
        }

        private string BuildMetaReplaceSqlQuery(
            IDataQuery dataQuery,
            IImportReferenceableStructure referencedStructure,
            IList<string> components,
            DbTableVersion tableVersion)
        {
            var sqlQuery = string.Empty;
            var dsd = referencedStructure.Dsd;
            var dataFlow = referencedStructure as Dataflow;

            //Dimensions
            var dimensionsColumns = dsd.Dimensions.OrderTimeFirst().ToArray().ToMariaDbColumnList(externalColumn: true);

            //Components
            var componentsColumns = string.Empty;
            if (components.Any())
            {
                componentsColumns = ", " + string.Join(", ", components.Select(c => $"`{c}`"));
            }
            else
                return sqlQuery;

            var wherePart = Predicate.BuildWhereForObservationLevel(dataQuery, dataFlow, codeTranslator: null, filterLastUpdated: true, nullTreatment: NullTreatment.IncludeNullsAndSwitchOff, serverType: DbType.MariaDb);

            //Transfer Ref. Metadata attached to DSD
            if (dataFlow is null)
            {
                var viewName = $"{DotStatDb.DataSchema}_{dsd.MariaDbMetaDataDsdViewName((char)tableVersion)}";

                sqlQuery =
                    $"SELECT {dimensionsColumns}{componentsColumns}, @BatchNumber as {_BATCH_NUMBER_COLUMN}\nFROM {viewName}";

                if (!string.IsNullOrEmpty(wherePart))
                    sqlQuery += $"\nWHERE {wherePart}";

                return sqlQuery;
            }

            //Transfer Ref. Metadata attached to DataFlow
            var dimensions = dsd.Dimensions.Where(dim => !dim.Base.TimeDimension).ToList();

            // SELECT ----------------------------------------------------------------

            var dfSelectStatement = new StringBuilder();
            var dsSelectStatement = new StringBuilder();

            //dimensions
            dfSelectStatement.Append(string.Join(", ", dimensions.Select(d =>
                d.Base.HasCodedRepresentation() ?
                    $"CASE WHEN CL_{d.Code}.ID IS NULL THEN '{MariaDbExtensions.DimensionSwitchedOff}' ELSE CL_{d.Code}.ID END AS {d.Code}"
                    : $"CASE WHEN ME.{d.MariaDbColumn()} IS NULL THEN '{MariaDbExtensions.DimensionSwitchedOff}' ELSE ME.{d.MariaDbColumn()} END AS {d.Code}")));

            dsSelectStatement.Append(string.Join(", ", dimensions.Select(d => $"'{MariaDbExtensions.DimensionSwitchedOff}' AS {d.Code}")));
            
            //time dimension
            var timeDim = dataFlow.Dsd.TimeDimension;
            var (sqlMin, sqlMax) = dataFlow.Dsd.Base.HasSupportDateTimeAnnotation()
                ? (DateTime.MinValue.ToString("yyyy-MM-dd HH:mm:ss"), DateTime.MaxValue.ToString("yyyy-MM-dd HH:mm:ss"))
                : (DateTime.MinValue.ToString("yyyy-MM-dd"), DateTime.MaxValue.ToString("yyyy-MM-dd"));

            if (timeDim != null)
            {
                dfSelectStatement
                    .Append(",")
                .Append(string.Join(",",
                        $"CASE WHEN {timeDim.MariaDbColumn()} IS NULL THEN '{MariaDbExtensions.DimensionSwitchedOff}' ELSE {timeDim.MariaDbColumn()} END AS {dataFlow.Dsd.Base.TimeDimension.Id}",
                        MariaDbExtensions.MariaDbPeriodStart(),
                        MariaDbExtensions.MariaDbPeriodEnd()
                    ));

                dsSelectStatement
                    .Append(", ")
                    .Append(string.Join(", ",
                        $"'{MariaDbExtensions.DimensionSwitchedOff}' AS {dataFlow.Dsd.Base.TimeDimension.Id}",
                        $"'{sqlMax}' AS {MariaDbExtensions.MariaDbPeriodStart()}",
                        $"'{sqlMin}' AS {MariaDbExtensions.MariaDbPeriodEnd()}"
                    ));
            }

            //metadata attributes
            if (dataFlow.Dsd.Msd.MetadataAttributes.Any())
            {
                dfSelectStatement
                    .Append(",")
                    .Append(string.Join(", ", dataFlow.Dsd.Msd.MetadataAttributes.Select(a => $"{a.MariaDbColumn()} AS {a.HierarchicalId}"
                    )));

                dsSelectStatement
                    .Append(',')
                    .Append(string.Join(", ", dataFlow.Dsd.Msd.MetadataAttributes.Select(a => $"ME.{a.MariaDbColumn()} AS {a.HierarchicalId}"
                )));
            }

            // LAST_UPDATED
            var updatedAfterColumns = $", {MariaDbExtensions.LAST_UPDATED_COLUMN}";

            // JOIN ------------------------------------------------------------------
            var joinStatement = new StringBuilder();

            //dimensions
            joinStatement.AppendLine(string.Join("\n", dimensions.Where(d => d.Base.HasCodedRepresentation()).Select(d =>
                    $"LEFT JOIN {DotStatDb.ManagementSchema}_CL_{d.Codelist.DbId} AS CL_{d.Code} ON ME.{d.MariaDbColumn()} = CL_{d.Code}.ITEM_ID"
                )
            ));

            sqlQuery = $@"SELECT {dimensionsColumns}{componentsColumns}, @BatchNumber as {_BATCH_NUMBER_COLUMN}
FROM (
    SELECT {dsSelectStatement}{updatedAfterColumns}
    FROM {DotStatDb.DataSchema}_{dataFlow.Dsd.MariaDbMetadataDataSetTable((char)tableVersion)} ME
    WHERE DF_ID = {dataFlow.DbId}
    UNION ALL 
    SELECT {dfSelectStatement}{updatedAfterColumns}
    FROM {DotStatDb.DataSchema}_{dataFlow.MariaDbMetadataDataflowTable((char)tableVersion)} ME
    {joinStatement}
) DF_META";
            if (!string.IsNullOrEmpty(wherePart))
                sqlQuery += $"\nWHERE {wherePart}";

            return sqlQuery;
        }

        private string BuildDataReplaceSqlQuery(
            IDataQuery dataQuery,
            Dataflow dataFlow,
            IList<string> components,
            DbTableVersion tableVersion,
            bool filterLastUpdated)
        {
            if(dataFlow is null)
                return string.Empty;

            var dsd = dataFlow.Dsd;
            //Dimensions
            var dimensions = dataFlow.Dimensions.OrderTimeFirst().ToArray();
            var dimensionsColumns = dimensions.ToMariaDbColumnList(externalColumn: true);

            //Components
            var componentsColumns = string.Empty;
            if (components.Any())
            {
                componentsColumns = ", " + string.Join(", ", components.Select(c => $"{c}"));
            }
            else
                return string.Empty;

            //Last updated
            var lastUpdatedFilter = (filterLastUpdated && dataQuery?.LastUpdatedDate is not null) ?
                $"WHERE {MariaDbExtensions.LAST_UPDATED_COLUMN} >= @LastUpdated" : "";
            
            //DATA QUERY
            //Simple query with no updatedAfter filtering
            if (!filterLastUpdated || dataQuery?.LastUpdatedDate is null)
            {
                var viewName = $"{DotStatDb.DataSchema}_{dataFlow.MariaDbDataflowViewName((char)tableVersion)}";
                var sqlQuery = $"SELECT {dimensionsColumns}{componentsColumns}, @BatchNumber as {_BATCH_NUMBER_COLUMN}\nFROM {viewName}";

                var wherePart = Predicate.BuildWhereForObservationLevel(dataQuery, dataFlow, codeTranslator: null, filterLastUpdated: false, nullTreatment: NullTreatment.ExcludeNulls, serverType: DbType.MariaDb);
                if (!string.IsNullOrEmpty(wherePart))
                    sqlQuery += $"\nWHERE {wherePart}";

                return sqlQuery;
            }

            //Complex query with updatedAfter filtering
            //Dimensions
            var nonTimedimensions = dataFlow.Dsd.Dimensions.Where(dim => !dim.Base.TimeDimension).ToList();

            var dimGroupAttributes = dataFlow.Dsd.Attributes.Where(attr =>
                (attr.Base.AttachmentLevel == AttributeAttachmentLevel.DimensionGroup || attr.Base.AttachmentLevel == AttributeAttachmentLevel.Group) &&
                !attr.Base.GetDimensionReferences().Contains(DimensionObject.TimeDimensionFixedId)).ToList();

            var hasDimGroupLevelAttributes = dimGroupAttributes.Any();

            var dimensionsJoin = string.Join("\n", nonTimedimensions.Where(d => d.Base.HasCodedRepresentation()).Select(d => hasDimGroupLevelAttributes 
                    ? $"LEFT JOIN {DotStatDb.ManagementSchema}_CL_{d.Codelist.DbId} AS CL_{d.Code} ON COALESCE({d.MariaDbColumn(suffix:"_OBS")},{d.MariaDbColumn(suffix:"_SRS")}) = CL_{d.Code}.ITEM_ID" 
                    : $"LEFT JOIN {DotStatDb.ManagementSchema}_CL_{d.Codelist.DbId} AS CL_{d.Code} ON {d.MariaDbColumn(suffix: "_OBS")} = CL_{d.Code}.ITEM_ID"
                ));

            var dimensionsSelectInner = string.Join(", ", nonTimedimensions.Select(d =>
                d.Base.HasCodedRepresentation() ? 
                    $"CL_{d.Code}.ID AS {d.Code}" : 
                    hasDimGroupLevelAttributes ?
                        $"COALESCE({d.MariaDbColumn(suffix: "_OBS")},{d.MariaDbColumn(suffix: "_SRS")}) AS {d.Code}" :
                        $"{d.MariaDbColumn(suffix: "_OBS")} AS {d.Code}"));
            var dimensionsSelectObsCte = string.Join(", ", nonTimedimensions.Select(d => $"FI.{d.MariaDbColumn()} AS {d.MariaDbColumn(suffix:"_OBS")}")) + ',';
            var dimensionsSelectSeriesCte = hasDimGroupLevelAttributes
                ? string.Join(", ",
                    nonTimedimensions.Select(d => $"FI.{d.MariaDbColumn()} AS {d.MariaDbColumn(suffix: "_SRS")}")) + ','
                : string.Empty;

            //time dimension
            var timeDim = dataFlow.Dsd.TimeDimension;
            var timeDimSelectObsCte = string.Empty;
            var timeDimSelect = string.Empty;
            if (timeDim != null)
            {
                timeDimSelectObsCte = string.Join(", ",
                        $"FA.{timeDim.MariaDbColumn()} AS {dataFlow.Dsd.Base.TimeDimension.Id}",
                        MariaDbExtensions.MariaDbPeriodStart(),
                        MariaDbExtensions.MariaDbPeriodEnd()) + ',';

                timeDimSelect = string.Join(", ",
                    $"{dataFlow.Dsd.Base.TimeDimension.Id}",
                    MariaDbExtensions.MariaDbPeriodStart(),
                    MariaDbExtensions.MariaDbPeriodEnd()) + ',';
            }

            //Measure
            var measureColumnSelect = $" FA.VALUE AS {dataFlow.Dsd.Base.PrimaryMeasure.Id}";

            //Attributes
            //Obs level
            var observationAttributes = dataFlow.Dsd.Attributes
                .Where(attr => attr.Base.AttachmentLevel == AttributeAttachmentLevel.Observation ||
                                (attr.Base.AttachmentLevel == AttributeAttachmentLevel.DimensionGroup || attr.Base.AttachmentLevel == AttributeAttachmentLevel.Group) &&
                                attr.Base.GetDimensionReferences().Contains(DimensionObject.TimeDimensionFixedId)).ToList();

            var obsLvlAttrSelect = string.Empty;
            var obsLvlAttrJoin = string.Empty;
            if (observationAttributes.Any())
            {
                obsLvlAttrSelect = ", " + string.Join(", ", observationAttributes.Select(a => a.Base.HasCodedRepresentation()
                        ? $"CL_{a.Code}.ID AS {a.Code}"
                        : $"FA.{a.MariaDbColumn()} AS {a.Code}"));

                obsLvlAttrJoin = string.Join("\n", observationAttributes.Where(a => a.Base.HasCodedRepresentation()).Select(a =>
                            $"LEFT JOIN {DotStatDb.ManagementSchema}_CL_{a.Codelist.DbId} AS CL_{a.Code} ON FA.{a.MariaDbColumn()} = CL_{a.Code}.ITEM_ID"));
            }

            //dimGroup level
            var dimGroupLvlAttrSelect = string.Empty;
            var dimGroupLvlAttrJoin = string.Empty;
            if (hasDimGroupLevelAttributes)
            {
                dimGroupLvlAttrSelect = string.Join(", ", dimGroupAttributes.Select(a => a.Base.HasCodedRepresentation()
                        ? $"CL_{a.Code}.ID AS {a.Code}"
                        : $"ATTR.{a.MariaDbColumn()} AS {a.Code}"));

                dimGroupLvlAttrJoin = string.Join("\n", dimGroupAttributes.Where(a => a.Base.HasCodedRepresentation()).Select(a =>
                            $"LEFT JOIN {DotStatDb.ManagementSchema}_CL_{a.Codelist.DbId} AS CL_{a.Code} ON ATTR.{a.MariaDbColumn()} = CL_{a.Code}.ITEM_ID"));
            }

            //dataSet level
            var dataSetAttributes = dataFlow.Dsd.Attributes
                .Where(attr => attr.Base.AttachmentLevel == AttributeAttachmentLevel.DataSet).ToList();

            var dataSetAttrLvlSelect = string.Empty;
            var dataSetAttrLvlJoin = string.Empty;
            var hasDataSetLevelAttributes = false;
            if (dataSetAttributes.Any())
            {
                hasDataSetLevelAttributes = true;
                dataSetAttrLvlSelect = string.Join(", ", dataSetAttributes.Select(a => a.Base.HasCodedRepresentation()
                    ? $"CL_{a.Code}.ID AS {a.Code}"
                    : $"ADF.{a.MariaDbColumn()} AS {a.Code}"));

                dataSetAttrLvlJoin = string.Join("\n", dataSetAttributes.Where(a => a.Base.HasCodedRepresentation()).Select(a =>
                            $"LEFT JOIN {DotStatDb.ManagementSchema}_CL_{a.Codelist.DbId} AS CL_{a.Code} ON ADF.{a.MariaDbColumn()} = CL_{a.Code}.ITEM_ID"));
            }

            //Tables
            var filterTable = $"{DotStatDb.DataSchema}_{dataFlow.Dsd.MariaDbFilterTable("FI")}";

            var ctePart =
                    $@"WITH OBS_LEVEL AS (
	SELECT FI.SID, {dimensionsSelectObsCte}{timeDimSelectObsCte}
        {measureColumnSelect}
        {obsLvlAttrSelect}
	 FROM {DotStatDb.DataSchema}_{dataFlow.Dsd.MariaDbFactTable((char)tableVersion, "FA")}
	 LEFT JOIN {filterTable} ON FI.SID = FA.SID
	 {obsLvlAttrJoin}
	 {lastUpdatedFilter}
 )";

            var innerSubQueryPartLeft = $@"SELECT OL.SID,{timeDimSelect}{dimensionsSelectInner}{componentsColumns}
FROM OBS_LEVEL OL";
            var innerSubQueryPartRight = $@"SELECT SL.SL_SID as SID,{timeDimSelect}{dimensionsSelectInner}{componentsColumns}
FROM OBS_LEVEL OL";

            if (hasDimGroupLevelAttributes)
            {
                ctePart += $@", SERIES_LEVEL AS (
SELECT FI.SID AS SL_SID, {dimensionsSelectSeriesCte}{dimGroupLvlAttrSelect}
FROM {DotStatDb.DataSchema}_{dataFlow.Dsd.MariaDbDimGroupAttrTable((char)tableVersion, "ATTR")}
LEFT JOIN {filterTable} ON FI.SID = ATTR.SID
{dimGroupLvlAttrJoin}
{lastUpdatedFilter}
)";
                innerSubQueryPartLeft += "\nLEFT JOIN SERIES_LEVEL SL ON OL.SID=SL_SID";
                innerSubQueryPartRight += "\nRIGHT JOIN SERIES_LEVEL SL ON OL.SID=SL_SID";
            }

            if (hasDataSetLevelAttributes)
            {
                ctePart += $@", DF_LEVEL AS (
SELECT DF_ID, {dataSetAttrLvlSelect}
FROM {DotStatDb.DataSchema}_{dataFlow.Dsd.MariaDbDsdAttrTable((char)tableVersion, "ADF")}
{dataSetAttrLvlJoin}
WHERE ADF.DF_ID = {dataFlow.DbId} {lastUpdatedFilter.Replace("WHERE", "AND")}
)";
                innerSubQueryPartLeft += $"\nLEFT JOIN DF_LEVEL DF ON DF_ID = {dataFlow.DbId}";
                innerSubQueryPartRight += $"\nLEFT JOIN DF_LEVEL DF ON DF_ID = {dataFlow.DbId}";
            }

            @ctePart += $@", INNER_QUERY AS (
{innerSubQueryPartLeft}
{dimensionsJoin}";
            if (hasDimGroupLevelAttributes)
            {
                ctePart += $@"
UNION ALL
{innerSubQueryPartRight}
{dimensionsJoin}
WHERE OL.SID IS NULL
)"; // Only the rows not in the first query selected so UNION ALL can be used
            }
            else
                ctePart += ")";
            var query = 
$@"{ctePart}
SELECT {dimensionsColumns}{componentsColumns}, @BatchNumber as {_BATCH_NUMBER_COLUMN}
FROM INNER_QUERY";

            var where = Predicate.BuildWhereForObservationLevel(dataQuery, dataFlow, codeTranslator: null, filterLastUpdated: false, nullTreatment: NullTreatment.IncludeNulls, serverType: DbType.MariaDb);
            if (!string.IsNullOrEmpty(where))
                query += $"\nWHERE {where}";

            if (hasDataSetLevelAttributes)
            {
                var componentsColumnsDf = ", " + string.Join(", ", components.Select(c => dataSetAttributes.Any(a => a.Code.Equals(c, StringComparison.InvariantCultureIgnoreCase)) ? $"DF.{c}" : $"{c}"));

                query +=
                    $@"
UNION ALL
SELECT {dimensionsColumns}{componentsColumnsDf}, @BatchNumber as {_BATCH_NUMBER_COLUMN}
FROM INNER_QUERY
RIGHT JOIN DF_LEVEL DF ON DF.DF_ID = {dataFlow.DbId}
WHERE INNER_QUERY.SID IS NULL"; // Only the row(s) not in INNER_QUERY selected so UNION ALL can be used

                if (!string.IsNullOrEmpty(where))
                    query += $"\nAND {where}";
            }

            return query;
        }

        // expected order from DB: [time dimension], [dimensions], [value], [attributes]
        private static ObservationRow GetObservation(
            IDataStructureObject dataStructureObject,
            IDataflowObject dataFlowObject,
            IList<Dimension> dimensions,
            string measure,
            IList<string> attributes,
            DbDataReader dr,
            StagingRowActionEnum action,
            bool isDataQuery,
            ref int batchNumber
            )
        {
            var index = 0;
            string time = null;
            var isWildCarded = false;

            if (dimensions[0].Base.TimeDimension)
            {
                var o = dr[index++];

                if (!isDataQuery)
                {
                    if (o != DBNull.Value && o != null)
                    {
                        time = (string)o;
                    }
                    else
                    {
                        isWildCarded = true;
                        time = MariaDbExtensions.DimensionSwitchedOff;
                    }
                }
                else
                {
                    if (o != DBNull.Value && o != null)
                    {
                        time = (string)o;
                    }
                    else
                    {
                        isWildCarded = true;
                    }
                }
            }

            var seriesKey = new List<IKeyValue>(dimensions.Count - index);

            for (var i = index; i < dimensions.Count; i++)
            {
                var o = dr[index++];

                if (!isDataQuery)
                {
                    if (o == DBNull.Value || o == null)
                    {
                        o = MariaDbExtensions.DimensionSwitchedOff;
                        isWildCarded = true;
                    }
                }
                else
                {
                    if (o == DBNull.Value || o == null)
                    {
                        isWildCarded = true;
                        continue;
                    }
                }

                var dim = dimensions[i];
                var dmCode = (string)o;
                seriesKey.Add(new KeyValueImpl(dmCode, dim.Code));
            }

            string value = null;
            if (!string.IsNullOrEmpty(measure))
            {
                var obj = dr[index++];
                value = (obj == DBNull.Value || obj == null) ? null : Convert.ToString(obj, CultureInfo.InvariantCulture);
            }
            var attrList = new List<IKeyValue>();

            // attributes ----------------------------

            foreach (var attr in attributes)
            {
                var o = dr[index++];
                var attrValue = (o == DBNull.Value || o == null) ? null : Convert.ToString(o, CultureInfo.InvariantCulture);
                attrList.Add(new KeyValueImpl(attrValue, attr));
            }

            batchNumber = dr.GetInt32(index++);

            // ---------------------------------------

            return new ObservationRow(
                batchNumber,
                action,
                new ObservationImpl(new KeyableImpl(dataFlowObject, dataStructureObject, seriesKey, null),
                    time,
                    value,
                    attrList,
                    crossSectionValue: null),
                isWildCarded);
        }

    }
}