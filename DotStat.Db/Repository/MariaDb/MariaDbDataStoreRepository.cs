﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using DotStat.Common.Configuration.Interfaces;
using DotStat.Common.Localization;
using DotStat.Common.Logger;
using DotStat.Db;
using DotStat.Db.DB;
using DotStat.Db.Exception;
using DotStat.Db.Reader;
using DotStat.Db.Repository;
using DotStat.Db.Util;
using DotStat.DB.Util;
using DotStat.Db.Validation;
using DotStat.Domain;
using MySqlConnector;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Constants.InterfaceConstant;
using Org.Sdmxsource.Sdmx.Api.Model.Data;
using Attribute = DotStat.Domain.Attribute;
using MySqlDbType = MySqlConnector.MySqlDbType;
using MySqlParameter = MySqlConnector.MySqlParameter;

namespace DotStat.DB.Repository.MariaDb
{
    public class MariaDbDataStoreRepository : MariaDbDataMergerBase, IDataStoreRepository
    {
        public MariaDbDataStoreRepository(MariaDbDotStatDb DotStatDb, IGeneralConfiguration generalConfiguration) :
            base(DotStatDb, generalConfiguration)
        {
        }

        public async Task<BulkImportResult> BulkInsertData(IAsyncEnumerable<ObservationRow> observations,
            ReportedComponents reportedComponents,
            CodeTranslator translator,
            Dataflow dataflow,
            bool fullValidation,
            bool isTimeAtTimeDimensionSupported,
            bool isXMLSource,
            CancellationToken cancellationToken)
        {
            const int batchSize = 30000;
            int rowsCopied = 0;
            int batchNr = 0;
            List<IValidationError> allErrors = new List<IValidationError>();
            Dictionary<int, DataSetAttributeRow> dataSetAttributeRows = new Dictionary<int, DataSetAttributeRow>();
            Dictionary<int, BatchAction> allBatchActions = new Dictionary<int, BatchAction>();

            await using (var connection = DotStatDb.GetConnectionAllowLoadLocal())
            {
                var bulkCopy = new MySqlBulkCopy(connection, null);

                bulkCopy.DestinationTableName = $"{DotStatDb.DataSchema}_{dataflow.Dsd.MariaDbStagingTable()}";
                bulkCopy.BulkCopyTimeout = DotStatDb.DatabaseCommandTimeout;

                await foreach (var batch in observations.Buffer(batchSize, cancellationToken))
                {
                    batchNr++;
                    var observationReader = new SdmxObservationReader(batch.ToAsyncEnumerable(), reportedComponents,
                        dataflow, translator, GeneralConfiguration, fullValidation, isTimeAtTimeDimensionSupported,
                        isXMLSource);

                    //Add previously processed DataSetAttributes
                    observationReader.DataSetAttributes = dataSetAttributeRows;

                    await bulkCopy.WriteToServerAsync(observationReader, cancellationToken);

                    //Get errors from batch
                    var errors = observationReader.GetErrors();
                    if (errors != null && errors.Any())
                    {
                        allErrors.AddRange(errors);
                    }

                    rowsCopied += observationReader.GetObservationsCount();
                    //Merge current batch actions with result
                    foreach (var batchAction in observationReader.GetBatchActions())
                    {
                        allBatchActions.TryAdd(batchAction.Key, batchAction.Value);
                    }

                    dataSetAttributeRows = observationReader.DataSetAttributes;

                    //Log status
                    OnSqlRowsCopied(rowsCopied, batchNr);
                }

                Log.Notice(string.Format(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.ReadingSourceFinished),
                    rowsCopied));

                var allDatasetMetadataAttributesRows = dataSetAttributeRows.Select(d => d.Value).ToList();

                return new BulkImportResult(allDatasetMetadataAttributesRows, allErrors, rowsCopied, allBatchActions);
            }
        }

        public async Task DropStagingTables(Dsd dsd, CancellationToken cancellationToken)
        {
            var tableName = dsd.MariaDbStagingTable();
            await DotStatDb.DropTable(DotStatDb.DataSchema, tableName, cancellationToken);
        }

        public async Task DropStagingTables(int dsdDbId, CancellationToken cancellationToken)
        {
            var tableName = MariaDbExtensions.MariaDbStagingTable(dsdDbId);
            await DotStatDb.DropTable(DotStatDb.DataSchema, tableName, cancellationToken);
        }

        public async Task RecreateStagingTables(Dsd dsd, ReportedComponents reportedComponents, bool isTimeAtTimeDimensionSupported, CancellationToken cancellationToken)
        {
            await DropStagingTables(dsd, cancellationToken);
            await BuildStagingTables(dsd, reportedComponents, isTimeAtTimeDimensionSupported, cancellationToken);
        }

        public async Task AddIndexStagingTable(Dsd dsd, ReportedComponents reportedComponents, CancellationToken cancellationToken)
        {
            //No index required for stating table; Request with only dataflow level attributes
            if (!reportedComponents.IsPrimaryMeasureReported && reportedComponents.ObservationAttributes.Count == 0 && reportedComponents.SeriesAttributesWithNoTimeDim.Count == 0)
                return;

            var dimensionColumns = reportedComponents.Dimensions.Where(d => !d.Base.TimeDimension).ToMariaDbColumnList(false, false, false, true);
            var timeDimColumns = reportedComponents.TimeDimension is not null
                ? string.Join(",", reportedComponents.TimeDimension, MariaDbExtensions.MariaDbPeriodStart(), MariaDbExtensions.MariaDbPeriodEnd()) : null;

            var indexColumns = "ROW_ID";
            var indexName = "idx_ROW_ID";
            //32 is the SQL limit of columns that can participate in a Unique index
            if (reportedComponents.Dimensions.Count + 1 <= 32)
            {
                indexColumns = $"{dimensionColumns}, BATCH_NUMBER";
                indexName = "idx_dimensions";
            }

            var obsAttrColumns = reportedComponents.ObservationAttributes.Any()
                ? "," + reportedComponents.ObservationAttributes.ToMariaDbColumnList() : null;
            var measure = reportedComponents.IsPrimaryMeasureReported ? ", VALUE" : null;
            var actionColumns = ", REAL_ACTION_FACT, REAL_ACTION_ATTR";

            await DotStatDb.ExecuteNonQuerySqlAsync($@"
                CREATE INDEX {indexName} ON {DotStatDb.DataSchema}_{dsd.MariaDbStagingTable()}({indexColumns})", cancellationToken);
        }

        public async Task AddUniqueIndexStagingTable(Dsd dsd, ReportedComponents reportedComponents, CancellationToken cancellationToken)
        {
            var dimensionColumns = reportedComponents.Dimensions.ToMariaDbColumnList();

            var indexColumns = "ROW_ID";
            var indexName = "UI_ROW_ID";
            //32 is the SQL limit of columns that can participate in a Unique index
            if (reportedComponents.Dimensions.Count + 1 <= 32)
            {
                indexColumns = $"{dimensionColumns}, BATCH_NUMBER";
                indexName = "UI_dimensions";
            }

            try
            {
                await DotStatDb.ExecuteNonQuerySqlAsync($@"
                    CREATE UNIQUE INDEX {indexName} 
                    ON {DotStatDb.DataSchema}_{dsd.MariaDbStagingTable()}({indexColumns})
                ", cancellationToken);

            }
            catch (MySqlException e)
            {
                switch (e.Number)
                {
                    case 1505://duplicates in the staging table insert
                        throw new ConsumerValidationException(new List<ValidationError> { new ObservationError() { Type = ValidationErrorType.DuplicatedRowsInStagingTable } });
                    default:
                        throw;
                }
            }
        }

        public async Task DeleteData(IImportReferenceableStructure referencedStructure, DbTableVersion tableVersion,
            CancellationToken cancellationToken)
        {
            if (referencedStructure is not Dsd dsd)
            {
                //Dsd level delete not required for data
                return;
            }

            if (DotStatDb.DataSpace.KeepHistory)
            {
                //Temporal tables do not allow Truncate commands
                await DeleteAllFromDsdTables(dsd, tableVersion, cancellationToken);
                await DeleteAllFromAttributeTables(dsd, tableVersion, cancellationToken);
            }
            else
            {
                await TruncateDsdTables(dsd, tableVersion, cancellationToken);
                await TruncateAttributeTables(dsd, tableVersion, cancellationToken);
            }
        }

        public async Task CopyDataToNewVersion(Dsd dsd, DbTableVersion sourceDbTableVersion, DbTableVersion targetDbTableVersion, CancellationToken cancellationToken)
        {
            if (sourceDbTableVersion == targetDbTableVersion)
                throw new ArgumentException($"The same value was provided for the parameters 'sourceDbTableVersion' and 'targetDbTableVersion'");

            var timeDim = dsd.TimeDimension;
            var timeDimColumns = timeDim != null
                ? "," + string.Join(",", timeDim.MariaDbColumn(), MariaDbExtensions.MariaDbPeriodStart(), MariaDbExtensions.MariaDbPeriodEnd())
                : null;

            var obsAttributes = dsd.Attributes
                .Where(a => a.Base.AttachmentLevel == AttributeAttachmentLevel.Observation ||
                            (a.Base.AttachmentLevel == AttributeAttachmentLevel.DimensionGroup || a.Base.AttachmentLevel == AttributeAttachmentLevel.Group) &&
                            a.Base.GetDimensionReferences().Contains(DimensionObject.TimeDimensionFixedId))
                .ToArray();

            var attributeColumns = obsAttributes.Any()
                ? "," + obsAttributes.ToMariaDbColumnList()
                : null;

            await DotStatDb.ExecuteNonQuerySqlAsync(
                $@" INSERT
                      INTO {DotStatDb.DataSchema}_{dsd.MariaDbFactTable((char)targetDbTableVersion)} 
                      (SID, `VALUE`, {MariaDbExtensions.LAST_UPDATED_COLUMN} {timeDimColumns} {attributeColumns} )
                    SELECT SID, `VALUE`, {MariaDbExtensions.LAST_UPDATED_COLUMN} {timeDimColumns} {attributeColumns} 
                      FROM {DotStatDb.DataSchema}_{dsd.MariaDbFactTable((char)sourceDbTableVersion)}"
                      , cancellationToken
            );
        }

        public async Task UpdateStatisticsOfFactTable(Dsd dsd, DbTableVersion tableVersion, CancellationToken cancellationToken)
        {
            await DotStatDb.ExecuteNonQuerySqlAsync(
                $@"OPTIMIZE TABLE {DotStatDb.DataSchema}_{dsd.MariaDbFactTable((char)tableVersion)}",
                cancellationToken
            );
        }

        public async Task UpdateStatisticsOfFilterTable(Dsd dsd, CancellationToken cancellationToken)
        {
            await DotStatDb.ExecuteNonQuerySqlAsync(
                $@"OPTIMIZE TABLE {DotStatDb.DataSchema}_{dsd.MariaDbFilterTable()}",
                cancellationToken
            );
        }

        public async ValueTask CopyAttributesToNewVersion(Dsd dsd, DbTableVersion sourceDbTableVersion, DbTableVersion targetDbTableVersion, CancellationToken cancellationToken)
        {
            if (sourceDbTableVersion == targetDbTableVersion)
                throw new ArgumentException($"The same value was provided for the parameters 'sourceDbTableVersion' and 'targetDbTableVersion'");

            if (!dsd.Attributes.Any())
                return;

            var dimGroupAttributes = dsd.Attributes.Where(a =>
                (a.Base.AttachmentLevel == AttributeAttachmentLevel.DimensionGroup ||
                a.Base.AttachmentLevel == AttributeAttachmentLevel.Group) &&
                !a.Base.GetDimensionReferences().Contains(DimensionObject.TimeDimensionFixedId)
            ).ToArray();

            if (dimGroupAttributes.Any())
            {
                var dimGroupAttributeColumns = dimGroupAttributes.ToMariaDbColumnList();

                await DotStatDb.ExecuteNonQuerySqlAsync(
                    $@" INSERT
                    INTO {DotStatDb.DataSchema}_{dsd.MariaDbDimGroupAttrTable((char)targetDbTableVersion)} (SID, {MariaDbExtensions.LAST_UPDATED_COLUMN}, {dimGroupAttributeColumns})
                    SELECT SID, {MariaDbExtensions.LAST_UPDATED_COLUMN}, {dimGroupAttributeColumns}
                    FROM {DotStatDb.DataSchema}_{dsd.MariaDbDimGroupAttrTable((char)sourceDbTableVersion)}",
                    cancellationToken
                );
            }

            var datasetAttributes = dsd.Attributes.Where(a => a.Base.AttachmentLevel == AttributeAttachmentLevel.DataSet).ToArray();

            if (datasetAttributes.Any())
            {
                var datasetAttributeColumns = datasetAttributes.ToMariaDbColumnList();

                await DotStatDb.ExecuteNonQuerySqlAsync(
                    $@" INSERT
                    INTO {DotStatDb.DataSchema}_{dsd.MariaDbDsdAttrTable((char)targetDbTableVersion)} (DF_ID,{MariaDbExtensions.LAST_UPDATED_COLUMN}, {datasetAttributeColumns})
                    SELECT DF_ID,{MariaDbExtensions.LAST_UPDATED_COLUMN}, {datasetAttributeColumns}
                    FROM {DotStatDb.DataSchema}_{dsd.MariaDbDsdAttrTable((char)sourceDbTableVersion)}",
                    cancellationToken
                );
            }
        }

        protected override async ValueTask MergeDsdStagingToDimensionsTable(Dsd dsd, ReportedComponents reportedComponents, CancellationToken cancellationToken)
        {
            var noTimeDimensions = reportedComponents.Dimensions.Where(d => !d.Base.TimeDimension).ToList();
            if (!noTimeDimensions.Any())
                return;

            var dimensionColumnsNoTimeDim = noTimeDimensions.ToMariaDbColumnList();
            var dimensionColumnsNoTimeDimSelect = string.Join(", ", noTimeDimensions.Select(a => string.Format("Import.{0}", a.MariaDbColumn())));
            var dimensionColumnsNoTimeDimJoin = string.Join(" AND ", noTimeDimensions.Select(a => string.Format("Import.{0} = Filt.{0}", a.MariaDbColumn())));
            var dimensionColumnsNoTimeNotNull = string.Join(" AND ", noTimeDimensions.Select(a => $"Import.{a.MariaDbColumn()} IS NOT NULL"));

            if (!string.IsNullOrEmpty(dimensionColumnsNoTimeNotNull))
            {
                dimensionColumnsNoTimeNotNull = $" AND {dimensionColumnsNoTimeNotNull}";
            }

            var sqlCommand = $@"
INSERT INTO {DotStatDb.DataSchema}_{dsd.MariaDbFilterTable()} ({dimensionColumnsNoTimeDim})
SELECT DISTINCT {dimensionColumnsNoTimeDimSelect} 
FROM {DotStatDb.DataSchema}_{dsd.MariaDbStagingTable()} AS Import
LEFT JOIN {DotStatDb.DataSchema}_{dsd.MariaDbFilterTable()} AS Filt ON
    {dimensionColumnsNoTimeDimJoin}
WHERE Import.REQUESTED_ACTION IN (@Merge, @Replace) {dimensionColumnsNoTimeNotNull}
AND Filt.SID IS NULL;";

            //Add Sql parameters
            var mySqlParameters = new List<DbParameter>
            {
                new MySqlParameter("Merge", MySqlDbType.Int32) { Value = (int)StagingRowActionEnum.Merge },
                new MySqlParameter("Replace", MySqlDbType.Int32) { Value = (int)StagingRowActionEnum.Replace }
            };

            await DotStatDb.ExecuteNonQuerySqlWithParamsAsync(sqlCommand, cancellationToken, mySqlParameters.ToArray());
        }

        public override async ValueTask<MergeResult> MergeStagingToObservationsTable(IImportReferenceableStructure referencedStructure,
            ReportedComponents reportedComponents, DbTableVersion tableVersion, BatchAction batchAction, bool includeSummary,
            ICodeTranslator codeTranslator, DateTime dateTime, CancellationToken cancellationToken)
        {
            var mergeResult = new MergeResult();
            if (referencedStructure is not Dataflow dataflow)
            {
                //Dsd level merge not required for data
                return mergeResult;
            }

            var action = batchAction.Action;
            var batchNumber = batchAction.BatchNumber;
            var isWildCarded = batchAction.IsWildCarded;

            if (action is StagingRowActionEnum.Skip || action is StagingRowActionEnum.DeleteAll)
                return mergeResult;

            //There are no reported dimensions, no action required at this level
            if (!reportedComponents.Dimensions.Any())
                return mergeResult;

            var dsd = dataflow.Dsd;

            //No reported components at observation level
            //Delete and Replace actions can set the observation level components to NULL if the components are not present in the import file
            if (!reportedComponents.IsPrimaryMeasureReported && !reportedComponents.ObservationAttributes.Any() && action == StagingRowActionEnum.Merge)
                return mergeResult;

            //Dimensions columns
            var dimensionColumnsNoTimeDimJoinTemplate = action switch
            {//Delete action allows for wildcard dimensions and Replaces can have attributes not referencing all dimensions 
                //Note that for Replaces, the replace will not be wildcarded because in the validation CheckReferenceDimensionsPresent, all referenced dimensions must be present
                StagingRowActionEnum.Delete or StagingRowActionEnum.Replace when isWildCarded => " Filt.{0} = IFNULL(Import2.{0}, Filt.{0}) ",
                _ => " Filt.{0} = Import2.{0} "
            };

            var noTimeDimensions = reportedComponents.Dimensions.Where(d => !d.Base.TimeDimension);
            var dimensionColumnsNoTimeDimJoin =
                string.Join(" AND ", noTimeDimensions.Select(a => string.Format(dimensionColumnsNoTimeDimJoinTemplate, a.MariaDbColumn())));

            //Time dimension columns
            string timeDimColumns = null;
            string timeDimColumnsInsert = null;
            string timeDimColumnsJoin = null;
            string sdmxTimeDimNotNull = null;
            if (reportedComponents.TimeDimension is not null)
            {
                timeDimColumns = "," + string.Join(",", reportedComponents.TimeDimension.MariaDbColumn(), MariaDbExtensions.MariaDbPeriodStart(), MariaDbExtensions.MariaDbPeriodEnd());

                string importFormat = "Import.{0}";
                timeDimColumnsInsert = "," + string.Join(",",
                    string.Format(importFormat, reportedComponents.TimeDimension.MariaDbColumn()),
                    string.Format(importFormat, MariaDbExtensions.MariaDbPeriodStart()),
                    string.Format(importFormat, MariaDbExtensions.MariaDbPeriodEnd()));

                var timeDimColumnsJoinTemplate = action switch
                {
                    //Delete action allows for wildcard dimensions 
                    StagingRowActionEnum.Delete when isWildCarded => " Fact.{0} = IFNULL(Import.{0}, Fact.{0}) ",
                    _ => "Fact.{0} = Import.{0}"
                };

                timeDimColumnsJoin = @$"AND (
                    {string.Format(timeDimColumnsJoinTemplate, MariaDbExtensions.MariaDbPeriodStart())} AND 
                    {string.Format(timeDimColumnsJoinTemplate, MariaDbExtensions.MariaDbPeriodEnd())})";

                sdmxTimeDimNotNull = string.Format("AND Import.{0} IS NOT NULL", reportedComponents.TimeDimension.MariaDbColumn());
            }

            string updateDiffCheck = null;
            //Measure columns
            string measureColumn = null;
            string measureSelect = null;
            string measureColumnInsert = null;
            string measureColumnUpdate = null;
            switch (action)
            {
                case StagingRowActionEnum.Merge when reportedComponents.IsPrimaryMeasureReported:
                    measureSelect = measureColumn = ", VALUE";
                    measureColumnInsert = ", Import.VALUE, ";
                    measureColumnUpdate = "Fact.VALUE = CASE WHEN Import.VALUE IS NOT NULL THEN Import.VALUE ELSE Fact.VALUE END,";
                    updateDiffCheck = $"(Import.VALUE IS NOT NULL AND ({dsd.PrimaryMeasure.MariaDbCollate($"Import.VALUE")} <> {dsd.PrimaryMeasure.MariaDbCollate($"Fact.VALUE")} OR Fact.VALUE IS NULL))";
                    break;
                case StagingRowActionEnum.Replace:
                    measureSelect = reportedComponents.IsPrimaryMeasureReported ? ", VALUE" : ", NULL as VALUE";
                    measureColumn = ", VALUE";
                    measureColumnInsert = ", Import.VALUE, ";
                    measureColumnUpdate = "Fact.VALUE = Import.VALUE,";
                    updateDiffCheck = reportedComponents.IsPrimaryMeasureReported ? $"((Import.VALUE IS NULL AND Fact.VALUE IS NOT NULL) OR (Import.VALUE IS NOT NULL AND Fact.VALUE IS NULL) OR {dsd.PrimaryMeasure.MariaDbCollate($"Fact.VALUE")} <> {dsd.PrimaryMeasure.MariaDbCollate($"Import.VALUE")})" : "Fact.VALUE IS NOT NULL";
                    break;
                case StagingRowActionEnum.Delete:
                    measureSelect = reportedComponents.IsPrimaryMeasureReported ? ", VALUE" : ", NULL as VALUE";
                    measureColumn = ", VALUE";
                    measureColumnUpdate = "Fact.VALUE = CASE WHEN Import.VALUE IS NOT NULL THEN NULL ELSE Fact.VALUE END,";
                    updateDiffCheck = "(Import.VALUE IS NOT NULL AND Fact.VALUE IS NOT NULL)";
                    break;
            }

            //Attributes columns
            var reportedAttributes = action == StagingRowActionEnum.Merge ?
                reportedComponents.ObservationAttributes :
                dsd.Attributes.Where(a => a.Base.AttachmentLevel == AttributeAttachmentLevel.Observation
                    || a.Base.GetDimensionReferences().Contains(DimensionObject.TimeDimensionFixedId)).ToList();

            string obsAttrColumns = reportedAttributes.ToMariaDbColumnList();
            string obsAttrColumnsInsert = reportedAttributes.ToMariaDbColumnListWithTablePrefix(null, false, true, false, "Import");
            string obsAttrColumnsUpdate = null;
            string updateDiffCheckAttr = null;
            switch (action)
            {
                case StagingRowActionEnum.Merge:
                    obsAttrColumnsUpdate = string.Join(",\n", reportedAttributes
                        .Select(a => $"Fact.{a.MariaDbColumn()} = CASE WHEN Import.{a.MariaDbColumn()} IS NOT NULL THEN Import.{a.MariaDbColumn()} ELSE Fact.{a.MariaDbColumn()} END"));
                    updateDiffCheckAttr = string.Join(" OR\n", reportedAttributes
                        .Select(a => $"(Import.{a.MariaDbColumn()} IS NOT NULL AND ({a.MariaDbCollate($"Import.{a.MariaDbColumn()}")} <> {a.MariaDbCollate($"Fact.{a.MariaDbColumn()}")} OR Fact.{a.MariaDbColumn()} IS NULL))"));
                    break;
                case StagingRowActionEnum.Delete:
                    obsAttrColumnsUpdate = string.Join(",\n", reportedAttributes
                        .Select(a => $"Fact.{a.MariaDbColumn()} = CASE WHEN Import.{a.MariaDbColumn()} IS NOT NULL THEN NULL ELSE Fact.{a.MariaDbColumn()} END"));
                    updateDiffCheckAttr = string.Join(" OR\n", reportedAttributes
                        .Select(a => $"(Import.{a.MariaDbColumn()} IS NOT NULL AND Fact.{a.MariaDbColumn()} IS NOT NULL)"));
                    break;
                case StagingRowActionEnum.Replace:
                    obsAttrColumnsUpdate = string.Join(",\n", reportedAttributes.Select(a => @$"Fact.{a.MariaDbColumn()} = Import.{a.MariaDbColumn()}"));

                    var allAttributes = dsd.Attributes.Where(a => a.Base.AttachmentLevel == AttributeAttachmentLevel.Observation
                     || a.Base.GetDimensionReferences().Contains(DimensionObject.TimeDimensionFixedId)).ToList();
                    var allAttributesWithInclusion = allAttributes
                        .Select(attribute => (reportedComponents.ObservationAttributes.Any(ra => ra.Code.Equals(attribute.Code, StringComparison.InvariantCultureIgnoreCase)), attribute)).ToList();

                    updateDiffCheckAttr = string.Join(" OR\n", allAttributesWithInclusion
                        .Select(a => a.Item1 ?
                            $"((Import.{a.attribute.MariaDbColumn()} IS NOT NULL AND Fact.{a.attribute.MariaDbColumn()} IS NULL) OR (Import.{a.attribute.MariaDbColumn()} IS NULL AND Fact.{a.attribute.MariaDbColumn()} IS NOT NULL) OR {a.attribute.MariaDbCollate($"Fact.{a.attribute.MariaDbColumn()}")} <> {a.attribute.MariaDbCollate($"Import.{a.attribute.MariaDbColumn()}")})"
                            : $"Fact.{a.attribute.MariaDbColumn()} IS NOT NULL"));
                    break;
            };

            if (!string.IsNullOrEmpty(obsAttrColumns))
                obsAttrColumns = "," + obsAttrColumns;

            if (!string.IsNullOrEmpty(obsAttrColumnsUpdate))
                obsAttrColumnsUpdate += ",";

            if (!string.IsNullOrEmpty(obsAttrColumnsInsert))
                obsAttrColumnsInsert += ",";

            if (!string.IsNullOrEmpty(updateDiffCheckAttr))
                updateDiffCheckAttr = $" OR {updateDiffCheckAttr}";

            updateDiffCheck += updateDiffCheckAttr;

            if (!string.IsNullOrEmpty(updateDiffCheck))
                updateDiffCheck = $" AND ({updateDiffCheck})";

            //Calculate summary
            var summaryStatement = string.Empty;
            if (includeSummary)
            {
                summaryStatement = action == StagingRowActionEnum.Delete ?
                "SELECT 0 as INSERTED, @updated as UPDATED, (@rowcount - @updated) as DELETED, @rowcount as TOTAL;" :
                "SELECT (@rowcount - @updated) as INSERTED, @updated as UPDATED, 0 AS DELETED, @rowcount as TOTAL;";
            }
            else
            {
                summaryStatement = "SELECT CASE WHEN @rowcount > 0 THEN 1 ELSE 0 END;";
            }

            var timeDimConstraint = new List<string>();
            Predicate.AddTimePredicate(timeDimConstraint, dataflow, null, "Fact", serverType: MappingStore.DbType.MariaDb);

            var timeConstraint = action is StagingRowActionEnum.Delete && timeDimConstraint.Any() ? " AND " + string.Join(" AND ", timeDimConstraint) : null;

            //Merge staging table to Fact table
            var actionStatement = action == StagingRowActionEnum.Delete ?
//Delete actions
$@"UPDATE {DotStatDb.DataSchema}_{dsd.MariaDbFactTable((char)tableVersion)} AS Fact 
INNER JOIN Import AS Import ON 
	Fact.SID = Import.SID AND Import.REAL_ACTION_FACT = @Merge {updateDiffCheck}
	{timeDimColumnsJoin} 
    {timeConstraint}
SET {measureColumnUpdate}
    {obsAttrColumnsUpdate}
    {MariaDbExtensions.LAST_UPDATED_COLUMN} = @CurrentDateTime;

SET @updated := ROW_COUNT();
SET @rowcount := @updated;

DELETE Fact FROM Import AS Import
INNER JOIN {DotStatDb.DataSchema}_{dsd.MariaDbFactTable((char)tableVersion)} AS Fact ON 
	Fact.SID = Import.SID 
	{timeDimColumnsJoin} 
    {timeConstraint}
WHERE Import.REAL_ACTION_FACT = @Delete;

SET @rowcount := (@rowcount + ROW_COUNT());" :

// Replace and Merge actions
$@"UPDATE {DotStatDb.DataSchema}_{dsd.MariaDbFactTable((char)tableVersion)} AS Fact
INNER JOIN Import AS Import ON 
	Fact.SID = Import.SID {timeDimColumnsJoin} 
    {timeConstraint} {updateDiffCheck}
SET 
    {measureColumnUpdate}
    {obsAttrColumnsUpdate}
    {MariaDbExtensions.LAST_UPDATED_COLUMN} = @CurrentDateTime;

SET @updated := ROW_COUNT();
SET @rowcount := @updated;

INSERT INTO {DotStatDb.DataSchema}_{dsd.MariaDbFactTable((char)tableVersion)} (SID{timeDimColumns}{measureColumn}{obsAttrColumns}, {MariaDbExtensions.LAST_UPDATED_COLUMN})
SELECT Import.SID{timeDimColumnsInsert}{measureColumnInsert}{obsAttrColumnsInsert} @CurrentDateTime
FROM Import AS Import
LEFT JOIN {DotStatDb.DataSchema}_{dsd.MariaDbFactTable((char)tableVersion)} AS Fact ON 
	Fact.SID = Import.SID {timeDimColumnsJoin} 
    {timeConstraint}
WHERE Fact.SID IS NULL {sdmxTimeDimNotNull};

SET @rowcount := (@rowcount + ROW_COUNT());";

            //Filter allowed content constraint
            var nonTimeConstraint = action == StagingRowActionEnum.Delete ?
                Predicate.BuildWhereForObservationLevel(dataQuery: null, dataflow, codeTranslator, useExternalValues: false, useExternalColumnNames: false, includeTimeConstraints: false, tableAlias: "Filt", serverType: MappingStore.DbType.MariaDb)
                : string.Empty;
            if (!string.IsNullOrEmpty(nonTimeConstraint))
                nonTimeConstraint = $"AND {nonTimeConstraint}";

            var sqlCommand = $@"
SET @rowcount := 0;
SET @updated := 0;

DROP TEMPORARY TABLE IF EXISTS Import;
CREATE TEMPORARY TABLE Import AS
    SELECT Filt.SID{timeDimColumns}{measureSelect}{obsAttrColumns}, REAL_ACTION_FACT
    FROM {DotStatDb.DataSchema}_{dsd.MariaDbStagingTable()} Import2
    LEFT JOIN {DotStatDb.DataSchema}_{dsd.MariaDbFilterTable()} Filt ON 
        {dimensionColumnsNoTimeDimJoin}

    WHERE Import2.BATCH_NUMBER = @BatchNumber AND REAL_ACTION_FACT <> @Skip
    {nonTimeConstraint};

{actionStatement}
{summaryStatement}";

            //Add Sql parameters
            var mySqlParameters = new List<DbParameter>
            {
                new MySqlParameter("CurrentDateTime", MySqlDbType.DateTime) { Value = dateTime },
                new MySqlParameter("Merge", MySqlDbType.Int32) { Value = (int)StagingRowActionEnum.Merge },
                new MySqlParameter("Delete", MySqlDbType.Int32) { Value = (int)StagingRowActionEnum.Delete },
                new MySqlParameter("Skip", MySqlDbType.Int32) { Value = (int)StagingRowActionEnum.Skip },
                new MySqlParameter("BatchNumber", MySqlDbType.Int32) { Value = batchNumber }
            };

            return await ExecuteMerge(sqlCommand, mySqlParameters, includeSummary, cancellationToken, dsd, reportedComponents);
        }

        public override async ValueTask<MergeResult> MergeStagingToSeriesTable(IImportReferenceableStructure referencedStructure,
            ReportedComponents reportedComponents, DbTableVersion tableVersion, BatchAction batchAction, bool includeSummary,
            ICodeTranslator codeTranslator, DateTime dateTime, CancellationToken cancellationToken)
        {
            var mergeResult = new MergeResult();
            if (referencedStructure is not Dataflow dataflow)
            {
                //Dsd level merge not required for data
                return mergeResult;
            }

            var noTimeDimensions = reportedComponents.Dimensions.Where(d => !d.Base.TimeDimension).ToList();
            //There are no dimensions (excluding time) reported, therefore no changes required a series/group series level
            //Action takes place at given the scope of the reported dimensions; No reported dimensions means either delete all or only at dataflow level
            if (!noTimeDimensions.Any())
                return mergeResult;

            var action = batchAction.Action;
            var batchNumber = batchAction.BatchNumber;
            var isWildCarded = batchAction.IsWildCarded;

            if (action is StagingRowActionEnum.Skip || action is StagingRowActionEnum.DeleteAll ||
                (!reportedComponents.SeriesAttributesWithNoTimeDim.Any() && action is StagingRowActionEnum.Merge))
                return mergeResult;

            var dsd = dataflow.Dsd;

            //Dimensions columns
            var dimensionColumnsNoTimeDimJoinTemplate = action switch
            {
                //Delete action allows for wildcard dimensions and Replaces can have attributes not referencing all dimensions 
                //Note that for Replaces, the replace will not be wildcarded because in the validation CheckReferenceDimensionsPresent, all referenced dimensions must be present
                StagingRowActionEnum.Delete or StagingRowActionEnum.Replace when isWildCarded => " Filt.{0} = IFNULL(Import2.{0}, Filt.{0}) ",
                _ => " Filt.{0} = Import2.{0} "
            };

            var dimensionColumnsNoTimeDimJoin =
                string.Join(" AND ", noTimeDimensions.Select(a => string.Format(dimensionColumnsNoTimeDimJoinTemplate, a.MariaDbColumn())));

            //Attributes columns
            var reportedAttributes = action == StagingRowActionEnum.Merge ?
                reportedComponents.SeriesAttributesWithNoTimeDim :
                dsd.Attributes
                    .Where(a => a.Base.AttachmentLevel is AttributeAttachmentLevel.DimensionGroup or AttributeAttachmentLevel.Group)
                    .Where(a => !a.Base.GetDimensionReferences().Contains(DimensionObject.TimeDimensionFixedId)).ToList();

            if (!reportedAttributes.Any())
                return mergeResult;

            var seriesAttrColumns = reportedAttributes.Any() ?
                ", " + reportedAttributes.ToMariaDbColumnList() : string.Empty;
            var seriesAttrColumnsInsert = reportedAttributes.Any() ?
                ", " + reportedAttributes.ToMariaDbColumnListWithTablePrefix(null, false, true, false, "Import") : string.Empty;

            var seriesAttrColumnsUpdate = action switch
            {
                //When Replacing data, then higher-level attributes (series, group, dataflow) are not to be replaced but only updated or added if related values are present in the upload file.
                //It means that they cannot be NULLed through a Replace action.
                //Only Delete actions can NULL those attributes.
                StagingRowActionEnum.Merge or StagingRowActionEnum.Replace =>
                    string.Join(",\n", reportedAttributes
                        .Select(a => $"Attr.{a.MariaDbColumn()} = CASE WHEN Import.{a.MariaDbColumn()} IS NOT NULL THEN Import.{a.MariaDbColumn()} ELSE Attr.{a.MariaDbColumn()} END")),

                StagingRowActionEnum.Delete =>
                    string.Join(",\n", reportedAttributes
                        .Select(a => $"Attr.{a.MariaDbColumn()} = CASE WHEN Import.{a.MariaDbColumn()} IS NOT NULL THEN NULL ELSE Attr.{a.MariaDbColumn()} END")),
                _ => string.Empty
            };

            var seriesAttrColumnsWithMin = string.Join(",", reportedAttributes.Select(a => string.Format("MIN({0}) {0}", a.MariaDbColumn())));

            if (!string.IsNullOrEmpty(seriesAttrColumnsWithMin))
                seriesAttrColumnsWithMin = "," + seriesAttrColumnsWithMin;

            var updateDiffCheck = action switch
            {
                StagingRowActionEnum.Merge or StagingRowActionEnum.Replace =>
                    string.Join(" OR\n", reportedAttributes
                        .Select(a => $"(Import.{a.MariaDbColumn()} IS NOT NULL AND ({a.MariaDbCollate($"Attr.{a.MariaDbColumn()}")} <> {a.MariaDbCollate($"Import.{a.MariaDbColumn()}")} OR Attr.{a.MariaDbColumn()} IS NULL))")),

                StagingRowActionEnum.Delete =>
                    string.Join(" OR\n", reportedAttributes
                        .Select(a => $"(Import.{a.MariaDbColumn()} IS NOT NULL AND Attr.{a.MariaDbColumn()} IS NOT NULL)")),
                _ => string.Empty
            };

            if (!string.IsNullOrEmpty(updateDiffCheck))
                updateDiffCheck = $"AND ({updateDiffCheck})";

            //Calculate summary
            var summaryStatement = string.Empty;
            if (includeSummary)
            {
                summaryStatement = action == StagingRowActionEnum.Delete ?
                    "SELECT 0 as INSERTED, @updated as UPDATED, (@rowcount - @updated) as DELETED, @rowcount as TOTAL;" :
                    "SELECT (@rowcount - @updated) as INSERTED, @updated as UPDATED, 0 AS DELETED, @rowcount as TOTAL;";
            }
            else
            {
                summaryStatement = "SELECT CASE WHEN @rowcount > 0 THEN 1 ELSE 0 END;";
            }

            //Merge staging table to Attr table
            var actionStatement = action == StagingRowActionEnum.Delete ?
//Delete actions
$@"
UPDATE {DotStatDb.DataSchema}_{dsd.MariaDbDimGroupAttrTable((char)tableVersion)} AS Attr
INNER JOIN Import AS Import ON 
	Attr.SID = Import.SID AND Import.REAL_ACTION_ATTR = @Merge 
	{updateDiffCheck}
SET {seriesAttrColumnsUpdate}
    , {MariaDbExtensions.LAST_UPDATED_COLUMN} = @CurrentDateTime;

SET @updated := ROW_COUNT();
SET @rowcount := @updated;

DELETE Attr FROM {DotStatDb.DataSchema}_{dsd.MariaDbDimGroupAttrTable((char)tableVersion)} AS Attr
INNER JOIN Import AS Import ON Attr.SID = Import.SID AND Import.REAL_ACTION_ATTR = @Delete;

SET @rowcount := (@rowcount + ROW_COUNT());" :

// Replace and Merge actions
$@"
UPDATE {DotStatDb.DataSchema}_{dsd.MariaDbDimGroupAttrTable((char)tableVersion)} AS Attr
INNER JOIN Import AS Import ON 
	Attr.SID = Import.SID 
	{updateDiffCheck}
SET 
    {seriesAttrColumnsUpdate}
    , {MariaDbExtensions.LAST_UPDATED_COLUMN} = @CurrentDateTime; 

SET @updated := ROW_COUNT();
SET @rowcount := @updated;

INSERT INTO {DotStatDb.DataSchema}_{dsd.MariaDbDimGroupAttrTable((char)tableVersion)} (SID{seriesAttrColumns},{MariaDbExtensions.LAST_UPDATED_COLUMN})
SELECT Import.SID{seriesAttrColumnsInsert}, @CurrentDateTime
FROM Import AS Import
LEFT JOIN {DotStatDb.DataSchema}_{dsd.MariaDbDimGroupAttrTable((char)tableVersion)} AS Attr ON 
	Attr.SID = Import.SID
WHERE Attr.SID IS NULL;

SET @rowcount := (@rowcount + ROW_COUNT());";

            //Filter allowed content constraint
            var nonTimeConstraint = action == StagingRowActionEnum.Delete ?
                Predicate.BuildWhereForObservationLevel(dataQuery: null, dataflow, codeTranslator, useExternalValues: false, useExternalColumnNames: false, includeTimeConstraints: false, tableAlias: "Filt", serverType: MappingStore.DbType.MariaDb)
                : string.Empty;
            if (!string.IsNullOrEmpty(nonTimeConstraint))
                nonTimeConstraint = $"AND {nonTimeConstraint}";

            //Merge staging table to Attributes table            
            var sqlCommand = $@"
SET @rowcount := 0;
SET @updated := 0;
DROP TEMPORARY TABLE IF EXISTS Import;
CREATE TEMPORARY TABLE Import AS
    SELECT Filt.SID {seriesAttrColumnsWithMin}, MIN(REAL_ACTION_ATTR) REAL_ACTION_ATTR
    FROM {DotStatDb.DataSchema}_{dsd.MariaDbStagingTable()} Import2
    LEFT JOIN {DotStatDb.DataSchema}_{dsd.MariaDbFilterTable()} Filt 
    ON {dimensionColumnsNoTimeDimJoin}
    WHERE Import2.BATCH_NUMBER = @BatchNumber AND REAL_ACTION_ATTR <> @Skip
    {nonTimeConstraint}
    GROUP BY Filt.SID;

{actionStatement}
{summaryStatement}";

            //Add Sql parameters
            var mySqlParameters = new List<DbParameter>
            {
                new MySqlParameter("CurrentDateTime", MySqlDbType.DateTime) { Value = dateTime},
                new MySqlParameter("Merge", MySqlDbType.Int32) { Value = (int)StagingRowActionEnum.Merge },
                new MySqlParameter("Delete", MySqlDbType.Int32) { Value = (int)StagingRowActionEnum.Delete },
                new MySqlParameter("Skip", MySqlDbType.Int32) { Value = (int)StagingRowActionEnum.Skip },
                new MySqlParameter("BatchNumber", MySqlDbType.Int32) { Value = batchNumber }
            };

            //EXECUTE MERGE

            return await ExecuteMerge(sqlCommand, mySqlParameters, includeSummary, cancellationToken, dsd, reportedComponents);
        }

        public override async ValueTask<MergeResult> MergeStagingToDatasetTable(IImportReferenceableStructure referencedStructure, IList<IKeyValue> attributes,
            CodeTranslator translator, DbTableVersion tableVersion, StagingRowActionEnum action, bool includeSummary,
            DateTime dateTime, CancellationToken cancellationToken)
        {
            var mergeResult = new MergeResult();
            if (referencedStructure is not Dataflow dataflow)
            {
                //Dsd level merge not required for data
                return mergeResult;
            }

            if (action is StagingRowActionEnum.Skip || action is StagingRowActionEnum.DeleteAll)
                return mergeResult;

            //No dataSet level attributes to process
            if (!attributes.Any())
                return mergeResult;

            var datasetAttributesOfDds = dataflow.Dsd.Attributes
                .Where(a => a.Base.AttachmentLevel is AttributeAttachmentLevel.DataSet or AttributeAttachmentLevel.Null).ToList();

            //No dsd attributes
            if (datasetAttributesOfDds.Count == 0)
                return mergeResult;

            //Attributes
            var attributesToModify = new Dictionary<Attribute, object>();
            var i = 0;

            var attributesPresent = new bool[datasetAttributesOfDds.Count];
            foreach (var attributeKeyValue in attributes.Where(x => !string.IsNullOrEmpty(x.Code)))
            {
                var attribute = datasetAttributesOfDds.First(a =>
                    a.Base.Id.Equals(attributeKeyValue.Concept, StringComparison.InvariantCultureIgnoreCase));

                //Translate to internal codes
                var attrValue = attribute.GetObjectFromString(attributeKeyValue.Code, translator, action);
                attributesToModify[attribute] = attrValue;
                attributesPresent[i++] = true;
            }

            if (!attributesToModify.Any())
                return mergeResult;

            var realAction = action switch
            {
                StagingRowActionEnum.Delete when attributesPresent.Distinct().Count() != 1 =>
                    StagingRowActionEnum.Merge,
                StagingRowActionEnum.Replace when !attributesToModify.Any() =>
                    StagingRowActionEnum.Delete,
                StagingRowActionEnum.Merge when !attributesToModify.Any() =>
                    StagingRowActionEnum.Skip,
                _ => action
            };

            if (realAction == StagingRowActionEnum.Skip)
                return mergeResult;

            //DELETE
            if (realAction == StagingRowActionEnum.Delete)
            {
                if (includeSummary)
                {
                    mergeResult.DeleteCount += (int)(long)await DotStatDb.ExecuteScalarSqlAsync(
                        $"DELETE FROM {DotStatDb.DataSchema}_{dataflow.Dsd.MariaDbDsdAttrTable((char)tableVersion)} WHERE DF_ID = {dataflow.DbId}; SELECT ROW_COUNT() AS ROW_COUNT;", cancellationToken);
                    mergeResult.TotalCount =
                        mergeResult.UpdateCount + mergeResult.DeleteCount + mergeResult.InsertCount;
                }
                else
                {
                    await DotStatDb.ExecuteNonQuerySqlAsync(
                        $"DELETE FROM {DotStatDb.DataSchema}_{dataflow.Dsd.MariaDbDsdAttrTable((char)tableVersion)} WHERE DF_ID = {dataflow.DbId}", cancellationToken);
                }

                return mergeResult;
            }

            //ATTRIBUTES
            var columnNames = string.Join(",", attributesToModify.Select(kvp => kvp.Key.MariaDbColumn()));
            var columnNamesInsert = string.Join(",", attributesToModify.Select(kvp => $"Import.{kvp.Key.MariaDbColumn()}"));

            var attributeColumns = string.Join(",", attributesToModify.Select(kvp => $"@Comp{kvp.Key.DbId} AS {kvp.Key.MariaDbColumn()}"));

            var attrColumnsUpdate = action switch
            {
                //When Replacing data, then higher-level attributes (series, group, dataflow) are not to be replaced but only updated or added if related values are present in the upload file.
                //It means that they cannot be NULLed through a Replace action.
                //Only Delete actions can NULL those attributes.
                StagingRowActionEnum.Merge or StagingRowActionEnum.Replace =>
                    string.Join(",\n", attributesToModify.Select(kvp => kvp.Key)
                        .Select(a => $"Fact.{a.MariaDbColumn()} = CASE WHEN Import.{a.MariaDbColumn()} IS NOT NULL THEN Import.{a.MariaDbColumn()} ELSE Fact.{a.MariaDbColumn()} END")),
                StagingRowActionEnum.Delete =>
                    string.Join(",\n", attributesToModify.Select(kvp => kvp.Key)
                        .Select(a => $"Fact.{a.MariaDbColumn()} = CASE WHEN Import.{a.MariaDbColumn()} IS NOT NULL THEN NULL ELSE Fact.{a.MariaDbColumn()} END")),
                _ => string.Empty
            };

            var updateDiffCheck = action switch
            {
                StagingRowActionEnum.Merge or StagingRowActionEnum.Replace =>
                    string.Join(" OR\n", attributesToModify.Select(kvp => kvp.Key)
                        .Select(a => $"(Import.{a.MariaDbColumn()} IS NOT NULL AND ({a.MariaDbCollate($"Fact.{a.MariaDbColumn()}")} <> {a.MariaDbCollate($"Import.{a.MariaDbColumn()}")} OR Fact.{a.MariaDbColumn()} IS NULL))")),

                StagingRowActionEnum.Delete =>
                    string.Join(" OR\n", attributesToModify.Select(kvp => kvp.Key)
                        .Select(a => $"(Import.{a.MariaDbColumn()} IS NOT NULL AND Fact.{a.MariaDbColumn()} IS NOT NULL)")),
                _ => string.Empty
            };

            if (!string.IsNullOrEmpty(updateDiffCheck))
                updateDiffCheck = $"AND ({updateDiffCheck})";

            //Merge staging table to DsdAttr table
            var actionStatement = action == StagingRowActionEnum.Delete ?
//Delete actions
$@"
UPDATE {DotStatDb.DataSchema}_{dataflow.Dsd.MariaDbDsdAttrTable((char)tableVersion)} AS Fact
INNER JOIN Import AS Import ON 
	Fact.DF_ID = Import.DF_ID
	{updateDiffCheck}
SET {attrColumnsUpdate}
    , {MariaDbExtensions.LAST_UPDATED_COLUMN} = @CurrentDateTime;

SET @updated := ROW_COUNT();
SET @rowcount := @updated;" :

// Replace and Merge actions
$@"
UPDATE {DotStatDb.DataSchema}_{dataflow.Dsd.MariaDbDsdAttrTable((char)tableVersion)} AS Fact
INNER JOIN Import AS Import ON 
	Fact.DF_ID = Import.DF_ID
	{updateDiffCheck}
SET 
    {attrColumnsUpdate}
    , {MariaDbExtensions.LAST_UPDATED_COLUMN} = @CurrentDateTime; 

SET @updated := ROW_COUNT();
SET @rowcount := @updated;

INSERT INTO {DotStatDb.DataSchema}_{dataflow.Dsd.MariaDbDsdAttrTable((char)tableVersion)} (DF_ID, {columnNames}, {MariaDbExtensions.LAST_UPDATED_COLUMN})
SELECT Import.DF_ID, {columnNamesInsert}, @CurrentDateTime
FROM Import AS Import
LEFT JOIN {DotStatDb.DataSchema}_{dataflow.Dsd.MariaDbDsdAttrTable((char)tableVersion)} AS Fact ON Fact.DF_ID = Import.DF_ID
WHERE Fact.DF_ID IS NULL;

SET @rowcount := (@rowcount + ROW_COUNT());";

            var sqlCommand = $@"
SET @rowcount := 0;
SET @updated := 0;
DROP TEMPORARY TABLE IF EXISTS Import;
CREATE TEMPORARY TABLE Import AS
    SELECT @DfId AS DF_ID, {attributeColumns};

{actionStatement}

{(includeSummary ? "SELECT @rowcount - @updated AS INSERTED, @updated as UPDATED, 0 as DELETED, @rowcount as TOTAL;" : "SELECT CASE WHEN @rowcount > 0 THEN 1 ELSE 0 END;")}";

            //SQL PARAMETERS
            var mySqlParameters = new List<DbParameter>
            {
                new MySqlParameter("CurrentDateTime", MySqlDbType.DateTime) { Value = dateTime}
            };

            mySqlParameters.AddRange(attributesToModify.Select(kvp =>
                new MySqlParameter($"Comp{kvp.Key.DbId}", kvp.Key.GetMariaDbType())
                {
                    Value = kvp.Value
                } as DbParameter).Concat(new[] { new MySqlParameter("DfId", MySqlDbType.Int32) { Value = dataflow.DbId } as DbParameter }).ToArray());

            //EXECUTE MERGE

            return await ExecuteMerge(sqlCommand, mySqlParameters, includeSummary, cancellationToken);
        }

        public async Task DropDsdTables(Dsd dsd, CancellationToken cancellationToken)
        {
            await DropDsdTables(dsd, DbTableVersion.A, cancellationToken);
            await DropDsdTables(dsd, DbTableVersion.B, cancellationToken);
        }

        protected override async Task<bool> CleanUpFullyEmptyObservations(IImportReferenceableStructure referencedStructure, DbTableVersion tableVersion,
            CancellationToken cancellationToken)
        {
            if (referencedStructure is not Dataflow dataFlow)
            {
                //Dsd level cleanup not required for data
                return true;
            }

            var dsd = dataFlow.Dsd;
            if (!dsd.Attributes.Any())
            {
                return false;
            }

            var attributeColumns = dsd.Attributes
                .Where(a => a.Base.AttachmentLevel == AttributeAttachmentLevel.Observation || a.Base.GetDimensionReferences().Contains(DimensionObject.TimeDimensionFixedId))
                .ToMariaDbColumnListWithIsNull();

            if (!string.IsNullOrWhiteSpace(attributeColumns))
            {
                attributeColumns += " AND ";
            }

            // Fact table
            //Delete fully empty rows at obs level
            var sqlCommand = $@"DELETE FROM {DotStatDb.DataSchema}_{dsd.MariaDbFactTable((char)tableVersion)} WHERE 
                {attributeColumns}
                VALUE IS NULL; 

                SELECT CASE WHEN ROW_COUNT() > 0 THEN TRUE ELSE FALSE END;";

            var coordinatesDeleted = await DotStatDb.ExecuteNonQuerySqlAsync(sqlCommand, cancellationToken) > 0;

            // Group attribute table
            attributeColumns = dsd.Attributes
                .Where(x => !x.Base.GetDimensionReferences().Contains(DimensionObject.TimeDimensionFixedId))
                .ToMariaDbColumnListWithIsNull(new[] { AttributeAttachmentLevel.DimensionGroup, AttributeAttachmentLevel.Group });

            if (!string.IsNullOrEmpty(attributeColumns))
            {
                sqlCommand = $@"DELETE FROM {DotStatDb.DataSchema}_{dsd.SqlDimGroupAttrTable((char)tableVersion)} WHERE {attributeColumns}; 
                SELECT CASE WHEN ROW_COUNT() > 0 THEN TRUE ELSE FALSE END; ";

                var attrRowsDeleted = await DotStatDb.ExecuteNonQuerySqlAsync(sqlCommand, cancellationToken) > 0;
                coordinatesDeleted = coordinatesDeleted || attrRowsDeleted;
            }

            // Dataset level attribute table
            attributeColumns = dsd.Attributes.ToMariaDbColumnListWithIsNull(new[] { AttributeAttachmentLevel.DataSet, AttributeAttachmentLevel.Null });

            if (!string.IsNullOrEmpty(attributeColumns))
            {
                await DotStatDb.ExecuteNonQuerySqlAsync($@"DELETE FROM {DotStatDb.DataSchema}_{dsd.MariaDbDsdAttrTable((char)tableVersion)} WHERE {attributeColumns}", cancellationToken);
            }

            //Indicate if there are coordinates that were deleted at observation or series level.
            return coordinatesDeleted;
        }

        private async Task DropDsdTables(Dsd dsd, DbTableVersion tableVersion, CancellationToken cancellationToken)
        {
            if (DotStatDb.DataSpace.KeepHistory)
            {
                await DotStatDb.RemoveTemporalTableSupport(DotStatDb.DataSchema, dsd.MariaDbFactTable((char)tableVersion),
                    cancellationToken);
                await DotStatDb.DropTable(DotStatDb.DataSchema, dsd.MariaDbFactTable((char)tableVersion), cancellationToken);
                await DotStatDb.DropTable(DotStatDb.DataSchema, dsd.MariaDbFactTable((char)tableVersion), cancellationToken);
            }
            else
            {
                await DotStatDb.DropTable(DotStatDb.DataSchema, dsd.SqlFactTable((char)tableVersion), cancellationToken);
            }

            await DotStatDb.DropTable(DotStatDb.DataSchema, dsd.MariaDbFilterTable(), cancellationToken);
            await DotStatDb.DropTable(DotStatDb.DataSchema, dsd.MariaDbDeletedTable((char)tableVersion), cancellationToken);
        }

        public async Task DropAttributeTables(Dsd dsd, CancellationToken cancellationToken)
        {
            if (DotStatDb.DataSpace.KeepHistory)
            {
                await DotStatDb.RemoveTemporalTableSupport(DotStatDb.DataSchema, dsd.MariaDbDimGroupAttrTable((char)DbTableVersion.A),
                    cancellationToken);
                await DotStatDb.RemoveTemporalTableSupport(DotStatDb.DataSchema, dsd.MariaDbDimGroupAttrTable((char)DbTableVersion.B),
                    cancellationToken);
                /* Mariadb system versioning is not in separate table
                await DotStatDb.DropTable(DotStatDb.DataSchema, dsd.MariaDbDimGroupAttrHistoryTable((char)DbTableVersion.A),
                    cancellationToken);
                await DotStatDb.DropTable(DotStatDb.DataSchema, dsd.MariaDbDimGroupAttrHistoryTable((char)DbTableVersion.B),
                    cancellationToken);*/
                await DotStatDb.DropTable(DotStatDb.DataSchema, dsd.MariaDbDimGroupAttrTable((char)DbTableVersion.A),
                    cancellationToken);
                await DotStatDb.DropTable(DotStatDb.DataSchema, dsd.MariaDbDimGroupAttrTable((char)DbTableVersion.B),
                    cancellationToken);
            }
            else
            {
                await DotStatDb.DropTable(DotStatDb.DataSchema, dsd.MariaDbDimGroupAttrTable((char)DbTableVersion.A),
                    cancellationToken);
                await DotStatDb.DropTable(DotStatDb.DataSchema, dsd.MariaDbDimGroupAttrTable((char)DbTableVersion.B),
                    cancellationToken);
            }
        }

        protected override async Task DeleteAll(IImportReferenceableStructure referencedStructure, DbTableVersion tableVersion, bool includeSummary,
            ImportSummary importSummary, ICodeTranslator codeTranslator, DateTime dateTime,
            CancellationToken cancellationToken)
        {
            if (referencedStructure is not Dataflow dataflow)
            {
                //Dsd level delete all not required for data
                return;
            }

            var noTimeDimConstraint = Predicate.BuildWhereForObservationLevel(dataQuery: null, dataflow, codeTranslator, useExternalValues: false, useExternalColumnNames: false, includeTimeConstraints: false, serverType: MappingStore.DbType.MariaDb);
            var filteredTimeDim = new List<string>();
            Predicate.AddTimePredicate(filteredTimeDim, dataflow, null, null, serverType: MappingStore.DbType.MariaDb);

            var filterConstraintObs = !string.IsNullOrEmpty(noTimeDimConstraint) || filteredTimeDim.Any() ? "WHERE " : string.Empty;
            if (!string.IsNullOrEmpty(noTimeDimConstraint))
            {
                filterConstraintObs += $@" SID IN (
SELECT SID FROM {DotStatDb.DataSchema}_{dataflow.Dsd.MariaDbFilterTable()} 
WHERE {noTimeDimConstraint}
)";
            }

            if (filteredTimeDim.Any())
            {
                filterConstraintObs += !string.IsNullOrEmpty(noTimeDimConstraint) ? " AND " : "";
                filterConstraintObs += string.Join(" AND ", filteredTimeDim);
            }

            var mergeResult = await DeleteAll(dataflow.Dsd.MariaDbFactTable((char)tableVersion), filterConstraintObs, includeSummary, cancellationToken);

            if (mergeResult.Errors.Any())
            {
                importSummary.Errors.AddRange(mergeResult.Errors);
                return;
            }

            importSummary.ObservationLevelMergeResult.Add(mergeResult);

            if (dataflow.Dsd.Attributes.Any(a => a.Base.AttachmentLevel is AttributeAttachmentLevel.DimensionGroup or AttributeAttachmentLevel.Group))
            {
                var filterConstraintSeries = !string.IsNullOrEmpty(noTimeDimConstraint) ? $@"WHERE SID IN (
SELECT SID FROM {DotStatDb.DataSchema}_{dataflow.Dsd.MariaDbFilterTable()} 
WHERE {noTimeDimConstraint}
)" : string.Empty;

                mergeResult = await DeleteAll(dataflow.Dsd.MariaDbDimGroupAttrTable((char)tableVersion), filterConstraintSeries, includeSummary, cancellationToken);

                if (mergeResult.Errors.Any())
                {
                    importSummary.Errors.AddRange(mergeResult.Errors);
                    return;
                }

                importSummary.SeriesLevelMergeResult.Add(mergeResult);
            }

            if (dataflow.Dsd.Attributes.Any(a => a.Base.AttachmentLevel is AttributeAttachmentLevel.DataSet or AttributeAttachmentLevel.Null))
            {
                mergeResult = await DeleteAll(dataflow.Dsd.MariaDbDsdAttrTable((char)tableVersion), filterAllowedContentConstraint: null, includeSummary, cancellationToken);

                if (mergeResult.Errors.Any())
                {
                    importSummary.Errors.AddRange(mergeResult.Errors);
                    return;
                }

                importSummary.DataFlowLevelMergeResult.Add(mergeResult);
            }

            Log.Warn(LocalizationRepository.GetLocalisedResource(Localization.ResourceId.DeleteAllActionPerformed));

            await InsertDeletedAllRow(dataflow.DbId, dataflow.Dsd.MariaDbDeletedTable((char)tableVersion), dateTime, cancellationToken);

        }

        public async Task TruncateDsdTables(Dsd dsd, DbTableVersion tableVersion, CancellationToken cancellationToken)
        {
            await DotStatDb.TruncateTable(DotStatDb.DataSchema, dsd.MariaDbFactTable((char)tableVersion), cancellationToken);
            await DotStatDb.TruncateTable(DotStatDb.DataSchema, dsd.MariaDbDeletedTable((char)tableVersion), cancellationToken);
        }

        public async Task DeleteAllFromDsdTables(Dsd dsd, DbTableVersion tableVersion, CancellationToken cancellationToken)
        {
            await DotStatDb.DeleteAllFromTable(DotStatDb.DataSchema, dsd.MariaDbFactTable((char)tableVersion), cancellationToken);
            await DotStatDb.DeleteAllFromTable(DotStatDb.DataSchema, dsd.MariaDbDeletedTable((char)tableVersion), cancellationToken);
        }

        public async Task TruncateAttributeTables(Dsd dsd, DbTableVersion tableVersion, CancellationToken cancellationToken)
        {
            await DotStatDb.TruncateTable(DotStatDb.DataSchema, dsd.MariaDbDsdAttrTable((char)tableVersion), cancellationToken);
            await DotStatDb.TruncateTable(DotStatDb.DataSchema, dsd.MariaDbDimGroupAttrTable((char)tableVersion), cancellationToken);
        }

        public async Task DeleteAllFromAttributeTables(Dsd dsd, DbTableVersion tableVersion, CancellationToken cancellationToken)
        {
            await DotStatDb.DeleteAllFromTable(DotStatDb.DataSchema, dsd.MariaDbDsdAttrTable((char)tableVersion), cancellationToken);
            await DotStatDb.DeleteAllFromTable(DotStatDb.DataSchema, dsd.MariaDbDimGroupAttrTable((char)tableVersion), cancellationToken);
        }


        public MappingSetParams GetMappingSetParams(Dataflow dataflow, char? tableVersion)
        {
            var emptyDataQuery = this.GetEmptyDataViewQuery(dataflow);

            return new MappingSetParams()
            {
                IsMetadata = false,
                ActiveQuery = tableVersion.HasValue ? GetDataViewQuery(dataflow, tableVersion.Value) : emptyDataQuery,
                ReplaceQuery = tableVersion.HasValue ? GetReplacedViewQuery(dataflow, tableVersion.Value) : emptyDataQuery,
                DeleteQuery = tableVersion.HasValue ? GetDeletedDataViewQuery(dataflow, tableVersion.Value) : emptyDataQuery,
                IncludeHistoryQuery = tableVersion.HasValue ? GetIncludeHistoryViewQuery(dataflow, tableVersion.Value) : emptyDataQuery,
                OrderByTimeAsc = GetOrderByClause(dataflow),
                OrderByTimeDesc = GetOrderByClause(dataflow, false)
            };
        }

        public Task CheckAndCreateSourceViews(Dataflow dataflow, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        public Task CreateAndFillHprDataTables(Dataflow dataflow, char hprVersion, ICodeTranslator codeTranslator, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        public Task DropDataTablesOfHprVersion(Dataflow currentDataFlow, char hprVersion, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        private string GetDataViewQuery(Dataflow dataflow, char tableVersion)
        {
            var columns = new List<string>();

            foreach (var dim in dataflow.Dsd.Dimensions)
            {
                if (dim.Base.TimeDimension)
                {
                    columns.Add($"`TIME_PERIOD` AS `{dim.Code}`");
                    columns.Add($"`{MariaDbExtensions.MariaDbPeriodStart()}`");
                    columns.Add($"`{MariaDbExtensions.MariaDbPeriodEnd()}`");
                }
                else
                {
                    columns.Add($"`{dim.Code}`");
                }
            }

            columns.Add($"`{dataflow.Dsd.Base.PrimaryMeasure.Id}`");

            foreach (var attr in dataflow.Dsd.Attributes)
            {
                columns.Add($"`{attr.Code}`");
            }

            columns.Add($"`{MariaDbExtensions.LAST_UPDATED_COLUMN}`");

            return $"SELECT `SID`, {string.Join(",", columns)} FROM {DotStatDb.DataSchema}_{dataflow.MariaDbDataflowViewName(tableVersion)}";
        }

        public string GetReplacedViewQuery(Dataflow dataflow, char tableVersion)
        {
            var columns = new List<string>();

            foreach (var dim in dataflow.Dsd.Dimensions)
            {
                if (dim.Base.TimeDimension)
                {
                    columns.Add($"`TIME_PERIOD` AS `{dim.Code}`");
                    columns.Add($"`{MariaDbExtensions.MariaDbPeriodStart()}`");
                    columns.Add($"`{MariaDbExtensions.MariaDbPeriodEnd()}`");
                }
                else
                {
                    columns.Add($"`{dim.Code}`");
                }
            }

            columns.Add($"`{dataflow.Dsd.Base.PrimaryMeasure.Id}`");

            foreach (var attr in dataflow.Dsd.Attributes)
            {
                columns.Add($"`{attr.Code}`");
            }

            columns.Add($"`{MariaDbExtensions.LAST_UPDATED_COLUMN}`");

            return $"SELECT `SID`, {string.Join(",", columns)} FROM {DotStatDb.DataSchema}_{dataflow.MariaDbDataReplaceDataflowViewName(tableVersion)}";
        }

        public string GetIncludeHistoryViewQuery(Dataflow dataflow, char tableVersion)
        {
            var columns = new List<string>();

            foreach (var dim in dataflow.Dsd.Dimensions)
            {
                if (dim.Base.TimeDimension)
                {
                    columns.Add($"`TIME_PERIOD` AS `{dim.Code}`");
                    columns.Add($"`{MariaDbExtensions.MariaDbPeriodStart()}`");
                    columns.Add($"`{MariaDbExtensions.MariaDbPeriodEnd()}`");
                }
                else
                {
                    columns.Add($"`{dim.Code}`");
                }
            }

            columns.Add($"`{dataflow.Dsd.Base.PrimaryMeasure.Id}`");

            foreach (var attr in dataflow.Dsd.Attributes)
            {
                columns.Add($"`{attr.Code}`");
            }

            columns.Add($"`{MariaDbExtensions.LAST_UPDATED_COLUMN}`");

            columns.Add("`ValidTo`");

            return $"SELECT `SID`, {string.Join(",", columns)} FROM {DotStatDb.DataSchema}_{dataflow.MariaDbDataIncludeHistoryDataflowViewName(tableVersion)}";
        }

        public string GetDeletedDataViewQuery(Dataflow dataflow, char tableVersion)
        {
            var columns = new List<string>();

            foreach (var dim in dataflow.Dsd.Dimensions)
            {
                if (dim.Base.TimeDimension)
                {
                    columns.Add($"`TIME_PERIOD` AS `{dim.Code}`");
                    columns.Add($"`{MariaDbExtensions.MariaDbPeriodStart()}`");
                    columns.Add($"`{MariaDbExtensions.MariaDbPeriodEnd()}`");
                }
                else
                {
                    columns.Add($"`{dim.Code}`");
                }
            }

            columns.Add($"`{dataflow.Dsd.Base.PrimaryMeasure.Id}`");

            foreach (var attr in dataflow.Dsd.Attributes)
            {
                columns.Add($"`{attr.Code}`");
            }

            columns.Add($"`{MariaDbExtensions.LAST_UPDATED_COLUMN}`");

            return $"SELECT 0 as `SID`, {string.Join(",", columns)} FROM {DotStatDb.DataSchema}_{dataflow.MariaDbDeletedDataViewName(tableVersion)}";
        }

        public string GetEmptyDataViewQuery(Dataflow dataflow)
        {
            var columns = new List<string>();
            foreach (var dim in dataflow.Dsd.Dimensions)
            {
                if (dim.Base.TimeDimension)
                {
                    columns.Add($"NULL AS `{dim.Code}`");
                    columns.Add($"NULL AS `{MariaDbExtensions.MariaDbPeriodStart()}`");
                    columns.Add($"NULL AS `{MariaDbExtensions.MariaDbPeriodEnd()}`");
                }
                else
                {
                    columns.Add($"NULL AS `{dim.Code}`");
                }
            }

            columns.Add($"NULL AS `{dataflow.Dsd.Base.PrimaryMeasure.Id}`");

            foreach (var attr in dataflow.Dsd.Attributes)
            {
                columns.Add($"NULL AS `{attr.Code}`");
            }

            columns.Add($"NULL as `{MariaDbExtensions.LAST_UPDATED_COLUMN}`");

            if (dataflow.Dsd.KeepHistory)
                columns.Add("NULL as `ValidTo`");

            return "SELECT NULL AS `SID`, " + string.Join(",", columns);
        }

        public string GetOrderByClause(Dataflow dataflow, bool isTimeAscending = true)
        {
            var columns = new List<string> { "SID ASC" };

            if (dataflow.Dsd.TimeDimension != null)
            {
                columns.Add($"{MariaDbExtensions.MariaDbPeriodStart()} {(isTimeAscending ? "ASC" : "DESC")}");
                columns.Add($"{MariaDbExtensions.MariaDbPeriodEnd()} {(isTimeAscending ? "DESC" : "ASC")}");
            }

            return string.Join(",", columns);
        }

        public async Task<bool> DataTablesExist(Dsd dsd, CancellationToken cancellationToken)
        {
            return
                await DotStatDb.TableExists(dsd.MariaDbFactTableWithSchema((char)DbTableVersion.A), cancellationToken)
                || await DotStatDb.TableExists(dsd.MariaDbFactTableWithSchema((char)DbTableVersion.B), cancellationToken);
        }

        protected override async Task StoreDeleteInstructions(IImportReferenceableStructure referencedStructure, ReportedComponents reportedComponents,
            IList<DataSetAttributeRow> dataSetAttributeRows, DbTableVersion tableVersion, DateTime dateTime, CancellationToken cancellationToken)
        {
            if (referencedStructure is not Dataflow dataflow)
            {
                //Dsd level delete all not required for data
                return;
            }

            var dimensions = new List<string>();
            var components = new List<string>();

            if (reportedComponents.Dimensions.Any())
            {
                dimensions.Add(reportedComponents.Dimensions.ToMariaDbColumnList());
            }

            if (reportedComponents.TimeDimension != null)
            {
                dimensions.Add(MariaDbExtensions.MariaDbPeriodStart());
                dimensions.Add(MariaDbExtensions.MariaDbPeriodEnd());
            }

            if (reportedComponents.IsPrimaryMeasureReported)
            {
                components.Add(MariaDbExtensions.ValueColumn);
            }

            if (dataflow.Dsd.Attributes.Any())
            {
                components.AddRange(dataflow.Dsd.Attributes.Select(attr => attr.MariaDbColumn()));
            }

            var dsd = dataflow.Dsd;

            if (!dimensions.Any() && !components.Any())
                return;

            // Store deleted instructions as indicated by the user in the import file
            var dimensionsString = dimensions.Any() ? "," + string.Join(",", dimensions) : string.Empty;
            var componentsString = components.Any() ? "," + string.Join(",", components) : string.Empty;

            var MySqlParameters = new List<DbParameter>()
            {
                new MySqlParameter("DF_ID", dataflow.DbId),
                new MySqlParameter("Delete", (int) StagingRowActionEnum.Delete),
                new MySqlParameter("DT_Now", dateTime)
            };

            var sql = @$"INSERT INTO {DotStatDb.DataSchema}_{dsd.MariaDbDeletedTable((char)tableVersion)}(DF_ID,{MariaDbExtensions.LAST_UPDATED_COLUMN}{dimensionsString}{componentsString}) 
                SELECT @DF_ID,@DT_Now{dimensionsString}{componentsString} FROM {DotStatDb.DataSchema}_{dsd.MariaDbStagingTable()} 
                WHERE REQUESTED_ACTION = @Delete";

            await DotStatDb.ExecuteNonQuerySqlWithParamsAsync(sql, cancellationToken, MySqlParameters.ToArray());

            // Store fully empty rows in deletions table as a result of a partial delete or a replace to all nulls at observation level
            var attributesStoredAtObsLevel = dsd.Attributes
                .Where(x => (x.Base.AttachmentLevel == AttributeAttachmentLevel.Observation || x.Base.AttachmentLevel == AttributeAttachmentLevel.DimensionGroup || x.Base.AttachmentLevel == AttributeAttachmentLevel.Group)
                    && x.Base.GetDimensionReferences().Contains(DimensionObject.TimeDimensionFixedId));

            var attributeColumns = attributesStoredAtObsLevel.ToMariaDbColumnListWithIsNull();
            if (!string.IsNullOrWhiteSpace(attributeColumns))
            {
                attributeColumns += " AND ";
            }

            var selectColumns = attributesStoredAtObsLevel.ToMariaDbColumnList();
            if (!string.IsNullOrWhiteSpace(selectColumns))
            {
                selectColumns = "," + selectColumns;
            }
            selectColumns += ", VALUE";


            var selectPresentColumns = string.Join(", ", attributesStoredAtObsLevel.Select(attr => $"@PresentColumn AS {attr.MariaDbColumn()}"));
            if (!string.IsNullOrWhiteSpace(selectPresentColumns))
            {
                selectPresentColumns = "," + selectPresentColumns;
            }
            selectPresentColumns += ", @PresentColumn AS VALUE";

            MySqlParameters = new List<DbParameter>()
            {
                new MySqlParameter("DF_ID", dataflow.DbId),
                new MySqlParameter("DT_Now", DateTime.UtcNow),
                new MySqlParameter("PresentColumn", MariaDbExtensions.ColumnPresentDbValue.ToString())
            };

            var sqlCommand = $@"INSERT INTO {DotStatDb.DataSchema}_{dsd.MariaDbDeletedTable((char)tableVersion)}(DF_ID,{MariaDbExtensions.LAST_UPDATED_COLUMN}{dimensionsString}{selectColumns}) 
SELECT @DF_ID,@DT_Now{dimensionsString}{selectPresentColumns} 
FROM {DotStatDb.DataSchema}_{dsd.MariaDbFactTable((char)tableVersion)} FA
JOIN {DotStatDb.DataSchema}_{dsd.MariaDbFilterTable()} FI
ON FA.SID = FI.SID
WHERE {attributeColumns} VALUE IS NULL";
            await DotStatDb.ExecuteNonQuerySqlWithParamsAsync(sqlCommand, cancellationToken, MySqlParameters.ToArray());

        }

        #region Private methods

        private async Task BuildStagingTables(Dsd dsd, ReportedComponents reportedComponents, bool isTimeAtTimeDimensionSupported, CancellationToken cancellationToken)
        {
            //Dimensions
            //32 is the SQL limit of columns that can participate in a Unique index
            var rowId = reportedComponents.Dimensions.Count > 32 ? $",ROW_ID blob AS ({dsd.MariaDbBuildRowIdFormula()}) PERSISTENT" : null;

            var dimensionColumns = reportedComponents.Dimensions?.ToMariaDbColumnList(true, true);
            if (!string.IsNullOrWhiteSpace(dimensionColumns))
                dimensionColumns += ',';

            var timePeriodColumns = reportedComponents.TimeDimension is not null
                ? string.Join(",",
                      MariaDbExtensions.MariaDbPeriodStart(true, isTimeAtTimeDimensionSupported, true),
                      MariaDbExtensions.MariaDbPeriodEnd(true, isTimeAtTimeDimensionSupported, true)
                ) + "," : null;

            //Measure
            var measure = reportedComponents.IsPrimaryMeasureReported ? $"`VALUE` {dsd.PrimaryMeasure.GetMariaDbType(dsd.SupportsIntentionallyMissingValues)} NULL," : null;

            //Attributes
            var attributeColumns = dsd.Attributes.OrderBy(a => a.DbId).ToMariaDbColumnList(withType: true, applyNotNull: false);

            if (!string.IsNullOrWhiteSpace(attributeColumns))
                attributeColumns += ',';

            //Create table
            var sqlCommand = $@"CREATE TABLE {DotStatDb.DataSchema}_{dsd.MariaDbStagingTable()}(
                    {timePeriodColumns}
                    {dimensionColumns}
                    {measure}
                    {attributeColumns}
                    REQUESTED_ACTION TINYINT NOT NULL,
                    REAL_ACTION_FACT TINYINT NOT NULL,
                    REAL_ACTION_ATTR TINYINT NOT NULL,
                    BATCH_NUMBER INT NULL
                    {rowId}
                )";

            await DotStatDb.ExecuteNonQuerySqlAsync(sqlCommand, cancellationToken);
        }

        private void OnSqlRowsCopied(int rowsCopied, int batchNr)
        {
            Log.Notice(string.Format(
                LocalizationRepository.GetLocalisedResource(Localization.ResourceId.ProcessingObservations),
                rowsCopied, batchNr));
        }

        private async Task<MergeResult> ExecuteMerge(string sqlCommand, List<DbParameter> sqlParams, bool includeSummary, CancellationToken cancellationToken, Dsd dsd = null, ReportedComponents reportedComponents = null)
        {
            var mergeResult = new MergeResult();
            if (string.IsNullOrEmpty(sqlCommand))
                return mergeResult;

            try
            {
                if (includeSummary)
                {
                    await using var dbDataReader = await DotStatDb.ExecuteReaderSqlWithParamsAsync(sqlCommand, cancellationToken, commandBehavior: CommandBehavior.SingleRow, parameters: sqlParams.ToArray());

                    if (await dbDataReader.ReadAsync(cancellationToken))
                    {
                        mergeResult.InsertCount = dbDataReader.GetInt32(0);
                        mergeResult.UpdateCount = dbDataReader.GetInt32(1);
                        mergeResult.DeleteCount = dbDataReader.GetInt32(2);
                        mergeResult.TotalCount = dbDataReader.GetInt32(3);
                        mergeResult.RowsAddedDeleted = mergeResult.InsertCount > 0 || mergeResult.DeleteCount > 0;
                    }
                }
                else
                {
                    mergeResult.RowsAddedDeleted = await DotStatDb.ExecuteNonQuerySqlWithParamsAsync(sqlCommand, cancellationToken, sqlParams.ToArray()) > 0;
                }
            }
            catch (MySqlException e)
            {
                switch (e.Number)
                {
                    case 1062://duplicates in the staging table insert
                        mergeResult.Errors.Add(new ObservationError { Type = ValidationErrorType.DuplicatedRowsInStagingTable });
                        break;
                    default:
                        throw;

                }
            }

            return mergeResult;
        }

        private async Task<bool> StagingTableHasDuplicates(Dsd dsd, ReportedComponents reportedComponents, CancellationToken cancellationToken)
        {
            if (dsd is null || reportedComponents is null)
                return false;

            //Dimensions columns
            var noTimeDimensions = reportedComponents.Dimensions.Where(d => !d.Base.TimeDimension
                                                                            && dsd.Dimensions.Any(rd => rd.Code.Equals(d.Code, StringComparison.CurrentCultureIgnoreCase)));

            var dimensionColumnsNoTimeDimJoin =
                string.Join(", ", noTimeDimensions.Select(a => a.MariaDbColumn()));

            string timeDimColumns = null;
            if (reportedComponents.TimeDimension is not null)
            {
                timeDimColumns = "," + string.Join(",", MariaDbExtensions.MariaDbPeriodStart(), MariaDbExtensions.MariaDbPeriodEnd());
            }

            var sqlCommand = $@"SELECT TOP 1 1 FROM {DotStatDb.DataSchema}_{dsd.MariaDbStagingTable()}
GROUP BY {dimensionColumnsNoTimeDimJoin} {timeDimColumns}, REQUESTED_ACTION
HAVING COUNT(1) > 1";

            return ((int?)await DotStatDb.ExecuteScalarSqlAsync(sqlCommand, cancellationToken) ?? 0) > 0;
        }

        public Task CreateHprDataViews(Dataflow dataflow, char hprVersion, IDictionary<DbTableVersion, IReadOnlyDictionary<string, string>> datasetAttributeValues, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}