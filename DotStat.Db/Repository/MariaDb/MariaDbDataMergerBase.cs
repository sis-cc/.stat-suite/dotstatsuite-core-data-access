﻿using DotStat.Common.Configuration.Interfaces;
using DotStat.Db;
using DotStat.Db.DB;
using DotStat.DB.Util;
using MySqlConnector;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Threading;
using System.Threading.Tasks;


namespace DotStat.DB.Repository.MariaDb
{
    public abstract class MariaDbDataMergerBase : DataMergerBase<MariaDbDotStatDb>
    {
        protected MariaDbDataMergerBase(MariaDbDotStatDb dotStatDb, IGeneralConfiguration generalConfiguration) :
            base(dotStatDb, generalConfiguration)
        {
        }

        protected async Task<MergeResult> DeleteAll(string tableName, string filterAllowedContentConstraint, bool includeSummary, CancellationToken cancellationToken)
        {
            var mergeResult = new MergeResult();
            if (!includeSummary && string.IsNullOrEmpty(filterAllowedContentConstraint))
            {
                if (DotStatDb.DataSpace.KeepHistory)
                {
                    //Temporal tables do not allow Truncate commands
                    await DotStatDb.DeleteAllFromTable(DotStatDb.DataSchema, tableName, cancellationToken);
                }
                else
                {
                    await DotStatDb.TruncateTable(DotStatDb.DataSchema, tableName, cancellationToken);
                }
            }
            else
            {
                var sqlCommand = $@"DELETE FROM {DotStatDb.DataSchema}_{tableName}
{filterAllowedContentConstraint};";

                if (includeSummary)
                {
                    sqlCommand += " SELECT ROW_COUNT() AS ROW_COUNT;";

                    mergeResult.DeleteCount += (int)(long)await DotStatDb.ExecuteScalarSqlAsync(sqlCommand, cancellationToken);
                    mergeResult.TotalCount =
                        mergeResult.UpdateCount + mergeResult.DeleteCount + mergeResult.InsertCount;
                }
                else
                {
                    await DotStatDb.ExecuteNonQuerySqlAsync(sqlCommand, cancellationToken);
                }
            }

            return mergeResult;
        }

        protected async Task InsertDeletedAllRow(int dataFlowId, string tableName, DateTime dateTime, CancellationToken cancellationToken)
        {
            //Insert delete all row to deleted table
            var sqlCommand = $@"INSERT INTO {DotStatDb.DataSchema}_{tableName} (DF_ID,{MariaDbExtensions.LAST_UPDATED_COLUMN})
SELECT @DataflowId, @CurrentDateTime;";
            var MySqlParameters = new List<DbParameter>
            {
                new MySqlParameter("DataflowId", MySqlDbType.Int32) { Value = dataFlowId },
                new MySqlParameter("CurrentDateTime", MySqlDbType.DateTime) { Value = dateTime }
            };

            await DotStatDb.ExecuteNonQuerySqlWithParamsAsync(sqlCommand, cancellationToken, MySqlParameters.ToArray());
        }
    }
}
