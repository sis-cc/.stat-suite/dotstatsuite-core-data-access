﻿using DotStat.Common.Configuration.Interfaces;
using DotStat.Common.Localization;
using DotStat.Common.Logger;
using DotStat.Db;
using DotStat.Db.DB;
using DotStat.Db.Exception;
using DotStat.Db.Reader;
using DotStat.Db.Repository;
using DotStat.Db.Util;
using DotStat.Db.Validation;
using DotStat.DB.Util;
using DotStat.Domain;
using MySqlConnector;
using Org.Sdmxsource.Sdmx.Api.Model.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Transactions;
using DbType = DotStat.MappingStore.DbType;
using MySqlDbType = MySqlConnector.MySqlDbType;
using MySqlException = MySqlConnector.MySqlException;
using MySqlParameter = MySqlConnector.MySqlParameter;

namespace DotStat.DB.Repository.MariaDb
{
    public class MariaDbMetadataStoreRepository : MariaDbDataMergerBase, IMetadataStoreRepository, IDataMerger
    {
        public MariaDbMetadataStoreRepository(MariaDbDotStatDb DotStatDb, IGeneralConfiguration generalConfiguration) :
            base(DotStatDb, generalConfiguration)
        {
        }

        public async Task<ImportSummary> MergeStagingTable(IImportReferenceableStructure referencedStructure, ReportedComponents reportedComponents, Dictionary<int, BatchAction> obsLevelBatchActions, IList<DataSetAttributeRow> dataSetAttributeRows, CodeTranslator translator,
            DbTableVersion tableVersion, bool includeSummary, CancellationToken cancellationToken)
        {
            var importSummary = new ImportSummary
            {
                //ObservationsCount = bulkImportResult.RowsCopied,
                Errors = new List<IValidationError>()
            };

            if (!obsLevelBatchActions.Any() && !dataSetAttributeRows.Any())
                return importSummary;

            var hasMerges = obsLevelBatchActions.Any(a => a.Value.Action == StagingRowActionEnum.Merge);
            var hasReplaces = obsLevelBatchActions.Any(a => a.Value.Action == StagingRowActionEnum.Replace);
            var hasDeletions = obsLevelBatchActions.Any(a => a.Value.Action == StagingRowActionEnum.Delete);
            var hasDeleteAll = obsLevelBatchActions.Any(a => a.Value.Action == StagingRowActionEnum.DeleteAll);

            if (hasMerges || hasReplaces)
            {
                //Insert new series found in the import file
                await MergeStagingToDimensionsTable(referencedStructure, reportedComponents, cancellationToken);
            }

            using (TransactionScope scope = DotStatDb.GetTransactionScope())
            {
                //Use the same datetime for the LAST_UPDATED column for all changes within the same import/transfer

                var dateTime = DateTime.UtcNow;
                //Merge non dataset level components 
                foreach (var (_, batchAction) in obsLevelBatchActions)
                {
                    var action = batchAction.Action;

                    //Special case DeleteAll 
                    if (action is StagingRowActionEnum.DeleteAll)
                    {
                        await DeleteAll(referencedStructure, tableVersion, includeSummary, importSummary, translator, dateTime, cancellationToken);

                        importSummary.RequiresRecalculationOfActualContentConstraint = true;
                        if (importSummary.Errors.Count > 0)
                            return importSummary;
                    }
                    else
                    {
                        //Process observation level components (primary measure and attributes which reference all dimensions, including time)
                        var mergeResult = await MergeStagingToObservationsTable(referencedStructure, reportedComponents, tableVersion,
                            batchAction, includeSummary, translator, dateTime, cancellationToken);

                        if (mergeResult.Errors.Any())
                        {
                            importSummary.Errors.AddRange(mergeResult.Errors);
                            return importSummary;
                        }

                        importSummary.RequiresRecalculationOfActualContentConstraint = importSummary.RequiresRecalculationOfActualContentConstraint || mergeResult.RowsAddedDeleted;
                        importSummary.ObservationLevelMergeResult.Add(mergeResult);
                       
                    }
                }

                //Merge  dataset level components 
                foreach (var dataSetAttributeRow in dataSetAttributeRows)
                {
                    var action = dataSetAttributeRow.Action;
                    hasReplaces = hasReplaces || action == StagingRowActionEnum.Replace;
                    hasDeletions = hasDeletions || action == StagingRowActionEnum.Delete;
                    hasDeleteAll = hasDeleteAll || action == StagingRowActionEnum.DeleteAll;

                    //Special case DeleteAll 
                    if (action is StagingRowActionEnum.DeleteAll)
                        continue;

                    var mergeResult = await MergeStagingToDatasetTable(referencedStructure, dataSetAttributeRow.Attributes,
                                    translator, tableVersion, action, includeSummary, dateTime, cancellationToken);

                    if (mergeResult.Errors.Any())
                    {
                        importSummary.Errors.AddRange(mergeResult.Errors);
                        return importSummary;
                    }

                    importSummary.DataFlowLevelMergeResult.Add(mergeResult);
                }

                //Store delete instructions
                if (hasReplaces || hasDeletions || hasDeleteAll)
                {
                    await StoreDeleteInstructions(
                        referencedStructure,
                        reportedComponents,
                        dataSetAttributeRows,
                        tableVersion,
                        dateTime,
                        cancellationToken
                    );
                }

                //Merge action cannot set components to NULL, no cleanup needed
                //Merge & Replace actions cannot set components to NULL at series not dataset level, no cleanup needed
                if (hasReplaces || hasDeletions)
                {
                    //Clean up fully deleted observations
                    var observationsRemoved = await CleanUpFullyEmptyObservations(referencedStructure, tableVersion, cancellationToken);
                    importSummary.RequiresRecalculationOfActualContentConstraint = importSummary.RequiresRecalculationOfActualContentConstraint || observationsRemoved;
                }

                scope.Complete();
            }

            return importSummary;
        }

        public async Task<BulkImportResult> BulkInsertMetadata(
            IAsyncEnumerable<ObservationRow> observations,
            ReportedComponents reportedComponents,
            ICodeTranslator translator,
            IImportReferenceableStructure referencedStructure,
            bool fullValidation, bool isTimeAtTimeDimensionSupported,
            CancellationToken cancellationToken)
        {
            const int batchSize = 30000;
            int rowsCopied = 0;
            int batchNr = 0;
            List<IValidationError> allErrors = new List<IValidationError>();
            Dictionary<int, DataSetAttributeRow> dataSetAttributeRows = new Dictionary<int, DataSetAttributeRow>();
            Dictionary<int, BatchAction> allBatchActions = new Dictionary<int, BatchAction>();

            await using (var connection = DotStatDb.GetConnectionAllowLoadLocal())
            {
                var bulkCopy = new MySqlBulkCopy(connection, null);

                bulkCopy.DestinationTableName = $"{DotStatDb.DataSchema}_{referencedStructure.Dsd.MariaDbMetadataStagingTable()}";
                bulkCopy.BulkCopyTimeout = DotStatDb.DatabaseCommandTimeout;

                await foreach (var batch in observations.Buffer(batchSize, cancellationToken))
                {
                    batchNr++;
                    var metadataReader = new SdmxMetadataObservationReader(batch.ToAsyncEnumerable(),
                        reportedComponents, referencedStructure.Dsd, translator, GeneralConfiguration, fullValidation,
                        isTimeAtTimeDimensionSupported);

                    //Add previously processed DataSetAttributes
                    metadataReader.DatasetMetadataAttributes = dataSetAttributeRows;

                    await bulkCopy.WriteToServerAsync(metadataReader, cancellationToken);

                    //Get errors from batch
                    var errors = metadataReader.GetErrors();
                    if (errors != null && errors.Any())
                    {
                        allErrors.AddRange(errors);
                    }

                    rowsCopied += metadataReader.GetObservationsCount();
                    //Merge current batch actions with result
                    foreach (var batchAction in metadataReader.GetBatchActions())
                    {
                        allBatchActions.TryAdd(batchAction.Key, batchAction.Value);
                    }

                    dataSetAttributeRows = metadataReader.DatasetMetadataAttributes;

                    //Logs status
                    OnSqlRowsCopied(rowsCopied, batchNr);
                }

                Log.Notice(string.Format(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.ReadingMetaSourceFinished),
                    rowsCopied));

                var allDatasetMetadataAttributesRows = dataSetAttributeRows.Select(d => d.Value).ToList();

                return new BulkImportResult(allDatasetMetadataAttributesRows, allErrors, rowsCopied, allBatchActions);
            }
        }

        public async Task DropMetadataStagingTables(Dsd dsd, CancellationToken cancellationToken)
        {
            await DotStatDb.DropTable(DotStatDb.DataSchema, dsd.MariaDbMetadataStagingTable(), cancellationToken);
        }

        public async Task DropMetadataStagingTables(int dsdDbId, CancellationToken cancellationToken)
        {
            await DotStatDb.DropTable(DotStatDb.DataSchema, MariaDbExtensions.MariaDbMetadataStagingTable(dsdDbId), cancellationToken);
        }

        public async Task RecreateMetadataStagingTables(Dsd dsd, CancellationToken cancellationToken)
        {
            await DropMetadataStagingTables(dsd, cancellationToken);
            await BuildMetadataStagingTables(dsd, cancellationToken);
        }

        public async Task AddIndexMetadataStagingTable(Dsd dsd, CancellationToken cancellationToken)
        {
            if (dsd?.Msd == null)
            {
                return;
            }

            var tableName = dsd.MariaDbMetadataStagingTable();
            var nonTimeDims = dsd.Dimensions
                .Where(x => !x.Base.TimeDimension)
                .ToArray();

            var metadataAttributesColumns = dsd.Msd.MetadataAttributes.ToMariaDbColumnList(true);

            var dimensionColumns = nonTimeDims.ToMariaDbColumnList();
            var timeDimColumns = dsd.TimeDimension != null
                ? "," + string.Join(",", dsd.TimeDimension.MariaDbColumn(), MariaDbExtensions.MariaDbPeriodStart(),
                    MariaDbExtensions.MariaDbPeriodEnd())
                : null;

            var indexColumns = dsd.Dimensions.Count > 32 ? "ROW_ID" : $"{dimensionColumns}{timeDimColumns}";
            var indexName = dsd.Dimensions.Count > 32 ? $"idx_ROW_ID_{dsd.Msd.DbId}" : $"idx_dimensions_{dsd.Msd.DbId}";

            await DotStatDb.ExecuteNonQuerySqlAsync($@"CREATE INDEX {indexName} 
                ON {DotStatDb.DataSchema}_{tableName}({indexColumns}{(!string.IsNullOrEmpty(metadataAttributesColumns) ? $",{metadataAttributesColumns}" : "")}",
                cancellationToken);
        }

        public async Task AddUniqueIndexMetadataStagingTable(Dsd dsd, CancellationToken cancellationToken)
        {
            if (dsd.Msd == null)
            {
                return;
            }

            var tableName = dsd.MariaDbMetadataStagingTable();
            var dimensionColumns = dsd.Dimensions.ToMariaDbColumnList();
            var indexColumns = dsd.Dimensions.Count > 32 ? "ROW_ID" : $"{dimensionColumns}";
            var indexName = dsd.Dimensions.Count > 32 ? $"UI_ROW_ID" : $"UI_dimensions";

            try
            {
                await DotStatDb.ExecuteNonQuerySqlAsync($@"CREATE UNIQUE INDEX {indexName} 
                ON {DotStatDb.DataSchema}_{tableName}({indexColumns})",
                    cancellationToken);
            }
            catch (MySqlException e)
            {
                switch (e.Number)
                {
                    case 1505://duplicates in the staging table insert
                        throw new ConsumerValidationException(new List<ValidationError> { new ObservationError() { Type = ValidationErrorType.DuplicatedRowsInMetadataStagingTable } });
                    default:
                        throw;
                }
            }
        }

        public async Task DeleteDsdMetadata(Dsd dsd, DbTableVersion tableVersion, CancellationToken cancellationToken)
        {
            await TruncateMetadataTables(dsd, tableVersion, cancellationToken);
        }

        public async Task DeleteDataFlowMetadata(Dataflow dataFlow, DbTableVersion tableVersion, CancellationToken cancellationToken)
        {
            await TruncateMetadataTables(dataFlow, tableVersion, cancellationToken);
        }

        public async ValueTask CopyDsdMetadataToNewVersion(Dsd dsd, DbTableVersion sourceTableVersion, DbTableVersion targetTableVersion, CancellationToken cancellationToken)
        {
            if (sourceTableVersion == targetTableVersion)
            {
                throw new ArgumentException($"The same value was provided for the parameters 'sourceDbTableVersion' and 'targetDbTableVersion'");
            }

            if (dsd?.Msd == null)
            {
                return;
            }

            var dimensionColumns = dsd.Dimensions.Where(x => !x.Base.TimeDimension).ToMariaDbColumnList();
            var timeDim = dsd.TimeDimension;
            var timeDimColumns = timeDim != null
                ? "," + string.Join(",", timeDim.MariaDbColumn(), MariaDbExtensions.MariaDbPeriodStart(), MariaDbExtensions.MariaDbPeriodEnd())
                : null;

            var metadataAttributes = dsd.Msd.MetadataAttributes;

            var metadataAttributeColumns = metadataAttributes.Any()
                ? "," + metadataAttributes.ToMariaDbColumnList()
                : null;

            await DotStatDb.ExecuteNonQuerySqlAsync(
                $@" INSERT
                      INTO {DotStatDb.DataSchema}_{dsd.MariaDbMetadataDataStructureTable((char)targetTableVersion)} 
                      ({dimensionColumns}{timeDimColumns}{metadataAttributeColumns}, {MariaDbExtensions.LAST_UPDATED_COLUMN})
                    SELECT {dimensionColumns}{timeDimColumns}{metadataAttributeColumns}, {MariaDbExtensions.LAST_UPDATED_COLUMN}
                      FROM {DotStatDb.DataSchema}_{dsd.MariaDbMetadataDataStructureTable((char)sourceTableVersion)}",
                cancellationToken
            );

            await DotStatDb.ExecuteNonQuerySqlAsync(
                $@" INSERT
                      INTO {DotStatDb.DataSchema}_{dsd.MariaDbMetadataDataSetTable((char)targetTableVersion)} 
                      (DF_ID{metadataAttributeColumns}, {MariaDbExtensions.LAST_UPDATED_COLUMN} )
                    SELECT DF_ID{metadataAttributeColumns}, {MariaDbExtensions.LAST_UPDATED_COLUMN} 
                      FROM {DotStatDb.DataSchema}_{dsd.MariaDbMetadataDataSetTable((char)sourceTableVersion)}
                      WHERE DF_ID = -1",
                cancellationToken
            );
        }

        public async ValueTask CopyDataFlowMetadataToNewVersion(Dataflow dataFlow, DbTableVersion sourceTableVersion, DbTableVersion targetTableVersion, CancellationToken cancellationToken)
        {
            if (sourceTableVersion == targetTableVersion)
            {
                throw new ArgumentException($"The same value was provided for the parameters 'sourceDbTableVersion' and 'targetDbTableVersion'");
            }

            if (dataFlow.Dsd?.Msd == null)
            {
                return;
            }

            var dimensionColumns = dataFlow.Dsd.Dimensions.Where(x => !x.Base.TimeDimension).ToMariaDbColumnList();
            var timeDim = dataFlow.Dsd.TimeDimension;
            var timeDimColumns = timeDim != null
                ? "," + string.Join(",", timeDim.MariaDbColumn(), MariaDbExtensions.MariaDbPeriodStart(), MariaDbExtensions.MariaDbPeriodEnd())
                : null;

            var metadataAttributes = dataFlow.Dsd.Msd.MetadataAttributes;

            var metadataAttributeColumns = metadataAttributes.Any()
                ? "," + metadataAttributes.ToMariaDbColumnList()
                : null;

            await DotStatDb.ExecuteNonQuerySqlAsync(
                $@" INSERT
                      INTO {DotStatDb.DataSchema}_{dataFlow.MariaDbMetadataDataflowTable((char)targetTableVersion)} 
                      ({dimensionColumns}{timeDimColumns}{metadataAttributeColumns}, {MariaDbExtensions.LAST_UPDATED_COLUMN})
                    SELECT {dimensionColumns}{timeDimColumns}{metadataAttributeColumns}, {MariaDbExtensions.LAST_UPDATED_COLUMN}
                      FROM {DotStatDb.DataSchema}_{dataFlow.MariaDbMetadataDataflowTable((char)sourceTableVersion)}",
                cancellationToken
            );

            await DotStatDb.ExecuteNonQuerySqlAsync(
                $@" INSERT
                      INTO {DotStatDb.DataSchema}_{dataFlow.Dsd.MariaDbMetadataDataSetTable((char)targetTableVersion)} 
                      (DF_ID{metadataAttributeColumns}, {MariaDbExtensions.LAST_UPDATED_COLUMN} )
                    SELECT DF_ID{metadataAttributeColumns}, {MariaDbExtensions.LAST_UPDATED_COLUMN} 
                      FROM {DotStatDb.DataSchema}_{dataFlow.Dsd.MariaDbMetadataDataSetTable((char)sourceTableVersion)}
                     WHERE DF_ID={dataFlow.DbId}",
                cancellationToken
            );
        }

        private async Task StoreDeleteInstructions(
            Dsd dsd,
            int dfId,
            string metadataTable,
            IList<DataSetAttributeRow> dataSetAttributeRows,
            DbTableVersion tableVersion,
            DateTime dateTime,
            CancellationToken cancellationToken)
        {
            var dimensions = new List<string>();

            if (dsd.Dimensions.Any())
            {
                dimensions.AddRange(dsd.Dimensions.Select(d => d.MariaDbColumn()));
            }
            var switchedOffDimParameters = new List<DbParameter>();
            var switchedOffDimParameters2 = new List<DbParameter>();
            for (var index = 0; index < dimensions.Count; index++)
            {
                switchedOffDimParameters.Add(new MySqlParameter($"pDim{index}", MariaDbExtensions.DimensionSwitchedOffDbValue.ToString()));
                switchedOffDimParameters2.Add(new MySqlParameter($"pDim{index}", MariaDbExtensions.DimensionSwitchedOffDbValue.ToString()));
            }

            var supportsDateTime = dsd.Base.HasSupportDateTimeAnnotation();
            if (dsd.TimeDimension != null)
            {
                dimensions.Add(MariaDbExtensions.MariaDbPeriodStart());
                switchedOffDimParameters.Add(new MySqlParameter($"pDim{MariaDbExtensions.MariaDbPeriodStart()}", MySqlDbType.DateTime) { Value = supportsDateTime ? DateTime.MaxValue : DateTime.MaxValue.Date });
                switchedOffDimParameters2.Add(new MySqlParameter($"pDim{MariaDbExtensions.MariaDbPeriodStart()}", MySqlDbType.DateTime) { Value = supportsDateTime ? DateTime.MaxValue : DateTime.MaxValue.Date });
                dimensions.Add(MariaDbExtensions.MariaDbPeriodEnd());
                switchedOffDimParameters.Add(new MySqlParameter($"pDim{MariaDbExtensions.MariaDbPeriodEnd()}", MySqlDbType.DateTime) { Value = supportsDateTime ? DateTime.MinValue : DateTime.MinValue.Date });
                switchedOffDimParameters2.Add(new MySqlParameter($"pDim{MariaDbExtensions.MariaDbPeriodEnd()}", MySqlDbType.DateTime) { Value = supportsDateTime ? DateTime.MinValue : DateTime.MinValue.Date });
            }

            var dimensionsString = string.Join(",", dimensions);
            var componentsString = dsd.Msd.MetadataAttributes.ToMariaDbColumnList();
            var mySqlParameters = new List<DbParameter>();
            if (dimensions.Any() && dsd.Msd.MetadataAttributes.Any())
            {
                mySqlParameters = new List<DbParameter>()
                {
                    new MySqlParameter("DF_ID", dfId),
                    new MySqlParameter("Delete", (int) StagingRowActionEnum.Delete),
                    new MySqlParameter("DT_Now", DateTime.UtcNow)
                };

                var sql = @$"INSERT INTO {DotStatDb.DataSchema}_{dsd.MariaDbDeletedMetadataTable((char)tableVersion)}(DF_ID,{MariaDbExtensions.LAST_UPDATED_COLUMN},{dimensionsString},{componentsString}) 
                    SELECT @DF_ID,@DT_Now,{dimensionsString},{componentsString} FROM {DotStatDb.DataSchema}_{dsd.MariaDbMetadataStagingTable()} 
                    WHERE REQUESTED_ACTION= @Delete";

                await DotStatDb.ExecuteNonQuerySqlWithParamsAsync(sql, cancellationToken, mySqlParameters.ToArray());
            }

            if (!dataSetAttributeRows.Any() ||
                dataSetAttributeRows.Any(x => x.Action == StagingRowActionEnum.DeleteAll))
            {
                return;
            }

            var datasetAttributesOfDds = dsd.Msd.MetadataAttributes.ToDictionary(x => x.HierarchicalId);
            foreach (var datasetRow in dataSetAttributeRows.Where(x => x.Action == StagingRowActionEnum.Delete))
            {
                mySqlParameters = new List<DbParameter>()
                {
                    new MySqlParameter("DF_ID", dfId),
                    new MySqlParameter("DT_Now", dateTime)
                };

                mySqlParameters.AddRange(switchedOffDimParameters);

                var components = new List<string>();
                foreach (var keyValue in datasetRow.Attributes)
                {
                    var value = !string.IsNullOrEmpty(keyValue.Code) ? MariaDbExtensions.ColumnPresentDbValue.ToString() : null;

                    var dsAttribute = datasetAttributesOfDds[keyValue.Concept];
                    components.Add(dsAttribute.MariaDbColumn());
                    mySqlParameters.Add(new MySqlParameter($"pComp{dsAttribute.DbId}", string.IsNullOrEmpty(value) ? DBNull.Value : value));
                }
                var sql = @$"INSERT INTO {DotStatDb.DataSchema}_{dsd.MariaDbDeletedMetadataTable((char)tableVersion)}(DF_ID,{MariaDbExtensions.LAST_UPDATED_COLUMN}, {dimensionsString},{string.Join(",", components)}) 
                    SELECT {string.Join(",", mySqlParameters.Select(x => "@" + x.ParameterName))}";

                await DotStatDb.ExecuteNonQuerySqlWithParamsAsync(sql, cancellationToken, mySqlParameters.ToArray());
            }

            // Store fully empty rows in deletions table as a result of a partial delete or a replace to all nulls
            var attributeColumnsIsNull = dsd.Msd.MetadataAttributes.ToMariaDbColumnListWithIsNull();
            var selectPresentColumns = string.Join(", ", dsd.Msd.MetadataAttributes.Select(attr => $"@PresentColumn AS {attr.MariaDbColumn()}"));

            mySqlParameters = new List<DbParameter>()
            {
                new MySqlParameter("DF_ID", dfId),
                new MySqlParameter("DT_Now", dateTime),
                new MySqlParameter("PresentColumn", MariaDbExtensions.ColumnPresentDbValue.ToString())
            };
            mySqlParameters.AddRange(switchedOffDimParameters2);

            var sqlCommand = $@"INSERT INTO {DotStatDb.DataSchema}_{dsd.MariaDbDeletedMetadataTable((char)tableVersion)}(DF_ID,{MariaDbExtensions.LAST_UPDATED_COLUMN},{dimensionsString},{componentsString}) 
SELECT @DF_ID,@DT_Now,{dimensionsString},{selectPresentColumns} 
FROM {DotStatDb.DataSchema}_{metadataTable} 
WHERE {attributeColumnsIsNull}
UNION ALL 
SELECT @DF_ID,@DT_Now,{string.Join(",", switchedOffDimParameters.Select(x => "@" + x.ParameterName))},{selectPresentColumns} 
FROM {DotStatDb.DataSchema}_{dsd.MariaDbMetadataDataSetTable((char)tableVersion)} 
WHERE {attributeColumnsIsNull} AND DF_ID=@DF_ID";
            await DotStatDb.ExecuteNonQuerySqlWithParamsAsync(sqlCommand, cancellationToken, mySqlParameters.ToArray());
        }

        protected override ValueTask MergeDsdStagingToDimensionsTable(Dsd dsd, ReportedComponents reportedComponents, CancellationToken cancellationToken) {
            return new ValueTask();
        }

        public override async ValueTask<MergeResult> MergeStagingToObservationsTable(IImportReferenceableStructure referencedStructure, 
            ReportedComponents reportedComponents, DbTableVersion tableVersion, BatchAction batchAction, bool includeSummary, 
            ICodeTranslator codeTranslator, DateTime dateTime, CancellationToken cancellationToken)
        {
            if (referencedStructure is Dataflow dataFlow)
            {
                //Filter allowed content constraint
                var constraint = batchAction.Action == StagingRowActionEnum.Delete ?
                    Predicate.BuildWhereForObservationLevel(dataQuery: null, dataFlow, codeTranslator, useExternalValues: false, useExternalColumnNames: false, nullTreatment: NullTreatment.IncludeNullsAndSwitchOff, tableAlias: "ME", serverType: DbType.MariaDb)
                    : string.Empty;
                if (!string.IsNullOrEmpty(constraint))
                    constraint = $"AND {constraint}";

                var metadataTableName = $"{DotStatDb.DataSchema}_{dataFlow.MariaDbMetadataDataflowTable((char)tableVersion)}";

                return await MergeStagingToObservationsTable(dataFlow.Dsd, constraint, metadataTableName, reportedComponents,
                    batchAction, includeSummary, dateTime, cancellationToken);
            }
            else
            {
                var dsd = referencedStructure as Dsd;
                var metadataTableName = $"{DotStatDb.DataSchema}_{dsd.SqlMetadataDataStructureTable((char)tableVersion)}";

                return await MergeStagingToObservationsTable(dsd, "", metadataTableName, reportedComponents,
                    batchAction, includeSummary, dateTime, cancellationToken);
            }
        }

        private async ValueTask<MergeResult> MergeStagingToObservationsTable(Dsd dsd, string constraint, string metadataTableName, ReportedComponents reportedComponents,
            BatchAction batchAction, bool includeSummary, DateTime dateTime, CancellationToken cancellationToken)
        {
            var action = batchAction.Action;
            var batchNumber =  batchAction.BatchNumber;
            var mergeResult = new MergeResult();

            if (action is StagingRowActionEnum.Skip || action is StagingRowActionEnum.DeleteAll || dsd?.Msd == null)
                return mergeResult;

            if (!reportedComponents.MetadataAttributes.Any() && action != StagingRowActionEnum.Replace)
                return mergeResult;

            //Dimensions
            var nonTimeDims = dsd.Dimensions
               .Where(x => !x.Base.TimeDimension)
               .ToArray();

            var dimensionColumnsNoTimeDim = nonTimeDims.ToMariaDbColumnList();
            var dimensionColumnsNoTimeInsert = string.Join(" AND ", nonTimeDims.Select(dim => $"ME.{dim.MariaDbColumn()} IS NULL"));
            
            var dimensionColumnsNoTimeDimJoin = string.Join(" AND ", nonTimeDims.Select(
                dim => dim.Codelist != null
                    ? string.Format("(ME.{0} = COALESCE(ST.{0}, ME.{0}) OR (ST.{0} = {1} AND ME.{0} != {2}))\n", dim.MariaDbColumn(), MariaDbExtensions.DimensionWildCardedDbValue, MariaDbExtensions.DimensionSwitchedOffDbValue)
                    : string.Format("(ME.{0} = COALESCE(ST.{0}, ME.{0}) OR (ST.{0} = '{1}' AND ME.{0} != '{2}'))\n", dim.MariaDbColumn(), MariaDbExtensions.DimensionWildCarded, MariaDbExtensions.DimensionSwitchedOff)
            ));

            var dimensionColumnsNoTimeDimInsertValues = string.Join(", ", nonTimeDims.Select(
                dim => dim.Codelist != null
                    ? $"COALESCE(ST.{dim.MariaDbColumn()}, {MariaDbExtensions.DimensionSwitchedOffDbValue})"
                    : $"COALESCE(ST.{dim.MariaDbColumn()}, '{MariaDbExtensions.DimensionSwitchedOff}')"
            ));

            string timeDimColumns = null;
            string timeDimColumnsInsertValues = null;
            string timeDimColumnsJoin = null;
            var sqlParams = new List<DbParameter>();

            if (dsd.TimeDimension != null)
            {
                timeDimColumns = ", " + string.Join(", ", dsd.TimeDimension.MariaDbColumn(), MariaDbExtensions.MariaDbPeriodStart(), MariaDbExtensions.MariaDbPeriodEnd());
                timeDimColumnsInsertValues = ", " + string.Join(", ", $"ST.{dsd.TimeDimension.MariaDbColumn()}", $"COALESCE(ST.{MariaDbExtensions.MariaDbPeriodStart()}, @maxDT)", $"COALESCE(ST.{MariaDbExtensions.MariaDbPeriodEnd()}, @minDT)");
                timeDimColumnsJoin =
                    string.Format(" AND (ME.{0} = COALESCE(ST.{0}, ME.{0}) OR (ST.{0} = @maxDTmin1Year AND ME.{0} != @maxDT))\n", MariaDbExtensions.MariaDbPeriodStart()) +
                    string.Format("AND (ME.{0} = COALESCE(ST.{0}, ME.{0}) OR (ST.{0} = @minDTplus1Year AND ME.{0} != @minDT))\n", MariaDbExtensions.MariaDbPeriodEnd());

                var supportsDateTime = dsd.Base.HasSupportDateTimeAnnotation();

                sqlParams.Add(new MySqlParameter("maxDTmin1Year", MySqlDbType.DateTime) { Value = supportsDateTime ? DateTime.MaxValue.AddYears(-1) : DateTime.MaxValue.Date.AddYears(-1) });
                sqlParams.Add(new MySqlParameter("maxDT", MySqlDbType.DateTime) { Value = supportsDateTime ? DateTime.MaxValue : DateTime.MaxValue.Date });
                sqlParams.Add(new MySqlParameter("minDTplus1Year", MySqlDbType.DateTime) { Value = supportsDateTime ? DateTime.MinValue.AddYears(1) : DateTime.MinValue.Date.AddYears(1) });
                sqlParams.Add(new MySqlParameter("minDT", MySqlDbType.DateTime) { Value = supportsDateTime ? DateTime.MinValue : DateTime.MinValue.Date });
            }

            string updateDiffCheck = null;

            //Metadata Attributes
            var reportedAttributes = dsd.Msd.MetadataAttributes
                .Where(a =>
                    reportedComponents.MetadataAttributes.Any(r =>
                        r.HierarchicalId.Equals(a.HierarchicalId, StringComparison.InvariantCultureIgnoreCase))).ToList();

            string attrColumnsSelect = null;
            string attrColumnsInsert = null;
            string attrColumnsInsertValues = null;
            string attrColumnsUpdate = null;
            switch (action)
            {
                case StagingRowActionEnum.Merge:
                    attrColumnsSelect = reportedAttributes.ToMariaDbColumnList();
                    attrColumnsInsert = reportedAttributes.ToMariaDbColumnList();
                    attrColumnsInsertValues = reportedAttributes.ToMariaDbColumnListWithTablePrefix(table: "ST");
                    attrColumnsUpdate = string.Join(",\n", reportedAttributes
                        .Select(a => $"ME.{a.MariaDbColumn()} = CASE WHEN ST.{a.MariaDbColumn()} IS NOT NULL THEN ST.{a.MariaDbColumn()} ELSE ME.{a.MariaDbColumn()} END"));
                    updateDiffCheck = string.Join(" OR\n", reportedAttributes
                        .Select(a => $"(ST.{a.MariaDbColumn()} IS NOT NULL AND ({a.MariaDbCollate($"ME.{a.MariaDbColumn()}")} <> {a.MariaDbCollate($"ST.{a.MariaDbColumn()}")} OR ME.{a.MariaDbColumn()} IS NULL))"));
                    break;
                case StagingRowActionEnum.Delete:
                    attrColumnsSelect = reportedAttributes.ToMariaDbColumnList();
                    attrColumnsInsert = reportedAttributes.ToMariaDbColumnList();
                    attrColumnsUpdate = string.Join(",\n", reportedAttributes
                        .Select(a => $"ME.{a.MariaDbColumn()} = CASE WHEN ST.{a.MariaDbColumn()} IS NOT NULL THEN NULL ELSE ME.{a.MariaDbColumn()} END"));
                    updateDiffCheck = string.Join(" OR\n", reportedAttributes
                        .Select(a => $"(ST.{a.MariaDbColumn()} IS NOT NULL AND ME.{a.MariaDbColumn()} IS NOT NULL)"));
                    break;
                case StagingRowActionEnum.Replace:
                    var allAttributesWithInclusion = dsd.Msd.MetadataAttributes
                        .Select(attribute => (reportedComponents.MetadataAttributes.Any(
                            r => r.HierarchicalId.Equals(attribute.HierarchicalId, StringComparison.InvariantCultureIgnoreCase)), attribute))
                        .ToList();

                    attrColumnsSelect = string.Join(",", allAttributesWithInclusion
                            .Select(a => a.Item1 ? a.attribute.MariaDbColumn() : $"NULL AS {a.attribute.MariaDbColumn()}"));
                    attrColumnsInsert = allAttributesWithInclusion.Select(a => a.attribute).ToMariaDbColumnList();
                    attrColumnsInsertValues = allAttributesWithInclusion.Select(a => a.attribute).ToMariaDbColumnListWithTablePrefix(table: "ST");
                    attrColumnsUpdate = string.Join(",\n", allAttributesWithInclusion
                        .Select(a => @$"ME.{a.attribute.MariaDbColumn()} =  {(a.Item1 ? $"ST.{a.attribute.MariaDbColumn()}" : "NULL")}"));
                    updateDiffCheck = string.Join(" OR\n", allAttributesWithInclusion
                        .Select(a => a.Item1 ?
                            $"((ST.{a.attribute.MariaDbColumn()} IS NOT NULL AND ME.{a.attribute.MariaDbColumn()} IS NULL) OR (ST.{a.attribute.MariaDbColumn()} IS NULL AND ME.{a.attribute.MariaDbColumn()} IS NOT NULL) OR {a.attribute.MariaDbCollate($"ME.{a.attribute.MariaDbColumn()}")} <> {a.attribute.MariaDbCollate($"ST.{a.attribute.MariaDbColumn()}")})"
                            : $"ME.{a.attribute.MariaDbColumn()} IS NOT NULL"));
                    break;
            };

            if (!string.IsNullOrEmpty(updateDiffCheck))
                updateDiffCheck = $"AND ({updateDiffCheck})";

            //Calculate summary
            var summaryStatement = string.Empty;
            if (includeSummary)
            {
                summaryStatement = action == StagingRowActionEnum.Delete ?
                "SELECT 0 as INSERTED, @updated as UPDATED, @rowcount - @updated as DELETED, @rowcount as TOTAL;" :
                "SELECT @rowcount - @updated as INSERTED, @updated as UPDATED, 0 AS DELETED, @rowcount as TOTAL;";
            }
            else
            {
                summaryStatement = "SELECT CASE WHEN @rowcount > 0 THEN 1 ELSE 0 END;";
            }

            var stagingTableName = $"{DotStatDb.DataSchema}_{dsd.MariaDbMetadataStagingTable()}";

            var actionStatement = action == StagingRowActionEnum.Delete ?
//Delete actions
$@"
UPDATE {metadataTableName} AS ME 
INNER JOIN ST ON 
	ST.REAL_ACTION = @Merge 
	{(string.IsNullOrEmpty(dimensionColumnsNoTimeDimJoin) ? null : $" AND {dimensionColumnsNoTimeDimJoin}")}
	{timeDimColumnsJoin}
	{constraint}
	{updateDiffCheck}
SET {attrColumnsUpdate},
    {MariaDbExtensions.LAST_UPDATED_COLUMN} = @CurrentDateTime;

SET @updated := ROW_COUNT();
SET @rowcount := @updated;

DELETE ME FROM ST
INNER JOIN {metadataTableName} AS ME ON 
	{dimensionColumnsNoTimeDimJoin}
	{timeDimColumnsJoin}
	{constraint}
WHERE ST.REAL_ACTION = @Delete;

SET @rowcount := (@rowcount + ROW_COUNT());" :
// Replace and Merge actions
$@"
UPDATE {metadataTableName} AS ME
INNER JOIN ST ON
    {dimensionColumnsNoTimeDimJoin}
    {timeDimColumnsJoin}
    {constraint}
    {updateDiffCheck}
SET 
    {attrColumnsUpdate},
    {MariaDbExtensions.LAST_UPDATED_COLUMN} = @CurrentDateTime;

SET @updated := ROW_COUNT();
SET @rowcount := @updated;

INSERT INTO {metadataTableName} ({dimensionColumnsNoTimeDim}{timeDimColumns},{attrColumnsInsert}, {MariaDbExtensions.LAST_UPDATED_COLUMN})
SELECT {dimensionColumnsNoTimeDimInsertValues}{timeDimColumnsInsertValues},{attrColumnsInsertValues}, @CurrentDateTime 
FROM ST
LEFT JOIN {metadataTableName} AS ME ON 
    {dimensionColumnsNoTimeDimJoin}
    {timeDimColumnsJoin}
    {constraint}
WHERE {dimensionColumnsNoTimeInsert};

SET @rowcount := (@rowcount + ROW_COUNT());";

            var sqlCommand = $@"
SET @rowcount := 0;
SET @updated := 0;

DROP TEMPORARY TABLE IF EXISTS ST;
CREATE TEMPORARY TABLE ST AS
    SELECT {dimensionColumnsNoTimeDim}{timeDimColumns},{attrColumnsSelect}, REAL_ACTION FROM {stagingTableName} 
    WHERE BATCH_NUMBER = @BatchNumber AND REAL_ACTION <> @Skip;

{actionStatement}
{summaryStatement}";

            //Add Sql parameters
            sqlParams.AddRange(new List<DbParameter>
            {
                new MySqlParameter("CurrentDateTime", MySqlDbType.DateTime) { Value = dateTime },
                new MySqlParameter("Merge", MySqlDbType.Int32) { Value = (int)StagingRowActionEnum.Merge },
                new MySqlParameter("Delete", MySqlDbType.Int32) { Value = (int)StagingRowActionEnum.Delete },
                new MySqlParameter("Skip", MySqlDbType.Int32) { Value = (int)StagingRowActionEnum.Skip },
                new MySqlParameter("BatchNumber", MySqlDbType.Int32) { Value = batchNumber }
            });

            return await ExecuteMerge(sqlCommand, sqlParams, includeSummary, cancellationToken);
        }

        public override ValueTask<MergeResult> MergeStagingToSeriesTable(IImportReferenceableStructure referencedStructure, ReportedComponents reportedComponents,
            DbTableVersion tableVersion, BatchAction batchAction, bool includeSummary, ICodeTranslator codeTranslator, DateTime dateTime, CancellationToken cancellationToken)
        {
            return new ValueTask<MergeResult>(new MergeResult());
        }

        public override async ValueTask<MergeResult> MergeStagingToDatasetTable(IImportReferenceableStructure referencedStructure,
            IList<IKeyValue> attributes, CodeTranslator codeTranslator, DbTableVersion tableVersion, StagingRowActionEnum action,
            bool includeSummary, DateTime dateTime, CancellationToken cancellationToken)
        {
            if (referencedStructure is Dataflow dataFlow)
            {
                var metadataTableName = $"{DotStatDb.DataSchema}_{dataFlow.Dsd.MariaDbMetadataDataSetTable((char)tableVersion)}";

                return await MergeStagingToDatasetTable(dataFlow.Dsd, dataFlow.DbId, metadataTableName,
                    attributes, action, includeSummary, dateTime, cancellationToken);
            }
            else
            {
                var dsd = referencedStructure as Dsd;
                var metadataTableName = $"{DotStatDb.DataSchema}_{dsd.MariaDbMetadataDataSetTable((char)tableVersion)}";

                return await MergeStagingToDatasetTable(dsd, -1, metadataTableName,
                    attributes, action, includeSummary, dateTime, cancellationToken);
            }
        }

        protected override async Task StoreDeleteInstructions(
            IImportReferenceableStructure referencedStructure,
            ReportedComponents reportedComponents,
            IList<DataSetAttributeRow> dataSetAttributeRows,
            DbTableVersion tableVersion,
            DateTime dateTime,
            CancellationToken cancellationToken)
        {
            if (referencedStructure is Dataflow dataFlow)
            {
                var metadataTable = dataFlow.MariaDbMetadataDataflowTable((char)tableVersion);
                await StoreDeleteInstructions(dataFlow.Dsd, dataFlow.DbId, metadataTable, dataSetAttributeRows, tableVersion,
                    dateTime, cancellationToken);
            }
            else
            {
                var dsd = referencedStructure as Dsd;
                var metadataTable = dsd.MariaDbMetadataDataStructureTable((char)tableVersion);
                await StoreDeleteInstructions(dsd, -1, metadataTable, dataSetAttributeRows, tableVersion,
                    dateTime, cancellationToken);
            }
        }

        private async ValueTask<MergeResult> MergeStagingToDatasetTable(Dsd dsd, int dfId, string metadataTableName, IList<IKeyValue> attributes, 
            StagingRowActionEnum action, bool includeSummary, DateTime dateTime, CancellationToken cancellationToken)
        {
            var mergeResult = new MergeResult();

            if (action is StagingRowActionEnum.Skip or StagingRowActionEnum.DeleteAll)
            {
                return mergeResult;
            }

            if (!attributes.Any() && action != StagingRowActionEnum.Replace)
            {
                return mergeResult;
            }


            var referencedMetadataAttributes = new Dictionary<MetadataAttribute, object>();
            foreach (var attributeKeyValue in attributes)
            {
                var attribute = dsd.Msd.MetadataAttributes.First(a => a.HierarchicalId.Equals(attributeKeyValue.Concept, StringComparison.InvariantCultureIgnoreCase));

                object attributeValue;
                if (string.IsNullOrWhiteSpace(attributeKeyValue.Code))
                {
                    attributeValue = (object)DBNull.Value;
                }
                else
                {
                    attributeValue = action == StagingRowActionEnum.Delete
                        ? MariaDbExtensions.ColumnPresentDbValue.ToString()
                        : attributeKeyValue.Code;
                }

                referencedMetadataAttributes[attribute] = attributeValue;
            }

            var allEmptyOrNull = referencedMetadataAttributes.Values.Distinct().Count() == 1;

            StagingRowActionEnum realAction;
            if (action == StagingRowActionEnum.Delete && 
                (referencedMetadataAttributes.Count == 0 || (allEmptyOrNull && referencedMetadataAttributes.Count == dsd.Msd.MetadataAttributes.Count)))
            {
                realAction = StagingRowActionEnum.Delete;
            }
            else
            {
                realAction = action == StagingRowActionEnum.Replace
                    ? StagingRowActionEnum.Replace
                    : StagingRowActionEnum.Merge;
            }

            if (realAction == StagingRowActionEnum.Delete)
            {
                if (includeSummary)
                {
                    mergeResult.DeleteCount += (int)(long)await DotStatDb.ExecuteScalarSqlAsync(
                        $"DELETE FROM {metadataTableName} WHERE DF_ID = {dfId}; SELECT ROW_COUNT() AS ROW_COUNT;", cancellationToken);
                    mergeResult.TotalCount =
                        mergeResult.UpdateCount + mergeResult.DeleteCount + mergeResult.InsertCount;
                }
                else
                {
                    await DotStatDb.ExecuteNonQuerySqlAsync(
                        $"DELETE FROM {metadataTableName} WHERE DF_ID = {dfId}", cancellationToken);
                }

                return mergeResult;
            }

            string updateDiffCheck = null;
            string attrColumnsSelect = null;
            string attrColumnsInsert = null;
            string attrColumnsInsertValues = null;
            string attrColumnsUpdate = null;

            switch (action)
            {
                case StagingRowActionEnum.Merge:
                    attrColumnsSelect = string.Join(",", referencedMetadataAttributes.Select(kvp => $"@Comp{kvp.Key.DbId} AS {kvp.Key.MariaDbColumn()}"));
                    attrColumnsInsert = string.Join(",", referencedMetadataAttributes.Select(kvp => kvp.Key.MariaDbColumn()));
                    attrColumnsInsertValues = string.Join(",", referencedMetadataAttributes.Select(kvp => $"Import.{kvp.Key.MariaDbColumn()}"));
                    attrColumnsUpdate = string.Join(",\n", referencedMetadataAttributes
                        .Select(a => $"ME.{a.Key.MariaDbColumn()} = CASE WHEN Import.{a.Key.MariaDbColumn()} IS NOT NULL THEN Import.{a.Key.MariaDbColumn()} ELSE ME.{a.Key.MariaDbColumn()} END"));
                    updateDiffCheck = string.Join(" OR\n", referencedMetadataAttributes
                        .Select(a => $"(Import.{a.Key.MariaDbColumn()} IS NOT NULL AND ({a.Key.MariaDbCollate($"ME.{a.Key.MariaDbColumn()}")} <> {a.Key.MariaDbCollate($"Import.{a.Key.MariaDbColumn()}")} OR ME.{a.Key.MariaDbColumn()} IS NULL))"));
                    break;
                case StagingRowActionEnum.Delete:
                    attrColumnsSelect = string.Join(",", referencedMetadataAttributes.Select(kvp => $"@Comp{kvp.Key.DbId} AS {kvp.Key.MariaDbColumn()}"));
                    attrColumnsInsert = string.Join(",", referencedMetadataAttributes.Select(kvp => kvp.Key.MariaDbColumn()));
                    attrColumnsUpdate = string.Join(",\n", referencedMetadataAttributes
                        .Select(a => $"ME.{a.Key.MariaDbColumn()} = CASE WHEN Import.{a.Key.MariaDbColumn()} IS NOT NULL THEN NULL ELSE ME.{a.Key.MariaDbColumn()} END"));
                    updateDiffCheck = string.Join(" OR\n", referencedMetadataAttributes
                        .Select(a => $"(Import.{a.Key.MariaDbColumn()} IS NOT NULL AND ME.{a.Key.MariaDbColumn()} IS NOT NULL)"));
                    break;
                case StagingRowActionEnum.Replace:
                    var allAttributesWithInclusion = dsd.Msd.MetadataAttributes
                        .Select(attribute => (referencedMetadataAttributes.Any(ra => ra.Key.Code.Equals(attribute.Code, StringComparison.InvariantCultureIgnoreCase)), attribute)).ToList();

                    attrColumnsSelect = string.Join(",", allAttributesWithInclusion
                            .Select(a => a.Item1 ? $"@Comp{a.attribute.DbId} AS {a.attribute.MariaDbColumn()}" : $"NULL AS {a.attribute.MariaDbColumn()}"));
                    attrColumnsInsert = allAttributesWithInclusion.Select(a => a.attribute).ToMariaDbColumnList();
                    attrColumnsInsertValues = allAttributesWithInclusion.Select(a => a.attribute).ToMariaDbColumnListWithTablePrefix(table: "Import");
                    attrColumnsUpdate = string.Join(",\n", allAttributesWithInclusion
                        .Select(a => @$"ME.{a.attribute.MariaDbColumn()} =  {(a.Item1 ? $"Import.{a.attribute.MariaDbColumn()}" : "NULL")}"));
                    updateDiffCheck = string.Join(" OR\n", allAttributesWithInclusion
                        .Select(a => a.Item1 ?
                            $"((Import.{a.attribute.MariaDbColumn()} IS NOT NULL AND ME.{a.attribute.MariaDbColumn()} IS NULL) OR (Import.{a.attribute.MariaDbColumn()} IS NULL AND ME.{a.attribute.MariaDbColumn()} IS NOT NULL) OR {a.attribute.MariaDbCollate($"ME.{a.attribute.MariaDbColumn()}")} <> {a.attribute.MariaDbCollate($"Import.{a.attribute.MariaDbColumn()}")})"
                            : $"ME.{a.attribute.MariaDbColumn()} IS NOT NULL"));
                    break;
            };

            if (!string.IsNullOrEmpty(updateDiffCheck))
                updateDiffCheck = $"AND ({updateDiffCheck})";

            //Merge in memory values to DsdAttr table
            var actionStatement = action == StagingRowActionEnum.Delete
                ? //Delete actions
                    $@"
UPDATE {metadataTableName} AS ME 
INNER JOIN Import AS Import ON 
    ME.DF_ID = Import.DF_ID
SET {attrColumnsUpdate}
    ,{MariaDbExtensions.LAST_UPDATED_COLUMN} = @CurrentDateTime;

SET @updated := ROW_COUNT();
SET @rowcount := @updated;"
                : // Replace and Merge actions
                    $@"
UPDATE {metadataTableName} AS ME
INNER JOIN Import AS Import ON
    ME.DF_ID = Import.DF_ID
    {updateDiffCheck}
SET 
    {attrColumnsUpdate}
    ,{MariaDbExtensions.LAST_UPDATED_COLUMN} = @CurrentDateTime;

SET @updated := ROW_COUNT();
SET @rowcount := @updated;

INSERT INTO {metadataTableName} (DF_ID, {attrColumnsInsert}, {MariaDbExtensions.LAST_UPDATED_COLUMN})
SELECT Import.DF_ID, {attrColumnsInsertValues}, @CurrentDateTime
FROM Import AS Import
LEFT JOIN {metadataTableName} AS ME ON 
    ME.DF_ID = Import.DF_ID
WHERE ME.DF_ID IS NULL;

SET @rowcount := (@rowcount + ROW_COUNT());";


            var sqlCommand = $@"
SET @rowcount := 0;
SET @updated := 0;

DROP TEMPORARY TABLE IF EXISTS Import;
CREATE TEMPORARY TABLE Import AS
    SELECT @DfId AS DF_ID, {attrColumnsSelect};

{actionStatement}
{(includeSummary
    ? "SELECT @rowcount - @updated AS INSERTED, @updated as UPDATED, 0 as DELETED, @rowcount as TOTAL;"
    : "SELECT CASE WHEN @rowcount > 0 THEN 1 ELSE 0 END;"
    )}";

            var mySqlParameters = new List<DbParameter>
            {
                new MySqlParameter("CurrentDateTime", MySqlDbType.DateTime) { Value = dateTime }
            };

            mySqlParameters.AddRange(referencedMetadataAttributes
                .Select(kvp =>
                    new MySqlParameter($"Comp{kvp.Key.DbId}", kvp.Key.GetMariaDbType())
                    {
                        Value = kvp.Value
                    } as DbParameter)
                .Concat(new[] { new MySqlParameter("DfId", MySqlDbType.Int32) { Value = dfId }, }).ToList());

            return await ExecuteMerge(sqlCommand, mySqlParameters, includeSummary, cancellationToken);
        }

        protected override async Task DeleteAll(IImportReferenceableStructure referencedStructure,
            DbTableVersion tableVersion, bool includeSummary, ImportSummary importSummary,
            ICodeTranslator codeTranslator, DateTime dateTime, CancellationToken cancellationToken)
        {
            if (referencedStructure is Dataflow dataFlow)
                await DeleteAll(dataFlow, tableVersion, includeSummary, importSummary, codeTranslator, dateTime,
                    cancellationToken);
            else
                await DeleteAll(referencedStructure as Dsd, tableVersion, includeSummary, importSummary, codeTranslator, dateTime,
                    cancellationToken);
        }

        private async Task DeleteAll(Dataflow dataflow, DbTableVersion tableVersion, bool includeSummary, ImportSummary importSummary, ICodeTranslator codeTranslator, DateTime dateTime, CancellationToken cancellationToken)
        {
            var allDimConstraint = Predicate.BuildWhereForObservationLevel(dataQuery: null, dataflow, codeTranslator, useExternalValues: false, useExternalColumnNames: false, nullTreatment: NullTreatment.IncludeNullsAndSwitchOff, includeTimeConstraints: true, serverType: DbType.MariaDb);
            if (!string.IsNullOrEmpty(allDimConstraint))
            {
                allDimConstraint = "WHERE " + allDimConstraint;
            }

            var mergeResult = await DeleteAll(dataflow.MariaDbMetadataDataflowTable((char)tableVersion), allDimConstraint, includeSummary, cancellationToken);

            if (mergeResult.Errors.Any())
            {
                importSummary.Errors.AddRange(mergeResult.Errors);
                return;
            }

            importSummary.ObservationLevelMergeResult.Add(mergeResult);

            mergeResult = await DeleteAll(dataflow.Dsd.MariaDbMetadataDataSetTable((char)tableVersion), null, includeSummary, cancellationToken);

            if (mergeResult.Errors.Any())
            {
                importSummary.Errors.AddRange(mergeResult.Errors);
                return;
            }

            importSummary.DataFlowLevelMergeResult.Add(mergeResult);

            Log.Warn(LocalizationRepository.GetLocalisedResource(Localization.ResourceId.DeleteAllMetadataActionPerformed));

            await InsertDeletedAllRow(dataflow.DbId, dataflow.Dsd.MariaDbDeletedMetadataTable((char)tableVersion), dateTime, cancellationToken);

        }

        private async Task DeleteAll(Dsd dsd, DbTableVersion tableVersion, bool includeSummary, ImportSummary importSummary, ICodeTranslator codeTranslator, DateTime dateTime, CancellationToken cancellationToken)
        {
            var mergeResult = await DeleteAll(dsd.MariaDbMetadataDataStructureTable((char)tableVersion), filterAllowedContentConstraint: "", includeSummary, cancellationToken);

            if (mergeResult.Errors.Any())
            {
                importSummary.Errors.AddRange(mergeResult.Errors);
                return;
            }

            importSummary.ObservationLevelMergeResult.Add(mergeResult);

            mergeResult = await DeleteAll(dsd.MariaDbMetadataDataSetTable((char)tableVersion), null, includeSummary, cancellationToken);

            if (mergeResult.Errors.Any())
            {
                importSummary.Errors.AddRange(mergeResult.Errors);
                return;
            }

            importSummary.DataFlowLevelMergeResult.Add(mergeResult);

            Log.Warn(LocalizationRepository.GetLocalisedResource(Localization.ResourceId.DeleteAllMetadataActionPerformed));

            await InsertDeletedAllRow(dataFlowId: -1, dsd.SqlDeletedMetadataTable((char)tableVersion), dateTime, cancellationToken);
        }

        public async Task UpdateStatisticsOfMetadataTable(Dsd dsd, DbTableVersion tableVersion, CancellationToken cancellationToken)
        {
            await DotStatDb.ExecuteNonQuerySqlAsync($@"OPTIMIZE TABLE {DotStatDb.DataSchema}_{dsd.MariaDbMetadataDataStructureTable((char)tableVersion)}", cancellationToken);
        }

        public async Task DropMetadataTables(Dsd dsd, CancellationToken cancellationToken)
        {
            await DropMetadataTables(dsd, DbTableVersion.A, cancellationToken);
            await DropMetadataTables(dsd, DbTableVersion.B, cancellationToken);
        }

        public async Task DropMetadataTables(Dsd dsd, DbTableVersion tableVersion, CancellationToken cancellationToken)
        {
            await DotStatDb.DropTable(DotStatDb.DataSchema, dsd.MariaDbMetadataDataStructureTable((char)tableVersion), cancellationToken);
            await DotStatDb.DropTable(DotStatDb.DataSchema, dsd.MariaDbMetadataDataSetTable((char)tableVersion), cancellationToken);
            await DotStatDb.DropTable(DotStatDb.DataSchema, dsd.MariaDbDeletedMetadataTable((char)tableVersion), cancellationToken);
        }

        public async Task TruncateMetadataTables(Dsd dsd, DbTableVersion tableVersion, CancellationToken cancellationToken)
        {
            await DotStatDb.TruncateTable(DotStatDb.DataSchema, dsd.MariaDbMetadataDataStructureTable((char)tableVersion), cancellationToken);
            await DotStatDb.TruncateTable(DotStatDb.DataSchema, dsd.MariaDbMetadataDataSetTable((char)tableVersion), cancellationToken);
            await DotStatDb.TruncateTable(DotStatDb.DataSchema, dsd.MariaDbDeletedMetadataTable((char)tableVersion), cancellationToken);
        }

        public Task CheckAndCreateSourceViews(Dataflow dataflow, ICodeTranslator codeTranslator, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        public Task DropMetadataHPRTables(Dataflow dataflow, char version, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        public Task CreateAndFillHPRMetadataTablesOfNextVersion(Dataflow dataflow, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        public Task CreateHPRMetadataViewsWithNextVersion(Dataflow dataflow, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        public MappingSetParams GetMappingSetParams(Dataflow dataflow, char? tableVersion)
        {
            var emptyMetaQuery = this.GetEmptyMetadataViewQuery(dataflow);

            return new MappingSetParams()
            {
                IsMetadata = true,
                ActiveQuery = tableVersion.HasValue ? GetMetadataViewQuery(dataflow, tableVersion.Value) : emptyMetaQuery,
                ReplaceQuery = tableVersion.HasValue ? GetMetadataViewQuery(dataflow, tableVersion.Value) : emptyMetaQuery,
                DeleteQuery = tableVersion.HasValue ? GetDeletedMetadataViewQuery(dataflow, tableVersion.Value) : emptyMetaQuery,
                IncludeHistoryQuery = null,
                OrderByTimeAsc = null,
                OrderByTimeDesc = null
            };
        }

        public async Task TruncateMetadataTables(Dataflow dataFlow, DbTableVersion tableVersion, CancellationToken cancellationToken)
        {
            await DotStatDb.TruncateTable(DotStatDb.DataSchema, dataFlow.MariaDbMetadataDataflowTable((char)tableVersion), cancellationToken);
        }

        public string GetMetadataViewQuery(Dataflow dataFlow, char tableVersion)
        {
            if (dataFlow.Dsd.Msd == null)
                throw new ArgumentException($"There is no linked MSD for the DSD of the parameter 'dataFlow'");

            var columns = new List<string>();

            foreach (var dim in dataFlow.Dsd.Dimensions)
            {
                if (dim.Base.TimeDimension)
                {
                    columns.Add($"`TIME_PERIOD` AS `{dim.Code}`");
                    columns.Add($"`{MariaDbExtensions.MariaDbPeriodStart()}`");
                    columns.Add($"`{MariaDbExtensions.MariaDbPeriodEnd()}`");
                }
                else
                {
                    columns.Add($"`{dim.Code}`");
                }
            }

            columns.AddRange(dataFlow.Dsd.Msd.MetadataAttributes.Select(attr => $"`{attr.HierarchicalId}`"));
            columns.Add($"`{MariaDbExtensions.LAST_UPDATED_COLUMN}`");

            return $"SELECT {string.Join(",", columns)} FROM {DotStatDb.DataSchema}_{dataFlow.MariaDbMetadataDataflowViewName(tableVersion)}";
        }

        public string GetDeletedMetadataViewQuery(Dataflow dataFlow, char tableVersion)
        {
            if (dataFlow.Dsd.Msd == null)
                throw new ArgumentException($"There is no linked MSD for the DSD of the parameter 'dataFlow'");

            var columns = new List<string>();

            foreach (var dim in dataFlow.Dsd.Dimensions)
            {
                if (dim.Base.TimeDimension)
                {
                    columns.Add($"TIME_PERIOD AS `{dim.Code}`");
                    columns.Add($"`{MariaDbExtensions.MariaDbPeriodStart()}`");
                    columns.Add($"`{MariaDbExtensions.MariaDbPeriodEnd()}`");
                }
                else
                {
                    columns.Add($"`{dim.Code}`");
                }
            }

            columns.AddRange(dataFlow.Dsd.Msd.MetadataAttributes.Select(attr => $"`{attr.HierarchicalId}`"));
            columns.Add($"`{MariaDbExtensions.LAST_UPDATED_COLUMN}`");

            return $"SELECT {string.Join(",", columns)} FROM {DotStatDb.DataSchema}_{dataFlow.MariaDbDeletedMetadataDataflowViewName(tableVersion)}";
        }

        public string GetEmptyMetadataViewQuery(Dataflow dataFlow)
        {
            if (dataFlow.Dsd.Msd == null)
                throw new ArgumentException($"There is no linked MSD for the DSD of the parameter 'dataFlow'");

            var columns = new List<string>();

            foreach (var dim in dataFlow.Dsd.Dimensions)
            {
                if (dim.Base.TimeDimension)
                {
                    columns.Add($"NULL AS `{dim.Code}`");
                    columns.Add($"NULL AS `{MariaDbExtensions.MariaDbPeriodStart()}`");
                    columns.Add($"NULL AS `{MariaDbExtensions.MariaDbPeriodEnd()}`");
                }
                else
                {
                    columns.Add($"NULL AS `{dim.Code}`");
                }
            }

            columns.AddRange(dataFlow.Dsd.Msd.MetadataAttributes.Select(attr => $"NULL AS `{attr.HierarchicalId}`"));
            columns.Add($"NULL as `{MariaDbExtensions.LAST_UPDATED_COLUMN}`");

            return "SELECT " + string.Join(",", columns) + " LIMIT 0";
        }

        public async Task<bool> MetadataTablesExist(Dsd dsd, CancellationToken cancellationToken)
        {
            return await DotStatDb.TableExists($"{DotStatDb.DataSchema}_{dsd.MariaDbMetadataDataStructureTable((char)DbTableVersion.A)}", cancellationToken) || 
                   await DotStatDb.TableExists($"{DotStatDb.DataSchema}_{dsd.MariaDbMetadataDataStructureTable((char)DbTableVersion.B)}", cancellationToken);
        }

        public async Task<bool> DataFlowMetadataTablesExist(Dataflow dataflow, CancellationToken cancellationToken)
        {
            return await DotStatDb.TableExists($"{DotStatDb.DataSchema}_{dataflow.MariaDbMetadataDataflowTable((char)DbTableVersion.A)}", cancellationToken) ||
                   await DotStatDb.TableExists($"{DotStatDb.DataSchema}_{dataflow.MariaDbMetadataDataflowTable((char)DbTableVersion.B)}", cancellationToken);
        }

        protected override async Task<bool> CleanUpFullyEmptyObservations(IImportReferenceableStructure referencedStructure, DbTableVersion tableVersion, CancellationToken cancellationToken)
        {
            var dataFlow = referencedStructure as Dataflow;
            var dsd = referencedStructure.Dsd;

            if (!dsd.Msd.MetadataAttributes.Any())
            {
                return false;
            }

            var attributeColumns = dsd.Msd.MetadataAttributes.ToMariaDbColumnListWithIsNull();

            if (!string.IsNullOrEmpty(attributeColumns))
            {
                if (dataFlow is not null)
                {
                    await DotStatDb.ExecuteNonQuerySqlAsync($@"DELETE FROM {DotStatDb.DataSchema}_{dataFlow.MariaDbMetadataDataflowTable((char)tableVersion)} WHERE {attributeColumns}", cancellationToken);
                    await DotStatDb.ExecuteNonQuerySqlAsync($@"DELETE FROM {DotStatDb.DataSchema}_{dataFlow.Dsd.MariaDbMetadataDataSetTable((char)tableVersion)} WHERE {attributeColumns} AND DF_ID= {dataFlow.DbId}", cancellationToken);
                }
                else
                {
                    await DotStatDb.ExecuteNonQuerySqlAsync($@"DELETE FROM {DotStatDb.DataSchema}_{dsd.MariaDbMetadataDataStructureTable((char)tableVersion)} WHERE {attributeColumns}", cancellationToken);
                    await DotStatDb.ExecuteNonQuerySqlAsync($@"DELETE FROM {DotStatDb.DataSchema}_{dsd.MariaDbMetadataDataSetTable((char)tableVersion)} WHERE {attributeColumns} AND DF_ID= -1", cancellationToken);
                }
            }

            return false;
        }

        private async ValueTask BuildMetadataStagingTables(Dsd dsd, CancellationToken cancellationToken)
        {
            if (dsd?.Msd == null)
                return;

            var tableName = dsd.MariaDbMetadataStagingTable();
            //Change to allowNull=true when non coded dimensions are introduced. 
            //A value of 0 should be given when the dimension is not being referenced.
            var dimensionColumns = dsd.Dimensions.Where(dim => !dim.Base.TimeDimension)
                .ToMariaDbColumnList(true, true);
            var metadataAttributeColumns = dsd.Msd.MetadataAttributes.OrderBy(a => a.DbId).ToMariaDbColumnList(true);

            if (!string.IsNullOrWhiteSpace(metadataAttributeColumns))
            {
                metadataAttributeColumns += ',';
            }

            var rowId = dsd.Dimensions.Count > 32 ?
                $"ROW_ID blob AS ({dsd.MariaDbBuildRowIdFormula()}) PERSISTENT,"
                : null;

            var timeDim = dsd.TimeDimension;
            // NULL is stored when time is not referenced by the metadata.
            var sdmxDimTimeColumn = timeDim != null
                ? new[] { timeDim }.ToMariaDbColumnList(true, true) + "," : null;

            var supportsDateTime = dsd.Base.HasSupportDateTimeAnnotation();
            var timeDimensionColumns = timeDim != null
                ? string.Join(",",
                    //The maximum SQL data/datetime is stored when time is not referenced by the metadata.
                    MariaDbExtensions.MariaDbPeriodStart(true, supportsDateTime, true),
                    //The minimum SQL data/datetime is stored when time is not referenced by the metadata.
                    MariaDbExtensions.MariaDbPeriodEnd(true, supportsDateTime, true)
                ) + ","
                : null;

            await DotStatDb.ExecuteNonQuerySqlAsync($@"CREATE TABLE {DotStatDb.DataSchema}_{tableName}(
                    {rowId}
                    {timeDimensionColumns}
                    {sdmxDimTimeColumn}
                    {dimensionColumns},
                    {metadataAttributeColumns}
                    REQUESTED_ACTION TINYINT NOT NULL,
                    REAL_ACTION TINYINT NOT NULL,
                    BATCH_NUMBER INT NULL
                )", cancellationToken);
        }

        private void OnSqlRowsCopied(int rowsCopied, int batchNr)
        {
            Log.Notice(string.Format(
                LocalizationRepository.GetLocalisedResource(Localization.ResourceId.ProcessingMetadataAttributes),
                rowsCopied, batchNr));
        }

        private async Task<MergeResult> ExecuteMerge(string sqlCommand, List<DbParameter> sqlParams, bool includeSummary, CancellationToken cancellationToken, Dsd dsd = null, ReportedComponents reportedComponents = null)
        {
            var mergeResult = new MergeResult();
            if (string.IsNullOrEmpty(sqlCommand))
                return mergeResult;

            try
            {
                if (includeSummary)
                {
                    await using var dbDataReader = await DotStatDb.ExecuteReaderSqlWithParamsAsync(sqlCommand, cancellationToken, commandBehavior: CommandBehavior.SingleRow, parameters: sqlParams.ToArray());

                    if (await dbDataReader.ReadAsync(cancellationToken))
                    {
                        mergeResult.InsertCount = dbDataReader.GetInt32(0);
                        mergeResult.UpdateCount = dbDataReader.GetInt32(1);
                        mergeResult.DeleteCount = dbDataReader.GetInt32(2);
                        mergeResult.TotalCount = dbDataReader.GetInt32(3);
                    }
                }
                else
                {
                    await DotStatDb.ExecuteNonQuerySqlWithParamsAsync(sqlCommand, cancellationToken, sqlParams.ToArray());
                }
            }
            catch (MySqlException e)
            {
                switch (e.Number)
                {
                    case 1062://duplicates in the staging table insert
                        mergeResult.Errors.Add(new ObservationError { Type = ValidationErrorType.DuplicatedRowsInMetadataStagingTable });
                        break;
                    default:
                        throw;

                }
            }

            return mergeResult;
        }
    }
}