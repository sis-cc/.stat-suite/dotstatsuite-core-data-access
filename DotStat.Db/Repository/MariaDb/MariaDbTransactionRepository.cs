using DotStat.Common.Auth;
using DotStat.Common.Configuration.Interfaces;
using DotStat.Common.Exceptions;
using DotStat.Common.Localization;
using DotStat.Common.Logger;
using DotStat.Db;
using DotStat.Db.DB;
using DotStat.Db.Dto;
using DotStat.Db.Repository;
using DotStat.Domain;
using MySqlConnector;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Transaction = DotStat.Domain.Transaction;
using TransactionStatus = DotStat.Domain.TransactionStatus;

namespace DotStat.DB.Repository.MariaDb
{
    public class MariaDbTransactionRepository : DatabaseRepositoryBase<MariaDbDotStatDb>, ITransactionRepository
    {
        public MariaDbTransactionRepository(MariaDbDotStatDb dotStatDb, IGeneralConfiguration generalConfiguration) :
            base(dotStatDb, generalConfiguration)
        {
        }
        
        public async Task<bool> UpdateTableVersionOfTransaction(int transactionId, DbTableVersion tableVersion, CancellationToken cancellationToken)
        {
            return await DotStatDb.ExecuteNonQuerySqlWithParamsAsync(
                $@"UPDATE {DotStatDb.ManagementSchema}_DSD_TRANSACTION
                     SET TABLE_VERSION = @TargetVersion
                   WHERE TRANSACTION_ID = @Id",
                cancellationToken,
                new MySqlParameter("Id", MySqlDbType.Int32) { Value = transactionId },
                new MySqlParameter("TargetVersion", MySqlDbType.String) { Value = (char)tableVersion }
            ) > 0;
        }

        public async Task<bool> UpdateFinalTargetVersionOfTransaction(int transactionId, TargetVersion targetVersion, CancellationToken cancellationToken)
        {
            return await DotStatDb.ExecuteNonQuerySqlWithParamsAsync(
                $@"UPDATE {DotStatDb.ManagementSchema}_DSD_TRANSACTION
                     SET FINAL_TARGET_VERSION = @TargetVersion
                   WHERE TRANSACTION_ID = @Id",
                cancellationToken,
                new MySqlParameter("Id", MySqlDbType.Int32) { Value = transactionId },
                new MySqlParameter("TargetVersion", MySqlDbType.VarChar) { Value = targetVersion }
            ) > 0;
        }

        public async Task<bool> UpdateArtefactFullIdOfTransaction(int transactionId, string artefactFullId, CancellationToken cancellationToken)
        {
            return await DotStatDb.ExecuteNonQuerySqlWithParamsAsync(
                $@"UPDATE {DotStatDb.ManagementSchema}_DSD_TRANSACTION
                     SET ARTEFACT_FULL_ID = @ArtefactFull
                   WHERE TRANSACTION_ID = @Id",
                cancellationToken,
                new MySqlParameter("Id", MySqlDbType.Int32) { Value = transactionId },
                new MySqlParameter("ArtefactFull", MySqlDbType.VarChar) { Value = artefactFullId }
            ) > 0;
        }

        public async Task<bool> TryLockNewTransaction(Transaction transaction, bool isTransactionWithNoDsd, DbTableVersion origTransferTargetTableVersion, CancellationToken cancellationToken, bool logErrors = true)
        {
            var dbId = transaction.ArtefactDbId ?? -1;
            try
            {
                Log.Debug($"Attempting to lock a new transaction for DSD id: {dbId} {transaction.ArtefactFullId ?? SpecialTransactionArtefactIds.None}");

                if (!await LockTransaction(transaction.Id, dbId, transaction.ArtefactChildDbId, origTransferTargetTableVersion, transaction.RequestedTargetVersion, cancellationToken))
                    return false;

                if (dbId > 0 && !await EnsureOnlySingleDsdTransaction(transaction, cancellationToken))
                    return false;

                //New transaction trying to block all the service
                if (dbId == -1)
                {
                    var ongoingTransactions = await GetNonCompletedTransactions(transaction.Id, cancellationToken);
                    //Check that there are no running transactions for any artefact
                    if (ongoingTransactions.Count > 0)
                    {
                        if (logErrors)
                        {
                            Log.Error(new DotStatException(string.Format(
                                LocalizationRepository.GetLocalisedResource(Localization.ResourceId.MaintenanceTransactionFailedOngoingTransactions),
                                transaction.Type.ToString(),
                                ongoingTransactions.Count,
                                string.Join(",", ongoingTransactions))));
                        }

                        return false;
                    }
                }
                //New transaction for specific artefact
                else
                {
                    var blockingTransactionId = await GetNonCompletedTransactionBlockingAll(transaction.Id, cancellationToken);
                    //Check that there are no running transactions blocking all the service
                    if (blockingTransactionId > -1)
                    {
                        if (logErrors)
                        {
                            Log.Error(new DotStatException(string.Format(
                                LocalizationRepository.GetLocalisedResource(Localization.ResourceId.OngoingTransactionBlockingAll),
                                blockingTransactionId)));
                        }

                        return false;
                    }
                }

                return true;
            }
            catch (MySqlException ex) when (ex.Number == 2627 || ex.Number == 2601)
            {
                //2601 - Cannot insert duplicate key row in object '%.*ls' with unique index '%.*ls'.The duplicate key value is % ls.
                //2627 - Violation of % ls constraint '%.*ls'.Cannot insert duplicate key in object '%.*ls'.The duplicate key value is % ls.
                //Concurrent process already inserted row for the same dsd, so primary key constraint violation.
                var transactionInProgress = await GetInProgressTransactions(transaction.Id, dbId, cancellationToken).FirstOrDefaultAsync(cancellationToken);

                if (logErrors)
                {
                    var errorMsg = isTransactionWithNoDsd
                    ? string.Format(
                        LocalizationRepository.GetLocalisedResource(Localization.ResourceId
                            .TransactionAbortedDueToConcurrentTransactionWithNoDsd),
                        transaction.Id,
                        transactionInProgress?.Id.ToString() ?? "unknown")
                    : string.Format(
                        LocalizationRepository.GetLocalisedResource(Localization.ResourceId
                            .TransactionAbortedDueToConcurrentTransaction),
                        transaction.Id,
                        transaction.ArtefactFullId,
                        transactionInProgress?.Id.ToString() ?? "unknown");

                    Log.Error(new DotStatException(errorMsg, ex));
                }

                return false;
            }
        }

        public async Task<int> GetNextTransactionId(CancellationToken cancellationToken)
        {
            var ret = await DotStatDb.ExecuteScalarSqlAsync(
                $@"SELECT NEXTVAL({DotStatDb.ManagementSchema}_TRANSFER_SEQUENCE)",
                cancellationToken
            );

            return (int)(long)ret;
        }

        public async Task<bool> MarkTransactionReadyForValidation(int transactionId, CancellationToken cancellationToken)
        {
            return await DotStatDb.ExecuteNonQuerySqlWithParamsAsync(
                $@"UPDATE {DotStatDb.ManagementSchema}_DSD_TRANSACTION
                      SET EXECUTION_END = @DT_Now
                    WHERE TRANSACTION_ID = @Id",
                cancellationToken,
                new MySqlParameter("DT_Now", DateTime.UtcNow),
                new MySqlParameter("Id", transactionId)
            ) > 0;
        }

        public async IAsyncEnumerable<TransactionLog> GetTransactionLogs(int transactionId, bool includeFatal, [EnumeratorCancellation] CancellationToken cancellationToken, bool tryUseReadOnlyConnection = false)
        {
            var sqlCommand = $@"SELECT DATE,LEVEL,SERVER,LOGGER,MESSAGE,EXCEPTION,USEREMAIL 
                        FROM {DotStatDb.ManagementSchema}_LOGS
                        WHERE TRANSACTION_ID = @Id {(!includeFatal ? " AND LEVEL != 'FATAL' " : string.Empty)}
                        ORDER BY DATE";
            
            await using (var dr = await DotStatDb.ExecuteReaderSqlWithParamsAsync(
                sqlCommand, 
                cancellationToken,
                tryUseReadOnlyConnection: tryUseReadOnlyConnection,
                parameters: new MySqlParameter("Id", MySqlDbType.Int32) { Value = transactionId }))
            {
                while (await dr.ReadAsync(cancellationToken))
                {
                    yield return new TransactionLog
                    {
                        Id = transactionId,
                        Date = GetDateTime(dr, 0)?.ToString(Transaction.TransactionDatetimeFormat),
                        Level = GetString(dr, 1),
                        Server = GetString(dr, 2),
                        Logger = GetString(dr, 3),
                        Message = GetString(dr, 4),
                        Exception = GetString(dr, 5),
                        UserEmail = GetString(dr, 6)
                    };
                    
                }
            }
        }

        public async Task<Transaction> CreateTransactionItem(int transactionId, ArtefactItem artefact, DotStatPrincipal principal, string sourceDataSpace, string dataSource, TransactionType type, TargetVersion? requestedTargetVersion, string serviceId, CancellationToken cancellationToken, bool blockAllTransactions = false)
        {
            return await CreateTransactionItem(transactionId, artefact?.ToString(), principal, sourceDataSpace, dataSource, type, requestedTargetVersion, serviceId, cancellationToken, blockAllTransactions);
        }
       
        public async Task<Transaction> CreateTransactionItem(int transactionId, string atrFullId, DotStatPrincipal principal, string sourceDataSpace, string dataSource, TransactionType type, TargetVersion? requestedTargetVersion, string serviceId, CancellationToken cancellationToken, bool blockAllTransactions = false)
        {
            var transactionCreated =await DotStatDb.ExecuteNonQuerySqlWithParamsAsync(
                $@"INSERT INTO {DotStatDb.ManagementSchema}_DSD_TRANSACTION(
                    TRANSACTION_ID,
                    ARTEFACT_FULL_ID,
                    QUEUED_DATETIME,
                    EXECUTION_START,
                    EXECUTION_END,
                    USER_EMAIL,
                    SOURCE_DATASPACE,
                    DATA_SOURCE,
                    TYPE,
                    STATUS,
                    REQUESTED_TARGET_VERSION,
                    BLOCK_ALL_TRANSACTIONS,
                    SERVICE_ID,
                    LAST_UPDATED
                )
                VALUES (@Id, @ArtefactFullId, @DT_Now, NULL, NULL, @Email, @SourceDataSpace, @DataSource, @Type, @Status, @RequestedTargetVersion, @BlockAllTransactions, @ServiceId, @DT_Now)",
                cancellationToken,
                new MySqlParameter("Id", MySqlDbType.Int32) { Value = transactionId },
                new MySqlParameter("ArtefactFullId", MySqlDbType.VarChar) { Value = (object)atrFullId ?? SpecialTransactionArtefactIds.None },
                new MySqlParameter("Email", MySqlDbType.VarChar) { Value = (object)principal?.Email ?? DBNull.Value },
                new MySqlParameter("SourceDataSpace", MySqlDbType.VarChar) { Value = (object)sourceDataSpace ?? DBNull.Value },
                new MySqlParameter("DataSource", MySqlDbType.VarChar) { Value = (object)dataSource ?? DBNull.Value },
                new MySqlParameter("Type", MySqlDbType.VarChar) { Value = type.ToString() },
                new MySqlParameter("Status", MySqlDbType.VarChar) { Value = TransactionStatus.Queued.ToString() },
                new MySqlParameter("RequestedTargetVersion", MySqlDbType.VarChar) { Value = (object)requestedTargetVersion ?? DBNull.Value },
                new MySqlParameter("BlockAllTransactions", MySqlDbType.Bit) { Value = blockAllTransactions },
                new MySqlParameter("ServiceId", MySqlDbType.VarChar) { Value = !string.IsNullOrWhiteSpace(serviceId) ? serviceId : DBNull.Value },
                new MySqlParameter("DT_Now", DateTime.UtcNow)
            );


            if (transactionCreated>0)
            {
                return new Transaction()
                {
                    Id = transactionId,
                    ArtefactFullId = atrFullId,
                    UserEmail = principal?.Email,
                    SourceDataspace = sourceDataSpace,
                    DataSource = dataSource,
                    Type = type,
                    Status = TransactionStatus.Queued,
                    RequestedTargetVersion = requestedTargetVersion,
                    FinalTargetVersion = requestedTargetVersion,
                    ServiceId = serviceId,
                    BlockAllTransactions = blockAllTransactions
                };
            }

            return null;
        }

        public async Task<Transaction> GetTransactionById(int transactionId, CancellationToken cancellationToken, bool tryUseReadOnlyConnection = false)
        {
            var sqlCommand =
                $@"SELECT {GetTransactionTableDbFields()}
                   FROM {DotStatDb.ManagementSchema}_DSD_TRANSACTION
                   WHERE TRANSACTION_ID = @Id";

            var transactions= LoadTransactionByCommand(
                sqlCommand, 
                cancellationToken, 
                tryUseReadOnlyConnection,
                new MySqlParameter("Id", MySqlDbType.Int32) { Value = transactionId });

            return await transactions.FirstOrDefaultAsync(cancellationToken);
        }

        public IAsyncEnumerable<Transaction> GetTransactions(string userEmail, string artFullId, DateTime start, DateTime end, TransactionStatus? status, CancellationToken cancellationToken, bool tryUseReadOnlyConnection = false)
        {
            var criteria = new StringBuilder();
            var @params = new List<DbParameter>()
            {
                new MySqlParameter("@start", MySqlDbType.DateTime) { Value = start },
                new MySqlParameter("@end", MySqlDbType.DateTime) { Value = end }
            };

            if (!string.IsNullOrEmpty(userEmail))
            {
                criteria.AppendLine("USER_EMAIL = @userEmail AND ");
                @params.Add(new MySqlParameter("userEmail", MySqlDbType.VarChar) { Value = userEmail });
            }

            if (!string.IsNullOrEmpty(artFullId))
            {
                criteria.AppendLine("ARTEFACT_FULL_ID = @artFullId AND ");
                @params.Add(new MySqlParameter("artFullId", MySqlDbType.VarChar) { Value = artFullId });
            }

            if (status != null)
            {
                criteria.AppendLine("STATUS = @status AND ");
                @params.Add(new MySqlParameter("status", MySqlDbType.VarChar) { Value = status.ToString() });
            }

            var sqlCommand = $@"SELECT {GetTransactionTableDbFields()}
                     FROM {DotStatDb.ManagementSchema}_DSD_TRANSACTION
                     WHERE {criteria}
                    (                     
                        (QUEUED_DATETIME >= @start AND EXECUTION_END  <= @end) 
                        OR (EXECUTION_END IS NULL AND QUEUED_DATETIME BETWEEN @start AND @end)
                    );";

            return LoadTransactionByCommand(
                sqlCommand,
                cancellationToken,
                tryUseReadOnlyConnection,
                @params.ToArray()
            );
        }

        public IAsyncEnumerable<Transaction> GetInProgressTransactions(int transactionId, int dsdId, CancellationToken cancellationToken)
        {
            var artIdFilter = dsdId == -1 ? 
                "ART_ID IS NOT NULL" : //Get any transaction in progress regardless if is maintenance or targeting a particular artefact
                $"(ART_ID = {dsdId} OR ART_ID = -1)"; //Get any transaction in progres for this dsdId or a maintenance transaction

            var sqlCommand = $@"SELECT {GetTransactionTableDbFields()}
                     FROM {DotStatDb.ManagementSchema}_DSD_TRANSACTION
                     WHERE TRANSACTION_ID <> {transactionId} AND {artIdFilter} AND SUCCESSFUL IS NULL";

            return LoadTransactionByCommand(sqlCommand, cancellationToken);            
        }

        public async Task<bool> LockTransaction(
            int transactionId, 
            int dsdDbId,
            int? dsdChildDbId,
            DbTableVersion tableVersion, 
            TargetVersion? FinalTargetVersion, 
            CancellationToken cancellationToken
        )
        {
            return await DotStatDb.ExecuteNonQuerySqlWithParamsAsync(
                $@"UPDATE {DotStatDb.ManagementSchema}_DSD_TRANSACTION
                   SET EXECUTION_START=@DT_Now, 
                        ART_ID=@ArtId,
                        ART_CHILD_ID=@ArtChildId,
                        TABLE_VERSION=@TableVersion,
                        STATUS=@Status, 
                        FINAL_TARGET_VERSION=@TargetVersion, 
                        LAST_UPDATED=@DT_Now
                    WHERE TRANSACTION_ID = @Id",
                cancellationToken,
                new MySqlParameter("Id", MySqlDbType.Int32) { Value = transactionId },
                new MySqlParameter("ArtId", MySqlDbType.Int32) { Value = dsdDbId },
                new MySqlParameter("ArtChildId", (object) dsdChildDbId ?? DBNull.Value),
                new MySqlParameter("TableVersion", MySqlDbType.String) { Value = (char)tableVersion },
                new MySqlParameter("Status", MySqlDbType.VarChar) { Value = TransactionStatus.InProgress.ToString() },
                new MySqlParameter("TargetVersion", MySqlDbType.VarChar) { Value = (object)FinalTargetVersion ?? DBNull.Value },
                new MySqlParameter("DT_Now", DateTime.Now)
            ) > 0;
        }
        private async Task<bool> EnsureOnlySingleDsdTransaction(Transaction transaction, CancellationToken cancellationToken)
        {
            return Convert.ToInt32(await DotStatDb.ExecuteScalarSqlWithParamsAsync(
                $@"select count(*) FROM {DotStatDb.ManagementSchema}_DSD_TRANSACTION where SUCCESSFUL IS NULL 
AND ART_ID = @ArtId AND ART_CHILD_ID IS {(transaction.ArtefactChildDbId == null ? "NOT" : "")} NULL",
                cancellationToken,
                new MySqlParameter("ArtId", MySqlDbType.Int32) { Value = transaction.ArtefactDbId }
            )) == 0;
        }

        public async Task<bool> MarkTransactionAsTimedOut(int transactionId)
        {
            return await UpdateTransactionStatus(transactionId, TransactionStatus.TimedOut, false, CancellationToken.None);
        }

        public async Task<bool> MarkTransactionAsCanceled(int transactionId)
        {
            return await UpdateTransactionStatus(transactionId, TransactionStatus.Canceled, false, CancellationToken.None);
        }

        public async Task<bool> CancelTransactions(string serviceId)
        {
            //Keep backwards compatibility for environments which do not have the setting 'SERVICE_ID' configured.
            //In this case, no transactions are Canceled
            if (string.IsNullOrEmpty(serviceId)) return true;

            var sqlCommand = $@"SELECT TRANSACTION_ID
                FROM {DotStatDb.ManagementSchema}_DSD_TRANSACTION
                WHERE SUCCESSFUL IS NULL AND SERVICE_ID = @ServiceId";

            await using (var dr = await DotStatDb.ExecuteReaderSqlWithParamsAsync(
                sqlCommand,
                CancellationToken.None,
                tryUseReadOnlyConnection: true,
                parameters: new MySqlParameter("ServiceId", MySqlDbType.VarChar) { Value = serviceId }
            ))
            {
                while (await dr.ReadAsync(CancellationToken.None))
                {
                    var transactionId = dr.GetInt32(0);
                    LogHelper.RecordNewTransaction(transactionId, DotStatDb.DataSpace);
                    Log.Warn(string.Format(
                        LocalizationRepository.GetLocalisedResource(Localization.ResourceId.CanceledTransactionOnStartup), transactionId));
                }
            }

            return await DotStatDb.ExecuteNonQuerySqlWithParamsAsync(
                $@"UPDATE {DotStatDb.ManagementSchema}_DSD_TRANSACTION
                  SET EXECUTION_END = @DT_Now, SUCCESSFUL = @Successful, STATUS=@Status, LAST_UPDATED=@DT_Now
                  WHERE SUCCESSFUL IS NULL AND SERVICE_ID = @ServiceId",
                CancellationToken.None,
                new MySqlParameter("ServiceId", MySqlDbType.VarChar) { Value = serviceId },
                new MySqlParameter("Successful", MySqlDbType.Bit) { Value = false },
                new MySqlParameter("Status", MySqlDbType.VarChar) { Value = TransactionStatus.Canceled.ToString() },
                new MySqlParameter("DT_Now", DateTime.UtcNow)
            ) > 0;
        }

        public async Task<bool> MarkTransactionAsCompleted(int transactionId, bool successful)
        {
            return await UpdateTransactionStatus(transactionId, TransactionStatus.Completed, successful, CancellationToken.None);
        }

        #region private methods

        private async Task<List<int>> GetNonCompletedTransactions(int transactionId, CancellationToken cancellationToken)
        {
            var sqlCommand = $@"SELECT TRANSACTION_ID
                FROM {DotStatDb.ManagementSchema}_DSD_TRANSACTION
                WHERE TRANSACTION_ID <> {transactionId} AND ART_ID IS NOT NULL AND SUCCESSFUL IS NULL;";

            var transactionIds = new List<int>();
            using (var dr = await DotStatDb.ExecuteReaderSqlAsync(sqlCommand, cancellationToken))
            {
                while (await dr.ReadAsync(cancellationToken))
                {
                    transactionIds.Add(dr.GetInt32(0));
                }
            }

            return transactionIds;
        }

        private async Task<int> GetNonCompletedTransactionBlockingAll(int transactionId, CancellationToken cancellationToken)
        {
            var sqlCommand = $@"SELECT TRANSACTION_ID
            FROM {DotStatDb.ManagementSchema}_DSD_TRANSACTION
            WHERE TRANSACTION_ID <> {transactionId} AND ART_ID IS NOT NULL
                AND BLOCK_ALL_TRANSACTIONS = 1 AND SUCCESSFUL IS NULL
                LIMIT 1;";

            return (int?)await DotStatDb.ExecuteScalarSqlAsync(sqlCommand, cancellationToken) ?? -1;
        }

        private static string GetTransactionTableDbFields()
        {
            return @" TRANSACTION_ID,ART_ID,ARTEFACT_FULL_ID,TABLE_VERSION,QUEUED_DATETIME,EXECUTION_START,
                EXECUTION_END,SUCCESSFUL,USER_EMAIL,SOURCE_DATASPACE,DATA_SOURCE,TYPE,STATUS,
                REQUESTED_TARGET_VERSION,FINAL_TARGET_VERSION,BLOCK_ALL_TRANSACTIONS,LAST_UPDATED,ART_CHILD_ID,SERVICE_ID";
        }

        private async ValueTask<bool> UpdateTransactionStatus(int transactionId, TransactionStatus status, bool? successful, CancellationToken cancellationToken)
        {
            var successfulPart = "";
            var executionStart = "";
            var executionEnd = "";
            switch (status)
            {
                case TransactionStatus.Queued:
                    return false;
                case TransactionStatus.InProgress:
                    return false;
                case TransactionStatus.Completed:
                    executionEnd = "EXECUTION_END = @DT_Now,";
                    successfulPart = "SUCCESSFUL = @Successful,";
                    break;
                case TransactionStatus.Canceled:
                    executionEnd = "EXECUTION_END = @DT_Now,";
                    successful = false;
                    successfulPart = "SUCCESSFUL = @Successful,";
                    break;
                case TransactionStatus.TimedOut:
                    executionEnd = "EXECUTION_END = @DT_Now,";
                    successful = false;
                    successfulPart = "SUCCESSFUL = @Successful,";
                    break;
                case TransactionStatus.Unknown:
                    return false;
            }

            return await DotStatDb.ExecuteNonQuerySqlWithParamsAsync(
                $@"UPDATE {DotStatDb.ManagementSchema}_DSD_TRANSACTION
                  SET {executionStart} {executionEnd} {successfulPart} STATUS=@Status, LAST_UPDATED=@DT_Now
                  WHERE TRANSACTION_ID = @Id",
                cancellationToken,
                new MySqlParameter("Id", MySqlDbType.Int32) { Value = transactionId },
                new MySqlParameter("Successful", MySqlDbType.Bit) { Value = (object)successful ?? DBNull.Value },
                new MySqlParameter("Status", MySqlDbType.VarChar) { Value = status.ToString() },
                new MySqlParameter("DT_Now", DateTime.UtcNow)
            ) > 0;
        }

        private async IAsyncEnumerable<Transaction> LoadTransactionByCommand(
            string sqlCommand,
            [EnumeratorCancellation] CancellationToken cancellationToken,
            bool tryUseReadOnlyConnection = false,
            params DbParameter[] parameters)
        {
            using (DbDataReader dbDataReader = await DotStatDb.ExecuteReaderSqlWithParamsAsync(sqlCommand, cancellationToken, tryUseReadOnlyConnection: tryUseReadOnlyConnection, parameters: parameters))
            {
                while (await dbDataReader.ReadAsync(cancellationToken))
                {
                    yield return new Transaction()
                    {
                        Id = dbDataReader.GetInt32(0),
                        ArtefactDbId = dbDataReader.ColumnValue<int?>(1),
                        ArtefactFullId = dbDataReader.ColumnValue<string>(2),
                        TableVersion =
                            Enum.TryParse(dbDataReader.ColumnValue<string>(3), true, out DbTableVersion tableVersion)
                                ? tableVersion
                                : DbTableVersions.GetDbTableVersion(null),
                        QueuedDateTime = dbDataReader.GetDateTime(4),
                        ExecutionStart = dbDataReader.ColumnValue<DateTime?>(5),
                        ExecutionEnd = dbDataReader.ColumnValue<DateTime?>(6),
                        Successful = dbDataReader.ColumnValue<bool?>(7),
                        UserEmail = dbDataReader.ColumnValue<string>(8),
                        SourceDataspace = dbDataReader.ColumnValue<string>(9),
                        DataSource = dbDataReader.ColumnValue<string>(10),
                        Type = Enum.TryParse(dbDataReader.ColumnValue<string>(11), true, out TransactionType type) ? type : TransactionType.Unknown,
                        Status = Enum.TryParse(dbDataReader.ColumnValue<string>(12), true, out TransactionStatus status) ? status : TransactionStatus.Unknown,
                        RequestedTargetVersion = Enum.TryParse(dbDataReader.ColumnValue<string>(13), true, out TargetVersion requestedTargetVersion) ? requestedTargetVersion : null as TargetVersion?,
                        FinalTargetVersion = Enum.TryParse(dbDataReader.ColumnValue<string>(14), true, out TargetVersion finalTargetVersion) ? finalTargetVersion : null as TargetVersion?,
                        BlockAllTransactions = dbDataReader.GetBoolean(15),
                        LastUpdated = dbDataReader.GetDateTime(16),
                        ArtefactChildDbId = dbDataReader.ColumnValue<int?>(17),
                        ServiceId = dbDataReader.ColumnValue<string>(18)
                    };
                }
            }
        }

        #endregion
    }
}
