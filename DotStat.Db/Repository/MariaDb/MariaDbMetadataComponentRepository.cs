using DotStat.Common.Configuration.Interfaces;
using DotStat.Common.Exceptions;
using DotStat.Common.Localization;
using DotStat.Db.DB;
using DotStat.Db.Dto;
using DotStat.Db.Exception;
using DotStat.Db.Helpers;
using DotStat.Db.Repository;
using DotStat.Domain;
using MySqlConnector;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.MetadataStructure;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DotStat.DB.Repository.MariaDb
{
    public class MariaDbMetadataComponentRepository : DatabaseRepositoryBase<MariaDbDotStatDb>, IMetadataComponentRepository
    {
        public MariaDbMetadataComponentRepository(MariaDbDotStatDb dotStatDb, IGeneralConfiguration generalConfiguration) :
            base(dotStatDb, generalConfiguration)
        {
        }
        
        public async Task<int> GetMetadataAttributeDbId(IDotStatIdentifiable component, int msdDbId, CancellationToken cancellationToken, bool errorIfNotFound = false)
        {
            if (msdDbId <= 0)
                return -1;

            var componentItem = await GetMetadataAttributeComponent(component, msdDbId, cancellationToken);

            if (componentItem != null && componentItem.DbId > 0)
                return componentItem.DbId;

            if (!errorIfNotFound)
                return -1;

            throw new ArtefactNotFoundException(string.Format(
                LocalizationRepository.GetLocalisedResource(Localization.ResourceId.MetadataAttributeNotFound),
                component.Code, msdDbId)
            );
        }

        public async ValueTask<IList<MetadataAttributeItem>> GetMetadataAttributesOfMsd(Msd msd, CancellationToken cancellationToken)
        {
            if (msd==null || msd.DbId <= 0)
                return new List<MetadataAttributeItem>();

            var result = await GetMetadataAttributeComponentsByWhereClause("MSD_ID = @MsdId",
                cancellationToken,
                sqlParametersInWhereClause: new MySqlParameter("MsdId", MySqlDbType.Int32) { Value = msd.DbId });

            return result;
        }

        public async Task<IList<MetadataAttributeItem>> GetAllMetadataAttributeComponents(CancellationToken cancellationToken)
        {
            var result = await GetMetadataAttributeComponentsByWhereClause(null, cancellationToken);

            return result;
        }

        #region Private methods
        private async ValueTask<MetadataAttributeItem> GetMetadataAttributeComponent(IDotStatIdentifiable component, int msdDbId, CancellationToken cancellationToken)
        {
            if (component is not CodeListBasedIdentifiableDotStatObject<IMetadataAttributeObject>)
            {
                throw new DotStatException(string.Format(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.NotMetadataAttributeComponentType),
                    component.Code));
            }

            var metaAttr = component as MetadataAttribute;

            var result = await GetMetadataAttributeComponentsByWhereClause("ID = @Id AND MSD_ID = @MsdId",
                cancellationToken,
                new MySqlParameter("Id", MySqlDbType.VarChar) { Value = metaAttr?.HierarchicalId },
                new MySqlParameter("MsdId", MySqlDbType.Int32) { Value = msdDbId });

            return result.FirstOrDefault();
        }

        
        private async Task<IList<MetadataAttributeItem>> GetMetadataAttributeComponentsByWhereClause(string whereClause, CancellationToken cancellationToken, params DbParameter[] sqlParametersInWhereClause)
        {
            var result = new List<MetadataAttributeItem>();
            var sql = new StringBuilder();

            sql.AppendLine(
                "SELECT MTD_ATTR_ID,ID,MSD_ID,CL_ID,PARENT,MIN_OCCURS,MAX_OCCURS,IS_REPRESENTATIONAL,ENUM_ID");
            sql.Append("FROM ").Append(DotStatDb.ManagementSchema).AppendLine("_METADATA_ATTRIBUTE");

            if (!string.IsNullOrWhiteSpace(whereClause))
            {
                sql.Append("WHERE ").AppendLine(whereClause);
            }

            await using (var dr = await DotStatDb.ExecuteReaderSqlWithParamsAsync(sql.ToString(),  cancellationToken, parameters: sqlParametersInWhereClause))
            {
                while (await dr.ReadAsync(cancellationToken))
                {
                    result.Add(new MetadataAttributeItem
                    {
                        DbId = dr.GetInt32(0),
                        Id = dr.GetNullableString(1),
                        MsdId = dr.GetInt32(2),
                        CodelistId = dr.GetNullableValue<int>(3),
                        Parent = dr.GetNullableValue<int>(4),
                        MinOccurs = dr.GetNullableValue<int>(5),
                        MaxOccurs = dr.GetNullableValue<int>(6),
                        IsRepresentational = dr.GetNullableValue<int>(7),
                        EnumId = dr.GetNullableValue<long>(8)
                    });
                }
            }
            return result;
        }
        
        #endregion
    }
}
