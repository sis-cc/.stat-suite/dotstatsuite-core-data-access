﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using DotStat.Common.Configuration.Interfaces;
using DotStat.Common.Exceptions;
using DotStat.Common.Localization;
using DotStat.Db;
using DotStat.Db.DB;
using DotStat.Db.Dto;
using DotStat.Db.Exception;
using DotStat.Db.Helpers;
using DotStat.Db.Repository;
using DotStat.Domain;
using MySqlConnector;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;

namespace DotStat.DB.Repository.MariaDb
{
    public class MariaDbComponentRepository : DatabaseRepositoryBase<MariaDbDotStatDb>, IComponentRepository
    {
        public MariaDbComponentRepository(MariaDbDotStatDb dotStatDb, IGeneralConfiguration generalConfiguration) :
            base(dotStatDb, generalConfiguration)
        {
        }
        
        public async ValueTask<int> GetComponentDbId(IDotStatIdentifiable component, int dsdDbId, CancellationToken cancellationToken, bool errorIfNotFound = false)
        {
            if(dsdDbId <= 0)
                return -1;

            var componentItem = await GetComponent(component, dsdDbId, cancellationToken);

            if (componentItem != null && componentItem.DbId > 0)
                return componentItem.DbId;

            if (!errorIfNotFound)
                return -1;

            throw new ArtefactNotFoundException(string.Format(
                LocalizationRepository.GetLocalisedResource(Localization.ResourceId.ComponentNotFound),
                component.Code, componentItem?.DsdId)
            );
        }
        
        public async ValueTask<IList<ComponentItem>> GetComponentsOfDsd(Dsd dsd, CancellationToken cancellationToken)
        {
            if (dsd.DbId <= 0)
            {
                throw new ArgumentException(nameof(dsd));
            }

            var result = await GetComponentsByWhereClause("DSD_ID = @DsdId",
                cancellationToken,
                MySqlParametersInWhereClause: new MySqlParameter("DsdId", MySqlDbType.Int32) { Value = dsd.DbId });

            return result;
        }

        public async Task<IList<ComponentItem>> GetAllComponents(CancellationToken cancellationToken)
        {
            var result = await GetComponentsByWhereClause(null, cancellationToken);

            return result;
        }
        
        public async Task<bool> CheckTimeDimensionOfDsdInDb(Dsd dsd, DbTableVersion tableVersion, CancellationToken cancellationToken)
        {
            return await DotStatDb.ColumnExists(dsd.MariaDbFactTableWithSchema((char)tableVersion), MariaDbExtensions.TimeDimColumn, cancellationToken);
        }

        #region Private methods

        private async ValueTask<ComponentItem> GetComponent(IDotStatIdentifiable component, int dsdId, CancellationToken cancellationToken)
        {
            if (component is not CodeListBasedIdentifiableDotStatObject<IDimension> &&
                component is not CodeListBasedIdentifiableDotStatObject<IAttributeObject> &&
                component is not CodeListBasedIdentifiableDotStatObject<IPrimaryMeasure>)
            {
                throw new DotStatException(string.Format(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.UnknownComponentType),
                    component.Code));
            }

            var result = await GetComponentsByWhereClause(
                "ID = @Id AND DSD_ID = @DsdId AND TYPE = @Type",
                cancellationToken,
                CommandBehavior.SingleRow,
                true,
                new MySqlParameter("Id", MySqlDbType.VarChar) { Value = component.Code },
                new MySqlParameter("DsdId", MySqlDbType.Int32) { Value = dsdId },
                new MySqlParameter("Type", MySqlDbType.VarChar) { Value = component.DbType });

            return result.FirstOrDefault();
        }

        private async Task<IList<ComponentItem>> GetComponentsByWhereClause(string whereClause, CancellationToken cancellationToken, CommandBehavior commandBehavior = CommandBehavior.CloseConnection, bool tryUseReadOnlyConnection = true, params DbParameter[] MySqlParametersInWhereClause)
        {
            var result = new List<ComponentItem>();
            var sql = new StringBuilder();

            sql.AppendLine(
                "SELECT COMP_ID, ID, TYPE, DSD_ID, CL_ID, ATT_ASS_LEVEL, ATT_STATUS, ENUM_ID, ATT_GROUP_ID, MIN_LENGTH, MAX_LENGTH, PATTERN, MIN_VALUE, MAX_VALUE");
            sql.Append("FROM ").Append(DotStatDb.ManagementSchema).AppendLine("_COMPONENT");

            if (!string.IsNullOrWhiteSpace(whereClause))
            {
                sql.Append("WHERE ").AppendLine(whereClause);
            }

            await using (var dr = await DotStatDb.ExecuteReaderSqlWithParamsAsync(sql.ToString(), cancellationToken, commandBehavior, parameters: MySqlParametersInWhereClause))
            {
                while (await dr.ReadAsync(cancellationToken))
                {
                    result.Add(new ComponentItem
                    {
                        DbId = dr.GetInt32(0),
                        Id = dr.GetNullableString(1),
                        Type = dr.GetNullableString(2),
                        DsdId = dr.GetInt32(3),
                        CodelistId = dr.GetNullableValue<int>(4),
                        AttributeAssignmentlevel = Enum.TryParse<AttributeAttachmentLevel>(dr.GetNullableString(5), true, out var attributeAssignmentLevel) ?
                            attributeAssignmentLevel : AttributeAttachmentLevel.Null,
                        AttributeStatus = Enum.TryParse<AttributeAssignmentStatus>(dr.GetNullableString(6), true, out var attributeAssignmentStatus) ?
                            attributeAssignmentStatus : AttributeAssignmentStatus.Null,
                        EnumId = dr.GetNullableValue<long>(7),
                        AttributeGroup = dr.GetNullableString(8),
                        MinLength = dr.GetNullableValue<int>(9),
                        MaxLength = dr.GetNullableValue<int>(10),
                        Pattern = dr.GetNullableString(11),
                        MinValue = dr.GetNullableValue<int>(12),
                        MaxValue = dr.GetNullableValue<int>(13)
                    });
                }
            }
            return result;
        }

        #endregion


    }
}