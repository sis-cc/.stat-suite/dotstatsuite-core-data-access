﻿using System;
using System.Data;
using System.Threading;
using System.Threading.Tasks;
using DotStat.Common.Configuration.Interfaces;
using DotStat.Common.Exceptions;
using DotStat.Common.Localization;
using DotStat.Db;
using DotStat.Db.DB;
using DotStat.Db.Repository;

namespace DotStat.DB.Repository.MariaDb
{
    public class MariaDbCodelistRepository : DatabaseRepositoryBase<MariaDbDotStatDb>, ICodelistRepository
    {
        public MariaDbCodelistRepository(MariaDbDotStatDb dotStatDb, IGeneralConfiguration generalConfiguration) :
            base(dotStatDb, generalConfiguration)
        {
        }

        public async Task<string[]> GetDimensionCodesFromDb(int codeListId, CancellationToken cancellationToken)
        {
            return await GetCodelist(
                $"SELECT ITEM_ID, ID FROM {DotStatDb.ManagementSchema}_{MariaDbExtensions.GetCodelistTableName(codeListId)} ORDER BY ITEM_ID DESC",
                (id, r) => r.GetString(1), cancellationToken);
        }

        #region Private methods

        private async Task<T[]> GetCodelist<T>(string sql, Func<int, IDataReader, T> readCode, CancellationToken cancellationToken)
        {
            T[] codelist;

            using (var dr = await DotStatDb.ExecuteReaderSqlAsync(sql, cancellationToken))
            {
                if (!dr.Read())
                    throw new DotStatException(
                        LocalizationRepository.GetLocalisedResource(Localization.ResourceId.EmptyCodelist));

                var id = dr.GetInt32(0);
                codelist = new T[id + 1]; // dimension desc order, first row has max capasity
                codelist[id] = readCode(id, dr);

                while (dr.Read())
                {
                    id = dr.GetInt32(0);
                    codelist[id] = readCode(id, dr);
                }
            }

            return codelist;
        }
        #endregion

    }
}