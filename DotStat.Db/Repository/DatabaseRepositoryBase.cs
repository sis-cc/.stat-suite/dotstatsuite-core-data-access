﻿using System;
using System.Data.Common;
using DotStat.Common.Configuration.Interfaces;
using DotStat.Db.DB;

namespace DotStat.Db.Repository
{
    public abstract class DatabaseRepositoryBase<TDotStatDb> where TDotStatDb : IDotStatDb
    {
        protected readonly TDotStatDb DotStatDb;
        protected readonly IGeneralConfiguration GeneralConfiguration;


        protected DatabaseRepositoryBase(TDotStatDb dotStatDb, IGeneralConfiguration generalConfiguration)
        {
            DotStatDb = dotStatDb;
            GeneralConfiguration = generalConfiguration;
        }
        
        protected string GetString(DbDataReader reader, int index)
        {
            return !reader.IsDBNull(index) ? reader.GetString(index) : null;
        }

        protected DateTime? GetDateTime(DbDataReader reader, int index)
        {
            return !reader.IsDBNull(index) ? reader.GetDateTime(index) : (DateTime?) null;
        }

        protected int? GetInt(DbDataReader reader, int index)
        {
            return !reader.IsDBNull(index) ? reader.GetInt32(index) : (int?) null;
        }

    }
}
