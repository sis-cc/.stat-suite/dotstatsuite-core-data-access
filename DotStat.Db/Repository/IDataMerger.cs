﻿using System;
using DotStat.Db.Util;
using DotStat.DB.Util;
using DotStat.Domain;
using Org.Sdmxsource.Sdmx.Api.Model.Data;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace DotStat.DB.Repository
{
    public interface IDataMerger
    {
        Task<ImportSummary> MergeStagingTable(IImportReferenceableStructure referencedStructure,
            ReportedComponents reportedComponents,
            Dictionary<int, BatchAction> obsLevelBatchActions, IList<DataSetAttributeRow> dataSetAttributeRows,
            CodeTranslator translator,
            DbTableVersion tableVersion, bool includeSummary, CancellationToken cancellationToken);

        ValueTask MergeStagingToDimensionsTable(IImportReferenceableStructure referencedStructure, ReportedComponents reportedComponents, CancellationToken cancellationToken);

        ValueTask<MergeResult> MergeStagingToObservationsTable(IImportReferenceableStructure referencedStructure, ReportedComponents reportedComponents, DbTableVersion tableVersion, BatchAction batchAction, bool includeSummary, ICodeTranslator codeTranslator, DateTime dateTime, CancellationToken cancellationToken);

        ValueTask<MergeResult> MergeStagingToSeriesTable(IImportReferenceableStructure referencedStructure, ReportedComponents reportedComponents, DbTableVersion tableVersion, BatchAction batchAction, bool includeSummary, ICodeTranslator codeTranslator, DateTime dateTime, CancellationToken cancellationToken);

        ValueTask<MergeResult> MergeStagingToDatasetTable(IImportReferenceableStructure referencedStructure, IList<IKeyValue> attributes, CodeTranslator translator, DbTableVersion tableVersion, StagingRowActionEnum action, bool includeSummary, DateTime dateTime, CancellationToken cancellationToken);

    }
}
