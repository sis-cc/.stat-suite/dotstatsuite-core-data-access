﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using Dapper;
using DotStat.Db.DB;
using Estat.Sri.MappingStore.Store.Model;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;

namespace DotStat.Db.Util
{
    public class MariaDbDataTypeEnumHelper : DataTypeEnumHelper
    {
        public MariaDbDataTypeEnumHelper(IDotStatDb dotStatDb, bool tryUseReadOnlyConnection = true) : base(dotStatDb, tryUseReadOnlyConnection) {
        }
        

        protected override void InitDataTypeMap(bool tryUseReadOnlyConnection)
        {
            DataTypeMap = new Dictionary<long, string>();
            using (var connection = DotStatDb.GetDbConnection(tryUseReadOnlyConnection))
            {
                foreach (var enumValue in
                         connection.Query<EnumerationValue>(
                             $"SELECT ENUM_ID AS id, ENUM_NAME AS name, ENUM_VALUE AS value FROM {DotStatDb.ManagementSchema}_ENUMERATIONS WHERE ENUM_NAME='DataType'",
                             null, (IDbTransaction)null, false))
                {
                    DataTypeMap.Add(enumValue.ID, enumValue.Value);
                }
            }
        }
    }
}