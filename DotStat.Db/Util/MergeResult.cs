﻿using DotStat.Db.Validation;
using System.Collections.Generic;

namespace DotStat.DB.Util
{
    public class MergeResult
    {
        public int DeleteCount;
        public int InsertCount;
        public int UpdateCount;
        public int TotalCount;
        public bool RowsAddedDeleted;

        public List<IValidationError> Errors;

        public MergeResult()
        {
            Errors = new List<IValidationError>();
        }

        public MergeResult(int deleteCount, int insertCount, int updateCount, List<IValidationError> errors)
        {
            DeleteCount = deleteCount;
            InsertCount = insertCount;
            UpdateCount = updateCount;
            TotalCount = deleteCount + insertCount + updateCount;
            Errors = errors;
        }

        public void Add(MergeResult m)
        {
            DeleteCount += m.DeleteCount;
            InsertCount += m.InsertCount;
            UpdateCount += m.UpdateCount;
            TotalCount = DeleteCount + InsertCount + UpdateCount;
            Errors.AddRange(m.Errors);
        }
    }
}
