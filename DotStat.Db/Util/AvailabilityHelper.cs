﻿using System;
using System.Collections.Generic;
using System.Linq;
using DotStat.Domain;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Registry;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;

namespace DotStat.Db.Util
{
    public class AvailabilityHelper
    {
        private readonly Dictionary<string, HashSet<string>> _availabilityInfo;
        private DateTime? _periodStart;
        private DateTime? _periodEnd;

        public AvailabilityHelper(Dsd dsd)
        {
            if(dsd == null)
                throw new ArgumentNullException(nameof(dsd));

            var nonTimeDims   = dsd.Dimensions.Where(x => !x.Base.TimeDimension).ToArray();
            _availabilityInfo = new Dictionary<string, HashSet<string>>();
            foreach (var dim in nonTimeDims)
            {
                _availabilityInfo.Add(dim.Code, new HashSet<string>());
            }
        }

        public void AddDimKey(string concept, string newCode)
        {
            _availabilityInfo[concept].Add(newCode);
        }

        public void AddTime(DateTime? periodStart, DateTime? periodEnd)
        {
            _periodStart = periodStart;
            _periodEnd = periodEnd;
        }

        public KeyValuesMutableImpl GetAvailabilityByDimension(Dimension dimension)
        {
            if(dimension==null)
                throw new ArgumentException(nameof(dimension));

            var keyValue = new KeyValuesMutableImpl()
            {
                Id = dimension.Code
            };

            if (!dimension.Base.TimeDimension)
            {
                var availableDimMembers = _availabilityInfo[dimension.Code];

                foreach (var code in availableDimMembers)
                    keyValue.AddValue(code);
            }
            else if(_periodStart!=null && _periodEnd != null)
            {
                keyValue.TimeRange = new TimeRangeMutableCore
                {
                    StartDate = new SdmxDateCore(_periodStart.Value, TimeFormatEnumType.DateTime),
                    IsStartInclusive = true,
                    EndDate = new SdmxDateCore(_periodEnd.Value, TimeFormatEnumType.DateTime),
                    IsEndInclusive = true
                };
            }

            return keyValue;
        }
      
    }
}