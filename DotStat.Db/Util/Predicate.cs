﻿using System;
using System.Collections.Generic;
using System.Linq;
using DotStat.DB;
using DotStat.Domain;
using DotStat.MappingStore;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Exception;
using Org.Sdmxsource.Sdmx.Api.Model.Data.Query;
using Attribute = DotStat.Domain.Attribute;

namespace DotStat.Db.Util
{
    public enum NullTreatment
    {
        IncludeNulls,
        IncludeNullsAndSwitchOff,
        ExcludeNulls
    }

    public abstract class Predicate
    {
        public static string BuildWhereForObservationLevel(
            IDataQuery dataQuery,
            Dataflow dataFlow,
            ICodeTranslator codeTranslator,
            bool filterLastUpdated = false,
            NullTreatment nullTreatment = NullTreatment.ExcludeNulls,
            bool useExternalValues = true,
            bool useExternalColumnNames = true,
            bool includeTimeConstraints = true,
            bool includeSpecialValues = false,
            string tableAlias = "",
            DbType serverType = DbType.SqlServer,
            bool includeAttributeConstraints = false,
            AttributeAttachmentLevel[] attributeAttachmentLevels = null
        ) {
            GetDelimiterValues(serverType, out string startDelim, out string endDelim);
            var where = BuildDimQuery(dataQuery, dataFlow, codeTranslator, nullTreatment, true, useExternalValues,
                useExternalColumnNames, includeSpecialValues, includeTimeConstraints, tableAlias, serverType, includeAttributeConstraints, attributeAttachmentLevels);

            //Last updated
            if (filterLastUpdated && dataQuery?.LastUpdatedDate is not null)
                where.Add($"{startDelim}{DbExtensions.LAST_UPDATED_COLUMN}{endDelim} >= @LastUpdated");

            return where.Any()
                ? string.Join(" AND ", where)
                : null;
        }

        public static void GetDelimiterValues(DbType serverType, out string startDelim, out string endDelim) 
        {
            switch (serverType)
            {
                case DbType.SqlServer:
                    startDelim = "[";
                    endDelim = "]";
                    break;
                case DbType.MariaDb:
                    startDelim = "`";
                    endDelim = "`";
                    break;
                default:
                    startDelim = "[";
                    endDelim = "]";
                    break;
            }
        }
       
        private static List<string> BuildDimQuery(
            IDataQuery dataQuery,
            Dataflow dataFlow,
            ICodeTranslator codeTranslator,
            NullTreatment nullTreatment,
            bool applyConstraints,
            bool useExternalValues,
            bool useExternalColumnNames,
            bool includeSpecialValues,
            bool includeTimeConstraints,
            string tableAlias,
            DbType serverType,
            bool includeAttributeConstraints,
            AttributeAttachmentLevel[] attributeAttachmentLevels = null
        )
        {
            GetDelimiterValues(serverType, out string startDelim, out string endDelim);
            
            var where = new List<string>();
            var appliedConstraints = new HashSet<string>();
            var querySelectionGroup = dataQuery?.SelectionGroups?.FirstOrDefault();
            var tablePrefix = string.IsNullOrEmpty(tableAlias) ? null : $"{startDelim}{tableAlias}{endDelim}.";

            //Apply query filters if any given by the user
            if (querySelectionGroup != null)
            {
                // non time dimensions
                foreach (var selection in querySelectionGroup.Selections)
                {
                    var filteredSelection = new List<string>(selection.Values.Count);
                    var dim = dataFlow.FindDimension(selection.ComponentId);
                    var dimColumn = GetDimColumn(serverType, dim, useExternalColumnNames);

                    // filter out non-existing codes & constrained codes from user selection
                    foreach (var mem in selection.Values)
                    {
                        if (dim.Base.HasCodedRepresentation() && !dim.HasCode(mem))
                        {
                            continue; // Non existing code
                        }

                        if (applyConstraints && !dim.IsAllowed(mem))
                        {
                            continue; // Constrained code
                        }

                        var value = useExternalValues || dim.Codelist == null
                            ? $"'{mem}'" 
                            : codeTranslator.TranslateCodeToId(dim.Code, mem).ToString();
                        
                        filteredSelection.Add(value);
                    }

                    if (!filteredSelection.Any())
                        throw new SdmxNoResultsException();
                    
                    var values = filteredSelection.ToList();

                    var predicate = new CodelistPredicate(
                        $"{tablePrefix}{dimColumn}",
                        values,
                        nullTreatment,
                        useExternalValues);

                    where.Add(predicate.ToString());

                    // As selection already narrowed down by user selection no need to apply dim constraint again
                    appliedConstraints.Add(dim.Code);
                }
            }

            if (!applyConstraints)
            {
                return where;
            }

            //No dimension constraints if dataFlow ref is null
            if (dataFlow == null)
            {
                return where;
            }

            // Apply dimension constraints ------------------------------
            foreach (var dim in dataFlow.Dimensions.Where(d => !appliedConstraints.Contains(d.Code) && d.Constraint?.IsCodeListBased == true))
            {
                var dimColumn = GetDimColumn(serverType, dim, useExternalColumnNames);
                var values = dim.Constraint.Codes
                    .Where(code => dim.Codelist.Codes.Any(clCode => clCode.Code.Contains(code)))
                    .Select(code => useExternalValues ? $"'{code}'" : codeTranslator.TranslateCodeToId(dim.Code, code).ToString())
                    .ToList();

                if (includeSpecialValues)
                {
                    values.Add(DbExtensions.DimensionSwitchedOffDbValue.ToString());
                }

                if (!values.Any())
                {
                    continue;
                }

                var predicate = new CodelistPredicate(
                    $"{tablePrefix}{dimColumn}",
                    values,
                    nullTreatment,
                    useExternalValues);

                where.Add(predicate.ToString());
            }

            if (includeAttributeConstraints)
            {
                IEnumerable<Attribute> attributesToInclude;
                if (attributeAttachmentLevels is null || attributeAttachmentLevels.Length == 0)
                    attributesToInclude = dataFlow.Dsd.Attributes.Where(a =>
                        !appliedConstraints.Contains(a.Code) && a.Constraint?.IsCodeListBased == true);
                else
                    attributesToInclude = dataFlow.Dsd.Attributes.Where(a =>
                        attributeAttachmentLevels.Contains(a.Base.AttachmentLevel) &&
                        !appliedConstraints.Contains(a.Code) && a.Constraint?.IsCodeListBased == true);

                // Apply attribute constraints ------------------------------
                foreach (var attr in attributesToInclude)
                {
                    var attrColumn = GetAttrColumn(serverType, attr, useExternalValues);
                    var values = attr.Constraint.Codes
                        .Where(code => attr.Codelist.Codes.Any(cl_code => cl_code.Code.Contains(code)))
                        .Select(code =>
                            useExternalValues
                                ? $"'{code}'"
                                : codeTranslator.TranslateCodeToId(attr.Code, code).ToString())
                        .ToList();

                    if (!values.Any())
                        continue;

                    var predicate = new CodelistPredicate(
                        $"{tablePrefix}{attrColumn}",
                        values,
                        nullTreatment,
                        useExternalValues
                    );

                    where.Add(predicate.ToString());
                }
            }
            // Apply time constraint/query range filter
            if (includeTimeConstraints)
            {
                AddTimePredicate(where, dataFlow, querySelectionGroup, tableAlias, nullTreatment, serverType);
            }

            return where;
        }
        
        public static string GetDimColumn(DbType serverType, Dimension dim, bool useExternalValues) 
        {
            switch (serverType) 
            {
                case DbType.MariaDb:
                    return dim.MariaDbColumn(externalColumn: useExternalValues);
                default:
                    return dim.SqlColumn(externalColumn: useExternalValues);
            };
        }

        public static string GetAttrColumn(DbType serverType, Attribute attr, bool useExternalValues)
        {
            switch (serverType)
            {
                case DbType.MariaDb:
                    return attr.MariaDbColumn(externalColumn: useExternalValues);
                default:
                    return attr.SqlColumn(externalColumn: useExternalValues);
            };
        }

        #region Time predicate

        public static void AddTimePredicate(
            List<string> where,
            Dataflow dataFlow,
            IDataQuerySelectionGroup querySelectionGroup,
            string tableAlias,
            NullTreatment nullTreatment = NullTreatment.ExcludeNulls,
            DbType serverType = DbType.SqlServer)
        {
            var timeDim = dataFlow.Dsd.TimeDimension;

            if (timeDim == null)
            {
                return;
            }

            var timeRange = timeDim.Constraint?.TimeRange;

            var startDate = Max(querySelectionGroup?.DateFrom?.Date, timeRange?.StartDate?.Date);
            var endDate = Min(querySelectionGroup?.DateTo?.EndDate(), timeRange?.EndDate?.Date);

            GetDelimiterValues(serverType, out string startDelim, out string endDelim);
            GetPeriodValues(serverType, out string periodStart, out string periodEnd);

            // todo refactor predicate logic to use db parameters

            var tablePrefix = string.IsNullOrEmpty(tableAlias) ? null : $"{startDelim}{tableAlias}{endDelim}.";
            if (startDate != null)
            {
                var timeClause = $"{tablePrefix}{periodEnd}>='{startDate:yyyy-MM-dd}'";
                var nullClause = "";
                if (nullTreatment is NullTreatment.IncludeNulls)
                {
                    nullClause = $" OR {tablePrefix}{periodEnd} IS NULL";
                }
                else if (nullTreatment is NullTreatment.IncludeNullsAndSwitchOff)
                {
                    var switchOffValue = dataFlow.Dsd.Base.HasSupportDateTimeAnnotation() ?
                        $"{DateTime.MinValue:yyyy-MM-dd hh:mm:ss}" : $"{DateTime.MinValue.Date:yyyy-MM-dd}";

                    nullClause = $" OR {tablePrefix}{periodEnd} IS NULL OR {tablePrefix}{periodEnd} = '{switchOffValue}'";
                }

                where.Add($"({timeClause}{nullClause})");
            }

            if (endDate != null)
            {
                var timeClause = $"{tablePrefix}{periodStart}<'{endDate:yyyy-MM-dd}'";
                var nullClause = "";
                if (nullTreatment is NullTreatment.IncludeNulls)
                {
                    nullClause = $" OR {tablePrefix}{periodStart} IS NULL";
                }
                else if (nullTreatment is NullTreatment.IncludeNullsAndSwitchOff)
                {
                    var switchOffValue = dataFlow.Dsd.Base.HasSupportDateTimeAnnotation() ?
                        $"{DateTime.MaxValue:yyyy-MM-dd hh:mm:ss}" : $"{DateTime.MaxValue.Date:yyyy-MM-dd}";

                    nullClause = $" OR {tablePrefix}{periodStart} IS NULL OR {tablePrefix}{periodStart} = '{switchOffValue}'";
                }

                where.Add($"({timeClause}{nullClause})");
            }
        }
        public static void GetPeriodValues(DbType serverType, out string periodStart, out string periodEnd) 
        {
            switch (serverType) 
            {
                case DbType.MariaDb:
                    periodStart = MariaDbExtensions.MariaDbPeriodStart();
                    periodEnd = MariaDbExtensions.MariaDbPeriodEnd();
                    break;
                default:
                    periodStart = DbExtensions.SqlPeriodStart();
                    periodEnd = DbExtensions.SqlPeriodEnd();
                    break;
            };
        }

        private static DateTime? Max(DateTime? dt1, DateTime? dt2) => dt2 == null || dt1 > dt2 ? dt1 : dt2;

        private static DateTime? Min(DateTime? dt1, DateTime? dt2) => dt2 == null || dt1 < dt2 ? dt1 : dt2;

        #endregion
        
        public abstract string ToString(NullTreatment nullTreatment);
    }
}