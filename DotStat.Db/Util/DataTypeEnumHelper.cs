﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using Dapper;
using DotStat.Db.DB;
using Estat.Sri.MappingStore.Store.Model;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;

namespace DotStat.Db.Util
{
    public class DataTypeEnumHelper
    {
        public Dictionary<long, string> DataTypeMap;
        public readonly IDotStatDb DotStatDb;

        public DataTypeEnumHelper(IDotStatDb dotStatDb, bool tryUseReadOnlyConnection = true)
        {
            DotStatDb = dotStatDb;

            InitData(tryUseReadOnlyConnection);
        }

        public long? GetIdFromValue(string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return null;
            }

            return DataTypeMap.FirstOrDefault(kvp => kvp.Value == value).Key;
        }

        public long? GetIdFromTextFormat(ITextFormat textFormat)
        {
            return GetIdFromValue(textFormat == null ? TextEnumType.String.ToString() : textFormat.TextType?.EnumType.ToString());
        }

        public string GetValueFromId(long value)
        {
            // Estat.Sri.MappingStore.Store.Model.EnumerationValue
            if (DataTypeMap.TryGetValue(value, out var enumText))
            {
                return enumText;
            }

            return null;
        }

        private void InitData(bool tryUseReadOnlyConnection) {
            InitDataTypeMap(tryUseReadOnlyConnection);
        }

        protected virtual void InitDataTypeMap(bool tryUseReadOnlyConnection)
        {
            DataTypeMap = new Dictionary<long, string>();
            using (var connection = DotStatDb.GetDbConnection(tryUseReadOnlyConnection))
            {
                foreach (var enumValue in
                    connection.Query<EnumerationValue>(
                        $"SELECT ENUM_ID AS id, ENUM_NAME AS name, ENUM_VALUE AS value FROM [{DotStatDb.ManagementSchema}].[ENUMERATIONS] WHERE ENUM_NAME='DataType'",
                        null, (IDbTransaction)null, false))
                {
                    DataTypeMap.Add(enumValue.ID, enumValue.Value);
                }
            }
        }
    }
}