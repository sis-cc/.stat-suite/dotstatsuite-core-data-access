﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using DotStat.Db.Dto;
using DotStat.Db.Util;
using DotStat.Domain;
using DotStat.MappingStore;
using Transaction = DotStat.Domain.Transaction;

namespace DotStat.Db.Service
{
    public interface IDotStatDbService
    {
        Task<bool> TryNewTransaction(Transaction transaction, IImportReferenceableStructure referencedStructure,
                IMappingStoreDataAccess mappingStoreDataAccess,
                CancellationToken cancellationToken, bool onlyLockTransaction = false);

        Task<bool> TryNewTransactionWithNoDsd(Transaction transaction, CancellationToken cancellationToken, bool onlyLockTransaction = false);

        Task<bool> TryNewTransactionWithDsdId(Transaction transaction, int dsdId, int? dsdChildDbId, string agency, string id, SdmxVersion version, CancellationToken cancellationToken, bool onlyLockTransaction = false);
        
        Task<bool> CloseTransaction(
            Transaction transaction,
            IImportReferenceableStructure referencedStructure,
            bool PITRestorationAllowed,
            bool calculateActualContentConstraint,
            bool includeRelatedDataFlows,
            IMappingStoreDataAccess mappingStoreDataAccess,
            bool? optimizeData,
            bool? optimizeMetadata,
            ICodeTranslator codeTranslator,
            CancellationToken cancellationToken);

        Task OptimizeDataForHighPerformanceReads(IImportReferenceableStructure referencedStructure, ICodeTranslator codeTranslator, IMappingStoreDataAccess mappingStoreDataAccess, CancellationToken cancellationToken);
        Task OptimizeMetadataForHighPerformanceReads(IImportReferenceableStructure referencedStructure, ICodeTranslator codeTranslator, CancellationToken cancellationToken);

        Task CleanUpFailedTransaction(Transaction transaction);

        Task CloseCanceledOrTimedOutTransaction(int transactionId);

        Task<TransactionStatus> GetFinalTransactionStatus(int transactionId, CancellationToken cancellationToken);

        Task FillIdsFromDisseminationDb(IImportReferenceableStructure referencedStructure, CancellationToken cancellationToken);

        Task FillDbIdsAndCreateMissingDbObjects(IImportReferenceableStructure referencedStructure, IMappingStoreDataAccess mappingStoreDataAccess, CancellationToken cancellationToken, bool skipDsd = true, bool throwConcurrencyError = true);

        Task<bool> Rollback(IImportReferenceableStructure referencedStructure, IMappingStoreDataAccess mappingStoreDataAccess, CancellationToken cancellationToken);

        Task<bool> Restore(IImportReferenceableStructure referencedStructure, IMappingStoreDataAccess mappingStoreDataAccess, CancellationToken cancellationToken);

        Task ApplyPITRelease(IImportReferenceableStructure referencedStructure, bool isRestorationAllowed, IMappingStoreDataAccess mappingStoreDataAccess);

        Task ManageMappingSets(IImportReferenceableStructure referencedStructure,
            IMappingStoreDataAccess mappingStoreDataAccess,
            bool includeRelatedDataFlows,
            CancellationToken cancellationToken);

        Task<bool> InitializeAllMappingSets(Transaction transaction, IMappingStoreDataAccess mappingStoreDataAccess, CancellationToken cancellationToken);

        Task<bool> CleanUpDsd(int dsdDbId, bool deleteMsdObjectsOnly, IMappingStoreDataAccess mappingStoreDataAccess, List<ComponentItem> allComponents, List<MetadataAttributeItem> allMetadataAttributeComponents, CancellationToken cancellationToken);

        DbTableVersion? GetDsdLiveVersion(Dsd dsd);
        DbTableVersion? GetDsdPITVersion(Dsd dsd);

        Task<bool> ChangeDataCompression(Dsd dsd, DataCompressionEnum dataCompression, IMappingStoreDataAccess mappingStoreDataAccess, CancellationToken cancellationToken);
        Task DeleteMetadata(Dsd dsd, DbTableVersion tableVersion, IMappingStoreDataAccess mappingStoreDataAccess, CancellationToken cancellationToken);
        ValueTask CopyMetadataToNewVersion(Dsd dsd, DbTableVersion sourceTableVersion, DbTableVersion targetTableVersion, IMappingStoreDataAccess mappingStoreDataAccess, CancellationToken cancellationToken);

    }
}
