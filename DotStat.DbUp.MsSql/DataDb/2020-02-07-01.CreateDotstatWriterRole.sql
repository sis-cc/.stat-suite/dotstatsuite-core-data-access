--Create role if it does not exist yet
IF NOT EXISTS(SELECT 1 FROM sys.database_principals WHERE TYPE = 'R' AND name = 'DotStatWriter')
BEGIN
   CREATE ROLE [DotStatWriter]
END
GO

--Add role to datareader and datawriter roles
ALTER ROLE [db_datareader] ADD MEMBER [DotStatWriter]
GO
ALTER ROLE [db_datawriter] ADD MEMBER [DotStatWriter]
GO

--Grant ddl permission on data and management schemas to role
GRANT ALTER ON SCHEMA::[data] TO [DotStatWriter]
GO
GRANT ALTER ON SCHEMA::[management] TO [DotStatWriter]
GO
GRANT CREATE TABLE TO [DotStatWriter]
GO

-- If user already exists in db, then remove datareader, datawriter and ddladmin roles added by previous version of DbUp and add to DotStatWriter role
IF EXISTS (SELECT 1 FROM sys.database_principals WHERE TYPE = 'S' AND name = '$loginName$')
BEGIN    
    ALTER ROLE [DotStatWriter] ADD MEMBER [$loginName$];

	ALTER ROLE [db_ddladmin] DROP MEMBER [$loginName$];
	ALTER ROLE [db_datareader] DROP MEMBER [$loginName$];
	ALTER ROLE [db_datawriter] DROP MEMBER [$loginName$];
END;
GO