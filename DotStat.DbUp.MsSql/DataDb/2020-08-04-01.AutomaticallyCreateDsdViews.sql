﻿SET NOCOUNT ON;
SET CONCAT_NULL_YIELDS_NULL ON;
GO


--CLEAN TEMP VIEWS
IF OBJECT_ID(N'tempdb..#Temp_DSD_Components') IS NOT NULL
BEGIN
DROP TABLE #Temp_DSD_Components
END
IF OBJECT_ID(N'tempdb..#Temp_DSDs') IS NOT NULL
BEGIN
DROP TABLE #Temp_DSDs
END
IF OBJECT_ID(N'tempdb..#DimGroupAttributes') IS NOT NULL
BEGIN
DROP TABLE #DimGroupAttributes
END
IF OBJECT_ID(N'tempdb..#AllDimensions') IS NOT NULL
BEGIN
DROP TABLE #AllDimensions
END


 SELECT ART_ID, ID
 INTO #Temp_DSDs
 FROM [management].[ARTEFACT] 
 WHERE [type] = 'DSD'
  
DECLARE @DSD_ID int,
	@DSD NVARCHAR(MAX),
	@msg NVARCHAR(MAX),
	@Transaction_ID int,
    @sql NVARCHAR(MAX),
    @viewA NVARCHAR(MAX),
    @viewB NVARCHAR(MAX),
	@SelectPart NVARCHAR(MAX),
	@FromPart NVARCHAR(MAX),
	@JoinPart NVARCHAR(MAX),
	@CTEDimGroupAttr NVARCHAR(MAX),
	@JoinSubDimGroupPart NVARCHAR(MAX),
	@DimGroupAttr int,
	@HasTimeDim as bit,
	@HasDimGroupAttr as bit,
    @NewLineChar AS CHAR(2) =  CHAR(10);

WHILE (Select Count(*) FROM #Temp_DSDs) > 0
BEGIN

    SELECT TOP 1 @DSD_ID = ART_ID FROM #Temp_DSDs

    SELECT c.DSD_ID, c.ID, c.CL_ID, c.COMP_ID,c.[TYPE], c.ATT_ASS_LEVEL, c.ATT_STATUS
    INTO #Temp_DSD_Components
    FROM [management].[ARTEFACT] a 
    INNER JOIN [management].[COMPONENT] c 
    ON a.ART_ID = c.DSD_ID
    WHERE A.ART_ID=@DSD_ID

	SELECT @HasTimeDim = 0
	IF EXISTS(SELECT 1 FROM sys.columns 
          WHERE Name = N'DIM_TIME'
          AND Object_ID = Object_ID(N'[data].[FACT_'+CAST(@DSD_ID AS VARCHAR)+'_A]'))
	BEGIN
		SELECT @HasTimeDim=1
	END

	--SELECT * FROM #Temp_DSD_Components
	
----------------------------START BUILD SELECT STATEMENT---------------------------
	SELECT @SelectPart='SELECT '+ STUFF(( 
		SELECT 
		  CASE 
		  --coded dimensions
			WHEN C.[CL_ID] IS NOT NULL AND C.[TYPE] = 'Dimension'  --(C.[ATT_ASS_LEVEL] IS NULL OR C.[ATT_ASS_LEVEL] <> 'DataSet') --
				THEN '[CL_'+[C].[ID]+'].[ID] AS ['+[C].[ID]+'],'+@NewLineChar
		  --Coded attributes at observation level
			WHEN C.[CL_ID] IS NOT NULL AND C.[TYPE] = 'Attribute' AND C.[ATT_ASS_LEVEL] = 'Observation'
				THEN '[CL_'+[C].[ID]+'].[ID] AS ['+[C].[ID]+'],'+@NewLineChar
		  --Non-coded dimensions
		   WHEN C.[CL_ID] IS NULL AND C.[TYPE] = 'Dimension'
				THEN '[FI].[DIM_'+CAST(C.[COMP_ID] AS VARCHAR)+'] AS ['+[C].[ID]+'],'+@NewLineChar
		  --Non-coded attributes at observation level
		   WHEN C.[CL_ID] IS NULL AND C.[TYPE] = 'Attribute' AND C.[ATT_ASS_LEVEL] = 'Observation'
				THEN '[FA].[COMP_'+CAST(C.[COMP_ID] AS VARCHAR)+'] AS ['+[C].[ID]+'],'+@NewLineChar
		  --dimgroup level
		   WHEN C.[TYPE] = 'Attribute' AND (C.[ATT_ASS_LEVEL] = 'DimensionGroup' OR C.[ATT_ASS_LEVEL] = 'Group')
				THEN '[ATTR_'+CAST(C.[COMP_ID] AS VARCHAR)+'].[ID] AS ['+[C].[ID]+'],'+@NewLineChar
			END
		FROM #Temp_DSD_Components C  FOR XML PATH('')
    ), 1, 0, '') 

	
	--check if dsd has time dimension
	IF (@HasTimeDim = 1)
	BEGIN
		SELECT @SelectPart= @SelectPart + '[T].[ID] AS [TIME_PERIOD], '+@NewLineChar
	END

	SELECT @SelectPart= @SelectPart + '[FA].[VALUE] AS [OBS_VALUE]'+@NewLineChar
	
--------------------------------END BUILD SELECT STATEMENT-------------------------------

	
--------------------------------START BUILD FROM STATEMENT-------------------------------
	SELECT @FromPart='FROM [data].[FILT_'+CAST(@DSD_ID AS VARCHAR)+'] FI'+@NewLineChar+'INNER JOIN [data].[FACT_'+CAST(@DSD_ID AS VARCHAR)+'_{V}] FA ON FI.[SID] = FA.[SID]'+@NewLineChar
--------------------------------END BUILD FROM STATEMENT-------------------------------------
	
--------------------------------START JOIN DIM GROUP ATTRIBUTES STATEMENT------------------------------------

	SELECT COMP_ID
	INTO #AllDimensions
	FROM #Temp_DSD_Components 
    WHERE DSD_ID = @DSD_ID AND [TYPE] ='Dimension'

	SELECT *
	INTO #DimGroupAttributes
	FROM #Temp_DSD_Components 
    WHERE DSD_ID = @DSD_ID AND (ATT_ASS_LEVEL ='DimensionGroup' OR [ATT_ASS_LEVEL] = 'Group')
	--SELECT * FROM #DimGroupAttributes

	SELECT @HasDimGroupAttr= CASE WHEN @@ROWCOUNT=0 THEN 0 ELSE 1 END
	IF(@HasDimGroupAttr=1) 
		BEGIN SELECT @CTEDimGroupAttr ='WITH '	END
	ELSE	
		BEGIN SELECT @CTEDimGroupAttr = ''
	END
	SELECT @JoinSubDimGroupPart = ''
	
	WHILE (SELECT Count(*) FROM #DimGroupAttributes) > 0
	BEGIN
		SELECT TOP 1 @DimGroupAttr = COMP_ID FROM #DimGroupAttributes
		SELECT @CTEDimGroupAttr += 'ATTR_'+CAST(@DimGroupAttr AS VARCHAR)+' AS('+@NewLineChar
		
		--SELECT PART
		SELECT @CTEDimGroupAttr += 'SELECT'+ STUFF(( 
			SELECT ', ADG.DIM_'+CAST(DIM_ID AS VARCHAR)
			FROM #DimGroupAttributes DG
			INNER JOIN [management].[ATTR_DIM_SET] ADS ON DG.COMP_ID = ADS.ATTR_ID
			WHERE COMP_ID = @DimGroupAttr   FOR XML PATH('')
		), 1,1, '')
		SELECT @CTEDimGroupAttr +=
			CASE WHEN CL_ID IS NOT NULL
				THEN ', [CL_'+CAST(ID AS VARCHAR)+'].ID AS [ID]'+@NewLineChar --CODED
			ELSE 
				', ADG.COMP_'+CAST(COMP_ID AS VARCHAR)+' AS [ID]'+@NewLineChar --NON-CODED
			END
		FROM #DimGroupAttributes
		WHERE COMP_ID = @DimGroupAttr

		--FROM PART
		SELECT @CTEDimGroupAttr += 'FROM [data].[ATTR_'+CAST(@DSD_ID AS VARCHAR)+'_{V}_DIMGROUP] ADG'+@NewLineChar
		SELECT @CTEDimGroupAttr +=
			CASE WHEN CL_ID IS NOT NULL
				THEN 'INNER JOIN [management].[CL_'+CAST(CL_ID AS VARCHAR)+'] AS [CL_'+[ID]+'] ON ADG.COMP_'+CAST(COMP_ID AS VARCHAR)+' = [CL_'+[ID]+'].[ITEM_ID]'+@NewLineChar --CODED
			ELSE ''--NON-CODED
			END
		FROM #DimGroupAttributes

		WHERE COMP_ID = @DimGroupAttr
		--WHERE PART
		SELECT  @CTEDimGroupAttr += 'WHERE'+ STUFF(( 
			SELECT  'AND '+ 
			 CASE WHEN ATTR.[ID_REFERENCED] = 1 
				THEN 'DIM_'+CAST(COMP_ID AS VARCHAR)+ ' IS NOT NULL '
			 ELSE 'DIM_'+CAST(COMP_ID AS VARCHAR)+ ' IS NULL '
			 END
			FROM #AllDimensions D
			LEFT JOIN (
				SELECT 1 AS [ID_REFERENCED], DIM_ID
				FROM #DimGroupAttributes DG
				INNER JOIN [management].[ATTR_DIM_SET] ADS ON DG.COMP_ID = ADS.ATTR_ID
				WHERE COMP_ID = @DimGroupAttr 
			) ATTR ON DIM_ID = D.[COMP_ID]  FOR XML PATH('')
		), 1,3, '') +@NewLineChar
		
		--CTE join
		SELECT @JoinSubDimGroupPart +=
			CASE WHEN ATT_STATUS='Mandatory' 
				THEN 'INNER JOIN [ATTR_'+CAST(COMP_ID AS VARCHAR)+'] ON' --Mandatory
			ELSE 
				'LEFT JOIN [ATTR_'+CAST(COMP_ID AS VARCHAR)+'] ON' --Conditional
			END
		FROM #DimGroupAttributes
		WHERE COMP_ID = @DimGroupAttr

		SELECT  @JoinSubDimGroupPart += STUFF(( 
			SELECT 'AND '+'[FI].[DIM_'+CAST(DIM_ID AS VARCHAR)+'] = [ATTR_'+CAST(COMP_ID AS VARCHAR)+'].[DIM_'+CAST(DIM_ID AS VARCHAR)+'] '
			FROM #DimGroupAttributes DG
			INNER JOIN [management].[ATTR_DIM_SET] ADS ON DG.COMP_ID = ADS.ATTR_ID
			WHERE COMP_ID = @DimGroupAttr  FOR XML PATH('')
		), 1,3, '') +@NewLineChar

		SELECT @CTEDimGroupAttr +='),'+@NewLineChar
		DELETE #DimGroupAttributes WHERE COMP_ID = @DimGroupAttr
    END

	IF(@HasDimGroupAttr=1) 
	BEGIN 
		SELECT @CTEDimGroupAttr = SUBSTRING(@CTEDimGroupAttr, 0, LEN(@CTEDimGroupAttr)-1)+@NewLineChar
	END

	DROP TABLE #DimGroupAttributes
	DROP TABLE #AllDimensions

--------------------------------END JOIN DIM GROUP ATTRIBUTES STATEMENT-------------------------------------


--------------------------------START JOIN FROM STATEMENT------------------------------------
	SELECT @JoinPart= STUFF(( 
		SELECT 
		  CASE 
		  --coded dimensions
			WHEN C.[CL_ID] IS NOT NULL AND C.[TYPE] = 'Dimension'
				THEN 'INNER JOIN [management].[CL_'+CAST(C.[CL_ID] AS VARCHAR)+'] AS [CL_'+[C].[ID]+'] ON FI.[DIM_'+CAST(C.[COMP_ID] AS VARCHAR)+'] = [CL_'+[C].[ID]+'].[ITEM_ID]'+@NewLineChar
		  --coded mandatory attributes at observation level
			WHEN C.[CL_ID] IS NOT NULL AND C.[TYPE] = 'Attribute' AND C.[ATT_ASS_LEVEL] = 'Observation' AND  C.ATT_STATUS = 'Mandatory'
				THEN 'INNER JOIN [management].[CL_'+CAST(C.[CL_ID] AS VARCHAR)+'] AS [CL_'+[C].[ID]+'] ON FA.[COMP_'+CAST(C.[COMP_ID] AS VARCHAR)+'] = [CL_'+[C].[ID]+'].[ITEM_ID]'+@NewLineChar
		  --coded conditional attributes at observation level
			WHEN C.[CL_ID] IS NOT NULL AND C.[TYPE] = 'Attribute' AND C.[ATT_ASS_LEVEL] = 'Observation' AND  C.ATT_STATUS = 'Conditional'
				THEN 'LEFT JOIN [management].[CL_'+CAST(C.[CL_ID] AS VARCHAR)+'] AS [CL_'+[C].[ID]+'] ON FA.[COMP_'+CAST(C.[COMP_ID] AS VARCHAR)+'] = [CL_'+[C].[ID]+'].[ITEM_ID]'+@NewLineChar
			END
		FROM #Temp_DSD_Components C  FOR XML PATH('')
    ), 1, 0, '') 

	--DIMGROUP lvl attributes
	SELECT @JoinPart+= @JoinSubDimGroupPart

	--check if dsd has time dimension
	IF (@HasTimeDim = 1)
	BEGIN
		SELECT @JoinPart+= 'INNER JOIN [management].[CL_TIME] T ON FA.[DIM_TIME] = T.[ITEM_ID]'+@NewLineChar
	END	
------------------------------------END JOIN FROM STATEMENT---------------------------------------

	BEGIN TRY  
		SELECT @sql = @CTEDimGroupAttr + @SelectPart + @FromPart + @JoinPart

		--DELETE VIEW A IF EXISTS
		SET @viewA ='[data].[VI_CurrentDataDsd_'+ CAST( @DSD_ID AS VARCHAR)+'_A]'
		IF OBJECT_ID(@viewA, 'V') IS NOT NULL
		BEGIN
			EXEC  ('DROP VIEW '+@viewA)
		END
	
		--DELETE VIEW B IF EXISTS
		SET @viewB ='[data].[VI_CurrentDataDsd_'+ CAST( @DSD_ID AS VARCHAR)+'_B]'
		IF OBJECT_ID(@viewB, 'V') IS NOT NULL
		BEGIN
			EXEC  ('DROP VIEW '+@viewB)
		END

		--CREATE THE VIEWS
		SELECT @sql = 'CREATE VIEW '+@viewA+' AS '+ REPLACE(@CTEDimGroupAttr + @SelectPart + @FromPart + @JoinPart,'{V}','A');
		EXEC sp_executesql @sql

		SELECT @sql = 'CREATE VIEW '+@viewB+' AS ' + REPLACE(@CTEDimGroupAttr + @SelectPart + @FromPart + @JoinPart,'{V}','B');
		EXEC sp_executesql @sql 
	END TRY  
	BEGIN CATCH  
		
		SELECT @DSD  = [AGENCY]+':'+[ID]+'('+CAST([VERSION_1] AS VARCHAR)+'.'+CAST([VERSION_2] AS VARCHAR)+'.'+ CASE WHEN [VERSION_3]IS NULL THEN '0' ELSE CAST([VERSION_3] AS VARCHAR) END +')'
		FROM [management].[ARTEFACT] 
		WHERE [ART_ID] = @DSD_ID AND [TYPE] = 'DSD'

		--Create new transaction for DSD
		SELECT @Transaction_ID = NEXT VALUE FOR [management].[TRANSFER_SEQUENCE];
		INSERT INTO [management].[DSD_TRANSACTION] 
		([TRANSACTION_ID],[ART_ID],[DSD_TARGET_VERSION],[EXECUTION_START],[EXECUTION_END],[SUCCESSFUL],[USER_EMAIL])
		VALUES (@Transaction_ID, @DSD_ID, 'X',GETDATE(),GETDATE(),0,NULL)
		
		SELECT @msg= 'The following error was found while trying to create the DSD Views for the DSD ['+ @DSD +'] (The creation of the view will be skiped):'+ ' ErrorNumber:'+CAST(ERROR_NUMBER() AS VARCHAR)+' ErrorMessage:'+ERROR_MESSAGE()+@NewLineChar 
	
		--LOG
		PRINT @msg
		INSERT INTO [management].[LOGS] 
		([TRANSACTION_ID],[DATE],[LEVEL],[SERVER],[LOGGER],[MESSAGE],[EXCEPTION],[APPLICATION])
		VALUES (@Transaction_ID,GETDATE(),'ERROR','MSSQL SERVER:'+@@SERVERNAME,'2020-08-04-01.AutomaticallyCreateDsdViews.sql',@msg,ERROR_MESSAGE(),'dotstatsuite-dbup')
		
		--update transaction info
		UPDATE  [management].[DSD_TRANSACTION] 
		SET [EXECUTION_END]= GETDATE(), [SUCCESSFUL] = 1
		WHERE [TRANSACTION_ID]=@Transaction_ID

	END CATCH

	DROP TABLE #Temp_DSD_Components

    DELETE #Temp_DSDs WHERE ART_ID = @DSD_ID

END

DROP TABLE #Temp_DSDs


