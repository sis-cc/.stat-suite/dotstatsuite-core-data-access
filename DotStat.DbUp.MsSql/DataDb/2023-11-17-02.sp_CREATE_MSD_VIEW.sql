CREATE OR ALTER PROC CREATE_MSD_VIEW
	@DSD_ID int,
	@MSD_ID int,
	@table_version char(1)
AS
BEGIN
	DECLARE
		@sdmxId as NVARCHAR(1000),
		@U_DSD_VERSION_ID varchar(20),
		@NewLineChar AS CHAR(2) =  CHAR(10),
		@isLiveSet as bit,
		@isPITset as bit,			
		@HasTimeDim as bit = 0,
		@TimeDimId as int = 0,
		@msg NVARCHAR(MAX),
		@sql NVARCHAR(MAX),
		@DSDLevel NVARCHAR(MAX),
		@DSLevel NVARCHAR(MAX),
		@UnionAllPart NVARCHAR(MAX);

	DECLARE
		@DSD_Components TABLE(
			COMP_ID int not null,
			ID varchar(50) not null, 
			[TYPE] varchar(50) not null, 	
			CL_ID int
		)

    SELECT
		@U_DSD_VERSION_ID = CAST(@DSD_ID AS VARCHAR) + '_' + @table_version,
		@sdmxId = [AGENCY]+':'+[ID]+'('+CAST([VERSION_1] AS VARCHAR)+'.'+CAST([VERSION_2] AS VARCHAR)+ CASE WHEN [VERSION_3] IS NULL THEN '' ELSE '.'+CAST([VERSION_3] AS VARCHAR) END +')',
		@isLiveSet = CASE WHEN [DSD_LIVE_VERSION] IS NULL THEN 0 ELSE 1 END,
		@isPITset = CASE WHEN [DSD_PIT_VERSION] IS NULL THEN 0 ELSE 1 END	
	FROM [management].[ARTEFACT] 		
	WHERE [type] = 'DSD' AND ART_ID = @DSD_ID;

	--- check if meta fact table exists
	IF (NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'data' AND  TABLE_NAME = 'META_DSD_' + @U_DSD_VERSION_ID))
	BEGIN
		-- SKIP when no meta fact table has been created and no [DSD_LIVE_VERSION], nor [DSD_PIT_VERSION] has been set
		IF( @isLiveSet = 0 AND @isPITset = 0)
		BEGIN
			RETURN	
		END

		set @msg = 'The following error was found while trying to recreate the MSD Views for the DSD ['+ @sdmxId +'] (The creation of the view will be skiped): The table [data].[META_DSD_'+ @U_DSD_VERSION_ID + '] does not exist in the database, but a value has been set for the fields [DSD_LIVE_VERSION] or [DSD_PIT_VERSION] in the [management].[ARTEFACT] for the given DSD.';

		throw 51000, @msg, 1;
	END

	-- retrieve IF DSD has TIME dimension
	SELECT @HasTimeDim=1, @TimeDimId = COMP_ID 
	FROM [management].[COMPONENT] WHERE [TYPE] ='TimeDimension' AND DSD_ID = @DSD_ID 

	-- get dimensions
	INSERT INTO @DSD_Components
	SELECT c.COMP_ID,c.ID,c.[TYPE],c.[CL_ID]
	FROM [management].[COMPONENT] c 	
	WHERE c.DSD_ID = @DSD_ID and [Type] = 'Dimension'
	
	-- get referential metadata attributes 
	INSERT INTO @DSD_Components
	SELECT m.[MTD_ATTR_ID],m.[ID], 'MetadataAttribute',m.[CL_ID]
	FROM [management].[METADATA_ATTRIBUTE] m
	WHERE m.MSD_ID = @MSD_ID
	
	----------------------------DSD ATTACHEMENT LEVEL STATEMENT---------------------------
	SET @DSDLevel = ' SELECT ' + ISNULL(STUFF(( 
		SELECT
			CASE 
			--coded dimensions and coded attributes
			WHEN C.[CL_ID] IS NOT NULL 
				THEN 'CASE WHEN [CL_'+[C].[ID]+'].[ID] IS NULL THEN ''~'' ELSE [CL_'+[C].[ID]+'].[ID] END AS ['+[C].[ID]+'],' 
			--Non-coded dimensions
			WHEN C.[CL_ID] IS NULL AND C.[TYPE] = 'Dimension'
				THEN '[ME].[DIM_'+CAST(C.[COMP_ID] AS VARCHAR)+'] AS ['+[C].[ID]+'],'
			--Non-coded referential metadata attributes 
			WHEN C.[CL_ID] IS NULL AND C.[TYPE] = 'MetadataAttribute' 
				THEN '[ME].[COMP_'+CAST(C.[COMP_ID] AS VARCHAR)+'] AS ['+[C].[ID]+'],' 
			END
		FROM @DSD_Components C  FOR XML PATH('')
	), 1, 0, ''), '')
	SET @DSDLevel +=  @NewLineChar; 
	SET @DSDLevel += 
		CASE @HasTimeDim
			WHEN 1 THEN ' CASE WHEN [PERIOD_SDMX] IS NULL THEN ''~'' ELSE [PERIOD_SDMX] END AS [TIME_PERIOD], [PERIOD_START], [PERIOD_END],' 
			ELSE ''
		END
	
	SET @DSDLevel+= ' [ME].[LAST_UPDATED]'

	--------------------------------START BUILD FROM STATEMENT-------------------------------
	SELECT @DSDLevel+='FROM [data].[META_DSD_' + @U_DSD_VERSION_ID + '] ME' + @NewLineChar 

	--------------------------------START JOIN  STATEMENT------------------------------------
	SELECT @DSDLevel += ISNULL(STUFF(( 
		SELECT 
			CASE 
			--coded dimensions
			WHEN C.[CL_ID] IS NOT NULL AND C.[TYPE] = 'Dimension'
				THEN 'LEFT JOIN [management].[CL_'+CAST(C.[CL_ID] AS VARCHAR)+'] AS [CL_'+[C].[ID]+'] ON ME.[DIM_'+CAST(C.[COMP_ID] AS VARCHAR)+'] = [CL_'+[C].[ID]+'].[ITEM_ID]'+@NewLineChar
			--coded referential metadata attributes 
			WHEN C.[CL_ID] IS NOT NULL AND C.[TYPE] = 'MetadataAttribute' 
				THEN 'LEFT JOIN [management].[CL_'+CAST(C.[CL_ID] AS VARCHAR)+'] AS [CL_'+[C].[ID]+'] ON ME.[COMP_'+CAST(C.[COMP_ID] AS VARCHAR)+'] = [CL_'+[C].[ID]+'].[ITEM_ID]'+@NewLineChar
			END
		FROM @DSD_Components C FOR XML PATH('')
	), 1, 0, ''),'') 	
	
	-------------------------------- UNION ALL DATASET AND DATAFLOW LEVELS STATEMENT-------------------------------
	SET @UnionAllPart = 'UNION ALL'+ @NewLineChar
		
	----------------------------DATASET ATTACHEMENT LEVEL STATEMENT---------------------------
	
	SET @DSLevel = 'SELECT ' + ISNULL(STUFF(( 
		SELECT 
			CASE 
			-- Dimensions
			WHEN C.[TYPE] = 'Dimension'
				THEN '''~'' AS ['+[C].[ID]+'],'
			-- Referential metadata attributes 
			WHEN C.[TYPE] = 'MetadataAttribute' 
				THEN '[COMP_'+CAST(C.[COMP_ID] AS VARCHAR)+'] AS ['+[C].[ID]+'],'
			END + @NewLineChar
		FROM @DSD_Components C  FOR XML PATH('')
	), 1, 0, ''), '')

	SET @DSLevel +=
		CASE @HasTimeDim
			WHEN 1 THEN '''~'' AS [TIME_PERIOD],''9999-12-31'' AS [PERIOD_START], ''0001-01-01'' AS [PERIOD_END],' 
			ELSE ''
		END

	SET @DSLevel += ' [LAST_UPDATED]'+ @NewLineChar
	
	SELECT @DSLevel+='FROM [data].META_DS_'+ @U_DSD_VERSION_ID + @NewLineChar; 
	SELECT @DSLevel+='WHERE [DF_ID] = -1 ' + @NewLineChar; 

	--------------------------------------------------------------------------------------------
	print 'Recreating MetadataDsd VIEW(' + @table_version + ') for DSD: ' + @sdmxId + ', ID:' + CAST(@DSD_ID AS VARCHAR);

	SET @sql = 'CREATE OR ALTER VIEW [data].[VI_MetadataDsd_' + @U_DSD_VERSION_ID + '] AS'  + @NewLineChar 
		+ @DSDLevel + @UnionAllPart  + @DSLevel 

	--PRINT @sql;
	exec sp_executesql @sql;
END