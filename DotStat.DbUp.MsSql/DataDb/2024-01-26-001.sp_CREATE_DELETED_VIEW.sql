CREATE OR ALTER PROC CREATE_DEL_VIEWS
	@DSD_ID int,
	@table_version char(1),
	@DF_ID int = NULL
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE
		@sdmxId as NVARCHAR(1000),
		@U_DSD_VERSION_ID varchar(20),
		@NewLineChar AS CHAR(2) =  CHAR(10),
		@isLiveSet as bit,
		@isPITset as bit,			
		@HasTimeDim as bit = 0,
		@msg NVARCHAR(MAX),
		@sql NVARCHAR(MAX),
		@InnerUnionSql NVARCHAR(MAX),
		@InnerSelectPartCommon NVARCHAR(MAX),
		@InnerSelectPart4SomeComp NVARCHAR(MAX),
		@InnerSelectPart4NoComp NVARCHAR(MAX),
		@InnerFromPart NVARCHAR(MAX),
		@InnerWherePart4NoComp NVARCHAR(MAX),
		@InnerWherePart4SomeComp NVARCHAR(MAX),
		@InnerGroupByPart NVARCHAR(MAX),
		@OuterSelectPart NVARCHAR(MAX),
		@OuterFromPart NVARCHAR(MAX),
		@Current_DF_ID int

	DECLARE
		@DSD_Components TABLE(
			COMP_ID int not null,
			ID varchar(50) not null, 
			[TYPE] varchar(50) not null, 	
			CL_ID int, 
			ATT_ASS_LEVEL varchar(50),
			ATT_STATUS varchar(11)
		);

	DECLARE 
		Dataflows_Cursor CURSOR FORWARD_ONLY 
		FOR 
			SELECT ART_ID, 
				   [AGENCY] + ':' + [ID] + '(' + CAST( [VERSION_1] AS VARCHAR) + '.' + CAST([VERSION_2] AS VARCHAR) + CASE WHEN [VERSION_3] IS NULL THEN '' ELSE '.' + CAST( [VERSION_3] AS VARCHAR) END + ')' AS SDMX_ID	
			  FROM management.ARTEFACT 
	         WHERE [TYPE] = 'DF' 
			   AND DF_DSD_ID = @DSD_ID 
			   AND (@DF_ID IS NULL OR ART_ID = @DF_ID);

    SET @U_DSD_VERSION_ID = CAST(@DSD_ID AS VARCHAR) + '_' + @table_version;

    SELECT
		@sdmxId = [AGENCY]+':'+[ID]+'('+CAST([VERSION_1] AS VARCHAR)+'.'+CAST([VERSION_2] AS VARCHAR)+ CASE WHEN [VERSION_3] IS NULL THEN '' ELSE '.'+CAST([VERSION_3] AS VARCHAR) END +')',
		@isLiveSet = CASE WHEN [DSD_LIVE_VERSION] IS NULL THEN 0 ELSE 1 END,
		@isPITset = CASE WHEN [DSD_PIT_VERSION] IS NULL THEN 0 ELSE 1 END	
	  FROM [management].[ARTEFACT] 		
	 WHERE [type] = 'DSD' AND ART_ID = @DSD_ID;

	--- check if fact table exists
	IF (NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'data' AND  TABLE_NAME = 'DELETED_' + @U_DSD_VERSION_ID))
	BEGIN
		-- SKIP when no fact table has been created and no [DSD_LIVE_VERSION], nor [DSD_PIT_VERSION] has been set
		IF( @isLiveSet = 0 AND @isPITset = 0)
		BEGIN
			RETURN	
		END

		IF (@sdmxId IS NULL)
			set @msg = 'The following error was found while trying to recreate the DELETED views for the DSD [ DbId = ' + CAST(@DSD_ID AS VARCHAR) + ' ] (The creation of the view will be skiped): There is no dsd artefact with the given DSD_ID in the [management].[ARTEFACT] table.';		
		ELSE
			set @msg = 'The following error was found while trying to recreate the DELETED views for the DSD [ ' + @sdmxId + ' ] (The creation of the view will be skiped): The table [data].[DELETED_'+ @U_DSD_VERSION_ID + '] does not exist in the database, but a value has been set for the fields [DSD_LIVE_VERSION] or [DSD_PIT_VERSION] in the [management].[ARTEFACT] table for the given DSD.';

		throw 51000, @msg, 1;
	END

	-- retrieve IF DSD has TIME dimension
	SELECT @HasTimeDim = 1 FROM [management].[COMPONENT] WHERE [TYPE] ='TimeDimension' AND DSD_ID = @DSD_ID 

	-- get dimension & attribute components
	INSERT 
	  INTO @DSD_Components
	SELECT c.COMP_ID, c.ID, c.[TYPE], c.[CL_ID], c.[ATT_ASS_LEVEL], c.[ATT_STATUS]
	  FROM [management].[COMPONENT] c
	 WHERE c.DSD_ID = @DSD_ID 
	   AND [Type] IN ('Dimension','Attribute')

	IF @@ROWCOUNT = 0
	BEGIN
		set @msg = 'The following error was found while trying to recreate the DELETED views for the DSD [ ' + @sdmxId + ' ] (The creation of the view will be skiped): There are no dimension and attribute components in the [management].[COMPONENT] table for the given DSD.';

		throw 51000, @msg, 1;
	END

	----------------------------BUILD INNER SELECT CLAUSE---------------------------

	SET @InnerSelectPartCommon = 'SELECT '
		+ CASE @HasTimeDim
			WHEN 1 THEN '[PERIOD_SDMX] AS [TIME_PERIOD], [PERIOD_START], [PERIOD_END],' 
			ELSE ''
		END + @NewLineChar;
	
	SET @InnerSelectPartCommon += ISNULL(( 
		SELECT '[DIM_' + CAST(C.[COMP_ID] AS VARCHAR) + '], '
		  FROM @DSD_Components C 
		 WHERE C.[TYPE] = 'Dimension' 
		   FOR XML PATH('')) , '') + @NewLineChar;

	SET @InnerSelectPart4SomeComp = ISNULL(( 
		SELECT 'MAX( [COMP_' + CAST(C.[COMP_ID] AS VARCHAR) + '] ) AS [' + [C].[ID] + '], '
		  FROM @DSD_Components C 
		 WHERE C.[TYPE] = 'Attribute' 
		   FOR XML PATH('')) , '') + @NewLineChar +
        'MAX( [VALUE] ) AS [OBS_VALUE], MAX( [LAST_UPDATED] ) AS [LAST_UPDATED]' + @NewLineChar;

	SET @InnerSelectPart4NoComp = ISNULL(( 
		SELECT 'NULL AS [' + [C].[ID] + '], '
		  FROM @DSD_Components C 
		 WHERE C.[TYPE] = 'Attribute' 
		   FOR XML PATH('')) , '') + @NewLineChar +
        'NULL AS [OBS_VALUE], MAX( [LAST_UPDATED] ) AS [LAST_UPDATED]' + @NewLineChar;
	
	--------------------------------BUILD INNER FROM CLAUSE-------------------------------

	SELECT @InnerFromPart = 'FROM [data].DELETED_'+ @U_DSD_VERSION_ID + ' AS [DEL]' + @NewLineChar; 
	
	--------------------------------BUILD INNER WHERE CLAUSE-------------------------------

	SELECT @InnerWherePart4NoComp = 'WHERE [DEL].[DF_ID] = {DF_ID}' + @NewLineChar +
		ISNULL(STUFF(( 
			SELECT ' AND [COMP_' + CAST(C.[COMP_ID] AS VARCHAR) + '] IS NULL' 
			  FROM @DSD_Components C  
			 WHERE C.[TYPE] = 'Attribute'
			   FOR XML PATH('')
		), 1, 0, ''), '') + @NewLineChar +
		'AND [VALUE] IS NULL' + @NewLineChar;

	SELECT @InnerWherePart4SomeComp = 'WHERE [DEL].[DF_ID] = {DF_ID}' + @NewLineChar + ' AND (' +
		ISNULL(STUFF(( 
			SELECT ' OR [COMP_' + CAST(C.[COMP_ID] AS VARCHAR) + '] IS NOT NULL'
			  FROM @DSD_Components C  
			 WHERE C.[TYPE] = 'Attribute'	 
			   FOR XML PATH('')
		), 1, 3, ''), '1 = 1') + @NewLineChar +
		'OR [VALUE] IS NOT NULL' + ')' + @NewLineChar;

	----------------------------BUILD INNER GROUP BY CLAUSE---------------------------

	SET @InnerGroupByPart = 'GROUP BY ' + ISNULL(STUFF(( 
		SELECT ', [DIM_' + CAST(C.[COMP_ID] AS VARCHAR) + ']' 
		  FROM @DSD_Components C 
		 WHERE C.[TYPE] = 'Dimension'	 
		   FOR XML PATH('')
	), 1, 1, ''), '') + @NewLineChar;
	
	SET @InnerGroupByPart += 
		CASE @HasTimeDim
			WHEN 1 THEN ', [PERIOD_SDMX], [PERIOD_START], [PERIOD_END]' 
			ELSE ''
		END + @NewLineChar;

    ----------------------------BUILD INNER UNION SELECT STATEMENT---------------------------

	SET @InnerUnionSql =  @InnerSelectPartCommon + @InnerSelectPart4SomeComp + @InnerFromPart + @InnerWherePart4SomeComp + @InnerGroupByPart + @NewLineChar;
	SET @InnerUnionSql += ' UNION ' + @NewLineChar;
	SET @InnerUnionSql += @InnerSelectPartCommon + @InnerSelectPart4NoComp + @InnerFromPart + @InnerWherePart4NoComp + @InnerGroupByPart + @NewLineChar;

	----------------------------BUILD OUTER SELECT CLAUSE---------------------------

	SET @OuterSelectPart = 'SELECT [OBS_VALUE]' 
		+ CASE @HasTimeDim
			WHEN 1 THEN ', [TIME_PERIOD], [PERIOD_START], [PERIOD_END]' 
			ELSE ''
		END + @NewLineChar

	SET @OuterSelectPart += ISNULL(STUFF(( 
		SELECT ', ' +
			CASE 
			-- Coded Dimensions
			WHEN C.[CL_ID] IS NOT NULL AND C.[TYPE] = 'Dimension'
				THEN '[CL_' + [C].[ID] + '].[ID] AS [' + [C].[ID] + ']' 
            -- Non coded dimensions
			WHEN C.[CL_ID] IS NULL AND C.[TYPE] = 'Dimension'
				THEN '[DIM_' + CAST(C.[COMP_ID] AS VARCHAR) + '] AS [' + [C].[ID] + ']' 
			-- Attributes
			WHEN C.[TYPE] = 'Attribute'
				THEN '[' + [C].[ID] + ']' 
			END + @NewLineChar
		FROM @DSD_Components C  FOR XML PATH('')
	), 1, 0, ''), '')
		
	SET @OuterSelectPart += ', [LAST_UPDATED]'+ @NewLineChar
	
	--------------------------------BUILD OUTER FROM CLAUSE-------------------------------

	SELECT @OuterFromPart = 'FROM ( ' + @NewLineChar + @InnerUnionSql + @NewLineChar + ' ) AS [DEL]' + @NewLineChar; 
	
	SELECT @OuterFromPart += ISNULL(STUFF(( 
		SELECT 'LEFT JOIN [management].[CL_'+CAST(C.[CL_ID] AS VARCHAR)+'] AS [CL_'+[C].[ID]+'] ON [DEL].[DIM_'+CAST(C.[COMP_ID] AS VARCHAR)+'] = [CL_'+[C].[ID]+'].[ITEM_ID]' + @NewLineChar
		FROM @DSD_Components C 
		WHERE C.[CL_ID] IS NOT NULL AND C.[TYPE] = 'Dimension'	 
		FOR XML PATH('')
	), 1, 0, ''),'') 
		
	---------------------------------CREATE OR REPLACE STORED PROCEDURE------------------------------------------
	
	OPEN Dataflows_Cursor  
  
    FETCH NEXT FROM Dataflows_Cursor   
    INTO @Current_DF_ID, @sdmxId
  
    WHILE @@FETCH_STATUS = 0  
	BEGIN  
		PRINT 'Recreating Deleted VIEW(' + @table_version + ') for DF: ' + @sdmxId + ', ID:' + CAST(@Current_DF_ID AS VARCHAR);

		SET @sql = REPLACE(
			'CREATE OR ALTER VIEW [data].[VI_DeletedDataDataFlow_{DF_ID}_' + @table_version + '] AS'  + @NewLineChar + @OuterSelectPart + @OuterFromPart,
			'{DF_ID}',
			CAST(@Current_DF_ID AS VARCHAR)
		);
		
		EXEC sp_executesql @sql;

	    -- Get the next row.  
		FETCH NEXT FROM Dataflows_Cursor   
		INTO @Current_DF_ID, @sdmxId 
	END   

	CLOSE Dataflows_Cursor;  
	DEALLOCATE Dataflows_Cursor; 
END