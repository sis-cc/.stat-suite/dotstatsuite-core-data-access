DECLARE 
	@TimeZone VARCHAR(50),
	@Msg VARCHAR(8000);

SET @TimeZone = '$timezone$';

IF @TimeZone IS NULL OR @TimeZone = '' 
BEGIN
	EXEC MASTER.dbo.xp_regread 
					'HKEY_LOCAL_MACHINE',
					'SYSTEM\CurrentControlSet\Control\TimeZoneInformation',
					'TimeZoneKeyName',
					@TimeZone OUT
END
ELSE
BEGIN
	IF NOT EXISTS(SELECT * FROM sys.time_zone_info WHERE UPPER(name) LIKE UPPER(@TimeZone)) --LIKE is used to not allow trailing spaces
	BEGIN
		SELECT @Msg = 'Invalid timezone provided: [' + @TimeZone + ']. See information about supported time zones in sys.time_zone_info view, e.g.: ' ;
		SELECT @Msg = @Msg + STRING_AGG(name, ',') FROM sys.time_zone_info;

		THROW 2024011501, @Msg, 1
	END
END

SELECT 'Time zone: [' + @TimeZone + ']';

UPDATE [management].[DSD_TRANSACTION]
	SET [EXECUTION_START] = CONVERT(DATETIME, [EXECUTION_START] AT TIME ZONE @timezone AT TIME ZONE 'UTC') ,
		[EXECUTION_END] = CONVERT(DATETIME, [EXECUTION_END] AT TIME ZONE @timezone AT TIME ZONE 'UTC'),
		[QUEUED_DATETIME] = CONVERT(DATETIME, [QUEUED_DATETIME] AT TIME ZONE @timezone AT TIME ZONE 'UTC'),
		[LAST_UPDATED] = CONVERT(DATETIME, [LAST_UPDATED] AT TIME ZONE @timezone AT TIME ZONE 'UTC');

UPDATE [management].[LOGS]
	SET [DATE] = CONVERT(DATETIME, [DATE] AT TIME ZONE @timezone AT TIME ZONE 'UTC');