SET NOCOUNT ON
SET CONCAT_NULL_YIELDS_NULL ON
GO
  
DECLARE 	
	@DSD_ID int,
	@sql NVARCHAR(MAX),	
	@DimCount int,	
	@DIMS varchar(max),
	@FILT_TBL varchar(50),
	@NewLineChar AS CHAR(2) =  CHAR(10);
	
DECLARE
	@DSD_IDS TABLE(ART_ID int PRIMARY KEY);

INSERT into @DSD_IDS 
	SELECT ART_ID FROM [management].[ARTEFACT]
	WHERE [type] = 'DSD' ORDER BY ART_ID
	
WHILE (Select Count(*) FROM @DSD_IDS) > 0
BEGIN	
    SELECT TOP 1 @DSD_ID = ART_ID FROM @DSD_IDS
	DELETE @DSD_IDS WHERE ART_ID = @DSD_ID

	SET @FILT_TBL = 'FILT_' + CAST(@DSD_ID AS VARCHAR);

	IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE [Name] = N'ROW_ID' AND Object_ID = Object_ID(N'[data].' + @FILT_TBL))
	BEGIN
		CONTINUE
	END

	SET @DIMS = null;

	SELECT @DimCount = count(*) FROM [management].[COMPONENT] c 
    WHERE c.DSD_ID=@DSD_ID AND c.[TYPE] = 'Dimension'

	SELECT @DIMS=CONCAT(@DIMS + ',','DIM_'+CAST(c.COMP_ID AS VARCHAR)) FROM [management].[COMPONENT] c 
    WHERE c.DSD_ID=@DSD_ID AND c.[TYPE] = 'Dimension'
	
	/* remove ROW_ID from FILT table if dataset has less than 33 NON-TIME dimensions*/
	IF(@DimCount <= 32)
	BEGIN TRY
		BEGIN TRAN

		SELECT @sql = 'DROP index UQ_ROW_ID_' + CAST(@DSD_ID AS VARCHAR) + ' on data.' + @FILT_TBL;
		exec sp_executesql @sql;

		SELECT @sql = 'ALTER TABLE data.' + @FILT_TBL + ' DROP COLUMN ROW_ID';
		exec sp_executesql @sql;
	
		SELECT @sql = 'CREATE UNIQUE INDEX U_' + @FILT_TBL + ' ON data.' + @FILT_TBL + '(' + @Dims + ')';
		exec sp_executesql @sql;
		
		COMMIT TRAN
	END TRY  
	BEGIN CATCH  		
		ROLLBACK TRAN

		DECLARE 
			@DSD nvarchar(100),
			@msg nvarchar(1000),
			@Transaction_ID int

		SELECT @DSD  = [AGENCY]+':'+[ID]+'('+CAST([VERSION_1] AS VARCHAR)+'.'+CAST([VERSION_2] AS VARCHAR)+'.'+ CASE WHEN [VERSION_3]IS NULL THEN '0' ELSE CAST([VERSION_3] AS VARCHAR) END +')'
		FROM [management].[ARTEFACT] 
		WHERE [ART_ID] = @DSD_ID AND [TYPE] = 'DSD'

		--Create new transaction for DSD
		SELECT @Transaction_ID = NEXT VALUE FOR [management].[TRANSFER_SEQUENCE];
		INSERT INTO [management].[DSD_TRANSACTION] 
		([TRANSACTION_ID],[ART_ID],[DSD_TARGET_VERSION],[EXECUTION_START],[EXECUTION_END],[SUCCESSFUL],[ARTEFACT_FULL_ID],[USER_EMAIL])
		VALUES (@Transaction_ID, @DSD_ID, 'X',GETDATE(),GETDATE(),0, @DSD,NULL)
		
		SELECT @msg= 'The following error was found while trying to remove ROW_ID FROM ['+ @DSD +']:'+ ' ErrorNumber:'+CAST(ERROR_NUMBER() AS VARCHAR)+' ErrorMessage:'+ERROR_MESSAGE()+@NewLineChar 
	
		--LOG
		PRINT @msg
		INSERT INTO [management].[LOGS] 
		([TRANSACTION_ID],[DATE],[LEVEL],[SERVER],[LOGGER],[MESSAGE],[EXCEPTION],[APPLICATION])
		VALUES (@Transaction_ID,GETDATE(),'ERROR','MSSQL SERVER:'+@@SERVERNAME,'2020-11-20-02.Remove-ROW_ID-Column.sql',@msg,ERROR_MESSAGE(),'dotstatsuite-dbup')
		
		--update transaction info
		UPDATE  [management].[DSD_TRANSACTION] 
		SET [EXECUTION_END]= GETDATE(), [SUCCESSFUL] = 1,TIMED_OUT=0
		WHERE [TRANSACTION_ID]=@Transaction_ID

	END CATCH
END