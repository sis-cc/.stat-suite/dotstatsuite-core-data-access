﻿-- Login (server principal) and user (database principal) creation/alteration has been split into 2 separate scripts in order 
-- to make DbUp tool compatible with Azure SQL Database. On SQL Database it is not supported to switch between databases
-- nor referencing other database than the one defined in the conenction string.
IF NOT EXISTS (
   SELECT 1 FROM sys.server_principals WHERE name = '$ROloginName$'
   UNION
   SELECT 1 FROM sys.sql_logins WHERE name = '$ROloginName$' -- Azure SQL Database
   )
BEGIN
    CREATE LOGIN [$ROloginName$] WITH PASSWORD = N'$ROloginPwd$';
END
ELSE IF '$alterPassword$' = '1' 
BEGIN
    ALTER LOGIN [$ROloginName$] WITH PASSWORD = N'$ROloginPwd$';
END
