﻿using System.Collections.Generic;
using System.Linq;
using DotStat.Common.Enums;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;

namespace DotStat.Domain
{
    public class Dataflow : MaintainableDotStatObject<IDataflowObject>, IImportReferenceableStructure
    {
        public bool IsFirstImport;
        public const string DefaultValueField = "VALUE";
        public string ValueFieldName => DefaultValueField;
        public Dsd Dsd { get;}

        public IReadOnlyList<Dimension> Dimensions => Dsd.Dimensions;

        public IReadOnlyList<Attribute> ObsAttributes => Dsd
                                                            .Attributes
                                                            .Where(x=>x.Base.AttachmentLevel == AttributeAttachmentLevel.Observation)
                                                            .ToArray();
        #region Constructor

        public Dataflow(IDataflowObject @base, Dsd dsd) : base(@base)
        {
            DbType = DbTypes.GetDbType(SDMXArtefactType.Dataflow);
            Dsd = dsd;
        }

        #endregion

        public virtual IEnumerable<string> GetColumnNames()
        {
            yield return ValueFieldName;

            foreach (var dim in Dimensions)
                yield return dim.FullCode;

            foreach (var attr in ObsAttributes)
                yield return attr.FullCode;
        }

        public Dimension FindDimension(string code)
        {
            return Dimensions.FirstOrDefault(dim => dim.Code.Equals(code));
        }
    }
}