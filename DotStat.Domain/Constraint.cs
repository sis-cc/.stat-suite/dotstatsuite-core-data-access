﻿using System.Collections.Generic;
using System.Linq;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Registry;

namespace DotStat.Domain
{
    public class Constraint : MaintainableDotStatObject<IContentConstraintObject>
    {
        public readonly bool Include;
        public readonly HashSet<string> Codes;
        public readonly ITimeRange TimeRange;

        public Constraint(IContentConstraintObject @base, bool include, IList<string> codes, ITimeRange timeRange) : base(@base)
        {
            Include = include;
            Codes = new HashSet<string>(codes ?? Enumerable.Empty<string>());
            TimeRange = timeRange;
        }

        public bool HasValidCodes = true;

        public bool IsCodeListBased => Codes.Any();
    }
}
