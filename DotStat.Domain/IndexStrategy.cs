﻿namespace DotStat.Domain
{
    public enum DataCompressionEnum
    {
        NONE = 0,
        COLUMNSTORE = 1,
        COLUMNSTORE_ARCHIVE = 2,
    }

}
