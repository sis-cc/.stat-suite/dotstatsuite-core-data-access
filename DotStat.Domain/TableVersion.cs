﻿using System;

namespace DotStat.Domain
{
    public enum DbTableVersion
    {
        A = 'A',
        B = 'B',
        X = 'X' // Used for cases when not only one table set is targeted, e.g. cleanup of a dsd's database objects
    }

    public enum OptimizedTableVersion
    {
        X = 'X',
        Y = 'Y'
    }

    public static class DbTableVersions
    {
        public static char GetDefaultLiveVersion()
        {
            return (char)DbTableVersion.A;
        }

        public static DbTableVersion? GetDbTableVersion(char dbTableVersion)
        {
            return GetDbTableVersion(dbTableVersion.ToString());
        }

        public static DbTableVersion? GetDbTableVersion(string dbTableVersion)
        {
            if (string.IsNullOrWhiteSpace(dbTableVersion) || !Enum.TryParse<DbTableVersion>(dbTableVersion, out var tableVersion))
            {
                return null;
            }

            return tableVersion;
        }

        public static DbTableVersion GetNewTableVersion(DbTableVersion dbTableVersion)
        {
            if (dbTableVersion == DbTableVersion.A)
                return DbTableVersion.B;
            return DbTableVersion.A;
        }

        public static bool IsValidVersion(char dbTableVersion)
        {
            return Enum.IsDefined(typeof(DbTableVersion), dbTableVersion.ToString());
        }
    }
}
