﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;

namespace DotStat.Domain
{
    public static class Extensions
    {
        public static Dictionary<string, string> ToDict(this IList<ITextTypeWrapper> names)
        {
            return names.ToDictionary(x => x.Locale, x => x.Value);
        }

        #region String

        public static Guid ToGuid(this string str)
        {
            return Guid.Parse(str);
        }

        public static DateTime? ToDt(this string str)
        {
            DateTime temp;
            return !string.IsNullOrEmpty(str) && str.Trim().Length > 0 && DateTime.TryParse(str, out temp)
                        ? (DateTime?)temp
                        : null;
        }

        public static bool? ToBool(this string str)
        {
            bool temp;
            return !string.IsNullOrEmpty(str) && str.Trim().Length > 0 && bool.TryParse(str, out temp)
                        ? (bool?)temp
                        : null;
        }

        public static int? ToInt(this string str)
        {
            int temp;
            return !string.IsNullOrEmpty(str) && str.Trim().Length > 0 && int.TryParse(str, NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture, out temp)
                        ? (int?)temp
                        : null;
        }

        public static decimal? ToDec(this string str)
        {
            decimal temp;
            return !string.IsNullOrEmpty(str) && str.Trim().Length > 0 && decimal.TryParse(str, NumberStyles.Any, CultureInfo.InvariantCulture, out temp)
                        ? (decimal?)temp
                        : null;
        }

        public static double? ToDouble(this string str)
        {
            double temp;
            return !string.IsNullOrEmpty(str) && str.Trim().Length > 0 && double.TryParse(str, NumberStyles.Any, CultureInfo.InvariantCulture, out temp)
                        ? (double?)temp
                        : null;
        }

        public static string Limit(this string str, int size)
        {
            if (string.IsNullOrEmpty(str) || str.Length <= size)
                return str;

            return str.Substring(0, size);
        }

        #endregion

        #region Constraint

        /// <summary>
        /// Is dimension member not constrained
        /// </summary>
        /// <param name="dimension"></param>
        /// <param name="dimMemberCode"></param>
        /// <returns></returns>
        public static bool IsAllowed(this Dimension dimension, string dimMemberCode)
        {
            return dimension.Constraint.IsAllowed(dimMemberCode);
        }

        /// <summary>
        /// Is attribute member not constrained
        /// </summary>
        /// <param name="attribute"></param>
        /// <param name="code"></param>
        /// <returns></returns>
        public static bool IsAllowed(this Attribute attribute, string code)
        {
            return attribute.Constraint.IsAllowed(code);
        }

        /// <summary>
        /// Is dimension member not constrained
        /// </summary>
        /// <param name="constraint"></param>
        /// <param name="dimMemberCode"></param>
        /// <returns></returns>
        public static bool IsAllowed(this Constraint constraint, string dimMemberCode)
        {
            return constraint == null 
                   || !constraint.Codes.Any() 
                   || !(constraint.Include ^ constraint.Codes.Contains(dimMemberCode));
        }
        #endregion

        #region IMaintainableObject

        public static string FullId(this IMaintainableObject obj)
        {
            return $"{obj.AgencyId}:{obj.Id}({obj.Version})";
        }

        public static string FullId(this IStructureReference obj)
        {
            return $"{obj.AgencyId}:{obj.MaintainableId}({obj.Version})";
        }

        #endregion

        public static Dictionary<int, DataSetAttributeRow> ToDict(this IList<DataSetAttributeRow> dataSetAttributeRows)
        {
            var dictionary = new Dictionary<int, DataSetAttributeRow>();
            foreach (DataSetAttributeRow dataSetAttributeRow in dataSetAttributeRows)
            {
                if (dictionary.TryGetValue(dataSetAttributeRow.BatchNumber, out var row))
                {
                    dictionary[dataSetAttributeRow.BatchNumber].Attributes.ToList().AddRange(row.Attributes);
                }
                else
                {
                    dictionary.Add(dataSetAttributeRow.BatchNumber, dataSetAttributeRow);
                }
            }
            return dictionary;
        }

        public static Dictionary<int, DataSetAttributeRow> MergeToDict(this IList<DataSetAttributeRow> dataSetAttributeRows1, IList<DataSetAttributeRow> dataSetAttributeRows2)
        {
            var dictionary = new Dictionary<int, DataSetAttributeRow>();
            var dataSetAttributeRows = (dataSetAttributeRows1 ?? new List<DataSetAttributeRow>()).Concat(dataSetAttributeRows2 ?? new List<DataSetAttributeRow>());
            foreach (DataSetAttributeRow dataSetAttributeRow in dataSetAttributeRows)
            {
                if (dictionary.TryGetValue(dataSetAttributeRow.BatchNumber, out var row))
                {
                    if (row.Action == dataSetAttributeRow.Action)
                    {
                        dictionary[dataSetAttributeRow.BatchNumber].Attributes.ToList().AddRange(row.Attributes);
                    }
                    else
                    {
                        throw new ArgumentException("Error trying to merge the two IList<DataSetAttributeRow> lists. There are different Actions for the same BatchNumber.");
                    }
                }
                else
                {
                    dictionary.Add(dataSetAttributeRow.BatchNumber, dataSetAttributeRow);
                }
            }

            return dictionary;
        }

        public static Dictionary<string, (List<Dimension> Dimensions, List<Attribute> Attributes)>
            GetAttributeGroupsOfSameDimensionRelations(this IList<Attribute> attributes, Dsd dsd)
        {
            //group attributes referencing the same dimensions
            var attributesWithSameDimensionReferences =
                new Dictionary<string, (List<Dimension> Dimensions, List<Attribute> Attributes)>();

            foreach (var attribute in attributes)
            {
                var dimensionsReferenced = attribute.Base.AttachmentLevel == AttributeAttachmentLevel.Observation
                    ? dsd.Dimensions.ToList()
                    : dsd.Dimensions.Where(dim => attribute.Base.DimensionReferences.Contains(dim.Code))
                        .ToList();

                if (dimensionsReferenced.Count == 0)
                {
                    var groupDimRefs = dsd.Base.Groups.Where(g =>
                            g.Id.Equals(attribute.Base.AttachmentGroup, StringComparison.InvariantCultureIgnoreCase))
                        .Select(g => g.DimensionRefs).FirstOrDefault();

                    if (groupDimRefs is { Count: > 0 })
                        dimensionsReferenced = dsd.Dimensions.Where(dim => groupDimRefs.Contains(dim.Code))
                            .ToList();
                }

                if (dimensionsReferenced.Count == 0)
                    continue;

                var coordinate = string.Join(",", dimensionsReferenced.OrderBy(e => e.Code).Select(d => d.Code));
                if (attributesWithSameDimensionReferences.ContainsKey(coordinate))
                    attributesWithSameDimensionReferences[coordinate].Attributes.Add(attribute);
                else
                    attributesWithSameDimensionReferences.Add(coordinate, (dimensionsReferenced, new List<Attribute>
                    {
                        attribute
                    }));
            }

            return attributesWithSameDimensionReferences;
        }
    }
}
